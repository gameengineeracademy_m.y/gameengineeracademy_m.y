﻿#include "Game/Key.h"

namespace {

  /// <summary>
  /// 拡大率 1.0倍
  /// </summary>
  const float kExRateMax = 1.0f;

  /// <summary>
  /// 拡大率 0.95倍
  /// </summary>
  const float kExRateMin = 0.95f;

  /// <summary>
  /// リセット時間
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 待機時間
  /// </summary>
  const float kWaitTime = 0.08f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name="key_type"> キーの種類 </param>
/// <returns></returns>
Key::Key(KeyType key_type)
  : current_phase_(PhaseType::kPrepare)
  , key_type_(key_type)
  , key_state_(KeyState::kDefault)
  , key_state_old_(KeyState::kDefault)
  , image_list_()
  , disp_()
  , key_wait_time_(0.0f) {

  // コンソールに出力
  std::cout << "Key コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Key::~Key() {

  // コンソールに出力
  std::cout << "~Key デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void Key::Update(float process_time) {

  if (key_state_old_ != key_state_) {
    switch (key_state_) {
    case KeyState::kDefault: {
      // 一旦押したままの状態に戻す
      key_state_ = KeyState::kPress;
      // 待機時間を加算
      // 待機時間が指定時間を超えたらキーの状態を押されていない状態にする
      key_wait_time_ += process_time;
      if (key_wait_time_ >= kWaitTime) {
        key_wait_time_ = kResetTime;
        key_state_ = KeyState::kDefault;
      }
      break;
    }
    }
  }
  else {
    // 1フレーム前のキーの状態に現在の状態を渡す
    key_wait_time_ = kResetTime;
    key_state_old_ = key_state_;
  }

  switch (key_state_) {
  case KeyState::kDefault: {
    // 拡大率を1.0倍に変更
    disp_.ex_rate = kExRateMax;
    break;
  }
  case KeyState::kPress: {
    // 拡大率を0.95倍に変更
    disp_.ex_rate = kExRateMin;
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Key::Render() {

  switch (current_phase_) {
  case PhaseType::kProcess: {
    DrawRotaGraph(disp_.pos.x, disp_.pos.y, disp_.ex_rate, disp_.angle,
                  image_list_[key_state_], true);
    break;
  }
  }
}

/// <summary>
/// キーの表示座標をセット
/// </summary>
/// <param name="x_pos"> X座標 </param>
/// <param name="y_pos"> Y座標 </param>
/// <returns></returns>
void Key::SetKeyPosition(int x_pos, int y_pos) {

  disp_.pos.x = x_pos;
  disp_.pos.y = y_pos;
}

/// <summary>
/// キーの状態を変更する
/// </summary>
/// <param name="key_state"> キーの状態 </param>
/// <returns></returns>
void Key::ChangeKeyState(KeyState key_state) {

  // 1フレーム前のキーの状態を取得
  // 現在のキーの状態を変更
  key_state_old_ = key_state_;
  key_state_ = key_state;
}

/// <summary>
/// リセット処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Key::ResetKeyState() {

  // キーの状態を押していない状態にする
  key_state_old_ = KeyState::kDefault;
  key_state_ = KeyState::kDefault;
}