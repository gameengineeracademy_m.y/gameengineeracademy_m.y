﻿#pragma once

#include "DxLib.h"
#include <iostream>
#include <unordered_map>

/// <summary>
/// キー処理
/// </summary>
class Key {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kPrepare,        // 準備フェーズ
    kProcess,        // 処理中フェーズ
    kPhaseMaxIndex   // フェーズ数
  };

  /// <summary>
  /// キーの種類
  /// </summary>
  enum class KeyType {
    kW,                 // Wキー
    kS,                 // Sキー
    kA,                 // Aキー
    kD,                 // Dキー
    kUp,                // 上矢印キー
    kDown,              // 下矢印キー
    kLeft,              // 左矢印キー
    kRight,             // 右矢印キー
    kSpace,             // Spaceキー
    kTypeMaxIndex       // キーの種類数
  };

  /// <summary>
  /// キーの状態
  /// </summary>
  enum class KeyState {
    kDefault,           // 押されていない
    kPress,             // 押されている
    kTypeMaxIndex       // 種類数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> キーの種類 </param>
  /// <returns></returns>
  Key(KeyType);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~Key();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float);

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render();

  /// <summary>
  /// 画像ハンドルをセット
  /// </summary>
  /// <param name="state"> キーの状態 </param>
  /// <param name="handle"> キーの画像 </param>
  /// <returns></returns>
  void SetImageHandle(KeyState state, int handle) { image_list_[state] = handle; }

  /// <summary>
  /// 画像ハンドルを取得
  /// </summary>
  /// <param name="state"> キーの状態 </param>
  /// <returns> 画像ハンドル </returns>
  int GetImageHandle(KeyState state) { return image_list_[state]; }

  /// <summary>
  /// キーの表示座標をセット
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  void SetKeyPosition(int, int);

  /// <summary>
  /// 拡大率をセット
  /// </summary>
  /// <param name=""> 拡大率 </param>
  /// <returns></returns>
  void SetExRate(float ex_rate) { disp_.ex_rate = ex_rate; }

  /// <summary>
  /// 角度をセット
  /// </summary>
  /// <param name=""> 角度 </param>
  /// <returns></returns>
  void SetAngle(float angle) { disp_.angle = angle; }

  /// <summary>
  /// リセット処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ResetKeyState();

  /// <summary>
  /// キーの状態を変更する
  /// </summary>
  /// <param name=""> キーの状態 </param>
  /// <returns></returns>
  void ChangeKeyState(KeyState);

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name="phase_type"> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }


private:

  /// <summary>
  /// 位置
  /// </summary>
  struct Pos {
    int x;
    int y;
  };

  /// <summary>
  /// 表示データ
  /// </summary>
  struct DispData {
    Pos pos;
    float ex_rate;
    float angle;
  };

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// キーの種類
  /// </summary>
  KeyType key_type_;

  /// <summary>
  /// キーの状態
  /// </summary>
  KeyState key_state_;

  /// <summary>
  /// 1フレーム前のキーの状態
  /// </summary>
  KeyState key_state_old_;

  /// <summary>
  /// キーの画像ハンドル
  /// </summary>
  std::unordered_map<KeyState, int> image_list_;

  /// <summary>
  /// 表示情報
  /// </summary>
  DispData disp_;

  /// <summary>
  /// キーが離されてからの待機時間
  /// </summary>
  float key_wait_time_;
};