﻿#pragma once

#include "DxLib.h"
#include "Game/CameraControllerEventInterface.h"

/// <summary>
/// カメラベース処理
/// </summary>
class CameraBase {
public:

  enum class Direction {
    kFront,         // 前進
    kBack,          // 後退
    kLeft,          // 左
    kRight,         // 右
    kUp,            // 上
    kDown,          // 下
    kTypeMaxIndex   // 方向の種類
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  CameraBase();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~CameraBase();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Update();

  /// <summary>
  /// カメラの位置(X座標)のセット
  /// </summary>
  /// <param name="x_pos"> X座標 </param>
  /// <returns></returns>
  void SetCameraPositionX(float x_pos) { pos_.x = x_pos; }

  /// <summary>
  /// カメラの位置(Y座標)のセット
  /// </summary>
  /// <param name="y_pos"> Y座標 </param>
  /// <returns></returns>
  void SetCameraPositionY(float y_pos) { pos_.y = y_pos; }

  /// <summary>
  /// カメラの位置(Z座標)のセット
  /// </summary>
  /// <param name="z_pos"> Z座標 </param>
  /// <returns></returns>
  void SetCameraPositionZ(float z_pos) { pos_.z = z_pos; }

  /// <summary>
  /// カメラの位置を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> ベクトル </returns>
  VECTOR GetCameraPosition() { return pos_; }

  /// <summary>
  /// 注視点の位置(X座標)のセット
  /// </summary>
  /// <param name="x_pos"> X座標 </param>
  /// <returns></returns>
  void SetLookAtPositionX(float x_pos) { pos_look_.x = x_pos; }

  /// <summary>
  /// 注視点の位置(Y座標)のセット
  /// </summary>
  /// <param name="y_pos"> Y座標 </param>
  /// <returns></returns>
  void SetLookAtPositionY(float y_pos) { pos_look_.y = y_pos; }

  /// <summary>
  /// 注視点の位置(Z座標)のセット
  /// </summary>
  /// <param name="z_pos"> Z座標 </param>
  /// <returns></returns>
  void SetLookAtPositionZ(float z_pos) { pos_look_.z = z_pos; }

  /// <summary>
  /// 注視点の位置を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> ベクトル </returns>
  VECTOR GetLookAtPosition() { return pos_look_; }

  /// <summary>
  /// カメラ水平角度のセット
  /// </summary>
  /// <param name="angle"> 角度 </param>
  /// <returns></returns>
  void SetHorizonAngle(float angle) { angle_horizon_ = angle; }

  /// <summary>
  /// カメラ水平角度を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> 角度 </returns>
  float GetHorizonAngle() { return angle_horizon_; }

  /// <summary>
  /// カメラ水平角度(初期値)のセット
  /// </summary>
  /// <param name="angle"> 角度 </param>
  /// <returns></returns>
  void SetInitialHorizonAngle(float angle) { angle_horizon_init_ = angle; }

  /// <summary>
  /// カメラ水平角度(初期値)を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> 角度 </returns>
  float GetInitialHorizonAngle() { return angle_horizon_init_; }

  /// <summary>
  /// カメラ垂直角度のセット
  /// </summary>
  /// <param name="angle"> 角度 </param>
  /// <returns></returns>
  void SetVerticalAngle(float angle) { angle_vertical_ = angle; }

  /// <summary>
  /// カメラ垂直角度を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> 角度 </returns>
  float GetVerticalAngle() { return angle_vertical_; }

  /// <summary>
  /// クリッピング距離のセット
  /// </summary>
  /// <param name=""> 手前クリップ距離 </param>
  /// <param name=""> 奥クリップ距離 </param>
  /// <returns></returns>
  void SetClippingDistance(float, float);

  /// <summary>
  /// カメラの位置変更
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void CalcPositionCamera();

  /// <summary>
  /// 注視点の変更
  /// </summary>
  /// <param name=""> 注視点のX方向移動量 </param>
  /// <param name=""> 注視点のY方向移動量 </param>
  /// <param name=""> 注視点のZ方向移動量 </param>
  /// <param name=""> 移動方向 </param>
  /// <returns></returns>
  void CalcLookAtPosition(float, float, float, Direction);

  /// <summary>
  /// カメラを注視点に近づける(拡大)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ExpandObject();

  /// <summary>
  /// カメラを注視点から遠ざける(縮小)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ShrinkObject();

  /// <summary>
  /// 注視点を中心に上方向へ移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void MoveUpCenteredOnGaze();

  /// <summary>
  /// 注視点を中心に下方向へ移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void MoveDownCenteredOnGaze();

  /// <summary>
  /// 注視点を中心に左方向へ移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void MoveLeftCenteredOnGaze();

  /// <summary>
  /// 注視点を中心に右方向へ移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void MoveRightCenteredOnGaze();


protected:

  /// <summary>
  /// カメラの位置
  /// </summary>
  VECTOR pos_;

  /// <summary>
  /// 注視点の位置
  /// </summary>
  VECTOR pos_look_;

  /// <summary>
  /// カメラの水平角度
  /// </summary>
  float angle_horizon_;

  /// <summary>
  /// カメラの垂直角度
  /// </summary>
  float angle_vertical_;

  /// <summary>
  /// カメラの水平角度 初期値
  /// </summary>
  float angle_horizon_init_;
};