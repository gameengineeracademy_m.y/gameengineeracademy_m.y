﻿#include "Game/CameraManager.h"

namespace {

  /// <summary>
  /// デバッグカメラ初期位置 X座標
  /// </summary>
  const float kDcamPosX = 1400.0f;

  /// <summary>
  /// デバッグカメラ初期位置 Y座標
  /// </summary>
  const float kDcamPosY = 1400.0f;

  /// <summary>
  /// デバッグカメラ初期位置 Z座標
  /// </summary>
  const float kDcamPosZ = 0.0f;

  /// <summary>
  /// 注視点 デバッグカメラ初期位置 X座標
  /// </summary>
  const float kDcamLookAtPosX = 0.0f;

  /// <summary>
  /// 注視点 デバッグカメラ初期位置 Y座標
  /// </summary>
  const float kDcamLookAtPosY = 0.0f;

  /// <summary>
  /// 注視点 デバッグカメラ初期位置 Z座標
  /// </summary>
  const float kDcamLookAtPosZ = 0.0f;

  /// <summary>
  /// プレイヤーカメラ初期位置 X座標
  /// </summary>
  const float kPcamPosX = 600.0f;

  /// <summary>
  /// プレイヤーカメラ初期位置 Y座標
  /// </summary>
  const float kPcamPosY = 138.0f;

  /// <summary>
  /// プレイヤーカメラ初期位置 Z座標
  /// </summary>
  const float kPcamPosZ = -705.0f;

  /// <summary>
  /// 注視点 プレイヤーカメラ初期位置 X座標
  /// </summary>
  const float kPcamLookAtPosX = 600.0f;

  /// <summary>
  /// 注視点 プレイヤーカメラ初期位置 Y座標
  /// </summary>
  const float kPcamLookAtPosY = 0.0f;

  /// <summary>
  /// 注視点 プレイヤーカメラ初期位置 Z座標
  /// </summary>
  const float kPcamLookAtPosZ = 1270.0f;

  /// <summary>
  /// カメラ移動量(1フレーム当たり)
  /// </summary>
  const float kCameraMoveValue = 5.0f;

  /// <summary>
  /// カメラ移動割合
  /// </summary>
  const float kCameraMoveRate = 0.1f;

  /// <summary>
  /// クリッピング距離 近距離
  /// </summary>
  const float kClipDistanceNearPlayerCam = 50.0f;

  /// <summary>
  /// クリッピング距離 遠距離
  /// </summary>
  const float kClipDistanceFarPlayerCam = 1500.0f;

  /// <summary>
  /// クリッピング距離 近距離
  /// </summary>
  const float kClipDistanceNearDebugCam = 50.0f;

  /// <summary>
  /// クリッピング距離 遠距離
  /// </summary>
  const float kClipDistanceFarDebugCam = 10000.0f;

  /// <summary>
  /// 累乗
  /// </summary>
  const float kSquare = 2.0f;

  /// <summary>
  /// 90°
  /// </summary>
  const float kAngle90 = 90.0f;

  /// <summary>
  /// 360°
  /// </summary>
  const float kAngle360 = 360.0f;

  /// <summary>
  /// 半分にする値
  /// </summary>
  const float kHalfValuef = 2.0f;

  /// <summary>
  /// ゼロ
  /// </summary>
  const float kZero = 0.0f;

  /// <summary>
  /// 符号変換
  /// </summary>
  const float kSignMinus = -1.0f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
CameraManager::CameraManager()
  : Task(TaskId::kCameraManager)
  , current_phase_(PhaseType::kInitialize)
  , camera_type_(CameraType::kDebug)
  , debug_camera_(nullptr)
  , player_camera_(nullptr){

  // コンソールに出力
  std::cout << "CameraManager コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
CameraManager::~CameraManager() {

  // コンソールに出力
  std::cout << "~CameraManager デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void CameraManager::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //------------------------------
    //初期化処理フェーズ
    //------------------------------
    Initialize();
    // 現在のフェーズを「初期化処理済み」に変更
    ChangeCurrentPhase(PhaseType::kInitialized);
    break;
  }
  case PhaseType::kInitialized: {
    //------------------------------
    //初期化処理済みフェーズ
    //------------------------------
    break;
  }
  case PhaseType::kPrepare: {
    //------------------------------
    //準備フェーズ
    //------------------------------
    Prepare();
    // 現在のフェーズを「処理中」に変更
    ChangeCurrentPhase(PhaseType::kProcess);
    break;
  }
  case PhaseType::kProcess: {
    //------------------------------
    //処理中フェーズ
    //------------------------------
    Process(process_time);
    break;
  }
  case PhaseType::kFinalize: {
    //------------------------------
    //終了処理フェーズ
    //------------------------------
    // 現在のフェーズを終了処理済みに変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CameraManager::Initialize() {

  // デバッグカメラ 初期設定値のセット
  InitializeDebugCamera();

  // プレイヤーカメラ 初期設定値のセット
  InitializePlayerCamera();
}

/// <summary>
/// デバッグカメラ初期化
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CameraManager::InitializeDebugCamera() {

  if (debug_camera_ == nullptr) {
    return;
  }

  // カメラ 位置
  debug_camera_->SetCameraPositionX(kDcamPosX);
  debug_camera_->SetCameraPositionY(kDcamPosY);
  debug_camera_->SetCameraPositionZ(kDcamPosZ);
  // 注視点 位置
  debug_camera_->SetLookAtPositionX(kDcamLookAtPosX);
  debug_camera_->SetLookAtPositionY(kDcamLookAtPosY);
  debug_camera_->SetLookAtPositionZ(kDcamLookAtPosZ);

  // クリッピング距離設定
  SetClippingDistance();
}


/// <summary>
/// プレイヤーカメラ初期化
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CameraManager::InitializePlayerCamera() {

  if (player_camera_ == nullptr) {
    return;
  }

  // カメラ 位置
  player_camera_->SetCameraPositionX(kPcamPosX);
  player_camera_->SetCameraPositionY(kPcamPosY);
  player_camera_->SetCameraPositionZ(kPcamPosZ);
  // 注視点 位置
  player_camera_->SetLookAtPositionX(kPcamLookAtPosX);
  player_camera_->SetLookAtPositionY(kPcamLookAtPosY);
  player_camera_->SetLookAtPositionZ(kPcamLookAtPosZ);

  // クリッピング距離設定
  SetClippingDistance();
}

/// <summary>
/// クリッピング距離設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CameraManager::SetClippingDistance() {

  switch (camera_type_) {
  case CameraType::kDebug: {
    //---------------------------------
    //デバッグカメラ
    //---------------------------------
    if (debug_camera_ == nullptr) {
      return;
    }
    // クリッピング距離設定
    debug_camera_->SetClippingDistance(kClipDistanceNearDebugCam, kClipDistanceFarDebugCam);

    break;
  }
  case CameraType::kPlayer: {
    //---------------------------------
    //プレイヤーカメラ
    //---------------------------------
    if (player_camera_ == nullptr) {
      return;
    }
    // クリッピング距離を設定
    player_camera_->SetClippingDistance(kClipDistanceNearPlayerCam, kClipDistanceFarPlayerCam);

    break;
  }
  }
}

/// <summary>
/// 準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CameraManager::Prepare() {

  switch (camera_type_) {
  case CameraType::kDebug: {
    //---------------------------------
    //デバッグカメラ
    //---------------------------------
    if (debug_camera_ == nullptr) {
      return;
    }
    // 角度計算
    CalcAngleDebugCamera();

    break;
  }
  case CameraType::kPlayer: {
    //---------------------------------
    //プレイヤーカメラ
    //---------------------------------
    if (player_camera_ == nullptr) {
      return;
    }
    // 角度計算
    CalcAnglePlayerCamera();

    break;
  }
  }
}

/// <summary>
/// 角度計算 デバッグカメラ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CameraManager::CalcAngleDebugCamera() {

  // デバッグカメラの位置情報を取得
  VECTOR pos_camera = debug_camera_->GetCameraPosition();
  VECTOR pos_look = debug_camera_->GetLookAtPosition();

  // X-Z平面の水平角度を算出
  float distance_x = pos_camera.x - pos_look.x;
  float distance_z = pos_camera.z - pos_look.z;
  float angle_horizon = std::atan(distance_z / distance_x);
  // ラジアン単位から度数単位に変換
  angle_horizon = angle_horizon * (kAngle360 / kHalfValuef) / DX_PI_F;
  if (angle_horizon < kZero) {
    angle_horizon += kAngle360;
  }
  // X-Y平面の垂直角度を度数単位で算出
  float distance = std::sqrtf(std::pow(distance_x, kSquare) + std::pow(distance_z, kSquare));
  if (distance_x < kZero) {
    distance *= kSignMinus;
  }
  if (distance_z < kZero) {
    distance *= kSignMinus;
  }

  float distance_y = std::abs(pos_camera.y - pos_look.y);
  float angle_vertical = std::atan(distance_y / distance);
  // ラジアン単位から度数単位に変換
  angle_vertical = angle_vertical * (kAngle360 / kHalfValuef) / DX_PI_F;
  if (angle_vertical < kZero) {
    angle_vertical += kAngle90;
  }
  // 角度
  debug_camera_->SetHorizonAngle(angle_horizon);
  debug_camera_->SetVerticalAngle(angle_vertical);
}

/// <summary>
/// 角度計算 プレイヤーカメラ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CameraManager::CalcAnglePlayerCamera() {

  // プレイヤーカメラの位置情報を取得
  VECTOR pos_camera = player_camera_->GetCameraPosition();
  VECTOR pos_look = player_camera_->GetLookAtPosition();

  // X-Z平面の水平角度を算出
  float distance_x = pos_camera.x - pos_look.x;
  float distance_z = pos_camera.z - pos_look.z;
  float angle_horizon = std::atan(distance_z / distance_x);
  // ラジアン単位から度数単位に変換
  angle_horizon = angle_horizon * (kAngle360 / kHalfValuef) / DX_PI_F;
  if (angle_horizon < kZero) {
    angle_horizon += kAngle360;
  }
  // X-Y平面の垂直角度を度数単位で算出
  float distance = std::sqrtf(std::pow(distance_x, kSquare) + std::pow(distance_z, kSquare));
  if (distance_x < kZero) {
    distance *= kSignMinus;
  }
  if (distance_z < kZero) {
    distance *= kSignMinus;
  }

  float distance_y = std::abs(pos_camera.y - pos_look.y);
  float angle_vertical = std::atan(distance_y / distance);
  // ラジアン単位から度数単位に変換
  angle_vertical = angle_vertical * (kAngle360 / kHalfValuef) / DX_PI_F;
  if (angle_vertical < kZero) {
    angle_vertical += kAngle90;
  }
  // 角度
  player_camera_->SetHorizonAngle(angle_horizon);
  player_camera_->SetVerticalAngle(angle_vertical);

  // プレイヤーとカメラの距離を計算し、セット
  float distance_player = std::sqrtf(std::powf(distance_x, kSquare) +
                                     std::powf(distance_y, kSquare) +
                                     std::powf(distance_z, kSquare));
  player_camera_->SetDistance(distance_player);
}

/// <summary>
/// 処理中処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void CameraManager::Process(float process_time) {

  switch (camera_type_) {
  case CameraType::kDebug: {
    //---------------------------------
    //デバッグカメラ
    //---------------------------------
    // カメラが未生成なら処理終了
    if (debug_camera_ == nullptr) {
      return;
    }
    debug_camera_->Update();

    break;
  }
  case CameraType::kPlayer: {
    //---------------------------------
    //プレイヤーカメラ
    //---------------------------------
    // カメラが未生成なら処理終了
    if (player_camera_ == nullptr) {
      return;
    }
    player_camera_->Update(process_time);

    break;
  }
  }
}


/// <summary>
/// カメラの生成
/// </summary>
/// <param name="camera_type"> カメラの種類 </param>
/// <returns></returns>
void CameraManager::CreateCamera(CameraType camera_type) {

  switch (camera_type) {
  case CameraType::kDebug: {
    //---------------------------------
    //デバッグカメラ
    //---------------------------------
    // プレイヤーカメラが存在するなら破棄
    if (player_camera_ != nullptr) {
      delete player_camera_;
      player_camera_ = nullptr;
    }

    // カメラが既に生成済みなら処理終了
    if (debug_camera_ != nullptr) {
      return;
    }
    // デバッグカメラ生成
    debug_camera_ = new DebugCamera();
    if (debug_camera_ == nullptr) {
      return;
    }

    // カメラの種類を切り変える
    ChangeCameraType(CameraType::kDebug);

    break;
  }
  case CameraType::kPlayer: {
    //---------------------------------
    //プレイヤーカメラ
    //---------------------------------
    // デバッグカメラが存在するなら破棄
    if (debug_camera_ != nullptr) {
      delete debug_camera_;
      debug_camera_ = nullptr;
    }

    // カメラが既に生成済みなら処理終了
    if (player_camera_ != nullptr) {
      return;
    }
    // プレイヤーカメラ生成
    player_camera_ = new PlayerCamera();
    if (player_camera_ == nullptr) {
      return;
    }

    // カメラの種類を切り変える
    ChangeCameraType(CameraType::kPlayer);

    break;
  }
  }
}

/// <summary>
/// カメラの破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CameraManager::DisposeCamera() {

  if (debug_camera_ != nullptr) {
    delete debug_camera_;
    debug_camera_ = nullptr;
  }

  if (player_camera_ != nullptr) {
    delete player_camera_;
    player_camera_ = nullptr;
  }
}