﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "Game/CameraControllerEventInterface.h"

/// <summary>
/// カメラコントローラ
/// </summary>
class CameraController : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kWait,               // 待機フェーズ
    kSwitch,             // 切り替えフェーズ
    kProcess,            // 処理中フェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

  /// <summary>
  /// キーの種類
  /// </summary>
  enum class KeyType {
    kMouseWheel,       // マウスホイール
    kLShift,           // 左Shiftキー
    kLAlt,             // 左ALTキー
    kUp,               // 上矢印キー
    kDown,             // 下矢印キー
    kRight,            // 右矢印キー
    kLeft,             // 左矢印キー
    kW,                // Wキー
    kS,                // Sキー
    kA,                // Aキー
    kD,                // Dキー
    kQ,                // Qキー
    kE,                // Eキー
    kKeyTypeMaxIndex   // キー項目数
  };

  /// <summary>
  /// キーの種類
  /// </summary>
  enum class KeyType2 {
    kEsc,              // Escキー
    kI,                // Iキー
    kP,                // Pキー
    kKeyTypeMaxIndex   // キー項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> カメラコントローライベントインターフェース </param>
  /// <returns></returns>
  CameraController(CameraControllerEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~CameraController();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 現在のフェーズの変更
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }


private:

  /// <summary>
  /// マウスホイール操作量取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> 回転量 </returns>
  int GetRotateMouseWheel();

  /// <summary>
  /// 左Shiftキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushShiftKey(float);

  /// <summary>
  /// 左ALTキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushAltKey(float);

  /// <summary>
  /// 上矢印キー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushUpKey(float);

  /// <summary>
  /// 下矢印キー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushDownKey(float);

  /// <summary>
  /// 右矢印キー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushRightKey(float);

  /// <summary>
  /// 左矢印キー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushLeftKey(float);

  /// <summary>
  /// Wキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushWKey(float);

  /// <summary>
  /// Sキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushSKey(float);

  /// <summary>
  /// Aキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushAKey(float);

  /// <summary>
  /// Dキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushDKey(float);

  /// <summary>
  /// Qキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushQKey(float);

  /// <summary>
  /// Eキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushEKey(float);

  /// <summary>
  /// Escキー押下処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushEscKey();

  /// <summary>
  /// Iキー押下処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushIKey();

  /// <summary>
  /// Pキー押下処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushPKey();

  /// <summary>
  /// 指定のキー押下有無を取得する
  /// </summary>
  /// <param name="key_push"> キーが押されているかどうか </param>
  /// <param name="key_push_before"> キーが押されていたかどうか </param>
  /// <param name="time_sec"> 毎フレームの処理時間 </param>
  /// <param name="push_time_sec"> キー押下継続時間 </param> 
  /// <returns> true：押された, false：押されていない </returns>
  bool CheckPressKey(bool, bool&, float, float&);

  /// <summary>
  /// 指定のキー押下有無を取得する　※単発押し
  /// </summary>
  /// <param name="key_code"> キーコード </param>
  /// <param name="push_key"> キーの押下の有無 </param>
  /// <returns>キーが押された：false、キーが押されていない：false</returns>
  bool CheckPushKey(int, bool&);


private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// カメラコントローライベントインターフェース
  /// </summary>
  CameraControllerEventInterface& event_interface_;

  /// <summary>
  /// ボタン長押し時間
  /// </summary>
  float key_press_keep_time_[static_cast<int>(KeyType::kKeyTypeMaxIndex)];

  /// <summary>
  /// キーの押下の有無
  /// </summary>
  bool push_key_[static_cast<int>(KeyType::kKeyTypeMaxIndex)];

  /// <summary>
  /// キーの押下の有無
  /// </summary>
  bool push_key2_[static_cast<int>(KeyType2::kKeyTypeMaxIndex)];
};