﻿#include "Game/BattleLevel.h"

namespace {

  /// <summary>
  /// 累積時間のリセット値
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 待機時間
  /// </summary>
  const float kWaitTime = 1.0f;

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjustInit = 5;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// ライン 始点
  /// </summary>
  const float kLineAxisDistance = 5000.0f;

  /// <summary>
  /// 距離ゼロ
  /// </summary>
  const float kZero = 0.0f;

  /// <summary>
  /// マイナス
  /// </summary>
  const float kSignMinus = -1.0f;

  /// <summary>
  /// ライン色
  /// </summary>
  const int kLineColorWhite = GetColor(255, 255, 255);

  /// <summary>
  /// ライン色
  /// </summary>
  const int kLineColorRed = GetColor(255, 0, 0);

  /// <summary>
  /// ライン色
  /// </summary>
  const int kLineColorGreen = GetColor(0, 255, 0);

  /// <summary>
  /// ライン色
  /// </summary>
  const int kLineColorBlue = GetColor(0, 0, 255);

  /// <summary>
  /// 背景色 R値
  /// </summary>
  const int kBackColorR = 25;

  /// <summary>
  /// 背景色 G値
  /// </summary>
  const int kBackColorG = 25;

  /// <summary>
  /// 背景色 B値
  /// </summary>
  const int kBackColorB = 25;

  /// <summary>
  /// カメラ名
  /// </summary>
  const char* kPlayerCamera = "◆ Player Camera ◆";

  /// <summary>
  /// カメラ名 デバッグカメラ
  /// </summary>
  const char* kDebugCamera = "★ Debug Camera ★";

  /// <summary>
  /// 10
  /// </summary>
  const float kTen = 10.0f;

  /// <summary>
  /// 1
  /// </summary>
  const float kOne = 1.0f;

  /// <summary>
  /// 3
  /// </summary>
  const float kThree = 3.0f;

  /// <summary>
  /// 自由落下位置
  /// </summary>
  const float kPlayerFreeFallPosY = 200.0f;

  /// <summary>
  /// カメラの高さ方向調整距離
  /// </summary>
  const float kAdjustDistance = 300.0f;

  /// <summary>
  /// 30°
  /// </summary>
  const float kAngle30 = 23.0f;

  /// <summary>
  /// 180°
  /// </summary>
  const float kAngle180 = 180.0f;

  /// <summary>
  /// 0.75倍
  /// </summary>
  const float kRate75 = 0.75f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> タスクID </param>
/// <param name=""> タスクマネージャ </param>
/// <returns></returns>
BattleLevel::BattleLevel(TaskId task_id, TaskManager& task_manager)
  : Level(task_id, task_manager)
  , current_phase_(PhaseType::kPrepare)
  , finalize_phase_(FinalizePhaseType::kUnloadTask)
  , field_manager_(nullptr)
  , player_(nullptr)
  , camera_manager_(nullptr)
  , camera_controller_(nullptr)
  , player_controller_(nullptr)
  , info_ui_(nullptr)
  , x_axis_()
  , y_axis_()
  , z_axis_()
  , origin_()
  , player_cam_data_()
  , font_handle_()
  , accumulate_time_(0.0f)
  , alpha_(0) {

  // コンソールに出力
  std::cout << "BattleLevel コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
BattleLevel::~BattleLevel() {

  // コンソールに出力
  std::cout << "~BattleLevel デストラクタ" << std::endl;
}

/// <summary>
/// 初期化処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool BattleLevel::InitializePhase(float process_time) {

  // 処理時間をリセットしておく
  accumulate_time_ = kResetTime;
  // 透過率 初期設定
  alpha_ = kAlphaMin;

  bool is_finish = false;

  // 画面上の初期設定
  InitializeScreen();

  // フィールドマネージャ 初期設定
  is_finish = InitializeFieldManager();
  if (!is_finish) {
    return false;
  }
  // プレイヤー 初期設定
  is_finish = InitializePlayer();
  if (!is_finish) {
    return false;
  }
  // カメラマネージャ 初期設定
  is_finish = InitializeCameraManager();
  if (!is_finish) {
    return false;
  }
  // プレイヤーコントローラ 初期設定
  is_finish = InitializePlayerController();
  if (!is_finish) {
    return false;
  }
  // カメラコントローラ 初期設定
  is_finish = InitializeCameraController();
  if (!is_finish) {
    return false;
  }
  // UI情報 初期設定
  is_finish = InitializeBattleInfoUi();
  if (!is_finish) {
    return false;
  }

  return true;
}

/// <summary>
/// 毎フレームの処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool BattleLevel::UpdatePhase(float process_time) {

  switch (current_phase_) {
  case PhaseType::kPrepare: {
    //-------------------------------
    //準備フェーズ
    //-------------------------------
    bool is_check = camera_manager_->IsInitialized();
    if (is_check) {
      // 準備処理
      Prepare();
      // フィールドマネージャのフェーズを「初期化」に変更
      field_manager_->ChangeCurrentPhase(FieldManager::PhaseType::kInitialize);
      // カメラマネージャのフェーズを「準備」に変更
      camera_manager_->ChangeCurrentPhase(CameraManager::PhaseType::kPrepare);
      // 現在のフェーズを「起動待機」に変更
      ChangeCurrentPhase(PhaseType::kStartWait);
    }
    break;
  }
  case PhaseType::kStartWait: {
    //-------------------------------
    //起動待機フェーズ
    //-------------------------------
    bool is_check_field = field_manager_->IsStartWait();
    bool is_check_ui = info_ui_->IsStartWait();
    bool is_check_player = player_->IsStartWait();
    if (is_check_field && is_check_ui && is_check_player) {
      // カメラ水平方向の初期角度取得
      GetCameraInitAngle();
      // 各クラス処理のフェーズを「起動」に変更
      field_manager_->ChangeCurrentPhase(FieldManager::PhaseType::kStartUp);
      info_ui_->ChangeCurrentPhase(BattleInfoUi::PhaseType::kStartUp);
      info_ui_->ChangeKeyCurrentPhase(Key::PhaseType::kProcess);
      player_->ChangeCurrentPhase(Player::PhaseType::kStartUp);
      // 現在のフェーズを「起動」に変更
      ChangeCurrentPhase(PhaseType::kStartUp);
    }
    break;
  }
  case PhaseType::kStartUp: {
    //-------------------------------
    //起動フェーズ
    //-------------------------------
    // フェードイン処理完了後、現在のフェーズを「処理中」に変更
    bool is_check = info_ui_->IsProcess();
    if (is_check) {
      // コントローラを使用可能にする
      player_controller_->ChangeCurrentPhase(PlayerController::PhaseType::kSwitch);
      camera_controller_->ChangeCurrentPhase(CameraController::PhaseType::kSwitch);
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //-------------------------------
    //処理中
    //-------------------------------
    break;
  }
  case PhaseType::kBeforeTransition: {
    //-------------------------------
    //遷移前フェーズ
    //-------------------------------
    // 各クラス処理のフェーズを「終了処理前」に変更
    field_manager_->ChangeCurrentPhase(FieldManager::PhaseType::kBeforeFinalize);
    info_ui_->ChangeCurrentPhase(BattleInfoUi::PhaseType::kBeforeFinalize);
    // 現在のフェーズを「レベル遷移」に変更する
    ChangeCurrentPhase(PhaseType::kLevelTransition);
    break;
  }
  case PhaseType::kLevelTransition: {
    //-------------------------------
    //レベル遷移フェーズ
    //-------------------------------
    // フェードアウト処理完了後、現在のフェーズを「終了処理」に変更
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更する
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //-------------------------------
    //終了処理フェーズ
    //-------------------------------
    // リザルトレベルへ切り替える
    ChangeLevel(TaskId::kResultLevel);
    // 現在のフェーズを「終了処理済み」に変更する
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }

  return false;
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::RenderPhase() {

  switch (current_phase_) {
  case PhaseType::kStartUp:
  case PhaseType::kProcess:
  case PhaseType::kBeforeTransition:
  case PhaseType::kLevelTransition: {

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, kAlphaMax);

    // X軸
    DrawLine3D(x_axis_.start, origin_, kLineColorWhite);
    DrawLine3D(origin_, x_axis_.end, kLineColorRed);
    // Y軸
    DrawLine3D(y_axis_.start, origin_, kLineColorWhite);
    DrawLine3D(origin_, y_axis_.end, kLineColorGreen);
    // Z軸
    DrawLine3D(z_axis_.start, origin_, kLineColorWhite);
    DrawLine3D(origin_, z_axis_.end, kLineColorBlue);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    break;
  }
  }

}

/// <summary>
/// 終了処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool BattleLevel::FinishPhase(float process_time) {

  switch (finalize_phase_) {
  case FinalizePhaseType::kUnloadTask: {
    //-------------------------------
    //タスクを降ろすフェーズ
    //-------------------------------
    if (field_manager_ != nullptr) {
      // フィールドマップの破棄
      field_manager_->DisposeFieldData();
      // フィールドリストの破棄
      field_manager_->DisposeFieldList();
    }
    if (camera_manager_ != nullptr) {
      // カメラの破棄
      camera_manager_->DisposeCamera();
    }
    if (player_ != nullptr) {
      // プレイヤーモデルの破棄
      MV1DeleteModel(player_->GetModelHandle());
    }
    if (info_ui_ != nullptr) {
      // キーの破棄
      info_ui_->DisposeKey();
    }

    TaskId task_id = TaskId::kDummyTask;
    Task* task = nullptr;

    // フィールドマネージャをタスクマネージャから降ろす
    if (field_manager_ != nullptr) {
      task_id = field_manager_->GetTaskId();
      task_manager_.ReleaseTask(task_id);
    }
    // プレイヤーをタスクマネージャから降ろす
    if (player_ != nullptr) {
      task_id = player_->GetTaskId();
      task_manager_.ReleaseTask(task_id);
    }
    // カメラマネージャをタスクマネージャから降ろす
    if (camera_manager_ != nullptr) {
      task_id = camera_manager_->GetTaskId();
      task_manager_.ReleaseTask(task_id);
    }
    // プレイヤーコントローラをタスクマネージャから降ろす
    if (player_controller_ != nullptr) {
      task_id = player_controller_->GetTaskId();
      task_manager_.ReleaseTask(task_id);
    }
    // カメラコントローラをタスクマネージャから降ろす
    if (camera_controller_ != nullptr) {
      task_id = camera_controller_->GetTaskId();
      task_manager_.ReleaseTask(task_id);
    }
    // UI情報をタスクマネージャから降ろす
    if (info_ui_ != nullptr) {
      task_id = info_ui_->GetTaskId();
      task_manager_.ReleaseTask(task_id);
    }

    // 終了フェーズをタスク破棄フェーズに変更
    ChangeFinalizePhase(FinalizePhaseType::kDisposeTask);

    return false;
  }
  case FinalizePhaseType::kDisposeTask: {
    //-------------------------------
    //タスクを破棄するフェーズ
    //-------------------------------
    if (info_ui_ != nullptr) {
      delete info_ui_;
      info_ui_ = nullptr;
    }
    if (camera_controller_ != nullptr) {
      delete camera_controller_;
      camera_controller_ = nullptr;
    }
    if (player_controller_ != nullptr) {
      delete player_controller_;
      player_controller_ = nullptr;
    }
    if (camera_manager_ != nullptr) {
      delete camera_manager_;
      camera_manager_ = nullptr;
    }
    if (player_ != nullptr) {
      delete player_;
      player_ = nullptr;
    }
    if (field_manager_ != nullptr) {
      delete field_manager_;
      field_manager_ = nullptr;
    }
    break;
  }
  }

  return true;
}

/// <summary>
/// 透過率変更処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率の調整
  switch (current_phase_) {
  case PhaseType::kStartUp: {
    // 起動フェーズ
    alpha_ += kAlphaAdjustInit;
    break;
  }
  case PhaseType::kLevelTransition: {
    // 終了処理前フェーズ
    alpha_ -= kAlphaAdjustInit;
    break;
  }
  }

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= kAlphaMax) {
    alpha_ = kAlphaMax;
    is_finish = true;
  }
  else if (alpha_ <= kAlphaMin) {
    alpha_ = kAlphaMin;
    is_finish = true;
  }

  return is_finish;
}

/// <summary>
/// 画面上の設定 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
void BattleLevel::InitializeScreen() {

  // X軸
  x_axis_.start.x = kSignMinus * kLineAxisDistance;
  x_axis_.start.y = kZero;
  x_axis_.start.z = kZero;
  x_axis_.end.x = kLineAxisDistance;
  x_axis_.end.y = kZero;
  x_axis_.end.z = kZero;

  // Y軸
  y_axis_.start.x = kZero;
  y_axis_.start.y = kSignMinus * kLineAxisDistance;
  y_axis_.start.z = kZero;
  y_axis_.end.x = kZero;
  y_axis_.end.y = kLineAxisDistance;
  y_axis_.end.z = kZero;

  // Z軸
  z_axis_.start.x = kZero;
  z_axis_.start.y = kZero;
  z_axis_.start.z = kSignMinus * kLineAxisDistance;
  z_axis_.end.x = kZero;
  z_axis_.end.y = kZero;
  z_axis_.end.z = kLineAxisDistance;

  // 原点
  origin_.x = kZero;
  origin_.y = kZero;
  origin_.z = kZero;

  // 背景の色を灰色にする
  SetBackgroundColor(kBackColorR, kBackColorG, kBackColorB);
}

/// <summary>
/// フィールドマネージャ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::InitializeFieldManager() {

  field_manager_ = new FieldManager();
  if (field_manager_ == nullptr) {
    std::cout << "BattleLevel InitializeFieldManager：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // フィールドマネージャをタスクマネージャに積む
  task_manager_.AddTask(field_manager_);

  return true;
}

/// <summary>
/// プレイヤー 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::InitializePlayer() {

  player_ = new Player(*this);
  if (player_ == nullptr) {
    std::cout << "BattleLevel InitializePlayer：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // プレイヤーをタスクマネージャに積む
  task_manager_.AddTask(player_);

  return true;
}


/// <summary>
/// カメラマネージャ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::InitializeCameraManager() {

  camera_manager_ = new CameraManager();
  if (camera_manager_ == nullptr) {
    std::cout << "BattleLevel InitializeCameraManager：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // カメラマネージャをタスクマネージャに積む
  task_manager_.AddTask(camera_manager_);

  // プレイヤーカメラの生成
  camera_manager_->CreateCamera(CameraManager::CameraType::kPlayer);

  return true;
}

/// <summary>
/// プレイヤーコントローラ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::InitializePlayerController() {

  player_controller_ = new PlayerController(*this);
  if (player_controller_ == nullptr) {
    std::cout << "BattleLevel InitializePlayerController：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // プレイヤーコントローラをタスクマネージャに積む
  task_manager_.AddTask(player_controller_);

  return true;
}

/// <summary>
/// カメラコントローラ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::InitializeCameraController() {

  camera_controller_ = new CameraController(*this);
  if (camera_controller_ == nullptr) {
    std::cout << "BattleLevel InitializeCameraController：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // カメラコントローラをタスクマネージャに積む
  task_manager_.AddTask(camera_controller_);

  return true;
}

/// <summary>
/// UI情報 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::InitializeBattleInfoUi() {

  info_ui_ = new BattleInfoUi(*this);
  if (info_ui_ == nullptr) {
    std::cout << "BattleLevel InitializeBattleInfoUi：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // UI情報をタスクマネージャに積む
  task_manager_.AddTask(info_ui_);

  return true;
}

/// <summary>
/// 準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::Prepare() {

  //-------------------------------------------
  //カメラ座標のセット
  //-------------------------------------------
  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {

    // 画面上のカメラ名をセット
    info_ui_->SetCameraName(kDebugCamera);
    // 操作説明文のセット
    info_ui_->SetDebugManual();
    info_ui_->ShowOperationManual();

    break;
  }
  case CameraManager::CameraType::kPlayer: {

    // 画面上のカメラ名をセット
    info_ui_->SetCameraName(kPlayerCamera);
    // 操作説明文のセット
    info_ui_->SetPlayerManual();
    info_ui_->ShowOperationManual();

    // プレイヤーの座標を取得
    VECTOR pos_player = player_->GetDispPosition();
    // カメラの注視点にプレイヤーの座標をセット
    PlayerCamera* player_camera = camera_manager_->GetPlayerCamera();
    if (player_camera == nullptr) {
      break;
    }

    player_camera->SetLookAtPositionX(pos_player.x);
    player_camera->SetLookAtPositionY(pos_player.y);
    player_camera->SetLookAtPositionZ(pos_player.z);

    break;
  }
  }

  // カメラの座標を取得
  SetCameraData();
}

/// <summary>
/// カメラの情報をセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::SetCameraData() {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {

    DebugCamera* debug_camera = camera_manager_->GetDebugCamera();
    if (debug_camera == nullptr) {
      break;
    }
    player_cam_data_.pos_camera = debug_camera->GetCameraPosition();
    player_cam_data_.pos_look = debug_camera->GetLookAtPosition();
    player_cam_data_.angle_h = debug_camera->GetHorizonAngle();
    player_cam_data_.angle_v = debug_camera->GetVerticalAngle();

    break;
  }
  case CameraManager::CameraType::kPlayer: {

    PlayerCamera* player_camera = camera_manager_->GetPlayerCamera();
    if (player_camera == nullptr) {
      break;
    }
    player_cam_data_.pos_camera = player_camera->GetCameraPosition();
    player_cam_data_.pos_look = player_camera->GetLookAtPosition();
    player_cam_data_.angle_h = player_camera->GetHorizonAngle();
    player_cam_data_.angle_v = player_camera->GetVerticalAngle();

    break;
  }
  }
}

/// <summary>
/// カメラ初期角度取得
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::GetCameraInitAngle() {

  PlayerCamera* player_cam = camera_manager_->GetPlayerCamera();
  if (player_cam == nullptr) {
    return;
  }
  float angle_h = player_cam->GetHorizonAngle();
  player_cam->SetInitialHorizonAngle(angle_h);

  // プレイヤーの向き(角度)をセット
  player_->SetAngle(angle_h);
}

/// <summary>
/// 小数第n位四捨五入
/// </summary>
/// <param name="num"> 対象数値 </param>
/// <param name="n"> 四捨五入する桁数 </param>
/// <returns> 四捨五入後の数値 </returns>
float BattleLevel::ExecRoundOffNumber(float num, float n) {

  float number = num * std::pow(kTen, n - kOne);
  number = std::roundf(number);
  number = number / std::pow(kTen, n - kOne);

  return number;
}

/// <summary>
/// カメラ座標取得
/// </summary>
/// <param name="x_pos"> X座標 </param>
/// <param name="y_pos"> Y座標 </param>
/// <param name="z_pos"> Z座標 </param>
/// <returns></returns>
void BattleLevel::OnGetCameraPosition(float& x_pos, float& y_pos, float& z_pos) {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {
    DebugCamera* debug_camera = camera_manager_->GetDebugCamera();
    if (debug_camera == nullptr) {
      break;
    }
    VECTOR pos_camera = debug_camera->GetCameraPosition();
    x_pos = pos_camera.x;
    y_pos = pos_camera.y;
    z_pos = pos_camera.z;
    break;
  }
  case CameraManager::CameraType::kPlayer: {
    PlayerCamera* player_camera = camera_manager_->GetPlayerCamera();
    if (player_camera == nullptr) {
      break;
    }
    VECTOR pos_camera = player_camera->GetCameraPosition();
    x_pos = pos_camera.x;
    y_pos = pos_camera.y;
    z_pos = pos_camera.z;
    break;
  }
  }
}

/// <summary>
/// 注視点座標取得
/// </summary>
/// <param name="x_pos"> X座標 </param>
/// <param name="y_pos"> Y座標 </param>
/// <param name="z_pos"> Z座標 </param>
/// <returns></returns>
void BattleLevel::OnGetLookAtPosition(float& x_pos, float& y_pos, float& z_pos) {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {
    DebugCamera* debug_camera = camera_manager_->GetDebugCamera();
    if (debug_camera == nullptr) {
      break;
    }
    VECTOR pos_look = debug_camera->GetLookAtPosition();
    x_pos = pos_look.x;
    y_pos = pos_look.y;
    z_pos = pos_look.z;
    break;
  }
  case CameraManager::CameraType::kPlayer: {
    PlayerCamera* player_camera = camera_manager_->GetPlayerCamera();
    if (player_camera == nullptr) {
      break;
    }
    VECTOR pos_look = player_camera->GetLookAtPosition();
    x_pos = pos_look.x;
    y_pos = pos_look.y;
    z_pos = pos_look.z;
    break;
  }
  }
}

/// <summary>
/// プレイヤー座標取得
/// </summary>
/// <param name=""> X座標 </param>
/// <param name=""> Y座標 </param>
/// <param name=""> Z座標 </param>
/// <returns></returns>
void BattleLevel::OnGetPlayerPosition(float& x_pos, float& y_pos, float& z_pos) {

  VECTOR pos_player = player_->GetDispPosition();
  x_pos = pos_player.x;
  y_pos = pos_player.y;
  z_pos = pos_player.z;
}

/// <summary>
/// 角度取得
/// </summary>
/// <param name="angle_h"> 水平角度 </param>
/// <param name="angle_v"> 垂直角度  </param>
/// <returns></returns>
void BattleLevel::OnGetAngle(float& angle_h, float& angle_v) {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {
    DebugCamera* debug_camera = camera_manager_->GetDebugCamera();
    if (debug_camera == nullptr) {
      break;
    }
    angle_h = debug_camera->GetHorizonAngle();
    angle_v = debug_camera->GetVerticalAngle();
    break;
  }
  case CameraManager::CameraType::kPlayer: {
    PlayerCamera* player_camera = camera_manager_->GetPlayerCamera();
    if (player_camera == nullptr) {
      break;
    }
    angle_h = player_camera->GetHorizonAngle();
    angle_v = player_camera->GetVerticalAngle();
    break;
  }
  }
}

/// <summary>
/// マウスホイールを奥に回す
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnRotateBackWheel() {

  //------------------------------------
  // カメラを注視点に近づける
  //------------------------------------
  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {
    DebugCamera* debug_camera = camera_manager_->GetDebugCamera();
    if (debug_camera == nullptr) {
      break;
    }
    debug_camera->ExpandObject();

    break;
  }
  case CameraManager::CameraType::kPlayer: {
    PlayerCamera* player_camera = camera_manager_->GetPlayerCamera();
    if (player_camera == nullptr) {
      break;
    }
    player_camera->ExpandObject();

    break;
  }
  }
}

/// <summary>
/// マウスホイールを手前に回す
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnRotateFrontWheel() {

  //------------------------------------
  // カメラを注視点から遠ざける
  //------------------------------------
  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {
    DebugCamera* debug_camera = camera_manager_->GetDebugCamera();
    if (debug_camera == nullptr) {
      break;
    }
    debug_camera->ShrinkObject();

    break;
  }
  case CameraManager::CameraType::kPlayer: {
    PlayerCamera* player_camera = camera_manager_->GetPlayerCamera();
    if (player_camera == nullptr) {
      break;
    }
    player_camera->ShrinkObject();

    break;
  }
  }
}

/// <summary>
/// Wキー押下 (プレイヤーコントローラ)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushPlayerControllerWKey() {

  // Wキーが押されていることにする
  info_ui_->SetPressKey(Key::KeyType::kW);

  //----------------------------------------------
  // プレイヤーを前方(カメラが向いている方向)に移動
  //----------------------------------------------
  PlayerCamera* player_cam = camera_manager_->GetPlayerCamera();
  if (player_cam == nullptr) {
    return;
  }
  // カメラの水平角度を取得
  float angle_h = player_cam->GetHorizonAngle();
  // プレイヤーを前進させる移動量を取得
  VECTOR move_value = player_->MoveFront(angle_h);

  //------------------------------------------
  //当たり判定の確認
  //------------------------------------------
  bool is_check = CheckCollision(move_value);
  if (is_check) {
    return;
  }

  //------------------------------------------
  //プレイヤー設定
  //------------------------------------------
  // プレイヤー座標に移動後の座標を加算
  player_->AddPosition(move_value);
  VECTOR pos_player_new = player_->GetDispPosition();
  player_->SetDispPosition(pos_player_new);

  //------------------------------------------
  //プレイヤーカメラ設定
  //------------------------------------------
  // プレイヤーカメラの位置を再セット
  VECTOR pos_cam = player_cam->GetCameraPosition();
  player_cam->SetCameraPositionX(pos_cam.x + move_value.x);
  player_cam->SetCameraPositionZ(pos_cam.z + move_value.z);
  // プレイヤーカメラの注視点をセット
  player_cam->SetLookAtPositionX(pos_player_new.x);
  player_cam->SetLookAtPositionZ(pos_player_new.z);
}

/// <summary>
/// Sキー押下 (プレイヤーコントローラ)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushPlayerControllerSKey() {

  // Sキーが押されていることにする
  info_ui_->SetPressKey(Key::KeyType::kS);

  //----------------------------------------------
  // プレイヤーを後方に移動
  //----------------------------------------------
  PlayerCamera* player_cam = camera_manager_->GetPlayerCamera();
  if (player_cam == nullptr) {
    return;
  }
  // カメラの水平角度を取得
  float angle_h = player_cam->GetHorizonAngle();
  // プレイヤーを前進させる移動量を取得
  VECTOR move_value = player_->MoveBack(angle_h);

  //------------------------------------------
  //当たり判定の確認
  //------------------------------------------
  bool is_check = CheckCollision(move_value);
  if (is_check) {
    return;
  }

  //------------------------------------------
  //プレイヤー設定
  //------------------------------------------
  // プレイヤー座標に移動後の座標を加算
  player_->AddPosition(move_value);
  VECTOR pos_player_new = player_->GetDispPosition();
  player_->SetDispPosition(pos_player_new);

  //------------------------------------------
  //プレイヤーカメラ設定
  //------------------------------------------
  // プレイヤーカメラの位置を再セット
  VECTOR pos_cam = player_cam->GetCameraPosition();
  player_cam->SetCameraPositionX(pos_cam.x + move_value.x);
  player_cam->SetCameraPositionZ(pos_cam.z + move_value.z);
  // プレイヤーカメラの注視点をセット
  player_cam->SetLookAtPositionX(pos_player_new.x);
  player_cam->SetLookAtPositionZ(pos_player_new.z);
}

/// <summary>
/// Aキー押下 (プレイヤーコントローラ)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushPlayerControllerAKey() {

  // Aキーが押されていることにする
  info_ui_->SetPressKey(Key::KeyType::kA);

  //----------------------------------------------
  // プレイヤーを左方向に移動
  //----------------------------------------------
  PlayerCamera* player_cam = camera_manager_->GetPlayerCamera();
  if (player_cam == nullptr) {
    return;
  }
  // カメラの水平角度を取得
  float angle_h = player_cam->GetHorizonAngle();
  // プレイヤーを左に移動させる移動量を取得
  VECTOR move_value = player_->MoveLeft(angle_h);

  //------------------------------------------
  //当たり判定の確認
  //------------------------------------------
  bool is_check = CheckCollision(move_value);
  if (is_check) {
    return;
  }

  //------------------------------------------
  //プレイヤー設定
  //------------------------------------------
  // プレイヤー座標に移動後の座標を加算
  player_->AddPosition(move_value);
  VECTOR pos_player_new = player_->GetDispPosition();
  player_->SetDispPosition(pos_player_new);

  //------------------------------------------
  //プレイヤーカメラ設定
  //------------------------------------------
  // プレイヤーカメラの位置を再セット
  VECTOR pos_cam = player_cam->GetCameraPosition();
  player_cam->SetCameraPositionX(pos_cam.x + move_value.x);
  player_cam->SetCameraPositionZ(pos_cam.z + move_value.z);
  // プレイヤーカメラの注視点をセット
  player_cam->SetLookAtPositionX(pos_player_new.x);
  player_cam->SetLookAtPositionZ(pos_player_new.z);
}

/// <summary>
/// Dキー押下 (プレイヤーコントローラ)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushPlayerControllerDKey() {

  // Dキーが押されていることにする
  info_ui_->SetPressKey(Key::KeyType::kD);

  //----------------------------------------------
  // プレイヤーを右方向に移動
  //----------------------------------------------
  PlayerCamera* player_cam = camera_manager_->GetPlayerCamera();
  if (player_cam == nullptr) {
    return;
  }
  // カメラの水平角度を取得
  float angle_h = player_cam->GetHorizonAngle();
  // プレイヤーを右に移動させる移動量を取得
  VECTOR move_value = player_->MoveRight(angle_h);

  //------------------------------------------
  //当たり判定の確認
  //------------------------------------------
  bool is_check = CheckCollision(move_value);
  if (is_check) {
    return;
  }

  //------------------------------------------
  //プレイヤー設定
  //------------------------------------------
  // プレイヤー座標に移動後の座標を加算
  player_->AddPosition(move_value);
  VECTOR pos_player_new = player_->GetDispPosition();
  player_->SetDispPosition(pos_player_new);

  //------------------------------------------
  //プレイヤーカメラ設定
  //------------------------------------------
  // プレイヤーカメラの位置を再セット
  VECTOR pos_cam = player_cam->GetCameraPosition();
  player_cam->SetCameraPositionX(pos_cam.x + move_value.x);
  player_cam->SetCameraPositionZ(pos_cam.z + move_value.z);
  // プレイヤーカメラの注視点をセット
  player_cam->SetLookAtPositionX(pos_player_new.x);
  player_cam->SetLookAtPositionZ(pos_player_new.z);
}

/// <summary>
/// W + Aキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushPlayerControllerWAKey() {

  // WキーとAキーが押されていることにする
  info_ui_->SetPressKey(Key::KeyType::kW);
  info_ui_->SetPressKey(Key::KeyType::kA);

  //----------------------------------------------
  // プレイヤーを左前方に移動
  //----------------------------------------------
  PlayerCamera* player_cam = camera_manager_->GetPlayerCamera();
  if (player_cam == nullptr) {
    return;
  }
  // カメラの水平角度を取得
  float angle_h = player_cam->GetHorizonAngle();
  // プレイヤーを左前方に移動させる移動量を取得
  VECTOR move_value = player_->MoveFrontLeft(angle_h);

  //------------------------------------------
  //当たり判定の確認
  //------------------------------------------
  bool is_check = CheckCollision(move_value);
  if (is_check) {
    return;
  }

  //------------------------------------------
  //プレイヤー設定
  //------------------------------------------
  // プレイヤー座標に移動後の座標を加算
  player_->AddPosition(move_value);
  VECTOR pos_player_new = player_->GetDispPosition();
  player_->SetDispPosition(pos_player_new);

  //------------------------------------------
  //プレイヤーカメラ設定
  //------------------------------------------
  // プレイヤーカメラの位置を再セット
  VECTOR pos_cam = player_cam->GetCameraPosition();
  player_cam->SetCameraPositionX(pos_cam.x + move_value.x);
  player_cam->SetCameraPositionZ(pos_cam.z + move_value.z);
  // プレイヤーカメラの注視点をセット
  player_cam->SetLookAtPositionX(pos_player_new.x);
  player_cam->SetLookAtPositionZ(pos_player_new.z);
}

/// <summary>
/// W + Dキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushPlayerControllerWDKey() {

  // WキーとDキーが押されていることにする
  info_ui_->SetPressKey(Key::KeyType::kW);
  info_ui_->SetPressKey(Key::KeyType::kD);

  //----------------------------------------------
  // プレイヤーを右前方に移動
  //----------------------------------------------
  PlayerCamera* player_cam = camera_manager_->GetPlayerCamera();
  if (player_cam == nullptr) {
    return;
  }
  // カメラの水平角度を取得
  float angle_h = player_cam->GetHorizonAngle();
  // プレイヤーを右前方に移動させる移動量を取得
  VECTOR move_value = player_->MoveFrontRight(angle_h);

  //------------------------------------------
  //当たり判定の確認
  //------------------------------------------
  bool is_check = CheckCollision(move_value);
  if (is_check) {
    return;
  }

  //------------------------------------------
  //プレイヤー設定
  //------------------------------------------
  // プレイヤー座標に移動後の座標を加算
  player_->AddPosition(move_value);
  VECTOR pos_player_new = player_->GetDispPosition();
  player_->SetDispPosition(pos_player_new);

  //------------------------------------------
  //プレイヤーカメラ設定
  //------------------------------------------
  // プレイヤーカメラの位置を再セット
  VECTOR pos_cam = player_cam->GetCameraPosition();
  player_cam->SetCameraPositionX(pos_cam.x + move_value.x);
  player_cam->SetCameraPositionZ(pos_cam.z + move_value.z);
  // プレイヤーカメラの注視点をセット
  player_cam->SetLookAtPositionX(pos_player_new.x);
  player_cam->SetLookAtPositionZ(pos_player_new.z);
}

/// <summary>
/// S + Aキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushPlayerControllerSAKey() {

  // SキーとAキーが押されていることにする
  info_ui_->SetPressKey(Key::KeyType::kS);
  info_ui_->SetPressKey(Key::KeyType::kA);

  //----------------------------------------------
  // プレイヤーを左後方に移動
  //----------------------------------------------
  PlayerCamera* player_cam = camera_manager_->GetPlayerCamera();
  if (player_cam == nullptr) {
    return;
  }
  // カメラの水平角度を取得
  float angle_h = player_cam->GetHorizonAngle();
  // プレイヤーを左後方に移動させる移動量を取得
  VECTOR move_value = player_->MoveBackLeft(angle_h);

  //------------------------------------------
  //当たり判定の確認
  //------------------------------------------
  bool is_check = CheckCollision(move_value);
  if (is_check) {
    return;
  }

  //------------------------------------------
  //プレイヤー設定
  //------------------------------------------
  // プレイヤー座標に移動後の座標を加算
  player_->AddPosition(move_value);
  VECTOR pos_player_new = player_->GetDispPosition();
  player_->SetDispPosition(pos_player_new);

  //------------------------------------------
  //プレイヤーカメラ設定
  //------------------------------------------
  // プレイヤーカメラの位置を再セット
  VECTOR pos_cam = player_cam->GetCameraPosition();
  player_cam->SetCameraPositionX(pos_cam.x + move_value.x);
  player_cam->SetCameraPositionZ(pos_cam.z + move_value.z);
  // プレイヤーカメラの注視点をセット
  player_cam->SetLookAtPositionX(pos_player_new.x);
  player_cam->SetLookAtPositionZ(pos_player_new.z);
}

/// <summary>
/// S + Dキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushPlayerControllerSDKey() {

  // SキーとDキーが押されていることにする
  info_ui_->SetPressKey(Key::KeyType::kS);
  info_ui_->SetPressKey(Key::KeyType::kD);

  //----------------------------------------------
  // プレイヤーを右後方に移動
  //----------------------------------------------
  PlayerCamera* player_cam = camera_manager_->GetPlayerCamera();
  if (player_cam == nullptr) {
    return;
  }
  // カメラの水平角度を取得
  float angle_h = player_cam->GetHorizonAngle();
  // プレイヤーを右後方に移動させる移動量を取得
  VECTOR move_value = player_->MoveBackRight(angle_h);

  //------------------------------------------
  //当たり判定の確認
  //------------------------------------------
  bool is_check = CheckCollision(move_value);
  if (is_check) {
    return;
  }

  //------------------------------------------
  //プレイヤー設定
  //------------------------------------------
  // プレイヤー座標に移動後の座標を加算
  player_->AddPosition(move_value);
  VECTOR pos_player_new = player_->GetDispPosition();
  player_->SetDispPosition(pos_player_new);

  //------------------------------------------
  //プレイヤーカメラ設定
  //------------------------------------------
  // プレイヤーカメラの位置を再セット
  VECTOR pos_cam = player_cam->GetCameraPosition();
  player_cam->SetCameraPositionX(pos_cam.x + move_value.x);
  player_cam->SetCameraPositionZ(pos_cam.z + move_value.z);
  // プレイヤーカメラの注視点をセット
  player_cam->SetLookAtPositionX(pos_player_new.x);
  player_cam->SetLookAtPositionZ(pos_player_new.z);
}

/// <summary>
/// 当たり判定確認処理
/// </summary>
/// <param name=""> 移動量 </param>
/// <returns> true:当たった, false:当たっていない </returns>
bool BattleLevel::CheckCollision(VECTOR& move_value) {

  // プレイヤーの移動後の座標の算出
  // プレイヤーのサイズ、角度を取得
  VECTOR pos_player = player_->GetDispPosition();
  VECTOR pos_player_new = VAdd(pos_player, move_value);
  float width = player_->GetModelWidth();
  float depth = player_->GetModelDepth();
  float angle = player_->GetModelAngle();
  VECTOR pos_col_wall = VGet(kZero, kZero, kZero);

  // 壁との当たり判定をチェック
  bool is_collided = field_manager_->CheckCollisionWall(pos_player_new, width, depth, angle, pos_col_wall);
  // 壁にぶつかっているなら元の座標をセットし、処理終了
  if (is_collided) {
    // 接触面ベクトル法線ベクトルを算出
    VECTOR collision_wall_norm = VNorm(pos_col_wall);
    collision_wall_norm = VTransform(collision_wall_norm, MGetRotY(DX_PI_F / kHalfValue));
    // 壁の法線ベクトルと移動量ベクトルとの外積を計算
    move_value = VCross(move_value, collision_wall_norm);
    // 上記で求めたベクトルと壁の法線ベクトルとの外積によって得られる
    // ベクトルが元の移動成分から壁方向の移動成分を抜いたベクトル
    move_value = VCross(collision_wall_norm, move_value);
    // 小数第3位で四捨五入する
    move_value.x = ExecRoundOffNumber(move_value.x, kThree);
    move_value.y = ExecRoundOffNumber(move_value.y, kThree);
    move_value.z = ExecRoundOffNumber(move_value.z, kThree);

    pos_col_wall = VGet(kZero, kZero, kZero);
    // 再度移動前(現在)のプレイヤーの座標に移動量調整値を加算
    pos_player_new = VAdd(pos_player, move_value);
    is_collided = field_manager_->CheckCollisionWall(pos_player_new, width, depth, angle, pos_col_wall);
    if (is_collided) {
      return true;
    }
  }

  return false;
}

/// <summary>
/// Spaceキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushPlayerControllerSpaceKey() {

  // Spaceキーが押されていることにする
  info_ui_->SetPressKey(Key::KeyType::kSpace);

  bool is_jump = player_->MoveJump();
  if (!is_jump) {
    return;
  }
  // プレイヤーカメラの情報を取得
  SetCameraData();
}

/// <summary>
/// Escキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushEscapeKey() {

  // 操作説明文が表示中かどうか
  bool is_show = info_ui_->IsShowOperationManual();
  if (is_show) {
    // 操作説明文を非表示にする
    info_ui_->HideOperationManual();
  }
  else {
    // 操作説明文を表示する
    info_ui_->ShowOperationManual();
  }
}

/// <summary>
/// Pキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushPlayerControllerPKey() {

  // プレイヤーの表示位置を初期位置に戻し、取得する
  player_->InitializeData();
  VECTOR pos_player = player_->GetDispPosition();
  // プレイヤーを自由落下させる
  pos_player.y = kPlayerFreeFallPosY;
  player_->SetDispPosition(pos_player);

  // カメラを初期位置に戻す
  camera_manager_->InitializePlayerCamera();
  PlayerCamera* player_camera = camera_manager_->GetPlayerCamera();
  if (player_camera == nullptr) {
    return;
  }
  player_camera->SetLookAtPositionX(pos_player.x);
  player_camera->SetLookAtPositionY(pos_player.y);
  player_camera->SetLookAtPositionZ(pos_player.z);

  camera_manager_->CalcAnglePlayerCamera();

  // カメラの向きを取得し、プレイヤーの向きをセット
  float angle_h = player_camera->GetHorizonAngle();
  player_->SetAngle(angle_h);
}

/// <summary>
/// Mキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushPlayerControllerMKey() {

  // キー操作メニューが表示中かどうか
  bool is_show = info_ui_->IsShowDisplay();
  if (is_show) {
    // メニューを非表示にする
    info_ui_->HideDisplayKeyMenu();
  }
  else {
    // メニューを表示する
    info_ui_->ShowDisplayKeyMenu();
  }
}

/// <summary>
/// Nキー押下   ※デバッグ専用機能
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushPlayerControllerNKey() {

  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  bool is_debug = game_info->IsDebug();
  if (!is_debug) {
    return;
  }

  // プレイヤーを自由落下させる
  VECTOR pos_player = player_->GetDispPosition();
  pos_player.y = kPlayerFreeFallPosY;
  player_->SetDispPosition(pos_player);
}

/// <summary>
/// 上矢印キー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushUpKey() {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kPlayer: {

    // 上矢印キーが押されていることにする
    info_ui_->SetPressKey(Key::KeyType::kUp);

    PlayerCamera* player_camera = camera_manager_->GetPlayerCamera();
    if (player_camera == nullptr) {
      break;
    }
    //------------------------------------
    // カメラを上方向へ移動
    //------------------------------------
    player_camera->MoveUpCenteredOnGaze();

    break;
  }
  }
}

/// <summary>
/// 下矢印キー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushDownKey() {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kPlayer: {

    // 下矢印キーが押されていることにする
    info_ui_->SetPressKey(Key::KeyType::kDown);

    PlayerCamera* player_camera = camera_manager_->GetPlayerCamera();
    if (player_camera == nullptr) {
      break;
    }
    //------------------------------------
    // カメラを下方向へ移動
    //------------------------------------
    player_camera->MoveDownCenteredOnGaze();

    break;
  }
  }
}

/// <summary>
/// 右矢印キー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushRightKey() {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kPlayer: {

    // 右矢印キーが押されていることにする
    info_ui_->SetPressKey(Key::KeyType::kRight);

    PlayerCamera* player_camera = camera_manager_->GetPlayerCamera();
    if (player_camera == nullptr) {
      break;
    }
    //------------------------------------
    // カメラを右方向へ移動
    //------------------------------------
    player_camera->MoveRightCenteredOnGaze();

    break;
  }
  }
}

/// <summary>
/// 左矢印キー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushLeftKey() {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kPlayer: {

    // 左矢印キーが押されていることにする
    info_ui_->SetPressKey(Key::KeyType::kLeft);

    PlayerCamera* player_camera = camera_manager_->GetPlayerCamera();
    if (player_camera == nullptr) {
      break;
    }
    //------------------------------------
    // カメラを左方向へ移動
    //------------------------------------
    player_camera->MoveLeftCenteredOnGaze();

    break;
  }
  }
}

/// <summary>
/// Wキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushWKey() {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {
    DebugCamera* debug_camera = camera_manager_->GetDebugCamera();
    if (debug_camera == nullptr) {
      break;
    }
    //------------------------------------
    // 注視点を前進させる
    //------------------------------------
    debug_camera->MoveFrontGaze();

    break;
  }
  }
}

/// <summary>
/// Sキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushSKey() {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {
    DebugCamera* debug_camera = camera_manager_->GetDebugCamera();
    if (debug_camera == nullptr) {
      break;
    }
    //------------------------------------
    // 注視点を後退させる
    //------------------------------------
    debug_camera->MoveBackGaze();

    break;
  }
  }
}

/// <summary>
/// Aキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushAKey() {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {
    DebugCamera* debug_camera = camera_manager_->GetDebugCamera();
    if (debug_camera == nullptr) {
      break;
    }
    //------------------------------------
    // 注視点を左方向へ移動
    //------------------------------------
    debug_camera->MoveLeftGaze();

    break;
  }
  }
}

/// <summary>
/// Dキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushDKey() {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {
    DebugCamera* debug_camera = camera_manager_->GetDebugCamera();
    if (debug_camera == nullptr) {
      break;
    }
    //------------------------------------
    // 注視点を右方向へ移動
    //------------------------------------
    debug_camera->MoveRightGaze();

    break;
  }
  }
}

/// <summary>
/// Shift + Wキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushShiftWKey() {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {
    DebugCamera* debug_camera = camera_manager_->GetDebugCamera();
    if (debug_camera == nullptr) {
      break;
    }
    //------------------------------------
    // カメラを上方向へ移動
    //------------------------------------
    debug_camera->MoveUpCenteredOnGaze();

    break;
  }
  }
}

/// <summary>
/// Shift + Sキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushShiftSKey() {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {
    DebugCamera* debug_camera = camera_manager_->GetDebugCamera();
    if (debug_camera == nullptr) {
      break;
    }
    //------------------------------------
    // カメラを下方向へ移動
    //------------------------------------
    debug_camera->MoveDownCenteredOnGaze();

    break;
  }
  }
}

/// <summary>
/// Shift + Aキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushShiftAKey() {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {
    DebugCamera* debug_camera = camera_manager_->GetDebugCamera();
    if (debug_camera == nullptr) {
      break;
    }
    //------------------------------------
    // カメラを左方向へ移動
    //------------------------------------
    debug_camera->MoveLeftCenteredOnGaze();

    break;
  }
  }
}

/// <summary>
/// Shift + Dキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushShiftDKey() {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {
    DebugCamera* debug_camera = camera_manager_->GetDebugCamera();
    if (debug_camera == nullptr) {
      break;
    }
    //------------------------------------
    // カメラを右方向へ移動
    //------------------------------------
    debug_camera->MoveRightCenteredOnGaze();

    break;
  }
  }
}

/// <summary>
/// Shift + Qキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushShiftQKey() {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {
    DebugCamera* debug_camera = camera_manager_->GetDebugCamera();
    if (debug_camera == nullptr) {
      break;
    }
    //------------------------------------
    // 注視点を上方向へ移動
    //------------------------------------
    debug_camera->MoveUpGaze();

    break;
  }
  }
}

/// <summary>
/// Shift + Eキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushShiftEKey() {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {
    DebugCamera* debug_camera = camera_manager_->GetDebugCamera();
    if (debug_camera == nullptr) {
      break;
    }
    //------------------------------------
    // 注視点を下方向へ移動
    //------------------------------------
    debug_camera->MoveDownGaze();

    break;
  }
  }
}

/// <summary>
/// Alt + Dキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushAltDKey() {

  //------------------------------------
  // カメラの種類の切り替え
  //------------------------------------
  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {

    // プレイヤーコントローラをタスクマネージャに乗せる
    task_manager_.AddTask(player_controller_);
    player_controller_->ChangeCurrentPhase(PlayerController::PhaseType::kSwitch);

    // コントローラのフェーズを「切り替え」に変更
    player_controller_->ChangeCurrentPhase(PlayerController::PhaseType::kSwitch);
    camera_controller_->ChangeCurrentPhase(CameraController::PhaseType::kSwitch);

    // カメラ名の変更、デバッグ時のフレーム非表示設定
    info_ui_->SetCameraName(kPlayerCamera);
    info_ui_->HideDisplayWindowFrame();
    // 操作説明の切り替え
    info_ui_->SetPlayerManual();

    //-------------------------------------------------------
    // プレイヤーカメラに元情報をセット
    //-------------------------------------------------------
    // プレイヤーカメラの生成
    camera_manager_->CreateCamera(CameraManager::CameraType::kPlayer);
    // プレイヤーカメラの元の座標をセット
    PlayerCamera* player_camera = camera_manager_->GetPlayerCamera();
    if (player_camera == nullptr) {
      break;
    }
    // カメラ位置
    player_camera->SetCameraPositionX(player_cam_data_.pos_camera.x);
    player_camera->SetCameraPositionY(player_cam_data_.pos_camera.y);
    player_camera->SetCameraPositionZ(player_cam_data_.pos_camera.z);
    // 注視点位置
    player_camera->SetLookAtPositionX(player_cam_data_.pos_look.x);
    player_camera->SetLookAtPositionY(player_cam_data_.pos_look.y);
    player_camera->SetLookAtPositionZ(player_cam_data_.pos_look.z);
    // カメラ角度
    player_camera->SetHorizonAngle(player_cam_data_.angle_h);
    player_camera->SetVerticalAngle(player_cam_data_.angle_v);
    // クリッピング距離設定
    camera_manager_->SetClippingDistance();

    // カメラマネージャのフェーズを準備フェーズに変更
    camera_manager_->ChangeCurrentPhase(CameraManager::PhaseType::kProcess);

    break;
  }
  case CameraManager::CameraType::kPlayer: {

    // キー入力判定メニューが表示中の場合のみ非表示にする
    bool is_show = info_ui_->IsShowDisplay();
    if (is_show) {
      info_ui_->HideDisplayKeyMenu();
    }

    // プレイヤーコントローラをタスクマネージャから降ろす
    TaskId task_id = player_controller_->GetTaskId();
    task_manager_.ReleaseTask(task_id);

    // コントローラのフェーズを「切り替え」に変更
    player_controller_->ChangeCurrentPhase(PlayerController::PhaseType::kSwitch);
    camera_controller_->ChangeCurrentPhase(CameraController::PhaseType::kSwitch);

    // カメラ名の変更、デバッグ時のフレーム表示設定
    info_ui_->SetCameraName(kDebugCamera);
    info_ui_->ShowDisplayWindowFrame();
    // 操作説明の切り替え
    info_ui_->SetDebugManual();

    //-------------------------------------------------------
    // プレイヤーカメラの表示位置をデバッグカメラに渡す
    //-------------------------------------------------------
    // カメラの情報を取得
    SetCameraData();

    // デバッグカメラの生成
    camera_manager_->CreateCamera(CameraManager::CameraType::kDebug);
    DebugCamera* debug_camera = camera_manager_->GetDebugCamera();
    if (debug_camera == nullptr) {
      break;
    }
    // カメラ位置
    debug_camera->SetCameraPositionX(player_cam_data_.pos_camera.x);
    debug_camera->SetCameraPositionY(player_cam_data_.pos_camera.y);
    debug_camera->SetCameraPositionZ(player_cam_data_.pos_camera.z);
    // 注視点位置
    debug_camera->SetLookAtPositionX(player_cam_data_.pos_look.x);
    debug_camera->SetLookAtPositionY(player_cam_data_.pos_look.y);
    debug_camera->SetLookAtPositionZ(player_cam_data_.pos_look.z);
    // カメラ角度
    debug_camera->SetHorizonAngle(player_cam_data_.angle_h);
    debug_camera->SetVerticalAngle(player_cam_data_.angle_v);
    // クリッピング距離設定
    camera_manager_->SetClippingDistance();

    // カメラマネージャのフェーズを準備フェーズに変更
    camera_manager_->ChangeCurrentPhase(CameraManager::PhaseType::kProcess);

    break;
  }
  }
}

/// <summary>
/// Iキー押下   ※デバッグ専用
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushIKey() {

  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  bool is_debug = game_info->IsDebug();
  if (!is_debug) {
    return;
  }

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {

    // 現在使用しているカメラを外部視点に移動する
    camera_manager_->InitializeDebugCamera();
    camera_manager_->CalcAngleDebugCamera();

    break;
  }
  }
}

/// <summary>
/// Pキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushPKey() {

  // カメラの種類を取得
  CameraManager::CameraType camera_type = camera_manager_->GetCameraType();
  switch (camera_type) {
  case CameraManager::CameraType::kDebug: {

    DebugCamera* debug_camera = camera_manager_->GetDebugCamera();
    if (debug_camera == nullptr) {
      break;
    }
    // 現在使用しているカメラをプレイヤー位置に戻す
    // カメラ位置
    debug_camera->SetCameraPositionX(player_cam_data_.pos_camera.x);
    debug_camera->SetCameraPositionY(player_cam_data_.pos_camera.y);
    debug_camera->SetCameraPositionZ(player_cam_data_.pos_camera.z);
    // 注視点位置
    debug_camera->SetLookAtPositionX(player_cam_data_.pos_look.x);
    debug_camera->SetLookAtPositionY(player_cam_data_.pos_look.y);
    debug_camera->SetLookAtPositionZ(player_cam_data_.pos_look.z);

    break;
  }
  }
}

/// <summary>
/// 床との接触判定処理
/// </summary>
/// <param name=""> 地面の高さ座標 </param>
/// <returns> true:接地している, false:接地していない </returns>
bool BattleLevel::OnCollisionFloor(float& y_pos_floor) {

  // プレイヤーのY座標とプレイヤーの高さを取得
  VECTOR pos_player = player_->GetDispPosition();
  float player_height = player_->GetModelHeight();
  // プレイヤーの足裏座標を算出
  pos_player.y = pos_player.y - player_height;
  // 足裏座標が地面と接地しているかどうか
  bool is_check = field_manager_->CheckCollisionFloor(pos_player, y_pos_floor);

  return is_check;
}

/// <summary>
/// カメラの注視点移動処理
/// </summary>
/// <param name="y_pos_player_new"> プレイヤー移動後位置 </param>
/// <returns></returns>
void BattleLevel::OnMoveCameraLookAtPos(float y_pos_player_new) {

  // プレイヤーの頂点座標を取得
  VECTOR pos_player = player_->GetDispPosition();
  float player_height = player_->GetModelHeight();
  float y_pos_player_top = pos_player.y + player_height / kHalfValue;
  // 移動量を算出
  float move_value = y_pos_player_new - pos_player.y;

  PlayerCamera* player_camera = camera_manager_->GetPlayerCamera();
  if (player_camera == nullptr) {
    return;
  }
  // カメラの表示位置と注視点を取得
  VECTOR y_pos_cam = player_camera->GetCameraPosition();
  VECTOR y_pos_look = player_camera->GetLookAtPosition();
  VECTOR y_pos_cam_diff = VSub(y_pos_cam, y_pos_look);
  float distance_3D = VSize(y_pos_cam_diff);

  std::cout << "カメラと注視点の距離 : " << distance_3D << std::endl;

  // 指定距離以上離れている場合は以降の処理は不要
  if (distance_3D > kAdjustDistance) {
    //player_camera->SetCameraPositionY(player_cam_data_.pos_camera.y);
    player_camera->SetLookAtPositionY(player_cam_data_.pos_look.y);
    return;
  }

  // 画面上端の投影位置算出
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  float window_width = static_cast<float>(game_info->GetResolutionWidth());
  float window_height = static_cast<float>(game_info->GetResolutionHeight());
  VECTOR y_pos_cam_diff_xz = y_pos_cam_diff;
  y_pos_cam_diff_xz.y = kZero;
  float distance_xz = VSize(y_pos_cam_diff_xz);
  // カメラの水平視野角を取得し、アスペクト比から垂直視野角を算出
  float angle_h = GetCameraFov();
  float angle_v = angle_h * window_height / window_width;
  float y_pos_cam_top = distance_xz * std::tanf(angle_v / kHalfValue) + player_cam_data_.pos_look.y;

  std::cout << "水平視野角     : " << angle_h / DX_PI_F * kAngle180 << std::endl;
  std::cout << "垂直視野角     : " << angle_v / DX_PI_F * kAngle180 << std::endl;
  std::cout << "注視点         : " << player_cam_data_.pos_look.y << std::endl;
  std::cout << "カメラ頂点     : " << y_pos_cam_top << std::endl;
  std::cout << "カメラ頂点(75%): " << y_pos_cam_top * kRate75 << std::endl;
  std::cout << "プレイヤー頂点 : " << y_pos_player_top << std::endl;

  // プレイヤーの頂点が画面上端の3/4の位置を超えたら
  // カメラがプレイヤーを追う
  if (y_pos_player_top < y_pos_cam_top * kRate75) {
    if (move_value < kZero) {
      //player_camera->SetCameraPositionY(player_cam_data_.pos_camera.y);
      player_camera->SetLookAtPositionY(player_cam_data_.pos_look.y);
    }
    return;
  }

  std::cout << "移動量         : " << move_value << std::endl;

  //player_camera->SetCameraPositionY(y_pos_cam.y + move_value);
  player_camera->SetLookAtPositionY(y_pos_look.y + move_value);
}

/// <summary>
/// カメラの注視点をプレイヤーに戻す
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnResetCameraLookAtPos() {

  PlayerCamera* player_camera = camera_manager_->GetPlayerCamera();
  if (player_camera == nullptr) {
    return;
  }
  // プレイヤー位置をカメラの注視点にセット
  VECTOR pos_player = player_->GetDispPosition();
  player_camera->SetLookAtPositionY(pos_player.y);
}