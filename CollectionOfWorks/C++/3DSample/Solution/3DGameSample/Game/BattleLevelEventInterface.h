﻿#pragma once

#include "Game/CameraControllerEventInterface.h"
#include "Game/PlayerControllerEventInterface.h"
#include "Game/BattleInfoUiEventInterface.h"
#include "Game/PlayerEventInterface.h"
#include <iostream>

/// <summary>
/// バトルレベルイベントインターフェース
/// </summary>
class BattleLevelEventInterface : public CameraControllerEventInterface,
                                  public PlayerControllerEventInterface,
                                  public BattleInfoUiEventInterface,
                                  public PlayerEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  BattleLevelEventInterface() {

    // コンソールに出力
    std::cout << "BattleLevelEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~BattleLevelEventInterface() {

    // コンソールに出力
    std::cout << "~BattleLevelEventInterface デストラクタ" << std::endl;
  }


private:


};