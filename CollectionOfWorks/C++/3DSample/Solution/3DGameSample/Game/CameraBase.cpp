﻿#include "Game/CameraBase.h"

namespace {

  /// <summary>
  /// 距離ゼロ
  /// </summary>
  const float kZero = 0.0f;

  /// <summary>
  /// マイナス
  /// </summary>
  const float kSignMinus = -1.0f;

  /// <summary>
  /// 累乗
  /// </summary>
  const float kSquare = 2.0f;

  /// <summary>
  /// 半分にする値
  /// </summary>
  const float kHalfValuef = 2.0f;

  /// <summary>
  /// カメラ角度移動量
  /// </summary>
  const float kCameraAngleMove = 1.0f;

  /// <summary>
  /// カメラ移動割合 増加量
  /// </summary>
  const float kCameraMoveRate = 0.05f;

  /// <summary>
  /// プレイヤーと注視点距離 最低値
  /// </summary>
  const float kDistanceMin = 50.0f;

  /// <summary>
  /// プレイヤーと注視点距離 最大値
  /// </summary>
  const float kDistanceMax = 2000.0f;

  /// <summary>
  /// 大きさ「1」
  /// </summary>
  const float kScaleY = 1.0f;

  /// <summary>
  /// 0°
  /// </summary>
  const float kAngle0 = 0.0f;

  /// <summary>
  /// 1°
  /// </summary>
  const float kAngle1 = 1.0f;

  /// <summary>
  /// 90°
  /// </summary>
  const float kAngle90 = 90.0f;

  /// <summary>
  /// 179°
  /// </summary>
  const float kAngle179 = 179.0f;

  /// <summary>
  /// 180°
  /// </summary>
  const float kAngle180 = 180.0f;

  /// <summary>
  /// 360°
  /// </summary>
  const float kAngle360 = 360.0f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
CameraBase::CameraBase()
  : pos_()
  , pos_look_()
  , angle_horizon_(0.0f)
  , angle_vertical_(0.0f)
  , angle_horizon_init_(0.0f) {

  // コンソールに出力
  std::cout << "CameraBase コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
CameraBase::~CameraBase() {

  // コンソールに出力
  std::cout << "~CameraBase デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CameraBase::Update() {

  //----------------------------------------------
  // カメラ位置と注視点のセット
  //----------------------------------------------
  SetCameraPositionAndTarget_UpVecY(pos_, pos_look_);
}

/// <summary>
/// クリッピング距離のセット
/// </summary>
/// <param name="clip_near"> 手前クリップ距離 </param>
/// <param name="clip_far"> 奥クリップ距離 </param>
/// <returns></returns>
void CameraBase::SetClippingDistance(float clip_near, float clip_far) {

  SetCameraNearFar(clip_near, clip_far);  
}

/// <summary>
/// カメラの位置変更
/// </summary>
/// <param name="debug_camera"> カメラ </param>
/// <returns></returns>
void CameraBase::CalcPositionCamera() {

  // カメラの座標を取得
  VECTOR pos_cam = GetCameraPosition();
  // 注視点の座標を取得
  VECTOR pos_look = GetLookAtPosition();
  // カメラの角度を取得
  float angle_horizon = GetHorizonAngle();
  float angle_vertical = GetVerticalAngle();

  std::cout << " --------------------------------------------- " << std::endl;

  std::cout << " カメラ座標 X座標： " << pos_cam.x << std::endl;
  std::cout << " カメラ座標 Y座標： " << pos_cam.y << std::endl;
  std::cout << " カメラ座標 Z座標： " << pos_cam.z << std::endl;
  std::cout << " 注視点座標 X座標： " << pos_look.x << std::endl;
  std::cout << " 注視点座標 Y座標： " << pos_look.y << std::endl;
  std::cout << " 注視点座標 Z座標： " << pos_look.z << std::endl;
  std::cout << " カメラ角度 水平 ： " << angle_horizon << std::endl;
  std::cout << " カメラ角度 垂直 ： " << angle_vertical << std::endl;

  // カメラと注視点の距離
  float distance = std::sqrtf(std::powf((pos_cam.x - pos_look.x), kSquare) +
                              std::powf((pos_cam.y - pos_look.y), kSquare) +
                              std::powf((pos_cam.z - pos_look.z), kSquare));

  std::cout << " カメラ注視点距離： " << distance << std::endl;

  VECTOR temp_pos1;
  VECTOR temp_pos2;

  // 垂直方向の角度から座標を算出(Z軸回転)
  float sin_param = sinf(angle_vertical / (kAngle360 / kHalfValuef) * DX_PI_F);
  float cos_param = cosf(angle_vertical / (kAngle360 / kHalfValuef) * DX_PI_F);
  temp_pos1.x = sin_param * distance;
  temp_pos1.y = cos_param * distance;
  temp_pos1.z = kZero;

  std::cout << " sinθ 垂直角度： " << sin_param << std::endl;
  std::cout << " cosθ 垂直角度： " << cos_param << std::endl;
  std::cout << " 算出値座標 X座標： " << temp_pos1.x << std::endl;
  std::cout << " 算出値座標 Y座標： " << temp_pos1.y << std::endl;
  std::cout << " 算出値座標 Z座標： " << temp_pos1.z << std::endl;

  // 水平方向の角度から座標を算出(Y軸回転)
  sin_param = sinf(angle_horizon / (kAngle360 / kHalfValuef) * DX_PI_F);
  cos_param = cosf(angle_horizon / (kAngle360 / kHalfValuef) * DX_PI_F);
  temp_pos2.x = cos_param * temp_pos1.x - sin_param * temp_pos1.z;
  temp_pos2.y = temp_pos1.y;
  temp_pos2.z = sin_param * temp_pos1.x + cos_param * temp_pos1.z;

  std::cout << " sinθ 水平角度： " << sin_param << std::endl;
  std::cout << " cosθ 水平角度： " << cos_param << std::endl;
  std::cout << " 算出値座標 X座標： " << temp_pos2.x << std::endl;
  std::cout << " 算出値座標 Y座標： " << temp_pos2.y << std::endl;
  std::cout << " 算出値座標 Z座標： " << temp_pos2.z << std::endl;

  // 算出した座標に注視点の位置を加算し、カメラの位置を算出する
  VECTOR camera_pos = VAdd(temp_pos2, pos_look);

  std::cout << " カメラ座標 X座標： " << camera_pos.x << std::endl;
  std::cout << " カメラ座標 Y座標： " << camera_pos.y << std::endl;
  std::cout << " カメラ座標 Z座標： " << camera_pos.z << std::endl;

  // 各座標値をカメラにセットする
  SetCameraPositionX(camera_pos.x);
  SetCameraPositionY(camera_pos.y);
  SetCameraPositionZ(camera_pos.z);
}

/// <summary>
/// 注視点の変更
/// </summary>
/// <param name="move_x"> 注視点のX方向移動量 </param>
/// <param name="move_z"> 注視点のZ方向移動量 </param>
/// <param name="direction"> 移動方向 </param>
/// <returns></returns>
void CameraBase::CalcLookAtPosition(float move_x, float move_y, float move_z, Direction direction) {

  // カメラと注視点の座標を取得
  VECTOR pos_cam = GetCameraPosition();
  VECTOR pos_look = GetLookAtPosition();
  // X-Z平面上でのカメラの座標をセット
  VECTOR pos_cam_xz = VSub(pos_cam, pos_look);
  // 高さを取得しておき、高さを「0」にしておく
  float cam_height = pos_cam_xz.y;
  pos_cam_xz.y = kZero;
  // X-Z平面上でのベクトルを正規化(単位ベクトルの取得)
  VECTOR cam_pos_norm = VNorm(pos_cam_xz);

  std::cout << " --------------------------------------------- " << std::endl;
  std::cout << " カメラ座標 X座標： " << pos_cam.x << std::endl;
  std::cout << " カメラ座標 Y座標： " << pos_cam.y << std::endl;
  std::cout << " カメラ座標 Z座標： " << pos_cam.z << std::endl;
  std::cout << " 注視点座標 X座標： " << pos_look.x << std::endl;
  std::cout << " 注視点座標 Y座標： " << pos_look.y << std::endl;
  std::cout << " 注視点座標 Z座標： " << pos_look.z << std::endl;
  std::cout << " X-Z平面座標 X座標： " << pos_cam_xz.x << std::endl;
  std::cout << " X-Z平面座標 Y座標： " << pos_cam_xz.y << std::endl;
  std::cout << " X-Z平面座標 Z座標： " << pos_cam_xz.z << std::endl;
  std::cout << " X-Z正規化座標 X座標： " << cam_pos_norm.x << std::endl;
  std::cout << " X-Z正規化座標 Y座標： " << cam_pos_norm.y << std::endl;
  std::cout << " X-Z正規化座標 Z座標： " << cam_pos_norm.z << std::endl;

  switch (direction) {
  case Direction::kLeft:
  case Direction::kRight: {
    // 横移動の場合、単位ベクトルの法線ベクトルを算出
    VECTOR pos_temp;
    pos_temp.x = cam_pos_norm.x * cosf(kAngle90 / (kAngle360 / kHalfValuef) * DX_PI_F) - cam_pos_norm.z * sinf(kAngle90 / (kAngle360 / kHalfValuef) * DX_PI_F);
    pos_temp.y = kZero;
    pos_temp.z = cam_pos_norm.x * sinf(kAngle90 / (kAngle360 / kHalfValuef) * DX_PI_F) + cam_pos_norm.z * cosf(kAngle90 / (kAngle360 / kHalfValuef) * DX_PI_F);
    // 法線ベクトルを単位ベクトルに再セット
    cam_pos_norm = pos_temp;
    break;
  }
  case Direction::kUp:
  case Direction::kDown: {
    VECTOR pos_temp;
    pos_temp.x = kZero;
    pos_temp.y = kScaleY;
    pos_temp.z = kZero;
    // 法線ベクトルを単位ベクトルに再セット
    cam_pos_norm = pos_temp;
    break;
  }
  default: {
    break;
  }
  }

  // 単位ベクトルに移動量を掛け合わせる
  MATRIX mtr_move = MGetScale(VGet(move_x, move_y, move_z));
  VECTOR move_adjust = VTransform(cam_pos_norm, mtr_move);
  std::cout << " 座標移動量 X座標： " << move_x << std::endl;
  std::cout << " 座標移動量 Y座標： " << move_y << std::endl;
  std::cout << " 座標移動量 Z座標： " << move_z << std::endl;
  std::cout << " 正規化 X座標： " << move_adjust.x << std::endl;
  std::cout << " 正規化 Y座標： " << move_adjust.y << std::endl;
  std::cout << " 正規化 Z座標： " << move_adjust.z << std::endl;

  // 移動量をカメラと注視点に加算
  pos_cam = VAdd(pos_cam, move_adjust);
  pos_look = VAdd(pos_look, move_adjust);

  std::cout << " カメラ座標 X座標： " << pos_cam.x << std::endl;
  std::cout << " カメラ座標 Y座標： " << pos_cam.y << std::endl;
  std::cout << " カメラ座標 Z座標： " << pos_cam.z << std::endl;
  std::cout << " 注視点座標 X座標： " << pos_look.x << std::endl;
  std::cout << " 注視点座標 Y座標： " << pos_look.y << std::endl;
  std::cout << " 注視点座標 Z座標： " << pos_look.z << std::endl;

  // 新しいカメラ座標、注視点をセット
  SetCameraPositionX(pos_cam.x);
  SetCameraPositionY(pos_cam.y);
  SetCameraPositionZ(pos_cam.z);
  SetLookAtPositionX(pos_look.x);
  SetLookAtPositionY(pos_look.y);
  SetLookAtPositionZ(pos_look.z);
}

/// <summary>
/// カメラを注視点に近づける(拡大)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CameraBase::ExpandObject() {

  // カメラと注視点の座標を取得
  VECTOR pos_camera = GetCameraPosition();
  VECTOR pos_look = GetLookAtPosition();
  // カメラ座標から注視点座標を引いたベクトルを取得
  VECTOR pos_diff = VSub(pos_camera, pos_look);
  // スケーリングを実行し、調整値のベクトルを取得
  VECTOR pos_cam_adjust = VScale(pos_diff, kCameraMoveRate);
  // カメラ座標に調整値を減算
  pos_camera = VSub(pos_camera, pos_cam_adjust);

  // 新しいカメラ位置と注視点の距離を算出
  // 指定距離より近づいていた場合、処理終了
  pos_diff = VSub(pos_camera, pos_look);
  float distance = VSize(pos_diff);
  if (distance <= kDistanceMin) {
    return;
  }

  SetCameraPositionX(pos_camera.x);
  SetCameraPositionY(pos_camera.y);
  SetCameraPositionZ(pos_camera.z);
}

/// <summary>
/// カメラを注視点から遠ざける(縮小)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CameraBase::ShrinkObject() {

  // カメラと注視点の座標を取得
  VECTOR pos_camera = GetCameraPosition();
  VECTOR pos_look = GetLookAtPosition();
  // カメラ座標から注視点座標を引いたベクトルを取得
  VECTOR pos_diff = VSub(pos_camera, pos_look);
  // スケーリングを実行し、調整値のベクトルを取得
  VECTOR pos_cam_adjust = VScale(pos_diff, kCameraMoveRate);
  // カメラ座標に調整値を加算
  pos_camera = VAdd(pos_camera, pos_cam_adjust);

  // 新しいカメラ位置と注視点の距離を算出
  // 指定距離より遠ざかっていた場合、処理終了
  pos_diff = VSub(pos_camera, pos_look);
  float distance = VSize(pos_diff);
  if (distance >= kDistanceMax) {
    return;
  }

  SetCameraPositionX(pos_camera.x);
  SetCameraPositionY(pos_camera.y);
  SetCameraPositionZ(pos_camera.z);
}

/// <summary>
/// 注視点を中心に上方向へ移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CameraBase::MoveUpCenteredOnGaze() {

  // 垂直方向の角度を取得、調整後再度セット
  float angle = GetVerticalAngle();
  angle -= kCameraAngleMove;
  if (angle <= kAngle1) {
    angle = kAngle1;
  }
  SetVerticalAngle(angle);

  // 座標計算処理
  CalcPositionCamera();
}

/// <summary>
/// 注視点を中心に下方向へ移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CameraBase::MoveDownCenteredOnGaze() {

  // 垂直方向の角度を取得、調整後再度セット
  float angle = GetVerticalAngle();
  angle += kCameraAngleMove;
  if (angle >= kAngle179) {
    angle = kAngle179;
  }
  SetVerticalAngle(angle);

  // 座標計算処理
  CalcPositionCamera();
}

/// <summary>
/// 注視点を中心に左方向へ移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CameraBase::MoveLeftCenteredOnGaze() {

  // 水平方向の角度を取得、調整後再度セット
  float angle = GetHorizonAngle();
  angle += kCameraAngleMove;
  if (angle >= kAngle360) {
    angle -= kAngle360;
  }
  SetHorizonAngle(angle);

  // 座標計算処理
  CalcPositionCamera();
}

/// <summary>
/// 注視点を中心に右方向へ移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CameraBase::MoveRightCenteredOnGaze() {

  // 水平方向の角度を取得、調整後再度セット
  float angle = GetHorizonAngle();
  angle -= kCameraAngleMove;
  if (angle <= kAngle0) {
    angle += kAngle360;
  }
  SetHorizonAngle(angle);

  // 座標計算処理
  CalcPositionCamera();
}