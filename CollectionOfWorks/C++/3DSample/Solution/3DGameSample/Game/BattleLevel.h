﻿#pragma once

#include "DxLib.h"
#include "System/Level.h"
#include "System/FontInfo.h"
#include "Game/GameInfo.h"
#include "Game/FieldManager.h"
#include "Game/Player.h"
#include "Game/CameraManager.h"
#include "Game/PlayerController.h"
#include "Game/CameraController.h"
#include "Game/BattleInfoUi.h"
#include "Game/BattleLevelEventInterface.h"
#include <vector>

/// <summary>
/// バトルレベル
/// </summary>
/// <remarks>
/// バトル画面のタスク
/// </remarks>
class BattleLevel : public Level, public BattleLevelEventInterface {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kPrepare,            // 準備フェーズ
    kStartWait,          // 起動待機フェーズ
    kStartUp,            // 起動フェーズ
    kProcess,            // 処理中フェーズ
    kBeforeTransition,   // レベル遷移前フェーズ
    kLevelTransition,    // レベル遷移フェーズ
    kFinalize,           // 終了処理フェーズ
    kFinalized,          // 終了処理済みフェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

  /// <summary>
  /// 終了フェーズの種類
  /// </summary>
  enum class FinalizePhaseType {
    kUnloadTask,         // タスクを降ろすフェーズ
    kDisposeTask,        // タスクを破棄するフェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

  /// <summary>
  /// フォントサイズ
  /// </summary>
  enum class FontSize {
    k45,                 // サイズ45
    kPhaseMaxIndex       // フェーズ項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> タスクID </param>
  /// <param name=""> タスクマネージャ </param>
  /// <returns></returns>
  BattleLevel(TaskId, TaskManager&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~BattleLevel();

  /// <summary>
  /// カメラ座標取得
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <param name=""> Z座標 </param>
  /// <returns></returns>
  void OnGetCameraPosition(float&, float&, float&) override;

  /// <summary>
  /// 注視点座標取得
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <param name=""> Z座標 </param>
  /// <returns></returns>
  void OnGetLookAtPosition(float&, float&, float&) override;

  /// <summary>
  /// プレイヤー座標取得
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <param name=""> Z座標 </param>
  /// <returns></returns>
  void OnGetPlayerPosition(float&, float&, float&) override;

  /// <summary>
  /// 角度取得
  /// </summary>
  /// <param name=""> 水平角度 </param>
  /// <param name=""> 垂直角度  </param>
  /// <returns></returns>
  void OnGetAngle(float&, float&) override;

  /// <summary>
  /// マウスホイールを奥に回す
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnRotateBackWheel() override;

  /// <summary>
  /// マウスホイールを手前に回す
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnRotateFrontWheel() override;

  /// <summary>
  /// Wキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushWKey() override;

  /// <summary>
  /// Sキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushSKey() override;

  /// <summary>
  /// Aキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushAKey() override;

  /// <summary>
  /// Dキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushDKey() override;

  /// <summary>
  /// Shift + Wキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushShiftWKey() override;

  /// <summary>
  /// Shift + Sキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushShiftSKey() override;

  /// <summary>
  /// Shift + Aキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushShiftAKey() override;

  /// <summary>
  /// Shift + Dキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushShiftDKey() override;

  /// <summary>
  /// Shift + Qキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushShiftQKey() override;

  /// <summary>
  /// Shift + Eキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushShiftEKey() override;

  /// <summary>
  /// Alt + Dキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushAltDKey() override;

  /// <summary>
  /// Iキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushIKey() override;

  /// <summary>
  /// Pキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushPKey() override;

  /// <summary>
  /// Wキー押下 (プレイヤーコントローラ)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushPlayerControllerWKey() override;

  /// <summary>
  /// Sキー押下 (プレイヤーコントローラ)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushPlayerControllerSKey() override;

  /// <summary>
  /// Aキー押下 (プレイヤーコントローラ)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushPlayerControllerAKey() override;

  /// <summary>
  /// Dキー押下 (プレイヤーコントローラ)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushPlayerControllerDKey() override;

  /// <summary>
  /// W + Aキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushPlayerControllerWAKey() override;

  /// <summary>
  /// W + Dキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushPlayerControllerWDKey() override;

  /// <summary>
  /// S + Aキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushPlayerControllerSAKey() override;

  /// <summary>
  /// S + Dキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushPlayerControllerSDKey() override;

  /// <summary>
  /// Spaceキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushPlayerControllerSpaceKey() override;

  /// <summary>
  /// Pキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushPlayerControllerPKey() override;

  /// <summary>
  /// Mキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushPlayerControllerMKey() override;

  /// <summary>
  /// Nキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushPlayerControllerNKey() override;

  /// <summary>
  /// 上矢印キー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushUpKey() override;

  /// <summary>
  /// 下矢印キー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushDownKey() override;

  /// <summary>
  /// 右矢印キー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushRightKey() override;

  /// <summary>
  /// 左矢印キー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushLeftKey() override;

  /// <summary>
  /// Escキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushEscapeKey() override;

  /// <summary>
  /// 床との接触判定処理
  /// </summary>
  /// <param name=""> 地面の高さ座標 </param>
  /// <returns> true:接地している, false:接地していない </returns>
  bool OnCollisionFloor(float&) override;

  /// <summary>
  /// カメラの注視点移動処理
  /// </summary>
  /// <param name=""> プレイヤー移動後位置 </param>
  /// <returns></returns>
  void OnMoveCameraLookAtPos(float) override;

  /// <summary>
  /// カメラの注視点をプレイヤーに戻す
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnResetCameraLookAtPos() override;


protected:

  /// <summary>
  /// 初期化処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool InitializePhase(float) override;

  /// <summary>
  /// 毎フレームの処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool UpdatePhase(float) override;

  /// <summary>
  /// 毎フレームの描画処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void RenderPhase() override;

  /// <summary>
  /// 終了処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool FinishPhase(float) override;


private:

  /// <summary>
  /// 画面上の設定 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void InitializeScreen();

  /// <summary>
  /// フィールドマネージャ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool InitializeFieldManager();

  /// <summary>
  /// プレイヤー 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool InitializePlayer();

  /// <summary>
  /// カメラマネージャ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool InitializeCameraManager();

  /// <summary>
  /// プレイヤーコントローラ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool InitializePlayerController();

  /// <summary>
  /// カメラコントローラ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool InitializeCameraController();

  /// <summary>
  /// UI情報 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool InitializeBattleInfoUi();

  /// <summary>
  /// 準備処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Prepare();

  /// <summary>
  /// カメラ初期角度取得
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void GetCameraInitAngle();

  /// <summary>
  /// 当たり判定確認処理
  /// </summary>
  /// <param name=""> 移動量 </param>
  /// <returns> true:当たった, false:当たっていない </returns>
  bool CheckCollision(VECTOR&);

  /// <summary>
  /// カメラの情報をセット
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetCameraData();

  /// <summary>
  /// 小数第n位四捨五入
  /// </summary>
  /// <param name=""> 対象数値 </param>
  /// <param name=""> 四捨五入する桁数 </param>
  /// <returns> 四捨五入後の数値 </returns>
  float ExecRoundOffNumber(float, float);

  /// <summary>
  /// 現在のフェーズの変更
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }

  /// <summary>
  /// 終了処理フェーズの変更
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeFinalizePhase(FinalizePhaseType finalize_phase) { finalize_phase_ = finalize_phase; }

  /// <summary>
  /// 透過率変更処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AdjustAlphaValue();


private:

  /// <summary>
  /// ライン座標
  /// </summary>
  struct LineAxis {
    VECTOR start;
    VECTOR end;
  };

  /// <summary>
  /// カメラ情報
  /// </summary>
  struct PlayerCameraData {
    VECTOR pos_camera;
    VECTOR pos_look;
    float angle_h;
    float angle_v;
  };

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 終了フェーズの種類
  /// </summary>
  FinalizePhaseType finalize_phase_;

  /// <summary>
  /// フィールドマネージャ
  /// </summary>
  FieldManager* field_manager_;

  /// <summary>
  /// プレイヤー
  /// </summary>
  Player* player_;

  /// <summary>
  /// カメラマネージャ
  /// </summary>
  CameraManager* camera_manager_;

  /// <summary>
  /// プレイヤーコントローラ
  /// </summary>
  PlayerController* player_controller_;

  /// <summary>
  /// カメラコントローラ
  /// </summary>
  CameraController* camera_controller_;

  /// <summary>
  /// UI情報
  /// </summary>
  BattleInfoUi* info_ui_;

  /// <summary>
  /// ライン X軸
  /// </summary>
  LineAxis x_axis_;

  /// <summary>
  /// ライン Y軸
  /// </summary>
  LineAxis y_axis_;

  /// <summary>
  /// ライン Z軸
  /// </summary>
  LineAxis z_axis_;

  /// <summary>
  /// 原点
  /// </summary>
  VECTOR origin_;

  /// <summary>
  /// プレイヤーカメラ情報
  /// </summary>
  PlayerCameraData player_cam_data_;

  /// <summary>
  /// フォントハンドル
  /// </summary>
  std::vector<int> font_handle_;

  /// <summary>
  /// 累積処理時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_;
};