﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "Game/PlayerEventInterface.h"

/// <summary>
/// プレイヤー
/// </summary>
class Player : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,       // 初期化処理フェーズ
    kStartWait,        // 起動待機フェーズ
    kStartUp,          // 起動フェーズ
    kProcess,          // 処理中フェーズ
    kStop,             // 停止フェーズ
    kBeforeFinalize,   // 終了処理前フェーズ
    kFinalize,         // 終了処理フェーズ
    kFinalized,        // 終了処理済みフェーズ
  };

  /// <summary>
  /// プレイヤーの状態
  /// </summary>
  enum class State {
    kStop,          // 待機
    kWalk,          // 歩行
    kJump,          // ジャンプ
    kLand,          // 着地
    kTypeMaxIndex   // 種類数
  };

  /// <summary>
  /// アニメーション
  /// </summary>
  enum class AnimationId {
    kJump,          // ジャンプ
    kLand,          // 着地
    kWalk,          // 歩行
    kStop,          // 待機
    kTypeMaxIndex   // 種類数
  };


public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> プレイヤーイベントインターフェース </param>
  /// <returns></returns>
  Player(PlayerEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~Player();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// モデルハンドルをセット
  /// </summary>
  /// <param name="handle"> モデルハンドル </param>
  /// <returns></returns>
  void SetModelHandle(int handle) { model_handle_ = handle; }

  /// <summary>
  /// モデルハンドルを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> モデルハンドル </returns>
  int GetModelHandle() { return model_handle_; }

  /// <summary>
  /// 表示座標のセット
  /// </summary>
  /// <param name=""> 座標ベクトル </param>
  /// <returns></returns>
  void SetDispPosition(VECTOR);

  /// <summary>
  /// 表示座標を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> 表示座標 </returns>
  VECTOR GetDispPosition() { return pos_; }

  /// <summary>
  /// モデルの幅をセット
  /// </summary>
  /// <param name=""> モデルの幅 </param>
  /// <returns></returns>
  void SetModelWidth(float width) { model_data_.size.width = width; }

  /// <summary>
  /// モデルの幅を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  float GetModelWidth() { return model_data_.size.width; }

  /// <summary>
  /// モデルの高さをセット
  /// </summary>
  /// <param name=""> モデルの高さ </param>
  /// <returns></returns>
  void SetModelHeight(float height) { model_data_.size.height = height; }

  /// <summary>
  /// モデルの高さを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  float GetModelHeight() { return model_data_.size.height; }

  /// <summary>
  /// モデルの奥行をセット
  /// </summary>
  /// <param name=""> モデルの奥行 </param>
  /// <returns></returns>
  void SetModelDepth(float depth) { model_data_.size.depth = depth; }

  /// <summary>
  /// モデルの奥行を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  float GetModelDepth() { return model_data_.size.depth; }

  /// <summary>
  /// モデルの角度をセット
  /// </summary>
  /// <param name=""> モデルの角度 </param>
  /// <returns></returns>
  void SetAngle(float);

  /// <summary>
  /// モデルの角度を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  float GetModelAngle() { return model_data_.angle; }

  /// <summary>
  /// プレイヤーの状態をセット
  /// </summary>
  /// <param name=""> プレイヤーの状態 </param>
  /// <returns></returns>
  void SetState(State state) { is_state_ = state; }

  /// <summary>
  /// プレイヤーの現在の状態を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  State GetCurrentState() { return is_exec_state_; }

  /// <summary>
  /// ジャンプ中かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:ジャンプ中, false:ジャンプ以外 </returns>
  bool IsStateJump() { return is_exec_state_ == State::kJump; }

  /// <summary>
  /// 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void InitializeData();

  /// <summary>
  /// 位置座標への加算
  /// </summary>
  /// <param name=""> 加算するベクトル </param>
  /// <returns> 加算後の位置座標 </returns>
  VECTOR AddPosition(VECTOR);

  /// <summary>
  /// 前に移動
  /// </summary>
  /// <param name=""> 現在のカメラ水平角度 </param>
  /// <returns> 移動量 </returns>
  VECTOR MoveFront(float);

  /// <summary>
  /// 左前に移動
  /// </summary>
  /// <param name=""> 現在のカメラ水平角度 </param>
  /// <returns> 移動量 </returns>
  VECTOR MoveFrontLeft(float);

  /// <summary>
  /// 右前に移動
  /// </summary>
  /// <param name=""> 現在のカメラ水平角度 </param>
  /// <returns> 移動量 </returns>
  VECTOR MoveFrontRight(float);

  /// <summary>
  /// 後ろに移動
  /// </summary>
  /// <param name=""> 現在のカメラ水平角度 </param>
  /// <returns> 移動量 </returns>
  VECTOR MoveBack(float);

  /// <summary>
  /// 左後ろに移動
  /// </summary>
  /// <param name=""> 現在のカメラ水平角度 </param>
  /// <returns> 移動量 </returns>
  VECTOR MoveBackLeft(float);

  /// <summary>
  /// 右後ろに移動
  /// </summary>
  /// <param name=""> 現在のカメラ水平角度 </param>
  /// <returns> 移動量 </returns>
  VECTOR MoveBackRight(float);

  /// <summary>
  /// 右に移動
  /// </summary>
  /// <param name=""> 現在のカメラ水平角度 </param>
  /// <returns> 移動量 </returns>
  VECTOR MoveRight(float);

  /// <summary>
  /// 左に移動
  /// </summary>
  /// <param name=""> 現在のカメラ水平角度 </param>
  /// <returns> 移動量 </returns>
  VECTOR MoveLeft(float);

  /// <summary>
  /// ジャンプする
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:ジャンプ実行, false:未実行 </returns>
  bool MoveJump();

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }

  /// <summary>
  /// 現在のフェーズが「起動待機」かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:起動待機, false:それ以外 </returns>
  bool IsStartWait() { return current_phase_ == PhaseType::kStartWait; }

  /// <summary>
  /// 現在のフェーズが「終了処理済み」かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理済み, false:それ以外 </returns>
  bool IsFinalized() { return current_phase_ == PhaseType::kFinalized; }


private:

  /// <summary>
  /// 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Initialize();

  /// <summary>
  /// 処理中処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Process();

  /// <summary>
  /// ジャンプ中の高さを算出
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void CalcJumpHeight();

  /// <summary>
  /// 待機アニメーション処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ExecWaitAnimation();

  /// <summary>
  /// 歩行アニメーション処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ExecWalkAnimation();

  /// <summary>
  /// ジャンプアニメーション処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ExecJumpAnimation();

  /// <summary>
  /// 着地アニメーション処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ExecLandAnimation();

  /// <summary>
  /// アニメーション切り替え処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SwitchAnimation();

  /// <summary>
  /// 移動処理
  /// </summary>
  /// <param name=""> X方向移動量 </param>
  /// <param name=""> Y方向移動量 </param>
  /// <param name=""> Z方向移動量 </param>
  /// <param name=""> 移動方向 </param>
  /// <returns> 移動量ベクトル </returns>
  VECTOR CalcMovePosition(float, float, float, float);


private:

  /// <summary>
  /// サイズ
  /// </summary>
  struct Size {
    float width;
    float height;
    float depth;
  };

  /// <summary>
  /// モデルデータ
  /// </summary>
  struct ModelData {
    Size size;
    float angle;
  };

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// プレイヤーイベントインターフェース
  /// </summary>
  PlayerEventInterface& player_event_interface_;

  /// <summary>
  /// モデルハンドル
  /// </summary>
  int model_handle_;

  /// <summary>
  /// 表示座標
  /// </summary>
  VECTOR pos_;

  /// <summary>
  /// モデルデータ
  /// </summary>
  ModelData model_data_;

  /// <summary>
  /// アニメーションインデックス
  /// </summary>
  int animation_id_;

  /// <summary>
  /// アニメーション再生時間
  /// </summary>
  float animation_time_;

  /// <summary>
  /// アニメーション総再生時間
  /// </summary>
  float animation_total_time_;

  /// <summary>
  /// 状態フラグ
  /// </summary>
  State is_state_;

  /// <summary>
  /// 実行中の状態
  /// </summary>
  State is_exec_state_;

  /// <summary>
  /// ジャンプしている時間
  /// </summary>
  float jump_time_;

  /// <summary>
  /// ジャンプ時のプレイヤー高さ
  /// </summary>
  float y_pos_height_;

  /// <summary>
  /// ジャンプ時の初速
  /// </summary>
  float velocity_init_;
};