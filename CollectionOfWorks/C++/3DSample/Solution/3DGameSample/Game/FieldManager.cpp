﻿#include "Game/FieldManager.h"

namespace {

  /// <summary>
  /// フィールドマップファイルパス
  /// </summary>
  const char* kCsvFileMap01 = "File/map01.csv";

  /// <summary>
  /// 床モデルファイルのファイルパス
  /// </summary>
  const char* kFloorModelFilePath = "Asset/Model/floor.mv1";

  /// <summary>
  /// 床テクスチャファイルのファイルパス
  /// </summary>
  const char* kFloorTextureFilePath = "Asset/Model/my_model.fbm/slate_driveway_diff_4k.jpg";

  /// <summary>
  /// 壁モデルファイルのファイルパス
  /// </summary>
  const char* kWallModelFilePath = "Asset/Model/wall.mv1";

  /// <summary>
  /// 壁テクスチャファイルのファイルパス
  /// </summary>
  const char* kWallTextureFilePath = "Asset/Model/my_model.fbm/brick_wall_006_diff_4k.jpg";

  /// <summary>
  /// 床・壁のサイズ
  /// </summary>
  const float kModelSize = 200.0f;

  /// <summary>
  /// 床・壁のハーフサイズ
  /// </summary>
  const float kModelHalfSize = 100.0f;

  /// <summary>
  /// 床・壁の厚み
  /// </summary>
  const float kModelThickness = 20.0f;

  /// <summary>
  /// 床・壁の厚みの半分
  /// </summary>
  const float kModelHalfThickness = 10.0f;

  /// <summary>
  /// Y座標
  /// </summary>
  const float kMapSizeY = 300.0f;

  /// <summary>
  /// ゼロ
  /// </summary>
  const float kZero = 0.0f;

  /// <summary>
  /// 0°
  /// </summary>
  const float kAngle0 = 0.0f;

  /// <summary>
  /// 90°
  /// </summary>
  const float kAngle90 = 90.0f;

  /// <summary>
  /// 180°
  /// </summary>
  const float kAngle180 = 180.0f;

  /// <summary>
  /// 半分
  /// </summary>
  const float kHalfValuef = 2.0f;

  /// <summary>
  /// 調整値
  /// </summary>
  const float kWallPosInterval = 196.8f;

  /// <summary>
  /// 1
  /// </summary>
  const int kOne = 1;

  /// <summary>
  /// インデックス最小値
  /// </summary>
  const int kMinIndex = 0;

  /// <summary>
  /// 符号変換
  /// </summary>
  const float kSignMinus = -1.0f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
FieldManager::FieldManager()
  : Task(TaskId::kFieldManager)
  , current_phase_(PhaseType::kWait)
  , initialize_phase_(InitPhase::kFieldMapLoad)
  , field_list_()
  , field_csv_()
  , field_floor_()
  , field_wall_()
  , index_wall_(0) {

  // コンソールに出力
  std::cout << "FieldManager コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
FieldManager::~FieldManager() {

  // コンソールに出力
  std::cout << "~FieldManager デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void FieldManager::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kWait:
  case PhaseType::kStartWait: {
    //------------------------------
    //待機フェーズ
    //------------------------------
    break;
  }
  case PhaseType::kInitialize: {
    //------------------------------
    //初期化処理フェーズ
    //------------------------------
    bool is_finish = Initialize();
    if (is_finish) {
      // 現在のフェーズを「起動待機」に変更
      ChangeCurrentPhase(PhaseType::kStartWait);
    }
    break;
  }
  case PhaseType::kStartUp: {
    //------------------------------
    //起動フェーズ
    //------------------------------
    // 現在のフェーズを「処理中」に変更
    ChangeCurrentPhase(PhaseType::kProcess);
    break;
  }
  case PhaseType::kProcess: {
    //------------------------------
    //処理中フェーズ
    //------------------------------
    break;
  }
  case PhaseType::kBeforeFinalize: {
    //------------------------------
    //終了処理前フェーズ
    //------------------------------
    // 現在のフェーズを「終了処理」に変更する
    ChangeCurrentPhase(PhaseType::kFinalize);
    break;
  }
  case PhaseType::kFinalize: {
    //------------------------------
    //終了処理フェーズ
    //------------------------------
    // 現在のフェーズを終了処理済みに変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }

  //------------------------------
  // フィールド処理
  //------------------------------
  switch (current_phase_) {
  case PhaseType::kStartUp:
  case PhaseType::kProcess:
  case PhaseType::kBeforeFinalize: {

    Process();
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void FieldManager::Render() {

  switch (current_phase_) {
  case PhaseType::kStartUp:
  case PhaseType::kProcess:
  case PhaseType::kBeforeFinalize: {

    for (int y = 0; y < static_cast<int>(field_floor_.size()); ++y) {
      for (int x = 0; x < static_cast<int>(field_floor_.at(0).size()); ++x) {
        if (field_floor_.at(y).at(x) == nullptr) {
          continue;
        }
        // 描画処理
        field_floor_.at(y).at(x)->Render();
      }
    }

    for (auto wall : field_wall_) {
      if (wall.second == nullptr) {
        continue;
      }
      // 描画処理実行
      wall.second->Render();
    }

    break;
  }
  }
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool FieldManager::Initialize() {

  switch (initialize_phase_) {
  case InitPhase::kFieldMapLoad: {
    //--------------------------------
    //フィールドマップのロード
    //--------------------------------
    bool is_finish = ReadCsvFile();
    if (is_finish) {
      // 初期化フェーズを「リスト登録」に変更
      ChangeInitializePhase(InitPhase::kListRegister);
    }
    break;
  }
  case InitPhase::kListRegister: {
    //--------------------------------
    //リスト登録処理
    //--------------------------------
    Field* field = nullptr;
    // フィールド処理(床)の登録
    field = RegisterField(FieldId::kFloor);
    // フィールド処理(壁)の登録
    field = RegisterField(FieldId::kWall);

    // 初期化フェーズを「床フィールド生成」に変更
    ChangeInitializePhase(InitPhase::kFloorCreate);

    break;
  }
  case InitPhase::kFloorCreate: {
    //--------------------------------
    //床生成
    //--------------------------------
    std::vector<Field*> field_line_floor;

    GameInfo* game_info = GameInfo::GetGameInfoInstance();
    if (game_info == nullptr) {
      std::cout << "FieldManager Initialize ゲーム情報インスタンス無し" << std::endl;
      return false;
    }
    float x_pos_start = game_info->GetMapPositionX();
    float y_pos_start = game_info->GetMapPositionY();
    float z_pos_start = game_info->GetMapPositionZ();
    float x_pos = x_pos_start;
    float y_pos = y_pos_start;
    float z_pos = z_pos_start;

    for (int y = 0; y < static_cast<int>(field_csv_.size()); ++y) {
      // Z方向の座標をセットする
      z_pos = z_pos_start + y * kModelSize + kModelHalfSize;

      for (int x = 0; x < static_cast<int>(field_csv_.at(0).size()); ++x) {

        // 床フィールド生成
        Field* field = CreateField(FieldId::kFloor);
        if (field == nullptr) {
          std::cout << "FieldManager Initialize 床フィールド生成エラー" << std::endl;
          // 初期化フェーズを「初期化エラー」に変更
          ChangeInitializePhase(InitPhase::kError);
          return false;
        }

        // 床フィールド反転させる
        MV1SetRotationXYZ(field->GetModelHandle(), VGet(DX_PI_F, kZero, kZero));

        // フィールド処理(モデル)の表示座標をセット
        x_pos = x_pos_start + x * kModelSize + kModelHalfSize;
        field->SetDispPosition(x_pos, y_pos, z_pos);

        // 四つ角座標をセット
        float width = field->GetModelWidth();
        float height = field->GetModelHeight();
        float angle = field->GetModelAngle();
        VECTOR pos_xz = VGet(x_pos, kZero, z_pos);
        field->SetModelCorner(CalcObjectCorner(width, height, angle, pos_xz));

        // フィールド処理を配列に格納
        field_line_floor.push_back(field);
      }
      // フィールド情報に横1列分のフィールド処理を格納
      field_floor_.push_back(field_line_floor);
      // 配列をクリア
      field_line_floor.clear();
    }

    // 初期化フェーズを「外壁生成」に変更
    ChangeInitializePhase(InitPhase::kOutWallCreate);
    break;
  }
  case InitPhase::kOutWallCreate: {
    //--------------------------------
    //外壁生成
    //--------------------------------
    GameInfo* game_info = GameInfo::GetGameInfoInstance();
    if (game_info == nullptr) {
      std::cout << "FieldManager Initialize ゲーム情報インスタンス無し" << std::endl;
      return false;
    }
    float x_pos_start = game_info->GetMapPositionX();
    float y_pos_start = game_info->GetMapPositionY();
    float z_pos_start = game_info->GetMapPositionZ();
    float x_pos = x_pos_start;
    float y_pos = y_pos_start;
    float z_pos = z_pos_start;

    // インデックス初期化
    index_wall_ = kMinIndex;

    for (int y = 0; y < static_cast<int>(field_csv_.size()); ++y) {
      // Z方向の座標をセットする
      z_pos = z_pos_start + y * kModelSize + kModelHalfSize;

      for (int x = 0; x < static_cast<int>(field_csv_.at(0).size()); ++x) {

        if (y == kMinIndex || y == static_cast<int>(field_csv_.size() - kOne)) {
          // 壁フィールド生成
          Field* field = CreateField(FieldId::kWall);
          if (field == nullptr) {
            std::cout << "FieldManager Initialize フィールド生成エラー" << std::endl;
              // 初期化フェーズを「初期化エラー」に変更
              ChangeInitializePhase(InitPhase::kError);
              return false;
          }

          float x_pos_wall = x_pos_start + x * kModelSize + kModelHalfSize;
          float y_pos_wall = y_pos_start + kModelHalfSize + kModelHalfThickness;
          float z_pos_wall = z_pos;

          if (y == 0) {
            z_pos_wall -= kModelHalfSize - kModelHalfThickness;
            // 角度をセット
            field->SetModelAngle(kAngle90);
            // 壁の向きを修正
            MV1SetRotationXYZ(field->GetModelHandle(), VGet(kZero, kAngle90 / kAngle180 * DX_PI_F, kZero));
          }
          else if (y == static_cast<int>(field_csv_.size() - kOne)) {
            z_pos_wall += kModelHalfSize - kModelHalfThickness;
            // 角度をセット
            field->SetModelAngle(kAngle90);
            // 壁の向きを修正
            MV1SetRotationXYZ(field->GetModelHandle(), VGet(kZero, kAngle90 / kAngle180 * DX_PI_F, kZero));
          }
          // 表示座標のセット
          field->SetDispPosition(x_pos_wall, y_pos_wall, z_pos_wall);

          // 四つ角座標をセット
          float width = field->GetModelWidth();
          float depth = field->GetModelDepth();
          float angle = field->GetModelAngle();
          VECTOR pos_xz = VGet(x_pos_wall, kZero, z_pos_wall);
          field->SetModelCorner(CalcObjectCorner(width, depth, angle, pos_xz));

          // フィールド情報に格納
          field_wall_[index_wall_] = field;
          ++index_wall_;
        }

        if (x == kMinIndex || x == static_cast<int>(field_csv_.at(0).size() - kOne)) {
          // 壁フィールド生成
          Field* field = CreateField(FieldId::kWall);
          if (field == nullptr) {
            std::cout << "FieldManager Initialize フィールド生成エラー" << std::endl;
            // 初期化フェーズを「初期化エラー」に変更
            ChangeInitializePhase(InitPhase::kError);
            return false;
          }

          float x_pos_wall = x_pos_start + x * kModelSize;
          float y_pos_wall = y_pos_start + kModelHalfSize + kModelHalfThickness;
          float z_pos_wall = z_pos;

          if (x == 0) {
            x_pos_wall += kModelHalfThickness;
          }
          else if (x == static_cast<int>(field_csv_.at(0).size() - kOne)) {
            x_pos_wall += kModelSize - kModelHalfThickness;
          }
          // 角度をセット
          field->SetModelAngle(kAngle0);
          MV1SetRotationXYZ(field->GetModelHandle(), VGet(kZero, kAngle0 / kAngle180 * DX_PI_F, kZero));
          field->SetDispPosition(x_pos_wall, y_pos_wall, z_pos_wall);

          // 四つ角座標をセット
          float width = field->GetModelWidth();
          float depth = field->GetModelDepth();
          float angle = field->GetModelAngle();
          VECTOR pos_xz = VGet(x_pos_wall, kZero, z_pos_wall);
          field->SetModelCorner(CalcObjectCorner(width, depth, angle, pos_xz));

          // フィールド情報に格納
          field_wall_[index_wall_] = field;
          ++index_wall_;
        }
      }
    }
    
    // 初期化フェーズを「内壁生成」に変更
    ChangeInitializePhase(InitPhase::kInnerWallCreate);

    break;
  }
  case InitPhase::kInnerWallCreate: {
    //--------------------------------
    //内壁生成
    //--------------------------------
    GameInfo* game_info = GameInfo::GetGameInfoInstance();
    if (game_info == nullptr) {
      std::cout << "FieldManager Initialize ゲーム情報インスタンス無し" << std::endl;
      return false;
    }
    float x_pos_start = game_info->GetMapPositionX();
    float y_pos_start = game_info->GetMapPositionY();
    float z_pos_start = game_info->GetMapPositionZ();
    float x_pos = x_pos_start;
    float y_pos = y_pos_start;
    float z_pos = z_pos_start;

    for (int y = 0; y < static_cast<int>(field_csv_.size()); ++y) {
      // Z方向の座標をセットする
      z_pos = z_pos_start + y * kModelSize + kModelHalfSize;

      for (int x = 0; x < static_cast<int>(field_csv_.at(0).size()); ++x) {

        FieldId field_id = static_cast<FieldId>(field_csv_.at(y).at(x));
        if (field_id != FieldId::kWall) {
          continue;
        }

        // 壁フィールド生成
        Field* field = CreateField(FieldId::kWall);
        if (field == nullptr) {
          std::cout << "FieldManager Initialize フィールド生成エラー" << std::endl;
          // 初期化フェーズを「初期化エラー」に変更
          ChangeInitializePhase(InitPhase::kError);
          return false;
        }

        float x_pos_wall = z_pos;
        float y_pos_wall = y_pos_start + kModelHalfSize + kModelHalfThickness;
        float z_pos_wall = x_pos_start + x * kModelSize + kModelHalfSize;

        if (x == 0) {
          x_pos_wall += kModelHalfSize - kModelHalfThickness;
        }
        else {

          // 左隣が壁なら向きを変える
          int x_pos_left = x - kOne;
          FieldId field_id_left = static_cast<FieldId>(field_csv_.at(y).at(x_pos_left));
          if (field_id == FieldId::kWall) {
            x_pos_wall += kModelHalfSize - kModelHalfThickness;
            // 角度をセット
            field->SetModelAngle(kAngle0);
            // 壁の向きを修正
            MV1SetRotationXYZ(field->GetModelHandle(), VGet(kZero, kAngle0 / kAngle180 * DX_PI_F, kZero));
          }
          else {
            z_pos_wall += kModelHalfSize - kModelHalfThickness;
            // 角度をセット
            field->SetModelAngle(kAngle90);
            // 壁の向きを修正
            MV1SetRotationXYZ(field->GetModelHandle(), VGet(kZero, kAngle90 / kAngle180 * DX_PI_F, kZero));
          }
        }


        field->SetDispPosition(x_pos_wall, y_pos_wall, z_pos_wall);

        // 四つ角座標をセット
        float width = field->GetModelWidth();
        float depth = field->GetModelDepth();
        float angle = field->GetModelAngle();
        VECTOR pos_xz = VGet(x_pos_wall, kZero, z_pos_wall);
        field->SetModelCorner(CalcObjectCorner(width, depth, angle, pos_xz));

        // フィールド情報に格納
        field_wall_[index_wall_] = field;
        ++index_wall_;
      }
    }

    // インデックスをデクリメント
    --index_wall_;

    // 初期化フェーズを「終了処理済み」に変更
    ChangeInitializePhase(InitPhase::kFinalized);

    break;
  }
  }

  // 初期化フェーズが「終了処理済み」なら終了として返す
  if (initialize_phase_ == InitPhase::kFinalized) {
    return true;
  }

  return false;
}

/// <summary>
/// 四つ角のセット
/// </summary>
/// <param name=""> 幅 </param>
/// <param name=""> 奥行 </param>
/// <param name=""> 角度 </param>
/// <param name=""> 中心座標(XZ平面) </param>
/// <returns></returns>
std::vector<VECTOR> FieldManager::CalcObjectCorner(float width, float depth, float angle, VECTOR pos_xz) {

  std::vector<VECTOR> corner;

  //std::cout << "中心　(X, Z) = (" << pos_xz.x << "," << pos_xz.z << ")" << std::endl;

  // 左上
  VECTOR pos_corner = VGet(kSignMinus * depth / kHalfValuef, kZero, width / kHalfValuef);
  //std::cout << "左上　(X, Z) = (" << pos_corner.x << "," << pos_corner.z << ")" << std::endl;
  pos_corner = VTransform(pos_corner, MGetRotY(angle / kAngle180 * DX_PI_F));
  //std::cout << "左上　(X, Z) = (" << pos_corner.x << "," << pos_corner.z << ")" << std::endl;
  corner.push_back(VAdd(pos_corner, pos_xz));
  //std::cout << "左上　(X, Z) = (" << pos_corner.x << "," << pos_corner.z << ")" << std::endl;
  
  // 右上
  pos_corner = VGet(depth / kHalfValuef, kZero, width / kHalfValuef);
  //std::cout << "右上　(X, Z) = (" << pos_corner.x << "," << pos_corner.z << ")" << std::endl;
  pos_corner = VTransform(pos_corner, MGetRotY(angle / kAngle180 * DX_PI_F));
  //std::cout << "右上　(X, Z) = (" << pos_corner.x << "," << pos_corner.z << ")" << std::endl;
  corner.push_back(VAdd(pos_corner, pos_xz));
  //std::cout << "右上　(X, Z) = (" << pos_corner.x << "," << pos_corner.z << ")" << std::endl;
  // 左下
  pos_corner = VGet(kSignMinus * depth / kHalfValuef, kZero, kSignMinus * width / kHalfValuef);
  //std::cout << "左下　(X, Z) = (" << pos_corner.x << "," << pos_corner.z << ")" << std::endl;
  pos_corner = VTransform(pos_corner, MGetRotY(angle / kAngle180 * DX_PI_F));
  //std::cout << "左下　(X, Z) = (" << pos_corner.x << "," << pos_corner.z << ")" << std::endl;
  corner.push_back(VAdd(pos_corner, pos_xz));
  //std::cout << "左下　(X, Z) = (" << pos_corner.x << "," << pos_corner.z << ")" << std::endl;
  // 右下
  pos_corner = VGet(depth / kHalfValuef, kZero, kSignMinus * width / kHalfValuef);
  //std::cout << "右下　(X, Z) = (" << pos_corner.x << "," << pos_corner.z << ")" << std::endl;
  pos_corner = VTransform(pos_corner, MGetRotY(angle / kAngle180 * DX_PI_F));
  //std::cout << "右下　(X, Z) = (" << pos_corner.x << "," << pos_corner.z << ")" << std::endl;
  corner.push_back(VAdd(pos_corner, pos_xz));
  //std::cout << "右下　(X, Z) = (" << pos_corner.x << "," << pos_corner.z << ")" << std::endl;

  return corner;
}

/// <summary>
/// 処理中処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void FieldManager::Process() {

  for (int y = 0; y < static_cast<int>(field_floor_.size()); ++y) {
    for (int x = 0; x < static_cast<int>(field_floor_.at(0).size()); ++x) {

      if (field_floor_.at(y).at(x) == nullptr) {
        continue;
      }
      // 更新処理
      field_floor_.at(y).at(x)->Update();
    }
  }
}

/// <summary>
/// CSVファイルの読み取り
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, flase:処理中 </returns>
bool FieldManager::ReadCsvFile() {

  std::string str_buf;
  std::string str_conma_buf;
  std::string input_csv_file_path = kCsvFileMap01;
  // 読み込むcsvファイルを開く(std::ifstreamのコンストラクタで開く)
  std::ifstream ifs_csv_file(input_csv_file_path);
  if (!ifs_csv_file) {
    std::cout << "FieldManager ReadCsvFile CSVファイルが開けません" << std::endl;
    return false;
  }

  std::vector<int> field_line;

  // getline関数で1行ずつ読み込む(読み込んだ内容はstr_bufに格納)
  while (getline(ifs_csv_file, str_buf)) {

    // 「,」区切りごとにデータを読み込むためにistringstream型にする
    std::istringstream i_stream(str_buf);
    // 「,」区切りごとにデータを読み込む
    while (getline(i_stream, str_conma_buf, ',')) {
      // 横1列のフィールド情報を格納
      field_line.push_back(std::stoi(str_conma_buf));
    }
    // フィールドマップ情報に横1列文のデータを格納
    field_csv_.push_back(field_line);
    // 配列をクリア
    field_line.clear();
  }

  ifs_csv_file.close();

  // フィールドの広さをセット
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    std::cout << "FieldManager ReadCsvFile ゲーム情報インスタンス無し" << std::endl;
    return false;
  }
  float size_x = static_cast<float>(field_csv_.at(0).size()) * kModelSize;
  float size_y = kMapSizeY;
  float size_z = static_cast<float>(field_csv_.size()) * kModelSize;

  game_info->SetMapSize(size_x, size_y, size_z);

  return true;
}

/// <summary>
/// フィールドリストへの登録
/// </summary>
/// <param name="field_id"> フィールドID </param>
/// <returns> フィールド </returns>
Field* FieldManager::RegisterField(FieldId field_id) {

  // 既にリストに登録済みの場合は処理終了
  if (field_list_.find(field_id) != field_list_.end()) {
    std::cout << "FieldManager RegisterField 既に登録済みID" << std::endl;
    return nullptr;
  }

  // フィールドの生成
  Field* field = new Field();
  if (field == nullptr) {
    std::cout << "FieldManager RegisterField Field生成失敗" << std::endl;
    return nullptr;
  }

  // リストに登録
  field_list_[field_id] = field;
  // フィールドIDをセット
  field->SetFieldId(field_id);

  // モデルのロード処理
  int model_handle = 0;
  int graphic_handle = 0;
  switch (field_id) {
  case FieldId::kFloor: {

    // 床モデルのロード
    model_handle = MV1LoadModel(kFloorModelFilePath);
    // 床のグラフィックハンドル
    graphic_handle = LoadGraph(kFloorTextureFilePath);

    // サイズと角度をセット
    field->SetModelWidth(kModelSize);
    field->SetModelHeight(kModelSize);
    field->SetModelDepth(kModelThickness);
    field->SetModelAngle(kAngle0);
    break;
  }
  case FieldId::kWall: {

    // 壁モデルのロード
    model_handle = MV1LoadModel(kWallModelFilePath);
    // 壁のグラフィックハンドル
    graphic_handle = LoadGraph(kWallTextureFilePath);

    // サイズと角度をセット
    field->SetModelWidth(kModelSize);
    field->SetModelHeight(kModelSize);
    field->SetModelDepth(kModelThickness);
    field->SetModelAngle(kAngle0);
    break;
  }
  }
  field->SetModelHandle(model_handle);
  field->SetGraphicHandle(graphic_handle);

  return field;
}

/// <summary>
/// フィールドの生成
/// </summary>
/// <param name="field_id"> フィールドID </param>
/// <returns> フィールド </returns>
Field* FieldManager::CreateField(FieldId field_id) {

  Field* field = nullptr;
  if (field_list_.find(field_id) == field_list_.end()) {
    return nullptr;
  }

  // フィールド生成
  field = field_list_[field_id]->GenerateClone();

  // モデルハンドルの再セット
  int model_handle = MV1DuplicateModel(field->GetModelHandle());
  field->SetModelHandle(model_handle);

  // 使用するテクスチャのセット
  MV1SetTextureGraphHandle(model_handle, kMinIndex, field->GetGraphicHandle(), false);

  return field;
}

/// <summary>
/// 壁との衝突判定
/// </summary>
/// <param name="pos_object"> 判定対象物の座標 </param>
/// <param name="width"> 対象物の幅 </param>
/// <param name="depth"> 対象物の奥行 </param>
/// <param name="angle"> 対象物の水平角度 </param>
/// <param name="wall_vec"> 接触面のベクトル </param>
/// <returns> true:衝突した, false:衝突してない </returns>
bool FieldManager::CheckCollisionWall(VECTOR pos_object, float width, float depth,
                                      float angle, VECTOR& wall_vec) {

  // 壁の四つ角、衝突判定対象物の格納配列
  std::vector<VECTOR> object_corner;
  // 対象オブジェクトの座標をX-Z平面に変換
  VECTOR pos_object_xz = VGet(pos_object.x, kZero, pos_object.z);

  // X-Z平面上の座標をY軸を中心に指定角度で回転、
  // オブジェクトの中心座標を加算することで角の座標を算出し、配列に格納
  object_corner = CalcObjectCorner(width, depth, angle, pos_object_xz);

  VECTOR obj_topleft = object_corner.at(static_cast<int>(Field::Position::kTopLeft));
  VECTOR obj_topright = object_corner.at(static_cast<int>(Field::Position::kTopRight));
  VECTOR obj_bottomleft = object_corner.at(static_cast<int>(Field::Position::kBottomLeft));
  VECTOR obj_bottomright = object_corner.at(static_cast<int>(Field::Position::kBottomRight));

  //std::cout << "【プレイヤー】" << std::endl;
  //std::cout << "　左上：(X, Z) = (" << obj_topleft.x << "," << obj_topleft.z << ")" << std::endl;
  //std::cout << "　右上：(X, Z) = (" << obj_topright.x << "," << obj_topright.z << ")" << std::endl;
  //std::cout << "　左下：(X, Z) = (" << obj_bottomleft.x << "," << obj_bottomleft.z << ")" << std::endl;
  //std::cout << "　右下：(X, Z) = (" << obj_bottomright.x << "," << obj_bottomright.z << ")" << std::endl;

  for (auto field : field_wall_) {

    bool is_collided = false;
    std::vector<VECTOR> wall_corner = field.second->GetModelCorner();
    VECTOR wall_topleft = wall_corner.at(static_cast<int>(Field::Position::kTopLeft));
    VECTOR wall_topright = wall_corner.at(static_cast<int>(Field::Position::kTopRight));
    VECTOR wall_bottomleft = wall_corner.at(static_cast<int>(Field::Position::kBottomLeft));
    VECTOR wall_bottomright = wall_corner.at(static_cast<int>(Field::Position::kBottomRight));

    //---------------------------------
    // 壁の左側面との接触
    //---------------------------------
    is_collided = CheckCollisionLineSegment(obj_topleft, obj_topright, wall_topleft, wall_bottomleft);
    if (is_collided) {
      wall_vec.x = wall_bottomleft.x - wall_topleft.x;
      wall_vec.y = kZero;
      wall_vec.z = wall_bottomleft.z - wall_topleft.z;

      std::cout << "プレイヤー(左側面)が壁に接触" << std::endl;
      return true;
    }
    is_collided = CheckCollisionLineSegment(obj_bottomleft, obj_bottomright, wall_topleft, wall_bottomleft);
    if (is_collided) {
      wall_vec.x = wall_bottomleft.x - wall_topleft.x;
      wall_vec.y = kZero;
      wall_vec.z = wall_bottomleft.z - wall_topleft.z;

      std::cout << "プレイヤー(右側面)が壁に接触" << std::endl;
      return true;
    }
    is_collided = CheckCollisionLineSegment(obj_topleft, obj_bottomleft, wall_topleft, wall_bottomleft);
    if (is_collided) {
      wall_vec.x = wall_bottomleft.x - wall_topleft.x;
      wall_vec.y = kZero;
      wall_vec.z = wall_bottomleft.z - wall_topleft.z;

      std::cout << "プレイヤー(後面)が壁に接触" << std::endl;
      return true;
    }
    is_collided = CheckCollisionLineSegment(obj_topright, obj_bottomright, wall_topleft, wall_bottomleft);
    if (is_collided) {
      wall_vec.x = wall_bottomleft.x - wall_topleft.x;
      wall_vec.y = kZero;
      wall_vec.z = wall_bottomleft.z - wall_topleft.z;

      std::cout << "プレイヤー(前面)が壁に接触" << std::endl;
      return true;
    }

    //---------------------------------
    // 壁の右側面との接触
    //---------------------------------
    is_collided = CheckCollisionLineSegment(obj_topleft, obj_topright, wall_topright, wall_bottomright);
    if (is_collided) {
      wall_vec.x = wall_bottomright.x - wall_topright.x;
      wall_vec.y = kZero;
      wall_vec.z = wall_bottomright.z - wall_topright.z;

      std::cout << "プレイヤー(左側面)が壁に接触" << std::endl;
      return true;
    }
    is_collided = CheckCollisionLineSegment(obj_bottomleft, obj_bottomright, wall_topright, wall_bottomright);
    if (is_collided) {
      wall_vec.x = wall_bottomright.x - wall_topright.x;
      wall_vec.y = kZero;
      wall_vec.z = wall_bottomright.z - wall_topright.z;

      std::cout << "プレイヤー(右側面)が壁に接触" << std::endl;
      return true;
    }
    is_collided = CheckCollisionLineSegment(obj_topleft, obj_bottomleft, wall_topright, wall_bottomright);
    if (is_collided) {
      wall_vec.x = wall_bottomright.x - wall_topright.x;
      wall_vec.y = kZero;
      wall_vec.z = wall_bottomright.z - wall_topright.z;

      std::cout << "プレイヤー(後面)が壁に接触" << std::endl;
      return true;
    }
    is_collided = CheckCollisionLineSegment(obj_topright, obj_bottomright, wall_topright, wall_bottomright);
    if (is_collided) {
      wall_vec.x = wall_bottomright.x - wall_topright.x;
      wall_vec.y = kZero;
      wall_vec.z = wall_bottomright.z - wall_topright.z;

      std::cout << "プレイヤー(前面)が壁に接触" << std::endl;
      return true;
    }
  }

  return false;
}

/// <summary>
/// 線分どうしの接触判定
/// </summary>
  /// <param name=""> 対象物 始点 </param>
  /// <param name=""> 対象物 終点 </param>
  /// <param name=""> 壁の線分 始点 </param>
  /// <param name=""> 壁の線分 終点 </param>
/// <returns> true:接触した, false:接触してない </returns>
bool FieldManager::CheckCollisionLineSegment(VECTOR obj_start, VECTOR obj_end, VECTOR wall_start, VECTOR wall_end) {

  bool is_collided = false;

  //=======================================
  // 対象物の上辺との接触確認
  //=======================================
  // 壁視点
  VECTOR wall = VGet(wall_end.x - wall_start.x, kZero, wall_end.z - wall_start.z);
  VECTOR wall_left = VGet(obj_start.x - wall_start.x, kZero, obj_start.z - wall_start.z);
  VECTOR wall_right = VGet(obj_end.x - wall_start.x, kZero, obj_end.z - wall_start.z);
  // オブジェクト視点
  VECTOR obj = VGet(obj_end.x - obj_start.x, kZero, obj_end.z - obj_start.z);
  VECTOR obj_left = VGet(wall_start.x - obj_start.x, kZero, wall_start.z - obj_start.z);
  VECTOR obj_right = VGet(wall_end.x - obj_start.x, kZero, wall_end.z - obj_start.z);

  //------------------------------------------------------
  //壁視点の外積とオブジェクト視点の外積を計算し、
  //計算結果がともに負の値となった場合、接触している
  //------------------------------------------------------
  is_collided = CalcCrossProduct(wall, wall_left, wall_right);
  if (is_collided) {
    is_collided = CalcCrossProduct(obj, obj_left, obj_right);
    if (is_collided) {
      return true;
    }
  }

  return false;
}

/// <summary>
/// 外積計算
/// </summary>
/// <param name=""> ベクトルA </param>
/// <param name=""> ベクトルB </param>
/// <param name=""> ベクトルC </param>
/// <returns> true:接触した, false:接触してない </returns>
bool FieldManager::CalcCrossProduct(VECTOR vector_a, VECTOR vector_b, VECTOR vector_c) {

  float cross_01 = vector_a.x * vector_b.z - vector_b.x * vector_a.z;
  float cross_02 = vector_a.x * vector_c.z - vector_c.x * vector_a.z;
  if (cross_01 * cross_02 < kZero) {
    return true;
  }

  return false;
}

/// <summary>
/// 床との衝突判定
/// </summary>
/// <param name="pos_object"> 判定対象物の座標 </param>
/// <param name="y_pos_floor"> 床の高さ </param>
/// <returns> true:衝突した, false:衝突してない </returns>
bool FieldManager::CheckCollisionFloor(VECTOR pos_object, float& y_pos_floor) {

  // 対象オブジェクトの座標をX-Z平面に変換
  VECTOR pos_object_xz = VGet(pos_object.x, kZero, pos_object.z);

  //--------------------------------------
  // 対象物が存在する床の特定
  //--------------------------------------
  for (int y = 0; y < static_cast<int>(field_floor_.size()); ++y) {
    for (int x = 0; x < static_cast<int>(field_floor_.at(0).size()); ++x) {

      if (field_floor_.at(y).at(x) == nullptr) {
        continue;
      }

      // 床の四つ角を取得
      std::vector<VECTOR> wall_corner = field_floor_.at(y).at(x)->GetModelCorner();
      VECTOR wall_topleft = wall_corner.at(static_cast<int>(Field::Position::kTopLeft));
      VECTOR wall_bottomright = wall_corner.at(static_cast<int>(Field::Position::kBottomRight));

      if ((pos_object_xz.x >= wall_topleft.x && pos_object_xz.x < wall_bottomright.x) &&
          (pos_object_xz.z >= wall_bottomright.z && pos_object_xz.z < wall_topleft.z)) {

        float y_pos = field_floor_.at(y).at(x)->GetDispPositionY();
        float floor_height = y_pos + field_floor_.at(y).at(x)->GetModelDepth();

        // プレイヤーの足裏が床の高さより低い位置にある場合
        // 床と衝突していることとする
        if (pos_object.y >= y_pos && pos_object.y <= floor_height) {
          y_pos_floor = floor_height;
          return true;
        }
        break;
      }
    }
  }

  return false;
}

/// <summary>
/// フィールドマップの破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void FieldManager::DisposeFieldData() {

  for (int y = 0; y < static_cast<int>(field_floor_.size()); ++y) {
    for (int x = 0; x < static_cast<int>(field_floor_.at(0).size()); ++x) {

      if (field_floor_.at(y).at(x) == nullptr) {
        continue;
      }

      // モデルの破棄
      MV1DeleteModel(field_floor_.at(y).at(x)->GetModelHandle());

      // フィールド処理の破棄
      delete field_floor_.at(y).at(x);
      field_floor_.at(y).at(x) = nullptr;
    }
  }

  for (auto wall : field_wall_) {

      if (wall.second == nullptr) {
        continue;
      }
      // モデルの破棄
      MV1DeleteModel(wall.second->GetModelHandle());
      // フィールド処理の破棄
      delete wall.second;
      wall.second = nullptr;
  }
}

/// <summary>
/// フィールドリストの破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void FieldManager::DisposeFieldList() {

  for (auto field : field_list_) {

    if (field.second == nullptr) {
      continue;
    }
    // モデルの破棄
    MV1DeleteModel(field.second->GetModelHandle());
    // フィールド処理の破棄
    delete field.second;
    field.second = nullptr;
  }
}