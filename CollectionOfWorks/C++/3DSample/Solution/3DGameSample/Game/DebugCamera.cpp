﻿#include "Game/DebugCamera.h"

namespace {

  /// <summary>
  /// 注視点移動倍率
  /// </summary>
  const float kLookAtMoveValue = 10.0f;

  /// <summary>
  /// マイナス
  /// </summary>
  const float kSignMinus = -1.0f;

  /// <summary>
  /// 距離ゼロ
  /// </summary>
  const float kZero = 0.0f;
}


/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
DebugCamera::DebugCamera()
  : CameraBase() {

  // コンソールに出力
  std::cout << "DebugCamera コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
DebugCamera::~DebugCamera() {

  // コンソールに出力
  std::cout << "~DebugCamera デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void DebugCamera::Update() {

  CameraBase::Update();
}

/// <summary>
/// 注視点を前進
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void DebugCamera::MoveFrontGaze() {

  // 注視点の移動量
  float move_x = kSignMinus * kLookAtMoveValue;
  float move_y = kZero;
  float move_z = kSignMinus * kLookAtMoveValue;

  // 注視点の座標計算
  CalcLookAtPosition(move_x, move_y, move_z, Direction::kFront);

  // 新しい座標を取得
  VECTOR pos_camera = GetCameraPosition();
  VECTOR pos_look = GetLookAtPosition();

  // カメラの設定に反映する
  SetCameraPositionAndTarget_UpVecY(pos_camera, pos_look);
}

/// <summary>
/// 注視点を後退
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void DebugCamera::MoveBackGaze() {

  // 注視点の移動量
  float move_x = kLookAtMoveValue;
  float move_y = kZero;
  float move_z = kLookAtMoveValue;

  // 注視点の座標計算
  CalcLookAtPosition(move_x, move_y, move_z, Direction::kBack);

  // 新しい座標を取得
  VECTOR pos_camera = GetCameraPosition();
  VECTOR pos_look = GetLookAtPosition();

  // カメラの設定に反映する
  SetCameraPositionAndTarget_UpVecY(pos_camera, pos_look);
}

/// <summary>
/// 注視点を上方向へ移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void DebugCamera::MoveUpGaze() {

  // 注視点の移動量
  float move_x = kZero;
  float move_y = kLookAtMoveValue;
  float move_z = kZero;

  // 注視点の座標計算
  CalcLookAtPosition(move_x, move_y, move_z, Direction::kUp);

  // 新しい座標を取得
  VECTOR pos_camera = GetCameraPosition();
  VECTOR pos_look = GetLookAtPosition();

  // カメラの設定に反映する
  SetCameraPositionAndTarget_UpVecY(pos_camera, pos_look);
}

/// <summary>
/// 注視点を下方向へ移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void DebugCamera::MoveDownGaze() {

  // 注視点の移動量
  float move_x = kZero;
  float move_y = kSignMinus * kLookAtMoveValue;
  float move_z = kZero;

  // 注視点の座標計算
  CalcLookAtPosition(move_x, move_y, move_z, Direction::kDown);

  // 新しい座標を取得
  VECTOR pos_camera = GetCameraPosition();
  VECTOR pos_look = GetLookAtPosition();

  // カメラの設定に反映する
  SetCameraPositionAndTarget_UpVecY(pos_camera, pos_look);
}

/// <summary>
/// 注視点を左方向へ移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void DebugCamera::MoveLeftGaze() {

  // 注視点の移動量
  float move_x = kSignMinus * kLookAtMoveValue;
  float move_y = kZero;
  float move_z = kSignMinus * kLookAtMoveValue;

  // 注視点の座標計算
  CalcLookAtPosition(move_x, move_y, move_z, Direction::kLeft);

  // 新しい座標を取得
  VECTOR pos_camera = GetCameraPosition();
  VECTOR pos_look = GetLookAtPosition();

  // カメラの設定に反映する
  SetCameraPositionAndTarget_UpVecY(pos_camera, pos_look);
}

/// <summary>
/// 注視点を右方向へ移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void DebugCamera::MoveRightGaze() {

  // 注視点の移動量
  float move_x = kLookAtMoveValue;
  float move_y = kZero;
  float move_z = kLookAtMoveValue;

  // 注視点の座標計算
  CalcLookAtPosition(move_x, move_y, move_z, Direction::kRight);

  // 新しい座標を取得
  VECTOR pos_camera = GetCameraPosition();
  VECTOR pos_look = GetLookAtPosition();

  // カメラの設定に反映する
  SetCameraPositionAndTarget_UpVecY(pos_camera, pos_look);
}