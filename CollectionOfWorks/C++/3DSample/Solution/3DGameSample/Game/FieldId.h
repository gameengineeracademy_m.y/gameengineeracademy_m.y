﻿#pragma once

/// <summary>
/// フィールドID
/// </summary>
enum class FieldId {
  kFloor,          // 床
  kWall,           // 壁
  kFieldMaxIndex   // フィールドの種類数
};