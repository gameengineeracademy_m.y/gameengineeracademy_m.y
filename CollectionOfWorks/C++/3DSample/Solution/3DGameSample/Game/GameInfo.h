﻿#pragma once

#include <iostream>

/// <summary>
/// ゲーム情報
/// </summary>
class GameInfo {
public:

  /// <summary>
  /// GameInfo インスタンスを作成する
  /// </summary>
  /// <param> 画面解像度 幅 </param>
  /// <param> 画面解像度 高さ </param>
  /// <returns></returns>
  static void CreateGameInfo();

  /// <summary>
  /// GameInfo インスタンスを取得する
  /// </summary>
  /// <param></param>
  /// <returns></returns>
  static GameInfo* GetGameInfoInstance() { return game_info_instance_; }

  /// <summary>
  /// GameInfo インスタンスを破棄する
  /// </summary>
  /// <param></param>
  /// <returns></returns>
  static void ReleaseGameInfo();

  /// <summary>
  /// ゲーム情報を初期化する
  /// </summary>
  /// <param></param>
  /// <returns></returns>
  void InitializeGameInfo();

  /// <summary>
  /// 解像度のXサイズを取得
  /// </summary>
  /// <param></param>
  /// <returns> 解像度のXサイズ </returns>
  int GetResolutionWidth() { return resolution_horizontal_; }

  /// <summary>
  /// 解像度のYサイズを取得
  /// </summary>
  /// <param></param>
  /// <returns> 解像度のYサイズ </returns>
  int GetResolutionHeight() { return resolution_vertical_; }

  /// <summary>
  /// 画面の中心点のX座標を取得
  /// </summary>
  /// <param></param>
  /// <returns> 画像の中心点のX座標 </returns>
  int GetCenterPointX() { return screen_center_pos_x_; }

  /// <summary>
  /// 画面の中心点のY座標を取得
  /// </summary>
  /// <param></param>
  /// <returns> 画像の中心点のY座標 </returns>
  int GetCenterPointY() { return screen_center_pos_y_; }

  /// <summary>
  /// マップのサイズをセットする
  /// </summary>
  /// <param> マップのXサイズ </param>
  /// <param> マップのYサイズ </param>
  /// <param> マップのZサイズ </param>
  /// <returns></returns>
  void SetMapSize(float, float, float);

  /// <summary>
  /// マップサイズ(X方向)を取得
  /// </summary>
  /// <param></param>
  /// <returns> マップサイズ(X方向) </returns>
  float GetMapSizeX() { return map_size_x_; }

  /// <summary>
  /// マップサイズ(Y方向)を取得
  /// </summary>
  /// <param></param>
  /// <returns> マップサイズ(Y方向) </returns>
  float GetMapSizeY() { return map_size_y_; }

  /// <summary>
  /// マップサイズ(Z方向)を取得
  /// </summary>
  /// <param></param>
  /// <returns> マップサイズ(Z方向) </returns>
  float GetMapSizeZ() { return map_size_z_; }

  /// <summary>
  /// マップの左上のX座標を取得
  /// </summary>
  /// <param></param>
  /// <returns> マップの左上のX座標 </returns>
  float GetMapPositionX() { return map_position_x_; }

  /// <summary>
  /// マップの左上のY座標を取得
  /// </summary>
  /// <param></param>
  /// <returns> マップの左上のY座標 </returns>
  float GetMapPositionY() { return map_position_y_; }

  /// <summary>
  /// マップの左上のZ座標を取得
  /// </summary>
  /// <param></param>
  /// <returns> マップの左上のY座標 </returns>
  float GetMapPositionZ() { return map_position_z_; }

  /// <summary>
  /// デバッグ有無を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:デバッグ, false:リリース </returns>
  bool IsDebug() { return is_debug_; }

  /// <summary>
  /// ゲームクリアをセット
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetGameClear() { is_clear_ = true; }

  /// <summary>
  /// ゲームクリア有無を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:クリア, false:失敗 </returns>
  bool GetGameClear() { return is_clear_; }


private:

  /// <summary>
  /// コンストラクタ
  /// </summary>
  /// <param></param>
  /// <returns></returns>
  GameInfo();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param></param>
  /// <returns></returns>
  ~GameInfo();


private:

  /// <summary>
  /// インスタンス (唯一のもの)
  /// </summary>
  static GameInfo* game_info_instance_;

  /// <summary>
  /// 解像度のXサイズ
  /// </summary>
  int resolution_horizontal_;

  /// <summary>
  /// 解像度のYサイズ
  /// </summary>
  int resolution_vertical_;

  /// <summary>
  /// 画面の中心のX座標 X-Y平面
  /// </summary>
  int screen_center_pos_x_;

  /// <summary>
  /// 画面の中心のY座標 X-Y平面
  /// </summary>
  int screen_center_pos_y_;

  /// <summary>
  /// マップサイズ X方向
  /// </summary>
  float map_size_x_;

  /// <summary>
  /// マップサイズ Y方向
  /// </summary>
  float map_size_y_;

  /// <summary>
  /// マップサイズ Z方向
  /// </summary>
  float map_size_z_;

  /// <summary>
  /// 表示位置(マップの左上)のX座標
  /// </summary>
  float map_position_x_;

  /// <summary>
  /// 表示位置(マップの左上)のY座標
  /// </summary>
  float map_position_y_;

  /// <summary>
  /// 表示位置(マップの左上)のZ座標
  /// </summary>
  float map_position_z_;

  /// <summary>
  /// デバッグ有無
  /// </summary>
  bool is_debug_;

  /// <summary>
  /// ゲームクリア有無
  /// </summary>
  bool is_clear_;
};