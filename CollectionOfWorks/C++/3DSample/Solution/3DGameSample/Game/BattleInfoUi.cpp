﻿#include "Game/BattleInfoUi.h"

namespace {

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjustInit = 5;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 透過率 最大値 キー操作背景
  /// </summary>
  const int kAlphaMaxBack = 220;

  /// <summary>
  /// ふた絵 左上座標
  /// </summary>
  const int kPaintLeftPosX = 0;

  /// <summary>
  /// ふた絵 左上座標
  /// </summary>
  const int kPaintLeftPosY = 0;

  /// <summary>
  /// ふた絵 右下座標
  /// </summary>
  const int kPaintRightPosX = 1024;

  /// <summary>
  /// ふた絵 右下座標
  /// </summary>
  const int kPaintRightPosY = 768;

  /// <summary>
  /// カメラ名 表示位置 X座標
  /// </summary>
  const int kDisplayPosX = 15;

  /// <summary>
  /// カメラ名 表示位置 Y座標
  /// </summary>
  const int kCameraNamePosY = 15;

  /// <summary>
  /// カメラ座標 表示位置 Y座標
  /// </summary>
  const int kCameraPosPosY = 45;

  /// <summary>
  /// 注視点座標 表示位置 Y座標
  /// </summary>
  const int kLookPosPosY = 75;

  /// <summary>
  /// プレイヤー座標 表示位置 Y座標
  /// </summary>
  const int kPlayerPosY = 105;

  /// <summary>
  /// 角度 表示位置 Y座標
  /// </summary>
  const int kAnglePosY = 135;

  /// <summary>
  /// 右側表示メニュー 表示位置 X座標
  /// </summary>
  const int kManualDispPosX = 700;

  /// <summary>
  /// 操作説明01 表示位置 Y座標
  /// </summary>
  const int kManual01PosY = 15;

  /// <summary>
  /// 操作説明02 表示位置 Y座標
  /// </summary>
  const int kManual02PosY = 45;

  /// <summary>
  /// 操作説明03 表示位置 Y座標
  /// </summary>
  const int kManual03PosY = 75;

  /// <summary>
  /// 操作説明04 表示位置 Y座標
  /// </summary>
  const int kManual04PosY = 105;

  /// <summary>
  /// 操作説明05 表示位置 Y座標
  /// </summary>
  const int kManual05PosY = 135;

  /// <summary>
  /// 操作説明06 表示位置 Y座標
  /// </summary>
  const int kManual06PosY = 165;

  /// <summary>
  /// 操作説明07 表示位置 Y座標
  /// </summary>
  const int kManual07PosY = 195;

  /// <summary>
  /// 操作説明08 表示位置 Y座標
  /// </summary>
  const int kManual08PosY = 225;

  /// <summary>
  /// カメラ名
  /// </summary>
  const char* kPlayerCamera = "◆ Player Camera ◆";

  /// <summary>
  /// カメラ名 デバッグカメラ
  /// </summary>
  const char* kDebugCamera = "★ Debug Camera ★";

  /// <summary>
  /// カメラ座標
  /// </summary>
  const char* kCameraPos = "Camera Pos：";

  /// <summary>
  /// 注視点座標
  /// </summary>
  const char* kLookPos = "Gaze Pos：";

  /// <summary>
  /// 注視点座標
  /// </summary>
  const char* kPlayerPos = "Player Pos：";

  /// <summary>
  /// 角度
  /// </summary>
  const char* kAngle = "angle(h,v)：";

  /// <summary>
  /// 操作説明ON/OFF
  /// </summary>
  const char* kSwitchManual = "Manual ON/OFF：Esc";

  /// <summary>
  /// カメラ切り替え
  /// </summary>
  const char* kSwitchCamera = "Switch Camera：Left ALT + D";

  /// <summary>
  /// プレイヤー操作 移動
  /// </summary>
  const char* kMovePlayer = "Move Player：WSAD";

  /// <summary>
  /// プレイヤー操作 ジャンプ
  /// </summary>
  const char* kJumpPlayer = "Jump Player：Space";

  /// <summary>
  /// プレイヤーカメラ操作
  /// </summary>
  const char* kOperatePlayerCamera = "Rotate Camera：Cross";

  /// <summary>
  /// ズーム操作
  /// </summary>
  const char* kZoom = "Zoom IN/OUT：Mouse Wheel";

  /// <summary>
  /// 配置初期化
  /// </summary>
  const char* kPlacementInitialize = "Placement Initialize：P";

  /// <summary>
  /// キー操作メニュー切り替え
  /// </summary>
  const char* kSwitchKeyMenu = "KeyMenu ON/OFF：M";

  /// <summary>
  /// デバッグカメラ操作 移動
  /// </summary>
  const char* kMoveDebugCamera = "Move Camera：WSAD";

  /// <summary>
  /// デバッグカメラ操作 上下
  /// </summary>
  const char* kUpDownDebugCamera = "UpDown Camera：Shift + QE";

  /// <summary>
  /// デバッグカメラ操作 回転
  /// </summary>
  const char* kRotateDebugCamera = "Rotate Camera：Shift + WSAD";

  /// <summary>
  /// 文字フォント名
  /// </summary>
  const char* kFontName = "メイリオ";

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSize = 20;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 1;

  /// <summary>
  /// ふた絵 色
  /// </summary>
  const int kPictureColor = GetColor(0, 0, 0);   // 黒色

  /// <summary>
  /// 文字の色
  /// </summary>
  const int kTextForeColor = GetColor(255, 255, 255);   // 白色

  /// <summary>
  /// 10
  /// </summary>
  const int kTen = 10;

  /// <summary>
  /// 1
  /// </summary>
  const int kOne = 1;

  /// <summary>
  /// Wキー(デフォルト) 画像パス
  /// </summary>
  const char* kImagePathWkeyDefault = "Asset/Image/w_key_default.png";

  /// <summary>
  /// Wキー(押下) 画像パス
  /// </summary>
  const char* kImagePathWkeyPress = "Asset/Image/w_key_press.png";

  /// <summary>
  /// Sキー(デフォルト) 画像パス
  /// </summary>
  const char* kImagePathSkeyDefault = "Asset/Image/s_key_default.png";

  /// <summary>
  /// Sキー(押下) 画像パス
  /// </summary>
  const char* kImagePathSkeyPress = "Asset/Image/s_key_press.png";

  /// <summary>
  /// Aキー(デフォルト) 画像パス
  /// </summary>
  const char* kImagePathAkeyDefault = "Asset/Image/a_key_default.png";

  /// <summary>
  /// Aキー(押下) 画像パス
  /// </summary>
  const char* kImagePathAkeyPress = "Asset/Image/a_key_press.png";

  /// <summary>
  /// Dキー(デフォルト) 画像パス
  /// </summary>
  const char* kImagePathDkeyDefault = "Asset/Image/d_key_default.png";

  /// <summary>
  /// Dキー(押下) 画像パス
  /// </summary>
  const char* kImagePathDkeyPress = "Asset/Image/d_key_press.png";

  /// <summary>
  /// 上矢印キー(デフォルト) 画像パス
  /// </summary>
  const char* kImagePathUpkeyDefault = "Asset/Image/up_default.png";

  /// <summary>
  /// 上矢印キー(押下) 画像パス
  /// </summary>
  const char* kImagePathUpkeyPress = "Asset/Image/up_press.png";

  /// <summary>
  /// 下矢印キー(デフォルト) 画像パス
  /// </summary>
  const char* kImagePathDownkeyDefault = "Asset/Image/down_default.png";

  /// <summary>
  /// 下矢印キー(押下) 画像パス
  /// </summary>
  const char* kImagePathDownkeyPress = "Asset/Image/down_press.png";

  /// <summary>
  /// 左矢印キー(デフォルト) 画像パス
  /// </summary>
  const char* kImagePathLeftkeyDefault = "Asset/Image/left_default.png";

  /// <summary>
  /// 左矢印キー(押下) 画像パス
  /// </summary>
  const char* kImagePathLeftkeyPress = "Asset/Image/left_press.png";

  /// <summary>
  /// 右矢印キー(デフォルト) 画像パス
  /// </summary>
  const char* kImagePathRightkeyDefault = "Asset/Image/right_default.png";

  /// <summary>
  /// 右矢印キー(押下) 画像パス
  /// </summary>
  const char* kImagePathRightkeyPress = "Asset/Image/right_press.png";

  /// <summary>
  /// スペースキー(デフォルト) 画像パス
  /// </summary>
  const char* kImagePathSpacekeyDefault = "Asset/Image/space_default.png";

  /// <summary>
  /// スペースキー(押下) 画像パス
  /// </summary>
  const char* kImagePathSpacekeyPress = "Asset/Image/space_press.png";

  /// <summary>
  /// Wキー X座標
  /// </summary>
  const int kWKeyPosX = 110;

  /// <summary>
  /// Wキー Y座標
  /// </summary>
  const int kWKeyPosY = 585;

  /// <summary>
  /// Sキー X座標
  /// </summary>
  const int kSKeyPosX = 110;

  /// <summary>
  /// Sキー Y座標
  /// </summary>
  const int kSKeyPosY = 655;

  /// <summary>
  /// Aキー X座標
  /// </summary>
  const int kAKeyPosX = 40;

  /// <summary>
  /// Aキー Y座標
  /// </summary>
  const int kAKeyPosY = 655;

  /// <summary>
  /// Dキー X座標
  /// </summary>
  const int kDKeyPosX = 180;

  /// <summary>
  /// Dキー Y座標
  /// </summary>
  const int kDKeyPosY = 655;

  /// <summary>
  /// 上矢印キー X座標
  /// </summary>
  const int kUpKeyPosX = 330;

  /// <summary>
  /// 上矢印キー Y座標
  /// </summary>
  const int kUpKeyPosY = 585;

  /// <summary>
  /// 下矢印キー X座標
  /// </summary>
  const int kDownKeyPosX = 330;

  /// <summary>
  /// 下矢印キー Y座標
  /// </summary>
  const int kDownKeyPosY = 655;

  /// <summary>
  /// 左矢印キー X座標
  /// </summary>
  const int kLeftKeyPosX = 260;

  /// <summary>
  /// 左矢印キー Y座標
  /// </summary>
  const int kLeftKeyPosY = 655;

  /// <summary>
  /// 右矢印キー X座標
  /// </summary>
  const int kRightKeyPosX = 400;

  /// <summary>
  /// 右矢印キー Y座標
  /// </summary>
  const int kRightKeyPosY = 655;

  /// <summary>
  /// スペースキー X座標
  /// </summary>
  const int kSpaceKeyPosX = 220;

  /// <summary>
  /// スペースキー Y座標
  /// </summary>
  const int kSpaceKeyPosY = 725;

  /// <summary>
  /// 拡大率 1.0倍
  /// </summary>
  const float kExRate10 = 1.0f;

  /// <summary>
  /// 角度
  /// </summary>
  const float kAngle0 = 0.0f;

  /// <summary>
  /// キー操作 背景左上 X座標
  /// </summary>
  const int kKeyBackLeftPosX = 5;

  /// <summary>
  /// キー操作 背景左上 Y座標
  /// </summary>
  const int kKeyBackLeftPosY = 545;

  /// <summary>
  /// キー操作 背景右下 X座標
  /// </summary>
  const int kKeyBackRightPosX = 435;

  /// <summary>
  /// キー操作 背景右下 Y座標
  /// </summary>
  const int kKeyBackRightPosY = 763;

  /// <summary>
  /// キー操作 背景 色
  /// </summary>
  const int kKeyBackColor = GetColor(75, 75, 75);

  /// <summary>
  /// フレーム表示座標
  /// </summary>
  const int kFrameTopPosX = 0;

  /// <summary>
  /// フレーム表示座標
  /// </summary>
  const int kFrameTopPosY = 0;

  /// <summary>
  /// フレーム枠　太さ
  /// </summary>
  const int kFrameThick = 10;

  /// <summary>
  /// フレーム枠 色
  /// </summary>
  const int kFrameColor = GetColor(255, 0, 0);
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
BattleInfoUi::BattleInfoUi(BattleInfoUiEventInterface& event_interface)
  : Task(TaskId::kBattleInfoUi)
  , current_phase_(PhaseType::kInitialize)
  , event_interface_(event_interface)
  , pos_camera_name_()
  , pos_camera_pos_()
  , pos_look_()
  , pos_player_pos_()
  , pos_angle_()
  , pos_manual_01_()
  , pos_manual_02_()
  , pos_manual_03_()
  , pos_manual_04_()
  , pos_manual_05_()
  , pos_manual_06_()
  , pos_manual_07_()
  , pos_manual_08_()
  , camera_name_()
  , camera_pos_value_()
  , look_pos_value_()
  , player_pos_value_()
  , angle_value_()
  , manual_01_()
  , manual_02_()
  , manual_03_()
  , manual_04_()
  , manual_05_()
  , manual_06_()
  , manual_07_()
  , manual_08_()
  , key_list_()
  , is_disp_key_(true)
  , is_disp_frame_(false)
  , is_disp_manual_(true)
  , font_handle_()
  , alpha_(0)
  , alpha_front_(0)
  , alpha_key_back_(0) {

  // コンソールに出力
  std::cout << "BattleInfoUi コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
BattleInfoUi::~BattleInfoUi() {

  // コンソールに出力
  std::cout << "~BattleInfoUi デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleInfoUi::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kStartWait: {
    //------------------------------
    //待機フェーズ
    //------------------------------
    break;
  }
  case PhaseType::kInitialize: {
    //------------------------------
    //初期化処理フェーズ
    //------------------------------
    Initialize();
    // 現在のフェーズを「起動待機」に変更
    ChangeCurrentPhase(PhaseType::kStartWait);
    break;
  }
  case PhaseType::kStartUp: {
    //------------------------------
    //起動フェーズ
    //------------------------------
    // フェードイン処理完了後、現在のフェーズを「処理中」に変更
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //------------------------------
    //処理中フェーズ
    //------------------------------
    Process(process_time);
    break;
  }
  case PhaseType::kBeforeFinalize: {
    //------------------------------
    //終了処理前フェーズ
    //------------------------------
    // フェードアウト処理完了後、現在のフェーズを「終了処理」に変更
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更する
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //------------------------------
    //終了処理フェーズ
    //------------------------------
    // 現在のフェーズを終了処理済みに変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleInfoUi::Render() {

  switch (current_phase_) {
  case PhaseType::kStartUp:
  case PhaseType::kProcess:
  case PhaseType::kBeforeFinalize: {

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_front_);

    DrawBox(kPaintLeftPosX, kPaintLeftPosY, kPaintRightPosX, kPaintRightPosX, kPictureColor, true);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

    //------------------------------------------------
    //サンプル内 位置情報
    //------------------------------------------------
    DrawStringToHandle(pos_camera_name_.x, pos_camera_name_.y, camera_name_.c_str(),
                       kTextForeColor, font_handle_.at(static_cast<int>(FontSize::k20)) );
    DrawStringToHandle(pos_camera_pos_.x, pos_camera_pos_.y, camera_pos_value_.c_str(),
                       kTextForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
    DrawStringToHandle(pos_look_.x, pos_look_.y, look_pos_value_.c_str(),
                       kTextForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
    DrawStringToHandle(pos_player_pos_.x, pos_player_pos_.y, player_pos_value_.c_str(),
                       kTextForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
    DrawStringToHandle(pos_angle_.x, pos_angle_.y, angle_value_.c_str(),
                       kTextForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));

    //------------------------------------------------
    //操作説明 位置情報
    //------------------------------------------------
    DrawStringToHandle(pos_manual_01_.x, pos_manual_01_.y, manual_01_.c_str(),
                       kTextForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));

    bool is_show = IsShowOperationManual();
    if (is_show) {
      DrawStringToHandle(pos_manual_02_.x, pos_manual_02_.y, manual_02_.c_str(),
                         kTextForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
      DrawStringToHandle(pos_manual_03_.x, pos_manual_03_.y, manual_03_.c_str(),
                         kTextForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
      DrawStringToHandle(pos_manual_04_.x, pos_manual_04_.y, manual_04_.c_str(),
                         kTextForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
      DrawStringToHandle(pos_manual_05_.x, pos_manual_05_.y, manual_05_.c_str(),
                         kTextForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
      DrawStringToHandle(pos_manual_06_.x, pos_manual_06_.y, manual_06_.c_str(),
                         kTextForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
      DrawStringToHandle(pos_manual_07_.x, pos_manual_07_.y, manual_07_.c_str(),
                         kTextForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
      DrawStringToHandle(pos_manual_08_.x, pos_manual_08_.y, manual_08_.c_str(),
                         kTextForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
    }

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    is_show = IsShowFrame();
    if (is_show) {
      GameInfo* game_info = GameInfo::GetGameInfoInstance();
      if (game_info != nullptr) {
        int window_width = game_info->GetResolutionWidth();
        int window_height = game_info->GetResolutionHeight();
        // 画面の枠
        DrawLine(kFrameTopPosX, kFrameTopPosY, kFrameTopPosX, window_height, kFrameColor, kFrameThick);
        DrawLine(kFrameTopPosX, kFrameTopPosY, window_width, kFrameTopPosY, kFrameColor, kFrameThick);
        DrawLine(window_width, kFrameTopPosY, window_width, window_height, kFrameColor, kFrameThick);
        DrawLine(kFrameTopPosX, window_height, window_width, window_height, kFrameColor, kFrameThick);
      }
    }

    is_show = IsShowDisplay();
    if (is_show) {
      // 描画ブレンドモードをアルファブレンドに設定
      SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_key_back_);

      // 背景ボード
      DrawBox(kKeyBackLeftPosX, kKeyBackLeftPosY, kKeyBackRightPosX, kKeyBackRightPosY, kKeyBackColor, true);
      // キー
      for (auto key : key_list_) {
        if (key.second == nullptr) {
          return;
        }
        key.second->Render();
      }

      // 描画ブレンドモードをノーブレンドにする
      SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
    }
    // キー
    for (auto key : key_list_) {
      if (key.second == nullptr) {
        return;
      }
      // キーを押されていない状態にする
      key.second->ChangeKeyState(Key::KeyState::kDefault);
    }    
    break;
  }
  }
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleInfoUi::Initialize() {

  // 透過率
  alpha_ = kAlphaMin;
  alpha_front_ = kAlphaMax;

  //---------------------------------------
  //カメラ情報 (座標・角度)
  //---------------------------------------
  // 使用カメラ表示位置
  pos_camera_name_.x = kDisplayPosX;
  pos_camera_name_.y = kCameraNamePosY;
  // カメラ座標 表示位置
  pos_camera_pos_.x = kDisplayPosX;
  pos_camera_pos_.y = kCameraPosPosY;
  // 注視点座標 表示位置
  pos_look_.x = kDisplayPosX;
  pos_look_.y = kLookPosPosY;
  // プレイヤー座標 表示位置
  pos_player_pos_.x = kDisplayPosX;
  pos_player_pos_.y = kPlayerPosY;
  // 角度 表示位置
  pos_angle_.x = kDisplayPosX;
  pos_angle_.y = kAnglePosY;

  // テキスト初期化
  camera_name_ = kDebugCamera;
  camera_pos_value_ = "";
  look_pos_value_ = "";
  player_pos_value_ = "";
  angle_value_ = "";

  //---------------------------------------
  // 操作方法 表示位置
  //---------------------------------------
  // 操作説明文 その1
  pos_manual_01_.x = kManualDispPosX;
  pos_manual_01_.y = kManual01PosY;
  manual_01_ = "";
  // 操作説明文 その2
  pos_manual_02_.x = kManualDispPosX;
  pos_manual_02_.y = kManual02PosY;
  manual_02_ = "";
  // 操作説明文 その3
  pos_manual_03_.x = kManualDispPosX;
  pos_manual_03_.y = kManual03PosY;
  manual_03_ = "";
  // 操作説明文 その4
  pos_manual_04_.x = kManualDispPosX;
  pos_manual_04_.y = kManual04PosY;
  manual_04_ = "";
  // 操作説明文 その5
  pos_manual_05_.x = kManualDispPosX;
  pos_manual_05_.y = kManual05PosY;
  manual_05_ = "";
  // 操作説明文 その6
  pos_manual_06_.x = kManualDispPosX;
  pos_manual_06_.y = kManual06PosY;
  manual_06_ = "";
  // 操作説明文 その7
  pos_manual_07_.x = kManualDispPosX;
  pos_manual_07_.y = kManual07PosY;
  manual_07_ = "";
  // 操作説明文 その8
  pos_manual_08_.x = kManualDispPosX;
  pos_manual_08_.y = kManual08PosY;
  manual_08_ = "";

  // キーの生成
  CreateKey();

  // フォント情報生成
  FontInfo* font_info = FontInfo::GetFontInfoInstance();
  if (font_info == nullptr) {
    std::cout << "TitleLevel InitializePhase FontInfoインスタンス無し" << std::endl;
    return false;
  }
  int font_handle = font_info->GetFontInfo(kFontName, kFontSize, kFontWidth, DX_FONTTYPE_NORMAL);
  font_handle_.push_back(font_handle);

  return true;
}

/// <summary>
/// 処理中処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void BattleInfoUi::Process(float process_time) {

  float x_pos = 0.0f;
  float y_pos = 0.0f;
  float z_pos = 0.0f;
  float angle_horizon = 0.0f;
  float angle_vertical = 0.0f;

  // カメラの座標を取得
  event_interface_.OnGetCameraPosition(x_pos, y_pos, z_pos);
  // カメラの表示位置をセット
  SetCameraPos(x_pos, y_pos, z_pos);

  // 注視点の座標を取得
  event_interface_.OnGetLookAtPosition(x_pos, y_pos, z_pos);
  // 注視点の座標をセット
  SetLookAtPos(x_pos, y_pos, z_pos);

  // プレイヤーの座標を取得
  event_interface_.OnGetPlayerPosition(x_pos, y_pos, z_pos);
  // 注視点の座標をセット
  SetPlayerPos(x_pos, y_pos, z_pos);

  // 角度を取得
  event_interface_.OnGetAngle(angle_horizon, angle_vertical);
  // 角度をセット
  SetAngle(angle_horizon, angle_vertical);

  // キー
  for (auto key : key_list_) {
    if (key.second == nullptr) {
      return;
    }
    key.second->Update(process_time);
  }
}

/// <summary>
/// カメラ座標セット
/// </summary>
/// <param name="x_pos"> X座標 </param>
/// <param name="y_pos"> Y座標 </param>
/// <param name="z_pos"> Z座標 </param>
/// <returns></returns>
void BattleInfoUi::SetCameraPos(float x_pos, float y_pos, float z_pos) {

  std::ostringstream x_pos_text;
  std::ostringstream y_pos_text;
  std::ostringstream z_pos_text;

  // 小数第1位まで表示する
  x_pos_text << std::fixed << std::setprecision(kOne) << ExecuteRound(x_pos, kOne);
  y_pos_text << std::fixed << std::setprecision(kOne) << ExecuteRound(y_pos, kOne);
  z_pos_text << std::fixed << std::setprecision(kOne) << ExecuteRound(z_pos, kOne);

  camera_pos_value_ = kCameraPos;
  camera_pos_value_ += " (" + x_pos_text.str() + " , "
                            + y_pos_text.str() + " , "
                            + z_pos_text.str() + ")";
}

/// <summary>
/// 注視点座標セット
/// </summary>
/// <param name="x_pos"> X座標 </param>
/// <param name="y_pos"> Y座標 </param>
/// <param name="z_pos"> Z座標 </param>
/// <returns></returns>
void BattleInfoUi::SetLookAtPos(float x_pos, float y_pos, float z_pos) {

  std::ostringstream x_pos_text;
  std::ostringstream y_pos_text;
  std::ostringstream z_pos_text;

  // 小数第1位まで表示する
  x_pos_text << std::fixed << std::setprecision(kOne) << ExecuteRound(x_pos, kOne);
  y_pos_text << std::fixed << std::setprecision(kOne) << ExecuteRound(y_pos, kOne);
  z_pos_text << std::fixed << std::setprecision(kOne) << ExecuteRound(z_pos, kOne);

  look_pos_value_ = kLookPos;
  look_pos_value_ += " (" + x_pos_text.str() + " , "
                          + y_pos_text.str() + " , "
                          + z_pos_text.str() + ")";
}

/// <summary>
/// プレイヤー座標セット
/// </summary>
/// <param name="x_pos"> X座標 </param>
/// <param name="y_pos"> Y座標 </param>
/// <param name="z_pos"> Z座標 </param>
/// <returns></returns>
void BattleInfoUi::SetPlayerPos(float x_pos, float y_pos, float z_pos) {

  std::ostringstream x_pos_text;
  std::ostringstream y_pos_text;
  std::ostringstream z_pos_text;

  // 小数第1位まで表示する
  x_pos_text << std::fixed << std::setprecision(kOne) << ExecuteRound(x_pos, kOne);
  y_pos_text << std::fixed << std::setprecision(kOne) << ExecuteRound(y_pos, kOne);
  z_pos_text << std::fixed << std::setprecision(kOne) << ExecuteRound(z_pos, kOne);

  player_pos_value_ = kPlayerPos;
  player_pos_value_ += " (" + x_pos_text.str() + " , "
                            + y_pos_text.str() + " , "
                            + z_pos_text.str() + ")";
}

/// <summary>
/// 角度セット
/// </summary>
/// <param name="angle_h"> 水平角度 </param>
/// <param name="angle_v"> 垂直角度 </param>
/// <returns></returns>
void BattleInfoUi::SetAngle(float angle_h, float angle_v) {

  std::ostringstream angle_h_text;
  std::ostringstream angle_v_text;

  // 小数第1位まで表示する
  angle_h_text << std::fixed << std::setprecision(kOne) << ExecuteRound(angle_h, kOne);
  angle_v_text << std::fixed << std::setprecision(kOne) << ExecuteRound(angle_v, kOne);

  angle_value_ = kAngle;
  angle_value_ += " (" + angle_h_text.str() + " , "
                       + angle_v_text.str() + ")";
}

/// <summary>
/// プレイヤーマニュアルのセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleInfoUi::SetPlayerManual() {

  manual_01_ = kSwitchManual;
  manual_02_ = kSwitchCamera;
  manual_03_ = kMovePlayer;
  manual_04_ = kJumpPlayer;
  manual_05_ = kOperatePlayerCamera;
  manual_06_ = kZoom;
  manual_07_ = kPlacementInitialize;
  manual_08_ = kSwitchKeyMenu;
}

/// <summary>
/// デバッグマニュアルのセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleInfoUi::SetDebugManual() {

  manual_01_ = kSwitchManual;
  manual_02_ = kSwitchCamera;
  manual_03_ = kMoveDebugCamera;
  manual_04_ = kUpDownDebugCamera;
  manual_05_ = kRotateDebugCamera;
  manual_06_ = kZoom;
  manual_07_ = kPlacementInitialize;
  manual_08_ = "";
}

/// <summary>
/// 小数第n位四捨五入
/// </summary>
/// <param name=""> 対象の値 </param>
/// <param name=""> 表示する位 </param>
/// <returns> 小数値 </returns>
float BattleInfoUi::ExecuteRound(float number, int n) {

  //四捨五入したい値を10の(n-1)乗倍する。
  number = number * static_cast<float>(std::pow(kTen, n - kOne));
  //小数点以下を四捨五入する。
  number = std::round(number);
  //10の(n-1)乗で割る。
  number /= static_cast<float>(std::pow(kTen, n - kOne));

  return number;
}

/// <summary>
/// 透過率変更処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleInfoUi::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率の調整
  switch (current_phase_) {
  case PhaseType::kStartUp: {
    // 起動フェーズ
    alpha_ += kAlphaAdjustInit;
    alpha_key_back_ += kAlphaAdjustInit;
    alpha_front_ -= kAlphaAdjustInit;
    break;
  }
  case PhaseType::kBeforeFinalize: {
    // 終了処理前フェーズ
    alpha_ -= kAlphaAdjustInit;
    alpha_key_back_ -= kAlphaAdjustInit;
    alpha_front_ += kAlphaAdjustInit;
    break;
  }
  }

  if (alpha_key_back_ >= kAlphaMaxBack) {
    alpha_key_back_ = kAlphaMaxBack;
  }
  else if (alpha_key_back_ <= kAlphaMin) {
    alpha_key_back_ = kAlphaMin;
  }

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= kAlphaMax) {
    alpha_ = kAlphaMax;
    alpha_front_ = kAlphaMin;
    is_finish = true;
  }
  else if (alpha_ <= kAlphaMin) {
    alpha_ = kAlphaMin;
    alpha_front_ = kAlphaMax;
    is_finish = true;
  }

  return is_finish;
}

/// <summary>
/// キーの生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleInfoUi::CreateKey() {

  Key* key = nullptr;
  int x_pos = 0;
  int y_pos = 0;
  std::string image_path_01;
  std::string image_path_02;

  for (int i = 0; i < static_cast<int>(Key::KeyType::kTypeMaxIndex); ++i) {

    Key::KeyType key_type = static_cast<Key::KeyType>(i);
    switch (key_type) {
    case Key::KeyType::kW: {
      // Wキー
      image_path_01 = kImagePathWkeyDefault;
      image_path_02 = kImagePathWkeyPress;
      x_pos = kWKeyPosX;
      y_pos = kWKeyPosY;
      break;
    }
    case Key::KeyType::kS: {
      // Sキー
      image_path_01 = kImagePathSkeyDefault;
      image_path_02 = kImagePathSkeyPress;
      x_pos = kSKeyPosX;
      y_pos = kSKeyPosY;
      break;
    }
    case Key::KeyType::kA: {
      // Aキー
      image_path_01 = kImagePathAkeyDefault;
      image_path_02 = kImagePathAkeyPress;
      x_pos = kAKeyPosX;
      y_pos = kAKeyPosY;
      break;
    }
    case Key::KeyType::kD: {
      // Dキー
      image_path_01 = kImagePathDkeyDefault;
      image_path_02 = kImagePathDkeyPress;
      x_pos = kDKeyPosX;
      y_pos = kDKeyPosY;
      break;
    }
    case Key::KeyType::kUp: {
      // 上矢印キー
      image_path_01 = kImagePathUpkeyDefault;
      image_path_02 = kImagePathUpkeyPress;
      x_pos = kUpKeyPosX;
      y_pos = kUpKeyPosY;
      break;
    }
    case Key::KeyType::kDown: {
      // 下矢印キー
      image_path_01 = kImagePathDownkeyDefault;
      image_path_02 = kImagePathDownkeyPress;
      x_pos = kDownKeyPosX;
      y_pos = kDownKeyPosY;
      break;
    }
    case Key::KeyType::kLeft: {
      // 左矢印キー
      image_path_01 = kImagePathLeftkeyDefault;
      image_path_02 = kImagePathLeftkeyPress;
      x_pos = kLeftKeyPosX;
      y_pos = kLeftKeyPosY;
      break;
    }
    case Key::KeyType::kRight: {
      // 右矢印キー
      image_path_01 = kImagePathRightkeyDefault;
      image_path_02 = kImagePathRightkeyPress;
      x_pos = kRightKeyPosX;
      y_pos = kRightKeyPosY;
      break;
    }
    case Key::KeyType::kSpace: {
      // スペースキー
      image_path_01 = kImagePathSpacekeyDefault;
      image_path_02 = kImagePathSpacekeyPress;
      x_pos = kSpaceKeyPosX;
      y_pos = kSpaceKeyPosY;
      break;
    }
    }

    key = new Key(key_type);
    int graphic_handle_01 = LoadGraph(image_path_01.c_str());
    int graphic_handle_02 = LoadGraph(image_path_02.c_str());
    key->SetImageHandle(Key::KeyState::kDefault, graphic_handle_01);
    key->SetImageHandle(Key::KeyState::kPress, graphic_handle_02);
    key->SetKeyPosition(x_pos, y_pos);
    key->SetExRate(kExRate10);
    key->SetAngle(kAngle0);
    key_list_[key_type] = key;
  }
}

/// <summary>
/// キーを押していることにする
/// </summary>
/// <param name="key_type"> キーの種類 </param>
/// <returns></returns>
void BattleInfoUi::SetPressKey(Key::KeyType key_type) {

  Key* key = key_list_[key_type];
  if (key == nullptr) {
    return;
  }
  key->ChangeKeyState(Key::KeyState::kPress);
}

/// <summary>
/// キーの破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleInfoUi::DisposeKey() {

  std::vector<Key::KeyType> key_type;

  for (auto key : key_list_) {
    if (key.second == nullptr) {
      continue;
    }
    // 画像リソースの破棄
    DeleteGraph(key.second->GetImageHandle(Key::KeyState::kDefault));
    DeleteGraph(key.second->GetImageHandle(Key::KeyState::kPress));
    // キーの種類を格納
    key_type.push_back(key.first);
  }

  Key* key = nullptr;
  for (int i = 0; i < static_cast<int>(key_type.size()); ++i) {
    key = key_list_[key_type.at(i)];
    delete key;
    key = nullptr;
    key_list_.erase(key_type.at(i));
  }
}

/// <summary>
/// キーの現在のフェーズの変更
/// </summary>
/// <param name=""> フェーズの種類 </param>
/// <returns></returns>
void BattleInfoUi::ChangeKeyCurrentPhase(Key::PhaseType phase_type) {

  for (auto key : key_list_) {
    if (key.second == nullptr) {
      continue;
    }
    key.second->ChangeCurrentPhase(phase_type);
  }
}