﻿#pragma once

#include <iostream>

/// <summary>
/// プレイヤーコントローライベントインターフェース
/// </summary>
class PlayerControllerEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  PlayerControllerEventInterface() {

    // コンソールに出力
    std::cout << "PlayerControllerEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~PlayerControllerEventInterface() {

    // コンソールに出力
    std::cout << "~PlayerControllerEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// Wキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushPlayerControllerWKey() = 0;

  /// <summary>
  /// Sキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushPlayerControllerSKey() = 0;

  /// <summary>
  /// Aキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushPlayerControllerAKey() = 0;

  /// <summary>
  /// Dキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushPlayerControllerDKey() = 0;

  /// <summary>
  /// W + Aキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushPlayerControllerWAKey() = 0;

  /// <summary>
  /// W + Dキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushPlayerControllerWDKey() = 0;

  /// <summary>
  /// S + Aキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushPlayerControllerSAKey() = 0;

  /// <summary>
  /// S + Dキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushPlayerControllerSDKey() = 0;

  /// <summary>
  /// Spaceキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushPlayerControllerSpaceKey() = 0;

  /// <summary>
  /// Escキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushEscapeKey() = 0;

  /// <summary>
  /// Pキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushPlayerControllerPKey() = 0;

  /// <summary>
  /// Mキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushPlayerControllerMKey() = 0;

  /// <summary>
  /// Nキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushPlayerControllerNKey() = 0;


private:
};