﻿#include "Game/GameInfo.h"

namespace {

  /// <summary>
  /// 解像度 横方向
  /// </summary>
  const int kResolutionHorizontalValue = 1024;

  /// <summary>
  /// 解像度 縦方向
  /// </summary>
  const int kResolutionVerticalValue = 768;

  /// <summary>
  /// 半分
  /// </summary>
  const int kValueToHalve = 2;

  /// <summary>
  /// X座標 中心座標
  /// </summary>
  const float kCenterPositionX = 0.0f;

  /// <summary>
  /// Y座標 中心座標
  /// </summary>
  const float kCenterPositionY = 0.0f;

  /// <summary>
  /// Z座標 中心座標
  /// </summary>
  const float kCenterPositionZ = 0.0f;
}

//-------------------------------
// データメンバの定義 
//-------------------------------
/// <summary>
/// ゲーム情報のインスタンス
/// </summary>
GameInfo* GameInfo::game_info_instance_ = nullptr;

/// <summary>
/// コンストラクタ
/// </summary>
/// <param></param>
/// <returns></returns>
GameInfo::GameInfo()
  : resolution_horizontal_(0)
  , resolution_vertical_(0)
  , screen_center_pos_x_(0)
  , screen_center_pos_y_(0)
  , map_size_x_(0.0f)
  , map_size_y_(0.0f)
  , map_size_z_(0.0f)
  , map_position_x_(0.0f)
  , map_position_y_(0.0f)
  , map_position_z_(0.0f)
  , is_debug_(false)
  , is_clear_(false) {

}

/// <summary>
/// デストラクタ
/// </summary>
/// <param></param>
/// <returns></returns>
GameInfo::~GameInfo() {

}

/// <summary>
/// GameInfo インスタンスを作成する
/// </summary>
/// <param></param>
/// <returns></returns>
void GameInfo::CreateGameInfo() {

  if (game_info_instance_ == nullptr) {
    game_info_instance_ = new GameInfo;
  }
}

/// <summary>
/// GameInfo インスタンスを解放する
/// </summary>
/// <param></param>
/// <returns></returns>
void GameInfo::ReleaseGameInfo() {

  if (game_info_instance_ != nullptr) {
    delete game_info_instance_;
    game_info_instance_ = nullptr;
  }
}

/// <summary>
/// ゲーム情報を初期化する
/// </summary>
/// <param></param>
/// <returns></returns>
void GameInfo::InitializeGameInfo() {

  // 解像度の設定
  resolution_horizontal_ = kResolutionHorizontalValue;
  resolution_vertical_ = kResolutionVerticalValue;

  // 画面の中心を計算から求める
  screen_center_pos_x_ = resolution_horizontal_ / kValueToHalve;
  screen_center_pos_y_ = resolution_vertical_ / kValueToHalve;

  // デバッグで起動されているかどうか
#ifdef _DEBUG
  is_debug_ = true;
#else
  is_debug_ = false;
#endif
}

/// <summary>
/// マップのサイズをセットする
/// </summary>
/// <param> マップのXサイズ </param>
/// <param> マップのYサイズ </param>
/// <returns></returns>
void GameInfo::SetMapSize(float map_size_x, float map_size_y, float map_size_z) {

  // マップサイズをセット
  map_size_x_ = map_size_x;
  map_size_y_ = map_size_y;
  map_size_z_ = map_size_z;

  // マップの端の座標を取得
  map_position_x_ = kCenterPositionX - (map_size_x_ / kValueToHalve);
  map_position_y_ = kCenterPositionY;
  map_position_z_ = kCenterPositionZ - (map_size_z_ / kValueToHalve);
}