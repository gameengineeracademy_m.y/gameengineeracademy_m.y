﻿#pragma once

#include <iostream>

/// <summary>
/// バトルUI情報イベントインターフェース
/// </summary>
class BattleInfoUiEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  BattleInfoUiEventInterface() {

    // コンソールに出力
    std::cout << "BattleInfoUiEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~BattleInfoUiEventInterface() {

    // コンソールに出力
    std::cout << "~BattleInfoUiEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// カメラ座標取得
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <param name=""> Z座標 </param>
  /// <returns></returns>
  virtual void OnGetCameraPosition(float&, float&, float&) = 0;

  /// <summary>
  /// 注視点座標取得
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <param name=""> Z座標 </param>
  /// <returns></returns>
  virtual void OnGetLookAtPosition(float&, float&, float&) = 0;

  /// <summary>
  /// プレイヤー座標取得
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <param name=""> Z座標 </param>
  /// <returns></returns>
  virtual void OnGetPlayerPosition(float&, float&, float&) = 0;

  /// <summary>
  /// 角度取得
  /// </summary>
  /// <param name=""> 水平角度 </param>
  /// <param name=""> 垂直角度  </param>
  /// <returns></returns>
  virtual void OnGetAngle(float&, float&) = 0;


private:

};