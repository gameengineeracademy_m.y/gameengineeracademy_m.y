﻿#pragma once

#include <iostream>

/// <summary>
/// プレイヤーイベントインターフェース
/// </summary>
class PlayerEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  PlayerEventInterface() {

    // コンソールに出力
    std::cout << "PlayerEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~PlayerEventInterface() {

    // コンソールに出力
    std::cout << "~PlayerEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// 床との接触判定処理
  /// </summary>
  /// <param name=""> 地面の高さ座標 </param>
  /// <returns> true:接地している, false:接地していない </returns>
  virtual bool OnCollisionFloor(float&) = 0;

  /// <summary>
  /// カメラの注視点移動処理
  /// </summary>
  /// <param name=""> プレイヤー移動後位置 </param>
  /// <returns></returns>
  virtual void OnMoveCameraLookAtPos(float) = 0;

  /// <summary>
  /// カメラの注視点をプレイヤーに戻す
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnResetCameraLookAtPos() = 0;


private:
};