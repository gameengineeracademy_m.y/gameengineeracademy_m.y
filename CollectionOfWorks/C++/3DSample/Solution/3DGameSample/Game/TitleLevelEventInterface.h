﻿#pragma once

#include "System/MenuControllerEventInterface.h"

/// <summary>
/// タイトルレベルイベントインターフェース
/// </summary>
class TitleLevelEventInterface : public MenuControllerEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  TitleLevelEventInterface() {

    // コンソールに出力
    std::cout << "TitleLevelEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~TitleLevelEventInterface() {

    // コンソールに出力
    std::cout << "~TitleLevelEventInterface デストラクタ" << std::endl;
  }


private:


};