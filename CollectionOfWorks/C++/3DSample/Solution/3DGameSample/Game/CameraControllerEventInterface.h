﻿#pragma once

#include <iostream>

/// <summary>
/// カメラコントローライベントインターフェース
/// </summary>
class CameraControllerEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  CameraControllerEventInterface() {

    // コンソールに出力
    std::cout << "CameraControllerEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~CameraControllerEventInterface() {

    // コンソールに出力
    std::cout << "~CameraControllerEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// マウスホイールを奥に回す
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnRotateBackWheel() = 0;

  /// <summary>
  /// マウスホイールを手前に回す
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnRotateFrontWheel() = 0;

  /// <summary>
  /// 上矢印キー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushUpKey() = 0;

  /// <summary>
  /// 下矢印キー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushDownKey() = 0;

  /// <summary>
  /// 右矢印キー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushRightKey() = 0;

  /// <summary>
  /// 左矢印キー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushLeftKey() = 0;

  /// <summary>
  /// Wキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushWKey() = 0;

  /// <summary>
  /// Sキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushSKey() = 0;

  /// <summary>
  /// Aキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushAKey() = 0;

  /// <summary>
  /// Dキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushDKey() = 0;

  /// <summary>
  /// Shift + Wキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushShiftWKey() = 0;

  /// <summary>
  /// Shift + Sキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushShiftSKey() = 0;

  /// <summary>
  /// Shift + Aキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushShiftAKey() = 0;

  /// <summary>
  /// Shift + Dキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushShiftDKey() = 0;

  /// <summary>
  /// Shift + Qキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushShiftQKey() = 0;

  /// <summary>
  /// Shift + Eキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushShiftEKey() = 0;

  /// <summary>
  /// Alt + Dキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushAltDKey() = 0;

  /// <summary>
  /// Escキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushEscapeKey() = 0;

  /// <summary>
  /// Iキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushIKey() = 0;

  /// <summary>
  /// Pキー押下
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushPKey() = 0;


private:

};