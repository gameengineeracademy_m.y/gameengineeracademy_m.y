﻿#pragma once

#include "DxLib.h"
#include "System/Level.h"
#include "System/FontInfo.h"
#include "System/MenuController.h"
#include "Game/GameInfo.h"
#include "Game/TitleLevelEventInterface.h"
#include <vector>

/// <summary>
/// タイトルレベル
/// </summary>
/// <remarks>
/// タイトル画面のタスク
/// </remarks>
class TitleLevel : public Level, public TitleLevelEventInterface {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kNone,               // 何もしないフェーズ
    kStartUp,            // 起動フェーズ
    kProcess,            // 処理中フェーズ
    kBeforeTransition,   // レベル遷移前フェーズ
    kLevelTransition,    // レベル遷移フェーズ
    kFinalize,           // 終了処理フェーズ
    kFinalized,          // 終了処理済みフェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

  /// <summary>
  /// 終了フェーズの種類
  /// </summary>
  enum class FinalizePhaseType {
    kUnloadTask,         // タスクを降ろすフェーズ
    kDisposeTask,        // タスクを破棄するフェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

  /// <summary>
  /// フォントサイズ
  /// </summary>
  enum class FontSize {
    k45,                 // サイズ45
    kPhaseMaxIndex       // フェーズ項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> タスクID </param>
  /// <param name=""> タスクマネージャ </param>
  /// <returns></returns>
  TitleLevel(TaskId, TaskManager&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~TitleLevel();

  /// <summary>
  /// Enterキーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushMenuControllerEnterKey() override;

  /// <summary>
  /// Escapeキーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushMenuControllerEscapeKey() override;


protected:

  /// <summary>
  /// 初期化処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool InitializePhase(float) override;

  /// <summary>
  /// 毎フレームの処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool UpdatePhase(float) override;

  /// <summary>
  /// 毎フレームの描画処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void RenderPhase() override;

  /// <summary>
  /// 終了処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool FinishPhase(float) override;


private:

  /// <summary>
  /// テキスト表示位置 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool InitializeDispTextPos();

  /// <summary>
  /// メニューコントローラ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool InitializeMenuController();

  /// <summary>
  /// 現在のフェーズの変更
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; };

  /// <summary>
  /// 終了処理フェーズの変更
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeFinalizePhase(FinalizePhaseType finalize_phase) { finalize_phase_ = finalize_phase; };

  /// <summary>
  /// 透過率変更処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AdjustAlphaValue();


private:

  /// <summary>
  /// 表示座標
  /// </summary>
  struct Pos {
    int x;
    int y;
  };

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 終了フェーズの種類
  /// </summary>
  FinalizePhaseType finalize_phase_;

  /// <summary>
  /// メニューコントローラ
  /// </summary>
  MenuController* menu_controller_;

  /// <summary>
  /// タイトル 表示位置
  /// </summary>
  Pos title_pos_;

  /// <summary>
  /// 遷移方法 表示位置
  /// </summary>
  Pos guide_pos_;

  /// <summary>
  /// フォントハンドル
  /// </summary>
  std::vector<int> font_handle_;

  /// <summary>
  /// 累積処理時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// ゲーム終了フラグ
  /// </summary>
  bool is_game_end_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_;
};