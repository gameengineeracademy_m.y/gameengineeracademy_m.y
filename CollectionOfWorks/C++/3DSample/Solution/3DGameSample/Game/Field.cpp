﻿#include "Game/Field.h"

namespace {

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Field::Field()
  : field_id_()
  , model_handle_(0)
  , graphic_handle_(0)
  , pos_()
  , model_data_()
  , corner_() {

  // コンソールに出力
  std::cout << "Field コンストラクタ" << std::endl;
}

/// <summary>
/// コピーコンストラクタ
/// </summary>
/// <param name="field"> コピー元クラス </param>
/// <returns></returns>
Field::Field(const Field& field) {

  // コンソールに出力
  std::cout << "Field コピーコンストラクタ" << std::endl;

  field_id_ = field.field_id_;
  model_handle_ = field.model_handle_;
  graphic_handle_ = field.graphic_handle_;
  pos_ = field.pos_;
  model_data_ = field.model_data_;
  corner_ = field.corner_;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Field::~Field() {

  // コンソールに出力
  std::cout << "~Field デストラクタ" << std::endl;
}

/// <summary>
/// クローン生成
/// </summary>
/// <param name=""></param>
/// <returns> バレッジ </returns>
Field* Field::GenerateClone() {

  // コピーコンストラクタを使用して生成
  Field* field = nullptr;
  field = new Field(*this);
  if (field == nullptr) {
    return nullptr;
  }

  return field;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Field::Update() {


}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Field::Render() {

  // ３Ｄモデルの描画
  MV1DrawModel(model_handle_);
}

/// <summary>
/// 表示座標のセット
/// </summary>
/// <param name="x_pos"> X座標 </param>
/// <param name="y_pos"> Y座標 </param>
/// <param name="z_pos"> Z座標 </param>
/// <returns></returns>
void Field::SetDispPosition(float x_pos, float y_pos, float z_pos) {

  pos_.x = x_pos;
  pos_.y = y_pos;
  pos_.z = z_pos;

  MV1SetPosition(model_handle_, VGet(pos_.x, pos_.y, pos_.z));
}