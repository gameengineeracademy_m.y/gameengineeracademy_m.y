﻿#include "Game/PlayerController.h"

namespace {

  /// <summary>
  /// ボタン押下継続時間 リセット
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// ボタン押下有無判定時間
  /// </summary>
  const float kExecuteCheckTime = 0.08f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name="event_interface"> プレイヤーコントローライベントインターフェース </param>
/// <returns></returns>
PlayerController::PlayerController(PlayerControllerEventInterface& event_interface)
  : Task(TaskId::kPlayerController)
  , current_phase_(PhaseType::kWait)
  , event_interface_(event_interface)
  , key_press_keep_time_{ 0.0f, 0.0f, 0.0f, 0.0f, 0.0f }
  , push_key_{ false, false, false, false, false, false }
  , push_key2_{ false, false, false } {

  // コンソールに出力
  std::cout << "PlayerController コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
PlayerController::~PlayerController() {

  // コンソールに出力
  std::cout << "~PlayerController デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void PlayerController::Update(float process_time) {

  bool is_space = GetPushSpaceKey(process_time);
  bool is_lalt = GetPushLAltKey(process_time);
  bool is_w = GetPushWKey(process_time);
  bool is_s = GetPushSKey(process_time);
  bool is_a = GetPushAKey(process_time);
  bool is_d = GetPushDKey(process_time);
  bool is_p = GetPushPKey();
  bool is_m = GetPushMKey();
  bool is_n = GetPushNKey();

  switch (current_phase_) {
  case PhaseType::kWait: {
    //--------------------------------------
    //待機フェーズ
    //--------------------------------------
    break;
  }
  case PhaseType::kSwitch: {
    //--------------------------------------
    //切り替えフェーズ
    //--------------------------------------
    if (is_w || is_s || is_a || is_d || is_lalt ||
        is_space || is_p || is_m || is_n) {
      return;
    }

    // 現在のフェーズを「処理中」に変更
    ChangeCurrentPhase(PhaseType::kProcess);
    break;
  }
  case PhaseType::kProcess: {
    //--------------------------------------
    //処理中フェーズ
    //--------------------------------------
    // 左Altキー押下中は処理を行わない
    if (is_lalt) {
      return;
    }

    if (is_w) {

      if (is_a) {
        // W + Aキー押下処理イベント実行
        event_interface_.OnPushPlayerControllerWAKey();
      }
      else if (is_d) {
        // W + Dキー押下処理イベント実行
        event_interface_.OnPushPlayerControllerWDKey();
      }
      else {
        // Wキー押下処理イベント実行
        event_interface_.OnPushPlayerControllerWKey();
      }
    }
    else if (is_s) {

      if (is_a) {
        // S + Aキー押下処理イベント実行
        event_interface_.OnPushPlayerControllerSAKey();
      }
      else if (is_d) {
        // S + Dキー押下処理イベント実行
        event_interface_.OnPushPlayerControllerSDKey();
      }
      else {
        // Sキー押下処理イベント実行
        event_interface_.OnPushPlayerControllerSKey();
      }
    }
    else if (is_a) {
      // Aキー押下処理イベント実行
      event_interface_.OnPushPlayerControllerAKey();
    }
    else if (is_d) {
      // Dキー押下処理イベント実行
      event_interface_.OnPushPlayerControllerDKey();
    }

    if (is_space) {
      // Spaceキー押下処理イベント実行
      event_interface_.OnPushPlayerControllerSpaceKey();
    }

    if (is_p) {
      // Iキー押下処理イベント実行
      event_interface_.OnPushPlayerControllerPKey();
    }
    else if (is_m) {
      // Mキー押下処理イベント実行
      event_interface_.OnPushPlayerControllerMKey();
    }
    else if (is_n) {
      // Nキー押下処理イベント実行
      event_interface_.OnPushPlayerControllerNKey();
    }

    break;
  }
  }
}

/// <summary>
/// Spaceキー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushSpaceKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_SPACE);
  return CheckPressKey(flag_key_push, push_key_[static_cast<int>(KeyType::kSpace)],
                       process_time, key_press_keep_time_[static_cast<int>(KeyType::kSpace)]);
}

/// <summary>
/// 左Altキー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushLAltKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_LALT);
  return CheckPressKey(flag_key_push, push_key_[static_cast<int>(KeyType::kLalt)],
                       process_time, key_press_keep_time_[static_cast<int>(KeyType::kLalt)]);
}

/// <summary>
/// Wキー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushWKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_W);
  return CheckPressKey(flag_key_push, push_key_[static_cast<int>(KeyType::kW)],
                       process_time, key_press_keep_time_[static_cast<int>(KeyType::kW)]);
}

/// <summary>
/// Sキー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushSKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_S);
  return CheckPressKey(flag_key_push, push_key_[static_cast<int>(KeyType::kS)],
                       process_time, key_press_keep_time_[static_cast<int>(KeyType::kS)]);
}

/// <summary>
/// Aキー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushAKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_A);
  return CheckPressKey(flag_key_push, push_key_[static_cast<int>(KeyType::kA)],
                       process_time, key_press_keep_time_[static_cast<int>(KeyType::kA)]);
}

/// <summary>
/// Dキー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushDKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_D);
  return CheckPressKey(flag_key_push, push_key_[static_cast<int>(KeyType::kD)],
                       process_time, key_press_keep_time_[static_cast<int>(KeyType::kD)]);
}

/// <summary>
/// 指定のキー押下有無を取得する
/// </summary>
/// <param name="key_push"> キーが押されているかどうか </param>
/// <param name="key_push_before"> キーが押されていたかどうか </param>
/// <param name="time_sec"> 毎フレームの処理時間 </param>
/// <param name="push_time_sec"> キー押下継続時間 </param> 
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::CheckPressKey(bool key_push, bool& key_push_before, float time_sec, float& push_time_sec) {

  if (key_push) {
    //------------------------------
    //現在押されている場合
    //------------------------------
    // 1つ前のフレーム処理で押されていたかチェック
    if (key_push_before) {
      push_time_sec += time_sec;
      if (push_time_sec > kExecuteCheckTime) {
        return true;
      }
    }
    else {
      key_push_before = true;
      return true;
    }
  }
  else {
    //------------------------------
    //現在押されていない場合
    //------------------------------
    // 長押し時間、押されたフラグ初期化
    push_time_sec = kResetTime;
    key_push_before = false;
  }

  return false;
}

/// <summary>
/// Pキー押下処理
/// </summary>
/// <param name=""></param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushPKey() {

  return CheckPushKey(KEY_INPUT_P, push_key2_[static_cast<int>(KeyType2::kP)]);
}

/// <summary>
/// Mキー押下処理
/// </summary>
/// <param name=""></param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushMKey() {

  return CheckPushKey(KEY_INPUT_M, push_key2_[static_cast<int>(KeyType2::kM)]);
}

/// <summary>
/// Nキー押下処理
/// </summary>
/// <param name=""></param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushNKey() {

  return CheckPushKey(KEY_INPUT_N, push_key2_[static_cast<int>(KeyType2::kN)]);
}

/// <summary>
/// 指定のキー押下有無を取得する　※単発押し
/// </summary>
/// <param name="key_code"> キーコード </param>
/// <param name="push_key"> キーの押下の有無 </param>
/// <returns>キーが押された：false、キーが押されていない：false</returns>
bool PlayerController::CheckPushKey(int key_code, bool& push_key) {

  // 指定のボタンが押されている
  if (CheckHitKey(key_code)) {

    // 直前まで押されていない場合は押されていることにする
    if (push_key == false) {
      push_key = true;
      // 判定を押されたとして返す
      return true;
    }
  }
  else {
    // 前回処理時にキーが押されていた場合は押されていないことにする
    if (push_key) {
      push_key = false;
    }
  }

  return false;
}