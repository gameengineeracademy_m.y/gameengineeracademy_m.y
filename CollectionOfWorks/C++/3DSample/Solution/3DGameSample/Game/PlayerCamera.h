﻿#pragma once

#include "DxLib.h"
#include "Game/CameraBase.h"
#include <iostream>

/// <summary>
/// プレイヤーカメラ
/// </summary>
class PlayerCamera : public CameraBase {
public:

  /// <summary>
  /// 移動フェーズの種類
  /// </summary>
  enum class MovePhase {
    kWait,           // 停止中フェーズ
    kMoveStart,      // 移動開始フェーズ
    kMove,           // 移動フェーズ
    kMoveInertia,    // 慣性移動フェーズ
    kPhaseMaxIndex   // フェーズ数
  };


public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  PlayerCamera();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~PlayerCamera();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float);

  /// <summary>
  /// プレイヤーとカメラの距離をセット
  /// </summary>
  /// <param name=""> 距離 </param>
  /// <returns></returns>
  void SetDistance(float distance) { distance_ = distance; }

  /// <summary>
  /// プレイヤーとカメラの距離を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  float GetDistance() { return distance_; }

  /// <summary>
  /// カメラを注視点に近づける(拡大)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ExpandObject();

  /// <summary>
  /// カメラを注視点から遠ざける(縮小)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ShrinkObject();

  /// <summary>
  /// 注視点を中心に上方向へ移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void MoveUpCenteredOnGaze();

  /// <summary>
  /// 注視点を中心に下方向へ移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void MoveDownCenteredOnGaze();

  /// <summary>
  /// 注視点を中心に左方向へ移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void MoveLeftCenteredOnGaze();

  /// <summary>
  /// 注視点を中心に右方向へ移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void MoveRightCenteredOnGaze();

  /// <summary>
  /// カメラ座標の計算
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void CalcPositionCamera();


private:

  /// <summary>
  /// プレイヤーとの距離
  /// </summary>
  float distance_;
};