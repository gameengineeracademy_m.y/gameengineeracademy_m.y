﻿#pragma once

#include "System/Task.h"
#include "Game/DebugCamera.h"
#include "Game/PlayerCamera.h"
#include <cmath>

/// <summary>
/// カメラマネージャ
/// </summary>
/// <remarks>
/// デバッグカメラとプレイヤーカメラの管理
/// </remarks>
class CameraManager : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,     // 初期化処理フェーズ
    kInitialized,    // 初期化処理済みフェーズ
    kPrepare,        // 準備フェーズ
    kProcess,        // 処理中フェーズ
    kFinalize,       // 終了処理フェーズ
    kFinalized,      // 終了処理済みフェーズ
    kPhaseMaxIndex   // フェーズ数
  };

  /// <summary>
  /// カメラの種類
  /// </summary>
  enum class CameraType {
    kDebug,         // デバッグ
    kPlayer,        // プレイヤー
    kTypeMaxIndex   // 種類数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  CameraManager();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~CameraManager();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// カメラの生成
  /// </summary>
  /// <param name=""> カメラの種類 </param>
  /// <returns></returns>
  void CreateCamera(CameraType);

  /// <summary>
  /// カメラの破棄
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DisposeCamera();

  /// <summary>
  /// デバッグカメラ初期化
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void InitializeDebugCamera();

  /// <summary>
  /// プレイヤーカメラ初期化
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void InitializePlayerCamera();

  /// <summary>
  /// クリッピング距離設定
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetClippingDistance();

  /// <summary>
  /// 角度計算 デバッグカメラ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void CalcAngleDebugCamera();

  /// <summary>
  /// 角度計算 プレイヤーカメラ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void CalcAnglePlayerCamera();

  /// <summary>
  /// カメラの種類を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> カメラの種類 </returns>
  CameraType GetCameraType() { return camera_type_; }

  /// <summary>
  /// デバッグカメラの取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> デバッグカメラ </returns>
  DebugCamera* GetDebugCamera() { return debug_camera_; }

  /// <summary>
  /// プレイヤーカメラの取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> プレイヤーカメラ </returns>
  PlayerCamera* GetPlayerCamera() { return player_camera_; }

  /// <summary>
  /// カメラの切り替え
  /// </summary>
  /// <param name="camera_type"> カメラの種類 </param>
  /// <returns></returns>
  void ChangeCameraType(CameraType camera_type) { camera_type_ = camera_type; }

  /// <summary>
  /// 現在のフェーズの変更
  /// </summary>
  /// <param name="phase_type"> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }

  /// <summary>
  /// 現在のフェーズが初期化済みどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:初期化処理済み, false:それ以外 </returns>
  bool IsInitialized() { return current_phase_ == PhaseType::kInitialized; }


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Initialize();

  /// <summary>
  /// 準備処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Prepare();

  /// <summary>
  /// 処理中処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Process(float);


private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// カメラの種類
  /// </summary>
  CameraType camera_type_;

  /// <summary>
  /// デバッグカメラ
  /// </summary>
  DebugCamera* debug_camera_;

  /// <summary>
  /// プレイヤーカメラ
  /// </summary>
  PlayerCamera* player_camera_;
};