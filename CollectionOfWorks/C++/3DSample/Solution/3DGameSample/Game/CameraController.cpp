﻿#include "Game/CameraController.h"

namespace {

  /// <summary>
  /// ボタン押下継続時間 リセット
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// ボタン押下有無判定時間
  /// </summary>
  const float kExecuteCheckTime = 0.08f;

  /// <summary>
  /// ホイール回転無し
  /// </summary>
  const int kWheelNotRota = 0;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name="event_interface"> カメラコントローライベントインターフェース </param>
/// <returns></returns>
CameraController::CameraController(CameraControllerEventInterface& event_interface)
  : Task(TaskId::kCameraController)
  , current_phase_(PhaseType::kWait)
  , event_interface_(event_interface)
  , key_press_keep_time_{ 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f }
  , push_key_{ false, false, false, false, false, false, false, false, false, false, false, false, false }
  , push_key2_{ false, false, false } {

  // コンソールに出力
  std::cout << "CameraController コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
CameraController::~CameraController() {

  // コンソールに出力
  std::cout << "~CameraController デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CameraController::Update(float process_time) {

  int rota_value = GetRotateMouseWheel();
  bool is_shift = GetPushShiftKey(process_time);
  bool is_alt = GetPushAltKey(process_time);
  bool is_up = GetPushUpKey(process_time);
  bool is_down = GetPushDownKey(process_time);
  bool is_right = GetPushRightKey(process_time);
  bool is_left = GetPushLeftKey(process_time);
  bool is_w = GetPushWKey(process_time);
  bool is_s = GetPushSKey(process_time);
  bool is_a = GetPushAKey(process_time);
  bool is_d = GetPushDKey(process_time);
  bool is_q = GetPushQKey(process_time);
  bool is_e = GetPushEKey(process_time);
  bool is_esc = GetPushEscKey();
  bool is_i = GetPushIKey();
  bool is_p = GetPushPKey();

  switch (current_phase_) {
  case PhaseType::kWait: {
    //--------------------------------------
    //待機フェーズ
    //--------------------------------------
    break;
  }
  case PhaseType::kSwitch: {
    //--------------------------------------
    //切り替えフェーズ
    //--------------------------------------
    // キー操作中なら処理終了
    if (is_w || is_s || is_a || is_d || is_alt ||
        is_up || is_down || is_right || is_left || is_esc ||
        is_q || is_e || is_i || is_p || is_shift || rota_value != kWheelNotRota) {
      return;
    }

    // 現在のフェーズを「処理中」に変更
    ChangeCurrentPhase(PhaseType::kProcess);
    break;
  }
  case PhaseType::kProcess: {
    //--------------------------------------
    //処理中フェーズ
    //--------------------------------------
    if (rota_value > kWheelNotRota) {
      // マウスホイールを奥に回す
      event_interface_.OnRotateBackWheel();
    }
    else if (rota_value < kWheelNotRota) {
      // マウスホイールを手前に回す
      event_interface_.OnRotateFrontWheel();
    }

    if (is_w) {
      if (is_shift) {
        // Shift + Wキー押下処理イベント実行
        event_interface_.OnPushShiftWKey();
      }
      else {
        // Wキー押下処理イベント実行
        event_interface_.OnPushWKey();
      }
    }

    if (is_s) {
      if (is_shift) {
        // Shift + Sキー押下処理イベント実行
        event_interface_.OnPushShiftSKey();
      }
      else {
        // Sキー押下処理イベント実行
        event_interface_.OnPushSKey();
      }
    }

    if (is_a) {
      if (is_shift) {
        // Shift + Aキー押下処理イベント実行
        event_interface_.OnPushShiftAKey();
      }
      else {
        // Aキー押下処理イベント実行
        event_interface_.OnPushAKey();
      }
    }

    if (is_d) {
      if (is_shift) {
        // Shift + Dキー押下処理イベント実行
        event_interface_.OnPushShiftDKey();
      }
      else if (is_alt) {
        // Alt + Dキー押下処理イベント実行
        event_interface_.OnPushAltDKey();
      }
      else {
        // Dキー押下処理イベント実行
        event_interface_.OnPushDKey();
      }
    }

    if (is_up) {
      event_interface_.OnPushUpKey();
    }
    else if (is_down) {
      event_interface_.OnPushDownKey();
    }

    if (is_right) {
      event_interface_.OnPushRightKey();
    }
    else if (is_left) {
      event_interface_.OnPushLeftKey();
    }

    if (is_q) {
      if (is_shift) {
        // Shift + Qキー押下処理イベント実行
        event_interface_.OnPushShiftQKey();
      }
    }

    if (is_e) {
      if (is_shift) {
        // Shift + Eキー押下処理イベント実行
        event_interface_.OnPushShiftEKey();
      }
    }

    if (is_esc) {
      // Escキー押下処理イベント実行
      event_interface_.OnPushEscapeKey();
    }
    else if (is_i) {
      // Iキー押下処理イベント実行
      event_interface_.OnPushIKey();
    }
    else if (is_p) {
      // Pキー押下処理イベント実行
      event_interface_.OnPushPKey();
    }

    break;
  }
  }
}

/// <summary>
/// 左Shiftキー押下処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool CameraController::GetPushShiftKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_LSHIFT);
  return CheckPressKey(flag_key_push, push_key_[static_cast<int>(KeyType::kLShift)],
                       process_time, key_press_keep_time_[static_cast<int>(KeyType::kLShift)]);
}

/// <summary>
/// 左ALTキー押下処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool CameraController::GetPushAltKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_LALT);
  return CheckPressKey(flag_key_push, push_key_[static_cast<int>(KeyType::kLAlt)],
                       process_time, key_press_keep_time_[static_cast<int>(KeyType::kLAlt)]);
}

/// <summary>
/// マウスホイール回転処理
/// </summary>
/// <param name=""></param>
/// <returns> 回転量 </returns>
int CameraController::GetRotateMouseWheel() {

  //ホイールの回転量取得
  int wheel_value = GetMouseWheelRotVol();

  return wheel_value;
}

/// <summary>
/// 上矢印キー押下処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool CameraController::GetPushUpKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_UP);
  return CheckPressKey(flag_key_push, push_key_[static_cast<int>(KeyType::kUp)],
                       process_time, key_press_keep_time_[static_cast<int>(KeyType::kUp)]);
}

/// <summary>
/// 下矢印キー押下処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool CameraController::GetPushDownKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_DOWN);
  return CheckPressKey(flag_key_push, push_key_[static_cast<int>(KeyType::kDown)],
                       process_time, key_press_keep_time_[static_cast<int>(KeyType::kDown)]);
}

/// <summary>
/// 右矢印キー押下処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool CameraController::GetPushRightKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_RIGHT);
  return CheckPressKey(flag_key_push, push_key_[static_cast<int>(KeyType::kRight)],
                       process_time, key_press_keep_time_[static_cast<int>(KeyType::kRight)]);
}

/// <summary>
/// 左矢印キー押下処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool CameraController::GetPushLeftKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_LEFT);
  return CheckPressKey(flag_key_push, push_key_[static_cast<int>(KeyType::kLeft)],
                       process_time, key_press_keep_time_[static_cast<int>(KeyType::kLeft)]);
}

/// <summary>
/// Wキー押下処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool CameraController::GetPushWKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_W);
  return CheckPressKey(flag_key_push, push_key_[static_cast<int>(KeyType::kW)],
                       process_time, key_press_keep_time_[static_cast<int>(KeyType::kW)]);
}

/// <summary>
/// Sキー押下処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool CameraController::GetPushSKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_S);
  return CheckPressKey(flag_key_push, push_key_[static_cast<int>(KeyType::kS)],
                       process_time, key_press_keep_time_[static_cast<int>(KeyType::kS)]);
}

/// <summary>
/// Aキー押下処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool CameraController::GetPushAKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_A);
  return CheckPressKey(flag_key_push, push_key_[static_cast<int>(KeyType::kA)],
                       process_time, key_press_keep_time_[static_cast<int>(KeyType::kA)]);
}

/// <summary>
/// Dキー押下処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool CameraController::GetPushDKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_D);
  return CheckPressKey(flag_key_push, push_key_[static_cast<int>(KeyType::kD)],
                       process_time, key_press_keep_time_[static_cast<int>(KeyType::kD)]);
}

/// <summary>
/// Qキー押下処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool CameraController::GetPushQKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_Q);
  return CheckPressKey(flag_key_push, push_key_[static_cast<int>(KeyType::kQ)],
                       process_time, key_press_keep_time_[static_cast<int>(KeyType::kQ)]);
}

/// <summary>
/// Eキー押下処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool CameraController::GetPushEKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_E);
  return CheckPressKey(flag_key_push, push_key_[static_cast<int>(KeyType::kE)],
                       process_time, key_press_keep_time_[static_cast<int>(KeyType::kE)]);
}

/// <summary>
/// 指定のキー押下有無を取得する
/// </summary>
/// <param name="key_push"> キーが押されているかどうか </param>
/// <param name="key_push_before"> キーが押されていたかどうか </param>
/// <param name="time_sec"> 毎フレームの処理時間 </param>
/// <param name="push_time_sec"> キー押下継続時間 </param> 
/// <returns> true：押された, false：押されていない </returns>
bool CameraController::CheckPressKey(bool key_push, bool& key_push_before, float time_sec, float& push_time_sec) {

  if (key_push) {
    //------------------------------
    //現在押されている場合
    //------------------------------
    // 1つ前のフレーム処理で押されていたかチェック
    if (key_push_before) {
      push_time_sec += time_sec;
      if (push_time_sec > kExecuteCheckTime) {
        return true;
      }
    }
    else {
      key_push_before = true;
      return true;
    }
  }
  else {
    //------------------------------
    //現在押されていない場合
    //------------------------------
    // 長押し時間、押されたフラグ初期化
    push_time_sec = kResetTime;
    key_push_before = false;
  }

  return false;
}


/// <summary>
/// Escキー押下処理
/// </summary>
/// <param name=""></param>
/// <returns> true：押された, false：押されていない </returns>
bool CameraController::GetPushEscKey() {

  return CheckPushKey(KEY_INPUT_ESCAPE, push_key2_[static_cast<int>(KeyType2::kEsc)]);
}

/// <summary>
/// Iキー押下処理
/// </summary>
/// <param name=""></param>
/// <returns> true：押された, false：押されていない </returns>
bool CameraController::GetPushIKey() {

  return CheckPushKey(KEY_INPUT_I, push_key2_[static_cast<int>(KeyType2::kI)]);
}

/// <summary>
/// Pキー押下処理
/// </summary>
/// <param name=""></param>
/// <returns> true：押された, false：押されていない </returns>
bool CameraController::GetPushPKey() {

  return CheckPushKey(KEY_INPUT_P, push_key2_[static_cast<int>(KeyType2::kP)]);
}

/// <summary>
/// 指定のキー押下有無を取得する　※単発押し
/// </summary>
/// <param name="key_code"> キーコード </param>
/// <param name="push_key"> キーの押下の有無 </param>
/// <returns>キーが押された：false、キーが押されていない：false</returns>
bool CameraController::CheckPushKey(int key_code, bool& push_key) {

  // 指定のボタンが押されている
  if (CheckHitKey(key_code)) {

    // 直前まで押されていない場合は押されていることにする
    if (push_key == false) {
      push_key = true;
      // 判定を押されたとして返す
      return true;
    }
  }
  else {
    // 前回処理時にキーが押されていた場合は押されていないことにする
    if (push_key) {
      push_key = false;
    }
  }

  return false;
}