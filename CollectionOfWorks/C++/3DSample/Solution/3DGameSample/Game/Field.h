﻿#pragma once

#include "DxLib.h"
#include "Game/FieldId.h"
#include <iostream>
#include <vector>

/// <summary>
/// フィールド処理
/// </summary>
class Field {
public:

  /// <summary>
  /// 位置
  /// </summary>
  enum class Position {
    kTopLeft,       // 左上
    kTopRight,      // 右上
    kBottomLeft,    // 左下
    kBottomRight,   // 右下
    kMaxIndex       // 位置の種類数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  Field();

  /// <summary>
  /// コピーコンストラクタ
  /// </summary>
  /// <param name=""> コピー元クラス </param>
  /// <returns></returns>
  Field(const Field&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~Field();

  /// <summary>
  /// クローン生成
  /// </summary>
  /// <param name=""></param>
  /// <returns> フィールド </returns>
  Field* GenerateClone();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Update();

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render();

  /// <summary>
  /// フィールドIDをセット
  /// </summary>
  /// <param name="field_id"> フィールドID </param>
  /// <returns></returns>
  void SetFieldId(FieldId field_id) { field_id_ = field_id; }

  /// <summary>
  /// モデルハンドルをセット
  /// </summary>
  /// <param name="handle"> モデルハンドル </param>
  /// <returns></returns>
  void SetModelHandle(int handle) { model_handle_ = handle; }

  /// <summary>
  /// モデルハンドルを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> モデルハンドル </returns>
  int GetModelHandle() { return model_handle_; }

  /// <summary>
  /// グラフィックハンドルをセット
  /// </summary>
  /// <param name="handle"> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandle(int handle) { graphic_handle_ = handle; }

  /// <summary>
  /// グラフィックハンドルを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandle() { return graphic_handle_; }

  /// <summary>
  /// 表示座標のセット
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <param name=""> Z座標 </param>
  /// <returns></returns>
  void SetDispPosition(float, float, float);

  /// <summary>
  /// 表示座標(X座標)の取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> X座標 </returns>
  float GetDispPositionX() { return pos_.x; }

  /// <summary>
  /// 表示座標(Y座標)の取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> Y座標 </returns>
  float GetDispPositionY() { return pos_.y; }

  /// <summary>
  /// 表示座標(Z座標)の取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> Z座標 </returns>
  float GetDispPositionZ() { return pos_.z; }

  /// <summary>
  /// モデルの幅をセット
  /// </summary>
  /// <param name=""> モデルの幅 </param>
  /// <returns></returns>
  void SetModelWidth(float width) { model_data_.size.width = width; }

  /// <summary>
  /// モデルの幅を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  float GetModelWidth() { return model_data_.size.width; }

  /// <summary>
  /// モデルの高さをセット
  /// </summary>
  /// <param name=""> モデルの高さ </param>
  /// <returns></returns>
  void SetModelHeight(float height) { model_data_.size.height = height; }

  /// <summary>
  /// モデルの高さを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  float GetModelHeight() { return model_data_.size.height; }

  /// <summary>
  /// モデルの奥行をセット
  /// </summary>
  /// <param name=""> モデルの奥行 </param>
  /// <returns></returns>
  void SetModelDepth(float depth) { model_data_.size.depth = depth; }

  /// <summary>
  /// モデルの奥行を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  float GetModelDepth() { return model_data_.size.depth; }

  /// <summary>
  /// モデルの角度をセット
  /// </summary>
  /// <param name=""> モデルの角度 </param>
  /// <returns></returns>
  void SetModelAngle(float angle) { model_data_.angle = angle; }

  /// <summary>
  /// モデルの角度を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  float GetModelAngle() { return model_data_.angle; }

  /// <summary>
  /// モデルの四つ角座標配列にセット
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetModelCorner(std::vector<VECTOR> corner) { corner_ = corner; }

  /// <summary>
  /// モデルの四つ角座標配列を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  std::vector<VECTOR> GetModelCorner() { return corner_; }

private:

  /// <summary>
  /// サイズ
  /// </summary>
  struct Size {
    float width;
    float height;
    float depth;
  };

  /// <summary>
  /// モデルデータ
  /// </summary>
  struct ModelData {
    Size size;
    float angle;
  };

private:

  /// <summary>
  /// フィールドID
  /// </summary>
  FieldId field_id_;

  /// <summary>
  /// モデルハンドル
  /// </summary>
  int model_handle_;

  /// <summary>
  /// グラフィックハンドル
  /// </summary>
  int graphic_handle_;

  /// <summary>
  /// 表示座標
  /// </summary>
  VECTOR pos_;

  /// <summary>
  /// モデルデータ
  /// </summary>
  ModelData model_data_;

  /// <summary>
  /// 四つ角
  /// </summary>
  std::vector<VECTOR> corner_;
};