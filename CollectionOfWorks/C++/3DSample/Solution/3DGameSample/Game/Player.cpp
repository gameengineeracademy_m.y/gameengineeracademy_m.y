﻿#include "Game/Player.h"

namespace {

  /// <summary>
  /// プレイヤーモデルファイルパス
  /// </summary>
  const char* kPlayerModelFilePath = "Asset/Model/human.mv1";

  /// <summary>
  /// 表示倍率
  /// </summary>
  const float kDispRate = 0.5f;

  /// <summary>
  /// プレイヤー 幅　※当たり判定に使用
  /// </summary>
  const float kPlayerWidth = 90.0f;

  /// <summary>
  /// プレイヤー 高さ　※当たり判定に使用
  /// </summary>
  const float kPlayerHeight = 184.0f;

  /// <summary>
  /// プレイヤー 奥行　※当たり判定に使用
  /// </summary>
  const float kPlayerDepth = 90.0f;

  /// <summary>
  /// プレイヤー初期表示位置 X座標
  /// </summary>
  const float kPlayerInitPosX = 600.0f;

  /// <summary>
  /// プレイヤー初期表示位置 Y座標
  /// </summary>
  const float kPlayerInitPosY = 66.0f;

  /// <summary>
  /// プレイヤー初期表示位置 Z座標
  /// </summary>
  const float kPlayerInitPosZ = -400.0f;

  /// <summary>
  /// ゼロ
  /// </summary>
  const float kZero = 0.0f;

  /// <summary>
  /// 半分
  /// </summary>
  const float kHalfValuef = 2.0f;

  /// <summary>
  /// マイナス
  /// </summary>
  const float kSignMinus = -1.0f;

  /// <summary>
  /// 移動量
  /// </summary>
  const float kMoveValue = 5.0f;

  /// <summary>
  /// 0°
  /// </summary>
  const float kAngle0 = 0.0f;

  /// <summary>
  /// 45°
  /// </summary>
  const float kAngle45 = 45.0f;

  /// <summary>
  /// 90°
  /// </summary>
  const float kAngle90 = 90.0f;

  /// <summary>
  /// 135°
  /// </summary>
  const float kAngle135 = 135.0f;

  /// <summary>
  /// 180°
  /// </summary>
  const float kAngle180 = 180.0f;

  /// <summary>
  /// 225°
  /// </summary>
  const float kAngle225 = 225.0f;

  /// <summary>
  /// 270°
  /// </summary>
  const float kAngle270 = 270.0f;

  /// <summary>
  /// 315°
  /// </summary>
  const float kAngle315 = 315.0f;

  /// <summary>
  /// 360°
  /// </summary>
  const float kAngle360 = 360.0f;

  /// <summary>
  /// リセット時間
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 開始時間
  /// </summary>
  const float kStartTime = 0.0f;

  /// <summary>
  /// 1フレームごとの加算値
  /// </summary>
  const float kWalkTimeAdjust = 1.0f;

  /// <summary>
  /// 1フレームごとの加算値
  /// </summary>
  const float kJumpTimeAdjust = 10.0f;

  /// <summary>
  /// 1フレームごとの加算値
  /// </summary>
  const float kLandTimeAdjust = 5.0f;

  /// <summary>
  /// プレイヤー重力加速度
  /// </summary>
  const float kGravityAccele = 9.81f;

  /// <summary>
  /// プレイヤージャンプ初速度
  /// </summary>
  const float kInitialJumpVelocity = 40.0f;

  /// <summary>
  /// プレイヤー落下初速度
  /// </summary>
  const float kInitialFallVelocity = 0.0f;

  /// <summary>
  /// 経過時間
  /// </summary>
  const float kElapsedTime = 0.16f;

  /// <summary>
  /// 1 / √2
  /// </summary>
  const float kDiagonalRate = 0.71f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name="event_interface"> プレイヤーイベントインターフェース </param>
/// <returns></returns>
Player::Player(PlayerEventInterface& event_interface)
  : Task(TaskId::kPlayer)
  , current_phase_(PhaseType::kInitialize)
  , player_event_interface_(event_interface)
  , model_handle_(0)
  , pos_()
  , model_data_()
  , animation_id_(0)
  , animation_time_(0.0f)
  , animation_total_time_(0.0f)
  , is_state_(State::kStop)
  , is_exec_state_(State::kStop)
  , jump_time_(0.0f)
  , y_pos_height_(0.0f)
  , velocity_init_(0.0f) {

  // コンソールに出力
  std::cout << "Player コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Player::~Player() {

  // コンソールに出力
  std::cout << "~Player デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void Player::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //-----------------------------
    //初期化処理フェーズ
    //-----------------------------
    Initialize();
    // 現在のフェーズを「起動待機」に変更
    ChangeCurrentPhase(PhaseType::kStartWait);
    break;
  }
  case PhaseType::kStartWait: {
    //-----------------------------
    //起動待機フェーズ
    //-----------------------------
    break;
  }
  case PhaseType::kStartUp: {
    //-----------------------------
    //起動フェーズ
    //-----------------------------
    // 現在のフェーズを「処理中」に変更
    ChangeCurrentPhase(PhaseType::kProcess);
    break;
  }
  case PhaseType::kProcess: {
    //-----------------------------
    //処理中フェーズ
    //-----------------------------
    Process();
    break;
  }
  case PhaseType::kBeforeFinalize: {
    //-----------------------------
    //終了処理前フェーズ
    //-----------------------------
    // 現在のフェーズを「終了処理」に変更
    ChangeCurrentPhase(PhaseType::kFinalize);
    break;
  }
  case PhaseType::kFinalize: {
    //-----------------------------
    //終了処理フェーズ
    //-----------------------------
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Player::Render() {

  switch (current_phase_) {
  case PhaseType::kStartUp:
  case PhaseType::kProcess:
  case PhaseType::kBeforeFinalize: {

    // ３Ｄモデルの描画
    MV1DrawModel(model_handle_);

    break;
  }
  }
}

/// <summary>
/// 表示座標のセット
/// </summary>
/// <param name="pos_vec"> 座標ベクトル </param>
/// <returns></returns>
void Player::SetDispPosition(VECTOR pos_vec) {

  pos_ = pos_vec;
  MV1SetPosition(model_handle_, pos_);
}

/// <summary>
/// 初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Player::Initialize() {

  // プレイヤーモデルをロードし、セット
  int model_handle = MV1LoadModel(kPlayerModelFilePath);
  SetModelHandle(model_handle);
  // モデルのスケールを0.5倍にする
  MV1SetScale(model_handle, VGet(kDispRate, kDispRate, kDispRate));
  // 初期表示位置セット
  SetDispPosition(VGet(kPlayerInitPosX, kPlayerInitPosY, kPlayerInitPosZ));

  // プレイヤーサイズセット
  SetModelWidth(kDispRate * kPlayerWidth);
  SetModelHeight((kDispRate * kPlayerHeight) / kHalfValuef);
  SetModelDepth(kDispRate * kPlayerDepth);
  SetAngle(kAngle0);

  // 状態を停止にしておく
  is_state_ = State::kStop;
  is_exec_state_ = State::kStop;
  // 待機アニメーションをアタッチ
  animation_id_ = MV1AttachAnim(model_handle, static_cast<int>(AnimationId::kStop));
  // 待機アニメーションの総時間を取得しておく
  animation_total_time_ = MV1GetAttachAnimTotalTime(model_handle, animation_id_);
  // 再生時間を初期値に設定
  animation_time_ = kStartTime;
}

/// <summary>
/// 処理中処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Player::Process() {

  float y_pos_floor = 0.0f;
  // 床と接地しているかどうか
  bool is_check = player_event_interface_.OnCollisionFloor(y_pos_floor);
  if (!is_check) {
    // 現在の状態が「ジャンプ中」か確認
    // ジャンプ中でなければジャンプ中にする
    if (!IsStateJump()) {
      is_state_ = State::kJump;
      // ジャンプ直前の高さ取得
      y_pos_height_ = pos_.y;
      // 初速を「0」でセット
      velocity_init_ = kInitialFallVelocity;
    }
  }
  else {
    // 現在の状態が「ジャンプ中」か確認
    // ジャンプ中なら「着地」にする
    if (IsStateJump()) {
      is_state_ = State::kLand;
      // ジャンプしている時間を初期化
      jump_time_ = kResetTime;
    }
    // プレイヤーの足裏座標を床の最大高さに合わせる
    pos_.y = y_pos_floor;
    pos_.y += GetModelHeight();
  }

  //---------------------------------------
  //アニメーション処理
  //---------------------------------------
  switch (is_state_) {
  case State::kStop: {
    //------------------------------
    //待機
    //------------------------------
    // カメラの注視点の位置確認
    player_event_interface_.OnResetCameraLookAtPos();
    // 待機処理
    ExecWaitAnimation();
    break;
  }
  case State::kWalk: {
    //------------------------------
    //歩行
    //------------------------------
    // 歩行処理
    ExecWalkAnimation();
    break;
  }
  case State::kJump: {
    //------------------------------
    //ジャンプ
    //------------------------------
    // ジャンプ時間を加算
    jump_time_ += kElapsedTime;
    // 高さ計算処理
    CalcJumpHeight();
    // ジャンプ処理
    ExecJumpAnimation();
    break;
  }
  case State::kLand: {
    //------------------------------
    //着地
    //------------------------------
    // 着地処理
    ExecLandAnimation();
    break;
  }
  }
}

/// <summary>
/// 待機アニメーション処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Player::ExecWaitAnimation() {

  if (is_exec_state_ != is_state_) {
    //----------------------------------
    // １フレーム前と処理が異なる場合
    //----------------------------------
    // アニメーションの切り替え
    SwitchAnimation();
  }
  else {
    //----------------------------------
    // １フレーム前と処理が同一の場合
    //----------------------------------
    // アニメーションの再生時間を加算
    animation_time_ += kWalkTimeAdjust;
    // 歩行開始
    if (animation_time_ >= animation_total_time_) {
      animation_time_ -= animation_total_time_;
    }
  }

  // 新しいアニメーション再生時間をセット
  MV1SetAttachAnimTime(model_handle_, animation_id_, animation_time_);
}

/// <summary>
/// 歩行アニメーション処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Player::ExecWalkAnimation() {

  if (is_exec_state_ != is_state_) {
    //----------------------------------
    // １フレーム前と処理が異なる場合
    //----------------------------------
    // アニメーションの切り替え
    SwitchAnimation();
  }
  else {
    //----------------------------------
    // １フレーム前と処理が同一の場合
    //----------------------------------
    // アニメーションの再生時間を加算
    animation_time_ += kWalkTimeAdjust;
    // 歩行開始
    if (animation_time_ >= animation_total_time_) {
      animation_time_ -= animation_total_time_;
    }
  }

  // 新しいアニメーション再生時間をセット
  MV1SetAttachAnimTime(model_handle_, animation_id_, animation_time_);

  // 処理終了後、状態を停止にする
  is_state_ = State::kStop;
}

/// <summary>
/// ジャンプアニメーション処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Player::ExecJumpAnimation() {

  if (is_exec_state_ != is_state_) {
    //----------------------------------
    // １フレーム前と処理が異なる場合
    //----------------------------------
    // アニメーションの切り替え
    SwitchAnimation();
  }
  else {
    //----------------------------------
    // １フレーム前と処理が同一の場合
    //----------------------------------
    // アニメーションの再生時間を加算
    animation_time_ += kJumpTimeAdjust;
    // 再生時間が総再生時間を超えた場合、再生時間に総再生時間をセット
    // ずっと腕を上げたままにする
    if (animation_time_ >= animation_total_time_) {
      animation_time_ = animation_total_time_;
    }
  }

  // 新しいアニメーション再生時間をセット
  MV1SetAttachAnimTime(model_handle_, animation_id_, animation_time_);
}

/// <summary>
/// 着地アニメーション処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Player::ExecLandAnimation() {

  if (is_exec_state_ != is_state_) {
    //----------------------------------
    // １フレーム前と処理が異なる場合
    //----------------------------------
    // アニメーションの切り替え
    SwitchAnimation();
  }
  else {
    //----------------------------------
    // １フレーム前と処理が同一の場合
    //----------------------------------
    // アニメーションの再生時間を加算
    animation_time_ += kLandTimeAdjust;
    // 再生時間が総再生時間を超えた場合、「待機」状態に遷移
    if (animation_time_ > animation_total_time_) {
      animation_time_ = animation_total_time_;
      is_state_ = State::kStop;
    }
  }

  // 新しいアニメーション再生時間をセット
  MV1SetAttachAnimTime(model_handle_, animation_id_, animation_time_);
}

/// <summary>
/// アニメーション切り替え処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Player::SwitchAnimation() {

  // 現在の状態にセット
  is_exec_state_ = is_state_;
  // 今までアタッチしていたアニメーションのデタッチ
  MV1DetachAnim(model_handle_, animation_id_);

  switch (is_exec_state_) {
  case State::kStop: {
    //------------------------
    // 停止
    //------------------------
    // 待機アニメーションをアタッチ
    animation_id_ = MV1AttachAnim(model_handle_, static_cast<int>(AnimationId::kStop));
    break;
  }
  case State::kWalk: {
    //------------------------
    // 歩行
    //------------------------
    // 歩行アニメーションをアタッチ
    animation_id_ = MV1AttachAnim(model_handle_, static_cast<int>(AnimationId::kWalk));
    break;
  }
  case State::kJump: {
    //------------------------
    // ジャンプ
    //------------------------
    // ジャンプアニメーションをアタッチ
    animation_id_ = MV1AttachAnim(model_handle_, static_cast<int>(AnimationId::kJump));
    break;
  }
  case State::kLand: {
    //------------------------
    // 着地
    //------------------------
    // 着地アニメーションをアタッチ
    animation_id_ = MV1AttachAnim(model_handle_, static_cast<int>(AnimationId::kLand));
    break;
  }
  }

  // アタッチしたアニメーションの総再生時間を取得する
  animation_total_time_ = MV1GetAttachAnimTotalTime(model_handle_, animation_id_);
  // 再生時間を初期値に設定
  animation_time_ = kStartTime;
}

/// <summary>
/// ジャンプ中の高さを算出
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Player::CalcJumpHeight() {

  // 上方向への放物軌道
  float y_pos = velocity_init_ * jump_time_ - kGravityAccele / kHalfValuef * jump_time_ * jump_time_ + y_pos_height_;

  // カメラ位置の調整
  player_event_interface_.OnMoveCameraLookAtPos(y_pos);

  // プレイヤーのY座標に加算
  VECTOR pos = GetDispPosition();
  pos.y = y_pos;
  SetDispPosition(pos);

  //std::cout << "累積時間 : " << jump_time_ << std::endl;
  //std::cout << "初速側   : " << velocity_init_ * jump_time_ << std::endl;
  //std::cout << "加速度側 : " << kGravityAccele / kHalfValuef * jump_time_ * jump_time_ << std::endl;
  //std::cout << "初期高さ : " << y_pos_height_ << std::endl;
  //std::cout << "高さ     : " << y_pos << std::endl;
  //std::cout << "Player高さ: " << pos.y << std::endl;
}

/// <summary>
/// 向きのセット
/// </summary>
/// <param name="angle"> 角度 </param>
/// <returns></returns>
void Player::SetAngle(float angle) {

  // プレイヤーの向きをセット
  model_data_.angle = angle;
  float angle_radian = model_data_.angle / kAngle180 * DX_PI_F;
  MV1SetRotationXYZ(model_handle_, VGet(kZero, angle_radian, kZero));
}

/// <summary>
/// 初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Player::InitializeData() {

  // 初期表示位置セット
  SetDispPosition(VGet(kPlayerInitPosX, kPlayerInitPosY, kPlayerInitPosZ));

  // 歩行フラグをOFF
  is_state_ = State::kStop;
  is_exec_state_ = State::kStop;

  // 今までアタッチしていたアニメーションのデタッチ
  MV1DetachAnim(model_handle_, animation_id_);
  // 待機アニメーションをアタッチ
  animation_id_ = MV1AttachAnim(model_handle_, static_cast<int>(AnimationId::kStop));
  // アタッチしたアニメーションの総再生時間を取得する
  animation_total_time_ = MV1GetAttachAnimTotalTime(model_handle_, animation_id_);
  // 再生時間を初期値に設定
  animation_time_ = kStartTime;
}

/// <summary>
/// 位置座標への加算
/// </summary>
/// <param name="add_value"> 加算するベクトル </param>
/// <returns> 加算後の位置座標 </returns>
VECTOR Player::AddPosition(VECTOR add_value) {

  pos_ = VAdd(pos_, add_value);

  return pos_;
}

/// <summary>
/// 前に移動
/// </summary>
/// <param name="angle_h"> カメラ水平角度 </param>
/// <returns></returns>
VECTOR Player::MoveFront(float angle_h) {

  bool is_jump = IsStateJump();
  if (!is_jump) {
    // 歩行中にする
    is_state_ = State::kWalk;
  }

  // 移動量
  float move_x = kZero;
  float move_y = kZero;
  float move_z = kMoveValue;
  float angle = angle_h - kAngle270;

  // 向きのセット
  SetAngle(kAngle270 - angle);
  // 表示座標の計算し、セット
  VECTOR move_adjust = CalcMovePosition(move_x, move_y, move_z, angle);

  std::cout << "カメラ 角度  　：" << angle_h << std::endl;
  std::cout << "進行方向 角度　：" << angle << std::endl;
  std::cout << "プレイヤー 角度：" << kAngle270 - angle << std::endl;

  return move_adjust;
}

/// <summary>
/// 左前に移動
/// </summary>
/// <param name="angle_h"> 現在のカメラ水平角度 </param>
/// <returns> 移動量 </returns>
VECTOR Player::MoveFrontLeft(float angle_h) {

  bool is_jump = IsStateJump();
  if (!is_jump) {
    // 歩行中にする
    is_state_ = State::kWalk;
  }

  // 向きのセット
  float angle = kAngle270 - angle_h;
  SetAngle(angle + kAngle225);

  // 前方に移動
  float move_x = kZero;
  float move_y = kZero;
  float move_z = kMoveValue;
  angle = angle_h - kAngle270;
  // 表示座標の計算し、セット
  VECTOR move_adjust_front = CalcMovePosition(move_x, move_y, move_z, angle);

  // 左に移動
  move_x = kMoveValue;
  move_y = kZero;
  move_z = kZero;
  angle = angle_h - kAngle90;
  // 表示座標の計算し、セット
  VECTOR move_adjust_left = CalcMovePosition(move_x, move_y, move_z, angle);

  // 2つの移動量を加算し、1/√2倍して移動量を調整する
  VECTOR move_adjust = VAdd(move_adjust_front, move_adjust_left);
  move_adjust = VScale(move_adjust, kDiagonalRate);

  std::cout << "カメラ 角度  　：" << angle_h << std::endl;
  std::cout << "進行方向 角度　：" << angle << std::endl;
  std::cout << "プレイヤー 角度：" << angle_h - kAngle45 << std::endl;

  return move_adjust;
}

/// <summary>
/// 右前に移動
/// </summary>
/// <param name="angle_h"> 現在のカメラ水平角度 </param>
/// <returns> 移動量 </returns>
VECTOR Player::MoveFrontRight(float angle_h) {

  bool is_jump = IsStateJump();
  if (!is_jump) {
    // 歩行中にする
    is_state_ = State::kWalk;
  }

  // 向きのセット
  float angle = kAngle270 - angle_h;
  SetAngle(angle + kAngle315);

  // 前方に移動
  float move_x = kZero;
  float move_y = kZero;
  float move_z = kMoveValue;
  angle = angle_h - kAngle270;
  // 表示座標の計算し、セット
  VECTOR move_adjust_front = CalcMovePosition(move_x, move_y, move_z, angle);

  // 右に移動
  move_x = kMoveValue;
  move_y = kZero;
  move_z = kZero;
  angle = angle_h - kAngle270;
  // 表示座標の計算し、セット
  VECTOR move_adjust_right = CalcMovePosition(move_x, move_y, move_z, angle);

  // 2つの移動量を加算し、1/√2倍して移動量を調整する
  VECTOR move_adjust = VAdd(move_adjust_front, move_adjust_right);
  move_adjust = VScale(move_adjust, kDiagonalRate);

  std::cout << "カメラ 角度  　：" << angle_h << std::endl;
  std::cout << "進行方向 角度　：" << angle << std::endl;
  std::cout << "プレイヤー 角度：" << angle_h + kAngle45 << std::endl;

  return move_adjust;
}

/// <summary>
/// 後ろに移動
/// </summary>
/// <param name="angle_h"> カメラ水平角度 </param>
/// <returns></returns>
VECTOR Player::MoveBack(float angle_h) {

  bool is_jump = IsStateJump();
  if (!is_jump) {
    // 歩行中にする
    is_state_ = State::kWalk;
  }

  // 注視点の移動量
  float move_x = kZero;
  float move_y = kZero;
  float move_z = kMoveValue;
  float angle = angle_h - kAngle90;

  // 向きのセット
  SetAngle(kAngle270 - angle);
  // 表示座標の計算し、セット
  VECTOR move_adjust = CalcMovePosition(move_x, move_y, move_z, angle);

  std::cout << "カメラ 角度  　：" << angle_h << std::endl;
  std::cout << "進行方向 角度　：" << angle << std::endl;
  std::cout << "プレイヤー 角度：" << kAngle90 - angle << std::endl;

  return move_adjust;
}


/// <summary>
/// 左後ろに移動
/// </summary>
/// <param name="angle_h"> 現在のカメラ水平角度 </param>
/// <returns> 移動量 </returns>
VECTOR Player::MoveBackLeft(float angle_h) {

  bool is_jump = IsStateJump();
  if (!is_jump) {
    // 歩行中にする
    is_state_ = State::kWalk;
  }

  // 向きのセット
  float angle = kAngle270 - angle_h;
  SetAngle(angle + kAngle135);

  // 後方に移動
  float move_x = kZero;
  float move_y = kZero;
  float move_z = kMoveValue;
  angle = angle_h - kAngle90;
  // 表示座標の計算し、セット
  VECTOR move_adjust_front = CalcMovePosition(move_x, move_y, move_z, angle);

  // 左に移動
  move_x = kMoveValue;
  move_y = kZero;
  move_z = kZero;
  angle = angle_h - kAngle90;
  // 表示座標の計算し、セット
  VECTOR move_adjust_right = CalcMovePosition(move_x, move_y, move_z, angle);

  // 2つの移動量を加算し、1/√2倍して移動量を調整する
  VECTOR move_adjust = VAdd(move_adjust_front, move_adjust_right);
  move_adjust = VScale(move_adjust, kDiagonalRate);

  std::cout << "カメラ 角度  　：" << angle_h << std::endl;
  std::cout << "進行方向 角度　：" << angle << std::endl;
  std::cout << "プレイヤー 角度：" << angle_h - kAngle135 << std::endl;

  return move_adjust;
}

/// <summary>
/// 右後ろに移動
/// </summary>
/// <param name="angle_h"> 現在のカメラ水平角度 </param>
/// <returns> 移動量 </returns>
VECTOR Player::MoveBackRight(float angle_h) {

  bool is_jump = IsStateJump();
  if (!is_jump) {
    // 歩行中にする
    is_state_ = State::kWalk;
  }

  // 向きのセット
  float angle = kAngle270 - angle_h;
  SetAngle(angle + kAngle45);

  // 後方に移動
  float move_x = kZero;
  float move_y = kZero;
  float move_z = kMoveValue;
  angle = angle_h - kAngle90;
  // 表示座標の計算し、セット
  VECTOR move_adjust_front = CalcMovePosition(move_x, move_y, move_z, angle);

  // 右に移動
  move_x = kMoveValue;
  move_y = kZero;
  move_z = kZero;
  angle = angle_h - kAngle270;
  // 表示座標の計算し、セット
  VECTOR move_adjust_right = CalcMovePosition(move_x, move_y, move_z, angle);

  // 2つの移動量を加算し、1/√2倍して移動量を調整する
  VECTOR move_adjust = VAdd(move_adjust_front, move_adjust_right);
  move_adjust = VScale(move_adjust, kDiagonalRate);

  std::cout << "カメラ 角度  　：" << angle_h << std::endl;
  std::cout << "進行方向 角度　：" << angle << std::endl;
  std::cout << "プレイヤー 角度：" << angle_h - kAngle225 << std::endl;

  return move_adjust;
}

/// <summary>
/// 右に移動
/// </summary>
/// <param name="angle_h"> カメラ水平角度 </param>
/// <returns></returns>
VECTOR Player::MoveRight(float angle_h) {

  bool is_jump = IsStateJump();
  if (!is_jump) {
    // 歩行中にする
    is_state_ = State::kWalk;
  }

  // 注視点の移動量
  float move_x = kMoveValue;
  float move_y = kZero;
  float move_z = kZero;
  float angle = angle_h - kAngle270;

  // 向きのセット
  SetAngle(kAngle0 - angle);
  // 表示座標の計算し、セット
  VECTOR move_adjust = CalcMovePosition(move_x, move_y, move_z, angle);

  std::cout << "カメラ 角度  　：" << angle_h << std::endl;
  std::cout << "進行方向 角度　：" << angle << std::endl;
  std::cout << "プレイヤー 角度：" << kAngle180 - angle << std::endl;

  return move_adjust;
}

/// <summary>
/// 左に移動
/// </summary>
/// <param name="angle_h"> カメラ水平角度 </param>
/// <returns></returns>
VECTOR Player::MoveLeft(float angle_h) {

  bool is_jump = IsStateJump();
  if (!is_jump) {
    // 歩行中にする
    is_state_ = State::kWalk;
  }

  // 注視点の移動量
  float move_x = kMoveValue;
  float move_y = kZero;
  float move_z = kZero;
  float angle = angle_h - kAngle90;

  // 向きのセット
  SetAngle(kAngle360 - angle);
  // 表示座標の計算し、セット
  VECTOR move_adjust = CalcMovePosition(move_x, move_y, move_z, angle);

  std::cout << "カメラ 角度  　：" << angle_h << std::endl;
  std::cout << "進行方向 角度　：" << angle << std::endl;
  std::cout << "プレイヤー 角度：" << kAngle360 - angle << std::endl;

  return move_adjust;
}

/// <summary>
/// 移動処理
/// </summary>
/// <param name="move_x"> X方向移動量 </param>
/// <param name="move_y"> Y方向移動量 </param>
/// <param name="move_z"> Z方向移動量 </param>
/// <param name="direction"> 移動方向 </param>
/// <returns></returns>
VECTOR Player::CalcMovePosition(float move_x, float move_y, float move_z,
                                float angle) {

  std::cout << "------------------------------------------------" << std::endl;
  std::cout << "プレイヤー座標【更新前】" << std::endl;
  std::cout << "X座標：" << pos_.x << std::endl;
  std::cout << "Y座標：" << pos_.y << std::endl;
  std::cout << "Z座標：" << pos_.z << std::endl;

  VECTOR move_adjust = VGet(move_x, move_y, move_z);

  // 向きに合わせて移動ベクトルを回転してから加算
  float sin_param = sinf(angle / kAngle180 * DX_PI_F);
  float cos_param = cosf(angle / kAngle180 * DX_PI_F);
  VECTOR temp_move;
  temp_move.x = cos_param * move_adjust.x - sin_param * move_adjust.z;
  temp_move.y = kZero;
  temp_move.z = sin_param * move_adjust.x + cos_param * move_adjust.z;
  move_adjust = temp_move;

  std::cout << "移動量" << std::endl;
  std::cout << "X座標 増加分：" << move_adjust.x << std::endl;
  std::cout << "Y座標 増加分：" << move_adjust.y << std::endl;
  std::cout << "Z座標 増加分：" << move_adjust.z << std::endl;

  return move_adjust;
}

/// <summary>
/// ジャンプする
/// </summary>
/// <param name=""></param>
/// <returns> true:ジャンプ実行, false:未実行 </returns>
bool Player::MoveJump() {

  // プレイヤーがジャンプ中なら処理終了
  bool is_jump = IsStateJump();
  if (is_jump) {
    return false;
  }

  // 状態を「ジャンプ中」にする
  SetState(State::kJump);
  // ジャンプしている時間を初期化
  jump_time_ = kResetTime;
  // 初速をセット
  velocity_init_ = kInitialJumpVelocity;
  // ジャンプ直前のプレイヤーの高さを取得
  y_pos_height_ = pos_.y;

  return true;
}