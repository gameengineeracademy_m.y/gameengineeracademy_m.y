﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "Game/PlayerControllerEventInterface.h"

/// <summary>
/// プレイヤーコントローラ
/// </summary>
class PlayerController : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kWait,               // 待機フェーズ
    kSwitch,             // コントローラ切り替えフェーズ
    kProcess,            // 処理中フェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

  /// <summary>
  /// キーの種類
  /// </summary>
  enum class KeyType {
    kSpace,            // スペースキー
    kLalt,             // 左ALTキー
    kW,                // Wキー
    kS,                // Sキー
    kA,                // Aキー
    kD,                // Dキー
    kKeyTypeMaxIndex   // キー項目数
  };

  /// <summary>
  /// キーの種類
  /// </summary>
  enum class KeyType2 {
    kP,                // Pキー
    kM,                // Mキー
    kN,                // Nキー
    kKeyTypeMaxIndex   // キー項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> プレイヤーコントローライベントインターフェース </param>
  /// <returns></returns>
  PlayerController(PlayerControllerEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~PlayerController();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 現在のフェーズの変更
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }


private:

  /// <summary>
  /// 左Altキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushLAltKey(float);

  /// <summary>
  /// Wキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushWKey(float);

  /// <summary>
  /// Sキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushSKey(float);

  /// <summary>
  /// Aキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushAKey(float);

  /// <summary>
  /// Dキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushDKey(float);

  /// <summary>
  /// Spaceキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushSpaceKey(float);

  /// <summary>
  /// Pキー押下処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushPKey();

  /// <summary>
  /// Mキー押下処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushMKey();

  /// <summary>
  /// Nキー押下処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushNKey();

  /// <summary>
  /// 指定のキー押下有無を取得する
  /// </summary>
  /// <param name="key_push"> キーが押されているかどうか </param>
  /// <param name="key_push_before"> キーが押されていたかどうか </param>
  /// <param name="time_sec"> 毎フレームの処理時間 </param>
  /// <param name="push_time_sec"> キー押下継続時間 </param> 
  /// <returns> true：押された, false：押されていない </returns>
  bool CheckPressKey(bool, bool&, float, float&);

  /// <summary>
  /// 指定のキー押下有無を取得する　※単発押し
  /// </summary>
  /// <param name="key_code"> キーコード </param>
  /// <param name="push_key"> キーの押下の有無 </param>
  /// <returns>キーが押された：false、キーが押されていない：false</returns>
  bool CheckPushKey(int, bool&);


private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// イベントインターフェース
  /// </summary>
  PlayerControllerEventInterface& event_interface_;

  /// <summary>
  /// ボタン長押し時間
  /// </summary>
  float key_press_keep_time_[static_cast<int>(KeyType::kKeyTypeMaxIndex)];

  /// <summary>
  /// キーの押下の有無
  /// </summary>
  bool push_key_[static_cast<int>(KeyType::kKeyTypeMaxIndex)];

  /// <summary>
  /// キーの押下の有無
  /// </summary>
  bool push_key2_[static_cast<int>(KeyType2::kKeyTypeMaxIndex)];
};