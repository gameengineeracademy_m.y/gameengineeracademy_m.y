﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "System/FontInfo.h"
#include "Game/GameInfo.h"
#include "Game/BattleInfoUiEventInterface.h"
#include "Game/Key.h"
#include <unordered_map>
#include <vector>
#include <string>
#include <iomanip>
#include <sstream>

/// <summary>
/// バトルUI情報
/// </summary>
class BattleInfoUi : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,      // 初期化処理フェーズ
    kStartWait,       // 起動待機フェーズ
    kStartUp,         // 起動フェーズ
    kProcess,         // 処理中フェーズ
    kBeforeFinalize,  // 終了処理前フェーズ
    kFinalize,        // 終了処理フェーズ
    kFinalized,       // 終了処理済みフェーズ
    kPhaseMaxIndex    // フェーズ数
  };

  /// <summary>
  /// フォントサイズ
  /// </summary>
  enum class FontSize {
    k20,                 // サイズ20
    kPhaseMaxIndex       // フェーズ項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> イベントインターフェース </param>
  /// <returns></returns>
  BattleInfoUi(BattleInfoUiEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~BattleInfoUi();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// 使用カメラ名セット
  /// </summary>
  /// <param name="name"> カメラ名 </param>
  /// <returns></returns>
  void SetCameraName(std::string name) { camera_name_ = name; }

  /// <summary>
  /// キーの取得
  /// </summary>
  /// <param name=""> キーの種類 </param>
  /// <returns> キー </returns>
  Key* GetKey(Key::KeyType key_type) { return key_list_[key_type]; }

  /// <summary>
  /// キーを押していることにする
  /// </summary>
  /// <param name=""> キーの種類 </param>
  /// <returns></returns>
  void SetPressKey(Key::KeyType);

  /// <summary>
  /// キーの破棄
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DisposeKey();

  /// <summary>
  /// キー操作表示設定をONにする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ShowDisplayKeyMenu() { is_disp_key_ = true; }

  /// <summary>
  /// キー操作表示設定をOFFにする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void HideDisplayKeyMenu() { is_disp_key_ = false; }

  /// <summary>
  /// キー操作が表示中かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:表示中, false:非表示 </returns>
  bool IsShowDisplay() { return is_disp_key_ == true; }

  /// <summary>
  /// デバッグ用の画面枠表示設定をONにする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ShowDisplayWindowFrame() { is_disp_frame_ = true; }

  /// <summary>
  /// デバッグ用の画面枠表示設定をOFFにする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void HideDisplayWindowFrame() { is_disp_frame_ = false; }

  /// <summary>
  /// プレイヤーマニュアルのセット
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetPlayerManual();

  /// <summary>
  /// デバッグマニュアルのセット
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetDebugManual();

  /// <summary>
  /// マニュアル表示をONにする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ShowOperationManual() { is_disp_manual_ = true; }

  /// <summary>
  /// マニュアル表示をOFFにする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void HideOperationManual() { is_disp_manual_ = false; }

  /// <summary>
  /// マニュアルが表示中かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:表示中, false:非表示 </returns>
  bool IsShowOperationManual() { return is_disp_manual_ == true; }

  /// <summary>
  /// キーの現在のフェーズの変更
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeKeyCurrentPhase(Key::PhaseType);

  /// <summary>
  /// 現在のフェーズの変更
  /// </summary>
  /// <param name="phase_type"> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }

  /// <summary>
  /// 現在のフェーズが「起動待機」かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:起動待機, false:それ以外 </returns>
  bool IsStartWait() { return current_phase_ == PhaseType::kStartWait; }

  /// <summary>
  /// 現在のフェーズが「処理中」かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:起動待機, false:それ以外 </returns>
  bool IsProcess() { return current_phase_ == PhaseType::kProcess; }

  /// <summary>
  /// 現在のフェーズが「終了処理済み」かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理済み, false:それ以外 </returns>
  bool IsFinalized() { return current_phase_ == PhaseType::kFinalized; }


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Initialize();

  /// <summary>
  /// 処理中処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Process(float);

  /// <summary>
  /// カメラ座標セット
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <param name=""> Z座標 </param>
  /// <returns></returns>
  void SetCameraPos(float, float, float);

  /// <summary>
  /// 注視点座標セット
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <param name=""> Z座標 </param>
  /// <returns></returns>
  void SetLookAtPos(float, float, float);

  /// <summary>
  /// プレイヤー座標セット
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <param name=""> Z座標 </param>
  /// <returns></returns>
  void SetPlayerPos(float, float, float);

  /// <summary>
  /// 角度セット
  /// </summary>
  /// <param name=""> 水平角度 </param>
  /// <param name=""> 垂直角度 </param>
  /// <returns></returns>
  void SetAngle(float, float);

  /// <summary>
  /// キーの生成
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void CreateKey();

  /// <summary>
  /// デバッグ用の画面枠表示中かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:表示中, false:非表示 </returns>
  bool IsShowFrame() { return is_disp_frame_ == true; }

  /// <summary>
  /// 透過率変更処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AdjustAlphaValue();

  /// <summary>
  /// 小数第n位四捨五入
  /// </summary>
  /// <param name=""> 対象の値 </param>
  /// <param name=""> 表示する位 </param>
  /// <returns> 小数値 </returns>
  float ExecuteRound(float, int);


private:

  /// <summary>
  /// 表示位置
  /// </summary>
  struct Pos {
    int x;
    int y;
  };

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// イベントインターフェース
  /// </summary>
  BattleInfoUiEventInterface& event_interface_;

  /// <summary>
  /// 使用カメラ名表示位置
  /// </summary>
  Pos pos_camera_name_;

  /// <summary>
  /// カメラ座標 表示位置
  /// </summary>
  Pos pos_camera_pos_;

  /// <summary>
  /// 注視点座標 表示位置
  /// </summary>
  Pos pos_look_;

  /// <summary>
  /// プレイヤー座標 表示位置
  /// </summary>
  Pos pos_player_pos_;

  /// <summary>
  /// 角度 表示位置
  /// </summary>
  Pos pos_angle_;

  /// <summary>
  /// 操作説明　その1
  /// </summary>
  Pos pos_manual_01_;

  /// <summary>
  /// 操作説明　その2
  /// </summary>
  Pos pos_manual_02_;

  /// <summary>
  /// 操作説明　その3
  /// </summary>
  Pos pos_manual_03_;

  /// <summary>
  /// 操作説明　その4
  /// </summary>
  Pos pos_manual_04_;

  /// <summary>
  /// 操作説明　その5
  /// </summary>
  Pos pos_manual_05_;

  /// <summary>
  /// 操作説明　その6
  /// </summary>
  Pos pos_manual_06_;

  /// <summary>
  /// 操作説明　その7
  /// </summary>
  Pos pos_manual_07_;

  /// <summary>
  /// 操作説明　その8
  /// </summary>
  Pos pos_manual_08_;

  /// <summary>
  /// 使用カメラ名
  /// </summary>
  std::string camera_name_;

  /// <summary>
  /// カメラの位置
  /// </summary>
  std::string camera_pos_value_;

  /// <summary>
  /// 注視点の位置
  /// </summary>
  std::string look_pos_value_;

  /// <summary>
  /// プレイヤーの位置
  /// </summary>
  std::string player_pos_value_;

  /// <summary>
  /// 角度
  /// </summary>
  std::string angle_value_;

  /// <summary>
  /// 操作説明　その1
  /// </summary>
  std::string manual_01_;

  /// <summary>
  /// 操作説明　その2
  /// </summary>
  std::string manual_02_;

  /// <summary>
  /// 操作説明　その3
  /// </summary>
  std::string manual_03_;

  /// <summary>
  /// 操作説明　その4
  /// </summary>
  std::string manual_04_;

  /// <summary>
  /// 操作説明　その5
  /// </summary>
  std::string manual_05_;

  /// <summary>
  /// 操作説明　その6
  /// </summary>
  std::string manual_06_;

  /// <summary>
  /// 操作説明　その7
  /// </summary>
  std::string manual_07_;

  /// <summary>
  /// 操作説明　その8
  /// </summary>
  std::string manual_08_;

  /// <summary>
  /// キーリスト
  /// </summary>
  std::unordered_map<Key::KeyType, Key*> key_list_;

  /// <summary>
  /// キー情報表示有無
  /// </summary>
  bool is_disp_key_;

  /// <summary>
  /// 画面枠表示有無
  /// </summary>
  bool is_disp_frame_;

  /// <summary>
  /// マニュアル表示有無
  /// </summary>
  bool is_disp_manual_;

  /// <summary>
  /// フォントハンドル
  /// </summary>
  std::vector<int> font_handle_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_front_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_key_back_;
};