﻿#pragma once

#include "System/Task.h"
#include "Game/GameInfo.h"
#include "Game/Field.h"
#include "Game/FieldId.h"
#include <vector>
#include <unordered_map>
#include <fstream>
#include <string>
#include <sstream>

/// <summary>
/// フィールドマネージャ
/// </summary>
/// <remarks>
/// 壁や床などフェールド処理の管理
/// </remarks>
class FieldManager : public Task {
public:

  /// <summary>
  /// フェーズの処理
  /// </summary>
  enum class PhaseType {
    kWait,             // 待機フェーズ
    kInitialize,       // 初期化処理フェーズ
    kStartWait,        // 起動待機フェーズ
    kStartUp,          // 起動フェーズ
    kProcess,          // 処理中フェーズ
    kBeforeFinalize,   // 終了処理前フェーズ
    kFinalize,         // 終了処理フェーズ
    kFinalized,        // 終了処理済みフェーズ
    kPhaseMaxIndex     // フェーズ数
  };

  /// <summary>
  /// 初期化フェーズの処理
  /// </summary>
  enum class InitPhase {
    kFieldMapLoad,     // フィールドマップロード
    kListRegister,     // リスト登録
    kFloorCreate,      // 床生成
    kOutWallCreate,    // 外壁生成
    kInnerWallCreate,  // 内壁生成
    kError,            // 初期化エラーフェーズ
    kFinalized,        // 終了処理済みフェーズ
    kPhaseMaxIndex     // フェーズ数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  FieldManager();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~FieldManager();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// フィールドリストへの登録
  /// </summary>
  /// <param name=""> フィールドID </param>
  /// <returns> フィールド </returns>
  Field* RegisterField(FieldId);

  /// <summary>
  /// フィールドの生成
  /// </summary>
  /// <param name=""> フィールドID </param>
  /// <returns> フィールド </returns>
  Field* CreateField(FieldId);

  /// <summary>
  /// 壁との衝突判定
  /// </summary>
  /// <param name=""> 判定対象物の座標 </param>
  /// <param name=""> 対象物の幅 </param>
  /// <param name=""> 対象物の奥行 </param>
  /// <param name=""> 対象物の水平角度 </param>
  /// <param name=""> 接触面のベクトル </param>
  /// <returns> true:衝突した, false:衝突してない </returns>
  bool CheckCollisionWall(VECTOR, float, float, float, VECTOR&);

  /// <summary>
  /// 床との衝突判定
  /// </summary>
  /// <param name=""> 判定対象物の座標 </param>
  /// <param name=""> 床の高さ </param>
  /// <returns> true:衝突した, false:衝突してない </returns>
  bool CheckCollisionFloor(VECTOR, float&);

  /// <summary>
  /// フィールドマップの破棄
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DisposeFieldData();

  /// <summary>
  /// フィールドリストの破棄
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DisposeFieldList();

  /// <summary>
  /// 現在のフェーズの変更
  /// </summary>
  /// <param name="phase_type"> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }

  /// <summary>
  /// 初期化フェーズの変更
  /// </summary>
  /// <param name="phase_type"> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeInitializePhase(InitPhase phase_type) { initialize_phase_ = phase_type; }

  /// <summary>
  /// 現在のフェーズが「起動待機」かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:起動待機, false:それ以外 </returns>
  bool IsStartWait() { return current_phase_ == PhaseType::kStartWait; }

  /// <summary>
  /// 現在のフェーズが「終了処理済み」かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理済み, false:それ以外 </returns>
  bool IsFinalized() { return current_phase_ == PhaseType::kFinalized; }


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Initialize();

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Process();

  /// <summary>
  /// CSVファイルの読み取り
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, flase:処理中 </returns>
  bool ReadCsvFile();

  /// <summary>
  /// 四つ角のセット
  /// </summary>
  /// <param name=""> 幅 </param>
  /// <param name=""> 奥行 </param>
  /// <param name=""> 角度 </param>
  /// <param name=""> 中心座標(XZ平面) </param>
  /// <returns></returns>
  std::vector<VECTOR> CalcObjectCorner(float, float, float, VECTOR);

  /// <summary>
  /// 線分どうしの接触判定
  /// </summary>
  /// <param name=""> 対象物 始点 </param>
  /// <param name=""> 対象物 終点 </param>
  /// <param name=""> 壁の線分 始点 </param>
  /// <param name=""> 壁の線分 終点 </param>
  /// <returns> true:接触した, false:接触してない </returns>
  bool CheckCollisionLineSegment(VECTOR, VECTOR, VECTOR, VECTOR);

  /// <summary>
  /// 外積計算
  /// </summary>
  /// <param name=""> ベクトルA </param>
  /// <param name=""> ベクトルB </param>
  /// <param name=""> ベクトルC </param>
  /// <returns> true:接触した, false:接触してない </returns>
  bool CalcCrossProduct(VECTOR, VECTOR, VECTOR);


private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 初期化フェーズ
  /// </summary>
  InitPhase initialize_phase_;

  /// <summary>
  /// フィールドリスト
  /// </summary>
  std::unordered_map<FieldId, Field*> field_list_;

  /// <summary>
  /// フィールド情報 CSV読み取り値
  /// </summary>
  std::vector<std::vector<int>> field_csv_;

  /// <summary>
  /// フィールド情報 床
  /// </summary>
  std::vector<std::vector<Field*>> field_floor_;

  /// <summary>
  /// フィールド情報 壁
  /// </summary>
  std::unordered_map<int, Field*> field_wall_;

  /// <summary>
  /// 壁インデックス
  /// </summary>
  int index_wall_;
};