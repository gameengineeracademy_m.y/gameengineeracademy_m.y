﻿#include "Game/ResultLevel.h"

namespace {

  /// <summary>
  /// 累積時間のリセット値
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 待機時間
  /// </summary>
  const float kWaitTime = 1.0f;

  /// <summary>
  /// タイトルテキスト
  /// </summary>
  const char* kTitleText = "RESULT LEVEL";

  /// <summary>
  /// 案内文テキスト
  /// </summary>
  const char* kGuideText = "Auto Transition";

  /// <summary>
  /// 文字フォント名
  /// </summary>
  const char* kFontName = "メイリオ";

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSize = 45;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 6;

  /// <summary>
  /// 文字の色
  /// </summary>
  const int kTextForeColor = GetColor(255, 255, 255);   // 白色

  /// <summary>
  /// テキスト表示位置
  /// </summary>
  const int kTextPosY = 50;

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjustInit = 5;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> タスクID </param>
/// <param name=""> タスクマネージャ </param>
/// <returns></returns>
ResultLevel::ResultLevel(TaskId task_id, TaskManager& task_manager)
  : Level(task_id, task_manager)
  , current_phase_(PhaseType::kNone)
  , finalize_phase_(FinalizePhaseType::kUnloadTask)
  , title_pos_()
  , guide_pos_()
  , font_handle_()
  , accumulate_time_(0.0f)
  , alpha_(0) {

  // コンソールに出力
  std::cout << "ResultLevel コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ResultLevel::~ResultLevel() {

  // コンソールに出力
  std::cout << "~ResultLevel デストラクタ" << std::endl;
}

/// <summary>
/// 初期化処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool ResultLevel::InitializePhase(float process_time) {

  // 処理時間をリセットしておく
  accumulate_time_ = kResetTime;
  // 透過率 初期設定
  alpha_ = kAlphaMin;

  // ゲーム情報生成
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    std::cout << "ResultLevel InitializePhase GameInfoインスタンス無し" << std::endl;
    return false;
  }
  int x_pos_center = game_info->GetCenterPointX();
  int y_pos_center = game_info->GetCenterPointY();

  // フォント情報生成
  FontInfo* font_info = FontInfo::GetFontInfoInstance();
  if (font_info == nullptr) {
    std::cout << "ResultLevel InitializePhase FontInfoインスタンス無し" << std::endl;
    return false;
  }
  int font_handle = font_info->GetFontInfo(kFontName, kFontSize, kFontWidth, DX_FONTTYPE_NORMAL);
  font_handle_.push_back(font_handle);

  // タイトルテキスト表示位置
  int text_width = GetDrawStringWidthToHandle(kTitleText, static_cast<int>(strlen(kTitleText)), font_handle_.at(static_cast<int>(FontSize::k45)));
  title_pos_.x = x_pos_center - (text_width / kHalfValue);
  title_pos_.y = y_pos_center - kTextPosY;
  // 説明文テキスト表示位置
  text_width = GetDrawStringWidthToHandle(kGuideText, static_cast<int>(strlen(kGuideText)), font_handle_.at(static_cast<int>(FontSize::k45)));
  guide_pos_.x = x_pos_center - (text_width / kHalfValue);
  guide_pos_.y = y_pos_center + kTextPosY;


  return true;
}

/// <summary>
/// 毎フレームの処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool ResultLevel::UpdatePhase(float process_time) {

  switch (current_phase_) {
  case PhaseType::kNone: {
    //-------------------------------
    //何もしないフェーズ
    //-------------------------------
    // 現在のフェーズを「起動」に変更
    ChangeCurrentPhase(PhaseType::kStartUp);
    break;
  }
  case PhaseType::kStartUp: {
    //-------------------------------
    //起動フェーズ
    //-------------------------------
    // フェードイン処理完了後、現在のフェーズを「処理中」に変更
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //-------------------------------
    //処理中
    //-------------------------------
    // 累積時間に加算し、指定時間後フェーズ切り替え
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 現在のフェーズを「遷移前」に変更する
      ChangeCurrentPhase(PhaseType::kBeforeTransition);
    }
    break;
  }
  case PhaseType::kBeforeTransition: {
    //-------------------------------
    //遷移前フェーズ
    //-------------------------------
    // 現在のフェーズを「レベル遷移」に変更する
    ChangeCurrentPhase(PhaseType::kLevelTransition);
    break;
  }
  case PhaseType::kLevelTransition: {
    //-------------------------------
    //レベル遷移フェーズ
    //-------------------------------
    // フェードアウト処理完了後、現在のフェーズを「終了処理」に変更
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更する
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //-------------------------------
    //終了処理フェーズ
    //-------------------------------
    // ブートレベルへ切り替える
    ChangeLevel(TaskId::kBootLevel);
    // 現在のフェーズを「終了処理済み」に変更する
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }

  return false;
}

/// <summary>
/// 毎フレームの描画処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ResultLevel::RenderPhase() {

  switch (current_phase_) {
  case PhaseType::kStartUp:
  case PhaseType::kProcess:
  case PhaseType::kBeforeTransition:
  case PhaseType::kLevelTransition: {

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

    DrawStringToHandle(title_pos_.x, title_pos_.y, kTitleText, kTextForeColor, font_handle_.at(static_cast<int>(FontSize::k45)));
    DrawStringToHandle(guide_pos_.x, guide_pos_.y, kGuideText, kTextForeColor, font_handle_.at(static_cast<int>(FontSize::k45)));

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    break;
  }
  }

}

/// <summary>
/// 終了処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool ResultLevel::FinishPhase(float process_time) {

  switch (finalize_phase_) {
  case FinalizePhaseType::kUnloadTask: {
    //-------------------------------
    //タスクを降ろすフェーズ
    //-------------------------------
    // 終了フェーズをタスク破棄フェーズに変更
    ChangeFinalizePhase(FinalizePhaseType::kDisposeTask);

    return false;
  }
  case FinalizePhaseType::kDisposeTask: {
    //-------------------------------
    //タスクを破棄するフェーズ
    //-------------------------------
    break;
  }
  }

  return true;
}

/// <summary>
/// 透過率変更処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool ResultLevel::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率の調整
  switch (current_phase_) {
  case PhaseType::kStartUp: {
    // 起動フェーズ
    alpha_ += kAlphaAdjustInit;
    break;
  }
  case PhaseType::kLevelTransition: {
    // 終了処理前フェーズ
    alpha_ -= kAlphaAdjustInit;
    break;
  }
  }

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= kAlphaMax) {
    alpha_ = kAlphaMax;
    is_finish = true;
  }
  else if (alpha_ <= kAlphaMin) {
    alpha_ = kAlphaMin;
    is_finish = true;
  }

  return is_finish;
}