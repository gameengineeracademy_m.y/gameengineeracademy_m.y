﻿#pragma once

#include "DxLib.h"
#include "System/Level.h"
#include "System/FontInfo.h"
#include "Game/GameInfo.h"
#include <vector>

/// <summary>
/// ブートレベル
/// </summary>
/// <remarks>
/// ブート画面のタスク
/// </remarks>
class BootLevel : public Level {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kNone,               // 何もしないフェーズ
    kStartUp,            // 起動フェーズ
    kProcess,            // 処理中フェーズ
    kBeforeTransition,   // レベル遷移前フェーズ
    kLevelTransition,    // レベル遷移フェーズ
    kFinalize,           // 終了処理フェーズ
    kFinalized,          // 終了処理済みフェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

  /// <summary>
  /// 終了フェーズの種類
  /// </summary>
  enum class FinalizePhaseType {
    kUnloadTask,         // タスクを降ろすフェーズ
    kDisposeTask,        // タスクを破棄するフェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

  /// <summary>
  /// フォントサイズ
  /// </summary>
  enum class FontSize {
    k45,                 // サイズ45
    kPhaseMaxIndex       // フェーズ項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> タスクID </param>
  /// <param name=""> タスクマネージャ </param>
  /// <returns></returns>
  BootLevel(TaskId, TaskManager&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~BootLevel();


protected:

  /// <summary>
  /// 初期化処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool InitializePhase(float) override;

  /// <summary>
  /// 毎フレームの処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool UpdatePhase(float) override;

  /// <summary>
  /// 毎フレームの描画処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void RenderPhase() override;

  /// <summary>
  /// 終了処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool FinishPhase(float) override;


private:

  /// <summary>
  /// 現在のフェーズの変更
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; };

  /// <summary>
  /// 終了処理フェーズの変更
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeFinalizePhase(FinalizePhaseType finalize_phase) { finalize_phase_ = finalize_phase; };

  /// <summary>
  /// 透過率変更処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AdjustAlphaValue();


private:

  /// <summary>
  /// 表示座標
  /// </summary>
  struct Pos {
    int x;
    int y;
  };

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 終了フェーズの種類
  /// </summary>
  FinalizePhaseType finalize_phase_;

  /// <summary>
  /// タイトル 表示位置
  /// </summary>
  Pos title_pos_;

  /// <summary>
  /// 遷移方法 表示位置
  /// </summary>
  Pos guide_pos_;

  /// <summary>
  /// フォントハンドル
  /// </summary>
  std::vector<int> font_handle_;

  /// <summary>
  /// 累積処理時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_;
};