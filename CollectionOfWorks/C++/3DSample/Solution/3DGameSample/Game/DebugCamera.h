﻿#pragma once

#include "DxLib.h"
#include "Game/CameraBase.h"
#include <iostream>

/// <summary>
/// デバッグカメラ
/// </summary>
class DebugCamera : public CameraBase {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  DebugCamera();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~DebugCamera();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Update();

  /// <summary>
  /// 注視点を前進
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void MoveFrontGaze();

  /// <summary>
  /// 注視点を後退
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void MoveBackGaze();

  /// <summary>
  /// 注視点を上方向へ移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void MoveUpGaze();

  /// <summary>
  /// 注視点を下方向へ移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void MoveDownGaze();

  /// <summary>
  /// 注視点を左方向へ移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void MoveLeftGaze();

  /// <summary>
  /// 注視点を右方向へ移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void MoveRightGaze();


private:


};