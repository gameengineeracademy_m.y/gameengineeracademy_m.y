﻿#include "Game/PlayerCamera.h"

namespace {

  /// <summary>
  /// カメラ移動倍率
  /// </summary>
  const float kCameraMoveRate = 0.05f;

  /// <summary>
  /// カメラ角度移動量
  /// </summary>
  const float kCameraAngleMove = 1.0f;

  /// <summary>
  /// マイナス
  /// </summary>
  const float kSignMinus = -1.0f;

  /// <summary>
  /// 距離ゼロ
  /// </summary>
  const float kZero = 0.0f;

  /// <summary>
  /// 累乗
  /// </summary>
  const float kSquare = 2.0f;

  /// <summary>
  /// 半分にする値
  /// </summary>
  const float kHalfValuef = 2.0f;

  /// <summary>
  /// 0°
  /// </summary>
  const float kAngle0 = 0.0f;

  /// <summary>
  /// 1°
  /// </summary>
  const float kAngle1 = 1.0f;

  /// <summary>
  /// 90°
  /// </summary>
  const float kAngle90 = 90.0f;

  /// <summary>
  /// 179°
  /// </summary>
  const float kAngle179 = 179.0f;

  /// <summary>
  /// 180°
  /// </summary>
  const float kAngle180 = 180.0f;

  /// <summary>
  /// 360°
  /// </summary>
  const float kAngle360 = 360.0f;

  /// <summary>
  /// プレイヤーと注視点距離 最低値
  /// </summary>
  const float kDistanceMin = 120.0f;

  /// <summary>
  /// プレイヤーと注視点距離 最低値
  /// </summary>
  const float kDistanceMax = 800.0f;
}


/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
PlayerCamera::PlayerCamera()
  : CameraBase()
  , distance_(0.0f) {

  // コンソールに出力
  std::cout << "PlayerCamera コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
PlayerCamera::~PlayerCamera() {

  // コンソールに出力
  std::cout << "~PlayerCamera デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void PlayerCamera::Update(float process_time) {

  // カメラの画角設定処理
  CameraBase::Update();
}

/// <summary>
/// カメラを注視点に近づける(拡大)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void PlayerCamera::ExpandObject() {

  // カメラと注視点の座標を取得
  VECTOR pos_camera = GetCameraPosition();
  VECTOR pos_look = GetLookAtPosition();
  // カメラ座標から注視点座標を引いたベクトルを取得
  VECTOR pos_diff = VSub(pos_camera, pos_look);
  // スケーリングを実行し、調整値のベクトルを取得
  VECTOR pos_cam_adjust = VScale(pos_diff, kCameraMoveRate);
  // カメラ座標に調整値を減算
  pos_camera = VSub(pos_camera, pos_cam_adjust);

  // 新しいカメラ位置と注視点の距離を算出
  // 指定距離より近づいていた場合、処理終了
  pos_diff = VSub(pos_camera, pos_look);
  float distance = VSize(pos_diff);
  if (distance <= kDistanceMin) {
    return;
  }

  // 目標位置の設定
  SetCameraPositionX(pos_camera.x);
  SetCameraPositionY(pos_camera.y);
  SetCameraPositionZ(pos_camera.z);
}

/// <summary>
/// カメラを注視点から遠ざける(縮小)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void PlayerCamera::ShrinkObject() {

  // カメラと注視点の座標を取得
  VECTOR pos_camera = GetCameraPosition();
  VECTOR pos_look = GetLookAtPosition();
  // カメラ座標から注視点座標を引いたベクトルを取得
  VECTOR pos_diff = VSub(pos_camera, pos_look);
  // スケーリングを実行し、調整値のベクトルを取得
  VECTOR pos_cam_adjust = VScale(pos_diff, kCameraMoveRate);
  // カメラ座標に調整値を加算
  pos_camera = VAdd(pos_camera, pos_cam_adjust);

  // 新しいカメラ位置と注視点の距離を算出
  // 指定距離より遠ざかっていた場合、処理終了
  pos_diff = VSub(pos_camera, pos_look);
  float distance = VSize(pos_diff);
  if (distance >= kDistanceMax) {
    return;
  }

  // 位置の設定
  SetCameraPositionX(pos_camera.x);
  SetCameraPositionY(pos_camera.y);
  SetCameraPositionZ(pos_camera.z);
}

/// <summary>
/// 注視点を中心に上方向へ移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void PlayerCamera::MoveUpCenteredOnGaze() {

  // 垂直方向の角度を取得、調整後再度セット
  float angle = GetVerticalAngle();
  angle -= kCameraAngleMove;
  if (angle <= kAngle1) {
    angle = kAngle1;
  }
  SetVerticalAngle(angle);

  // 座標計算処理
  CalcPositionCamera();
}

/// <summary>
/// 注視点を中心に下方向へ移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void PlayerCamera::MoveDownCenteredOnGaze() {

  // 垂直方向の角度を取得、調整後再度セット
  float angle = GetVerticalAngle();
  angle += kCameraAngleMove;
  if (angle >= kAngle179) {
    angle = kAngle179;
  }
  SetVerticalAngle(angle);

  // 座標計算処理
  CalcPositionCamera();
}

/// <summary>
/// 注視点を中心に左方向へ移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void PlayerCamera::MoveLeftCenteredOnGaze() {

  // 水平方向の角度を取得、調整後再度セット
  float angle = GetHorizonAngle();
  angle -= kCameraAngleMove;
  if (angle <= kAngle0) {
    angle += kAngle360;
  }
  SetHorizonAngle(angle);

  // 座標計算処理
  CalcPositionCamera();
}

/// <summary>
/// 注視点を中心に右方向へ移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void PlayerCamera::MoveRightCenteredOnGaze() {

  // 水平方向の角度を取得、調整後再度セット
  float angle = GetHorizonAngle();
  angle += kCameraAngleMove;
  if (angle >= kAngle360) {
    angle -= kAngle360;
  }
  SetHorizonAngle(angle);

  // 座標計算処理
  CalcPositionCamera();
}

/// <summary>
/// カメラの位置変更
/// </summary>
/// <param name="debug_camera"> カメラ </param>
/// <returns></returns>
void PlayerCamera::CalcPositionCamera() {

  // カメラの座標を取得
  VECTOR pos_cam = GetCameraPosition();
  // 注視点の座標を取得
  VECTOR pos_look = GetLookAtPosition();
  // カメラの角度を取得
  float angle_horizon = GetHorizonAngle();
  float angle_vertical = GetVerticalAngle();

  std::cout << " --------------------------------------------- " << std::endl;
  std::cout << " カメラ座標 X座標： " << pos_cam.x << std::endl;
  std::cout << " カメラ座標 Y座標： " << pos_cam.y << std::endl;
  std::cout << " カメラ座標 Z座標： " << pos_cam.z << std::endl;
  std::cout << " 注視点座標 X座標： " << pos_look.x << std::endl;
  std::cout << " 注視点座標 Y座標： " << pos_look.y << std::endl;
  std::cout << " 注視点座標 Z座標： " << pos_look.z << std::endl;
  std::cout << " カメラ角度 水平 ： " << angle_horizon << std::endl;
  std::cout << " カメラ角度 垂直 ： " << angle_vertical << std::endl;

  // カメラと注視点の距離
  distance_ = std::sqrtf(std::powf((pos_cam.x - pos_look.x), kSquare) +
                         std::powf((pos_cam.y - pos_look.y), kSquare) +
                         std::powf((pos_cam.z - pos_look.z), kSquare));

  std::cout << " カメラ注視点距離： " << distance_ << std::endl;

  VECTOR temp_pos1;
  VECTOR temp_pos2;

  // 垂直方向の角度から座標を算出(Z軸回転)
  float sin_param = sinf(angle_vertical / (kAngle360 / kHalfValuef) * DX_PI_F);
  float cos_param = cosf(angle_vertical / (kAngle360 / kHalfValuef) * DX_PI_F);
  temp_pos1.x = sin_param * distance_;
  temp_pos1.y = cos_param * distance_;
  temp_pos1.z = kZero;

  std::cout << " sinθ 垂直角度： " << sin_param << std::endl;
  std::cout << " cosθ 垂直角度： " << cos_param << std::endl;
  std::cout << " 算出値座標 X座標： " << temp_pos1.x << std::endl;
  std::cout << " 算出値座標 Y座標： " << temp_pos1.y << std::endl;
  std::cout << " 算出値座標 Z座標： " << temp_pos1.z << std::endl;

  // 水平方向の角度から座標を算出(Y軸回転)
  sin_param = sinf(angle_horizon / (kAngle360 / kHalfValuef) * DX_PI_F);
  cos_param = cosf(angle_horizon / (kAngle360 / kHalfValuef) * DX_PI_F);
  temp_pos2.x = cos_param * temp_pos1.x - sin_param * temp_pos1.z;
  temp_pos2.y = temp_pos1.y;
  temp_pos2.z = sin_param * temp_pos1.x + cos_param * temp_pos1.z;

  std::cout << " sinθ 水平角度： " << sin_param << std::endl;
  std::cout << " cosθ 水平角度： " << cos_param << std::endl;
  std::cout << " 算出値座標 X座標： " << temp_pos2.x << std::endl;
  std::cout << " 算出値座標 Y座標： " << temp_pos2.y << std::endl;
  std::cout << " 算出値座標 Z座標： " << temp_pos2.z << std::endl;

  // 算出した座標に注視点の位置を加算し、カメラの位置を算出する
  VECTOR camera_pos = VAdd(temp_pos2, pos_look);

  std::cout << " カメラ座標 X座標： " << camera_pos.x << std::endl;
  std::cout << " カメラ座標 Y座標： " << camera_pos.y << std::endl;
  std::cout << " カメラ座標 Z座標： " << camera_pos.z << std::endl;

  // 各座標値をカメラにセットする
  SetCameraPositionX(camera_pos.x);
  SetCameraPositionY(camera_pos.y);
  SetCameraPositionZ(camera_pos.z);
}