﻿#include "System/MenuController.h"

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> メニューコントローライベントインターフェース </param>
/// <returns></returns>
MenuController::MenuController(MenuControllerEventInterface& menu_controller_event_interface)
  : Task(TaskId::kMenuController)
  , menu_controller_event_interface_(menu_controller_event_interface)
  , push_key_{ false, false } {

  // コンソールに出力
  std::cout << "MenuController コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
MenuController::~MenuController() {

  // コンソールに出力
  std::cout << "~MenuController デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void MenuController::Update(float process_time) {

  bool is_enter = GetPushEnterKey();
  bool is_escape = GetPushEscapeKey();

  if (is_enter) {
    // Enterキー押下処理イベント実行
    menu_controller_event_interface_.OnPushMenuControllerEnterKey();
  }
  else if (is_escape) {
    // Escapeキー押下処理イベント実行
    menu_controller_event_interface_.OnPushMenuControllerEscapeKey();
  }
}

/// <summary>
/// Enterキー押下処理
/// </summary>
/// <param name=""></param>
/// <returns> true：押された, false：押されていない </returns>
bool MenuController::GetPushEnterKey() {

  return CheckPushKey(KEY_INPUT_RETURN, push_key_[static_cast<int>(KeyType::kEnter)]);
}

/// <summary>
/// Esacapeキー押下処理
/// </summary>
/// <param name=""></param>
/// <returns> true：押された, false：押されていない </returns>
bool MenuController::GetPushEscapeKey() {

  return CheckPushKey(KEY_INPUT_ESCAPE, push_key_[static_cast<int>(KeyType::kEscape)]);
}

/// <summary>
/// 指定のキー押下有無を取得する　※離したら押されたことにする
/// </summary>
/// <param name="key_code"> キーコード </param>
/// <param name="push_key"> キーの押下の有無 </param>
/// <returns>キーが押された：false、キーが押されていない：false</returns>
bool MenuController::CheckPushKey(int key_code, bool& push_key) {

  // 指定のボタンが押されている
  if (CheckHitKey(key_code)) {

    // 直前まで押されていない場合は押されていることにする
    if (push_key == false) {
      push_key = true;
      // 判定を押されたとして返す
      return true;
    }
  }
  else {
    // 前回処理時にキーが押されていた場合は押されていないことにする
    if (push_key) {
      push_key = false;
    }
  }

  return false;
}