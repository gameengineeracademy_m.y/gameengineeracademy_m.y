﻿#include "System/Level.h"

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> タスクID </param>
/// <returns></returns>
Level::Level(TaskId task_id, TaskManager& task_manager)
  : Task(task_id)
  , task_manager_(task_manager)
  , current_phase_(PhaseType::kNone)
  , next_level_task_id_(TaskId::kDummyTask)
  , is_error_(false) {

  // コンソールに出力
  std::cout << "Level コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Level::~Level() {

  // コンソールに出力
  std::cout << "~Level デストラクタ" << std::endl;
}

/// <summary>
/// 1フレームの処理を実行する
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void Level::Update(float process_time) {

  bool change_phase = false;

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //--------------------------
    //初期化処理フェーズ
    //--------------------------
    change_phase = InitializePhase(process_time);
    if (change_phase) {
      // 初期化処理済みフェーズへ変更
      ChangeCurrentPhase(PhaseType::kInitialized);
    }
    break;
  }
  case PhaseType::kProcess:
  case PhaseType::kRequestFinish: {
    //--------------------------
    //処理中・終了リクエストフェーズ
    //--------------------------
    change_phase = UpdatePhase(process_time);
    if (change_phase) {
      // 終了フェーズへ変更
      ChangeCurrentPhase(PhaseType::kFinish);
    }
    break;
  }
  case PhaseType::kFinish: {
    //--------------------------
    //終了フェーズ
    //--------------------------
    change_phase = FinishPhase(process_time);
    if (change_phase) {
      // 初期化処理済みフェーズへ変更
      ChangeCurrentPhase(PhaseType::kFinished);
    }
    break;
  }
  }
}

/// <summary>
/// 1フレームの描画処理を実行する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Level::Render() {

  // 描画処理
  RenderPhase();
}

/// <summary>
/// レベルを切り替える
/// </summary>
/// <param name="task_id"> 次のレベル </param>
/// <returns></returns>
bool Level::ChangeLevel(TaskId task_id) {

  // 次のレベルが設定されているなら処理終了
  if (next_level_task_id_ != TaskId::kDummyTask) {
    return false;
  }

  // 次のレベルのタスクIDを設定
  next_level_task_id_ = task_id;
  // レベル側のフェーズを終了リクエストフェーズに変更
  ChangeCurrentPhase(PhaseType::kRequestFinish);

  return true;
}

/// <summary>
/// 現在のフェーズの変更
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Level::ChangeCurrentPhase(PhaseType phase_type) {

  current_phase_ = phase_type;
}

/// <summary>
/// エラー発生
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Level::OccurredError() {

  // エラーフラグを立てる
  is_error_ = true;

  // レベルのフェーズを終了フェーズに変更する
  ChangeCurrentPhase(PhaseType::kFinish);
}