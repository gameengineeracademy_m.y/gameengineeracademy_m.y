﻿#pragma once

#include <iostream>

/// <summary>
/// メニューコントローライベントインターフェース
/// </summary>
class MenuControllerEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  MenuControllerEventInterface() {

    // コンソールに出力
    std::cout << "MenuControllerEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~MenuControllerEventInterface() {

    // コンソールに出力
    std::cout << "~MenuControllerEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// Enterキーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushMenuControllerEnterKey() {};

  /// <summary>
  /// Escapeキーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushMenuControllerEscapeKey() {};


private:

};