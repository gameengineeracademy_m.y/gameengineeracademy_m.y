﻿#pragma once
#include <windows.h>
#include <stdio.h>

/// <summary>
/// ログコンソール
/// </summary>
class LogConsole {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  LogConsole();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~LogConsole();

  /// <summary>
  /// コンソールの生成
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void CreateConsole();

  /// <summary>
  /// コンソールの破棄
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ReleaseConsole();


private:

  /// <summary>
  /// 標準出力のファイルハンドル
  /// </summary>
  FILE* std_output_;

  /// <summary>
  /// 標準エラー出力のファイルハンドル
  /// </summary>
  FILE* std_error_output_;
};