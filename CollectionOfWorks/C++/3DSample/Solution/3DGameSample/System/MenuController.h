﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "System/MenuControllerEventInterface.h"

/// <summary>
/// メニューコントローラ
/// </summary>
/// <remarks>
/// 仮想コントローラ
/// </remarks>
class MenuController : public Task {
public:

  /// <summary>
  /// キーの種類
  /// </summary>
  enum class KeyType {
    kEnter,               // Enterキー
    kEscape,              // Escapeキー
    kKeyTypeMaxIndex      // キー項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> メニューコントローライベントインターフェース </param>
  /// <returns></returns>
  MenuController(MenuControllerEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~MenuController();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// Enterキー押下処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushEnterKey();

  /// <summary>
  /// Escapeキー押下処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushEscapeKey();


private:

  /// <summary>
  /// 指定のキー押下有無を取得する　※離したら押されたことにする
  /// </summary>
  /// <param name="key_code"> キーコード </param>
  /// <param name="push_key"> キーの押下の有無 </param>
  /// <returns>キーが押された：false、キーが押されていない：false</returns>
  bool CheckPushKey(int, bool&);


private:

  /// <summary>
  /// メニューコントローラのイベントインターフェース
  /// </summary>
  MenuControllerEventInterface& menu_controller_event_interface_;

  /// <summary>
  /// キーの押下の有無
  /// </summary>
  bool push_key_[static_cast<int>(KeyType::kKeyTypeMaxIndex)];
};