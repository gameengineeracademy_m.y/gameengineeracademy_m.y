﻿#include "System/LevelChanger.h"

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name="task_manager"> タスクマネージャ </param>
/// <returns></returns>
LevelChanger::LevelChanger(TaskManager& task_manager)
  : Task(TaskId::kLevelChanger)
  , task_manager_(task_manager)
  , current_phase_(PhaseType::kInitialize)
  , transition_phase_(LevelTransitionPhase::kNone)
  , current_level_(nullptr)
  , next_level_task_id_(TaskId::kDummyTask) {

  // コンソールに出力
  std::cout << "LevelChanger コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
LevelChanger::~LevelChanger() {

  // コンソールに出力
  std::cout << "~LevelChanger デストラクタ" << std::endl;
}

/// <summary>
/// 1フレームの処理を実行する
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void LevelChanger::Update(float process_time) {

  // 各処理の戻り値
  bool result = false;

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //--------------------------
    // 初期化処理
    //--------------------------
    result = Initialize();
    // 現在のフェーズの処理が完了したら次のフェーズに遷移する
    if (result) {
      ChangePhaseType(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //--------------------------
    // フェーズ管理処理
    //--------------------------
    result = Process();
    // 現在のフェーズの処理が完了したら次のフェーズに遷移する
    if (result) {
      ChangePhaseType(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //--------------------------
    // 終了処理
    //--------------------------
    result = Finish();
    // 現在のフェーズの処理が完了したら次のフェーズに遷移する
    if (result) {
      ChangePhaseType(PhaseType::kFinalized);
    }
    break;
  }
  }

}

/// <summary>
/// 1フレームの描画処理を実行する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void LevelChanger::Render() {


}

/// <summary>
/// 現在のフェーズを変更する
/// </summary>
/// <param name="phase_type"> フェーズの種類 </param>
/// <returns></returns>
void LevelChanger::ChangePhaseType(PhaseType phase_type) {

  current_phase_ = phase_type;
}

/// <summary>
/// 遷移フェーズを変更する
/// </summary>
/// <param name="phase_type"> フェーズの種類 </param>
/// <returns></returns>
void LevelChanger::ChangeTransitionPhase(LevelTransitionPhase transition_phase) {

  transition_phase_ = transition_phase;
}


/// <summary>
/// レベル処理を生成する
/// </summary>
/// <param name=""> タスクID </param>
/// <returns> レベル処理 </returns>
Level* LevelChanger::CreateNextLevel(TaskId task_id) {

  Level* level = nullptr;

  switch (task_id) {
  case TaskId::kBootLevel: {
    level = new BootLevel(task_id, task_manager_);
    break;
  }
  case TaskId::kTitleLevel: {
    level = new TitleLevel(task_id, task_manager_);
    break;
  }
  case TaskId::kBattleLevel: {
    level = new BattleLevel(task_id, task_manager_);
    break;
  }
  case TaskId::kResultLevel: {
    level = new ResultLevel(task_id, task_manager_);
    break;
  }
  }

  return level;
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理継続 </returns>
bool LevelChanger::Initialize() {

  // 現在のレベルがない場合、次のレベルを「ブートレベル」に、
  // 遷移フェーズを「次のレベルを生成」に変更する
  if (current_level_ == nullptr &&
    transition_phase_ == LevelTransitionPhase::kNone) {
    next_level_task_id_ = TaskId::kBattleLevel;
    ChangeTransitionPhase(LevelTransitionPhase::kNextLevelCreate);
  }
  return true;
}

/// <summary>
/// 処理を行う
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理継続 </returns>
bool LevelChanger::Process() {

  if (current_level_ != nullptr &&
      current_level_->IsError() &&
      current_level_->IsFinishedPhase()) {
    next_level_task_id_ = TaskId::kDummyTask;
    ChangeTransitionPhase(LevelTransitionPhase::kCurrentLevelFinalized);
  }
  // 現在のレベルが設定済み、現在のレベルが「終了リクエスト」フェーズ、
  // 遷移フェーズが「切り替え終了済み」の場合、
  // 次のレベルのタスクIDを取得、遷移フェーズを「現在のレベル終了」フェーズに設定
  else if (current_level_ != nullptr &&
           current_level_->IsRequestFinishPhase() &&
           transition_phase_ == LevelTransitionPhase::kSwitchFinished) {
    next_level_task_id_ = current_level_->GetNextLevelTaskId();
    ChangeTransitionPhase(LevelTransitionPhase::kCurrentLevelFinalize);
  }
  else if (current_level_ != nullptr &&
           current_level_->IsRequestFinishPhase() &&
           transition_phase_ == LevelTransitionPhase::kNextLevelInitialized) {
    next_level_task_id_ = current_level_->GetNextLevelTaskId();
    ChangeTransitionPhase(LevelTransitionPhase::kCurrentLevelFinalize);
  }
  // レベル遷移のフェーズが切り替え終了フェーズで
  // 現在のレベルフェーズが「終了処理済み」の場合
  // レベル遷移フェーズを「現在レベル終了済み」へ変更する
  else if (transition_phase_ == LevelTransitionPhase::kSwitchFinished &&
           current_level_ != nullptr &&
           current_level_->IsFinishedPhase()) {
    ChangeTransitionPhase(LevelTransitionPhase::kCurrentLevelFinalized);
  }

  switch (transition_phase_) {
  case LevelTransitionPhase::kCurrentLevelFinalize: {
    //---------------------------------------
    //現在のレベルを終了するフェーズ
    //---------------------------------------
    // 現在のレベル側のフェーズを「終了フェーズ」に変更する
    current_level_->SetFinishPhase();
    // 遷移フェーズを「現在のレベル終了済み」に変更する
    ChangeTransitionPhase(LevelTransitionPhase::kCurrentLevelFinalized);
    break;
  }
  case LevelTransitionPhase::kCurrentLevelFinalized: {
    //---------------------------------------
    //現在のレベル終了済みフェーズ
    //---------------------------------------
    // 現在のレベル側が終了済みか確認、終了していないなら抜ける
    bool check_finished = current_level_->IsFinishedPhase();
    if (!check_finished) {
      break;
    }

    // タスクマネージャから現在のレベル(タスク)を降ろす
    TaskId task_id = current_level_->GetTaskId();
    task_manager_.ReleaseTask(task_id);

    // 次のレベルが設定されている場合、遷移フェーズを変更
    // 次のレベルが設定されていない場合、本処理を終了する
    if (next_level_task_id_ != TaskId::kDummyTask) {
      // 遷移フェーズを「次のレベルを生成する」に変更する
      ChangeTransitionPhase(LevelTransitionPhase::kNextLevelCreate);
    }
    else {
      // 遷移フェーズを「レベルチェンジャー終了」に変更する
      ChangeTransitionPhase(LevelTransitionPhase::kProcessFinish);
      return true;
    }

    break;
  }
  case LevelTransitionPhase::kNextLevelCreate: {
    //---------------------------------------
    //次のレベルを生成するフェーズ
    //---------------------------------------
    // 次のレベルを生成する前に現在のレベルを破棄する
    if (current_level_ != nullptr) {
      delete current_level_;
      current_level_ = nullptr;
    }
    // レベルを生成する
    current_level_ = CreateNextLevel(next_level_task_id_);
    // 遷移フェーズを「次のレベルを初期化する」に変更する
    ChangeTransitionPhase(LevelTransitionPhase::kNextLevelInitialize);
    break;
  }
  case LevelTransitionPhase::kNextLevelInitialize: {
    //---------------------------------------
    //次のレベルを初期化するフェーズ
    //---------------------------------------
    // 現在のレベルをタスクに積む
    task_manager_.AddTask(current_level_);
    // 現在のレベル側のフェーズを初期化処理に設定する
    current_level_->SetInitializePhase();
    // 遷移フェーズを「切り替え終了」に変更する
    ChangeTransitionPhase(LevelTransitionPhase::kNextLevelInitialized);
    break;
  }
  case LevelTransitionPhase::kNextLevelInitialized: {
    //---------------------------------------
    //次のレベルを初期化済みフェーズ
    //---------------------------------------
    // 現在のレベル側が初期化済みか確認、終了していないなら抜ける
    bool check_initialized = current_level_->IsInitializedPhase();
    if (!check_initialized) {
      break;
    }

    // レベル処理を開始
    current_level_->SetProcessPhase();
    // 遷移フェーズを「切り替え終了」に変更する
    ChangeTransitionPhase(LevelTransitionPhase::kSwitchFinished);
    // 次のレベルを一旦、初期設定時の値に変更する
    next_level_task_id_ = TaskId::kDummyTask;

    break;
  }
  }

  return false;
}

/// <summary>
/// 終了処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理継続 </returns>
bool LevelChanger::Finish() {

  // 遷移フェーズが「レベルチェンジャー終了」以外の場合、
  // 現在のフェーズを一旦「処理中」に戻す
  switch (transition_phase_) {
  case LevelTransitionPhase::kProcessFinish: {
    break;
  }
  default: {

    // 遷移フェーズが「切り替え終了」の場合、「現在のレベル終了」フェーズに変更
    if (transition_phase_ == LevelTransitionPhase::kSwitchFinished) {
      ChangeTransitionPhase(LevelTransitionPhase::kCurrentLevelFinalize);
    }
    // 現在のレベルが存在し、現在のレベル側のフェーズが「終了リクエスト」でない場合、
    // 現在のレベル側のフェーズを「終了リクエスト」に設定
    if (current_level_ != nullptr && !current_level_->IsRequestFinishPhase()) {
      current_level_->SetRequestFinish();
    }

    // 現在のフェーズを「処理中」に変更
    ChangePhaseType(PhaseType::kProcess);
    return false;
  }
  }

  // 現在のレベルを破棄する
  if (current_level_ != nullptr) {
    delete current_level_;
    current_level_ = nullptr;
  }

  return true;
}