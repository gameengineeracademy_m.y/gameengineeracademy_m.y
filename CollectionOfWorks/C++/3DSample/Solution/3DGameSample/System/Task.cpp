﻿#include "System/Task.h"

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Task::Task(TaskId task_id)
  : task_id_(task_id)
  , is_release_(false) {

  // コンソールに出力
  std::cout << "Task コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Task::~Task() {

  // コンソールに出力
  std::cout << "~Task デストラクタ" << std::endl;
}