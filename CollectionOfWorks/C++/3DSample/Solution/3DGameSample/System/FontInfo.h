﻿#pragma once

#include "DxLib.h"
#include <iostream>
#include <unordered_map>

/// <summary>
/// フォント情報
/// </summary>
class FontInfo {
public:

  /// <summary>
  /// インスタンスを作成する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  static void CreateFontInfoInstance();

  /// <summary>
  /// インスタンスを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  static FontInfo* GetFontInfoInstance() { return font_info_instance_; }

  /// <summary>
  ///  インスタンスを破棄する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  static void ReleaseFontInfoInstance();

  /// <summary>
  ///  フォントの生成
  /// </summary>
  /// <param name=""> ファイルパス </param>
  /// <returns> 0:生成失敗, それ以外:生成成功 </returns>
  int CreateFontInfo(const char*);

  /// <summary>
  ///  フォントの破棄
  /// </summary>
  /// <param name=""> ファイルパス </param>
  /// <returns> 0:破棄失敗, それ以外:破棄成功 </returns>
  int ReleaseFontInfo(const char*);

  /// <summary>
  /// フォント情報を取得する
  /// </summary>
  /// <param name=""> フォント名 </param>
  /// <param name=""> フォントサイズ </param>
  /// <param name=""> フォント幅 </param>
  /// <param name=""> フォントの種類 </param>
  /// <returns> フォントハンドル </returns>
  int GetFontInfo(const char*, int, int, int);


private:

  /// <summary>
  /// コンストラクタ
  /// </summary>
  /// <param></param>
  /// <returns></returns>
  FontInfo();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param></param>
  /// <returns></returns>
  ~FontInfo();


private:

  /// <summary>
  /// インスタンス (唯一のもの)
  /// </summary>
  static FontInfo* font_info_instance_;
};