﻿#pragma once

/// <summary>
/// タスクID一覧
/// </summary>
enum class TaskId {
  kDummyTask,              // 初期設定用ダミータスク
  kLevelChanger,           // レベルチェンジャータスク
  kBootLevel,              // ブートレベルタスク
  kTitleLevel,             // タイトルレベルタスク
  kBattleLevel,            // バトルレベルタスク
  kResultLevel,            // リザルトレベルタスク
  kMenuController,         // メニューコントローラタスク
  kPlayerController,       // プレイヤーコントローラタスク
  kCameraController,       // カメラコントローラタスク
  kFieldManager,           // フィールドマネージャタスク
  kPlayer,                 // プレイヤータスク
  kCameraManager,          // カメラマネージャタスク
  kBattleInfoUi,           // バトルUI情報タスク
  kTaskIdMaxIndex          // タスクID最大値
};