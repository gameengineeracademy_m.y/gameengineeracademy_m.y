# 作品名
	3DGameSample

# 作品概要	
	本制作物はゲームではなく、UnrealEngineを使い始める前の準備制作物
	3次元座標上でのカメラやキャラクターの機能を実装することで
	Engine内部で行われている処理を学習する目的で制作

### 開発環境
* Unreal Engine 4.27.2
* Visual Studio 2019

## 制作実績　（制作期間：12日）

　☆操作方法
	【キーボードとマウスを使用】

	　〇プレイヤー操作モード
	　WASD				：	プレイヤーの移動
	　Space				：	ジャンプ
	　矢印キー				：	カメラの回転
	　マウスホイール		：	ズーム機能	
	　
	　P					：	配置初期化
	　M					：	キー入力判定表示切替
	　Esc				：	マニュアル表示切替
	　左Alt + D			：	モード切替

	　〇デバッグモード
	　WASD				：	カメラの移動
	　左Shift + Q			：	カメラの上昇
	　左Shift + E			：	カメラの下降	
	　左Shift + WASD		：	カメラの回転　
	　マウスホイール		：	ズーム機能
	　
	　P					：	配置初期化
	　Esc				：	マニュアル表示切替
	　左Alt + D			：	モード切替

　☆実装した項目

　　●全体的要素
	・生成したフィールド内をキャラクターが移動したりジャンプしたりできるように実装

　☆実装に注力した部分
	・CSVを用いたフィールドの自動生成
	・キャラクターと床の当たり判定実装
	・キャラクターの移動やジャンプ処理
	・キャラクターのボーンやアニメーション作成と実装
	・カメラのプレイヤー追従処理
	・注視点を中心としたカメラの回転処理やズーム機能の実装
	
　☆制作を通して学んだこと
	・3次元空間上での移動処理の実装方法
	・ジンバルロックなど3次元空間上での制作上気を付けなければならないこと