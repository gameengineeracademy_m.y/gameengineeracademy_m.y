﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "System/BackScreenEventInterface.h"
#include "Game/GameInfo.h"

/// <summary>
/// バックスクリーン
/// </summary>
class BackScreen : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,     // 初期化処理
    kProcess,        // 処理中
    kFinalize,       // 終了処理
    kFinalized,      // 終了処理済み
    kPhaseMaxIndex   // フェーズ数
  };

  /// <summary>
  /// 描画フェーズ
  /// </summary>
  enum class DrawPhaseType {
    kStart,              // 開始フェーズ
    kFinish,             // 終了フェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> 表示倍率 </param>
  /// <param name=""> 表示角度 </param>
  /// <param name=""> バックスクリーンイベントインターフェース </param>
  /// <returns></returns>
  BackScreen(float, float, BackScreenEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~BackScreen();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// グラフィックハンドルをセットする
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandle(int graphic_handle) { back_screen_handle_ = graphic_handle; }

  /// <summary>
  /// グラフィックハンドルを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandle() { return back_screen_handle_; }

  /// <summary>
  /// 透過率 調整値をセットする
  /// </summary>
  /// <param name=""> 開始変化量 </param>
  /// <param name=""> 終了変化量 </param>
  /// <returns></returns>
  void SetAlphaAdjustValue(int, int);

  /// <summary>
  /// 描画フェーズを変更する
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns></returns>
  void ChangePhaseType(PhaseType phase_type) { current_phase_ = phase_type; };

  /// <summary>
  /// 描画フェーズを変更する
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns></returns>
  void ChangeDrawPhase(DrawPhaseType draw_phase) { current_draw_phase_ = draw_phase; };

  /// <summary>
  /// 現在のフェーズが「終了済み」かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了済み, false:終了済み以外 </returns>
  bool IsPhaseFinalized() { return current_phase_ == PhaseType::kFinalized; };

  /// <summary>
  /// 現在の描画フェーズが「終了フェーズ」かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了フェーズ, false:終了フェーズ以外 </returns>
  bool IsDrawPhaseFinish() { return current_draw_phase_ == DrawPhaseType::kFinish; };


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Initialize();

  /// <summary>
  /// 処理中
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理継続 </returns>
  bool Process();

  /// <summary>
  /// 終了処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  bool Finalize();

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; };


private:

  /// <summary>
  /// バックスクリーンイベントインターフェース
  /// </summary>
  BackScreenEventInterface& backscreen_event_interface_;

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// フェーズの種類
  /// </summary>
  DrawPhaseType current_draw_phase_;

  /// <summary>
  /// 背景グラフィックハンドル
  /// </summary>
  int back_screen_handle_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// 表示倍率
  /// </summary>
  float magnification_;

  /// <summary>
  /// 表示角度
  /// </summary>
  float angle_;

  /// <summary>
  /// 描画 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 描画 透過率 開始変化値
  /// </summary>
  int alpha_adjust_start_;

  /// <summary>
  /// 描画 透過率 終了変化値
  /// </summary>
  int alpha_adjust_finish_;
};