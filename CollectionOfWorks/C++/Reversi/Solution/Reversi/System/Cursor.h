﻿#pragma once

#include "DxLib.h"
#include "System/CursorBase.h"

/// <summary>
/// カーソル
/// </summary>
class Cursor : public CursorBase {
public:

  /// <summary>
  /// カーソルの種類
  /// </summary>
  enum class CursorType {
    kPlayer,           // プレイヤーカーソル
    kEnemy,            // エネミーカーソル
    kPlaceable,        // 配置可能カーソル
    kTurnOver,         // ひっくり返せるカーソル
    kCursorMaxIndex    // カーソルの種類の数
  };


public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> カーソルID </param>
  /// <param name=""> カーソルの種類 </param>
  /// <returns></returns>
  Cursor(int, CursorType);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~Cursor();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float);

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render();

  /// <summary>
  /// X座標をセットする
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <returns></returns>
  void SetPositionX(int x_pos) { x_pos_ = x_pos; }

  /// <summary>
  /// X座標を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> X座標 </returns>
  int GetPositionX() { return x_pos_; }

  /// <summary>
  /// Y座標をセットする
  /// </summary>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  void SetPositionY(int y_pos) { y_pos_ = y_pos; }

  /// <summary>
  /// Y座標を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> Y座標 </returns>
  int GetPositionY() { return y_pos_; }

  /// <summary>
  /// 画像のサイズを取得する
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns></returns>
  void SetImageSize(int);

  /// <summary>
  /// 画像の幅を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 画像の幅 </returns>
  int GetImageWidth() { return image_width_; }

  /// <summary>
  /// 画像の幅を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 画像の高さ </returns>
  int GetImageHeight() { return image_height_; }

  /// <summary>
  /// カーソルの種類の取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> カーソルの種類 </returns>
  CursorType GetCursorType() { return cursor_type_; }

  /// <summary>
  /// 表示有無をセットする
  /// </summary>
  /// <param name=""> 有無 </param>
  /// <returns></returns>
  void SetDisplay(bool disp) { is_display_ = disp; }

  /// <summary>
  /// 表示有無を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:表示, false:非表示 </returns>
  bool GetDisplay() { return is_display_; }


private:

  /// <summary>
  /// カーソルの種類
  /// </summary>
  CursorType cursor_type_;

  /// <summary>
  /// 表示有無
  /// </summary>
  bool is_display_;

  /// <summary>
  /// X座標
  /// </summary>
  int x_pos_;

  /// <summary>
  /// Y座標
  /// </summary>
  int y_pos_;

  /// <summary>
  /// 画像の幅
  /// </summary>
  int image_width_;

  /// <summary>
  /// 画像の高さ
  /// </summary>
  int image_height_;
};