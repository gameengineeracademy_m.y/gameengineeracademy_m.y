﻿#include "System/SoundManager.h"

namespace {

  /// <summary>
  /// サウンドリストにサウンドがないこと
  /// </summary>
  const int kZero = 0;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
SoundManager::SoundManager()
  :Task(TaskId::kSoundManager)
  , sound_phase_(SoundPhaseType::kPlay)
  , sound_list_()
  , play_sound_list_()
  , stop_sound_list_()
  , accumulate_time_(0.0f) {

  // コンソールに出力
  std::cout << "SoundManager コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
SoundManager::~SoundManager() {

  // コンソールに出力
  std::cout << "~SoundManager デストラクタ" << std::endl;
}

/// <summary>
/// サウンドの1フレームの処理を実行する
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void SoundManager::Update(float process_time) {

  std::vector<SoundId> played_sound_list;

  switch (sound_phase_) {
  case SoundPhaseType::kPlay: {
    //---------------------------------
    // サウンドの再生処理
    //---------------------------------
    for (auto sound_id : play_sound_list_) {
      // サウンドを再生する
      bool is_played = sound_list_[sound_id]->SoundPlay();
      if (is_played) {
        played_sound_list.push_back(sound_id);
      }
    }

    for (auto sound_id : played_sound_list) {
      // 再生したサウンドをリストから削除する
      auto iterator = std::find(play_sound_list_.begin(), play_sound_list_.end(), sound_id);
      if (iterator != play_sound_list_.end()) {
        play_sound_list_.erase(iterator);
      }
    }
    break;
  }
  case SoundPhaseType::kPause: {
    //---------------------------------
    // サウンドの一時停止処理
    //---------------------------------
    for (auto sound : sound_list_) {
      // サウンドを一時停止する
      bool is_stop = sound.second->SoundPause();
      if (is_stop) {
        // 一時停止したサウンドのサウンドIDを再生するサウンドリストにセット
        play_sound_list_.push_back(sound.first);
      }
    }
    break;
  }
  }
}

/// <summary>
/// サウンドを積む
/// </summary>
/// <param name="sound"> サウンドID </param>
/// <returns> サウンド </returns>
Sound* SoundManager::AddSound(SoundId sound_id) {

  // サウンドが既にリストに登録済みか確認し、登録済みなら処理終了
  if (sound_list_.find(sound_id) != sound_list_.end()) {
    return nullptr;
  }

  // 指定のサウンドIDのサウンドを生成する
  Sound* sound = new Sound(sound_id);
  // サウンドを降ろす設定をリセットする
  sound->SetResetReleaseSound();
  // サウンドリストに追加
  sound_list_[sound_id] = sound;

  return sound;
}

/// <summary>
/// サウンドを降ろす
/// </summary>
/// <param name="sound_id"> サウンドID </param>
/// <returns></returns>
Sound* SoundManager::ReleaseSound(SoundId sound_id) {

  // サウンドリストに存在するサウンドか確認
  if (sound_list_.find(sound_id) == sound_list_.end()) {
    return nullptr;
  }

  // サウンドIDからサウンドを取得
  Sound* sound = sound_list_[sound_id];
  // サウンドを降ろす設定
  sound->SetReleaseSound();
  // サウンドリストからサウンドを降ろす
  sound_list_.erase(sound_id);

  return sound;
}

/// <summary>
/// サウンドを破棄する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void SoundManager::DisposeSound() {

  std::vector<SoundId> dispose_sound_list;

  for (auto sound : sound_list_) {

    if (sound.second == nullptr) {
      continue;
    }
    // サウンドリソースの破棄
    DeleteSoundMem(sound.second->GetSoundHandle());
    // サウンドをリストから降ろしたことにする
    sound.second->SetReleaseSound();
    // サウンドIDを破棄リストに追加する
    dispose_sound_list.push_back(sound.second->GetSoundId());
  }

  for (int i = 0; i < static_cast<int>(dispose_sound_list.size()); ++i) {
    // サウンドをサウンドマネージャから降ろす
    Sound* release_sound = ReleaseSound(dispose_sound_list.at(i));
    if (release_sound != nullptr) {
      delete release_sound;
      release_sound = nullptr;
    }
  }
}

/// <summary>
/// サウンドを取り出す
/// </summary>
/// <param name=""> サウンドID </param>
/// <returns> サウンド </returns>
Sound* SoundManager::GetSound(SoundId sound_id) {

  // サウンドリストに存在するサウンドか確認
  if (sound_list_.find(sound_id) == sound_list_.end()) {
    return nullptr;
  }

  return sound_list_[sound_id];
}

/// <summary>
/// 再生するサウンドリストに追加する
/// </summary>
/// <param name=""> サウンドID </param>
/// <returns> true:リスト追加成功, false:リスト追加失敗 </returns>
bool SoundManager::SetPlaySoundList(SoundId sound_id) {

  if (sound_list_.find(sound_id) == sound_list_.end()) {
    return false;
  }

  play_sound_list_.push_back(sound_id);

  return true;
}