﻿#pragma once

#include "System/SoundID.h"
#include <iostream>

/// <summary>
/// サウンドベース
/// </summary>
class SoundBase {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> サウンドID </param>
  /// <returns></returns>
  SoundBase(SoundId);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~SoundBase();

  /// <summary>
  /// サウンドIDを設定する
  /// </summary>
  /// <param name="sound_id"> サウンドID </param>
  /// <returns></returns>
  void SetSoundId(SoundId sound_id) { sound_id_ = sound_id; }

  /// <summary>
  /// サウンドIDを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> サウンドID </returns>
  SoundId GetSoundId() { return sound_id_; }

  /// <summary>
  /// サウンドを降ろしたことにする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetReleaseSound() { is_release_ = true; }

  /// <summary>
  /// サウンドを降ろす設定をリセットする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetResetReleaseSound() { is_release_ = false; }

  /// <summary>
  /// サウンドを降ろしたかの有無を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> サウンドを降ろしたかの有無 </returns>
  bool IsReleaseSound() { return is_release_ == true; }


private:

  /// <summary>
  /// サウンドID
  /// </summary>
  SoundId sound_id_;

  /// <summary>
  /// 降ろしたかの有無
  /// </summary>
  bool is_release_;
};