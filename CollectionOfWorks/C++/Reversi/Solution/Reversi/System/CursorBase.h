﻿#pragma once

#include <iostream>

class CursorBase {
public:
  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> カーソルID </param>
  /// <returns></returns>
  CursorBase(int);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~CursorBase();

  /// <summary>
  /// カーソルIDを設定する
  /// </summary>
  /// <param name="cursor_id"> カーソルID </param>
  /// <returns></returns>
  void SetCursorId(int cursor_id) { cursor_id_ = cursor_id; }

  /// <summary>
  /// カーソルIDを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> カーソルID </returns>
  int GetCursorId() { return cursor_id_; }

  /// <summary>
  /// カーソルを降ろしたことにする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetReleaseCursor() { is_release_ = true; }

  /// <summary>
  /// カーソルを降ろす設定をリセットする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetResetReleaseCursor() { is_release_ = false; }

  /// <summary>
  /// カーソルを降ろしたかの有無を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 石を降ろしたかの有無 </returns>
  bool IsReleaseCursor() { return is_release_ == true; }

  /// <summary>
  /// 1フレームの処理を実行する
  /// </summary>
  /// <param name=""> 前回のメインループ処理に掛かった時間 </param>
  /// <returns></returns>
  virtual void Update(float) {}

  /// <summary>
  /// 1フレームの描画処理を実行する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void Render() {}


private:

  /// <summary>
  /// カーソルID
  /// </summary>
  int cursor_id_;

  /// <summary>
  /// 降ろしたかの有無
  /// </summary>
  bool is_release_;
};