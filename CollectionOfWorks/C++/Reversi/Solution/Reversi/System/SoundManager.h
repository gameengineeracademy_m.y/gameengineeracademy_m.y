﻿#pragma once

#include "System/Task.h"
#include "System/Sound.h"
#include "System/SoundId.h"
#include <unordered_map>

/// <summary>
/// サウンドマネージャ
/// </summary>
/// <remarks>
/// ゲーム内で扱うサウンド、BGMの管理を行うクラス
/// </remarks>
class SoundManager : public Task {
public:

  /// <summary>
  /// サウンド処理のフェーズ
  /// </summary>
  enum class SoundPhaseType {
    kPlay,           // 再生中フェーズ
    kPause,          // 一時停止中フェーズ
    kPhaseMaxIndex   // フェーズ数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  SoundManager();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~SoundManager();

  /// <summary>
  /// サウンドの1フレームの処理を実行する
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// サウンドを積む
  /// </summary>
  /// <param name=""> サウンドID </param>
  /// <returns> サウンド </returns>
  Sound* AddSound(SoundId);

  /// <summary>
  /// サウンドを降ろす
  /// </summary>
  /// <param name=""> サウンドID </param>
  /// <returns> サウンド </returns>
  Sound* ReleaseSound(SoundId);

  /// <summary>
  /// サウンドを破棄する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DisposeSound();

  /// <summary>
  /// サウンドを取り出す
  /// </summary>
  /// <param name=""> サウンドID </param>
  /// <returns> サウンド </returns>
  Sound* GetSound(SoundId);

  /// <summary>
  /// 再生するサウンドリストに追加する
  /// </summary>
  /// <param name=""> サウンドID </param>
  /// <returns> true:リスト追加成功, false:リスト追加失敗 </returns>
  bool SetPlaySoundList(SoundId);

  /// <summary>
  /// サウンド処理のフェーズを変更する
  /// </summary>
  /// <param name=""> サウンド処理のフェーズ </param>
  /// <returns></returns>
  void ChangeSoundPhaseType(SoundPhaseType sound_phase) { sound_phase_ = sound_phase; }


private:

  /// <summary>
  /// サウンド処理のフェーズ
  /// </summary>
  SoundPhaseType sound_phase_;

  /// <summary>
  /// サウンドリスト
  /// </summary>
  std::unordered_map<SoundId, Sound*> sound_list_;

  /// <summary>
  /// 再生するサウンドのサウンドIDリスト
  /// </summary>
  std::vector<SoundId> play_sound_list_;

  /// <summary>
  /// 停止したサウンドのサウンドIDリスト
  /// </summary>
  std::vector<SoundId> stop_sound_list_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;
};