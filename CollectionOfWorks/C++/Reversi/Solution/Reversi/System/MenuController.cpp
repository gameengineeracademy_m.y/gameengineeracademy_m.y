﻿#include "System/MenuController.h"

namespace {

  /// <summary>
  /// 戻り値 正常値
  /// </summary>
  const int kSuccess = 0;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> メニューコントローライベントインターフェース </param>
/// <returns></returns>
MenuController::MenuController(MenuControllerEventInterface& menu_controller_event_interface)
  : Task(TaskId::kMenuController)
  , menu_controller_event_interface_(menu_controller_event_interface)
  , push_button_{ false } {

  // コンソールに出力
  std::cout << "MenuController コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
MenuController::~MenuController() {

  // コンソールに出力
  std::cout << "~MenuController デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void MenuController::Update(float process_time) {

  int x_pos, y_pos = 0;
  // 画面上でのマウスの位置を取得
  GetMousePoint(&x_pos, &y_pos);

  // マウスの座標を取得
  int check_get_pos = GetMousePoint(&x_pos, &y_pos);
  if (check_get_pos == kSuccess) {
    menu_controller_event_interface_.OnGetMousePosition(x_pos, y_pos, push_button_[static_cast<int>(ButtonType::kLeft)]);
  }

  bool check_left = GetPushLeftButton();
  if (check_left) {
    // マウス左ボタン クリックイベント実行
    menu_controller_event_interface_.OnPushMouseLeftButton(x_pos, y_pos);
  }
}

/// <summary>
/// 左クリック
/// </summary>
/// <param name=""></param>
/// <returns></returns>
bool MenuController::GetPushLeftButton() {

  return GetPushButton(MOUSE_INPUT_LEFT, push_button_[static_cast<int>(ButtonType::kLeft)]);
}

/// <summary>
/// ボタンが押されたかの有無を取得する
/// </summary>
/// <param name="key_code"> キーコード </param>
/// <param name="push_key"> ボタンの押下の有無 </param>
/// <returns> true:ボタンが押された, false:ボタンが押されていない </returns>
bool MenuController::GetPushButton(int key_code, bool& push_button) {

  // 指定のボタンが押されている
  if (key_code == GetMouseInput()) {

    // 直前まで押されていない場合は押されていることにする
    if (push_button == false) {
      push_button = true;
    }
  }
  else {
    // 前回処理時にキーが押されていた場合は押されていないことにする
    if (push_button == true) {
      push_button = false;

      // 判定を押されたとして返す
      return true;
    }
  }

  return false;
}