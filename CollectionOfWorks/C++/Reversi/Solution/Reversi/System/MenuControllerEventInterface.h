﻿#pragma once

#include <iostream>

/// <summary>
/// メニューコントローライベントインターフェース
/// </summary>
class MenuControllerEventInterface {
public:

   /// <summary>
   /// コンストラクタ 初期化処理
   /// </summary>
   /// <param name=""></param>
   /// <returns></returns>
  MenuControllerEventInterface() {

    // コンソールに出力
    std::cout << "MenuControllerEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~MenuControllerEventInterface() {

    // コンソールに出力
    std::cout << "~MenuControllerEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// マウスの左クリックが押された時のイベント
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  virtual void OnPushMouseLeftButton(int, int) = 0;

  /// <summary>
  /// マウスの座標取得イベント
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <param name=""> 押されているかどうか </param>
  /// <returns></returns>
  virtual void OnGetMousePosition(int, int, bool) = 0;


private:

};