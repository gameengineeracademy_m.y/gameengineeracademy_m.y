﻿#include "System/CursorManager.h"


namespace {

  /// <summary>
  /// カーソルの最大数
  /// </summary>
  const int kCursorMaxNum = 64;

  /// <summary>
  /// カーソルの最小数
  /// </summary>
  const int kCursorMinNum = 0;

  /// <summary>
  /// プレイヤーカーソルの画像パス
  /// </summary>
  const char* kImagePathPlayerCursor = "Assets/Image/player_cursor.png";

  /// <summary>
  /// エネミーカーソルの画像パス
  /// </summary>
  const char* kImagePathEnemyCursor = "Assets/Image/enemy_cursor.png";

  /// <summary>
  /// 配置可能箇所表示カーソルの画像パス
  /// </summary>
  const char* kImagePathPlaceableCursor = "Assets/Image/placeable_cursor.png";

  /// <summary>
  /// ひっくり返す石のカーソルの画像パス
  /// </summary>
  const char* kImagePathTurnOverCursor = "Assets/Image/turn_over_cursor.png";

  /// <summary>
  /// 画像の総分割数
  /// </summary>
  const int kDivTotalNum = 2;

  /// <summary>
  /// 画像の横方向の分割数
  /// </summary>
  const int kDivXNum = 1;

  /// <summary>
  /// 画像の縦方向の分割数
  /// </summary>
  const int kDivYNum = 2;

  /// <summary>
  /// 画像分割後の1枚あたりの幅
  /// </summary>
  const int kWidth = 64;

  /// <summary>
  /// 画像分割後の1枚あたりの高さ
  /// </summary>
  const int kHeight = 64;

  /// <summary>
  /// 画像読み込み時のエラー値
  /// </summary>
  const int kLoadError = -1;

  /// <summary>
  /// 盤 横のマス数
  /// </summary>
  const int kBoardHorizonNum = 8;

  /// <summary>
  /// 先頭インデックス
  /// </summary>
  const int kInitialIndex = 0;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
CursorManager::CursorManager()
  : Task(TaskId::kCursorManager)
  , player_cursor_(nullptr)
  , enemy_cursor_(nullptr)
  , placeable_cursor_list_()
  , turn_over_cursor_list_() {

  // コンソールに出力
  std::cout << "CursorManager コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
CursorManager::~CursorManager() {

  // コンソールに出力
  std::cout << "~CursorManager デストラクタ" << std::endl;
}

/// <summary>
/// 配置可能箇所表示カーソルを積む
/// </summary>
/// <param name="placeable_cursor"> 配置可能箇所表示カーソル </param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool CursorManager::AddPlaceableCursor(PlaceableCursor* placeable_cursor) {

  // カーソルが存在しない場合は処理終了
  if (placeable_cursor == nullptr) {
    return false;
  }

  // カーソルを降ろす設定をリセットする
  placeable_cursor->SetResetReleaseCursor();
  // カーソルIDを取得
  int cursor_id = placeable_cursor->GetCursorId();
  // 既にリストに登録済みか確認し、登録済みなら処理終了
  if (placeable_cursor_list_.find(cursor_id) != placeable_cursor_list_.end()) {
    return false;
  }
  // リストに追加
  placeable_cursor_list_[cursor_id] = placeable_cursor;
  return true;
}

/// <summary>
/// ひっくり返す石表示カーソルを積む
/// </summary>
/// <param name=""> ひっくり返す石表示カーソル </param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool CursorManager::AddTurnOverCursor(TurnOverCursor* turn_over_cursor) {

  // カーソルが存在しない場合は処理終了
  if (turn_over_cursor == nullptr) {
    return false;
  }

  // カーソルを降ろす設定をリセットする
  turn_over_cursor->SetResetReleaseCursor();
  // カーソルIDを取得
  int cursor_id = turn_over_cursor->GetCursorId();
  // 既にリストに登録済みか確認し、登録済みなら処理終了
  if (turn_over_cursor_list_.find(cursor_id) != turn_over_cursor_list_.end()) {
    return false;
  }
  // リストに追加
  turn_over_cursor_list_[cursor_id] = turn_over_cursor;
  return true;
}

/// <summary>
/// 配置可能箇所表示カーソルを降ろす
/// </summary>
/// <param name=""> カーソルID </param>
/// <returns> 配置可能箇所表示カーソル </returns>
PlaceableCursor* CursorManager::ReleasePlaceableCursor(int cursor_id) {

  // リストに存在するカーソルか確認
  if (placeable_cursor_list_.find(cursor_id) == placeable_cursor_list_.end()) {
    return nullptr;
  }

  // カーソルIDからカーソルを取得
  PlaceableCursor* placeable_cursor = placeable_cursor_list_[cursor_id];
  // カーソルを降ろす設定
  placeable_cursor->SetReleaseCursor();
  // リストからカーソルを降ろす
  placeable_cursor_list_.erase(cursor_id);

  return placeable_cursor;

}

/// <summary>
/// ひっくり返す石表示カーソルを降ろす
/// </summary>
/// <param name=""> カーソルID </param>
/// <returns> ひっくり返す石表示カーソル </returns>
TurnOverCursor* CursorManager::ReleaseTurnOverCursor(int cursor_id) {

  // リストに存在するカーソルか確認
  if (turn_over_cursor_list_.find(cursor_id) == turn_over_cursor_list_.end()) {
    return nullptr;
  }

  // カーソルIDからカーソルを取得
  TurnOverCursor* turn_over_cursor = turn_over_cursor_list_[cursor_id];
  // カーソルを降ろす設定
  turn_over_cursor->SetReleaseCursor();
  // リストからカーソルを降ろす
  turn_over_cursor_list_.erase(cursor_id);

  return turn_over_cursor;
}

/// <summary>
/// カーソルの1フレームの処理を実行する
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void CursorManager::Update(float process_time) {

  // プレイヤーカーソルの処理
  bool is_release = player_cursor_->IsReleaseCursor();
  if (!is_release) {
    player_cursor_->Update(process_time);
  }

  // CPUカーソルの処理
  is_release = enemy_cursor_->IsReleaseCursor();
  if (!is_release) {
    enemy_cursor_->Update(process_time);
  }

  // 配置可能箇所表示カーソルの処理
  for (auto PlaceableCursor : placeable_cursor_list_) {
    bool is_release = PlaceableCursor.second->IsReleaseCursor();
    if (!is_release) {
      PlaceableCursor.second->Update(process_time);
    }
  }

  // ひっくり返す石表示カーソルの処理
  for (auto TurnOverCursor : turn_over_cursor_list_) {
    bool is_release = TurnOverCursor.second->IsReleaseCursor();
    if (!is_release) {
      TurnOverCursor.second->Update(process_time);
    }
  }
}

/// カーソルの1フレームの描画処理を実行する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CursorManager::Render() {

  // 配置可能箇所表示カーソルの描画
  for (auto PlaceableCursor : placeable_cursor_list_) {
    bool is_release = PlaceableCursor.second->IsReleaseCursor();
    if (!is_release) {
      PlaceableCursor.second->Render();
    }
  }

  // ひっくり返す石表示カーソルの描画
  for (auto TurnOverCursor : turn_over_cursor_list_) {
    bool is_release = TurnOverCursor.second->IsReleaseCursor();
    if (!is_release) {
      TurnOverCursor.second->Render();
    }
  }

  // プレイヤーカーソルの描画
  bool is_release = player_cursor_->IsReleaseCursor();
  if (!is_release) {
    player_cursor_->Render();
  }

  // CPUカーソルの処理
  is_release = enemy_cursor_->IsReleaseCursor();
  if (!is_release) {
    enemy_cursor_->Render();
  }
}

/// <summary>
/// カーソルを生成する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CursorManager::CreateCursor() {

  // プレイヤーカーソル 初期設定
  CreatePlayerCursor();

  // CPUカーソル 初期設定
  CreateEnemyCursor();

  // 配置可能箇所表示カーソル 初期設定
  CreatePlaceableCursor();

  // ひっくり返す石表示カーソル 初期設定
  CreateTurnOverCursor();
}

/// <summary>
/// カーソルを破棄する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CursorManager::DisposeCursor() {

  // 画像リソースの破棄
  for (int i = 0; i < kDivTotalNum; ++i) {
    int graphic_handle = player_cursor_->GetGraphicHandleCursor(i);
    if (graphic_handle != kLoadError) {
      DeleteGraph(graphic_handle);
    }
  }
  // 画像リソースの破棄
  for (int i = 0; i < kDivTotalNum; ++i) {
    int graphic_handle = enemy_cursor_->GetGraphicHandleCursor(i);
    if (graphic_handle != kLoadError) {
      DeleteGraph(graphic_handle);
    }
  }

  // 配置可能箇所表示カーソルリストに要素がない場合は処理終了
  if (placeable_cursor_list_.size() != kCursorMinNum) {
    // 画像リソースの破棄
    for (int i = 0; i < kDivTotalNum; ++i) {
      DeleteGraph(placeable_cursor_list_[kInitialIndex]->GetGraphicHandleCursor(i));
    }
  }

  // ひっくり返す場所カーソルリストに要素がない場合は処理終了
  if (turn_over_cursor_list_.size() != kCursorMinNum) {
    // 画像リソースの破棄
    for (int i = 0; i < kDivTotalNum; ++i) {
      DeleteGraph(turn_over_cursor_list_[kInitialIndex]->GetGraphicHandleCursor(i));
    }
  }

  for (int i = 0; i < kCursorMaxNum; ++i) {
    //--------------------------------------
    //配置可能箇所表示カーソル
    //--------------------------------------
    // 指定インデックスの要素がない場合、継続処理
    if (placeable_cursor_list_.find(i) != placeable_cursor_list_.end()) {
      if (placeable_cursor_list_[i] != nullptr) {
        // カーソルをリストから降ろしたことにする
        placeable_cursor_list_[i]->SetReleaseCursor();
        // カーソルをカーソルマネージャから降ろす
        int cursor_id = placeable_cursor_list_[i]->GetCursorId();
        PlaceableCursor* release_cursor = ReleasePlaceableCursor(cursor_id);

        // カーソルを破棄する
        if (release_cursor != nullptr) {
          delete release_cursor;
          release_cursor = nullptr;
        }
      }
    }

    //--------------------------------------
    //ひっくり返す石表示カーソル
    //--------------------------------------
    // 指定インデックスの要素がない場合、継続処理
    if (turn_over_cursor_list_.find(i) != turn_over_cursor_list_.end()) {
      if (turn_over_cursor_list_[i] != nullptr) {
        // カーソルをリストから降ろしたことにする
        turn_over_cursor_list_[i]->SetReleaseCursor();
        // カーソルをカーソルマネージャから降ろす
        int cursor_id = turn_over_cursor_list_[i]->GetCursorId();
        TurnOverCursor* release_cursor = ReleaseTurnOverCursor(cursor_id);

        // カーソルを破棄する
        if (release_cursor != nullptr) {
          delete release_cursor;
          release_cursor = nullptr;
        }
      }
    }
  }
  // CPUカーソルを破棄する
  if (enemy_cursor_ != nullptr) {
    delete enemy_cursor_;
    enemy_cursor_ = nullptr;
  }
  // プレイヤーカーソルを破棄する
  if (player_cursor_ != nullptr) {
    delete player_cursor_;
    player_cursor_ = nullptr;
  }
}

/// <summary>
/// プレイヤーカーソル 初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CursorManager::CreatePlayerCursor() {

  std::array<int, static_cast<int>(PlayerCursor::SizeType::kSizeMaxIndex)> graphic_handle;
  // 画像のロード
  int result = LoadDivGraph(kImagePathPlayerCursor, kDivTotalNum, kDivXNum, kDivYNum, kWidth, kHeight, graphic_handle.data());
  // 画像読み込み処理の結果、エラーだった場合
  if (result == kLoadError) {
    // コンソールに出力
    std::cout << "CursorManager CreatePlayerCursor：画像読み込み失敗" << std::endl;
    return;
  }
  // プレイヤーカーソル 生成
  player_cursor_ = new PlayerCursor();
  // プレイヤーカーソルにグラフィックハンドルを渡す
  player_cursor_->SetGraphicHandle(graphic_handle);
  // グラフィックハンドルから画像のサイズを取得
  player_cursor_->SetImageSize(graphic_handle[static_cast<int>(PlayerCursor::SizeType::kSizeMax)]);

}

/// <summary>
/// CPUカーソル 初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CursorManager::CreateEnemyCursor() {

  std::array<int, static_cast<int>(EnemyCursor::SizeType::kSizeMaxIndex)> graphic_handle;
  // 画像のロード
  int result = LoadDivGraph(kImagePathEnemyCursor, kDivTotalNum, kDivXNum, kDivYNum, kWidth, kHeight, graphic_handle.data());
  // 画像読み込み処理の結果、エラーだった場合
  if (result == kLoadError) {
    // コンソールに出力
    std::cout << "CursorManager CreateEnemyCursor：画像読み込み失敗" << std::endl;
    return;
  }
  // CPUカーソル 生成
  enemy_cursor_ = new EnemyCursor();
  // CPUカーソルにグラフィックハンドルを渡す
  enemy_cursor_->SetGraphicHandle(graphic_handle);
  // グラフィックハンドルから画像のサイズを取得
  enemy_cursor_->SetImageSize(graphic_handle[static_cast<int>(PlayerCursor::SizeType::kSizeMax)]);
}

/// <summary>
/// 配置可能箇所表示カーソル 初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CursorManager::CreatePlaceableCursor() {

  std::array<int, static_cast<int>(PlaceableCursor::PatternType::kPatternMaxIndex) > graphic_handle;
  // 画像のロード
  int result = LoadDivGraph(kImagePathPlaceableCursor, kDivTotalNum, kDivXNum, kDivYNum, kWidth, kHeight, graphic_handle.data());
  // 画像読み込み処理の結果、エラーだった場合
  if (result == kLoadError) {
    // コンソールに出力
    std::cout << "CursorManager CreatePlaceableCursor：画像読み込み失敗" << std::endl;
    return;
  }

  int x_pos = 0;
  int y_pos = 0;

  for (int i = 0; i < kCursorMaxNum; ++i) {
    // 配置可能箇所表示カーソルを生成してリストに追加
    PlaceableCursor* placeable_cursor = new PlaceableCursor(i);
    // グラフィックハンドルを渡す
    placeable_cursor->SetGraphicHandle(graphic_handle);
    // グラフィックハンドルから画像のサイズを取得
    placeable_cursor->SetImageSize(graphic_handle[static_cast<int>(PlaceableCursor::PatternType::kPattern1)]);
    // カーソルリストにカーソルを積む
    AddPlaceableCursor(placeable_cursor);
    // インデックスから盤上の座標を取得
    CalcBoradPosition(i, x_pos, y_pos);
    // 表示座標位置の設定
    placeable_cursor->SetPositionX(x_pos);
    placeable_cursor->SetPositionY(y_pos);
  }
}

/// <summary>
/// ひっくり返す石表示カーソル 初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CursorManager::CreateTurnOverCursor() {

  std::array<int, static_cast<int>(TurnOverCursor::PatternType::kPatternMaxIndex) > graphic_handle;
  // 画像のロード
  int result = LoadDivGraph(kImagePathTurnOverCursor, kDivTotalNum, kDivXNum, kDivYNum, kWidth, kHeight, graphic_handle.data());
  // 画像読み込み処理の結果、エラーだった場合
  if (result == kLoadError) {
    // コンソールに出力
    std::cout << "CursorManager CreateTurnOverCursor：画像読み込み失敗" << std::endl;
    return;
  }

  int x_pos = 0;
  int y_pos = 0;

  for (int i = 0; i < kCursorMaxNum; ++i) {
    // 配置可能箇所表示カーソルを生成
    TurnOverCursor* turn_over_cursor = new TurnOverCursor(i);
    // グラフィックハンドルを渡す
    turn_over_cursor->SetGraphicHandle(graphic_handle);
    // グラフィックハンドルから画像のサイズを取得
    turn_over_cursor->SetImageSize(graphic_handle[static_cast<int>(TurnOverCursor::PatternType::kPattern1)]);
    // カーソルリストにカーソルを積む
    AddTurnOverCursor(turn_over_cursor);
    // インデックスから盤上の座標を取得
    CalcBoradPosition(i, x_pos, y_pos);
    // 表示座標位置の設定
    turn_over_cursor->SetPositionX(x_pos);
    turn_over_cursor->SetPositionY(y_pos);
  }
}

/// <summary>
/// インデックスから盤上の座標を取得する
/// </summary>
/// <param name=""> インデックス </param>
/// <param name=""> X座標 </param>
/// <param name=""> Y座標 </param>
/// <returns></returns>
void CursorManager::CalcBoradPosition(int index, int& x_pos, int& y_pos) {

  if (index == 0) {
    // 初期値
    x_pos = 0;
    y_pos = 0;
  }
  else if (index % kBoardHorizonNum == 0) {
    // X座標を0にリセットし、Y座標を1つ増やす
    x_pos = 0;
    ++y_pos;
  }
  else {
    // X座標を1つ増やす
    ++x_pos;
  }

  // 管理リストにまだ追加されていない場合は対象座標とインデックスをセット
  if (position_list_.find(std::make_pair(x_pos, y_pos)) == position_list_.end()) {
    position_list_[std::make_pair(x_pos, y_pos)] = index;
  }
}