﻿#pragma once

/// <summary>
/// タスクID一覧
/// </summary>
enum class TaskId {
  kDummyTask,            // 初期設定用ダミータスク
  kLevelChanger,         // レベルチェンジャータスク
  kBackScreen,           // 背景タスク
  kBootLevel,            // ブートレベルタスク
  kTitleLevel,           // タイトルレベルタスク
  kBattleLevel,          // バトルレベルタスク
  kResultLevel,          // リザルトレベルタスク
  kMenuController,       // メニューコントローラタスク
  kPlayerController,     // プレイヤーコントローラタスク
  kEnemy,                // CPUタスク
  kField,                // フィールドタスク
  kStoneManager,         // 石管理タスク
  kCursorManager,        // 配置可能場所表示カーソルタスク
  kGameMode,             // ゲームモードタスク
  kStartUi,              // 開始UIタスク
  kFinishUi,             // 終了UIタスク
  kBattleInfoUi,         // バトルレベルのHUD情報
  kResultInfoUi,         // リザルトレベルのHUD情報
  kTurnSelectMenu,       // 手番選択メニュータスク
  kSoundManager,         // サウンドマネージャタスク
  kTaskIdMaxIndex        // タスクID最大値
};