﻿#pragma once

/// <summary>
/// サウンドID一覧
/// </summary>
enum class SoundId {
  kTittleBgm,         // タイトル画面BGM
  kGameStart,         // ゲームスタート
  kBattleBGM,         // バトル画面BGM
  kSetStone,          // 石を置く音
  kWin,               // 勝利サウンド
  kLose,              // 敗北サウンド
  kHandClap,          // 拍手
  kResultBgm,         // リザルト画面BGM
  kSoundMaxIndex      // サウンド数
};