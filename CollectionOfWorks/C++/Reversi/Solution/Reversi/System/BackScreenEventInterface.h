﻿#pragma once

#include <iostream>

/// <summary>
/// バックスクリーンイベントインターフェース
/// </summary>
class BackScreenEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  BackScreenEventInterface() {

    // コンソールに出力
    std::cout << "BackScreenEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~BackScreenEventInterface() {

    // コンソールに出力
    std::cout << "~BackScreenEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// 終了処理通知
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnFinishLevelEvent() {};


private:

};