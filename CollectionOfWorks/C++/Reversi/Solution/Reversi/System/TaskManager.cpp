﻿#include "System/TaskManager.h"
#include "DxLib.h"

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
TaskManager::TaskManager()
  : task_list_()
  , add_task_list_()
  , release_task_list_()
  , is_updating_(false) {

  // コンソールに出力
  std::cout << "TaskManager コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
TaskManager::~TaskManager() {

  // コンソールに出力
  std::cout << "~TaskManager デストラクタ" << std::endl;
}

/// <summary>
/// タスクを積む
/// </summary>
/// <param name=""> タスク </param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool TaskManager::AddTask(Task* task) {

  // タスクが存在しない場合は処理終了
  if (task == nullptr) {
    return false;
  }

  // タスクを降ろす設定をリセットする
  task->SetResetReleaseTask();

  // タスクIDを取得
  TaskId task_id = task->GetTaskId();

  // タスクが既にリストに登録済みか確認し、登録済みなら処理終了
  if (task_list_.find(task_id) != task_list_.end()) {
    return false;
  }

  // 更新処理中の場合は追加するタスクリストに追加
  // 更新処理中でない場合はタスクリストに追加
  if (is_updating_) {
    add_task_list_.push_back(task);
  }
  else {
    task_list_[task_id] = task;
  }

  return true;
}

/// <summary>
/// タスクを降ろす
/// </summary>
/// <param name="task_id"> タスクID </param>
/// <returns> タスク </returns>
Task* TaskManager::ReleaseTask(TaskId task_id) {

  // タスクリストに存在するタスクか確認
  if (task_list_.find(task_id) == task_list_.end()) {
    return nullptr;
  }

  // タスクIDからタスクを取得
  Task* task = task_list_[task_id];

  // タスクを降ろす設定
  task->SetReleaseTask();

  // 更新処理中の場合は降ろすタスクのタスクIDリストに追加
  // 更新処理中でない場合はタスクリストからタスクを降ろす
  if (is_updating_) {
    release_task_list_.push_back(task_id);
  }
  else {
    task_list_.erase(task_id);
  }

  return task;
}

/// <summary>
/// タスクの1フレームの処理を実行する
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void TaskManager::UpdateTask(float process_time) {

  // 更新処理中にする
  is_updating_ = true;

  // タスクリストに積んだタスクの更新処理を実行する
  for (auto task : task_list_) {
    // 対象タスクのタスクが降りていないか確認
    if (!task.second->IsReleaseTask()) {

      std::cout << "TaskManager UpdateTask タスクID：" << static_cast<int>(task.first) << std::endl;
      int start_time = GetNowCount();

      task.second->Update(process_time);

      int end_time = GetNowCount();
      float diff_time = (end_time - start_time) / 1000.0f;
      std::cout << "TaskManager UpdateTask 処理時間：" << diff_time << std::endl;
    }
  }

  // 更新処理を終了する
  is_updating_ = false;

  // 追加するタスクリストに存在するタスクをタスクリストに追加
  for (auto task : add_task_list_) {
    TaskId task_id = task->GetTaskId();
    task_list_[task_id] = task;
  }
  // 追加するタスクリストをクリアする
  add_task_list_.clear();

  // 降ろすタスクリストに存在するタスクをタスクリストから降ろす
  for (auto task_id : release_task_list_) {
    task_list_.erase(task_id);
  }
  // 降ろすタスクリストをクリアする
  release_task_list_.clear();
}

/// タスクの1フレームの描画処理を実行する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TaskManager::RenderTask() {

  // タスクリストに積んだタスクの描画処理を実行する
  for (auto task : task_list_) {
    // 対象タスクのタスクが降りていないか確認
    if (!task.second->IsReleaseTask()) {

      std::cout << "TaskManager RenderTask タスクID：" << static_cast<int>(task.first) << std::endl;
      int start_time = GetNowCount();

      task.second->Render();

      int end_time = GetNowCount();
      float diff_time = (end_time - start_time) / 1000.0f;
      std::cout << "TaskManager RenderTask 処理時間：" << diff_time << std::endl;
    }
  }
}