﻿#pragma once

#include "System/Cursor.h"

/// <summary>
/// プレイヤーコントローライベントインターフェース
/// </summary>
class PlayerControllerEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  PlayerControllerEventInterface() {

    // コンソールに出力
    std::cout << "PlayerControllerEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~PlayerControllerEventInterface() {

    // コンソールに出力
    std::cout << "~PlayerControllerEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// マウスの座標取得イベント
  /// </summary>
  /// <param name=""> カーソル </param>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  virtual void OnGetMousePosition(Cursor*, int, int) = 0;

  /// <summary>
  /// マウスの左クリックが押された時のイベント
  /// </summary>
  /// <param name=""> カーソル </param>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  virtual void OnPushMouseLeftButton(Cursor*, int, int) = 0;


private:

};