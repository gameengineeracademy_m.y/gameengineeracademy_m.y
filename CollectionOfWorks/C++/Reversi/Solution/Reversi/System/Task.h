﻿#pragma once
#include "System/TaskID.h"
#include <iostream>

/// <summary>
/// タスク
/// </summary>
/// <remarks>
/// 毎フレームの更新処理、描画処理を実行 <br/>
/// 実際の処理はタスクを継承した先で行う
/// </remarks>
class Task {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> タスクID </param>
  /// <returns></returns>
  Task(TaskId);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~Task();

  /// <summary>
  /// タスクIDを設定する
  /// </summary>
  /// <param name="task_id_"> タスクID </param>
  /// <returns></returns>
  void SetTaskId(TaskId task_id) { task_id_ = task_id; }

  /// <summary>
  /// タスクIDを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> タスクID </returns>
  TaskId GetTaskId() { return task_id_; }
  
  /// <summary>
  /// タスクを降ろしたことにする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetReleaseTask() { is_release_ = true; }

  /// <summary>
  /// タスクを降ろす設定をリセットする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetResetReleaseTask() { is_release_ = false; }

  /// <summary>
  /// タスクを降ろしたかの有無を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> タスクを降ろしたかの有無 </returns>
  bool IsReleaseTask() { return is_release_ == true; }

  /// <summary>
  /// 1フレームの処理を実行する
  /// </summary>
  /// <param name=""> 前回のメインループ処理に掛かった時間 </param>
  /// <returns></returns>
  virtual void Update(float) {}

  /// <summary>
  /// 1フレームの描画処理を実行する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void Render() {}


private:

  /// <summary>
  /// タスクID
  /// </summary>
  TaskId task_id_;

  /// <summary>
  /// 降ろしたかの有無
  /// </summary>
  bool is_release_;
};