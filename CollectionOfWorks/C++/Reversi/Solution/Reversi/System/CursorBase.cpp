﻿#include "System/CursorBase.h"

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
CursorBase::CursorBase(int cursor_id)
  : cursor_id_(cursor_id)
  , is_release_(false) {

  // コンソールに出力
  std::cout << "CursorBase コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
CursorBase::~CursorBase() {

  // コンソールに出力
  std::cout << "~CursorBase デストラクタ" << std::endl;
}