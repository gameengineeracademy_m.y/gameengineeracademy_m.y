﻿#include "System/PlayerController.h"

namespace {

  /// <summary>
  /// 戻り値 正常値
  /// </summary>
  const int kSuccess = 0;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
PlayerController::PlayerController(PlayerControllerEventInterface& player_controller_event_interface)
  : Task(TaskId::kPlayerController)
  , player_controller_event_interface_(player_controller_event_interface)
  , cursor_(nullptr)
  , push_button_{ false } {

  // コンソールに出力
  std::cout << "PlayerController コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
PlayerController::~PlayerController() {

  // コンソールに出力
  std::cout << "~PlayerController デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void PlayerController::Update(float process_time) {

  int x_pos = 0;
  int y_pos = 0;

  // マウスの座標を取得
  int check_get_pos = GetMousePoint(&x_pos, &y_pos);
  if (check_get_pos == kSuccess) {
    player_controller_event_interface_.OnGetMousePosition(cursor_, x_pos, y_pos);
  }

  // マウスの左ボタンが押されたかどうか
  bool check_left = GetPushLeftButton();
  if (check_left) {
    // マウス左ボタン クリックイベント実行
    player_controller_event_interface_.OnPushMouseLeftButton(cursor_, x_pos, y_pos);
  }
}

/// <summary>
/// 左クリック
/// </summary>
/// <param name=""></param>
/// <returns></returns>
bool PlayerController::GetPushLeftButton() {

  return GetPushButton(MOUSE_INPUT_LEFT, push_button_[static_cast<int>(ButtonType::kLeft)]);
}

/// <summary>
/// ボタンが押されたかの有無を取得する
/// </summary>
/// <param name="key_code"> キーコード </param>
/// <param name="push_key"> ボタンの押下の有無 </param>
/// <returns> ボタンが押された：false、ボタンが押されていない：false </returns>
bool PlayerController::GetPushButton(int key_code, bool& push_button) {

  // 指定のボタンが押されている
  if (key_code == GetMouseInput()) {

    // 直前まで押されていない場合は押されていることにする
    if (push_button == false) {
      push_button = true;
    }
  }
  else {
    // 前回処理時にキーが押されていた場合は押されていないことにする
    if (push_button == true) {
      push_button = false;

      // 判定を押されたとして返す
      return true;
    }
  }

  return false;
}