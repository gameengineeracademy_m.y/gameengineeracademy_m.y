﻿#include "System/Cursor.h"


/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> タスクID </param>
/// <param name=""> カーソルの種類 </param>
/// <returns></returns>
Cursor::Cursor(int cursor_id, CursorType cursor_type)
  : CursorBase(cursor_id)
  , cursor_type_(cursor_type)
  , is_display_(false)
  , x_pos_(0)
  , y_pos_(0)
  , image_width_(0)
  , image_height_(0) {

  // コンソールに出力
  std::cout << "Cursor コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Cursor::~Cursor() {

  // コンソールに出力
  std::cout << "~Cursor デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void Cursor::Update(float process_time) {

}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Cursor::Render() {

}

/// <summary>
/// 画像のサイズを取得する
/// </summary>
/// <param name="graphic_handle"> グラフィックハンドル </param>
/// <returns></returns>
void Cursor::SetImageSize(int graphic_handle) {

  // グラフィックハンドルから画像のサイズを取得
  GetGraphSize(graphic_handle, &image_width_, &image_height_);
}