﻿#pragma once

#include "DxLib.h"
#include "System/SoundID.h"
#include "System/SoundBase.h"

/// <summary>
/// サウンド
/// </summary>
class Sound : public SoundBase {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,     // 初期化処理フェーズ
    kPlay,           // 音楽再生フェーズ
    kPause,          // 音楽停止フェーズ
    kFinalize,       // 最終処理フェーズ
    kFinalized,      // 最終処理済みフェーズ
    kPhaseMaxIndex   // フェーズ数
  };

  /// <summary>
  /// サウンドの種類
  /// </summary>
  enum class PlaySoundType {
    kSoundEffect,        // SE
    kBackGroundMusic,    // BGM
    kTypeMaxIndex        // 種類の数
  };

  /// <summary>
  /// サウンドの効果の種類
  /// </summary>
  enum class EffectType {
    kNone,               // 何もしない
    kFadeIn,             // フェードイン
    kFadeOut,            // フェードアウト
    kTypeMaxIndex        // 種類の数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> サウンドID </param>
  /// <returns></returns>
  Sound(SoundId);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~Sound();

  /// <summary>
  /// 再生処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  bool SoundPlay();

  /// <summary>
  /// 再生処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  bool SoundPause();

  /// <summary>
  /// 音楽をセットする
  /// </summary>
  /// <param name=""> サウンドハンドル </param>
  /// <returns></returns>
  void SetSoundHandle(int sound_handle) { sound_handle_ = sound_handle; }

  /// <summary>
  /// 音楽を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> サウンドハンドル </returns>
  int GetSoundHandle() { return sound_handle_; }

  /// <summary>
  /// サウンドの再生方法を設定する
  /// </summary>
  /// <param name=""> サウンドの再生方法 </param>
  /// <returns></returns>
  void SetSoundPlayType(Sound::PlaySoundType sound_type) { sound_type_ = sound_type; };

  /// <summary>
  /// サウンドの再生方法を設定する
  /// </summary>
  /// <param name=""> サウンドの効果 </param>
  /// <returns></returns>
  void SetEffectType(Sound::EffectType effect_type) { effect_type_ = effect_type; };

  /// <summary>
  /// サウンド音量をセットする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetSoundVolume();

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhaseType(PhaseType phase_type) { current_phase_ = phase_type; }


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Initialize();

  /// <summary>
  /// 終了処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Finalize();

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// サウンドの再生方法
  /// </summary>
  PlaySoundType sound_type_;

  /// <summary>
  /// サウンドの効果
  /// </summary>
  EffectType effect_type_;

  /// <summary>
  /// サウンドハンドル
  /// </summary>
  int sound_handle_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;
};