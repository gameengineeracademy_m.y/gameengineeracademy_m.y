﻿#include "System/BackScreen.h"

namespace {

  /// <summary>
  /// 透過率 起動時調整値
  /// </summary>
  const int kAlphaStartAdjast = 15;

  /// <summary>
  /// 透過率 終了時調整値
  /// </summary>
  const int kAlphaFinishAdjast = 5;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
  /// <param name=""> 表示倍率 </param>
  /// <param name=""> 表示角度 </param>
  /// <param name=""> バックスクリーンイベントインターフェース </param>
/// <returns></returns>
BackScreen::BackScreen(float magnification, float angle, BackScreenEventInterface& backscreen_event_interface)
  : Task(TaskId::kBackScreen)
  , backscreen_event_interface_(backscreen_event_interface)
  , current_phase_(PhaseType::kInitialize)
  , current_draw_phase_(DrawPhaseType::kStart)
  , back_screen_handle_(0)
  , accumulate_time_(0.0f)
  , magnification_(magnification)
  , angle_(angle)
  , alpha_(0)
  , alpha_adjust_start_(0)
  , alpha_adjust_finish_(0) {

  // コンソールに出力
  std::cout << "BackScreen コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
BackScreen::~BackScreen() {

  // コンソールに出力
  std::cout << "~BackScreen デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void BackScreen::Update(float process_time) {

  bool is_finish = false;

  // 累積時間を加算
  //accumulate_time_ += process_time;

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //-------------------------
    //初期化処理フェーズ
    //-------------------------
    Initialize();
    // 現在フェーズを「処理中」に変更する
    ChangeCurrentPhase(PhaseType::kProcess);
    break;
  }
  case PhaseType::kProcess: {
    //-------------------------
    //処理中フェーズ
    //-------------------------
    is_finish = Process();
    if (is_finish) {
      // 現在フェーズを「終了」に変更する
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //-------------------------
    //終了処理フェーズ
    //-------------------------
    is_finish = Finalize();
    if (is_finish) {
      // 現在フェーズを「終了済み」に変更する
      ChangeCurrentPhase(PhaseType::kFinalized);
    }
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BackScreen::Render() {

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // マップ中央の位置を取得(マップ表示時の調整値)
  int x_center = game_info->GetCenterPointX();
  int y_center = game_info->GetCenterPointY();

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

  DrawRotaGraph(x_center, y_center, magnification_, angle_, back_screen_handle_, true);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BackScreen::Initialize() {

}

/// <summary>
/// 処理中
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理継続 </returns>
bool BackScreen::Process() {

  switch (current_draw_phase_) {
  case DrawPhaseType::kStart: {
    // 透過率に調整値を加算
    alpha_ += alpha_adjust_start_;
    if (alpha_ >= kAlphaMax) {
      alpha_ = kAlphaMax;
    }
    break;
  }
  case DrawPhaseType::kFinish: {
    // 透過率に調整値を減算
    alpha_ -= alpha_adjust_finish_;
    if (alpha_ <= kAlphaMin) {
      alpha_ = kAlphaMin;
    }
    break;
  }
  }
  return false;
}

/// <summary>
/// 終了処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
bool BackScreen::Finalize() {

  switch (current_draw_phase_) {
  case DrawPhaseType::kFinish: {
    // 透過率に調整値を減算
    alpha_ -= alpha_adjust_finish_;
    if (alpha_ <= kAlphaMin) {
      alpha_ = kAlphaMin;
    }
    break;
  }
  }

  switch (current_draw_phase_) {
  case DrawPhaseType::kFinish: {
    if (alpha_ != kAlphaMin) {
      return false;
    }
  }
  }

  // 実行レベルへ終了通知
  backscreen_event_interface_.OnFinishLevelEvent();

  return true;
}

/// <summary>
/// 透過率 調整値をセットする
/// </summary>
/// <param name=""> 開始変化量 </param>
/// <param name=""> 終了変化量 </param>
/// <returns></returns>
void BackScreen::SetAlphaAdjustValue(int start_value, int finish_value) {

  alpha_adjust_start_ = start_value;
  alpha_adjust_finish_ = finish_value;
}