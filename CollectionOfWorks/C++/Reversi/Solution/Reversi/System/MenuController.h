﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "System/MenuControllerEventInterface.h"

/// <summary>
/// メニューコントローラ
/// </summary>
/// <remarks>
/// 仮想コントローラ
/// </remarks>
class MenuController : public Task {
public:

  /// <summary>
  /// ボタンの種類
  /// </summary>
  enum class ButtonType {
    kLeft,                // 左クリック
    kButtonTypeMaxIndex   // ボタン項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> メニューコントローライベントインターフェース </param>
  /// <returns></returns>
  MenuController(MenuControllerEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~MenuController();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 左クリック
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  bool GetPushLeftButton();


private:

  /// <summary>
  /// ボタンが押されたかの有無を取得する
  /// </summary>
  /// <param name="key_code">キーコード</param>
  /// <param name="push_key">ボタンの押下の有無</param>
  /// <returns>キーが押された：false、キーが押されていない：false</returns>
  bool GetPushButton(int, bool&);


private:

  /// <summary>
  /// メニューコントローラのイベントインターフェース
  /// </summary>
  MenuControllerEventInterface& menu_controller_event_interface_;

  /// <summary>
  /// ボタンの押下の有無
  /// </summary>
  bool push_button_[static_cast<int>(ButtonType::kButtonTypeMaxIndex)];
};