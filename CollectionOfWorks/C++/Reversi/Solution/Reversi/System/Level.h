﻿#pragma once

#include "System/Task.h"
#include "System/TaskManager.h"

/// <summary>
/// レベル
/// </summary>
/// <remarks>
/// 起動画面、タイトル画面など各シーンを扱う
/// </remarks>
class Level : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kNone,              // 何もしないフェーズ
    kInitialize,        // 初期化処理フェーズ
    kInitialized,       // 初期化処理済みフェーズ
    kProcess,           // 処理フェーズ
    kRequestFinish,     // 終了リクエストフェーズ
    kFinish,            // 終了フェーズ
    kFinished,          // 終了済みフェーズ
    kPhaseTypeMaxIndex  // フェーズ数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> タスクID </param>
  /// <param name=""> タスクマネージャ </param>
  /// <returns></returns>
  Level(TaskId, TaskManager&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~Level();

  /// <summary>
  /// 1フレームの処理を実行する
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 1フレームの描画処理を実行する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// 終了リクエストのフェーズかの有無を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了リクエストフェーズ, false:それ以外 </returns>
  bool IsRequestFinishPhase() { return current_phase_ == PhaseType::kRequestFinish; }

  /// <summary>
  /// 初期化処理を開始する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetInitializePhase() { ChangeCurrentPhase(PhaseType::kInitialize); }

  /// <summary>
  /// 初期化済みのフェーズかの有無を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:初期化済みフェーズ, false:それ以外 </returns>
  bool IsInitializedPhase() { return current_phase_ == PhaseType::kInitialized; }

  /// <summary>
  /// 終了処理を開始する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetFinishPhase() { ChangeCurrentPhase(PhaseType::kFinish); }

  /// <summary>
  /// 終了済みのフェーズかの有無を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了済みのフェーズ, false:それ以外 </returns>
  bool IsFinishedPhase() { return current_phase_ == PhaseType::kFinished; }

  /// <summary>
  /// レベル処理を開始する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetProcessPhase() { ChangeCurrentPhase(PhaseType::kProcess); }

  /// <summary>
  /// レベルを切り替える
  /// </summary>
  /// <param name=""> 次のレベル </param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool ChangeLevel(TaskId);

  /// <summary>
  /// 次のレベルに遷移するタスクIDを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  TaskId GetNextLevelTaskId() { return next_level_task_id_; }

  /// <summary>
  /// 終了リクエストを設定する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetRequestFinish() { ChangeCurrentPhase(PhaseType::kRequestFinish); }

  /// <summary>
  /// エラー発生
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OccurredError();

  /// <summary>
  /// エラー発生の有無を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  bool IsError() { return is_error_; }

  /// <summary>
  /// 現在のフェーズを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  PhaseType GetPhaseType() { return current_phase_; }


protected:

  /// <summary>
  /// 初期化処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  virtual bool InitializePhase(float) = 0;

  /// <summary>
  /// 毎フレームの処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  virtual bool UpdatePhase(float) = 0;

  /// <summary>
  /// 毎フレームの描画処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void RenderPhase() = 0;

  /// <summary>
  /// 終了処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  virtual bool FinishPhase(float) = 0;


private:

  /// <summary>
  /// 現在のフェーズの変更
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType);


protected:

  /// <summary>
  /// タスクマネージャ
  /// </summary>
  TaskManager& task_manager_;

private:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 次に遷移するレベルのタスクID
  /// </summary>
  TaskId next_level_task_id_;

  /// <summary>
  /// エラー発生の有無
  /// </summary>
  bool is_error_;
};