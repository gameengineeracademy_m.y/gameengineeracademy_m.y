﻿#include "System/SoundBase.h"

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> サウンドID </param>
/// <returns></returns>
SoundBase::SoundBase(SoundId sound_id)
  : sound_id_(sound_id)
  , is_release_(false) {

  // コンソールに出力
  std::cout << "SoundBase コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
SoundBase::~SoundBase() {

  // コンソールに出力
  std::cout << "~SoundBase デストラクタ" << std::endl;
}

