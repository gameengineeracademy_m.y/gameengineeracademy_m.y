﻿#include "System/Sound.h"

namespace {

  /// <summary>
  /// 再生中
  /// </summary>
  const int kSoundPlay = 1;

  /// <summary>
  /// 音量 BGM初期値
  /// </summary>
  const int kInitialVolumeBgm = 100;

  /// <summary>
  /// 音量 SE初期値
  /// </summary>
  const int kInitialVolumeSe = 100;

  /// <summary>
  /// 待機時間
  /// </summary>
  const float kWaitTime = 0.05f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Sound::Sound(SoundId sound_id)
  : SoundBase(sound_id)
  , current_phase_(PhaseType::kInitialize)
  , sound_type_(PlaySoundType::kSoundEffect)
  , effect_type_(EffectType::kNone)
  , sound_handle_(0)
  , accumulate_time_(0.0f) {

  // コンソールに出力
  std::cout << "Sound コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Sound::~Sound() {

  // コンソールに出力
  std::cout << "~Sound デストラクタ" << std::endl;
}

/// <summary>
/// サウンド音量をセットする
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Sound::SetSoundVolume() {

  switch (sound_type_) {
  case PlaySoundType::kSoundEffect: {
    // 音量を設定する
    ChangeVolumeSoundMem(kInitialVolumeSe, sound_handle_);
  }
  case PlaySoundType::kBackGroundMusic: {
    // 音量を設定する
    ChangeVolumeSoundMem(kInitialVolumeBgm, sound_handle_);
  }
  }
}

/// <summary>
/// 再生処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
bool Sound::SoundPlay() {

  int result = -1;

  int start_time = GetNowCount();

  switch (sound_type_) {
  case PlaySoundType::kSoundEffect: {
    //----------------------------
    //サウンドエフェクト
    //----------------------------
    // サウンドが「一時停止中」なら続きから再生
    // それ以外なら最初から再生
    if (current_phase_ == PhaseType::kPause) {
      result = PlaySoundMem(sound_handle_, DX_PLAYTYPE_BACK, false);
    }
    else {
      result = PlaySoundMem(sound_handle_, DX_PLAYTYPE_BACK, true);
      std::cout << "Sound::SoundPlay BACK" << std::endl;
    }
    break;
  }
  case PlaySoundType::kBackGroundMusic: {
    //----------------------------
    //バックグラウンドミュージック
    //----------------------------
    // サウンドが「一時停止中」なら続きから再生
    // それ以外なら最初から再生
    if (current_phase_ == PhaseType::kPause) {
      result = PlaySoundMem(sound_handle_, DX_PLAYTYPE_LOOP, false);
    }
    else {
      // 再生中か確認し、再生中なら処理終了
      if (CheckSoundMem(sound_handle_) == kSoundPlay) {
        return true;
      }

      result = PlaySoundMem(sound_handle_, DX_PLAYTYPE_LOOP, true);
      std::cout << "Sound::SoundPlay LOOP" << std::endl;
    }
    break;
  }
  }

  int end_time = GetNowCount();
  float diff_time = (end_time - start_time) / 1000.0f;
  std::cout << "Sound::SoundPlay 開始時間：" << start_time << std::endl;
  std::cout << "Sound::SoundPlay 終了時間：" << end_time << std::endl;
  std::cout << "Sound::SoundPlay 処理時間：" << diff_time << std::endl;

  if (result == 0) {
    // 現在のフェーズを「再生中」に変更
    ChangeCurrentPhaseType(PhaseType::kPlay);
    return true;
  }

  return false;
}

/// <summary>
/// 停止処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
bool Sound::SoundPause() {

  // サウンドが再生中以外の場合は処理終了
  if (CheckSoundMem(sound_handle_) != kSoundPlay) {
    return false;
  }

  // 現在のフェーズを「停止中」に変更
  ChangeCurrentPhaseType(PhaseType::kPause);
  // 指定ハンドルの再生を止める
  StopSoundMem(sound_handle_);

  return true;
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Sound::Initialize() {


}

/// <summary>
/// 終了処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Sound::Finalize() {


}