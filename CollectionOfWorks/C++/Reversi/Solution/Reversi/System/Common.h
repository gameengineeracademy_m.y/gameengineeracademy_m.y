﻿#pragma once

#include "System/TaskID.h"
#include <vector>

/// <summary>
/// 共通変数宣言
/// </summary>
/// <remarks>
/// デバッグ時とリリースで読み込み値を分ける場合に使用 <br/>
/// </remarks>
class Common {
public:

#ifdef _DEBUG

  // 起動後の初回遷移レベル指定　LevelChangerで使用
  static const TaskId start_level_ = TaskId::kBootLevel;

  // 先攻/後攻の決定方法
  static const bool first_attack_decide = true;
  // 石 初期設置数
  static const int initial_black_stone = 2;
  // 石 初期設置数
  static const int initial_white_stone = 2;
  // オートで進める手数
  static const int auto_step_advance = 0;


#else

  // 起動後の初回遷移レベル指定　LevelChangerで使用
  static const TaskId start_level_ = TaskId::kBootLevel;

  // 先攻/後攻の決定方法
  static const bool first_attack_decide = true;
  // 石 初期設置数
  static const int initial_black_stone = 2;
  // 石 初期設置数
  static const int initial_white_stone = 2;
  // オートで進める手数
  static const int auto_step_advance = 0;

#endif
};