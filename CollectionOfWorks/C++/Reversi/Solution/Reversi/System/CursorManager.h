﻿#pragma once

#include "System/Task.h"
#include "System/Cursor.h"
#include "Game/PlayerCursor.h"
#include "Game/EnemyCursor.h"
#include "Game/PlaceableCursor.h"
#include "Game/TurnOverCursor.h"
#include <map>
#include <vector>
#include <iostream>

class CursorManager : public Task {
public:
  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  CursorManager();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~CursorManager();

  /// <summary>
  /// カーソルを生成する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void CreateCursor();

  /// <summary>
  /// カーソルを破棄する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DisposeCursor();

  /// <summary>
  /// 配置可能箇所表示カーソルを積む
  /// </summary>
  /// <param name=""> 配置可能箇所表示カーソル </param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool AddPlaceableCursor(PlaceableCursor*);

  /// <summary>
  /// ひっくり返す石表示カーソルを積む
  /// </summary>
  /// <param name=""> ひっくり返す石表示カーソル </param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool AddTurnOverCursor(TurnOverCursor*);

  /// <summary>
  /// 配置可能箇所表示カーソルを降ろす
  /// </summary>
  /// <param name=""> カーソルID </param>
  /// <returns> 配置可能箇所表示カーソル </returns>
  PlaceableCursor* ReleasePlaceableCursor(int);

  /// <summary>
  /// ひっくり返す石表示カーソルを降ろす
  /// </summary>
  /// <param name=""> カーソルID </param>
  /// <returns> ひっくり返す石表示カーソル </returns>
  TurnOverCursor* ReleaseTurnOverCursor(int);

  /// <summary>
  /// タスクの1フレームの処理を実行する
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// タスクの1フレームの描画処理を実行する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// プレイヤーカーソルを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  PlayerCursor* GetPlayerCursor() { return player_cursor_; }

  /// CPUカーソルを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  EnemyCursor* GetEnemyCursor() { return enemy_cursor_; }

  /// 配置可能箇所表示カーソルを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  PlaceableCursor* GetPlaceableCursor(std::pair<int, int> pos) { return placeable_cursor_list_[position_list_[pos]]; }

  /// ひっくり返す石表示カーソルを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  TurnOverCursor* GetTurnOverCursor(std::pair<int, int> pos) { return turn_over_cursor_list_[position_list_[pos]]; }


private:

  /// <summary>
  /// プレイヤーカーソル 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void CreatePlayerCursor();

  /// <summary>
  /// CPUカーソル 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void CreateEnemyCursor();

  /// <summary>
  /// 配置可能箇所表示カーソル 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void CreatePlaceableCursor();

  /// <summary>
  /// ひっくり返す石表示カーソル 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void CreateTurnOverCursor();

  /// <summary>
  /// インデックスから盤上の座標を取得する
  /// </summary>
  /// <param name=""> インデックス </param>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  void CalcBoradPosition(int, int&, int&);


private:

  /// <summary>
  /// プレイヤー選択カーソルリスト
  /// </summary>
  PlayerCursor* player_cursor_;

  /// <summary>
  /// CPU選択カーソルリスト
  /// </summary>
  EnemyCursor* enemy_cursor_;

  /// <summary>
  /// 配置可能カーソルリスト
  /// </summary>
  std::unordered_map<int, PlaceableCursor*> placeable_cursor_list_;

  /// <summary>
  /// ひっくり返す石カーソルリスト
  /// </summary>
  std::unordered_map<int, TurnOverCursor*> turn_over_cursor_list_;

  /// <summary>
  /// 盤上の座標→カーソルID取得リスト 
  /// </summary>
  std::map<std::pair<int, int>, int> position_list_;
};