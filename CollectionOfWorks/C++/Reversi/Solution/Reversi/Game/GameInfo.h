﻿#pragma once

#include <iostream>
#include <unordered_map>

/// <summary>
/// ゲーム情報
/// </summary>
class GameInfo {
public:

  /// <summary>
  /// 操作モード
  /// </summary>
  enum class OperationMode {
    kManual,           // マニュアル
    kAuto,             // オート
    kModeMaxIndex      // モード数
  };

  /// <summary>
  /// 勝敗結果
  /// </summary>
  enum class BattleResult {
    kWin,              // プレイヤー勝利
    kDraw,             // 引き分け
    kLose,             // プレイヤー敗北
    kModeMaxIndex      // 要素数
  };

  /// <summary>
  /// プレイヤーの種類
  /// </summary>
  enum class PlayerType {
    kPlayer,           // プレイヤー
    kCpu,              // CPU
    kPlayerMaxIndex    // 要素数
  };

  /// <summary>
  /// 石の種類
  /// </summary>
  enum class StoneType {
    kBlack,            // 黒
    kWhite,            // 白
    kStoneMaxIndex     // 要素数
  };

  /// <summary>
  /// プレイヤー手番の種類
  /// </summary>
  enum class TurnType {
    kNone,              // 設定なし
    kFirstAttack,       // 先攻
    kSecondAttack,      // 後攻
    kRandom,            // ランダム
    kTurnTypeMaxIndex   // 要素数
  };

public:

  /// <summary>
  /// GameInfo インスタンスを作成する
  /// </summary>
  /// <param> 画面解像度 幅 </param>
  /// <param> 画面解像度 高さ </param>
  /// <returns></returns>
  static void CreateGameInfo();

  /// <summary>
  /// GameInfo インスタンスを取得する
  /// </summary>
  /// <param></param>
  /// <returns></returns>
  static GameInfo* GetGameInfoInstance() { return game_info_instance_; }

  /// <summary>
  /// GameInfo インスタンスを破棄する
  /// </summary>
  /// <param></param>
  /// <returns></returns>
  static void ReleaseGameInfo();

  /// <summary>
  /// ゲーム情報を初期化する
  /// </summary>
  /// <param></param>
  /// <returns></returns>
  void InitializeGameInfo();

  /// <summary>
  /// 解像度のXサイズを取得
  /// </summary>
  /// <param></param>
  /// <returns> 解像度のXサイズ </returns>
  int GetResolutionWidth() { return resolution_horizontal_; }

  /// <summary>
  /// 解像度のYサイズを取得
  /// </summary>
  /// <param></param>
  /// <returns> 解像度のYサイズ </returns>
  int GetResolutionHeight() { return resolution_vertical_; }

  /// <summary>
  /// 画面の中心点のX座標を取得
  /// </summary>
  /// <param></param>
  /// <returns> 画像の中心点のX座標 </returns>
  int GetCenterPointX() { return center_point_x_; }

  /// <summary>
  /// 画面の中心点のY座標を取得
  /// </summary>
  /// <param></param>
  /// <returns> 画像の中心点のY座標 </returns>
  int GetCenterPointY() { return center_point_y_; }

  /// <summary>
  /// マップのサイズをセットする
  /// </summary>
  /// <param> マップのXサイズ </param>
  /// <param> マップのYサイズ </param>
  /// <returns></returns>
  void SetMapSize(int, int);

  /// <summary>
  /// マップの左上のX座標を取得
  /// </summary>
  /// <param></param>
  /// <returns> マップの左上のX座標 </returns>
  int GetMapPositionX() { return map_position_x_; }

  /// <summary>
  /// マップの左上のY座標を取得
  /// </summary>
  /// <param></param>
  /// <returns> マップの左上のY座標 </returns>
  int GetMapPositionY() { return map_position_y_; }

  /// <summary>
  /// 操作モードを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 操作モード </returns>
  OperationMode GetOperationMode() { return operation_mode_; }

  /// <summary>
  /// 操作モードがマニュアルか確認する
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:マニュアル, false:オート </returns>
  bool IsOperationModeMaual() { return operation_mode_ == OperationMode::kManual; }

  /// <summary>
  /// 操作モードをセットする
  /// </summary>
  /// <param name=""></param>
  /// <returns> 操作モード </returns>
  void SetOperationMode(OperationMode operation_mode) { operation_mode_ = operation_mode; }

  /// <summary>
  /// バトル結果をセットする
  /// </summary>
  /// <param name=""> バトル結果 </param>
  /// <returns></returns>
  void SetBattleResult(BattleResult battle_result) { battle_result_ = battle_result; }

  /// <summary>
  /// バトル結果を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> バトル結果 </returns>
  BattleResult GetBattleResult() { return battle_result_; }

  /// <summary>
  /// 黒色の石の数をセット
  /// </summary>
  /// <param name=""> 黒色の石の数 </param>
  /// <param name=""> 白色の石の数 </param>
  /// <returns></returns>
  void SetStoneCount(int, int);

  /// <summary>
  /// 黒色の石の数を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 石の数 </returns>
  int GetBlackStoneCount() { return stone_black_count_; }

  /// <summary>
  /// 白色の石の数を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 石の数 </returns>
  int GetWhiteStoneCount() { return stone_white_count_; }

  /// <summary>
  /// ゲーム情報の設定
  /// </summary>
  /// <param name=""> プレイヤーの種類 </param>
  /// <param name=""> 石の種類 </param>
  /// <returns></returns>
  void SetGameData(PlayerType player_type, StoneType stone_type) { game_data_[player_type] = stone_type; };

  /// <summary>
  /// ゲーム情報を取得する
  /// </summary>
  /// <param name=""> プレイヤーの種類 </param>
  /// <returns> 石の種類 </returns>
  StoneType GetGameData(PlayerType player_type) { return game_data_[player_type]; };

  /// <summary>
  /// デバッグ有無を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:デバッグ, false:リリース </returns>
  bool IsDebug() { return is_debug_; }

  /// <summary>
  /// プレイヤーの手番の種類をセットする
  /// </summary>
  /// <param name=""> 手番の種類 </param>
  /// <returns></returns>
  void SetTurnType(TurnType turn_type) { turn_type_ = turn_type; }

  /// <summary>
  /// プレイヤーの手番の種類を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 手番の種類 </returns>
  TurnType GetTurnType() { return turn_type_; }


private:

  /// <summary>
  /// コンストラクタ
  /// </summary>
  /// <param></param>
  /// <returns></returns>
  GameInfo();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param></param>
  /// <returns></returns>
  ~GameInfo();


private:

  /// <summary>
  /// インスタンス (唯一のもの)
  /// </summary>
  static GameInfo* game_info_instance_;

  /// <summary>
  /// 解像度のXサイズ
  /// </summary>
  int resolution_horizontal_;

  /// <summary>
  /// 解像度のYサイズ
  /// </summary>
  int resolution_vertical_;

  /// <summary>
  /// 画面の中心のX座標
  /// </summary>
  int center_point_x_;

  /// <summary>
  /// 画面の中心のY座標
  /// </summary>
  int center_point_y_;

  /// <summary>
  /// マップの横幅
  /// </summary>
  int map_width_;

  /// <summary>
  /// マップの高さ
  /// </summary>
  int map_height_;

  /// <summary>
  /// 表示位置(マップの左上)のXサイズ
  /// </summary>
  int map_position_x_;

  /// <summary>
  /// 表示位置(マップの左上)のYサイズ
  /// </summary>
  int map_position_y_;

  /// <summary>
  /// モード設定
  /// </summary>
  OperationMode operation_mode_;

  /// <summary>
  /// ターン情報 ターンと石の種類
  /// </summary>
  std::unordered_map<PlayerType, StoneType> game_data_;

  /// <summary>
  /// バトル結果
  /// </summary>
  BattleResult battle_result_;

  /// <summary>
  /// 盤上の黒の石の総数
  /// </summary>
  int stone_black_count_;

  /// <summary>
  /// 盤上の白の石の総数
  /// </summary>
  int stone_white_count_;

  /// <summary>
  /// デバッグ有無
  /// </summary>
  bool is_debug_;

  /// <summary>
  /// プレイヤー手番情報
  /// </summary>
  TurnType turn_type_;
};