﻿#pragma once

#include <iostream>
#include <vector>

/// <summary>
/// エネミーイベントインターフェース
/// </summary>
class EnemyEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  EnemyEventInterface() {

    // コンソールに出力
    std::cout << "EnemyEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~EnemyEventInterface() {

    // コンソールに出力
    std::cout << "~EnemyEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// バトルレベルのフェーズが「プレイ中」かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual bool OnCheckBattleLevelPhasePlay() = 0;

  /// <summary>
  /// CPUの思考処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnThinkPlacement() = 0;

  /// <summary>
  /// カーソルに表示座標をセット
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  virtual void OnSetCursorPosition(int, int) = 0;


private:

};