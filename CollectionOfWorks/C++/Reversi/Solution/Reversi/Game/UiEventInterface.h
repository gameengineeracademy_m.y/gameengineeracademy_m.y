﻿#pragma once

#include <iostream>

class UiEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  UiEventInterface() {

    // コンソールに出力
    std::cout << "UiEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~UiEventInterface() {

    // コンソールに出力
    std::cout << "~UiEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// UI終了のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnFinishDisplayUi() = 0;


private:

};