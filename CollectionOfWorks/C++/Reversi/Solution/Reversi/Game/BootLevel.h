﻿#pragma once

#include "DxLib.h"
#include "System/Level.h"
#include "System/BackScreen.h"
#include "Game/GameInfo.h"
#include "Game/BootLevelEventInterface.h"
#include <array>


namespace {

  /// <summary>
  /// 文字列最大値
  /// </summary>
  const int kStringMaxNum = 4;

  /// <summary>
  /// 石の最大要素数
  /// </summary>
  const int kImageStoneMaxNum = 16;
}

/// <summary>
/// ブートレベル
/// </summary>
/// <remarks>
/// 起動した直後に表示される画面のタスク
/// </remarks>
class BootLevel : public Level, public BootLevelEventInterface {
public:

  /// <summary>
  /// 終了フェーズの種類
  /// </summary>
  enum class FinalizePhaseType {
    kUnloadTask,         // タスクを降ろすフェーズ
    kDisposeTask,        // タスクを破棄するフェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

  /// <summary>
  /// 描画フェーズ
  /// </summary>
  enum class DrawPhaseType {
    kStart,              // 開始フェーズ
    kFinish,             // 終了フェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> タスクID </param>
  /// <param name=""> タスクマネージャ </param>
  /// <returns></returns>
  BootLevel(TaskId, TaskManager&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~BootLevel();

  /// <summary>
  /// 描画フェーズを変更する
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns></returns>
  void ChangeDrawPhase(DrawPhaseType draw_phase) { current_draw_phase_ = draw_phase; };


protected:

  /// <summary>
  /// 初期化処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool InitializePhase(float) override;

  /// <summary>
  /// 毎フレームの処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool UpdatePhase(float) override;

  /// <summary>
  /// 毎フレームの描画処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void RenderPhase() override;

  /// <summary>
  /// 終了処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool FinishPhase(float) override;


private:

  /// <summary>
  /// 背景
  /// </summary>
  BackScreen* back_screen_;

  /// <summary>
  /// 現在の終了フェーズ
  /// </summary>
  FinalizePhaseType finalize_phase_;

  /// <summary>
  /// フェーズの種類
  /// </summary>
  DrawPhaseType current_draw_phase_;

  /// <summary>
  /// 累積処理時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// 表示ブート文字列
  /// </summary>
  std::string display_string_[kStringMaxNum];

  /// <summary>
  /// 配列インデックス
  /// </summary>
  int string_index_;

  /// <summary>
  /// 石のグラフィックハンドル
  /// </summary>
  std::array<int, kImageStoneMaxNum> stone_handle_;

  /// <summary>
  /// 石の描画インデックス
  /// </summary>
  int drawing_index_;

  /// <summary>
  /// 表示位置 調整値
  /// </summary>
  int y_pos_;

  /// <summary>
  /// 表示位置 変化量
  /// </summary>
  int y_pos_adjast_;

  /// <summary>
  /// 描画 透過率
  /// </summary>
  int alpha_;
};