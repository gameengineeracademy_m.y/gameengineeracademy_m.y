﻿#pragma once

#include <iostream>

/// <summary>
/// ストーンベース
/// </summary>
/// <remarks>
/// ストーンクラスの基底クラス
/// </remarks>
class StoneBase {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> ストーンID </param>
  /// <returns></returns>
  StoneBase(int);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~StoneBase();

  /// <summary>
  /// ストーンIDを設定する
  /// </summary>
  /// <param name="stone_id_"> ストーンID </param>
  /// <returns></returns>
  void SetStoneId(int stone_id) { stone_id_ = stone_id; }

  /// <summary>
  /// ストーンIDを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> ストーンID </returns>
  int GetStoneId() { return stone_id_; }

  /// <summary>
  /// 石を降ろしたことにする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetReleaseStone() { is_release_ = true; }

  /// <summary>
  /// 石を降ろす設定をリセットする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetResetReleaseStone() { is_release_ = false; }

  /// <summary>
  /// 石を降ろしたかの有無を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 石を降ろしたかの有無 </returns>
  bool IsReleaseStone() { return is_release_ == true; }

  /// <summary>
  /// 1フレームの処理を実行する
  /// </summary>
  /// <param name=""> 前回のメインループ処理に掛かった時間 </param>
  /// <returns></returns>
  virtual void Update(float) {}

  /// <summary>
  /// 1フレームの描画処理を実行する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void Render() {}


private:

  /// <summary>
  /// ストーンID
  /// </summary>
  int stone_id_;

  /// <summary>
  /// 降ろしたかの有無
  /// </summary>
  bool is_release_;
};