﻿#include "Game/BootLevel.h"

namespace {

  /// <summary>
  /// 累積時間のリセット値
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// タイトルレベルへ遷移する時間 [sec]
  /// </summary>
  const float kWaitTimeSecond = 1.5f;

  /// <summary>
  /// 文字の色
  /// </summary>
  const int kStringForeColor = GetColor(0, 0, 0);     // 黒色

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 背景 表示倍率
  /// </summary>
  const float kBackScreenMagnification = 1.4f;

  /// <summary>
  /// 背景 表示角度 [rad]
  /// </summary>
  const float kBackScreenAngle = 0.0f;

  /// <summary>
  /// アイコン 表示倍率
  /// </summary>
  const float kIconMagnification = 1.2f;

  /// <summary>
  /// アイコン 表示角度 [rad]
  /// </summary>
  const float kIconAngle = 0.0f;

  /// <summary>
  /// 背景画像パス
  /// </summary>
  const char* kImagePathBackScreen = "Assets/Image/backscreen.png";

  /// <summary>
  /// アイコン画像パス
  /// </summary>
  const char* kImagePathIcon = "Assets/Image/boot_icon.png";

  /// <summary>
  /// 表示文字列
  /// </summary>
  const char* kDisplayString = "Boot";

  /// <summary>
  /// 表示文字列 ピリオド
  /// </summary>
  const char* kDisplayStringPeriod = ".";

  /// <summary>
  /// 指定時間 ピリオド増加
  /// </summary>
  const float kDispWaitTime = 0.32f;

  /// <summary>
  /// 石の画像の総分割数
  /// </summary>
  const int kDivTotalNum = 16;

  /// <summary>
  /// 石の画像の横方向の分割数
  /// </summary>
  const int kDivXNum = 2;

  /// <summary>
  /// 石の画像の縦方向の分割数
  /// </summary>
  const int kDivYNum = 8;

  /// <summary>
  /// 画像分割後の1枚あたりの幅
  /// </summary>
  const int kWidth = 64;

  /// <summary>
  /// 画像分割後の1枚あたりの高さ
  /// </summary>
  const int kHeight = 64;

  /// <summary>
  /// 画像読み込み時のエラー値
  /// </summary>
  const int kLoadError = -1;

  /// <summary>
  /// 石の画像最小インデックス
  /// </summary>
  const int kImageStoneInitIndex = 0;

  /// <summary>
  /// 石の画像最大インデックス
  /// </summary>
  const int kImageStoneMaxIndex = 15;

  /// <summary>
  /// 表示文字 最大インデックス
  /// </summary>
  const int kStringMaxIndex = 3;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSize = 50;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 8;

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Meiryo UI";

  /// <summary>
  /// 透過率 起動時調整値
  /// </summary>
  const int kAlphaAdjastStart = 15;

  /// <summary>
  /// 透過率 終了時調整値
  /// </summary>
  const int kAlphaAdjastFinish = 15;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// マイナスの符号
  /// </summary>
  const int kSignMinus = -1;

  /// <summary>
  /// 石が跳ねる表現 変化量 その1
  /// </summary>
  const int kJumpRate01 = 7;

  /// <summary>
  /// 石が跳ねる表現 変化量 その2
  /// </summary>
  const int kJumpRate02 = 2;

  /// <summary>
  /// 石の到達位置1
  /// </summary>
  const int kArrivalPos01 = 0;

  /// <summary>
  /// 石の到達位置2
  /// </summary>
  const int kArrivalPos02 = 70;

  /// <summary>
  /// 石の到達位置3
  /// </summary>
  const int kArrivalPos03 = 90;

  /// <summary>
  /// ゼロ
  /// </summary>
  const int kZero = 0;

  /// <summary>
  /// 石の描画位置 横方向調整値
  /// </summary>
  const int kStonePosX = 70;

  /// <summary>
  /// 石の描画位置 縦方向調整値
  /// </summary>
  const int kStonePosY = 15;

  /// <summary>
  /// 文字の描画位置 横方向調整値
  /// </summary>
  const int kTextPosX = 60;
}


/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> タスクID </param>
/// <param name=""> タスクマネージャ </param>
/// <returns></returns>
BootLevel::BootLevel(TaskId task_id, TaskManager& task_manager)
  : Level(task_id, task_manager)
  , back_screen_(nullptr)
  , finalize_phase_(FinalizePhaseType::kUnloadTask)
  , current_draw_phase_(DrawPhaseType::kStart)
  , accumulate_time_(0.0f)
  , display_string_()
  , string_index_(0)
  , stone_handle_()
  , drawing_index_(0)
  , y_pos_(0)
  , y_pos_adjast_(0)
  , alpha_(0) {

  // コンソールに出力
  std::cout << "BootLevel コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
BootLevel::~BootLevel() {

  // コンソールに出力
  std::cout << "~BootLevel デストラクタ" << std::endl;
}

/// <summary>
/// 初期化処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool BootLevel::InitializePhase(float process_time) {

  // 処理時間をリセットしておく
  accumulate_time_ = kResetTime;

  //------------------------------------
  //背景 準備
  //------------------------------------
  // 画像のロード
  int graphic_handle = LoadGraph(kImagePathBackScreen);
  if (graphic_handle == kLoadError) {
    std::cout << "BootLevel InitializePhase：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }
  // 背景 生成
  back_screen_ = new BackScreen(kBackScreenMagnification, kBackScreenAngle, *this);
  if (back_screen_ == nullptr) {
    std::cout << "BootLevel InitializePhase：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // グラフィックハンドルを渡す
  back_screen_->SetGraphicHandle(graphic_handle);
  // 透過率の変化量の指定
  back_screen_->SetAlphaAdjustValue(kAlphaAdjastStart, kAlphaAdjastFinish);
  // タスクマネージャにタスクを積む
  task_manager_.AddTask(back_screen_);

  //------------------------------------
  //画面表示画像 準備
  //------------------------------------
  // 画像のロード
  std::array<int, static_cast<int>(kDivTotalNum)> stone_handle;
  int result = LoadDivGraph(kImagePathIcon, kDivTotalNum, kDivXNum, kDivYNum, kWidth, kHeight, stone_handle.data());
  if (result == kLoadError) {
    // コンソールに出力
    std::cout << "BootLevel InitializePhase：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }
  stone_handle_ = stone_handle;

  //------------------------------------
  //画面表示文字列 準備
  //------------------------------------
  std::string period = "";
  // 配列の作成
  for (int i = 0; i < kStringMaxNum; ++i) {
    display_string_[i] = kDisplayString + period;
    // ピリオドの数を追加
    period += static_cast<std::string>(kDisplayStringPeriod);
  }
  return true;
}

/// <summary>
/// 毎フレームの処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool BootLevel::UpdatePhase(float process_time) {

  // 処理時間を累積していく
  accumulate_time_ += process_time;

  // 背景処理が破棄済みなら処理終了し、終了処理へ
  if (back_screen_ == nullptr) {
    return true;
  }

  switch (current_draw_phase_) {
  case DrawPhaseType::kStart: {
    // 透過率に調整値を加算
    alpha_ += kAlphaAdjastStart;
    if (alpha_ >= kAlphaMax) {
      alpha_ = kAlphaMax;
    }
    break;
  }
  case DrawPhaseType::kFinish: {
    // 透過率に調整値を減算
    alpha_ -= kAlphaAdjastFinish;
    if (alpha_ <= kAlphaMin) {
      alpha_ = kAlphaMin;
    }
    break;
  }
  }

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return false;
  }

  // 描画インデックスをインクリメント
  ++drawing_index_;
  if (drawing_index_ == kImageStoneMaxIndex) {
    drawing_index_ = kImageStoneInitIndex;
  }

  // 文字の描画インデックスを指定時間ごとにインクリメント
  int index = string_index_;
  if (string_index_ < kStringMaxIndex) {
    if (accumulate_time_ >= kDispWaitTime * (++index)) {
      ++string_index_;
    }
  }

  // 文字の描画インデックスが最大値を迎えたら処理開始
  if (string_index_ == kStringMaxIndex) {
    switch (y_pos_) {
    case kArrivalPos01: {
      y_pos_adjast_ = kJumpRate01;
      break;
    }
    case kArrivalPos02: {
      if (y_pos_adjast_ > kZero) {
        y_pos_adjast_ = kJumpRate02;
      }
      else {
        y_pos_adjast_ = kSignMinus * kJumpRate01;
      }
      break;
    }
    case kArrivalPos03: {
      y_pos_adjast_ = kSignMinus * kJumpRate02;
      break;
    }
    }
  }

  y_pos_ += y_pos_adjast_;

  if (current_draw_phase_ == DrawPhaseType::kFinish) {
    if (alpha_ == kAlphaMin) {
      // レベルを遷移する
      ChangeLevel(TaskId::kTitleLevel);
    }
    return false;
  }

  // 一定時間経過後、描画フェーズを「終了処理」へ遷移する
  if (accumulate_time_ >= kWaitTimeSecond) {
    // 現在の描画フェーズを「終了」に遷移
    ChangeDrawPhase(DrawPhaseType::kFinish);
    // 背景の現在のフェーズを「終了フェーズ」に変更
    // 背景の描画フェーズを「終了フェーズ」に変更
    back_screen_->ChangePhaseType(BackScreen::PhaseType::kFinalize);
    back_screen_->ChangeDrawPhase(BackScreen::DrawPhaseType::kFinish);
  }

  return false;
}

/// <summary>
/// 毎フレームの描画処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BootLevel::RenderPhase() {

  if (stone_handle_.at(kImageStoneInitIndex) == kZero) {
    return;
  }

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // マップ中央の位置を取得(マップ表示時の調整値)
  int x_center = game_info->GetCenterPointX();
  int y_center = game_info->GetCenterPointY();

  // 文字列の描画処理
  SetFontSize(kFontSize);
  SetFontThickness(kFontWidth);
  ChangeFont(kFontName);
  ChangeFontType(DX_FONTTYPE_ANTIALIASING);

  // 文字サイズ(幅)を取得
  int string_width = GetDrawStringWidth(kDisplayString, static_cast<int>(strlen(kDisplayString)));

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

  DrawRotaGraph(x_center + kStonePosX, y_center + kStonePosY - y_pos_, kIconMagnification, kIconAngle, stone_handle_.at(drawing_index_), true);
  // 文字列の描画処理
  DrawString(x_center - (string_width / kHalfValue) - kTextPosX, y_center, display_string_[string_index_].c_str(), kStringForeColor);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
}

/// <summary>
/// 終了処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool BootLevel::FinishPhase(float process_time) {

  switch (finalize_phase_) {
  case FinalizePhaseType::kUnloadTask: {
    //-------------------------------
    //タスクを降ろすフェーズ
    //-------------------------------
    // 画像のリソースを破棄
    if (back_screen_ != nullptr) {
      DeleteGraph(back_screen_->GetGraphicHandle());       // 背景
    }

    for (int i = 0; i < kImageStoneMaxNum; ++i) {
      DeleteGraph(stone_handle_[i]);                     // 石
    }

    // 背景をタスクマネージャから降ろす
    if (back_screen_ != nullptr) {
      TaskId task_id = back_screen_->GetTaskId();
      Task* release_task = task_manager_.ReleaseTask(task_id);
    }

    // 終了フェーズをタスク破棄フェーズに変更
    finalize_phase_ = FinalizePhaseType::kDisposeTask;
    return false;
  }
  case FinalizePhaseType::kDisposeTask: {
    //-------------------------------
    //タスクを破棄するフェーズ
    //-------------------------------
    if (back_screen_ != nullptr) {
      delete back_screen_;
      back_screen_ = nullptr;
    }
    break;
  }
  }

  return true;
}