﻿#include "Game/GameMode.h"

namespace {

  /// <summary>
  /// 連続スキップカウント リセット値
  /// </summary>
  const int kCountResetValue = 0;

  /// <summary>
  /// 連続スキップカウント ゲーム終了値
  /// </summary>
  const int kGameEnd = 2;

  /// <summary>
  /// 累積時間リセット
  /// </summary>
  const float kTimeResetValue = 0.0f;

  /// <summary>
  /// 石を置いてからの待機時間
  /// </summary>
  const float kPutStoneWaitTime = 0.7f;

  /// <summary>
  /// パネル表示開始時間
  /// </summary>
  const float kShowStart = 0.3f;

  /// <summary>
  /// パネル表示終了開始時間
  /// </summary>
  const float kHideStart = 1.2f;

  /// <summary>
  /// パネル表示時間
  /// </summary>
  const float kShowSkipMessage = 1.5f;

  /// <summary>
  /// 文字の色
  /// </summary>
  const int kStringForeColor = GetColor(255, 255, 255);     // 白色

  /// <summary>
  /// パネル拡大率
  /// </summary>
  const float kScalingFactor = 0.5f;

  /// <summary>
  /// パネル回転率
  /// </summary>
  const float kAngle = 0.0f;

  /// <summary>
  /// 透過率 起動時調整値
  /// </summary>
  const int kAlphaStartAdjast = 20;

  /// <summary>
  /// 透過率 終了時調整値
  /// </summary>
  const int kAlphaFinishAdjast = 15;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 200;

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 描画 水平方向初期値
  /// </summary>
  const int kInitialPosX = 30;

  /// <summary>
  /// 描画 水平方向移動量
  /// </summary>
  const int kMoveValue = 1;

  /// <summary>
  /// 描画 水平方向最小値
  /// </summary>
  const int kMoveMin = 0;

  /// <summary>
  /// 手動操作 手数
  /// </summary>
  const int kManualStep = 0;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> ゲームモードイベントインターフェース </param>
/// <returns></returns>
GameMode::GameMode(GameModeEventInterface& game_mode_event_interface)
  : Task(TaskId::kGameMode)
  , game_mode_event_interface_(game_mode_event_interface)
  , current_phase_(PhaseType::kInitialize)
  , current_turn_phase_(TurnPhase::kNone)
  , current_turn_(PlayerTurn::kPlayer)
  , player_data_()
  , skip_count_(0)
  , accumulate_time_(0.0f)
  , turn_handle_()
  , image_width_(0)
  , image_height_(0)
  , move_x_pos_(0)
  , alpha_(0)
  , auto_step_advance_(0)
  , current_step_(0) {

  // コンソールに出力
  std::cout << "GameMode コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
GameMode::~GameMode() {

  // コンソールに出力
  std::cout << "~GameMode デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void GameMode::Update(float process_time) {

  // 累積時間に加算
  accumulate_time_ += process_time;

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //---------------------------
    //初期設定フェーズ
    //---------------------------
    // 初期設定
    Initialize();
    // 現在のフェーズを「処理中」に変更する
    ChangePhaseType(PhaseType::kProcess);
    break;
  }
  case PhaseType::kProcess: {
    //---------------------------
    //処理中フェーズ
    //---------------------------
    bool check_finish = Process();
    if (check_finish) {
      // 現在のフェーズを「終了処理」に変更する
      ChangePhaseType(PhaseType::kFinish);
    }
    break;
  }
  case PhaseType::kFinish: {
    //---------------------------
    //終了処理フェーズ
    //---------------------------
    Finish();
    // 現在のフェーズを「終了処理済み」に変更する
    ChangePhaseType(PhaseType::kFinished);
    break;
  }
  }

  //---------------------------
  //描画前計算
  //---------------------------
    // 現在が「ターンパネル切り替え」かどうか確認し、異なる場合は処理終了
  bool is_display = IsTurnPhasekDisplayTurnPanel();
  if (!is_display) {
    // パネルの移動量初期化
    move_x_pos_ = kInitialPosX;
    return;
  }
   
  // ターンパネル透過率、表示位置計算処理
  if (accumulate_time_ < kShowStart) {
    // 透過率に調整値を加算
    alpha_ += kAlphaStartAdjast;
    if (alpha_ >= kAlphaMax) {
      alpha_ = kAlphaMax;
    }
    // 横方向座標に調整値減算
    move_x_pos_ -= kMoveValue;
    if (move_x_pos_ <= kMoveMin) {
      move_x_pos_ = kMoveMin;
    }
  }
  if (accumulate_time_ >= kHideStart) {
    // 透過率に調整値を減算
    alpha_ -= kAlphaFinishAdjast;
    if (alpha_ <= kAlphaMin) {
      alpha_ = kAlphaMin;
    }
  }


}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void GameMode::Render() {

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }

  // オートモード設定時は以降の処理は行わない
  bool is_manual = game_info->IsOperationModeMaual();
  if (!is_manual) {
    return;
  }

  // 現在のフェーズが「パネル切り替え」の場合
  // スキップカウントが「0」なら処理終了
  // スキップカウントが「1以上」ならスキップパネルセット
  if (skip_count_ == kCountResetValue) {
    return;
  }
  std::cout << "配置可能場所なし　スキップ" << std::endl;

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

  // マップ中央の位置を取得(マップ表示時の調整値)
  int x_center = game_info->GetCenterPointX();
  int y_center = game_info->GetCenterPointY();

  // 描画処理　※表示座標は画像の中心を表す
  DrawRotaGraph(x_center - move_x_pos_, y_center, kScalingFactor, kAngle, turn_handle_, true);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
}

/// <summary>
/// 初期設定フェーズ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void GameMode::Initialize() {

  // 自動で進める手数を設定
  auto_step_advance_ = Common::auto_step_advance;

  // 操作モード設定
  // 自動で進める手数が設定されていない場合は処理終了
  if (auto_step_advance_ == kManualStep) {
    return;
  }

  // ゲーム情報を取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  // 自動で進める手数が設定されている場合、モードをオートモードに設定
  game_info->SetOperationMode(GameInfo::OperationMode::kAuto);
}

/// <summary>
/// 処理中フェーズ
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
bool GameMode::Process() {

  // ゲーム情報を取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();

  // 石操作終了フェーズの場合、モードを切り替えるかどうか判断する
  switch (current_turn_phase_) {
  case TurnPhase::kFinishProcess: {
    // 現在の手数が自動で進める手数の場合、マニュアルモードに切り替え
    if (current_step_ == auto_step_advance_) {
      game_info->SetOperationMode(GameInfo::OperationMode::kManual);
    }
    break;
  }
  }

  // 操作モードをゲーム情報から取得する
  GameInfo::OperationMode orepation_mode = game_info->GetOperationMode();
  switch (orepation_mode) {
  case GameInfo::OperationMode::kManual: {
    //---------------------------
    //手動操作モード
    //---------------------------
    OperateManual();
    break;
  }
  case GameInfo::OperationMode::kAuto: {
    //---------------------------
    //自動操作モード
    //---------------------------
    OperateAuto();
    break;
  }
  }

  return false;
}

/// <summary>
/// 終了処理フェーズ
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void GameMode::Finish() {

}

/// <summary>
/// マニュアルモード処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void GameMode::OperateManual() {

  switch (current_turn_phase_) {
  case TurnPhase::kThink: {
    //-------------------------------
    //思考中フェーズ
    //-------------------------------
    break;
  }
  case TurnPhase::kPutStone: {
    //-------------------------------
    //石を置くフェーズ
    //-------------------------------
    // カーソルを非表示にする
    game_mode_event_interface_.OnHideCursor();
    // 現在のフェーズを「石を返す」に変更する
    ChangeTurnPhase(TurnPhase::kTurnOverStone);
    break;
  }
  case TurnPhase::kTurnOverStone: {
    //-------------------------------
    //石を返すフェーズ
    //-------------------------------
    // 石を返す演出が終了後、バトルレベル側からフェーズ変更
    // 累積時間をリセットする
    accumulate_time_ = kTimeResetValue;
    break;
  }
  case TurnPhase::kFinishProcess: {
    //-------------------------------
    //石操作終了フェーズ
    //-------------------------------
    if (accumulate_time_ >= kPutStoneWaitTime) {
      // フェーズを「ターン切り替え」に変更する
      ChangeTurnPhase(TurnPhase::kSwitchTurn);
    }
    break;
  }
  case TurnPhase::kSwitchTurn: {
    //-------------------------------
    //ターン切り替えフェーズ
    //-------------------------------
    // ターンのセット
    int turn = static_cast<int>(!static_cast<bool>(current_turn_));
    SetPlayerTurn(static_cast<PlayerTurn>(turn));
    // 手数のカウント
    ++current_step_;
    // 未配置の石があるかどうかチェック
    bool is_exist = game_mode_event_interface_.OnIsUnplaceStone();
    if (!is_exist) {
      ChangeTurnPhase(TurnPhase::kFinishGame);
      std::cout << "プレイヤー：" << static_cast<int>(player_data_[PlayerTurn::kPlayer]) << std::endl;
      std::cout << "CPU：" << static_cast<int>(player_data_[PlayerTurn::kCpu]) << std::endl;
      std::cout << "勝負あり！" << std::endl;
      break;
    }
    // 現在のフェーズを「配置可能箇所確認」に変更する
    ChangeTurnPhase(TurnPhase::kCheckPlaceablePlace);
    break;
  }
  case TurnPhase::kCheckPlaceablePlace: {
    //-------------------------------
    //配置可能箇所確認フェーズ
    //-------------------------------
    bool is_placeable = game_mode_event_interface_.OnCheckPlaceablePlace();
    // 配置可能箇所がない場合、スキップカウントを加算
    // 配置可能箇所がある場合、スキップカウントをリセット
    if (!is_placeable) {
      ++skip_count_;
    }
    else {
      skip_count_ = kCountResetValue;
    }
    // 連続スキップカウントが指定数を超えた場合、処理終了
    if (skip_count_ >= kGameEnd) {
      // 現在のフェーズを「ゲーム終了」に変更する
      ChangeTurnPhase(TurnPhase::kFinishGame);
      std::cout << "勝負あり！" << std::endl;
    }
    else {
      // 累積時間をリセット
      accumulate_time_ = kTimeResetValue;
      // 現在のフェーズを「ターン表示切り替え」に変更する
      ChangeTurnPhase(TurnPhase::kDisplayTurnPanel);
    }
    break;
  }
  case TurnPhase::kDisplayTurnPanel: {
    //-------------------------------
    //画面上のターンパネル切り替えフェーズ
    //-------------------------------
    // スキップターンなら一定時間待機(スキップメッセージ表示)後、現在のフェーズを「石操作終了」に変更する

    if (skip_count_ != kCountResetValue) {
      if (accumulate_time_ >= kShowSkipMessage) {
        // 指定時間待機する
        ChangeTurnPhase(TurnPhase::kFinishProcess);
      }
      break;
    }
    // 現在のフェーズを「各種描画処理」に変更する
    ChangeTurnPhase(TurnPhase::kShowDisplay);
    break;
  }
  case TurnPhase::kShowDisplay: {
    //-------------------------------
    //思考中前に各設定を調整
    //-------------------------------
    // カーソルを表示する
    // ※CPUのターンならCPUのフェーズを変更する
    game_mode_event_interface_.OnShowCursor();
    // 現在のフェーズを「思考中」に変更する
    ChangeTurnPhase(TurnPhase::kThink);
    break;
  }
  case TurnPhase::kFinishGame: {
    //-------------------------------
    //ゲーム終了フェーズ
    //-------------------------------
    // バトルレベルへゲーム終了のイベント通知
    game_mode_event_interface_.OnFinishGame();
    // 現在のフェーズを「何もしない」に変更する
    ChangeTurnPhase(TurnPhase::kNone);
    break;
  }
  }
}

/// <summary>
/// オートモード処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void GameMode::OperateAuto() {

  switch (current_turn_phase_) {
  case TurnPhase::kThink: {
    //-------------------------------
    //思考中フェーズ
    //-------------------------------
    break;
  }
  case TurnPhase::kPutStone: {
    //-------------------------------
    //石を置くフェーズ
    //-------------------------------
    // 累積時間をリセットする
    accumulate_time_ = kTimeResetValue;

    // カーソルを非表示にする
    game_mode_event_interface_.OnHideCursor();
    // 現在のフェーズを「石を返す」に変更する
    ChangeTurnPhase(TurnPhase::kTurnOverStone);
    break;
  }
  case TurnPhase::kTurnOverStone: {
    //-------------------------------
    //石を返すフェーズ
    //-------------------------------
    // 石を返す演出が終了後、バトルレベル側からフェーズ変更
    break;
  }
  case TurnPhase::kFinishProcess: {
    //-------------------------------
    //石操作終了フェーズ
    //-------------------------------
    // フェーズを「ターン切り替え」に変更する
    ChangeTurnPhase(TurnPhase::kSwitchTurn);
    break;
  }
  case TurnPhase::kSwitchTurn: {
    //-------------------------------
    //ターン切り替えフェーズ
    //-------------------------------
    // ターンのセット
    int turn = static_cast<int>(!static_cast<bool>(current_turn_));
    SetPlayerTurn(static_cast<PlayerTurn>(turn));
    // 手数のカウント
    ++current_step_;

    // 未配置の石があるかどうかチェック
    bool is_exist = game_mode_event_interface_.OnIsUnplaceStone();
    if (!is_exist) {
      ChangeTurnPhase(TurnPhase::kFinishGame);
      std::cout << "プレイヤー：" << static_cast<int>(player_data_[PlayerTurn::kPlayer]) << std::endl;
      std::cout << "CPU：" << static_cast<int>(player_data_[PlayerTurn::kCpu]) << std::endl;
      std::cout << "勝負あり！" << std::endl;
      break;
    }
    // 現在のフェーズを「配置可能箇所確認」に変更する
    ChangeTurnPhase(TurnPhase::kCheckPlaceablePlace);
    break;
  }
  case TurnPhase::kCheckPlaceablePlace: {
    //-------------------------------
    //配置可能箇所確認フェーズ
    //-------------------------------
    bool is_placeable = game_mode_event_interface_.OnCheckPlaceablePlace();
    // 配置可能箇所がない場合、スキップカウントを加算
    // 配置可能箇所がある場合、スキップカウントをリセット
    if (!is_placeable) {
      ++skip_count_;
    }
    else {
      skip_count_ = kCountResetValue;
    }
    // 連続スキップカウントが指定数を超えた場合、処理終了
    if (skip_count_ >= kGameEnd) {
      // 現在のフェーズを「ゲーム終了」に変更する
      ChangeTurnPhase(TurnPhase::kFinishGame);
      std::cout << "プレイヤー：" << static_cast<int>(player_data_[PlayerTurn::kPlayer]) << std::endl;
      std::cout << "CPU：" << static_cast<int>(player_data_[PlayerTurn::kCpu]) << std::endl;
      std::cout << "勝負あり！" << std::endl;
    }
    else {
      // 累積時間をリセット
      accumulate_time_ = kTimeResetValue;
      // 現在のフェーズを「ターン表示切り替え」に変更する
      ChangeTurnPhase(TurnPhase::kDisplayTurnPanel);
    }
    break;
  }
  case TurnPhase::kDisplayTurnPanel: {
    //-------------------------------
    //画面上のターンパネル切り替えフェーズ
    //-------------------------------
    // スキップターンなら現在のフェーズを「石操作終了」に変更する
    if (skip_count_ != kCountResetValue) {
      ChangeTurnPhase(TurnPhase::kFinishProcess);
      break;
    }
    // 現在のフェーズを「各種描画処理」に変更する
    ChangeTurnPhase(TurnPhase::kShowDisplay);
    break;
  }
  case TurnPhase::kShowDisplay: {
    //-------------------------------
    //思考中前に各設定を調整
    //-------------------------------
    // カーソルを表示する
    // ※CPUのターンならCPUのフェーズを変更する
    game_mode_event_interface_.OnShowCursor();
    // 現在のフェーズを「思考中」に変更する
    ChangeTurnPhase(TurnPhase::kThink);
    break;
  }
  case TurnPhase::kFinishGame: {
    //-------------------------------
    //ゲーム終了フェーズ
    //-------------------------------
    // バトルレベルへゲーム終了のイベント通知
    game_mode_event_interface_.OnFinishGame();
    // 現在のフェーズを「何もしない」に変更する
    ChangeTurnPhase(TurnPhase::kNone);
    break;
  }
  }

}

/// <summary>
/// 現在のフェーズを変更する
/// </summary>
/// <param name="phase_type"> フェーズの種類 </param>
/// <returns></returns>
void GameMode::ChangePhaseType(PhaseType phase_type) {

  current_phase_ = phase_type;
}

/// <summary>
/// 現在のターンフェーズを変更する
/// </summary>
/// <param name=""> ターンフェーズの種類 </param>
/// <returns></returns>
void GameMode::ChangeTurnPhase(TurnPhase turn_phase) {

  current_turn_phase_ = turn_phase;
}

/// <summary>
/// プレイヤー情報の設定
/// </summary>
/// <param name="first">  </param>
/// <returns></returns>
void GameMode::SetPlayerData(PlayerTurn player_turn, Stone::StoneType stone_type) {

  player_data_[player_turn] = stone_type;
}

/// <summary>
/// ターンの設定
/// </summary>
/// <param name="player_turn"> プレイヤーの種類 </param>
/// <returns></returns>
void GameMode::SetPlayerTurn(PlayerTurn player_turn) {

  current_turn_ = player_turn;
}

/// <summary>
/// 画像のサイズを取得する
/// </summary>
/// <param name="graphic_handle"> グラフィックハンドル </param>
/// <returns></returns>
void GameMode::SetImageSize(int graphic_handle) {

  // グラフィックハンドルから画像のサイズを取得
  GetGraphSize(graphic_handle, &image_width_, &image_height_);
}