﻿#pragma once

#include "System/Cursor.h"
#include "Game/GameInfo.h"
#include <array>

/// <summary>
/// 石配置可能場所表示カーソル
/// </summary>
class PlaceableCursor : public Cursor {
public:

  /// <summary>
  /// プレイヤーカーソルのサイズ種類
  /// </summary>
  enum class PatternType {
    kPattern1,        // カーソルパターン①
    kPattern2,        // カーソルパターン②
    kPatternMaxIndex  // カーソルサイズの種類数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> カーソルID </param>
  /// <returns></returns>
  PlaceableCursor(int);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~PlaceableCursor();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float);

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render();

  /// <summary>
  /// グラフィックハンドルをセット
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandle(std::array<int, static_cast<int>(PatternType::kPatternMaxIndex)> cursor_handle) { cursor_handle_ = cursor_handle; }

  /// <summary>
  /// グラフィックハンドルを取得する
  /// </summary>
  /// <param name=""> 配列のインデックス </param>
  /// <returns></returns>
  int GetGraphicHandleCursor(int index) { return cursor_handle_.at(index); }


private:

  /// <summary>
  /// グラフィックハンドル
  /// </summary>
  std::array<int,static_cast<int>(PatternType::kPatternMaxIndex)> cursor_handle_;

  /// <summary>
  /// グラフィックの種類
  /// </summary>
  PatternType pattern_type_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time;
};