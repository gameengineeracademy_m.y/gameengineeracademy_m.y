﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "Game/GameInfo.h"

class TurnSelectMenu : public Task {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  TurnSelectMenu();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~TurnSelectMenu();

  /// <summary>
  /// 毎フレーム更新処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 毎フレーム描画処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// マウスカーソルがボタン上に存在するか確認
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <param name=""> 押されているかどうか </param>
  /// <returns> true:存在する, false:存在しない </returns>
  bool CheckMouseCursorPosition(int, int, bool);

  /// <summary>
  /// マウスカーソルが存在するボタンを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  GameInfo::TurnType GetExistsCursorButton() { return exists_button_; }

  /// <summary>
  /// フラグを立てる
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetButtonClick() { is_button_click_ = true; }

  /// <summary>
  /// フラグを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:クリック済み, false:クリックされていない </returns>
  bool IsButtonClick() { return is_button_click_; }


private:

  /// <summary>
  /// カーソルがあるボタン
  /// </summary>
  GameInfo::TurnType exists_button_;

  /// <summary>
  /// メニューUI座標
  /// </summary>
  int x_pos_menu_left_;

  /// <summary>
  /// メニューUI座標
  /// </summary>
  int y_pos_menu_left_;

  /// <summary>
  /// メニューUI座標
  /// </summary>
  int x_pos_menu_right_;

  /// <summary>
  /// メニューUI座標
  /// </summary>
  int y_pos_menu_right_;

  /// <summary>
  /// 先攻ボタンUI座標
  /// </summary>
  int x_pos_first_attack_button_left_;

  /// <summary>
  /// 先攻ボタンUI座標
  /// </summary>
  int y_pos_first_attack_button_left_;

  /// <summary>
  /// 先攻ボタンUI座標
  /// </summary>
  int x_pos_first_attack_button_right_;

  /// <summary>
  /// 先攻ボタンUI座標
  /// </summary>
  int y_pos_first_attack_button_right_;

  /// <summary>
  /// 後攻ボタンUI座標
  /// </summary>
  int x_pos_second_attack_button_left_;

  /// <summary>
  /// 後攻ボタンUI座標
  /// </summary>
  int y_pos_second_attack_button_left_;

  /// <summary>
  /// 後攻ボタンUI座標
  /// </summary>
  int x_pos_second_attack_button_right_;

  /// <summary>
  /// 後攻ボタンUI座標
  /// </summary>
  int y_pos_second_attack_button_right_;

  /// <summary>
  /// ランダムボタンUI座標
  /// </summary>
  int x_pos_random_button_left_;

  /// <summary>
  /// ランダムボタンUI座標
  /// </summary>
  int y_pos_random_button_left_;

  /// <summary>
  /// ランダムボタンUI座標
  /// </summary>
  int x_pos_random_button_right_;

  /// <summary>
  /// ランダムボタンUI座標
  /// </summary>
  int y_pos_random_button_right_;

  /// <summary>
  /// 先攻ボタンUI 影 座標
  /// </summary>
  int x_pos_first_attack_shadow_left_;

  /// <summary>
  /// 先攻ボタンUI 影 座標
  /// </summary>
  int y_pos_first_attack_shadow_left_;

  /// <summary>
  /// 先攻ボタンUI 影 座標
  /// </summary>
  int x_pos_first_attack_shadow_right_;

  /// <summary>
  /// 先攻ボタンUI 影 座標
  /// </summary>
  int y_pos_first_attack_shadow_right_;

  /// <summary>
  /// 後攻ボタンUI 影 座標
  /// </summary>
  int x_pos_second_attack_shadow_left_;

  /// <summary>
  /// 後攻ボタンUI 影 座標
  /// </summary>
  int y_pos_second_attack_shadow_left_;

  /// <summary>
  /// 後攻ボタンUI 影 座標
  /// </summary>
  int x_pos_second_attack_shadow_right_;

  /// <summary>
  /// 後攻ボタンUI 影 座標
  /// </summary>
  int y_pos_second_attack_shadow_right_;

  /// <summary>
  /// ランダムボタンUI 影 座標
  /// </summary>
  int x_pos_random_shadow_left_;

  /// <summary>
  /// ランダムボタンUI 影 座標
  /// </summary>
  int y_pos_random_shadow_left_;

  /// <summary>
  /// ランダムボタンUI 影 座標
  /// </summary>
  int x_pos_random_shadow_right_;

  /// <summary>
  /// ランダムボタンUI 影 座標
  /// </summary>
  int y_pos_random_shadow_right_;

  /// <summary>
  /// 先攻ボタン 押下時の調整値
  /// </summary>
  int x_pos_first_attack_button_adjust_;

  /// <summary>
  /// 先攻ボタン 押下時の調整値
  /// </summary>
  int y_pos_first_attack_button_adjust_;

  /// <summary>
  /// 後攻ボタン 押下時の調整値
  /// </summary>
  int x_pos_second_attack_button_adjust_;

  /// <summary>
  /// 後攻ボタン 押下時の調整値
  /// </summary>
  int y_pos_second_attack_button_adjust_;

  /// <summary>
  /// ランダムボタン 押下時の調整値
  /// </summary>
  int x_pos_random_button_adjust_;

  /// <summary>
  /// ランダムボタン 押下時の調整値
  /// </summary>
  int y_pos_random_button_adjust_;

  /// <summary>
  /// メニューテキスト表示座標
  /// </summary>
  int x_pos_menu_text_;

  /// <summary>
  /// メニューテキスト表示座標
  /// </summary>
  int y_pos_menu_text_;

  /// <summary>
  /// 先攻ボタンテキスト表示座標
  /// </summary>
  int x_pos_first_attack_button_text_;

  /// <summary>
  /// 先攻ボタンテキスト表示座標
  /// </summary>
  int y_pos_first_attack_button_text_;

  /// <summary>
  /// 後攻ボタンテキスト表示座標
  /// </summary>
  int x_pos_second_attack_button_text_;

  /// <summary>
  /// 後攻ボタンテキスト表示座標
  /// </summary>
  int y_pos_second_attack_button_text_;

  /// <summary>
  /// ランダムボタンテキスト表示座標
  /// </summary>
  int x_pos_random_button_text_;

  /// <summary>
  /// ランダムボタンテキスト表示座標
  /// </summary>
  int y_pos_random_button_text_;

  /// <summary>
  /// メニューの色
  /// </summary>
  int menu_color_;

  /// <summary>
  /// 先攻ボタンの色
  /// </summary>
  int first_attack_button_color_;

  /// <summary>
  /// 後攻ボタンの色
  /// </summary>
  int second_attack_button_color_;

  /// <summary>
  /// ランダムボタンの色
  /// </summary>
  int random_button_color_;

  /// <summary>
  /// 描画 テキスト透過率
  /// </summary>
  int alpha_text_;

  /// <summary>
  /// 描画 メニュー透過率
  /// </summary>
  int alpha_menu_;

  /// <summary>
  /// 描画 ボタン透過率
  /// </summary>
  int alpha_button_;

  /// <summary>
  /// ボタンクリックフラグ
  /// </summary>
  bool is_button_click_;
};