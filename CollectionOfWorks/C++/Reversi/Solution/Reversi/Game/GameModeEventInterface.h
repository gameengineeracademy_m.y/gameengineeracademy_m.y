﻿#pragma once

#include <iostream>

/// <summary>
/// ゲームモードイベントインターフェース
/// </summary>
class GameModeEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  GameModeEventInterface() {

    // コンソールに出力
    std::cout << "GameModeEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~GameModeEventInterface() {

    // コンソールに出力
    std::cout << "~GameModeEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// 未配置の石があるかどうかの確認
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:未配置の石がある, false:未配置の石はない </returns>
  virtual bool OnIsUnplaceStone() = 0;

  /// <summary>
  /// カーソルの非表示
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnHideCursor() = 0;

  /// <summary>
  /// カーソルの表示
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnShowCursor() = 0;

  /// <summary>
  /// 配置可能場所の確認
  /// </summary>
  /// <param name=""> ゲームモード </param>
  /// <returns> true:配置可能場所あり, false:配置可能場所なし </returns>
  virtual bool OnCheckPlaceablePlace() = 0;

  /// <summary>
  /// ゲーム終了
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnFinishGame() = 0;


private:


};