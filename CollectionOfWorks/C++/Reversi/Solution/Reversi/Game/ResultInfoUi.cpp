﻿#include "Game/ResultInfoUi.h"

namespace {

  /// <summary>
  /// 表示位置 横方向 調整値
  /// </summary>
  const int kDispPosX = 400;

  /// <summary>
  /// プレイヤー情報 表示位置 縦方向 調整値
  /// </summary>
  const int kPlayerDispPosY = 100;

  /// <summary>
  /// 石 表示位置 縦方向 調整値
  /// </summary>
  const int kStoneDispPosY = 162;

  /// <summary>
  /// プレイヤーテキスト
  /// </summary>
  const char* kPlayerText = "YOU";

  /// <summary>
  /// CPUテキスト
  /// </summary>
  const char* kCpuText = "CPU";

  /// <summary>
  /// 文字の色 黒色
  /// </summary>
  const int kTextForeColorBlack = GetColor(0, 0, 0);   // 黒色

  /// <summary>
  /// 数字 表示位置 縦方向 調整値
  /// </summary>
  const int kNumberDispPosY = 25;

  /// <summary>
  /// リザルト 表示位置 縦方向 調整値
  /// </summary>
  const int kResultDispPosY = 113;

  /// <summary>
  /// 石 表示倍率
  /// </summary>
  const float kStoneDispMagnification = 0.3f;

  /// <summary>
  /// リザルト 表示倍率 最小
  /// </summary>
  const float kResultDispMinMagnification = 0.3f;

  /// <summary>
  /// リザルト 表示倍率 最大
  /// </summary>
  const float kResultDispMaxMagnification = 0.35f;

  /// <summary>
  /// リザルト 表示倍率 変化量
  /// </summary>
  const float kResultMagnificationRate = 0.001f;

  /// <summary>
  /// 石 表示角度
  /// </summary>
  const float kStoneDispAngle = 0.0f;

  /// <summary>
  /// リザルト 表示角度 最小値
  /// </summary>
  const float kResultDispMinAngle = 0.0f;

  /// <summary>
  /// リザルト 表示角度 最大値
  /// </summary>
  const float kResultDispMaxAngle = 0.1f;

  /// <summary>
  /// リザルト 表示角度 変化量
  /// </summary>
  const float kResultAngleRate = 0.01f;

  /// <summary>
  /// 符号の切り替え
  /// </summary>
  const int kSwitchSign = -1;

  /// <summary>
  /// 累積時間 リセット値
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 待機時間 角度表示 水平時点
  /// </summary>
  const float kHorizonWaitTime = 0.5f;

  /// <summary>
  /// 待機時間 角度表示 傾いた後
  /// </summary>
  const float kTiltedWaitTime = 1.5f;

  /// <summary>
  /// レベル遷移ボタンUI 幅
  /// </summary>
  const int kButtonUiWidth = 200;

  /// <summary>
  /// レベル遷移ボタンUI 高さ
  /// </summary>
  const int kButtonUiHeight = 60;

  /// <summary>
  /// レベル遷移ボタンUI 表示位置調整 水平
  /// </summary>
  const int kButtonUiAdjustHorizon = 125;

  /// <summary>
  /// レベル遷移ボタンUI 表示位置調整 垂直
  /// </summary>
  const int kButtonUiAdjustVirtical = 340;

  /// <summary>
  /// 図形の色
  /// </summary>
  const int kShapeColorGray = GetColor(125, 125, 125);   // 灰色

  /// <summary>
  /// 図形の色
  /// </summary>
  const int kShapeColorMiddleGray = GetColor(150, 150, 150);   // 灰色

  /// <summary>
  /// 図形の色
  /// </summary>
  const int kShapeColorWhiteGray = GetColor(190, 190, 190);   // 灰色

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// ボタンテキスト
  /// </summary>
  const char* kTitleButtonText = "Go To Title";

  /// <summary>
  /// ボタンテキスト
  /// </summary>
  const char* kBattleButtonText = "Retry";

  /// <summary>
  /// レベル遷移ボタンテキスト 表示位置調整 垂直
  /// </summary>
  const int kButtonTextAdjustVirtical = 15;

  /// <summary>
  /// 文字の色
  /// </summary>
  const int kTextForeColorWhite = GetColor(255, 255, 255);   // 白色

  /// <summary>
  /// 透過率 起動時調整値
  /// </summary>
  const int kAlphaAdjastStart = 30;

  /// <summary>
  /// 透過率 終了時調整値
  /// </summary>
  const int kAlphaAdjastFinish = 5;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 図形の透過度
  /// </summary>
  const int kShapeAlpha = 250;

  /// <summary>
  /// 押されてない時のボタン位置調整値
  /// </summary>
  const int kInitialPosAdjust = 0;

  /// <summary>
  /// 押されている時のボタン位置調整値
  /// </summary>
  const int kPushPosAdjust = 3;

  /// <summary>
  /// ボタンの影 位置調整値
  /// </summary>
  const int kShadowPosAdjust = 6;

  /// <summary>
  /// 影の色
  /// </summary>
  const int kShadowColor = GetColor(0, 0, 0);   // 黒色

  /// <summary>
  /// 影の透過度
  /// </summary>
  const int kShadowAlpha = 150;

  /// <summary>
  /// 遷移説明フォントサイズ
  /// </summary>
  const int kFontSizeExp = 50;

  /// <summary>
  /// 遷移説明文字幅
  /// </summary>
  const int kFontWidthExp = 10;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSize = 30;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 8;

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Meiryo UI";
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param></param>
/// <returns></returns>
ResultInfoUi::ResultInfoUi()
  : Task(TaskId::kResultInfoUi)
  , current_draw_phase_(DrawPhaseType::kNone)
  , winner_handle_(0)
  , lose_handle_(0)
  , draw_handle_(0)
  , stone_black_handle_(0)
  , stone_white_handle_(0)
  , magnification_player_(kResultDispMinMagnification)
  , magnification_cpu_(kResultDispMinMagnification)
  , magnification_rate_(kResultMagnificationRate)
  , angle_player_(kResultDispMinAngle)
  , angle_cpu_(kResultDispMinAngle)
  , angle_rate_(kResultAngleRate)
  , accumulate_time_(0.0f)
  , x_pos_player_data_(0)
  , x_pos_cpu_data_(0)
  , x_pos_black_count_(0)
  , x_pos_white_count_(0)
  , x_pos_black_(0)
  , x_pos_white_(0)
  , result_player_(0)
  , result_cpu_(0)
  , x_pos_title_button_left_(0)
  , y_pos_title_button_left_(0)
  , x_pos_title_button_right_(0)
  , y_pos_title_button_right_(0)
  , x_pos_battle_button_left_(0)
  , y_pos_battle_button_left_(0)
  , x_pos_battle_button_right_(0)
  , y_pos_battle_button_right_(0)
  , x_pos_title_button_adjust_(0)
  , y_pos_title_button_adjust_(0)
  , x_pos_battle_button_adjust_(0)
  , y_pos_battle_button_adjust_(0)
  , x_pos_title_button_text_(0)
  , y_pos_title_button_text_(0)
  , x_pos_battle_button_text_(0)
  , y_pos_battle_button_text_(0)
  , title_button_color_(kShapeColorGray)
  , battle_button_color_(kShapeColorGray)
  , exists_button_(ButtonType::kNone)
  , alpha_(kAlphaMax)
  , alpha_shape_(kShapeAlpha) {

  // コンソールに出力
  std::cout << "ResultInfoUi コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param></param>
/// <returns></returns>
ResultInfoUi::~ResultInfoUi() {

  // コンソールに出力
  std::cout << "~ResultInfoUi デストラクタ" << std::endl;
}

/// <summary>
/// 毎フレーム更新処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void ResultInfoUi::Update(float process_time) {

  accumulate_time_ += process_time;

  if (current_draw_phase_ == DrawPhaseType::kNone) {
    return;
  }

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // 画面の中心座標を取得
  int x_pos_center = game_info->GetCenterPointX();
  int y_pos_center = game_info->GetCenterPointY();

  // プレイヤーが使用していた石の種類を取得
  GameInfo::StoneType stone_type = game_info->GetGameData(GameInfo::PlayerType::kPlayer);
  // 石の描画位置
  // プレイヤーが使用していた石を画面左側へ設定する
  switch (stone_type) {
  case GameInfo::StoneType::kBlack: {
    x_pos_black_ = x_pos_center - kDispPosX;
    x_pos_white_ = x_pos_center + kDispPosX;
    break;
  }
  case GameInfo::StoneType::kWhite: {
    x_pos_black_ = x_pos_center + kDispPosX;
    x_pos_white_ = x_pos_center - kDispPosX;
    break;
  }
  }

  //-----------------------------------
  // プレイヤー情報の描画準備
  //-----------------------------------
  // 文字列の描画処理
  SetFontSize(kFontSizeExp);
  SetFontThickness(kFontWidthExp);
  ChangeFont(kFontName);
  ChangeFontType(DX_FONTTYPE_ANTIALIASING);
  // 文字サイズ(幅)を取得
  int string_width = GetDrawStringWidth(kPlayerText, static_cast<int>(strlen(kPlayerText)));
  x_pos_player_data_ = x_pos_center - kDispPosX - (string_width / 2);
  string_width = GetDrawStringWidth(kCpuText, static_cast<int>(strlen(kCpuText)));
  x_pos_cpu_data_ = x_pos_center + kDispPosX - (string_width / 2);

  // ゲームの結果を取得する
  GameInfo::BattleResult battle_level = game_info->GetBattleResult();

  //-----------------------------------
  // リザルト情報の描画準備
  //-----------------------------------
  switch (battle_level) {
  case GameInfo::BattleResult::kWin: {
    // グラフィックハンドルの設定
    result_player_ = GetGraphicHandleWinner();
    result_cpu_ = GetGraphicHandleLose();

    // 表示倍率の設定
    if (magnification_player_ <= kResultDispMinMagnification) {
      magnification_rate_ = kResultMagnificationRate;
    }
    else if (magnification_player_ >= kResultDispMaxMagnification) {
      magnification_rate_ = kSwitchSign * kResultMagnificationRate;
    }
    magnification_player_ += magnification_rate_;

    // 表示角度の設定
    if (angle_cpu_ == kResultDispMinAngle) {
      if (accumulate_time_ >= kHorizonWaitTime) {
        // 累積時間をリセットする
        accumulate_time_ = kResetTime;
        // 角度の変化量をセット
        angle_rate_ = kResultAngleRate;
      }
    }
    else if (angle_cpu_ >= kResultDispMaxAngle) {
      angle_rate_ = kResultDispMinAngle;

      if (accumulate_time_ >= kTiltedWaitTime) {
        // 累積時間をリセットする
        accumulate_time_ = kResetTime;
        // 角度を初期値に戻す
        angle_cpu_ = kResultDispMinAngle;
      }
    }
    angle_cpu_ += angle_rate_;

    break;
  }
  case GameInfo::BattleResult::kLose: {
    // グラフィックハンドルの設定
    result_player_ = GetGraphicHandleLose();
    result_cpu_ = GetGraphicHandleWinner();

    // 表示倍率の設定
    if (magnification_cpu_ <= kResultDispMinMagnification) {
      magnification_rate_ = kResultMagnificationRate;
    }
    else if (magnification_cpu_ >= kResultDispMaxMagnification) {
      magnification_rate_ = kSwitchSign * kResultMagnificationRate;
    }
    magnification_cpu_ += magnification_rate_;

    // 表示角度の設定
    if (angle_player_ == kResultDispMinAngle) {
      if (accumulate_time_ >= kHorizonWaitTime) {
        // 累積時間をリセットする
        accumulate_time_ = kResetTime;
        // 角度の変化量をセット
        angle_rate_ = kResultAngleRate;
      }
    }
    else if (angle_player_ >= kResultDispMaxAngle) {
      angle_rate_ = kResultDispMinAngle;

      if (accumulate_time_ >= kTiltedWaitTime) {
        // 累積時間をリセットする
        accumulate_time_ = kResetTime;
        // 角度を初期値に戻す
        angle_player_ = kResultDispMinAngle;
      }
    }
    angle_player_ += angle_rate_;

    break;
  }
  case GameInfo::BattleResult::kDraw: {
    // グラフィックハンドルの設定
    result_player_ = GetGraphicHandleDraw();
    result_cpu_ = GetGraphicHandleDraw();
    break;
  }
  }

  //// 以降の処理は「遷移レベル選択」フェーズ以外は行わない
  //if (current_draw_phase_ != DrawPhaseType::kSelectTransitionLevel) {
  //  return;
  //}
  //-----------------------------------
  // タイトルレベル遷移ボタンの描画準備
  //-----------------------------------
  x_pos_title_button_left_ = x_pos_center + kButtonUiAdjustHorizon - (kButtonUiWidth / kHalfValue);
  y_pos_title_button_left_ = y_pos_center + kButtonUiAdjustVirtical - (kButtonUiHeight / kHalfValue);
  x_pos_title_button_right_ = x_pos_title_button_left_ + kButtonUiWidth;
  y_pos_title_button_right_ = y_pos_title_button_left_ + kButtonUiHeight;

  //-----------------------------------
  // バトルレベル遷移ボタンの描画準備
  //-----------------------------------
  x_pos_battle_button_left_ = x_pos_center - kButtonUiAdjustHorizon - (kButtonUiWidth / kHalfValue);
  y_pos_battle_button_left_ = y_pos_center + kButtonUiAdjustVirtical - (kButtonUiHeight / kHalfValue);;
  x_pos_battle_button_right_ = x_pos_battle_button_left_ + kButtonUiWidth;
  y_pos_battle_button_right_ = y_pos_battle_button_left_ + kButtonUiHeight;

  // 文字列の描画処理
  SetFontSize(kFontSize);
  SetFontThickness(kFontWidth);
  ChangeFont(kFontName);
  ChangeFontType(DX_FONTTYPE_ANTIALIASING);
  //-----------------------------------
  // タイトル遷移ボタンテキストの描画準備
  //-----------------------------------
  int text_width = GetDrawStringWidth(kTitleButtonText, static_cast<int>(strlen(kTitleButtonText)));
  x_pos_title_button_text_ = (x_pos_title_button_left_ + (kButtonUiWidth / kHalfValue)) - (text_width / kHalfValue);
  y_pos_title_button_text_ = (y_pos_title_button_left_ + (kButtonUiHeight / kHalfValue) - kButtonTextAdjustVirtical);

  //-----------------------------------
  // バトル遷移ボタンテキストの描画準備
  //-----------------------------------
  text_width = GetDrawStringWidth(kBattleButtonText, static_cast<int>(strlen(kBattleButtonText)));
  x_pos_battle_button_text_ = (x_pos_battle_button_left_ + (kButtonUiWidth / kHalfValue)) - (text_width / kHalfValue);
  y_pos_battle_button_text_ = (y_pos_battle_button_left_ + (kButtonUiHeight / kHalfValue) - kButtonTextAdjustVirtical);

  // 以降の処理は「終了」フェーズ以外は行わない
  if (current_draw_phase_ != DrawPhaseType::kFinish) {
    return;
  }

  // 透過率に調整値を減算
  alpha_ -= kAlphaAdjastFinish;
  alpha_shape_ -= kAlphaAdjastFinish;
  if (alpha_ <= kAlphaMin) {
    alpha_ = kAlphaMin;
  }
  if (alpha_shape_ <= kAlphaMin) {
    alpha_shape_ = kAlphaMin;
  }

}

/// <summary>
/// 毎フレーム描画処理
/// </summary>
/// <param></param>
/// <returns></returns>
void ResultInfoUi::Render() {

  // リザルト描画フェーズ、遷移レベル選択フェーズ以外なら処理終了
  if (current_draw_phase_ != DrawPhaseType::kResultDrawing &&
      current_draw_phase_ != DrawPhaseType::kSelectTransitionLevel) {
    return;
  }

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // 画面の中心座標を取得
  int x_pos_center = game_info->GetCenterPointX();
  int y_pos_center = game_info->GetCenterPointY();

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_shape_);

  // 文字列の描画処理
  SetFontSize(50);
  SetFontThickness(10);
  ChangeFont("Meiryo UI");
  ChangeFontType(DX_FONTTYPE_ANTIALIASING);

  //-----------------------------------
  // プレイヤー情報の描画
  //-----------------------------------
  DrawString(x_pos_player_data_, y_pos_center - kPlayerDispPosY, kPlayerText, kTextForeColorBlack);
  DrawString(x_pos_cpu_data_, y_pos_center - kPlayerDispPosY, kCpuText, kTextForeColorBlack);

  //-----------------------------------
  // 石の描画
  //-----------------------------------
  DrawRotaGraph(x_pos_black_, y_pos_center - kStoneDispPosY, kStoneDispMagnification,
                kStoneDispAngle, stone_black_handle_, true);
  DrawRotaGraph(x_pos_white_, y_pos_center - kStoneDispPosY, kStoneDispMagnification,
                kStoneDispAngle, stone_white_handle_, true);

  //-----------------------------------
  // 数字情報の描画
  //-----------------------------------
  // 黒色の石の数を桁数ごとに取得する
  int stone_count = game_info->GetBlackStoneCount();
  std::string stone_conut_text = std::to_string(stone_count);
  // 文字サイズ(幅)を取得
  int string_width = GetDrawStringWidth(stone_conut_text.c_str(), static_cast<int>(strlen(stone_conut_text.c_str())));
  DrawString(x_pos_black_ - (string_width / kHalfValue), y_pos_center - kNumberDispPosY, stone_conut_text.c_str(), kTextForeColorBlack);
  
  // 白色の石の数を桁数ごとに取得する
  stone_count = game_info->GetWhiteStoneCount();
  stone_conut_text = std::to_string(stone_count);
  // 文字サイズ(幅)を取得
  string_width = GetDrawStringWidth(stone_conut_text.c_str(), static_cast<int>(strlen(stone_conut_text.c_str())));
  DrawString(x_pos_white_ - (string_width / kHalfValue), y_pos_center - kNumberDispPosY, stone_conut_text.c_str(), kTextForeColorBlack);

  //-----------------------------------
  // リザルト情報の描画
  //-----------------------------------
  DrawRotaGraph(x_pos_center - kDispPosX, y_pos_center + kResultDispPosY, magnification_player_,
                angle_player_, result_player_, true);
  DrawRotaGraph(x_pos_center + kDispPosX, y_pos_center + kResultDispPosY, magnification_cpu_,
                angle_cpu_, result_cpu_, true);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, kShadowAlpha);

  //-----------------------------------
  // ボタンの影
  //-----------------------------------
  DrawBox(x_pos_battle_button_left_ + kShadowPosAdjust, y_pos_battle_button_left_ + kShadowPosAdjust, x_pos_battle_button_right_ + kShadowPosAdjust, y_pos_battle_button_right_ + kShadowPosAdjust, kShadowColor, true);
  DrawBox(x_pos_title_button_left_ + kShadowPosAdjust, y_pos_title_button_left_ + kShadowPosAdjust, x_pos_title_button_right_ + kShadowPosAdjust, y_pos_title_button_right_ + kShadowPosAdjust, kShadowColor, true);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_shape_);

  SetFontSize(kFontSize);
  SetFontThickness(kFontWidth);
  ChangeFont(kFontName);
  ChangeFontType(DX_FONTTYPE_ANTIALIASING);

  //-----------------------------------
  // バトルレベル遷移ボタン
  //-----------------------------------
  DrawBox(x_pos_battle_button_left_ + x_pos_battle_button_adjust_, y_pos_battle_button_left_ + y_pos_battle_button_adjust_, x_pos_battle_button_right_ + x_pos_battle_button_adjust_, y_pos_battle_button_right_ + y_pos_battle_button_adjust_, battle_button_color_, true);
  DrawString(x_pos_battle_button_text_ + x_pos_battle_button_adjust_, y_pos_battle_button_text_ + y_pos_battle_button_adjust_, kBattleButtonText, kTextForeColorWhite);

  //-----------------------------------
  // タイトルレベル遷移ボタン
  //-----------------------------------
  DrawBox(x_pos_title_button_left_ + x_pos_title_button_adjust_, y_pos_title_button_left_ + y_pos_title_button_adjust_, x_pos_title_button_right_ + x_pos_title_button_adjust_, y_pos_title_button_right_ + y_pos_title_button_adjust_, title_button_color_, true);
  DrawString(x_pos_title_button_text_ + x_pos_title_button_adjust_, y_pos_title_button_text_ + y_pos_title_button_adjust_, kTitleButtonText, kTextForeColorWhite);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
}

/// <summary>
/// 描画フェーズを変更する
/// </summary>
/// <param name=""> 描画フェーズ </param>
/// <returns></returns>
void ResultInfoUi::ChangeDrawPhase(DrawPhaseType draw_phase) {

  current_draw_phase_ = draw_phase;
}

/// <summary>
/// 画像のサイズを取得する
/// </summary>
/// <param name=""> グラフィックハンドル </param>
/// <param name=""> 画像の幅 </param>
/// <param name=""> 画像の高さ </param>
/// <returns></returns>
void ResultInfoUi::SetImageSize(int graphic_handle, int& image_width, int& image_height) {

  // グラフィックハンドルから画像のサイズを取得
  GetGraphSize(graphic_handle, &image_width, &image_height);
}

/// <summary>
/// マウスカーソルがボタン上に存在するか確認
/// </summary>
/// <param name=""> X座標 </param>
/// <param name=""> Y座標 </param>
/// <param name=""> 押されているかどうか </param>
/// <returns> true:存在する, false:存在しない </returns>
bool ResultInfoUi::CheckMouseCursorPosition(int x_pos, int y_pos, bool is_push) {

  battle_button_color_ = kShapeColorGray;
  title_button_color_ = kShapeColorGray;
  x_pos_battle_button_adjust_ = kInitialPosAdjust;
  y_pos_battle_button_adjust_ = kInitialPosAdjust;
  x_pos_title_button_adjust_ = kInitialPosAdjust;
  y_pos_title_button_adjust_ = kInitialPosAdjust;
  exists_button_ = ButtonType::kNone;

  // バトルレベル遷移ボタン上に存在するかチェック
  if ((x_pos >= x_pos_battle_button_left_ && x_pos <= x_pos_battle_button_right_) &&
      (y_pos >= y_pos_battle_button_left_ && y_pos <= y_pos_battle_button_right_)) {

    exists_button_ = ButtonType::kBattleTransition;

    // ボタンが押されている場合、ボタンの色を少し濃い灰色に設定
    // ボタンが押されていない場合、ボタンの色を白に近い灰色に設定
    if (is_push) {
      battle_button_color_ = kShapeColorMiddleGray;
      x_pos_battle_button_adjust_ = kPushPosAdjust;
      y_pos_battle_button_adjust_ = kPushPosAdjust;
     } 
    else {
      battle_button_color_ = kShapeColorWhiteGray;
    }
    return true;
  }
  
  // タイトルレベル遷移ボタン上に存在するかチェック
  if ((x_pos >= x_pos_title_button_left_ && x_pos <= x_pos_title_button_right_) && 
      (y_pos >= y_pos_title_button_left_ && y_pos <= y_pos_title_button_right_)) {

    exists_button_ = ButtonType::kTitleTransition;

    // ボタンが押されている場合、ボタンの色を少し濃い灰色に設定
    // ボタンが押されていない場合、ボタンの色を白に近い灰色に設定
    if (is_push) {
      title_button_color_ = kShapeColorMiddleGray;
      x_pos_title_button_adjust_ = kPushPosAdjust;
      y_pos_title_button_adjust_ = kPushPosAdjust;
    }
    else {
      title_button_color_ = kShapeColorWhiteGray;
    }
    return true;
  }  
  return false;
}
