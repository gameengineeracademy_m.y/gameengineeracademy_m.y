﻿#pragma once

#include "Game/BattleLevel.h"
#include "System/MenuControllerEventInterface.h"
#include "System/PlayerControllerEventInterface.h"
#include "System/BackScreenEventInterface.h"
#include "Game/GameModeEventInterface.h"
#include "Game/EnemyEventInterface.h"
#include "Game/EnemyControllerEventInterfac.h"
#include "Game/StoneManagerEventInterface.h"
#include "Game/UiEventInterface.h"

/// <summary>
/// バトルレベルイベントインターフェース
/// </summary>
class BattleLevelEventInterface : public MenuControllerEventInterface,
                                  public PlayerControllerEventInterface,
                                  public BackScreenEventInterface,
                                  public GameModeEventInterface,
                                  public EnemyEventInterface,
                                  public EnemyControllerEventInterface,
                                  public StoneManagerEventInterface,
                                  public UiEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  BattleLevelEventInterface() {

    // コンソールに出力
    std::cout << "BattleLevelEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~BattleLevelEventInterface() {

    // コンソールに出力
    std::cout << "~BattleLevelEventInterface デストラクタ" << std::endl;
  }


private:

};