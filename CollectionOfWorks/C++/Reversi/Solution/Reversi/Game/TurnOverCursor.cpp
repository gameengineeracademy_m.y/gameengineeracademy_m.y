﻿#include "Game/TurnOverCursor.h"

namespace {

  /// <summary>
  /// 待機時間
  /// </summary>
  const float kWaitingTime = 0.2f;

  /// <summary>
  /// リセット時間
  /// </summary>
  const float kResetTime = 0.0f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> タスクID </param>
/// <returns></returns>
TurnOverCursor::TurnOverCursor(int cursor_id)
  : Cursor(cursor_id, CursorType::kTurnOver)
  , cursor_handle_()
  , pattern_type_(PatternType::kPattern1)
  , accumulate_time(0.0f) {

  // コンソールに出力
  std::cout << "TurnOverCursor コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
TurnOverCursor::~TurnOverCursor() {

  // コンソールに出力
  std::cout << "~TurnOverCursor デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void TurnOverCursor::Update(float process_time) {

  // 累積時間に加算
  accumulate_time += process_time;

  if (accumulate_time >= kWaitingTime) {
    // 累積時間をリセットする
    accumulate_time = kResetTime;
    // 描画するイメージの選択
    switch (pattern_type_) {
    case PatternType::kPattern1: {
      pattern_type_ = PatternType::kPattern2;
      break;
    }
    case PatternType::kPattern2: {
      pattern_type_ = PatternType::kPattern1;
      break;
    }
    }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TurnOverCursor::Render() {

  // 表示有無を取得し、非表示なら処理終了
  bool is_display = GetDisplay();
  if (!is_display) {
    return;
  }

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();

  if (game_info == nullptr) {
    return;
  }
  // マップ表示位置を取得
  int x_adjustment = game_info->GetMapPositionX();
  int y_adjustment = game_info->GetMapPositionY();

  int x_pos = GetPositionX();
  int y_pos = GetPositionY();
  int image_width = GetImageWidth();
  int image_height = GetImageHeight();

  // 描画処理
  DrawGraph(x_adjustment + x_pos * image_width, y_adjustment + y_pos * image_height, cursor_handle_.at(static_cast<int>(pattern_type_)), true);
}