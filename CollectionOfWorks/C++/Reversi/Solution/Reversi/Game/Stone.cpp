﻿#include "Game/Stone.h"

namespace {

  /// <summary>
  /// 黒色の石のインデックス
  /// </summary>
  const int kBlackStoneIndex = 0;

  /// <summary>
  /// 白色の石のインデックス
  /// </summary>
  const int kWhiteStoneIndex = 7;

  /// <summary>
  /// 累積時間 リセット
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 累積時間 待機時間
  /// </summary>
  const float kWaitTime = 0.05f;

  /// <summary>
  /// 透過率 起動時調整値
  /// </summary>
  const int kAlphaAdjustStart = 15;

  /// <summary>
  /// 透過率 終了時調整値
  /// </summary>
  const int kAlphaAdjustFinish = 10;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> ストーンID </param>
/// <returns></returns>
Stone::Stone(int stone_id)
  : StoneBase(stone_id)
  , stone_type_(StoneType::kBlack)
  , current_phase_(PhaseType::kInitialize)
  , current_draw_phase_(DrawPhaseType::kStart)
  , stone_handle_()
  , x_pos_(0)
  , y_pos_(0)
  , image_width_(0)
  , image_height_(0)
  , is_placed_(false)
  , is_drawing_(false)
  , drawing_index_(0)
  , accumulate_time_(0.0f)
  , alpha_(0) {

  // コンソールに出力
  std::cout << "Stone コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Stone::~Stone() {

  // コンソールに出力
  std::cout << "~Stone デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void Stone::Update(float process_time) {

  bool check_finish = false;

  // 累積時間に処理時間を加算
  accumulate_time_ += process_time;

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //--------------------------
    //初期化処理フェーズ
    //--------------------------
    check_finish = Initialize();
    if (check_finish) {
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //--------------------------
    //処理中フェーズ
    //--------------------------
    check_finish = Process();
    if (check_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }

  case PhaseType::kFinalize: {
    //--------------------------
    //終了処理フェーズ
    //--------------------------
    check_finish = Finalize();

    if (check_finish) {
      // 現在のフェーズを「終了処理済み」に変更
      ChangeCurrentPhase(PhaseType::kFinalized);
    }
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Stone::Render() {

  if (current_phase_ != PhaseType::kProcess) {
    return;
  }

  // 配置済みでない場合、処理終了
  bool check_placed = IsPlaced();
  if (!check_placed) {
    return;
  }

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

  DrawGraph(x_pos_render_, y_pos_render_, stone_handle_.at(drawing_index_),true);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
}

/// <summary>
/// フェーズの変更
/// </summary>
/// <param name=""> フェーズの種類 </param>
/// <returns></returns>
void Stone::ChangeCurrentPhase(PhaseType phase_type) {

  current_phase_ = phase_type;
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""> 石の画像ハンドル </param>
/// <returns> true:処理終了, false:処理継続 </returns>
bool Stone::Initialize() {

  // 画像のサイズを取得
  GetGraphSize(stone_handle_.at(static_cast<int>(StoneType::kBlack)), &image_width_, &image_height_);
  // 透過率をセット
  alpha_ = kAlphaMax;

  return true;
}

/// <summary>
/// 処理中
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理継続 </returns>
bool Stone::Process() {

  // 配置済みでない場合、処理終了
  bool check_placed = IsPlaced();
  if (!check_placed) {
    return false;
  }

  switch (current_draw_phase_) {
  case DrawPhaseType::kFinish: {
    // 透過率に調整値を減算
    alpha_ -= kAlphaAdjustFinish;
    if (alpha_ <= kAlphaMin) {
      alpha_ = kAlphaMin;
    }
    break;
  }
  }

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return false;
  }

  GameInfo::OperationMode operation_mode = game_info->GetOperationMode();
  switch (operation_mode) {
  case GameInfo::OperationMode::kManual: {
    if (!is_drawing_) {
      accumulate_time_ = kResetTime;
    }
    else {
      if (accumulate_time_ >= kWaitTime) {
        // 累積時間をリセット
        accumulate_time_ = kResetTime;
        // 現在の石の種類によってインデックスを増減させる
        switch (stone_type_) {
        case StoneType::kBlack: {
          --drawing_index_;
          break;
        }
        case StoneType::kWhite: {
          ++drawing_index_;
          break;
        }
        }
        // インデックスが最後まで到達したら描画中フラグを降ろし、
        // 盤上の石の数を数える
        if (drawing_index_ == kBlackStoneIndex || drawing_index_ == kWhiteStoneIndex) {
          SetFinishDrawing();
        }
      }
    }
    break;
  }
  case GameInfo::OperationMode::kAuto: {
    // 現在の石の種類によってインデックスを増減させる
    switch (stone_type_) {
    case StoneType::kBlack: {
      drawing_index_ = kBlackStoneIndex;
      break;
    }
    case StoneType::kWhite: {
      drawing_index_ = kWhiteStoneIndex;
      break;
    }
    }
    if (is_drawing_) {
      SetFinishDrawing();
    }

    break;
  }
  }

  // マップ表示位置を取得
  int x_adjustment = game_info->GetMapPositionX();
  int y_adjustment = game_info->GetMapPositionY();

  // 石の種類と座標、画像サイズを取得し、表示する
  StoneType stone_type = GetStoneType();
  int x_pos = GetPositionX();
  int y_pos = GetPositionY();
  int image_width = GetImageWidth();
  int image_height = GetImageHeight();

  // 描画位置をセット
  x_pos_render_ = x_adjustment + x_pos * image_width;
  y_pos_render_ = y_adjustment + y_pos * image_height;

  return false;
}

/// <summary>
/// 終了処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理継続 </returns>
bool Stone::Finalize() {

  return true;
}

/// <summary>
/// 石の種類をセットする
/// </summary>
/// <param name=""> 石の種類 </param>
/// <returns></returns>
void Stone::SetStoneType(StoneType stone_type) {

  stone_type_ = stone_type;

  // 描画中の場合は処理終了
  if (is_drawing_) {
    return;
  }

  switch (stone_type_) {
  case StoneType::kBlack: {
    drawing_index_ = kBlackStoneIndex;
    break;
  }
  case StoneType::kWhite: {
    drawing_index_ = kWhiteStoneIndex;
    break;
  }
  }
}