﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "Game/GameMode.h"
#include <array>
#include <string>

/// <summary>
/// バトル、リザルト情報(HUD情報)
/// </summary>
class ResultInfoUi : public Task {
public:

  /// <summary>
  /// 描画フェーズ
  /// </summary>
  enum class DrawPhaseType {
    kNone,                   // 何もしないフェーズ
    kResultDrawing,          // 結果描画フェーズ
    kSelectTransitionLevel,  // 遷移レベル選択フェーズ
    kFinish,                 // 終了フェーズ
    kDrawPhaseMaxIndex       // フェーズ項目数
  };

  /// <summary>
  /// ボタンの種類
  /// </summary>
  enum class ButtonType {
    kNone,                  // 何もない
    kTitleTransition,       // タイトルレベル遷移ボタン
    kBattleTransition,      // バトルレベル遷移ボタン
    kButtonMaxIndex         // フェーズ項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ResultInfoUi();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~ResultInfoUi();

  /// <summary>
  /// 毎フレーム更新処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 毎フレーム描画処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// リザルト勝利のグラフィックハンドルを設定する
  /// </summary>
  /// <param name="graphic_handle"> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandleWinner(int graphic_handle) { winner_handle_ = graphic_handle; };

  /// <summary>
  /// リザルト勝利のグラフィックハンドルを取得する
  /// </summary>
  /// <param></param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandleWinner() { return winner_handle_; }

  /// <summary>
  /// リザルト敗北のグラフィックハンドルを設定する
  /// </summary>
  /// <param name="graphic_handle"> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandleLose(int graphic_handle) { lose_handle_ = graphic_handle; };

  /// <summary>
  /// リザルト敗北のグラフィックハンドルを取得する
  /// </summary>
  /// <param></param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandleLose() { return lose_handle_; }

  /// <summary>
  /// リザルト引分のグラフィックハンドルを設定する
  /// </summary>
  /// <param name="graphic_handle"> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandleDraw(int graphic_handle) { draw_handle_ = graphic_handle; };

  /// <summary>
  /// リザルト引分のグラフィックハンドルを取得する
  /// </summary>
  /// <param></param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandleDraw() { return draw_handle_; }

  /// <summary>
   /// 黒色の石のグラフィックハンドルを設定する
   /// </summary>
   /// <param name="graphic_handle"> グラフィックハンドル </param>
   /// <returns></returns>
  void SetGraphicHandleStoneBlack(int graphic_handle) { stone_black_handle_ = graphic_handle; };

  /// <summary>
  /// 黒色の石のグラフィックハンドルを取得する
  /// </summary>
  /// <param></param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandleStoneBlack() { return stone_black_handle_; }

  /// <summary>
   /// 白色の石のグラフィックハンドルを設定する
   /// </summary>
   /// <param name="graphic_handle"> グラフィックハンドル </param>
   /// <returns></returns>
  void SetGraphicHandleStoneWhite(int graphic_handle) { stone_white_handle_ = graphic_handle; };

  /// <summary>
  /// 白色の石のグラフィックハンドルを取得する
  /// </summary>
  /// <param></param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandleStoneWhite() { return stone_white_handle_; }

  /// <summary>
  /// 描画フェーズを変更する
  /// </summary>
  /// <param name=""> 描画フェーズ </param>
  /// <returns></returns>
  void ChangeDrawPhase(DrawPhaseType);

  /// <summary>
  /// マウスカーソルがボタン上に存在するか確認
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <param name=""> 押されているかどうか </param>
  /// <returns> true:存在する, false:存在しない </returns>
  bool CheckMouseCursorPosition(int, int, bool);

  /// <summary>
  /// マウスカーソルが存在するボタンを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ButtonType GetExistsCursorButton() { return exists_button_; }


private:

  /// <summary>
  /// 画像のサイズを取得する
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <param name=""> 画像の幅 </param>
  /// <param name=""> 画像の高さ </param>
  /// <returns></returns>
  void SetImageSize(int, int&, int&);


private:

  /// <summary>
  /// 現在の描画フェーズ
  /// </summary>
  DrawPhaseType current_draw_phase_;

  /// <summary>
  /// リザルト勝利 グラフィックハンドル
  /// </summary>
  int winner_handle_;

  /// <summary>
  /// リザルト敗北 グラフィックハンドル
  /// </summary>
  int lose_handle_;

  /// <summary>
  /// リザルト引分 グラフィックハンドル
  /// </summary>
  int draw_handle_;

  /// <summary>
  /// 黒色の石 グラフィックハンドル
  /// </summary>
  int stone_black_handle_;

  /// <summary>
  /// 白色の石 グラフィックハンドル
  /// </summary>
  int stone_white_handle_;

  /// <summary>
  /// プレイヤー側リザルト 表示倍率
  /// </summary>
  float magnification_player_;

  /// <summary>
  /// CPU側リザルト 表示倍率
  /// </summary>
  float magnification_cpu_;

  /// <summary>
  /// リザルト 表示倍率 変化量
  /// </summary>
  float magnification_rate_;

  /// <summary>
  /// プレイヤー側リザルト 角度
  /// </summary>
  float angle_player_;

  /// <summary>
  /// CPU側リザルト 角度
  /// </summary>
  float angle_cpu_;

  /// <summary>
  /// リザルト 角度
  /// </summary>
  float angle_rate_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// プレイヤー情報 描画位置 X座標
  /// </summary>
  int x_pos_player_data_;

  /// <summary>
  /// CPU情報 描画位置 X座標
  /// </summary>
  int x_pos_cpu_data_;

  /// <summary>
  /// 黒色の石の数 描画位置 X座標
  /// </summary>
  int x_pos_black_count_;

  /// <summary>
  /// 白色の石の数 描画位置 X座標
  /// </summary>
  int x_pos_white_count_;

  /// <summary>
  /// 黒色の石 描画位置 X座標
  /// </summary>
  int x_pos_black_;

  /// <summary>
  /// 白色の石 描画位置 X座標
  /// </summary>
  int x_pos_white_;

  /// <summary>
  /// プレイヤーリザルト
  /// </summary>
  int result_player_;

  /// <summary>
  /// CPUリザルト
  /// </summary>
  int result_cpu_;

  /// <summary>
  /// レベル遷移タイトル遷移ボタンUI座標
  /// </summary>
  int x_pos_title_button_left_;

  /// <summary>
  /// レベル遷移タイトル遷移ボタンUI座標
  /// </summary>
  int y_pos_title_button_left_;

  /// <summary>
  /// レベル遷移タイトル遷移ボタンUI座標
  /// </summary>
  int x_pos_title_button_right_;

  /// <summary>
  /// レベル遷移タイトル遷移ボタンUI座標
  /// </summary>
  int y_pos_title_button_right_;

  /// <summary>
  /// レベル遷移バトル遷移ボタンUI座標
  /// </summary>
  int x_pos_battle_button_left_;

  /// <summary>
  /// レベル遷移バトル遷移ボタンUI座標
  /// </summary>
  int y_pos_battle_button_left_;

  /// <summary>
  /// レベル遷移バトル遷移ボタンUI座標
  /// </summary>
  int x_pos_battle_button_right_;

  /// <summary>
  /// レベル遷移バトル遷移ボタンUI座標
  /// </summary>
  int y_pos_battle_button_right_;

  /// <summary>
  /// タイトル遷移ボタン 押下時の調整値
  /// </summary>
  int x_pos_title_button_adjust_;

  /// <summary>
  /// タイトル遷移ボタン 押下時の調整値
  /// </summary>
  int y_pos_title_button_adjust_;

  /// <summary>
  /// バトル遷移ボタン 押下時の調整値
  /// </summary>
  int x_pos_battle_button_adjust_;

  /// <summary>
  /// バトル遷移ボタン 押下時の調整値
  /// </summary>
  int y_pos_battle_button_adjust_;

  /// <summary>
  /// タイトル遷移ボタンテキスト表示座標
  /// </summary>
  int x_pos_title_button_text_;

  /// <summary>
  /// タイトル遷移ボタンテキスト表示座標
  /// </summary>
  int y_pos_title_button_text_;

  /// <summary>
  /// バトル遷移ボタンテキスト表示座標
  /// </summary>
  int x_pos_battle_button_text_;

  /// <summary>
  /// バトル遷移ボタンテキスト表示座標
  /// </summary>
  int y_pos_battle_button_text_;

  /// <summary>
  /// タイトル遷移ボタンの色
  /// </summary>
  int title_button_color_;

  /// <summary>
  /// バトル遷移ボタンの色
  /// </summary>
  int battle_button_color_;

  /// <summary>
  /// カーソルがあるボタン
  /// </summary>
  ButtonType exists_button_;

  /// <summary>
  /// 描画 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 描画 図形透過率
  /// </summary>
  int alpha_shape_;
};