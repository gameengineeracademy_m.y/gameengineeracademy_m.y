﻿#include "Game/Enemy.h"

namespace {

  /// <summary>
  /// 累積時間リセット
  /// </summary>
  const float kTimeResetValue = 0.0f;

  /// <summary>
  /// 思考時間 下限(思考している風に見せる)
  /// </summary>
  const float kThinkTimeLowLimit = 0.7f;

  /// <summary>
  /// 思考時間 上限(思考している風に見せる)
  /// </summary>
  const float kThinkTimeHighLimit = 1.0f;

  /// <summary>
  /// 思考時間 固定値 (オートモード用)
  /// </summary>
  const float kThinkTimeFixed = 0.0f;

  /// <summary>
  /// 待機時間 (マニュアルモード用)
  /// </summary>
  const float kWaitTimeManual = 0.8f;

  /// <summary>
  /// 待機時間 (オートモード用)
  /// </summary>
  const float kWaitTimeAuto = 0.0f;

  /// <summary>
  /// 配置可能場所が1箇所
  /// </summary>
  const int kOnePlace = 1;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name="enemy_event_interface"> エネミーイベントインターフェース </param>
/// <param name="enemy_event_interface"> エネミーコントローライベントインターフェース </param>
/// <param name=""> カーソルの種類 </param>
/// <returns></returns>
Enemy::Enemy(EnemyEventInterface& enemy_event_interface,
             EnemyControllerEventInterface& enemy_controller_event_interface,
             Cursor* cursor)
  : Task(TaskId::kEnemy)
  , enemy_event_interface_(enemy_event_interface)
  , enemy_controller_(enemy_controller_event_interface, cursor)
  , current_phase_(PhaseType::kNone)
  , pattern_(ThinkingPattern::kNormal)
  , accumulate_time_(0.0f)
  , thinking_time_(0.0f)
  , waiting_time_(0.0f)
  , x_pos_(0)
  , y_pos_(0) {

  // コンソールに出力
  std::cout << "Enemy コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Enemy::~Enemy() {

  // コンソールに出力
  std::cout << "~Enemy デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void Enemy::Update(float process_time) {

  bool is_playing = enemy_event_interface_.OnCheckBattleLevelPhasePlay();
  if (!is_playing) {
    return;
  }

  switch (current_phase_) {
  case PhaseType::kThink: {
    //----------------------------
    //石の配置場所思考フェーズ
    //----------------------------
    // 思考処理
    ThinkPlacement();

    // ゲーム情報のインスタンスを取得し、操作モードを取得する
    GameInfo* game_info = GameInfo::GetGameInfoInstance();
    GameInfo::OperationMode operation_mode = game_info->GetOperationMode();
    // 待機時間を算出
    switch (operation_mode) {
    case GameInfo::OperationMode::kManual: {
      // 乱数を用いて思考時間を決定する
      thinking_time_ = GetRandomWaitTime();
      break;
    }
    case GameInfo::OperationMode::kAuto: {
      thinking_time_ = kThinkTimeFixed;
      break;
    }
    }

    // 累積時間をリセットする
    accumulate_time_ = kTimeResetValue;
    // 現在のフェーズを「思考中」フェーズに変更する
    ChangeCurrentPhase(PhaseType::kThinking);
    break;
  }
  case PhaseType::kThinking: {
    //----------------------------
    //思考しているように見せるフェーズ
    //----------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    // 指定時間待機後、現在のフェーズを「思考終了」に変更する
    if (accumulate_time_ >= thinking_time_) {
      // 累積時間をリセットする
      accumulate_time_ = kTimeResetValue;
      ChangeCurrentPhase(PhaseType::kFinishThink);
    }
    break;
  }
  case PhaseType::kFinishThink: {
    //----------------------------
    //思考終了フェーズ
    //----------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    // エネミーカーソルを石を置く座標にセットする
    enemy_event_interface_.OnSetCursorPosition(x_pos_, y_pos_);

    // ゲーム情報のインスタンスを取得し、操作モードを取得する
    GameInfo* game_info = GameInfo::GetGameInfoInstance();
    GameInfo::OperationMode operation_mode = game_info->GetOperationMode();
    // 待機時間を算出
    switch (operation_mode) {
    case GameInfo::OperationMode::kManual: {
      waiting_time_ = kWaitTimeAuto;
      break;
    }
    case GameInfo::OperationMode::kAuto: {
      waiting_time_ = kWaitTimeAuto;
      break;
    }
    }

    if (accumulate_time_ >= waiting_time_) {
      // 現在のフェーズを「石を置く」フェーズに変更する
      ChangeCurrentPhase(PhaseType::kPutStone);
    }
    break;
  }
  case PhaseType::kPutStone: {
    //----------------------------
    //石を置くフェーズ
    //----------------------------
    enemy_controller_.PushMouseLeftButton();
    // 現在のフェーズを「何もしない」フェーズに変更する
    ChangeCurrentPhase(PhaseType::kNone);
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Enemy::Render() {

}

/// <summary>
/// 待機時間をランダムで生成する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
float Enemy::GetRandomWaitTime() {

  float wait_time = 0.0f;

  // ランダム生成器を作成
  std::random_device seed_gen;
  std::mt19937 engine(seed_gen());
  // ランダムに取得する値の範囲(実数)
  std::uniform_real_distribution<float> dist(kThinkTimeLowLimit, kThinkTimeHighLimit);
  // 待機する時間をランダムに設定する
  wait_time = dist(engine);

  return wait_time;
}

/// <summary>
/// 現在のフェーズを変更
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Enemy::ChangeCurrentPhase(PhaseType phase_type) {

  current_phase_ = phase_type;
}

/// <summary>
/// 思考処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Enemy::ThinkPlacement() {

  switch (pattern_) {
  case ThinkingPattern::kNotSet: {
    //-----------------------------
    //未設定
    //-----------------------------
    return;
  }
  case ThinkingPattern::kNormal: {
    //-----------------------------
    //難易度：普通
    //-----------------------------
    ThinkPlacementLevelNormal();
    break;
  }
  }

}

/// <summary>
/// 思考処理 難易度：普通
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Enemy::ThinkPlacementLevelNormal() {

  enemy_event_interface_.OnThinkPlacement();
}