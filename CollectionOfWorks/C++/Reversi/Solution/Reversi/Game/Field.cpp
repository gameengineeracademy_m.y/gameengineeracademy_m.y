﻿#include "Game/Field.h"

namespace {

  /// <summary>
  /// 画像サイズ　幅
  /// </summary>
  const int kImageSizeWidth = 64;

  /// <summary>
  /// 画像サイズ　高さ
  /// </summary>
  const int kImageSizeHeight = 64;

  /// <summary>
  /// 評価値の最大値 初期値
  /// </summary>
  const int kEvaluationInitialValue = -10000;

  /// <summary>
  /// 正の符号
  /// </summary>
  const int kSignPlus = +1;

  /// <summary>
  /// 負の符号
  /// </summary>
  const int kSignMinus = -1;

  /// <summary>
  /// 評価処理 リセット値
  /// </summary>
  const int kEvaReset = 0;

  /// <summary>
  /// 盤上の先頭インデックス
  /// </summary>
  const int kStartIndex = 0;

  /// <summary>
  /// 盤上の終端インデックス
  /// </summary>
  const int kEndIndex = 7;

  /// <summary>
  /// 乱数 下限
  /// </summary>
  const float kLowLimit = 0.0f;

  /// <summary>
  /// 乱数 上限
  /// </summary>
  const float kHighLimit = 1.0f;

  /// <summary>
  /// 乱数 倍率
  /// </summary>
  const int kRondomMagnification = 3;

  /// <summary>
  /// 盤評価係数
  /// </summary>
  const int kEvaBoardCoeffient = 2;

  /// <summary>
  /// 確定石差分係数
  /// </summary>
  const int kConfirmStoneCoeffient = 55;

  /// <summary>
  /// 盤枠 描画位置 X方向調整値
  /// </summary>
  const int kLeftFrameX = kImageSizeWidth / 2;

  /// <summary>
  /// 盤枠 描画位置 Y方向調整値
  /// </summary>
  const int kLeftFrameY = kImageSizeHeight / 2;

  /// <summary>
  /// 盤枠を含めた盤面の終端
  /// </summary>
  const int kFrameIndex = 10;

  /// <summary>
  /// 盤枠を含めた盤面の先頭インデックス
  /// </summary>
  const int kStartFrameIndex = 0;

  /// <summary>
  /// 盤枠を含めた盤面の終端インデックス
  /// </summary>
  const int kEndFrameIndex = 9;

  /// <summary>
  /// 拡大率
  /// </summary>
  const float kMagnification = 1.0f;

  /// <summary>
  /// 角度 初期値 [rad]
  /// </summary>
  const double k0degree = 0.0;

  /// <summary>
  /// 角度 90°[rad]
  /// </summary>
  const double k90degree = M_PI / 2;

  /// <summary>
  /// 角度 180°[rad]
  /// </summary>
  const double k180degree = M_PI;

  /// <summary>
  /// 調査量 初期値
  /// </summary>
  const int kInitialRate = 1;

  /// <summary>
  /// 透過率 起動時調整値
  /// </summary>
  const int kAlphaAdjustStart = 15;

  /// <summary>
  /// 透過率 終了時調整値
  /// </summary>
  const int kAlphaAdjustFinish = 10;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Field::Field()
  : Task(TaskId::kField)
  , field_data_()
  , field_data_stone_()
  , placeable_place_()
  , direction_data_()
  , current_phase_(PhaseType::kInitialize)
  , board_handle_(0)
  , frame_handle_()
  , search_rate_(0) {

  // コンソールに出力
  std::cout << "Field コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Field::~Field() {

  // コンソールに出力
  std::cout << "~Field デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void Field::Update(float process_time) {

  bool check_finish = false;

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //-----------------------------
    //初期化処理フェーズ
    //-----------------------------
    check_finish = Initialize();

    if (check_finish) {
      // フェーズを「処理中」へ変更する
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //-----------------------------
    //処理中フェーズ
    //-----------------------------
    check_finish = Process();

    if (check_finish) {
      // フェーズを「処理中」へ変更する
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //-----------------------------
    //終了処理フェーズ
    //-----------------------------
    check_finish = Finalize();

    if (check_finish) {
      // フェーズを「処理中」へ変更する
      ChangeCurrentPhase(PhaseType::kFinalized);
    }
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Field::Render() {

  // 現在のフェーズが「処理中」以外の場合、処理終了
  if (current_phase_ != PhaseType::kProcess) {
    return;
  }

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }

  // マップ表示位置を取得
  int x_adjustment = game_info->GetMapPositionX();
  int y_adjustment = game_info->GetMapPositionY();

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

  // 盤面の描画
  for (int y = 0; y < static_cast<int>(field_data_.size()); ++y) {
    for (int x = 0; x < static_cast<int>(field_data_.at(0).size()); ++x) {
      DrawGraph(x_adjustment + x * kImageSizeWidth, y_adjustment + y * kImageSizeHeight, board_handle_, true);
    }
  }

  // 盤枠の描画
  int index = 0;
  double angle = k0degree;
  for (int y = 0; y < kFrameIndex; y++) {
    for (int x = 0; x < kFrameIndex; x++) {

      // 枠の位置以外は処理を行わない
      if ((x != kStartFrameIndex && x != kEndFrameIndex) &&
          (y != kStartFrameIndex && y != kEndFrameIndex)) {
        continue;
      }

      if (x == kStartFrameIndex && y == kStartFrameIndex) {
        index = kCornerFrame;
        angle = k0degree;
      }
      else if (x == kStartFrameIndex && y == kEndFrameIndex) {
        index = kCornerFrame;
        angle = kSignMinus * k90degree;
      }
      else if (x == kEndFrameIndex && y == kEndFrameIndex) {
        index = kCornerFrame;
        angle = k180degree;
      }
      else if (x == kEndFrameIndex && y == kStartFrameIndex) {
        index = kCornerFrame;
        angle = k90degree;
      }
      else {
        index = kOuterFrame;
        if (x == kStartFrameIndex) {
          angle = kSignMinus * k90degree;
        }
        else if (x == kEndFrameIndex) {
          angle = k90degree;
        }
        else if (y == kStartFrameIndex) {
          angle = k0degree;
        }
        else if (y == kEndFrameIndex) {
          angle = k180degree;
        }
      }
      // 盤枠の描画
      DrawRotaGraph(x_adjustment + x * kImageSizeWidth - kLeftFrameX, y_adjustment + y * kImageSizeHeight - kLeftFrameY, kMagnification, angle, frame_handle_.at(index), true);
    }
  }

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
}

/// <summary> 
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
bool Field::Initialize() {
  //------------------------------
  //フィールド情報の設定
  //------------------------------
  field_data_ = {
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 2, 1, 0, 0, 0},
    {0, 0, 0, 1, 2, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0}
  };

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();

  // フィールド情報からマップの横幅と高さを取得
  int field_width = static_cast<int>(field_data_.at(0).size()) * kImageSizeWidth;
  int field_height = static_cast<int>(field_data_.size()) * kImageSizeHeight;

  if (game_info != nullptr) {
    // ゲーム情報にマップサイズをセット
    game_info->SetMapSize(field_width, field_height);
  }

  //------------------------------
  //石配置用配列の初期化
  //------------------------------
  // 配列の空情報を作成
  std::vector<Stone*> initial_set_array;

  for (int i = 0; i < static_cast<int>(field_data_.at(0).size()); ++i) {
    initial_set_array.push_back(nullptr);
  }

  // 石配置用の配列をから情報で初期化する
  field_data_stone_.resize(field_data_.size());

  for (int i = 0; i < static_cast<int>(field_data_stone_.size()); ++i) {
    field_data_stone_[i].resize(field_data_.at(0).size());
    field_data_stone_[i] = initial_set_array;
  }

  //------------------------------
  //方向情報の初期設定
  //------------------------------
  direction_data_[FieldDirection::kUp] = std::make_pair(0, -1);
  direction_data_[FieldDirection::kRight] = std::make_pair(1, 0);
  direction_data_[FieldDirection::kDown] = std::make_pair(0, 1);
  direction_data_[FieldDirection::kLeft] = std::make_pair(-1, 0);
  direction_data_[FieldDirection::kUpperRight] = std::make_pair(1, -1);
  direction_data_[FieldDirection::kLowerRight] = std::make_pair(1, 1);
  direction_data_[FieldDirection::kLowerLeft] = std::make_pair(-1, 1);
  direction_data_[FieldDirection::kUpperLeft] = std::make_pair(-1, -1);

  //------------------------------
  //ひっくり返す石の調査量
  //------------------------------
  search_rate_ = kInitialRate;

  return true;
}

/// <summary>
/// 処理中
/// </summary>
/// <param name=""></param>
/// <returns></returns>
bool Field::Process() {

  switch (current_draw_phase_) {
  case DrawPhaseType::kStart: {
    // 透過率に調整値を加算
    alpha_ += kAlphaAdjustStart;
    if (alpha_ >= kAlphaMax) {
      alpha_ = kAlphaMax;
    }
    break;
  }
  case DrawPhaseType::kFinish: {
    // 透過率に調整値を減算
    alpha_ -= kAlphaAdjustFinish;
    if (alpha_ <= kAlphaMin) {
      alpha_ = kAlphaMin;
    }
    break;
  }
  }

  return false;
}

/// <summary>
/// 終了処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
bool Field::Finalize() {

  // フィールド情報クリア
  field_data_.clear();

  return true;
}

/// <summary>
/// 石の初期配置位置にセット
/// </summary>
/// <param name=""> 石 </param>
/// <returns> true:セット成功, false:セット失敗 </returns>
bool Field::PutInitialPositionStone(Stone& stone) {

  int x_pos = 0;
  int y_pos = 0;

  // 石の初期配置位置を取得
  bool check_pos = SearchInitialPositionStone(stone, x_pos, y_pos);

  if (check_pos) {
    // 石の配置処理
    PutStone(stone, x_pos, y_pos);
  }

  return check_pos;
}

/// <summary>
/// 石の初期配置位置を取得
/// </summary>
/// <param name=""> 石 </param>
/// <param name=""> 初期位置のX座標 </param>
/// <param name=""> 初期位置のY座標 </param>
/// <returns> true:取得成功, false:取得失敗 </returns>
bool Field::SearchInitialPositionStone(Stone& stone, int& x_pos, int& y_pos) {

  // 変数初期化
  x_pos = y_pos = 0;

  FieldElement element;
  Stone::StoneType stone_type = stone.GetStoneType();

  // 石の種類からフィールド要素を取得
  switch (stone_type) {
  case Stone::StoneType::kBlack: {
    element = FieldElement::kBlack;
    break;
  }
  case Stone::StoneType::kWhite: {
    element = FieldElement::kWhite;
    break;
  }
  default: {
    return false;
  }
  }

  for (int y = 0; y < static_cast<int>(field_data_.size()); ++y) {
    for (int x = 0; x < static_cast<int>(field_data_.at(0).size()); ++x) {
      if (field_data_.at(y).at(x) != static_cast<int>(element)) {
        // フィールド情報から対象座標が対象の要素以外の場合、処理を続ける
        continue;
      }

      if (field_data_stone_.at(y).at(x) != nullptr) {
        // 石配置済みの場合、処理を続ける
        continue;
      }

      x_pos = x;
      y_pos = y;
      return true;
    }
  }

  return false;
}

/// <summary>
/// マウスカーソルが盤上にあるかチェック
/// </summary>
/// <param name=""> マウスのX座標 </param>
/// <param name=""> マウスのY座標 </param>
/// <returns> true:盤上にある, false:盤上にない </returns>
bool Field::CheckBoardDisplayRange(int mouse_x_pos, int mouse_y_pos) {

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();

  if (game_info == nullptr) {
    return false;
  }
  // マップ表示位置を取得
  int upper_left_x = game_info->GetMapPositionX();
  int upper_left_y = game_info->GetMapPositionY();

  // 右下の座標
  int bottom_right_x = upper_left_x + kImageSizeWidth * static_cast<int>(field_data_.at(0).size());
  int bottom_right_y = upper_left_y + kImageSizeHeight * static_cast<int>(field_data_.size());

  if ((mouse_x_pos >= upper_left_x && mouse_x_pos < bottom_right_x) &&
    (mouse_y_pos >= upper_left_y && mouse_y_pos < bottom_right_y)) {
    return true;
  }
  return false;
}

/// <summary>
/// 指定座標に石が配置済みかどうか確認
/// </summary>
/// <param name=""> 盤面上のX座標 </param>
/// <param name=""> 盤面上のY座標 </param>
/// <returns> true:配置可能, false:配置不可 </returns>
bool Field::CheckCanPut(int x_pos, int y_pos) {

  // 指定座標が配置可能場所でないなら「配置不可」と返す
  std::pair<int, int> search_value(x_pos, y_pos);
  auto iterator = std::find(std::begin(placeable_place_), std::end(placeable_place_), search_value);
  if (iterator == placeable_place_.end()) {
    return false;
  }
  return true;
}

/// <summary>
/// 石の配置
/// </summary>
  /// <param name=""> 石 </param>
/// <param name=""> 盤面上のX座標 </param>
/// <param name=""> 盤面上のY座標 </param>
/// <returns></returns>
void Field::PutStone(Stone& stone, int x_pos, int y_pos) {

  // 座標をセットする
  stone.SetPositionX(x_pos);
  stone.SetPositionY(y_pos);
  // 石の配置用のフィールド情報にもセット
  field_data_stone_.at(y_pos).at(x_pos) = &stone;
}

/// <summary>
/// 配置可能場所の探索
/// </summary>
/// <param name="stone_type"> 石の種類 </param>
/// <returns> true:配置可能場所あり, false:配置可能場所なし </returns>
bool Field::IsPlaceablePlace(Stone::StoneType stone_type) {

  // 配置可能座標をクリア
  placeable_place_.clear();

  // 配置場所探索処理
  for (int y = 0; y < static_cast<int>(field_data_stone_.size()); ++y) {
    for (int x = 0; x < static_cast<int>(field_data_stone_.at(0).size()); ++x) {
      // 石配置場所情報にて石が置かれている場合は、ループを続ける
      if (field_data_stone_.at(y).at(x) != nullptr) {
        continue;
      }

      // 配置可能か調査
      bool is_placeable = SearchPlaceablePlace(stone_type, x, y);
      // 石配置可能座標の場合は配列に格納する
      if (is_placeable) {
        placeable_place_.push_back(std::make_pair(x, y));
        //std::cout << "<< 配置可能座標 >> : (" << x << "," << y << ")" << std::endl;
      }
    }
  }

  // 配置可能場所がない場合、配置場所がないと返す
  if (placeable_place_.empty()) {
    return false;
  }

  return true;
}

/// <summary>
/// 配置可能場所の探索
/// </summary>
/// <param name=""> 石の種類 </param>
/// <param name=""> X座標 </param>
/// <param name=""> Y座標 </param>
/// <returns> true:配置可能, false:配置可能 </returns>
bool Field::SearchPlaceablePlace(Stone::StoneType stone_type, int x_pos, int y_pos) {

  // 返す石があるかどうか
  // true:返す石がある, false:返す石はない
  bool is_turn_over = false;

  // ひっくり返す石 座標
  std::queue<std::pair<int, int>> turn_over_pos;
  std::vector<std::pair<int, int>> turn_over_all_pos;

  //------------------------------------------------
  //調査方向分のループ処理を行う
  //------------------------------------------------
  for (int i = 0; i < static_cast<int>(direction_data_.size()); ++i) {

    // 調査座標のセット
    int check_pos_x = x_pos;
    int check_pos_y = y_pos;
    // 調査方向の移動量を取得
    int add_x = direction_data_[static_cast<FieldDirection>(i)].first;
    int add_y = direction_data_[static_cast<FieldDirection>(i)].second;
    // ひっくり返す石の座標をクリア
    while (!turn_over_pos.empty()) {
      turn_over_pos.pop();
    }

    //------------------------------------------------
    //ひっくり返せる石があるかどうか確認する
    //------------------------------------------------
    while (true) {
      // 探索方向へ座標を調整
      check_pos_x += add_x;
      check_pos_y += add_y;

      // 調査座標が盤の範囲外の場合、ループを抜ける
      if ((check_pos_x < 0 || check_pos_x >= static_cast<int>(field_data_stone_.at(0).size())) ||
          (check_pos_y < 0 || check_pos_y >= static_cast<int>(field_data_stone_.size()))) {
        //std::cout << i << " 調査座標が盤の範囲外 : (" << check_pos_x << "," << check_pos_y << ")" << std::endl;
        break;
      }
      // 調査座標に石が存在しない場合、ループを抜ける
      if (field_data_stone_.at(check_pos_y).at(check_pos_x) == nullptr) {
        //std::cout << i << " 調査座標に石が存在しない : (" << check_pos_x << "," << check_pos_y << ")" << std::endl;
        break;
      }
      // 調査座標に自分と異なる石が存在する場合、座標を格納
      // 調査座標に自分と同じ石が存在する場合、
      // ①ひっくり返す石が既に存在：フラグを立てて、ループを抜ける
      // ②ひっくり返す石の存在なし：ループを抜ける
      Stone::StoneType check_stone_type = field_data_stone_.at(check_pos_y).at(check_pos_x)->GetStoneType();
      if (check_stone_type != stone_type) {
        turn_over_pos.push(std::make_pair(check_pos_x, check_pos_y));
        //std::cout << i << " 調査座標に自分と異なる石が存在 : (" << check_pos_x << "," << check_pos_y << ")" << std::endl;
      }
      else {
        if (!turn_over_pos.empty()) {
          is_turn_over = true;
          // ひっくり返す座標を格納
          while (!turn_over_pos.empty()) {
            turn_over_all_pos.push_back(turn_over_pos.front());
            turn_over_pos.pop();
          }
        }
        //std::cout << i << " 調査座標に自分と同じ石が存在 : (" << check_pos_x << "," << check_pos_y << ")" << std::endl;
        break;
      }
    }
  }

  if (is_turn_over) {
    // ひっくり返す石を追加
    turn_over_place_[std::make_pair(x_pos, y_pos)] = turn_over_all_pos;
  }

  return is_turn_over;
}

/// <summary>
/// はさまれた石をひっくり返す
/// </summary>
/// <param name=""> 石設置座標 </param>
/// <returns> true:処理完了, false:処理未完了 </returns>
bool Field::TurnOverStone(std::pair<int, int> pos) {

  // ゲーム情報を取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  GameInfo::OperationMode operation_mode = game_info->GetOperationMode();
  switch (operation_mode) {
  case GameInfo::OperationMode::kManual: {
    //-------------------------------------
    //操作モード：マニュアル
    //-------------------------------------
    // 設置した石の座標
    int x_pos = pos.first;
    int y_pos = pos.second;
    // ひっくり返す石が存在するかどうか
    bool is_exists = false;

    // 設置した石の周囲のひっくり返す石を調べる
    for (int i = 0; i < static_cast<int>(FieldDirection::kDirectionMaxIndex); ++i) {

      FieldDirection direction = static_cast<FieldDirection>(i);
      x_pos = pos.first + search_rate_ * direction_data_[direction].first;
      y_pos = pos.second + search_rate_ * direction_data_[direction].second;
      std::pair<int, int> search_pos(x_pos, y_pos);

      auto iterator = std::find(turn_over_place_[pos].begin(), turn_over_place_[pos].end(), search_pos);
      if (iterator == turn_over_place_[pos].end()) {
        continue;
      }

      // ひっくり返す石が存在したら存在フラグを立てる
      is_exists = true;
      // 対象座標の石の種類を取得し、対戦相手の石の色に変換する
      Stone::StoneType stone_type = field_data_stone_.at(y_pos).at(x_pos)->GetStoneType();
      Stone::StoneType opponent_type = static_cast<Stone::StoneType>(!static_cast<bool>(stone_type));
      // 描画を開始、石の種類をセットする
      field_data_stone_.at(y_pos).at(x_pos)->SetStartDrawing();
      field_data_stone_.at(y_pos).at(x_pos)->SetStoneType(opponent_type);
    }

    // 調査後、フラグが立っているなら調査量をインクリメントする
    // 調査後、フラグが降りているなら調査量を初期化、返す石をクリアする
    if (is_exists) {
      ++search_rate_;
      return false;
    }
    else {
      search_rate_ = kInitialRate;
      turn_over_place_.clear();
    }
    break;
  }
  case GameInfo::OperationMode::kAuto: {
    //-------------------------------------
    //操作モード：オート
    //-------------------------------------
    // 石をひっくり返していく
    for (int i = 0; i < static_cast<int>(turn_over_place_[pos].size()); ++i) {
      // ひっくり返す座標を取得
      int x_pos = turn_over_place_[pos].at(i).first;
      int y_pos = turn_over_place_[pos].at(i).second;
      // 対象座標の石の種類を取得し、対戦相手の石の色に変換する
      Stone::StoneType stone_type = field_data_stone_.at(y_pos).at(x_pos)->GetStoneType();
      Stone::StoneType opponent_type = static_cast<Stone::StoneType>(!static_cast<bool>(stone_type));
      // 描画を開始、石の種類をセットする
      field_data_stone_.at(y_pos).at(x_pos)->SetStartDrawing();
      field_data_stone_.at(y_pos).at(x_pos)->SetStoneType(opponent_type);
    }

    // ひっくり返す石の座標をクリアする
    turn_over_place_.clear();

    break;
  }
  }


  return true;
}

/// <summary>
/// スコア計算
/// </summary>
/// <param name=""> 設置する石の種類 </param>
/// <param name=""> 設置するX座標 </param>
/// <param name=""> 設置するY座標 </param>
/// <returns></returns>
void Field::CalculateScore(Stone::StoneType stone_type, int& x_pos_eva, int& y_pos_eva) {

  // マスの評価(各マスに対する重み付け)
  std::vector<std::vector<int>> field_weight = {
                                 { 45, -11,  4, -1, -1,  4, -11,  45},
                                 {-11, -16, -1, -3, -3,  2, -16, -11},
                                 {  4,  -1,  2, -1, -1,  2,  -1,   4},
                                 { -1,  -3, -1,  0,  0, -1,  -3,  -1},
                                 { -1,  -3, -1,  0,  0, -1,  -3,  -1},
                                 {  4,  -1,  2, -1, -1,  2,  -1,   4},
                                 {-11, -16, -1, -3, -3, -1, -16, -11},
                                 { 45, -11,  4, -1, -1,  4, -11,  45}
  };

  // フィールド情報を用意
  std::vector<std::vector<SetStone>> field_data;
  // 配列の空情報を作成
  std::vector<SetStone> initial_set_array;
  for (int i = 0; i < static_cast<int>(field_data_.at(0).size()); ++i) {
    initial_set_array.push_back(SetStone::kNone);
  }
  // 石配置用の配列を空情報で初期化する
  field_data.resize(field_data_.size());
  for (int i = 0; i < static_cast<int>(field_data.size()); ++i) {
    field_data[i].resize(field_data_.at(0).size());
    field_data[i] = initial_set_array;
  }

  for (int y = 0; y < static_cast<int>(field_data_stone_.size()); ++y) {
    for (int x = 0; x < static_cast<int>(field_data_stone_.at(0).size()); ++x) {

      if (field_data_stone_.at(y).at(x) == nullptr) {
        continue;
      }

      Stone::StoneType stone_type = field_data_stone_.at(y).at(x)->GetStoneType();
      switch (stone_type) {
      case Stone::StoneType::kBlack: {
        field_data.at(y).at(x) = SetStone::kBlack;
        break;
      }
      case Stone::StoneType::kWhite: {
        field_data.at(y).at(x) = SetStone::kWhite;
        break;
      }
      }
    }
  }

  // 評価値の最大値
  int evaluation_max = kEvaluationInitialValue;
  x_pos_eva = 0;
  y_pos_eva = 0;
  // 配置する石の種類
  SetStone put_stone = SetStone::kNone;
  // 計算用のフィールド情報を用意
  std::vector<std::vector<SetStone>> field_data_calc;

  // チェック対象の石の種類を取得
  switch (stone_type) {
  case Stone::StoneType::kBlack: {
    put_stone = SetStone::kBlack;
    break;
  }
  case Stone::StoneType::kWhite: {
    put_stone = SetStone::kWhite;
    break;
  }
  }

  // 配置可能な場所の分だけ評価を行う
  for (int index = 0; index < static_cast<int>(placeable_place_.size()); ++index) {

    // フィールド情報をセット
    field_data_calc = field_data;
    // 配置可能座標を取得し、石を配置する
    int x_pos = placeable_place_.at(index).first;
    int y_pos = placeable_place_.at(index).second;
    field_data_calc.at(y_pos).at(x_pos) = put_stone;
    // 挟まれた石をひっくり返す
    std::pair<int, int> pos(x_pos, y_pos);
    for (int i = 0; i < static_cast<int>(turn_over_place_[pos].size()); ++i) {
      int x_pos_turn = turn_over_place_[pos].at(i).first;
      int y_pos_turn = turn_over_place_[pos].at(i).second;
      field_data_calc.at(y_pos_turn).at(x_pos_turn) = put_stone;
    }

    //---------------------------------
    //評価処理
    //---------------------------------
    int evaluation_value = CalculateBoardScore(field_weight, field_data_calc, put_stone);

    //---------------------------------
    //確定石調査 ※外周の辺のみ調査
    //---------------------------------
    int confirm_black = CountConfirmationStone(field_data_calc, SetStone::kBlack);
    int confirm_white = CountConfirmationStone(field_data_calc, SetStone::kWhite);
    int confirm_value = 0;
    // 確定石の差分を取得
    switch (stone_type) {
    case Stone::StoneType::kBlack: {
      confirm_value = confirm_black - confirm_white;
      break;
    }
    case Stone::StoneType::kWhite: {
      confirm_value = confirm_white - confirm_black;
      break;
    }
    }

    float random_value = 0.0f;
    // ランダム生成器を作成
    std::random_device seed_gen;
    std::mt19937 engine(seed_gen());
    // ランダムに取得する値の範囲(実数)
    std::uniform_real_distribution<float> dist(kLowLimit, kHighLimit);

    // ランダム値を取得する
    random_value = dist(engine);
    // 盤の評価を計算式から算出する
    evaluation_value = static_cast<int>(kEvaBoardCoeffient * random_value * kRondomMagnification * evaluation_value);
    // ランダム値を取得する
    random_value = dist(engine);
    // 確定石の評価を計算式から算出する
    confirm_value = static_cast<int>(kConfirmStoneCoeffient * (confirm_value + random_value * kRondomMagnification));

    // 盤の評価と確定石の差分からトータルの評価値を算出する
    int total_value = evaluation_value + confirm_value;

    // 評価値を出力
    std::cout << "評価値  設置座標(" << x_pos << "," << y_pos << ") : " << total_value << std::endl;
    std::cout << "確定石  黒：" << confirm_black << ", 白：" << confirm_white << std::endl;
    // 評価値の最大値と比較して今回取得した評価値の方が大きい場合、最大値を置き換える
    if (total_value > evaluation_max) {
      evaluation_max = total_value;
      x_pos_eva = x_pos;
      y_pos_eva = y_pos;
    }
  }

  // 評価値を出力
  std::cout << "☆ 評価値最大  設置座標(" << x_pos_eva << "," << y_pos_eva << ") : " << evaluation_max << std::endl;
}

/// <summary>
/// 盤上の評価計算処理
/// </summary>
/// <param name=""> フィールド情報(マスごとの重み) </param>
/// <param name=""> フィールド情報(数値版) </param>
/// <param name=""> 盤上のマスの状態 </param>
/// <returns> 評価値 </returns>
int Field::CalculateBoardScore(std::vector<std::vector<int>> field_weight,
                               std::vector<std::vector<SetStone>> field_data,
                               SetStone put_stone) {

  int evaluation_value = 0;

  // 評価値計算
  for (int y = 0; y < static_cast<int>(field_data.size()); ++y) {
    for (int x = 0; x < static_cast<int>(field_data.at(0).size()); ++x) {
      // 正負の符号
      int evaluation_sign = 0;

      // 指定座標が空マスならループ処理を続ける
      if (field_data.at(y).at(x) == SetStone::kNone) {
        continue;
      }
      // 指定座標が置いた石と同じ種類なら符号は「プラス」にする
      // 指定座標が置いた石と異なる種類なら符号は「マイナス」にする
      if (field_data.at(y).at(x) == put_stone) {
        evaluation_sign = kSignPlus;
      }
      else {
        evaluation_sign = kSignMinus;
      }

      // 各マスの重みを考慮して評価値に加算していく
      evaluation_value += evaluation_sign * field_weight.at(y).at(x);
    }
  }

  return evaluation_value;
}

/// <summary>
/// 確定石を数える
/// </summary>
/// <param name=""> フィールド情報(数値版) </param>
/// <param name=""> 盤上のマスの状態 </param>
/// <returns> 評価値 </returns>
int Field::CountConfirmationStone(std::vector<std::vector<SetStone>> field_data, SetStone check_stone) {

  // 確定石格納配列
  std::vector<std::pair<int, int>> confirm_stone_list;
  // 四隅
  std::vector<std::pair<int, int>> corner;
  corner.push_back(std::make_pair(kStartIndex, kStartIndex));   // 盤の左上
  corner.push_back(std::make_pair(kEndIndex, kStartIndex));     // 盤の右上
  corner.push_back(std::make_pair(kEndIndex, kEndIndex));       // 盤の右下
  corner.push_back(std::make_pair(kStartIndex, kEndIndex));     // 盤の左下

  // 四隅から盤上の外周の石を調査し、確定石が存在するか確認する
  for (int i = 0; i < static_cast<int>(corner.size()); ++i) {

    // 四隅の内のひとつの座標を取得
    int x_pos = corner.at(i).first;
    int y_pos = corner.at(i).second;
    // 現在探索している経路の石の種類
    std::stack<SetStone> pre_stone;
    // 探索した全経路
    std::vector<std::pair<int, int>> explored_pos;
    // 調査方向
    FieldDirection direction = FieldDirection::kUp;
    // 座標を一旦格納しておく配列
    std::vector<std::pair<int, int>> temp_stone_list;

    // 隅のマスに石がなければ処理終了
    if (field_data.at(y_pos).at(x_pos) == SetStone::kNone) {
      continue;
    }

    //-----------------------------------------------------
    //水平方向と垂直方向を分けて探索
    //-----------------------------------------------------
    //-----------------------------------------------------
    //水平方向探索
    //-----------------------------------------------------
    // 隅に置かれた石の種類を確認
    if (field_data.at(y_pos).at(x_pos) == check_stone) {
      temp_stone_list.push_back(std::make_pair(x_pos, y_pos));
    }
    pre_stone.push(field_data.at(y_pos).at(x_pos));

    // 隅のマスのX座標が「0:左端」なら右方向へ調査
    // 隅のマスのX座標が「7:右端」なら左方向へ調査
    if (x_pos == kStartIndex) {
      direction = FieldDirection::kRight;
    }
    else {
      direction = FieldDirection::kLeft;
    }

    while (true) {
      // マスを指定方向へ進める
      // 0:上方向(0, -1), 1:右方向(1, 0), 2:下方向(0, 1), 3:左方向(-1, 0)
      x_pos += direction_data_[direction].first;

      // 既に探索済みなら処理次のマスを処理
      auto iterator = std::find(explored_pos.begin(), explored_pos.end(), std::make_pair(x_pos, y_pos));
      if (iterator != explored_pos.end()) {
        break;
      }
      // 探索経路をセット
      explored_pos.push_back(std::make_pair(x_pos, y_pos));

      // 調査マスに石が置かれていない場合は処理終了
      if (field_data.at(y_pos).at(x_pos) == SetStone::kNone) {
        break;
      }
      // 現在探索している経路の石の種類を取得
      pre_stone.push(field_data.at(y_pos).at(x_pos));

      // 調査マスに調査対象の石がある場合は一時保存配列に座標を格納
      // 調査マスに調査対象ではない石がある場合、一時保存している座標を確定石とする
      if (field_data.at(y_pos).at(x_pos) == check_stone) {
        temp_stone_list.push_back(std::make_pair(x_pos, y_pos));
      }
      else {
        if (!temp_stone_list.empty()) {
          // 既に確定石として登録済みか確認し、未登録なら登録する
          for (auto pos : temp_stone_list) {
            auto iterator = std::find(confirm_stone_list.begin(), confirm_stone_list.end(), pos);
            if (iterator == confirm_stone_list.end()) {
              confirm_stone_list.push_back(pos);
            }
          }
          // 一時保管分をクリアする
          temp_stone_list.clear();
        }
      }

      // 盤の端まで調査し終えたら処理終了
      if (x_pos <= kStartIndex || x_pos >= kEndIndex) {
        break;
      }
    }

    // 一時保管配列が空ではない場合
    if (!temp_stone_list.empty()) {
      while (!pre_stone.empty()) {
        // 終了直前に格納した石の種類をひとつずつ遡る
        // 調査対象以外の石が存在する場合は一時保管分をクリアする
        if (pre_stone.top() != check_stone) {
          temp_stone_list.clear();
          break;
        }
        pre_stone.pop();
      }
      // 一時保管分の石の座標を確定石として登録
      for (auto pos : temp_stone_list) {
        // 既に確定石として登録済みか確認し、未登録なら登録する
        auto iterator = std::find(confirm_stone_list.begin(), confirm_stone_list.end(), pos);
        if (iterator == confirm_stone_list.end()) {
          confirm_stone_list.push_back(pos);
        }
      }
    }

    // 変数を初期化する
    temp_stone_list.clear();
    while (!pre_stone.empty()) {
      pre_stone.pop();
    }
    // 四隅の内のひとつの座標を再度取得
    x_pos = corner.at(i).first;
    y_pos = corner.at(i).second;

    //-----------------------------------------------------
    //垂直方向探索
    //-----------------------------------------------------
    // 隅に置かれた石の種類を確認
    if (field_data.at(y_pos).at(x_pos) == check_stone) {
      temp_stone_list.push_back(std::make_pair(x_pos, y_pos));
    }
    pre_stone.push(field_data.at(y_pos).at(x_pos));

    // 隅のマスのY座標が「0:上端」なら下方向へ調査
    // 隅のマスのY座標が「7:下端」なら上方向へ調査
    if (y_pos == kStartIndex) {
      direction = FieldDirection::kDown;
    }
    else {
      direction = FieldDirection::kUp;
    }

    while (true) {
      // マスを指定方向へ進める
      // 0:上方向(0, -1), 1:右方向(1, 0), 2:下方向(0, 1), 3:左方向(-1, 0)
      y_pos += direction_data_[direction].second;

      // 既に探索済みなら処理次のマスを処理
      auto iterator = std::find(explored_pos.begin(), explored_pos.end(), std::make_pair(x_pos, y_pos));
      if (iterator != explored_pos.end()) {
        break;
      }
      // 探索経路をセット
      explored_pos.push_back(std::make_pair(x_pos, y_pos));

      // 調査マスに石が置かれていない場合は処理終了
      if (field_data.at(y_pos).at(x_pos) == SetStone::kNone) {
        break;
      }
      // 現在探索している経路の石の種類を取得
      pre_stone.push(field_data.at(y_pos).at(x_pos));

      // 調査マスに調査対象の石がある場合は一時保存配列に座標を格納
      // 調査マスに調査対象ではない石がある場合、一時保存している座標を確定石とする
      if (field_data.at(y_pos).at(x_pos) == check_stone) {
        temp_stone_list.push_back(std::make_pair(x_pos, y_pos));
      }
      else {
        if (!temp_stone_list.empty()) {
          // 既に確定石として登録済みか確認し、未登録なら登録する
          for (auto pos : temp_stone_list) {
            auto iterator = std::find(confirm_stone_list.begin(), confirm_stone_list.end(), pos);
            if (iterator == confirm_stone_list.end()) {
              confirm_stone_list.push_back(pos);
            }
          }
          // 一時保管分をクリアする
          temp_stone_list.clear();
        }
      }
      // 盤の端まで調査し終えたら処理終了
      if (y_pos == kStartIndex || y_pos == kEndIndex) {
        break;
      }
    }

    // 一時保管配列が空ではない場合
    if (!temp_stone_list.empty()) {
      while (!pre_stone.empty()) {
        // 終了直前に格納した石の種類をひとつずつ遡る
        // 調査対象以外の石が存在する場合は一時保管分をクリアする
        if (pre_stone.top() != check_stone) {
          temp_stone_list.clear();
          break;
        }
        pre_stone.pop();
      }
      // 一時保管分の石の座標を確定石として登録
      for (auto pos : temp_stone_list) {
        // 既に確定石として登録済みか確認し、未登録なら登録する
        auto iterator = std::find(confirm_stone_list.begin(), confirm_stone_list.end(), pos);
        if (iterator == confirm_stone_list.end()) {
          confirm_stone_list.push_back(pos);
        }
      }
    }

  }

  return static_cast<int>(confirm_stone_list.size());
}

/// <summary>
/// 石の数を数える
/// </summary>
/// <param name=""> 黒色の石の数 </param>
/// <param name=""> 白色の石の数 </param>
/// <returns></returns>
void Field::CountStone(int& stone_black,int& stone_white) {

  stone_black = 0;
  stone_white = 0;

  for (int y = 0; y < static_cast<int>(field_data_stone_.size()); ++y) {
    for (int x = 0; x < static_cast<int>(field_data_stone_.at(0).size()); ++x) {

      if (field_data_stone_.at(y).at(x) == nullptr) {
        continue;
      }

      Stone::StoneType stone_type = field_data_stone_.at(y).at(x)->GetStoneType();
      switch (stone_type) {
      case Stone::StoneType::kBlack: {
        ++stone_black;
        break;
      }
      case Stone::StoneType::kWhite: {
        ++stone_white;
        break;
      }
      }

    }
  }

  // コンソールに出力
  std::cout << "【 黒色 】：" << stone_black << std::endl;
  std::cout << "【 白色 】：" << stone_white << std::endl;
}