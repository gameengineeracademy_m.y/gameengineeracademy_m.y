﻿#pragma once

#include "DxLib.h"
#include "Game/StoneBase.h"
#include "Game/GameInfo.h"
#include <array>

namespace {

  /// <summary>
  /// 石の画像の総分割数
  /// </summary>
  const int kImageStoneNum = 8;
}

/// <summary>
/// 石
/// </summary>
class Stone : public StoneBase {
public:

  /// <summary>
  /// 石の種類一覧
  /// </summary>
  enum class StoneType {
    kBlack,           // 黒
    kWhite,           // 白
    kStoneMaxIndex    // 石の種類の数
  };

  /// <summary>
  /// 処理フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,      // 初期化処理フェーズ
    kProcess,         // 処理中フェーズ
    kFinalize,        // 終了処理フェーズ
    kFinalized,       // 終了処理済みフェーズ
    kPhaseMaxIndex    // フェーズ数
  };

  /// <summary>
  /// 描画フェーズ
  /// </summary>
  enum class DrawPhaseType {
    kStart,              // 開始フェーズ
    kPlay,               // ゲーム中フェーズ
    kFinish,             // 終了フェーズ
    kDrawPhaseMaxIndex   // フェーズ項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> ストーンID </param>
  /// <returns></returns>
  Stone(int);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~Stone();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float);

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render();

  /// <summary>
  /// 画像をセットする
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandle(std::array<int, kImageStoneNum> stone_handle) { stone_handle_ = stone_handle; }

  /// <summary>
  /// 画像を取得する
  /// </summary>
  /// <param name="index"> インデックス </param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandleStone(int index) { return stone_handle_.at(index); }

  /// <summary>
  /// X座標をセットする
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <returns></returns>
  void SetPositionX(int x_pos) { x_pos_ = x_pos; }

  /// <summary>
  /// X座標を設定する
  /// </summary>
  /// <param name=""></param>
  /// <returns> X座標 </returns>
  int GetPositionX() { return x_pos_; }

  /// <summary>
  /// Y座標をセットする
  /// </summary>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  void SetPositionY(int y_pos) { y_pos_ = y_pos; }

  /// <summary>
  /// Y座標を設定する
  /// </summary>
  /// <param name=""></param>
  /// <returns> Y座標 </returns>
  int GetPositionY() { return y_pos_; }

  /// <summary>
  /// 画像の幅を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 画像の幅 </returns>
  int GetImageWidth() { return image_width_; }

  /// <summary>
  /// 画像の高さを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 画像の高さ </returns>
  int GetImageHeight() { return image_height_; }

  /// <summary>
  /// 配置済みとする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetPlaced() { is_placed_ = true; }

  /// <summary>
  /// 配置済みを解除する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetUnplaced() { is_placed_ = false; }

  /// <summary>
  /// 配置済みかどうかチェック
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  bool IsPlaced() { return is_placed_ == true; }

  /// <summary>
  /// 石の種類をセットする
  /// </summary>
  /// <param name=""> 石の種類 </param>
  /// <returns></returns>
  void SetStoneType(StoneType);

  /// <summary>
  /// 石の種類を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 石の種類 </returns>
  StoneType GetStoneType() { return stone_type_; }

  /// <summary>
  /// 描画処理開始
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetStartDrawing() { is_drawing_ = true; }

  /// <summary>
  /// 描画処理終了
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetFinishDrawing() { is_drawing_ = false; }

  /// <summary>
  /// 描画フェーズを変更する
  /// </summary>
  /// <param name=""> 描画フェーズ </param>
  /// <returns></returns>
  void ChangeDrawPhase(DrawPhaseType draw_phase) { current_draw_phase_ = draw_phase; };


private:

  /// <summary>
  /// フェーズの変更
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType);

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""> 石の画像ハンドル </param>
  /// <returns> true:処理終了, false:処理継続 </returns>
  bool Initialize();

  /// <summary>
  /// 処理中
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理継続 </returns>
  bool Process();

  /// <summary>
  /// 終了処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理継続 </returns>
  bool Finalize();


private:

  /// <summary>
  /// 石の種類
  /// </summary>
  StoneType stone_type_;

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 現在の描画フェーズ
  /// </summary>
  DrawPhaseType current_draw_phase_;

  /// <summary>
  /// 石の画像
  /// </summary>
  std::array<int, kImageStoneNum> stone_handle_;

  /// <summary>
  /// X座標
  /// </summary>
  int x_pos_;

  /// <summary>
  /// Y座標
  /// </summary>
  int y_pos_;

  /// <summary>
  /// 画像の幅
  /// </summary>
  int image_width_;

  /// <summary>
  /// 画像の高さ
  /// </summary>
  int image_height_;

  /// <summary>
  /// 配置済み
  /// </summary>
  bool is_placed_;

  /// <summary>
  /// 描画フラグ
  /// </summary>
  bool is_drawing_;

  /// <summary>
  /// 描画インデックス
  /// </summary>
  int drawing_index_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// 描画 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 描画時のX座標
  /// </summary>
  int x_pos_render_;

  /// <summary>
  /// 描画時のY座標
  /// </summary>
  int y_pos_render_;
};