﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "System/Common.h"
#include "Game/Stone.h"
#include "Game/GameModeEventInterface.h"
#include <unordered_map>
#include <array>

/// <summary>
/// ゲームモード
/// </summary>
/// <remarks>
/// 主にターンの管理を行うクラス
/// </remarks>
class GameMode : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType{
    kInitialize,           // 初期設定フェーズ
    kProcess,              // 処理中フェーズ
    kFinish,               // 終了処理フェーズ
    kFinished,             // 終了処理済みフェーズ
    kPhaseMaxIndex         // フェーズ数
  };

  /// <summary>
  /// ターンフェーズの種類
  /// </summary>
  enum class TurnPhase {
    kNone,                 // 何もしない
    kThink,                // 思考中
    kPutStone,             // 石を置く
    kTurnOverStone,        // 石をひっくり返す
    kFinishProcess,        // 石操作終了
    kSwitchTurn,           // ターンを切り替える
    kCheckPlaceablePlace,  // 配置可能場所確認
    kFinishGame,           // ゲーム終了
    kDisplayTurnPanel,     // ターン表示を切り替える
    kShowDisplay,          // 画面への各種描画処理
    kTurnMaxIndex          // ターンフェーズの種類の数
  };

  /// <summary>
  /// ターンの種類
  /// </summary>
  enum class PlayerTurn {
    kPlayer,            // プレイヤー
    kCpu,               // CPU
    kSkip,              // スキップ
    kPlayerTurnMaxIndex // ターンの種類の数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> ゲームモードイベントインターフェース </param>
  /// <returns></returns>
  GameMode(GameModeEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~GameMode();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float);

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render();

  /// <summary>
  /// 現在のターンフェーズを変更する
  /// </summary>
  /// <param name=""> ターンフェーズの種類 </param>
  /// <returns></returns>
  void ChangeTurnPhase(TurnPhase);

  /// <summary>
  /// プレイヤー情報の設定
  /// </summary>
  /// <param name=""> プレイヤーの種類 </param>
  /// <param name=""> 石の種類 </param>
  /// <returns></returns>
  void SetPlayerData(PlayerTurn, Stone::StoneType);

  /// <summary>
  /// ターンの設定
  /// </summary>
  /// <param name=""> プレイヤーの種類 </param>
  /// <returns></returns>
  void SetPlayerTurn(PlayerTurn);

  /// <summary>
  /// 現在のフェーズが「思考中」かどうか
  /// </summary>
  /// <param name="">  </param>
  /// <returns> true:思考中, false:思考中以外 </returns>
  bool GetCurrentPhaseThink() { return current_turn_phase_ == TurnPhase::kThink; }

  /// <summary>
  /// 現在のフェーズが「石を返す」かどうか
  /// </summary>
  /// <param name="">  </param>
  /// <returns> true:石を返す, false:石を返す以外 </returns>
  bool GetCurrentPhaseTurnOver() { return current_turn_phase_ == TurnPhase::kTurnOverStone; }

  /// <summary>
  /// 現在のターンを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> 現在のターン </returns>
  PlayerTurn GetPlayerTurn() { return current_turn_; }

  /// <summary>
  /// プレイヤーが使用している石の種類を取得
  /// </summary>
  /// <param name="player_turn"> プレイヤーターン </param>
  /// <returns> 石の種類 </returns>
  Stone::StoneType GetStoneType(PlayerTurn player_turn) { return player_data_[player_turn]; }

  /// <summary>
  /// グラフィックハンドルをセット
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandle(int cursor_handle) { turn_handle_ = cursor_handle; }

  /// <summary>
  /// グラフィックハンドルを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  int GetGraphicHandle() { return turn_handle_; }

  /// <summary>
  /// 画像のサイズを取得する
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns></returns>
  void SetImageSize(int);


private:

  /// <summary>
  /// 初期設定フェーズ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Initialize();

  /// <summary>
  /// 処理中フェーズ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  bool Process();

  /// <summary>
  /// 終了処理フェーズ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Finish();

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangePhaseType(PhaseType);

  /// <summary>
  /// 現在のターンフェーズが「ターンパネル切り替え」かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:ターンパネル切り替えフェーズ, false:それ以外 </returns>
  bool IsTurnPhasekDisplayTurnPanel() { return current_turn_phase_ == TurnPhase::kDisplayTurnPanel; }

  /// <summary>
  /// マニュアルモード処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OperateManual();

  /// <summary>
  /// オートモード処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OperateAuto();


private:

  /// <summary>
  /// ゲームモードイベントインターフェース
  /// </summary>
  GameModeEventInterface& game_mode_event_interface_;

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 現在のターンフェーズ
  /// </summary>
  TurnPhase current_turn_phase_;

  /// <summary>
  /// 現在のターン
  /// </summary>
  PlayerTurn current_turn_;

  /// <summary>
  /// ターン情報 ターンと石の種類
  /// </summary>
  std::unordered_map<PlayerTurn, Stone::StoneType> player_data_;

  /// <summary>
  /// スキップ回数
  /// </summary>
  int skip_count_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// スキップターン表示 グラフィックハンドル
  /// </summary>
  int turn_handle_;

  /// <summary>
  /// 画像の幅
  /// </summary>
  int image_width_;

  /// <summary>
  /// 画像の高さ
  /// </summary>
  int image_height_;

  /// <summary>
  /// 描画 横方向移動距離
  /// </summary>
  int move_x_pos_;

  /// <summary>
  /// 描画 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 自動で進める手数
  /// </summary>
  int auto_step_advance_;

  /// <summary>
  /// 現在の手数
  /// </summary>
  int current_step_;
};