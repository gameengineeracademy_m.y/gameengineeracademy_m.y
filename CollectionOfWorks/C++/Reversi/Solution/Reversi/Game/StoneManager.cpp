﻿#include "Game/StoneManager.h"

namespace {

  /// <summary>
  /// 石の最大枚数
  /// </summary>
  const int kStoneMaxNum = 64;

  /// <summary>
  /// 石の最小枚数
  /// </summary>
  const int kStoneMinNum = 0;

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 先頭インデックス
  /// </summary>
  const int kInitialIndex = 0;

  /// <summary>
  /// 石の画像パス
  /// </summary>
  const char* kImagePathStone = "Assets/Image/stone.png";

  /// <summary>
  /// 石の画像の総分割数
  /// </summary>
  const int kDivTotalNum = 8;

  /// <summary>
  /// 石の画像の横方向の分割数
  /// </summary>
  const int kDivXNum = 2;

  /// <summary>
  /// 石の画像の縦方向の分割数
  /// </summary>
  const int kDivYNum = 4;

  /// <summary>
  /// 画像分割後の1枚あたりの幅
  /// </summary>
  const int kWidth = 64;

  /// <summary>
  /// 画像分割後の1枚あたりの高さ
  /// </summary>
  const int kHeight = 64;

  /// <summary>
  /// 累積時間 リセット
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 描画待機時間
  /// </summary>
  const float kDrawWaitTime = 0.1f;

  /// <summary>
  /// 画像読み込み時のエラー値
  /// </summary>
  const int kLoadError = -1;

  /// <summary>
  /// サイズをインデックスに変換
  /// </summary>
  const int kMinusValue = 1;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> ストーンマネージャイベントインターフェース </param>
/// <returns></returns>
StoneManager::StoneManager(StoneManagerEventInterface& stone_manager_event_interface)
  : Task(TaskId::kStoneManager)
  , stone_manager_event_interface_(stone_manager_event_interface)
  , stone_list_()
  , unplaced_stone_()
  , placed_stone_()
  , accumulate_time_(0.0f) {

  // コンソールに出力
  std::cout << "StoneManager コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
StoneManager::~StoneManager() {

  // コンソールに出力
  std::cout << "~StoneManager デストラクタ" << std::endl;
}

/// <summary>
/// 石を積む
/// </summary>
/// <param name=""> 石 </param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool StoneManager::AddStone(Stone* stone) {

  // 石が存在しない場合は処理終了
  if (stone == nullptr) {
    return false;
  }

  // 石を降ろす設定をリセットする
  stone->SetResetReleaseStone();
  // ストーンIDを取得
  int stone_id = stone->GetStoneId();
  // 石が既にリストに登録済みか確認し、登録済みなら処理終了
  if (stone_list_.find(stone_id) != stone_list_.end()) {
    return false;
  }
  // 石リストに追加
  stone_list_[stone_id] = stone;
  
  return true;
}

/// <summary>
/// 石を降ろす
/// </summary>
/// <param name=""> ストーンID </param>
/// <returns> 石 </returns>
Stone* StoneManager::ReleaseStone(int stone_id) {

  // 石リストに存在する石か確認
  if (stone_list_.find(stone_id) == stone_list_.end()) {
    return nullptr;
  }

  // ストーンIDから石を取得
  Stone* stone = stone_list_[stone_id];
  // 石を降ろす設定
  stone->SetReleaseStone();
  // 石リストから石を降ろす
  stone_list_.erase(stone_id);
  
  return stone;
}

/// <summary>
/// ストーンの1フレームの処理を実行する
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void StoneManager::Update(float process_time) {

  // 累積時間を加算
  accumulate_time_ += process_time;
  if (accumulate_time_ >= kDrawWaitTime) {
    // 累積時間をリセット
    accumulate_time_ = kResetTime;
    // 順番に石を返す処理
    stone_manager_event_interface_.OnTurnOverStone();
  }

  // 各石の描画処理を行う
  for (auto stone : stone_list_) {
    bool is_release = stone.second->IsReleaseStone();
    if (!is_release) {
      stone.second->Update(process_time);
    }
  }
}

/// ストーンの1フレームの描画処理を実行する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void StoneManager::Render() {

  for (auto stone : stone_list_) {
    bool is_release = stone.second->IsReleaseStone();
    if (!is_release) {
      stone.second->Render();
    }
  }
}

/// <summary>
/// 石を生成する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
bool StoneManager::CreateStone() {

  int index = kStoneMaxNum;
  std::array<int, static_cast<int>(kDivTotalNum)> graphic_handle;
  // 画像のロード
  int result = LoadDivGraph(kImagePathStone, kDivTotalNum, kDivXNum, kDivYNum, kWidth, kHeight, graphic_handle.data());
  if (result == kLoadError) {
    std::cout << "StoneManager CreateStone：画像読み込み失敗" << std::endl;
    return false;
  }

  while (index > kStoneMinNum) {
    // デクリメント
    --index;
    // 石の種類をセット
    Stone::StoneType stone_type = static_cast<Stone::StoneType>(index % kHalfValue);
    // 石の生成
    Stone* stone = new Stone(index);
    if (stone == nullptr) {
      std::cout << "StoneManager 石の生成失敗：" << index << std::endl;
      return false;
    }
    // 石の解放設定をリセットする
    stone->SetResetReleaseStone();
    // 石の色をセットする
    stone->SetStoneType(stone_type);
    // グラフィックハンドルを渡す
    stone->SetGraphicHandle(graphic_handle);
    // ストーンマネージャにタスクを積む
    AddStone(stone);
    // 石を管理スタックに入れる
    unplaced_stone_[stone_type].push(stone);
  }

  return true;
}

/// <summary>
/// 石を破棄する
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
void StoneManager::DisposeStone() {

  // ストーンリストに要素がない場合は処理終了
  if (stone_list_.size() == kStoneMinNum) {
    return;
  }
  // 画像リソースの破棄
  for (int i = 0; i < kDivTotalNum; ++i) {
    DeleteGraph(stone_list_[kInitialIndex]->GetGraphicHandleStone(i));     // 石
  }

  for (int i = 0; i < kStoneMaxNum; ++i) {
    // 指定インデックスの要素がない場合、継続処理
    if (stone_list_.find(i) == stone_list_.end()) {
      continue;
    }
    // ストーンが存在しない場合は継続処理
    if (stone_list_[i] == nullptr) {
      continue;
    }

    // 石をリストから降ろしたことにする
    stone_list_[i]->SetReleaseStone();
    // 石をストーンマネージャから降ろす
    int stone_id = stone_list_[i]->GetStoneId();
    Stone* release_stone = ReleaseStone(stone_id);

    // 石を破棄する
    if (release_stone != nullptr) {
      delete release_stone;
      release_stone = nullptr;
    }
  }
}

/// <summary>
/// 配置済みの石をセットする
/// </summary>
/// <param name=""> 石の種類 </param>
/// <returns></returns>
void StoneManager::SetPlacedStone(Stone::StoneType stone_type) {

  // 未配置の石管理スタックから先頭の石を取得する
  Stone* stone = unplaced_stone_[stone_type].top();
  // 先頭の石と設置した際の石の種類を配置済み管理スタックに格納
  std::pair<Stone*, Stone::StoneType> stone_data(stone, stone_type);
  placed_stone_.push(stone_data);
  // 未配置の石管理スタックから先頭の石を削除する
  unplaced_stone_[stone_type].pop();
}

/// <summary>
/// 石の描画フェーズを「終了フェーズ」に変更する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void StoneManager::SetStoneDrawPhaseFinish() {

  for (auto stone : stone_list_) {
    bool is_release = stone.second->IsReleaseStone();
    if (!is_release) {
      stone.second->ChangeDrawPhase(Stone::DrawPhaseType::kFinish);
    }
  }
}