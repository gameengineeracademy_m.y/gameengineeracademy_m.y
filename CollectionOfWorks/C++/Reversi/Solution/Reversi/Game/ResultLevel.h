﻿#pragma once

#include "DxLib.h"
#include "System/Level.h"
#include "System/MenuController.h"
#include "System/BackScreen.h"
#include "System/SoundManager.h"
#include "Game/Field.h"
#include "Game/StoneManager.h"
#include "Game/GameInfo.h"
#include "Game/ResultInfoUi.h"
#include "Game/ResultLevelEventInterface.h"
#include <unordered_map>
#include <array>
#include <queue>
#include <stack>

/// <summary>
/// リザルトレベル
/// </summary>
/// <remarks>
/// ゲームの結果を扱う画面のタスク
/// </remarks>
class ResultLevel : public Level, public ResultLevelEventInterface {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kPrepareWait,       // 準備待機フェーズ
    kPreparate,         // 準備フェーズ
    kProcessWait,       // 処理待機フェーズ
    kStoneDraw,         // 石描画処理中フェーズ
    kResultDrawSwitch,  // 結果描画切り替えフェーズ
    kResultDraw,        // 結果描画フェーズ
    kFinishWaiting,     // 終了待機フェーズ
    kFinishSwitch,      // 終了切り替えフェーズ
    kFinish,            // 終了処理フェーズ
    kFinished,          // 終了処理済みフェーズ
    kPhaseMaxIndex      // フェーズ項目数
  };

  /// <summary>
  /// 描画フェーズ
  /// </summary>
  enum class DrawPhaseType {
    kStoneDrawing,       // 石描画フェーズ
    kResultDrawing,      // 結果描画フェーズ
    kDrawPhaseMaxIndex   // フェーズ項目数
  };

  /// <summary>
  /// 終了フェーズの種類
  /// </summary>
  enum class FinalizePhaseType {
    kUnloadTask,         // タスクを降ろすフェーズ
    kDisposeTask,        // タスクを破棄するフェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> タスクID </param>
  /// <param name=""> タスクマネージャ </param>
  /// <returns></returns>
  ResultLevel(TaskId, TaskManager&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~ResultLevel();

  /// <summary>
  /// マウスの左クリックが押された時のイベント
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  void OnPushMouseLeftButton(int, int) override;

  /// <summary>
  /// マウスの座標取得イベント
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <param name=""> 押されているかどうか </param>
  /// <returns></returns>
  void OnGetMousePosition(int, int, bool) override;

  /// <summary>
  /// 終了処理通知
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnFinishLevelEvent() override;


protected:

  /// <summary>
  /// 初期化処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool InitializePhase(float) override;

  /// <summary>
  /// 毎フレームの処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool UpdatePhase(float) override;

  /// <summary>
  /// 毎フレームの描画処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void RenderPhase() override;

  /// <summary>
  /// 終了処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool FinishPhase(float) override;


private:

  /// <summary>
  /// 準備処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Preparate();

  /// <summary>
  /// 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  bool Process();

  /// <summary>
  /// 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Finish();

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; };

  /// <summary>
  /// 描画フェーズを終了フェーズに遷移
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeCurrentDrawPhase(DrawPhaseType phase_type) { draw_phase_ = phase_type; };

  /// <summary>
  /// フィールド処理 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeField();

  /// <summary>
  /// ストーンマネージャ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeStoneManager();

  /// <summary>
  /// メニューコントローラ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeMenuController();

  /// <summary>
  /// HUD情報 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeInfoUi();

  /// <summary>
  /// 背景 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeBackScreen();

  /// <summary>
  /// サウンドマネージャ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeSoundManager();

  /// <summary>
  /// バトル結果をセットする
  /// </summary>
  /// <param name=""> バトル結果 </param>
  /// <returns></returns>
  void SetBattleResult(GameInfo::BattleResult battle_result) { battle_result_ = battle_result; };

  /// <summary>
  /// バトル結果を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> バトル結果 </returns>
  GameInfo::BattleResult GetBattleResult() { return battle_result_; };

  /// <summary>
  /// サウンド生成
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool CreateSound();


private:

  /// <summary>
  /// 累積処理時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// メニューコントローラ
  /// </summary>
  MenuController* menu_controller_;

  /// <summary>
  /// フィールド
  /// </summary>
  Field* field_;

  /// <summary>
  /// ストーンマネージャ
  /// </summary>
  StoneManager* stone_manager_;

  /// <summary>
  /// HUD情報
  /// </summary>
  ResultInfoUi* result_info_ui_;

  /// <summary>
  /// 背景
  /// </summary>
  BackScreen* back_screen_;

  /// <summary>
  /// サウンドマネージャ
  /// </summary>
  SoundManager* sound_manager_;

  /// <summary>
  /// フェーズの種類
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 描画フェーズの種類
  /// </summary>
  DrawPhaseType draw_phase_;

  /// <summary>
  /// 終了フェーズの種類
  /// </summary>
  FinalizePhaseType finalize_phase_;

  /// <summary>
  /// 文字描画フラグ
  /// </summary>
  bool is_display;

  /// <summary>
  /// バトル結果
  /// </summary>
  GameInfo::BattleResult battle_result_;

  /// <summary>
  /// 黒色の石格納キュー
  /// </summary>
  std::queue<Stone*> black_stone_;

  /// <summary>
  /// 白色の石格納スタック
  /// </summary>
  std::stack<Stone*> white_stone_;
};