﻿#include "Game/ResultLevel.h"

namespace {

  /// <summary>
  /// 累積時間のリセット値
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// タイトルレベルへ遷移する時間 [sec]
  /// </summary>
  const float kWaitTimeSecond = 1.0f;

  /// <summary>
  /// 画面情報 出力位置 Y座標
  /// </summary>
  const int kInfoOutputPosY = 350;

  /// <summary>
  /// 文字の色
  /// </summary>
  const int kStringForeColor = GetColor(0, 0, 0);     // 黒色

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 背景画像
  /// </summary>
  const char* kImagePathBackScreen = "Assets/Image/backscreen.png";

  /// <summary>
  /// レベル遷移後の待機時間
  /// </summary>
  const float kTransitionLevelWaitTime = 0.3f;

  /// <summary>
  /// 石を表示する時間の間隔
  /// </summary>
  const float kStoneDisplayTime = 0.07f;

  /// <summary>
  /// フェーズ遷移待機時間
  /// </summary>
  const float kTransitionPhaseWaitTime = 0.7f;

  /// <summary>
  /// 表示文字列の描画有無間隔
  /// </summary>
  const float kDisplayInterval = 0.6f;

  /// <summary>
  /// 文字列表示位置 高さ
  /// </summary>
  const int kStringPosY = 55;

  /// <summary>
  /// 盤上の先頭インデックス
  /// </summary>
  const int kStartIndex = 0;

  /// <summary>
  /// 盤上の終端インデックス
  /// </summary>
  const int kEndIndex = 7;

  /// <summary>
  /// 石の総数(最終インデックス)
  /// </summary>
  const int kStoneTotalNumIndex = 63;

  /// <summary>
  /// 勝利画像取得先パス
  /// </summary>
  const char* kImagePathWinner = "Assets/Image/result_winner.png";

  /// <summary>
  /// 敗北画像取得先パス
  /// </summary>
  const char* kImagePathLose = "Assets/Image/result_lose.png";

  /// <summary>
  /// 引分画像取得先パス
  /// </summary>
  const char* kImagePathDraw = "Assets/Image/result_draw.png";

  /// <summary>
  /// HUD用黒色の石の画像取得先パス
  /// </summary>
  const char* kImagePathStoneBlack = "Assets/Image/stone_black.png";

  /// <summary>
  /// HUD用白色の石の画像取得先パス
  /// </summary>
  const char* kImagePathStoneWhite = "Assets/Image/stone_white.png";

  /// <summary>
  /// 盤枠分割後の1枚あたりの幅
  /// </summary>
  const int kWidth = 64;

  /// <summary>
  /// 盤枠分割後の1枚あたりの高さ
  /// </summary>
  const int kHeight = 64;

  /// <summary>
  /// 画像読み込み時のエラー値
  /// </summary>
  const int kLoadError = -1;

  /// <summary>
  /// 背景 表示倍率
  /// </summary>
  const float kMagnification = 2.5f;

  /// <summary>
  /// 背景 表示角度 [rad]
  /// </summary>
  const float kAngle = 0.0f;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSize = 30;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 8;

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Meiryo UI";

  /// <summary>
  /// メッセージテキスト
  /// </summary>
  const char* kMessageText = "Please Click Button !!";

  /// <summary>
  /// 透過率 起動時調整値
  /// </summary>
  const int kAlphaAdjastStart = 15;

  /// <summary>
  /// 透過率 終了時調整値
  /// </summary>
  const int kAlphaAdjastFinish = 10;

  /// <summary>
  /// 石を置いた時の音 ファイルパス
  /// </summary>
  const char* kSoundPathSetStone = "Assets/Sound/reversi_setstone.mp3";

  /// <summary>
  /// 拍手 ファイルパス
  /// </summary>
  const char* kSoundPathHandClap = "Assets/Sound/hand_clap.mp3";

  /// <summary>
  /// 勝利サウンド ファイルパス
  /// </summary>
  const char* kSoundPathWinner = "Assets/Sound/winner.mp3";

  /// <summary>
  /// 敗北サウンド ファイルパス
  /// </summary>
  const char* kSoundPathLoser = "Assets/Sound/loser.mp3";

  /// <summary>
  /// リザルトレベルBGM ファイルパス
  /// </summary>
  const char* kSoundPathBgm = "Assets/Sound/hello_world.mp3";

  /// <summary>
  /// 1000 単位変換に使用
  /// </summary>
  const float kThousand = 1000.0f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> タスクID </param>
/// <param name=""> タスクマネージャ </param>
/// <returns></returns>
ResultLevel::ResultLevel(TaskId task_id, TaskManager& task_manager)
  : Level(task_id, task_manager)
  , accumulate_time_(0.0f)
  , menu_controller_(nullptr)
  , field_(nullptr)
  , stone_manager_(nullptr)
  , result_info_ui_(nullptr)
  , back_screen_(nullptr)
  , sound_manager_(nullptr)
  , current_phase_(PhaseType::kPrepareWait)
  , draw_phase_(DrawPhaseType::kStoneDrawing)
  , finalize_phase_(FinalizePhaseType::kUnloadTask)
  , is_display(false)
  , battle_result_(GameInfo::BattleResult::kDraw)
  , black_stone_()
  , white_stone_() {

  // コンソールに出力
  std::cout << "ResultLevel コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ResultLevel::~ResultLevel() {

  // コンソールに出力
  std::cout << "~ResultLevel デストラクタ" << std::endl;
}

/// <summary>
/// 初期化処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool ResultLevel::InitializePhase(float process_time) {

  bool is_success = false;

  // 処理時間をリセットしておく
  accumulate_time_ = kResetTime;

  // フィールド処理 初期設定
  is_success = InitializeField();
  if (!is_success) {
    return false;
  }

  // ストーンマネージャ 初期設定
  is_success = InitializeStoneManager();
  if (!is_success) {
    return false;
  }

  // メニューコントローラ 初期設定
  is_success = InitializeMenuController();
  if (!is_success) {
    return false;
  }

  // HUD情報 初期設定
  is_success = InitializeInfoUi();
  if (!is_success) {
    return false;
  }

  // 背景 初期設定
  is_success = InitializeBackScreen();
  if (!is_success) {
    return false;
  }

  // サウンドマネージャ 初期設定
  is_success = InitializeSoundManager();
  if (!is_success) {
    return false;
  }

  return true;
}

/// <summary>
/// 毎フレームの処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool ResultLevel::UpdatePhase(float process_time) {

  // 処理時間を累積していく
  accumulate_time_ += process_time;

  switch (current_phase_) {
  case PhaseType::kPrepareWait: {
    //---------------------------
    //準備待機フェーズ
    //---------------------------
    bool check_finish = field_->IsProcess();
    if (check_finish) {
      // 現在のフェーズを「処理中」に変更する
      ChangeCurrentPhase(PhaseType::kPreparate);
    }
    break;
  }
  case PhaseType::kPreparate: {
    //---------------------------
    //準備待機フェーズ
    //---------------------------
    // 累積時間をリセットする
    accumulate_time_ = kResetTime;
    // 準備処理
    Preparate();
    // 現在のフェーズを「処理待機」に変更する
    ChangeCurrentPhase(PhaseType::kProcessWait);
    break;
  }
  case PhaseType::kProcessWait: {
    //---------------------------
    //処理待機フェーズ
    //---------------------------
    if (accumulate_time_ >= kTransitionLevelWaitTime) {
      // 現在のフェーズを「石描画」に変更する
      ChangeCurrentPhase(PhaseType::kStoneDraw);
    }
    break;
  }
  case PhaseType::kStoneDraw:
  case PhaseType::kResultDrawSwitch:
  case PhaseType::kResultDraw:
  case PhaseType::kFinishWaiting:
  case PhaseType::kFinishSwitch: {
    //---------------------------
    //描画処理中/終了待機フェーズ
    //---------------------------
    bool is_finish = Process();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更する
      ChangeCurrentPhase(PhaseType::kFinish);
    }
    break;
  }
  case PhaseType::kFinish: {
    //---------------------------
    //終了処理フェーズ
    //---------------------------
    Finish();
    // 現在のフェーズを「終了処理済み」に変更する
    ChangeCurrentPhase(PhaseType::kFinished);
    break;
  }
  }

  return false;
}

/// <summary>
/// 毎フレームの描画処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ResultLevel::RenderPhase() {

  switch (draw_phase_) {
  case DrawPhaseType::kResultDrawing: {
    break;
  }
  default: {
    return;
  }
  }

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // マップ中央の位置を取得(マップ表示時の調整値)
  int x_center = game_info->GetCenterPointX();
  int y_center = game_info->GetCenterPointY();

  // 文字列の描画処理
  SetFontSize(kFontSize);
  SetFontThickness(kFontWidth);
  ChangeFont(kFontName);
  ChangeFontType(DX_FONTTYPE_ANTIALIASING);

  // 一定時間ごとに文字列を描画する
  if (is_display) {
    // 文字サイズ(幅)を取得し、文字列の描画処理
    int string_width_ex = GetDrawStringWidth(kMessageText, static_cast<int>(strlen(kMessageText)));
    DrawString(x_center - (string_width_ex / kHalfValue), kStringPosY, kMessageText, kStringForeColor);
  }
}

/// <summary>
/// 終了処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool ResultLevel::FinishPhase(float process_time) {

  switch (finalize_phase_) {
  case FinalizePhaseType::kUnloadTask: {
    //-------------------------------
    //タスクを降ろすフェーズ
    //-------------------------------
    // 石の破棄を行う
    if (stone_manager_ != nullptr) {
      stone_manager_->DisposeStone();
    }
    // 音の破棄を行う
    if (sound_manager_ != nullptr) {
      sound_manager_->DisposeSound();
    }

    // 画像のリソースを破棄
    if (back_screen_ != nullptr) {
      DeleteGraph(back_screen_->GetGraphicHandle());       // 背景
    }
    if (field_ != nullptr) {
      DeleteGraph(field_->GetGraphicHandleBoard());              // 盤面
      DeleteGraph(field_->GetGraphicHandleFrame(kOuterFrame));   // 盤枠
      DeleteGraph(field_->GetGraphicHandleFrame(kCornerFrame));  // 盤枠
    }
    if (result_info_ui_ != nullptr) {
      DeleteGraph(result_info_ui_->GetGraphicHandleStoneBlack()); // 黒色の石
      DeleteGraph(result_info_ui_->GetGraphicHandleStoneWhite()); // 白色の石
      DeleteGraph(result_info_ui_->GetGraphicHandleWinner());     // リザルト勝利
      DeleteGraph(result_info_ui_->GetGraphicHandleLose());       // リザルト敗北
      DeleteGraph(result_info_ui_->GetGraphicHandleDraw());       // リザルト引分
    }

    TaskId task_id;
    Task* release_task = nullptr;

    // 背景をタスクマネージャから降ろす
    if (back_screen_ != nullptr) {
      task_id = back_screen_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // フィールド処理をタスクマネージャから降ろす
    if (field_ != nullptr) {
      task_id = field_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // ストーンマネージャをタスクマネージャから降ろす
    if (stone_manager_ != nullptr) {
      task_id = stone_manager_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // メニューコントローラをタスクマネージャから降ろす
    if (menu_controller_ != nullptr) {
      task_id = menu_controller_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // HUD情報をタスクマネージャから降ろす
    if (result_info_ui_ != nullptr) {
      task_id = result_info_ui_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // サウンドマネージャをタスクマネージャから降ろす
    if (sound_manager_ != nullptr) {
      task_id = sound_manager_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }

    // 終了フェーズをタスク破棄フェーズに変更
    finalize_phase_ = FinalizePhaseType::kDisposeTask;
    return false;

    break;
  }
  case FinalizePhaseType::kDisposeTask: {
    //-------------------------------
    //タスクを破棄するフェーズ
    //-------------------------------
    if (sound_manager_ != nullptr) {
      delete sound_manager_;
      sound_manager_ = nullptr;
    }
    if (result_info_ui_ != nullptr) {
      delete result_info_ui_;
      result_info_ui_ = nullptr;
    }
    if (menu_controller_ != nullptr) {
      delete menu_controller_;
      menu_controller_ = nullptr;
    }
    if (stone_manager_ != nullptr) {
      delete stone_manager_;
      stone_manager_ = nullptr;
    }
    if (field_ != nullptr) {
      delete field_;
      field_ = nullptr;
    }
    if (back_screen_ != nullptr) {
      delete back_screen_;
      back_screen_ = nullptr;
    }
    break;
  }
  }

  return true;
}

/// <summary>
/// マウスの座標取得イベント
/// </summary>
/// <param name="x_pos"> X座標 </param>
/// <param name="y_pos"> Y座標 </param>
/// <param name="is_push"> 押されているかどうか </param>
/// <returns></returns>
void ResultLevel::OnGetMousePosition(int x_pos, int y_pos, bool is_push) {

  // 現在のフェーズが「終了待機」以外なら処理終了
  if (current_phase_ != PhaseType::kFinishWaiting) {
    return;
  }
  // ボタン上にマウスカーソルが存在するか確認
  bool is_exists = result_info_ui_->CheckMouseCursorPosition(x_pos, y_pos, is_push);
  //if (is_exists) {
  //  std::cout << "ボタン上あり！：座標(" << x_pos << "," << y_pos << ")" << std::endl;
  //}
  //else {
  //  std::cout << "ボタン上なし：座標(" << x_pos << "," << y_pos << ")" << std::endl;
  //}
}

/// <summary>
/// マウスの左クリックが押された時のイベント
/// </summary>
/// <param name="x_pos"> X座標 </param>
/// <param name="y_pos"> Y座標 </param>
/// <returns></returns>
void ResultLevel::OnPushMouseLeftButton(int x_pos, int y_pos) {

  // 現在のフェーズが「終了待機」以外なら処理終了
  if (current_phase_ != PhaseType::kFinishWaiting) {
    return;
  }

  // ボタンの種類を取得
  ResultInfoUi::ButtonType button_type = result_info_ui_->GetExistsCursorButton();

  if (button_type == ResultInfoUi::ButtonType::kNone) {
    std::cout << "ResultLevel OnPushMouseLeftButton 押されたボタン：なし" << std::endl;
    return;
  }

  switch (button_type) {
  case ResultInfoUi::ButtonType::kTitleTransition: 
  case ResultInfoUi::ButtonType::kBattleTransition: {

    // 現在のフェーズを「終了切り替えフェーズ」に変更する
    ChangeCurrentPhase(PhaseType::kFinishSwitch);

    // リザルトHUD情報のフェーズを「終了フェーズ」に変更
    result_info_ui_->ChangeDrawPhase(ResultInfoUi::DrawPhaseType::kFinish);
    // 石の描画フェーズを「終了フェーズ」に変更
    stone_manager_->SetStoneDrawPhaseFinish();
    // フィールド処理の描画フェーズを「終了フェーズ」に変更
    field_->ChangeDrawPhase(Field::DrawPhaseType::kFinish);
    // 背景の現在のフェーズを「終了フェーズ」に変更
    // 背景の描画フェーズを「終了フェーズ」に変更
    back_screen_->ChangePhaseType(BackScreen::PhaseType::kFinalize);
    back_screen_->ChangeDrawPhase(BackScreen::DrawPhaseType::kFinish);

    break;
  }
  }

  std::cout << "ResultLevel OnPushMouseLeftButton 押されたボタン：" << static_cast<int>(button_type) << std::endl;

}

/// <summary>
/// 準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ResultLevel::Preparate() {
  //---------------------------------
  //石の初期配置場所の設定処理
  //---------------------------------
  int x_pos = 0;
  int y_pos = 0;
  int stone_total_number = stone_manager_->GetStoneTotalNumber();
  // 盤の上段または下段に到達したかどうか
  bool is_frame_reached = true;
  // 移動する方向と移動する座標の大きさを格納
  std::unordered_map<Field::FieldDirection, std::pair<int, int>> direction_data;
  direction_data[Field::FieldDirection::kUp] = std::make_pair(0, -1);
  direction_data[Field::FieldDirection::kDown] = std::make_pair(0, 1);
  direction_data[Field::FieldDirection::kRight] = std::make_pair(1, 0);
  // 移動方向の指定
  Field::FieldDirection direction = Field::FieldDirection::kDown;
  // 石設置場所リスト
  std::unordered_map<int, std::pair<int, int>> stone_location;

  // 盤の左上から下→上→下→...を繰り返していく形で石の配置場所を設定する
  // ↓→↓→↓→↓→↓
  // ↓↑↓↑↓↑↓↑↓
  // →↑→↑→↑→↑↓
  for (int i = 0; i < stone_total_number; ++i) {
    // 石の配置場所を格納
    stone_location[i] = std::make_pair(x_pos, y_pos);

    // 盤の上段 または 下段に到達したら右方向にひとつずらす
    // 上下方向の進む向きを変えるため、フラグを立てる
    if ((!is_frame_reached && y_pos == kStartIndex) ||
        (!is_frame_reached && y_pos == kEndIndex)) {
      is_frame_reached = true;
      direction = Field::FieldDirection::kRight;
    }
    // 盤の上段に到達、フラグが立っている場合、進む方向を「下方向」に設定する
    else if (is_frame_reached && y_pos == kStartIndex) {
      is_frame_reached = false;
      direction = Field::FieldDirection::kDown;
    }
    // 盤の下段に到達、フラグが立っている場合、進む方向を「上方向」に設定する
    else if (is_frame_reached && y_pos == kEndIndex) {
      is_frame_reached = false;
      direction = Field::FieldDirection::kUp;
    }

    // 石を配置する座標をセットする
    x_pos += direction_data[direction].first;
    y_pos += direction_data[direction].second;
  }

  //---------------------------------
  //ゲーム情報からバトル情報を取得
  //---------------------------------
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  // バトル結果を取得し、セットする
  GameInfo::BattleResult battle_result = game_info->GetBattleResult();
  SetBattleResult(battle_result);
  // 各石ごとの数を取得する
  int stone_black = game_info->GetBlackStoneCount();
  int stone_white = game_info->GetWhiteStoneCount();

  Stone* stone = nullptr;

  //---------------------------------
  //石の初期配置を行う処理
  //---------------------------------
  int index = 0;
  // 黒色の石をセットする
  for (index; index < stone_black; ++index) {
    // 石を取り出し、配置リストから配置座標を取得する
    stone = stone_manager_->GetStone(index);
    stone->SetStoneType(Stone::StoneType::kBlack);
    std::pair<int, int>pos = stone_location[index];
    // フィールド処理から石の配置場所を取得する
    field_->PutStone(*stone, pos.first, pos.second);
    // 石をキューに格納する
    black_stone_.push(stone);
  }
  // 白色の石をセットする
  for (index; index < stone_black + stone_white; ++index) {
    // 石を取り出し、配置リストから配置座標を取得する
    stone = stone_manager_->GetStone(index);
    stone->SetStoneType(Stone::StoneType::kWhite);
    std::pair<int, int>pos = stone_location[index];
    // フィールド処理から石の配置場所を取得する
    field_->PutStone(*stone, pos.first, pos.second);
    // 石をスタックに格納する
    white_stone_.push(stone);
  }
}

/// <summary>
/// 処理中
/// </summary>
/// <param name=""></param>
/// <returns></returns>
bool ResultLevel::Process() {

  switch (current_phase_) {
  case PhaseType::kStoneDraw: {
    //-----------------------------
    // 石を順番に表示していく
    //-----------------------------
    if (accumulate_time_ >= kStoneDisplayTime) {

      // 累積時間のリセット
      accumulate_time_ = kResetTime;
      // 石の総数を取得
      int total_number = stone_manager_->GetStoneTotalNumber();

      // 黒色の石
      if (!black_stone_.empty()) {
        Stone* stone = black_stone_.front();
        if (stone->GetStoneType() == Stone::StoneType::kBlack) {
          stone->SetPlaced();
          black_stone_.pop();
          // 石を置いた時の音を再生リストにセット
          sound_manager_->SetPlaySoundList(SoundId::kSetStone);
        }
      }
      // 白色の石
      if (!white_stone_.empty()) {
        Stone* stone = white_stone_.top();
        if (stone->GetStoneType() == Stone::StoneType::kWhite) {
          stone->SetPlaced();
          white_stone_.pop();
          // 石を置いた時の音を再生リストにセット
          sound_manager_->SetPlaySoundList(SoundId::kSetStone);
        }
      }
    }

    if (black_stone_.empty() && white_stone_.empty()) {
      // 現在のフェーズを「結果描画切り替え」に変更する
      ChangeCurrentPhase(PhaseType::kResultDrawSwitch);
    }
    break;
  }
  case PhaseType::kResultDrawSwitch: {
    //-----------------------------
    // 結果描画切り替えフェーズ
    //-----------------------------
    // 累積時間をリセット
    accumulate_time_ = kResetTime;
    
    // 現在の描画フェーズを「結果描画」に変更する
    ChangeCurrentDrawPhase(DrawPhaseType::kResultDrawing);
    // HUD情報の描画フェーズを変更する
    result_info_ui_->ChangeDrawPhase(ResultInfoUi::DrawPhaseType::kResultDrawing);

    // バトル結果を取得、結果に応じたSEを鳴らす
    switch (battle_result_) {
    case GameInfo::BattleResult::kWin: {
      sound_manager_->SetPlaySoundList(SoundId::kWin);
      sound_manager_->SetPlaySoundList(SoundId::kHandClap);
      break;
    }
    case GameInfo::BattleResult::kLose: {
      sound_manager_->SetPlaySoundList(SoundId::kLose);
      break;
    }
    }
    // リザルト画面用BGMの再生
    sound_manager_->SetPlaySoundList(SoundId::kResultBgm);
    // 現在のフェーズを「結果描画」に変更する
    ChangeCurrentPhase(PhaseType::kResultDraw);
    break;
  }
  case PhaseType::kResultDraw: {
    //-----------------------------
    // 結果描画フェーズ
    //-----------------------------
    if (accumulate_time_ >= kTransitionPhaseWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 現在のフェーズを「終了待機」に変更する
      ChangeCurrentPhase(PhaseType::kFinishWaiting);
    }
    break;
  }
  case PhaseType::kFinishWaiting: {
    //-----------------------------
    // 終了待機フェーズ
    //-----------------------------
    if (accumulate_time_ >= kDisplayInterval) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 表示フラグを切り替える
      is_display = !is_display;
    }
    break;
  }
  }

  return false;
}

/// <summary>
/// 終了処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ResultLevel::Finish() {

}

/// <summary>
/// フィールド処理 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool ResultLevel::InitializeField() {

  // 画像のロード
  int board_handle = LoadGraph(kImagePathBoard);
  if (board_handle == kLoadError) {
    std::cout << "ResultLevel InitializeField：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }

  std::array<int, kBoardFrame> frame_handle;
  int result = LoadDivGraph(kImagePathFrame, kFrameDivTotalNum, kFrameDivXNum, kFrameDivYNum, kWidth, kHeight, frame_handle.data());
  // 画像読み込み処理の結果、エラーだった場合
  if (result == kLoadError) {
    std::cout << "ResultLevel InitializeField：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }

  // フィールド処理 生成
  field_ = new Field();
  if (field_ == nullptr) {
    std::cout << "BattleLevel InitializeField：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // フィールド処理にグラフィックハンドルを渡す
  field_->SetGraphicHandleBoard(board_handle);
  field_->SetGraphicHandleFrame(frame_handle);
  // タスクマネージャにタスクを積む
  task_manager_.AddTask(field_);

  return true;
}

/// <summary>
/// ストーンマネージャ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool ResultLevel::InitializeStoneManager() {

  // ストーンマネージャを生成
  stone_manager_ = new StoneManager(*this);
  if (stone_manager_ == nullptr) {
    std::cout << "ResultLevel InitializeStoneManager：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // タスクマネージャにタスクを積む
  task_manager_.AddTask(stone_manager_);

  // 石の生成
  bool is_success = stone_manager_->CreateStone();
  if (!is_success) {
    OccurredError();
    return false;
  }

  return true;
}

/// <summary>
/// メニューコントローラ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool ResultLevel::InitializeMenuController() {

  // メニューコントローラの生成
  menu_controller_ = new MenuController(*this);
  if (menu_controller_ == nullptr) {
    std::cout << "ResultLevel InitializeMenuController：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // タスクマネージャにタスクを積む
  task_manager_.AddTask(menu_controller_);

  return true;
}

/// <summary>
/// HUD情報 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool ResultLevel::InitializeInfoUi() {

  int winner_hanndle = LoadGraph(kImagePathWinner);
  if (winner_hanndle == kLoadError) {
    std::cout << "ResultLevel InitializeInfoUi：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }
  int lose_hanndle = LoadGraph(kImagePathLose);
  if (lose_hanndle == kLoadError) {
    std::cout << "ResultLevel InitializeInfoUi：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }
  int draw_hanndle = LoadGraph(kImagePathDraw);
  if (draw_hanndle == kLoadError) {
    std::cout << "ResultLevel InitializeInfoUi：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }
  int stone_black = LoadGraph(kImagePathStoneBlack);
  if (stone_black == kLoadError) {
    std::cout << "ResultLevel InitializeInfoUi：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }
  int stone_white = LoadGraph(kImagePathStoneWhite);
  if (stone_white == kLoadError) {
    std::cout << "ResultLevel InitializeInfoUi：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }

  // HUD情報の生成
  result_info_ui_ = new ResultInfoUi();
  if (result_info_ui_ == nullptr) {
    std::cout << "ResultLevel InitializeInfoUi：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // リザルト勝利のグラフィックハンドルを渡す
  result_info_ui_->SetGraphicHandleWinner(winner_hanndle);
  // リザルト敗北のグラフィックハンドルを渡す
  result_info_ui_->SetGraphicHandleLose(lose_hanndle);
  // リザルト引分のグラフィックハンドルを渡す
  result_info_ui_->SetGraphicHandleDraw(draw_hanndle);
  // 黒色の石のグラフィックハンドルを渡す
  result_info_ui_->SetGraphicHandleStoneBlack(stone_black);
  // 白色の石のグラフィックハンドルを渡す
  result_info_ui_->SetGraphicHandleStoneWhite(stone_white);

  // タスクマネージャにタスクを積む
  task_manager_.AddTask(result_info_ui_);

  return true;
}

/// <summary>
/// 背景 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool ResultLevel::InitializeBackScreen() {

  // 画像のロード
  int graphic_handle = LoadGraph(kImagePathBackScreen);
  if (graphic_handle == kLoadError) {
    std::cout << "ResultLevel InitializeBackScreen：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }

  // 背景 生成
  back_screen_ = new BackScreen(kMagnification, kAngle, *this);
  if (back_screen_ == nullptr) {
    std::cout << "ResultLevel InitializeBackScreen：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // グラフィックハンドルを渡す
  back_screen_->SetGraphicHandle(graphic_handle);
  // 透過率の変化量の指定
  back_screen_->SetAlphaAdjustValue(kAlphaAdjastStart, kAlphaAdjastFinish);
  // タスクマネージャにタスクを積む
  task_manager_.AddTask(back_screen_);

  return true;
}

/// <summary>
/// サウンドマネージャ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool ResultLevel::InitializeSoundManager() {

  // サウンドマネージャ 生成
  sound_manager_ = new SoundManager();
  if (back_screen_ == nullptr) {
    std::cout << "ResultLevel InitializeSoundManager：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // タスクマネージャにタスクを積む
  task_manager_.AddTask(sound_manager_);

  std::cout << "ResultLevel InitializeSoundManager サウンド生成" << std::endl;
  int start_time = GetNowCount();

  // サウンドを生成
  CreateSound();

  int end_time = GetNowCount();
  float diff_time = (end_time - start_time) / kThousand;
  std::cout << "ResultLevel InitializeSoundManager CreateSound処理時間：" << diff_time << std::endl;

  return true;
}


/// <summary>
/// サウンド生成
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool ResultLevel::CreateSound() {

  Sound* sound = nullptr;
  // 音をロードする
  int sound_handle = LoadSoundMem(kSoundPathSetStone);
  std::cout << "ResultLevel CreateSound ロード戻り値：" << sound_handle << std::endl;
  if (sound_handle != kLoadError) {
    // 「石を置く設置したときの音」をサウンドマネージャに積む
    sound = sound_manager_->AddSound(SoundId::kSetStone);
    if (sound != nullptr) {
      // サウンドハンドル、サウンドの種類、サウンド効果の種類を設定する
      sound->SetSoundHandle(sound_handle);
      sound->SetSoundPlayType(Sound::PlaySoundType::kSoundEffect);
      sound->SetEffectType(Sound::EffectType::kNone);
      sound->SetSoundVolume();
    }
  }
  // 音をロードする
  sound_handle = LoadSoundMem(kSoundPathHandClap);
  std::cout << "ResultLevel CreateSound ロード戻り値：" << sound_handle << std::endl;
  if (sound_handle != kLoadError) {
    // 「拍手」をサウンドマネージャに積む
    sound = sound_manager_->AddSound(SoundId::kHandClap);
    if (sound != nullptr) {
      // サウンドハンドル、サウンドの種類、サウンド効果の種類を設定する
      sound->SetSoundHandle(sound_handle);
      sound->SetSoundPlayType(Sound::PlaySoundType::kSoundEffect);
      sound->SetEffectType(Sound::EffectType::kNone);
      sound->SetSoundVolume();
    }
  }
  // 音をロードする
  sound_handle = LoadSoundMem(kSoundPathWinner);
  std::cout << "ResultLevel CreateSound ロード戻り値：" << sound_handle << std::endl;
  if (sound_handle != kLoadError) {
    // 「勝利」をサウンドマネージャに積む
    sound = sound_manager_->AddSound(SoundId::kWin);
    if (sound != nullptr) {
      // サウンドハンドル、サウンドの種類、サウンド効果の種類を設定する
      sound->SetSoundHandle(sound_handle);
      sound->SetSoundPlayType(Sound::PlaySoundType::kSoundEffect);
      sound->SetEffectType(Sound::EffectType::kNone);
      sound->SetSoundVolume();
    }
  }
  // 音をロードする
  sound_handle = LoadSoundMem(kSoundPathLoser);
  std::cout << "ResultLevel CreateSound ロード戻り値：" << sound_handle << std::endl;
  if (sound_handle != kLoadError) {
    // 「敗者」をサウンドマネージャに積む
    sound = sound_manager_->AddSound(SoundId::kLose);
    if (sound != nullptr) {
      // サウンドハンドル、サウンドの種類、サウンド効果の種類を設定する
      sound->SetSoundHandle(sound_handle);
      sound->SetSoundPlayType(Sound::PlaySoundType::kSoundEffect);
      sound->SetEffectType(Sound::EffectType::kNone);
      sound->SetSoundVolume();
    }
  }
  // 音をロードする
  sound_handle = LoadSoundMem(kSoundPathBgm);
  std::cout << "ResultLevel CreateSound ロード戻り値：" << sound_handle << std::endl;
  if (sound_handle != kLoadError) {
    // 「リザルトBGM」をサウンドマネージャに積む
    sound = sound_manager_->AddSound(SoundId::kResultBgm);
    if (sound != nullptr) {
      // サウンドハンドル、サウンドの種類、サウンド効果の種類を設定する
      sound->SetSoundHandle(sound_handle);
      sound->SetSoundPlayType(Sound::PlaySoundType::kBackGroundMusic);
      sound->SetEffectType(Sound::EffectType::kNone);
      sound->SetSoundVolume();
    }
  }

  return true;
}

/// <summary>
/// 終了処理通知
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ResultLevel::OnFinishLevelEvent() {

  // ボタンの種類を取得
  ResultInfoUi::ButtonType button_type = result_info_ui_->GetExistsCursorButton();

  std::cout << "ResultLevel OnFinishLevelEvent ボタン種類：" << static_cast<int>(button_type) << std::endl;

  switch (button_type) {
  case ResultInfoUi::ButtonType::kTitleTransition: {
    // タイトルレベルへ遷移
    ChangeLevel(TaskId::kTitleLevel);

    // ゲーム情報のプレイヤー手番を初期化する
    GameInfo* game_info = GameInfo::GetGameInfoInstance();
    if (game_info == nullptr) {
      return;
    }
    game_info->SetTurnType(GameInfo::TurnType::kNone);

    break;
  }
  case ResultInfoUi::ButtonType::kBattleTransition: {
    // バトルレベルへ遷移
    ChangeLevel(TaskId::kBattleLevel);
    break;
  }
  }
}