﻿#include "Game/BattleLevel.h"

namespace {

  /// <summary>
  /// 半分
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// ターンパネルの画像パス
  /// </summary>
  const char* kImagePathTurnPanel = "Assets/Image/turn.png";

  /// <summary>
  /// 背景画像
  /// </summary>
  const char* kImagePathBackScreen = "Assets/Image/backscreen.png";

  /// <summary>
  /// パネル画像
  /// </summary>
  const char* kImagePathPanel01 = "Assets/Image/wood_frame_01.png";

  /// <summary>
  /// パネル画像
  /// </summary>
  const char* kImagePathPanel02 = "Assets/Image/wood_frame_02.png";

  /// <summary>
  /// HUD用黒色の石の画像取得先パス
  /// </summary>
  const char* kImagePathStoneBlack = "Assets/Image/stone_black.png";

  /// <summary>
  /// HUD用白色の石の画像取得先パス
  /// </summary>
  const char* kImagePathStoneWhite = "Assets/Image/stone_white.png";

  /// <summary>
  /// ターン表示枠の画像取得パス
  /// </summary>
  const char* kImagePathTurnFrame = "Assets/Image/turn_cursor.png";

  /// <summary>
  /// スタートUIの画像取得先パス
  /// </summary>
  const char* kImagePathStartUi = "Assets/Image/start.png";

  /// <summary>
  /// フィニッシュUIの画像取得先パス
  /// </summary>
  const char* kImagePathFinishUi = "Assets/Image/finish.png";

  /// <summary>
  /// 画像の総分割数
  /// </summary>
  const int kDivTotalNum = 2;

  /// <summary>
  /// 画像の横方向の分割数
  /// </summary>
  const int kDivXNum = 1;

  /// <summary>
  /// 画像の縦方向の分割数
  /// </summary>
  const int kDivYNum = 2;

  /// <summary>
  /// 画像分割後の1枚あたりの幅
  /// </summary>
  const int kWidth = 64;

  /// <summary>
  /// 画像分割後の1枚あたりの高さ
  /// </summary>
  const int kHeight = 64;

  /// <summary>
  /// ターンパネルの画像の総分割数
  /// </summary>
  const int kTurnDivTotalNum = 3;

  /// <summary>
  /// ターンパネルの画像の横方向の分割数
  /// </summary>
  const int kTurnDivXNum = 1;

  /// <summary>
  /// ターンパネルの画像の縦方向の分割数
  /// </summary>
  const int kTurnDivYNum = 3;

  /// <summary>
  /// ターンパネル画像分割後の1枚あたりの幅
  /// </summary>
  const int kTurnWidth = 1066;

  /// <summary>
  /// ターンパネル画像分割後の1枚あたりの高さ
  /// </summary>
  const int kTurnHeight = 235;

  /// <summary>
  /// 画像読み込み時のエラー値
  /// </summary>
  const int kLoadError = -1;

  /// <summary>
  /// カーソル表示 表示状態
  /// </summary>
  const bool kShowStatus = true;

  /// <summary>
  /// カーソル表示 非表示状態
  /// </summary>
  const bool kHideStatus = false;

  /// <summary>
  /// 背景 表示倍率
  /// </summary>
  const float kMagnification = 1.4f;

  /// <summary>
  /// 背景 表示角度 [rad]
  /// </summary>
  const float kAngle = 0.0f;

  /// <summary>
  /// 透過率 起動時調整値
  /// </summary>
  const int kAlphaAdjastStart = 15;

  /// <summary>
  /// 透過率 終了時調整値
  /// </summary>
  const int kAlphaAdjastFinish = 10;

  /// <summary>
  /// バトルレベルBGM ファイルパス
  /// </summary>
  const char* kSoundPathBgm = "Assets/Sound/larimar.mp3";

  /// <summary>
  /// 石を置いた時の音 ファイルパス
  /// </summary>
  const char* kSoundPathSetStone = "Assets/Sound/reversi_setstone.mp3";

  /// <summary>
  /// 1000 単位変換に使用
  /// </summary>
  const float kThousand = 1000.0f;

  /// <summary>
  /// 累積時間 リセット
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 待機時間
  /// </summary>
  const float kWaitTime = 0.2f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> タスクID </param>
/// <param name=""> タスクマネージャ </param>
/// <returns></returns>
BattleLevel::BattleLevel(TaskId task_id, TaskManager& task_manager)
  : Level(task_id, task_manager)
  , field_(nullptr)
  , stone_manager_(nullptr)
  , cursor_manager_(nullptr)
  , menu_controller_(nullptr)
  , player_controller_(nullptr)
  , enemy_(nullptr)
  , game_mode_(nullptr)
  , back_screen_(nullptr)
  , battle_info_ui_(nullptr)
  , start_ui_(nullptr)
  , finish_ui_(nullptr)
  , sound_manager_(nullptr)
  , turn_select_menu_(nullptr)
  , finalize_phase_(FinalizePhaseType::kUnloadTask)
  , current_phase_(PhaseType::kPrepareWait)
  , accumulate_time_(0.0f) {

  // コンソールに出力
  std::cout << "BattleLevel コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
BattleLevel::~BattleLevel() {

  // コンソールに出力
  std::cout << "~BattleLevel デストラクタ" << std::endl;
}

/// <summary>
/// 初期化処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializePhase(float process_time) {

  bool is_success = false;

  // フィールド処理 初期設定
  is_success = InitializeField();
  if (!is_success) {
    return false;
  }

  // ゲームルール(ターン管理) 初期設定
  is_success = InitializeGameMode();
  if (!is_success) {
    return false;
  }

  // ストーンマネージャ 初期設定
  is_success = InitializeStoneManager();
  if (!is_success) {
    return false;
  }

  // カーソルマネージャ 初期設定
  is_success = InitializeCursorManager();
  if (!is_success) {
    return false;
  }

  // メニューコントローラ 初期設定
  is_success = InitializeMenuController();
  if (!is_success) {
    return false;
  }

  // プレイヤーコントローラ 初期設定
  is_success = InitializePlayerController();
  if (!is_success) {
    return false;
  }

  // CPU 初期設定
  is_success = InitializeEnemy();
  if (!is_success) {
    return false;
  }

  // 背景 初期設定
  is_success = InitializeBackScreen();
  if (!is_success) {
    return false;
  }

  // HUD情報 初期設定
  is_success = InitializeBattleInfoUi();
  if (!is_success) {
    return false;
  }

  // スタートUI 初期設定
  is_success = InitializeStartUi();
  if (!is_success) {
    return false;
  }

  // フィニッシュUI 初期設定
  is_success = InitializeFinishUi();
  if (!is_success) {
    return false;
  }

  // サウンドマネージャ 初期設定
  is_success = InitializeSoundManager();
  if (!is_success) {
    return false;
  }

  // 手番選択メニュー 初期設定
  is_success = InitializeTurnSelectMenu();
  if (!is_success) {
    return false;
  }

  return true;
}

/// <summary>
/// 毎フレームの処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool BattleLevel::UpdatePhase(float process_time) {

  bool check_finish = false;

  switch (current_phase_) {
  case PhaseType::kPrepareWait: {
    //--------------------------
    //準備待機フェーズ
    //--------------------------
    check_finish = field_->IsProcess();
    if (check_finish) {
      // 現在のフェーズを「準備中」に変更
      ChangeCurrentPhase(PhaseType::kPrepare);
    }
    break;
  }
  case PhaseType::kPrepare: {
    //--------------------------
    //準備中フェーズ
    //--------------------------
    check_finish = Preparate();
    if (check_finish) {
      // 現在のフェーズを「準備完了フェーズ」に変更
      ChangeCurrentPhase(PhaseType::kPrepared);
    }
    break;
  }
  case PhaseType::kPrepared: {
    //--------------------------
    //準備完了フェーズ
    //--------------------------
    // ゲーム情報から手番を取得する
    GameInfo* game_info = GameInfo::GetGameInfoInstance();
    if (game_info == nullptr) {
      break;
    }
    GameInfo::TurnType turn_type = game_info->GetTurnType();
    switch (turn_type) {
    case GameInfo::TurnType::kNone: {
      //------------------------------------
      //先攻/後攻が決まっていない場合
      //------------------------------------
      // 現在のフェーズを「手番選択フェーズ」に変更
      ChangeCurrentPhase(PhaseType::kSelectTurn);
      // タスクマネージャに「手番調整メニュー」を積む
      task_manager_.AddTask(turn_select_menu_);
      break;
    }
    default: {
      //------------------------------------
      //先攻/後攻が決まっている場合
      //------------------------------------
      // 現在のフェーズを「ゲーム開始切り替え」に変更
      ChangeCurrentPhase(PhaseType::kSwitchGameStart);
      break;
    }
    }

    break;
  }
  case PhaseType::kSelectTurn: {
    //--------------------------
    //手番選択フェーズ
    //--------------------------
    // 累積時間をリセット
    accumulate_time_ = kResetTime;
    break;
  }
  case PhaseType::kSelectedTurn: {
    //--------------------------
    //手番選択終了フェーズ
    //--------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;

    if (accumulate_time_ >= kWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 現在のフェーズを「ゲーム開始切り替え」に変更
      ChangeCurrentPhase(PhaseType::kSwitchGameStart);
    }
    break;
  }
  case PhaseType::kSwitchGameStart: {
    //--------------------------
    //ゲーム開始切り替え
    //--------------------------
    // メニューコントローラをタスクマネージャから降ろす
    TaskId task_id = menu_controller_->GetTaskId();
    Task* release_task = task_manager_.ReleaseTask(task_id);
    // 手番調整メニューをタスクマネージャから降ろす
    task_id = turn_select_menu_->GetTaskId();
    release_task = task_manager_.ReleaseTask(task_id);

    // プレイヤーコントローラをタスクマネージャに積む
    task_manager_.AddTask(player_controller_);

    GameInfo* game_info = GameInfo::GetGameInfoInstance();
    if (game_info == nullptr) {
      return false;
    }
    GameInfo::TurnType turn_type = game_info->GetTurnType();
    // 先攻/後攻をセットする
    DecideTurnType(turn_type);

    // 現在のフェーズを「スタートUI表示フェーズ」に変更
    ChangeCurrentPhase(PhaseType::kStartUiDisplay);
    break;
  }
  case PhaseType::kStartUiDisplay: {
    //--------------------------
    //スタートUI表示フェーズ
    //--------------------------
    // タスクマネージャにスタートUIを積む
    task_manager_.AddTask(start_ui_);
    // 現在のフェーズを「スタートUI終了待機」に変更
    ChangeCurrentPhase(PhaseType::kStartUiFinishWait);

    // バトルレベルのBGMを流す
    sound_manager_->SetPlaySoundList(SoundId::kBattleBGM);
    break;
  }
  case PhaseType::kStartUiFinishWait: {
    //--------------------------
    //スタートUI終了待機フェーズ
    //--------------------------
    break;
  }
  case PhaseType::kPlay: {
    //--------------------------
    //プレイ中フェーズ
    //--------------------------
    break;
  }
  case PhaseType::kFinishUiDisplay: {
    //--------------------------
    //フィニッシュUI表示フェーズ
    //--------------------------
    // タスクマネージャにフィニッシュUIを積む
    task_manager_.AddTask(finish_ui_);
    // 現在のフェーズを「フィニッシュUI終了待機」に変更
    ChangeCurrentPhase(PhaseType::kFinishUiFinishWait);

    break;
  }
  case PhaseType::kFinishUiFinishWait: {
    //--------------------------
    //フィニッシュUI終了待機フェーズ
    //--------------------------
    break;
  }
  case PhaseType::kFinalize: {
    //--------------------------
    //終了処理フェーズ
    //--------------------------
    // バトルレベルのBGMを止める
    Sound* sound = sound_manager_->GetSound(SoundId::kBattleBGM);
    if (sound != nullptr) {
      sound->SoundPause();
    }

    // リザルトレベルへ切り替える
    ChangeLevel(TaskId::kResultLevel);

    GameInfo* game_info = GameInfo::GetGameInfoInstance();

    // 黒色と白色の石の数を取得する
    int stone_black_count = 0;
    int stone_white_count = 0;
    field_->CountStone(stone_black_count, stone_white_count);

    // プレイヤーの使用した石を取得
    Stone::StoneType stone_type_player = game_mode_->GetStoneType(GameMode::PlayerTurn::kPlayer);
    Stone::StoneType stone_type_win;

    // ゲーム情報を渡す
    switch (stone_type_player) {
    case Stone::StoneType::kBlack: {
      game_info->SetGameData(GameInfo::PlayerType::kPlayer, GameInfo::StoneType::kBlack);
      game_info->SetGameData(GameInfo::PlayerType::kCpu, GameInfo::StoneType::kWhite);
      break;
    }
    case Stone::StoneType::kWhite: {
      game_info->SetGameData(GameInfo::PlayerType::kPlayer, GameInfo::StoneType::kWhite);
      game_info->SetGameData(GameInfo::PlayerType::kCpu, GameInfo::StoneType::kBlack);
      break;
    }
    }

    // 引き分けの場合、結果をセットしてここで処理を終了する
    if (stone_black_count == stone_white_count) {
      game_info->SetBattleResult(GameInfo::BattleResult::kDraw);
      game_info->SetStoneCount(stone_black_count, stone_white_count);
      break;
    }

    // 石の総数を取得
    int stone_total_number = stone_manager_->GetStoneTotalNumber();

    // 黒色が多い
    if (stone_black_count > stone_white_count) {
      stone_type_win = Stone::StoneType::kBlack;
      // 黒と白の石の総数が石全体の総数と一致しない場合、黒の石の数を調整
      // ※盤上の石が置かれていないマスの分の石は勝者側の石とする
      if ((stone_black_count + stone_white_count) != stone_total_number) {
        stone_black_count = stone_total_number - stone_white_count;
      }
    }
    // 白色が多い
    else if (stone_black_count < stone_white_count) {
      stone_type_win = Stone::StoneType::kWhite;
      // 黒と白の石の総数が石全体の総数と一致しない場合、白の石の数を調整
      // ※盤上の石が置かれていないマスの分の石は勝者側の石とする
      if ((stone_black_count + stone_white_count) != stone_total_number) {
        stone_white_count = stone_total_number - stone_black_count;
      }
    }

    // プレイヤー使用の石の種類と勝利した石の種類が一致 = プレイヤー勝利
    // プレイヤー使用の石の種類と勝利した石の種類が不一致 = プレイヤー敗北
    if (stone_type_player == stone_type_win) {
      game_info->SetBattleResult(GameInfo::BattleResult::kWin);
      game_info->SetStoneCount(stone_black_count, stone_white_count);
    }
    else {
      game_info->SetBattleResult(GameInfo::BattleResult::kLose);
      game_info->SetStoneCount(stone_black_count, stone_white_count);
    }
    break;

  }
  }
  return false;
}

/// <summary>
/// 毎フレームの描画処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::RenderPhase() {

}

/// <summary>
/// 終了処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool BattleLevel::FinishPhase(float process_time) {

  switch (finalize_phase_) {
  case FinalizePhaseType::kUnloadTask: {
    //-------------------------------
    //タスクを降ろすフェーズ
    //-------------------------------
    // 石の破棄を行う
    if (stone_manager_ != nullptr) {
      stone_manager_->DisposeStone();
    }
    // カーソルの破棄を行う
    if (cursor_manager_ != nullptr) {
      cursor_manager_->DisposeCursor();
    }
    // 音の破棄を行う
    if (sound_manager_ != nullptr) {
      sound_manager_->DisposeSound();
    }
   
    // 画像のリソースを破棄
    if (field_ != nullptr) {
      DeleteGraph(field_->GetGraphicHandleBoard());                 // 盤面
      DeleteGraph(field_->GetGraphicHandleFrame(kOuterFrame));      // 盤枠
      DeleteGraph(field_->GetGraphicHandleFrame(kCornerFrame));     // 盤枠
    }
    if (back_screen_ != nullptr) {
      DeleteGraph(back_screen_->GetGraphicHandle());                // 背景
    }
    if (game_mode_ != nullptr) {
      DeleteGraph(game_mode_->GetGraphicHandle());                  // スキップ
    }
    if (battle_info_ui_ != nullptr) {
      DeleteGraph(battle_info_ui_->GetGraphicHandlePanel01());      // パネル1
      DeleteGraph(battle_info_ui_->GetGraphicHandlePanel02());      // パネル2
      DeleteGraph(battle_info_ui_->GetGraphicHandleStoneBlack());   // 黒色の石
      DeleteGraph(battle_info_ui_->GetGraphicHandleStoneWhite());   // 白色の石
      DeleteGraph(battle_info_ui_->GetGraphicHandleTurnFrame());    // ターンフレーム
    }
    if (start_ui_ != nullptr) {
      DeleteGraph(start_ui_->GetGraphicHandle());                   // スタートUI
    }
    if (finish_ui_ != nullptr) {
      DeleteGraph(finish_ui_->GetGraphicHandle());                  // フィニッシュUI
    }

    TaskId task_id;
    Task* release_task = nullptr;
    // フィールド処理をタスクマネージャから降ろす
    if (field_ != nullptr) {
      task_id = field_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // ストーンマネージャをタスクマネージャから降ろす
    if (stone_manager_ != nullptr) {
      task_id = stone_manager_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // カーソルマネージャをタスクマネージャから降ろす
    if (cursor_manager_ != nullptr) {
      task_id = cursor_manager_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // プレイヤーコントローラをタスクマネージャから降ろす
    if (player_controller_ != nullptr) {
      task_id = player_controller_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // CPUをタスクマネージャから降ろす
    if (enemy_ != nullptr) {
      task_id = enemy_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // ゲームモードをタスクマネージャから降ろす
    if (game_mode_ != nullptr) {
      task_id = game_mode_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // 背景をタスクマネージャから降ろす
    if (back_screen_ != nullptr) {
      task_id = back_screen_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // HUD情報をタスクマネージャから降ろす
    if (battle_info_ui_ != nullptr) {
      task_id = battle_info_ui_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // スタートUIをタスクマネージャから降ろす
    if (start_ui_ != nullptr) {
      task_id = start_ui_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // フィニッシュUIをタスクマネージャから降ろす
    if (finish_ui_ != nullptr) {
      task_id = finish_ui_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // サウンドマネージャをタスクマネージャから降ろす
    if (sound_manager_ != nullptr) {
      task_id = sound_manager_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // メニューコントローラをタスクマネージャから降ろす
    if (menu_controller_ != nullptr) {
      task_id = menu_controller_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // 手番調整メニューをタスクマネージャから降ろす
    if (turn_select_menu_ != nullptr) {
      task_id = turn_select_menu_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }

    // 終了フェーズを「タスクを破棄する」に変更
    finalize_phase_ = FinalizePhaseType::kDisposeTask;

    return false;
  }

  case FinalizePhaseType::kDisposeTask: {
    //-------------------------------
    //タスクを破棄するフェーズ
    //-------------------------------
    if (turn_select_menu_ != nullptr) {
      delete turn_select_menu_;
      turn_select_menu_ = nullptr;
    }
    if (sound_manager_ != nullptr) {
      delete sound_manager_;
      sound_manager_ = nullptr;
    }
    if (finish_ui_ != nullptr) {
      delete finish_ui_;
      finish_ui_ = nullptr;
    }
    if (start_ui_ != nullptr) {
      delete start_ui_;
      start_ui_ = nullptr;
    }
    if (battle_info_ui_ != nullptr) {
      delete battle_info_ui_;
      battle_info_ui_ = nullptr;
    }
    if (enemy_ != nullptr) {
      delete enemy_;
      enemy_ = nullptr;
    }
    if (player_controller_ != nullptr) {
      delete player_controller_;
      player_controller_ = nullptr;
    }
    if (menu_controller_ != nullptr) {
      delete menu_controller_;
      menu_controller_ = nullptr;
    }
    if (cursor_manager_ != nullptr) {
      delete cursor_manager_;
      cursor_manager_ = nullptr;
    }
    if (stone_manager_ != nullptr) {
      delete stone_manager_;
      stone_manager_ = nullptr;
    }
    if (game_mode_ != nullptr) {
      delete game_mode_;
      game_mode_ = nullptr;
    }
    if (back_screen_ != nullptr) {
      delete back_screen_;
      back_screen_ = nullptr;
    }
    if (field_ != nullptr) {
      delete field_;
      field_ = nullptr;
    }
    break;
  }
  }

  return true;
}

/// <summary>
/// 現在のフェーズを変更する
/// </summary>
/// <param name=""> フェーズの種類 </param>
/// <returns></returns>
void BattleLevel::ChangeCurrentPhase(PhaseType phase_type) {

  current_phase_ = phase_type;
}

/// <summary>
/// フィールド処理 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeField() {

  // 画像のロード
  int board_handle = LoadGraph(kImagePathBoard);
  if (board_handle == kLoadError) {
    std::cout << "BattleLevel InitializeField：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }
  std::array<int, kBoardFrame> frame_handle;
  int result = LoadDivGraph(kImagePathFrame, kFrameDivTotalNum, kFrameDivXNum, kFrameDivYNum, kWidth, kHeight, frame_handle.data());
  if (result == kLoadError) {
    std::cout << "BattleLevel InitializeField：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }

  // フィールド処理 生成
  field_ = new Field();
  if (field_ == nullptr) {
    std::cout << "BattleLevel InitializeField：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // フィールド処理にグラフィックハンドルを渡す
  field_->SetGraphicHandleBoard(board_handle);
  field_->SetGraphicHandleFrame(frame_handle);
  // タスクマネージャにタスクを積む
  task_manager_.AddTask(field_);

  return true;
}

/// <summary>
/// ストーンマネージャ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeStoneManager() {

  // ストーンマネージャを生成
  stone_manager_ = new StoneManager(*this);
  if (stone_manager_ == nullptr) {
    std::cout << "BattleLevel InitializeStoneManager：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // タスクマネージャにタスクを積む
  task_manager_.AddTask(stone_manager_);

  // 石の生成
  bool is_success = stone_manager_->CreateStone();
  if (!is_success) {
    OccurredError();
    return false;
  }

  return true;
}

/// <summary>
/// カーソルマネージャ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeCursorManager() {

  // ストーンマネージャを生成
  cursor_manager_ = new CursorManager();
  if (cursor_manager_ == nullptr) {
    std::cout << "BattleLevel InitializeCursorManager：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // タスクマネージャにタスクを積む
  task_manager_.AddTask(cursor_manager_);
  // カーソルの生成
  cursor_manager_->CreateCursor();


  return true;
}

/// <summary>
/// メニューコントローラ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeMenuController() {

  // メニューコントローラ 生成
  menu_controller_ = new MenuController(*this);
  if (menu_controller_ == nullptr) {
    std::cout << "BattleLevel InitializeMenuController：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // タスクマネージャにタスクを積む
  task_manager_.AddTask(menu_controller_);

  return true;
}

/// <summary>
/// プレイヤーコントローラ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializePlayerController() {

  // プレイヤーコントローラ 生成
  player_controller_ = new PlayerController(*this);
  if (player_controller_ == nullptr) {
    std::cout << "BattleLevel InitializePlayerController：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // プレイヤーコントローラにカーソルの種類をセット
  PlayerCursor* player_cursor = cursor_manager_->GetPlayerCursor();
  player_controller_->SetCursor(player_cursor);

  return true;
}

/// <summary>
/// CPU 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeEnemy() {

  // CPUのカーソルを取得
  EnemyCursor* enemy_cursor = cursor_manager_->GetEnemyCursor();
  // CPUの生成
  enemy_ = new Enemy(*this, *this, enemy_cursor);
  if (enemy_ == nullptr) {
    std::cout << "BattleLevel InitializeEnemy：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // タスクマネージャにタスクを積む
  task_manager_.AddTask(enemy_);

  return true;
}

/// <summary>
/// ゲームモード 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeGameMode() {

  // 画像のロード
  int graphic_handle = LoadGraph(kImagePathTurnPanel);
  if (graphic_handle == kLoadError) {
    std::cout << "BattleLevel InitializeGameMode：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }

  // ゲームモード 生成
  game_mode_ = new GameMode(*this);
  if (game_mode_ == nullptr) {
    std::cout << "BattleLevel InitializeGameMode：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // 画像のグラフィックハンドルを渡す
  game_mode_->SetGraphicHandle(graphic_handle);
  // グラフィックハンドルから画像のサイズを取得
  game_mode_->SetImageSize(graphic_handle);
  // タスクマネージャにタスクを積む
  task_manager_.AddTask(game_mode_);

  return true;
}

/// <summary>
/// 背景 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeBackScreen() {

  // 画像のロード
  int graphic_handle = LoadGraph(kImagePathBackScreen);
  if (graphic_handle == kLoadError) {
    std::cout << "BattleLevel InitializeBackScreen：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }

  // 背景 生成
  back_screen_ = new BackScreen(kMagnification, kAngle, *this);
  if (back_screen_ == nullptr) {
    std::cout << "BattleLevel InitializeBackScreen：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // グラフィックハンドルを渡す
  back_screen_->SetGraphicHandle(graphic_handle);
  // 透過率の変化量の指定
  back_screen_->SetAlphaAdjustValue(kAlphaAdjastStart, kAlphaAdjastFinish);
  // タスクマネージャにタスクを積む
  task_manager_.AddTask(back_screen_);

  return true;
}

/// <summary>
/// HUD情報 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool  BattleLevel::InitializeBattleInfoUi() {

  // 画像のロード
  int graphic_handle_01 = LoadGraph(kImagePathPanel01);
  if (graphic_handle_01 == kLoadError) {
    std::cout << "BattleLevel InitializeBattleInfoUi：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }
  int graphic_handle_02 = LoadGraph(kImagePathPanel02);
  if (graphic_handle_02 == kLoadError) {
    std::cout << "BattleLevel InitializeBattleInfoUi：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }
  int stone_black = LoadGraph(kImagePathStoneBlack);
  if (stone_black == kLoadError) {
    std::cout << "BattleLevel InitializeBattleInfoUi：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }
  int stone_white = LoadGraph(kImagePathStoneWhite);
  if (stone_white == kLoadError) {
    std::cout << "BattleLevel InitializeBattleInfoUi：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }
  int turn_frame = LoadGraph(kImagePathTurnFrame);
  if (turn_frame == kLoadError) {
    std::cout << "BattleLevel InitializeBattleInfoUi：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }

  // HUD 生成
  battle_info_ui_ = new BattleInfoUi();
  if (battle_info_ui_ == nullptr) {
    std::cout << "BattleLevel InitializeBattleInfoUi：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // グラフィックハンドルを渡す
  battle_info_ui_->SetGraphicHandlePanel01(graphic_handle_01);
  battle_info_ui_->SetGraphicHandlePanel02(graphic_handle_02);
  battle_info_ui_->SetGraphicHandleStoneBlack(stone_black);
  battle_info_ui_->SetGraphicHandleStoneWhite(stone_white);
  battle_info_ui_->SetGraphicHandleTurnFrame(turn_frame);
  // タスクマネージャにタスクを積む
  task_manager_.AddTask(battle_info_ui_);

  return true;
}

/// <summary>
/// スタートUI 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeStartUi() {

  // 画像のロード
  int graphic_handle = LoadGraph(kImagePathStartUi);
  if (graphic_handle == kLoadError) {
    std::cout << "BattleLevel InitializeStartUi：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }
  // スタートUI 生成
  start_ui_ = new StartUi(*this);
  if (start_ui_ == nullptr) {
    std::cout << "BattleLevel InitializeStartUi：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // グラフィックハンドルを渡す
  start_ui_->SetGraphicHandle(graphic_handle);

  return true;
}

/// <summary>
/// フィニッシュUI 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeFinishUi() {

  // 画像のロード
  int graphic_handle = LoadGraph(kImagePathFinishUi);
  if (graphic_handle == kLoadError) {
    std::cout << "BattleLevel InitializeFinishUi：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }
  // フィニッシュUI 生成
  finish_ui_ = new FinishUi(*this);
  if (finish_ui_ == nullptr) {
    std::cout << "BattleLevel InitializeFinishUi：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // グラフィックハンドルを渡す
  finish_ui_->SetGraphicHandle(graphic_handle);

  return true;
}

/// <summary>
/// サウンドマネージャ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeSoundManager() {

  // サウンドマネージャ 生成
  sound_manager_ = new SoundManager();
  if (sound_manager_ == nullptr) {
    std::cout << "BattleLevel InitializeSoundManager：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // タスクマネージャにタスクを積む
  task_manager_.AddTask(sound_manager_);

  return true;
}

/// <summary>
/// 手番選択メニュー 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeTurnSelectMenu() {

  // 手番選択メニュー 生成
  turn_select_menu_ = new TurnSelectMenu();
  if (turn_select_menu_ == nullptr) {
    std::cout << "BattleLevel InitializeTurnSelectMenu：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  return true;
}

/// <summary>
/// サウンド生成
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::CreateSound() {

  Sound* sound = nullptr;
  int start_time, end_time;
  float diff_time;

  start_time = GetNowCount();

  // 音をロードする
  int sound_handle = LoadSoundMem(kSoundPathSetStone);

  end_time = GetNowCount();
  diff_time = (end_time - start_time) / kThousand;
  std::cout << "BattleLevel CreateSound ロード処理時間：" << diff_time << std::endl;
  std::cout << "BattleLevel CreateSound ロード戻り値：" << sound_handle << std::endl;

  if (sound_handle != kLoadError) {
    // 「石を置く設置したときの音」をサウンドマネージャに積む
    sound = sound_manager_->AddSound(SoundId::kSetStone);
    if (sound != nullptr) {
      // サウンドハンドル、サウンドの種類、サウンド効果の種類を設定する
      sound->SetSoundHandle(sound_handle);
      sound->SetSoundPlayType(Sound::PlaySoundType::kSoundEffect);
      sound->SetEffectType(Sound::EffectType::kNone);
      sound->SetSoundVolume();
    }
  }

  start_time = GetNowCount();

  // 音をロードする
  sound_handle = LoadSoundMem(kSoundPathBgm);

  end_time = GetNowCount();
  diff_time = (end_time - start_time) / kThousand;
  std::cout << "BattleLevel CreateSound ロード処理時間：" << diff_time << std::endl;
  std::cout << "BattleLevel CreateSound ロード戻り値：" << sound_handle << std::endl;

  if (sound_handle != kLoadError) {
    // 「石を置く設置したときの音」をサウンドマネージャに積む
    sound = sound_manager_->AddSound(SoundId::kBattleBGM);
    if (sound != nullptr) {
      // サウンドハンドル、サウンドの種類、サウンド効果の種類を設定する
      sound->SetSoundHandle(sound_handle);
      sound->SetSoundPlayType(Sound::PlaySoundType::kBackGroundMusic);
      sound->SetEffectType(Sound::EffectType::kNone);
      sound->SetSoundVolume();
    }
  }

  return true;
}

/// <summary>
/// 準備フェーズ
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:異常終了 </returns>
bool BattleLevel::Preparate() {

  //----------------------------------------
  //石の初期配置処理
  //----------------------------------------
  // 黒色の石、白色の石 初期配置数
  int initial_black_stone = Common::initial_black_stone;
  int initial_white_stone = Common::initial_white_stone;
  // 黒色の石の初期配置処理
  for (int i = 0; i < initial_black_stone; ++i) {
    PutStoneInitialPlace(Stone::StoneType::kBlack);
  }
  // 白色の石の初期配置処理
  for (int i = 0; i < initial_white_stone; ++i) {
    PutStoneInitialPlace(Stone::StoneType::kWhite);
  }

  int black_stone, white_stone;
  // 盤上の石の数を取得
  field_->CountStone(black_stone, white_stone);
  battle_info_ui_->SetStoneCount(black_stone, white_stone);
  // ゲームモードのフェーズを「配置可能場所確認」に変更する
  game_mode_->ChangeTurnPhase(GameMode::TurnPhase::kCheckPlaceablePlace);

  //----------------------------------------
  //サウンドの生成
  //----------------------------------------
  std::cout << "BattleLevel InitializeSoundManager サウンド生成" << std::endl;
  int start_time = GetNowCount();

  // サウンドを生成
  CreateSound();

  int end_time = GetNowCount();
  float diff_time = (end_time - start_time) / kThousand;
  std::cout << "BattleLevel InitializeSoundManager CreateSound処理時間：" << diff_time << std::endl;

  return true;
}

/// <summary>
/// マウスの座標取得イベント
/// </summary>
/// <param name=""> X座標 </param>
/// <param name=""> Y座標 </param>
/// <returns></returns>
void BattleLevel::OnGetMousePosition(Cursor* cursor, int x_pos, int y_pos) {

  // 現在のフェーズが「プレイ中」以外なら処理終了
  if (current_phase_ != PhaseType::kPlay) {
    return;
  }

  // 現在のターンフェーズが「思考中」以外なら処理終了
  if (!game_mode_->GetCurrentPhaseThink()) {
    return;
  }

  // ゲーム情報を取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  // 操作モードを取得、オートモードなら処理終了
  bool is_manual = game_info->IsOperationModeMaual();
  if (!is_manual) {
    return;
  }

  // 現在のターンを取得
  GameMode::PlayerTurn player_turn = game_mode_->GetPlayerTurn();
  // カーソルの種類を取得
  Cursor::CursorType cursor_type = cursor->GetCursorType();

  switch (player_turn) {
  //case GameMode::PlayerTurn::kPlayer: {
  //  if (cursor_type != Cursor::CursorType::kPlayer) {
  //    std::cout << "OnGetMousePosition 処理終了：自分のターン以外" << std::endl;
  //    return;
  //  }
  //  break;
  //}
  case GameMode::PlayerTurn::kCpu: {
    if (cursor_type != Cursor::CursorType::kEnemy) {
      //std::cout << "OnPushMouseLeftButton 処理終了：自分のターン以外" << std::endl;
      return;
    }
    break;
  }
  }

  // 現在のカーソル位置を取得
  int old_x_pos = cursor->GetPositionX();
  int old_y_pos = cursor->GetPositionY();

  // 対象座標が設置可能な座標かどうか確認し、設置可能であれば返す石のカーソルを表示
  // 設置不可であれば表示済の返す石のカーソルを非表示にする
  bool check_canput = field_->CheckCanPut(old_x_pos, old_y_pos);
  if ((check_canput)) {
    // ひっくり返す石の座標のカーソルを非表示にする
    ChangeStatusTurnOverCursor(old_x_pos, old_y_pos, kHideStatus);
  }

  int x_pos_board = 0;
  int y_pos_board = 0;

  // マウスカーソルが存在する座標(盤面の座標)を取得する
  bool check_exist = LocateMouseCursorPosition(*cursor, x_pos, y_pos, x_pos_board, y_pos_board);
  if (!check_exist) {
    // カーソルに非表示をセット
    cursor->SetDisplay(kHideStatus);
    return;
  }

  // カーソルに座標をセット
  cursor->SetPositionX(x_pos_board);
  cursor->SetPositionY(y_pos_board);

  if (game_mode_->GetCurrentPhaseThink() &&
      game_mode_->GetPlayerTurn() == GameMode::PlayerTurn::kPlayer) {
    // カーソルに表示をセット
    cursor->SetDisplay(kShowStatus);
  }

  // 対象座標が設置可能な座標かどうか確認し、設置可能であれば返す石のカーソルを表示
  // 設置不可であれば表示済の返す石のカーソルを非表示にする
  check_canput = field_->CheckCanPut(x_pos_board, y_pos_board);
  if ((check_canput)) {
    // ひっくり返す石の座標にカーソルを表示
    ChangeStatusTurnOverCursor(x_pos_board, y_pos_board, kShowStatus);
  }

}

/// <summary>
/// ひっくり返す石のカーソルの表示する
/// </summary>
/// <param name="x_pos"> X座標 </param>
/// <param name="y_pos"> Y座標 </param>
/// <param name=""> 表示状態 </param>
/// <returns></returns>
void BattleLevel::ChangeStatusTurnOverCursor(int x_pos, int y_pos, bool disp_status) {

  // 設置座標をセットする
  std::pair<int, int> set_pos(x_pos, y_pos);
  // ひっくり返す石の数を取得する
  int turn_over_count = field_->GetTurnOverCount(set_pos);

  for (int i = 0; i < turn_over_count; ++i) {
    // ひっくり返す石の座標を取得する
    std::pair<int, int> pos = field_->GetTurnOverPosition(set_pos, i);
    TurnOverCursor* turn_over_cursor = cursor_manager_->GetTurnOverCursor(pos);
    // 指定カーソルの表示状態を変更する
    turn_over_cursor->SetDisplay(disp_status);
  }
}

/// <summary>
/// マウスの左クリックが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushMouseLeftButton(Cursor* cursor, int x_pos, int y_pos) {

  bool continue_process = false;

  // 現在のフェーズが「プレイ中」以外なら処理終了
  if (current_phase_ != PhaseType::kPlay) {
    return;
  }

  // 現在のターンフェーズが「思考中」以外なら処理終了
  continue_process = game_mode_->GetCurrentPhaseThink();
  if (!continue_process) {
    std::cout << "OnPushMouseLeftButton 処理終了：「思考中」以外のフェーズ" << std::endl;
    return;
  }

  // 現在のターンを取得
  GameMode::PlayerTurn player_turn = game_mode_->GetPlayerTurn();
  // カーソルの種類を取得
  Cursor::CursorType cursor_type = cursor->GetCursorType();

  int x_pos_board = 0;
  int y_pos_board = 0;

  // ゲーム情報を取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  GameInfo::OperationMode operation_mode = game_info->GetOperationMode();
  switch (operation_mode) {
  case GameInfo::OperationMode::kManual:{
    //-----------------------------------
    //手動処理モード
    //-----------------------------------
    switch (player_turn) {
    case GameMode::PlayerTurn::kPlayer: {

      if (cursor_type != Cursor::CursorType::kPlayer) {
        std::cout << "OnPushMouseLeftButton 処理終了：自分のターン以外" << std::endl;
        return;
      }
      break;
    }
    case GameMode::PlayerTurn::kCpu: {
      if (cursor_type != Cursor::CursorType::kEnemy) {
        std::cout << "OnPushMouseLeftButton 処理終了：自分のターン以外" << std::endl;
        return;
      }
      break;
    }
    }

    switch (cursor->GetCursorType()) {
    case Cursor::CursorType::kPlayer: {
      // マウスカーソルが存在する座標(盤面の座標)を取得する
      continue_process = LocateMouseCursorPosition(*cursor, x_pos, y_pos, x_pos_board, y_pos_board);
      if (!continue_process) {
        std::cout << "OnPushMouseLeftButton 処理終了：カーソル位置が盤上外 座標(" << x_pos << "," << y_pos << ")" << std::endl;
        return;
      }
      break;
    }
    case Cursor::CursorType::kEnemy: {
      x_pos_board = x_pos;
      y_pos_board = y_pos;
      break;
    }
    }
    break;
  }
  case GameInfo::OperationMode::kAuto: {
    //-----------------------------------
    //自動処理モード
    //-----------------------------------
    x_pos_board = x_pos;
    y_pos_board = y_pos;
    break;
  }
  }

  // 配置可能かチェックする
  continue_process = field_->CheckCanPut(x_pos_board, y_pos_board);
  if (!continue_process) {
    std::cout << "OnPushMouseLeftButton 処理終了：配置不可 座標(" << x_pos_board << "," << y_pos_board << ")" << std::endl;
    return;
  }

  // ひっくり返す石の座標にカーソルを非表示にする
  ChangeStatusTurnOverCursor(x_pos_board, y_pos_board, kHideStatus);

  // 石の種類を取得する
  Stone::StoneType stone_type = game_mode_->GetStoneType(player_turn);
  // 石を配置する
  Stone* stone = stone_manager_->GetUnplacedStone(stone_type);
  field_->PutStone(*stone, x_pos_board, y_pos_board);
  // 配置済みの石としてセット
  stone->SetPlaced();
  stone_manager_->SetPlacedStone(stone_type);

  // 石を置いた時の音を再生リストにセット
  sound_manager_->SetPlaySoundList(SoundId::kSetStone);

  // ゲームモードのフェーズを「石を設置」に変更する
  game_mode_->ChangeTurnPhase(GameMode::TurnPhase::kPutStone);
}

/// <summary>
/// マウスカーソルが存在する座標(盤面の座標)を取得する
/// </summary>
/// <param name=""></param>
/// <returns> true:座標取得, false:座標取得不可 </returns>
bool BattleLevel::LocateMouseCursorPosition(Cursor& cursor, int x_pos, int y_pos, int& x_pos_board, int& y_pos_board) {

  // フィールド処理が「処理中フェーズ」以外なら処理終了
  bool check_process = field_->IsProcess();
  if (!check_process) {
    return false;
  }

  // 盤上にカーソルがあるかどうかを確認、存在しないなら処理終了
  bool check_exist = field_->CheckBoardDisplayRange(x_pos, y_pos);
  if (!check_exist) {
    return false;
  }

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();

  if (game_info == nullptr) {
    return false;
  }
  // マップ表示位置を取得(マップ表示時の調整値)
  int upper_left_x = game_info->GetMapPositionX();
  int upper_left_y = game_info->GetMapPositionY();

  // カーソルの画像サイズを取得
  int image_width = cursor.GetImageWidth();
  int image_height = cursor.GetImageHeight();

  // マウスカーソルの位置から盤面上の座標を取得
  // ※マップ表示時の調整値を引き、調整前の位置で座標を取得
  x_pos_board = (x_pos - upper_left_x) / image_width;
  y_pos_board = (y_pos - upper_left_y) / image_height;

  return true;
}

/// <summary>
/// 未配置の石があるかどうかの確認
/// </summary>
/// <param name=""></param>
/// <returns> true:未配置の石がある, false:未配置の石はない </returns>
bool BattleLevel::OnIsUnplaceStone() {

  // 現在のターンを取得する
  GameMode::PlayerTurn player_turn = game_mode_->GetPlayerTurn();
  Stone::StoneType stone_type = game_mode_->GetStoneType(player_turn);

  // 未配置の石が存在するか確認する
  bool is_exists = stone_manager_->IsUnPlacedStone(stone_type);

  return is_exists;
}

/// <summary>
/// カーソルの非表示設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnHideCursor() {

  // 配置可能箇所表示カーソルを非表示にする
  ChangeStatusPlaceableCursor(kHideStatus);
}

/// <summary>
/// カーソルの表示設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnShowCursor() {

  // 現在のフェーズが「プレイ中」以外なら処理終了
  if (current_phase_ != PhaseType::kPlay) {
    return;
  }

  // 配置可能箇所表示カーソルを表示する
  ChangeStatusPlaceableCursor(kShowStatus);
  // プレイヤー、CPUのカーソルを表示する
  ChangeStatusPlayerEnemyCursor();

  // 現在のターンを取得し、HUD情報に渡す
  GameMode::PlayerTurn player_turn = game_mode_->GetPlayerTurn();
  battle_info_ui_->SetCurrentTurn(player_turn);
}

/// <summary>
/// 配置可能箇所表示カーソルの表示状態を変更
/// </summary>
/// <param name="disp_status"> 表示状態 </param>
/// <returns></returns>
void BattleLevel::ChangeStatusPlaceableCursor(bool disp_status) {

  // ゲーム情報を取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  GameInfo::OperationMode operation_mode = game_info->GetOperationMode();
  switch (operation_mode) {
  case GameInfo::OperationMode::kAuto: {
    // オートモードの場合は処理終了
    return;
  }
  case GameInfo::OperationMode::kManual: {
    // 現在のターンを取得し、プレイヤーターン以外なら処理終了
    GameMode::PlayerTurn player_turn = game_mode_->GetPlayerTurn();
    if (player_turn != GameMode::PlayerTurn::kPlayer) {
      return;
    }
    break;
  }
  }

  // 配置可能箇所の数を取得する
  int placeable_count = field_->GetPlaceablePlaceCount();

  for (int i = 0; i < placeable_count; ++i) {
    // 配置可能座標を取得する
    std::pair<int, int> pos = field_->GetPlaceablePlacePosition(i);
    PlaceableCursor* placeable_cursor = cursor_manager_->GetPlaceableCursor(pos);
    // 指定カーソルの表示状態を変更する
    placeable_cursor->SetDisplay(disp_status);
  }
}

/// <summary>
/// プレイヤー、CPUカーソルの表示状態を変更
/// </summary>
/// <param name=""> 表示状態 </param>
/// <returns></returns>
void BattleLevel::ChangeStatusPlayerEnemyCursor() {

  // 各カーソルを取得
  PlayerCursor* player_cursor = cursor_manager_->GetPlayerCursor();
  //EnemyCursor* enemy_cursor = cursor_manager_->GetEnemyCursor();

  // 現在のターンを取得する
  GameMode::PlayerTurn player_turn = game_mode_->GetPlayerTurn();
  switch (player_turn) {
  case GameMode::PlayerTurn::kPlayer: {
    // ゲーム情報を取得
    GameInfo* game_info = GameInfo::GetGameInfoInstance();
    GameInfo::OperationMode mode = game_info->GetOperationMode();
    switch (mode) {
    case GameInfo::OperationMode::kManual: {
      player_cursor->SetDisplay(kShowStatus);
      //enemy_cursor->SetDisplay(kHideStatus);
      break;
    }
    case GameInfo::OperationMode::kAuto: {
      player_cursor->SetDisplay(kHideStatus);
      //enemy_cursor->SetDisplay(kShowStatus);
      // CPUの現在のフェーズを「思考」フェーズに変更
      enemy_->ChangeCurrentPhase(Enemy::PhaseType::kThink);
      break;
    }
    }
    break;
  }
  case GameMode::PlayerTurn::kCpu: {
    player_cursor->SetDisplay(kHideStatus);
    //enemy_cursor->SetDisplay(kShowStatus);
    // CPUの現在のフェーズを「思考」フェーズに変更
    enemy_->ChangeCurrentPhase(Enemy::PhaseType::kThink);
    break;
  }
  }
};

/// <summary>
/// 返す石のセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnTurnOverStone() {

  // 現在のターンフェーズが「石を返す」以外なら処理終了
  bool is_check = game_mode_->GetCurrentPhaseTurnOver();
  if (!is_check) {
    return;
  }

  // 直近設置した石を取得する
  Stone* stone = stone_manager_->GetPlacedStone();

  // 設置した石の座標を取得
  int x_pos = stone->GetPositionX();
  int y_pos = stone->GetPositionY();
  std::pair<int, int> pos(x_pos, y_pos);

  // ひっくり返す石の座標を石配置用フィールド情報に反映する
  bool is_finish = field_->TurnOverStone(pos);

  int black_stone, white_stone = 0;
  // 盤上の石の数を取得
  field_->CountStone(black_stone, white_stone);
  battle_info_ui_->SetStoneCount(black_stone, white_stone);

  // 石を置いた時の音を再生リストにセット
  sound_manager_->SetPlaySoundList(SoundId::kSetStone);

  if (is_finish) {
    // 現在のフェーズを「石操作終了」に変更する
    game_mode_->ChangeTurnPhase(GameMode::TurnPhase::kFinishProcess);
  }
}

/// <summary>
/// 配置可能場所の確認
/// </summary>
/// <param name=""></param>
/// <returns> true:配置可能場所あり, false:配置可能場所なし </returns>
bool BattleLevel::OnCheckPlaceablePlace() {

  // 現在のターンを取得
  GameMode::PlayerTurn player_turn = game_mode_->GetPlayerTurn();
  // 現在のターンの石の種類を取得
  Stone::StoneType stone_type = game_mode_->GetStoneType(player_turn);

  // チェック処理は「配置可能マス探索処理」実装時に記述
  bool is_placeable = field_->IsPlaceablePlace(stone_type);

  return is_placeable;
}

/// <summary>
/// CPUの思考処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnThinkPlacement() {

  // 処理開始時間を取得
  int start_time = GetNowCount();
  // 現在のターンを取得し、現在のターンの石の種類を取得
  GameMode::PlayerTurn player_turn = game_mode_->GetPlayerTurn();
  Stone::StoneType stone_type = game_mode_->GetStoneType(player_turn);

  int x_pos = 0;
  int y_pos = 0;

  // 評価値の計算処理を行い、配置場所を確定
  field_->CalculateScore(stone_type, x_pos, y_pos);
  // CPUの石を置く座標に値をセットする
  enemy_->SetPositionX(x_pos);
  enemy_->SetPositionY(y_pos);

  // 処理終了時間を取得し、処理時間を出力する
  int end_time = GetNowCount();
  int diff_time = end_time - start_time;
  float diff_time_sec = diff_time / 1000.0f;
  std::cout << "処理時間：" << diff_time_sec << std::endl;
}

/// <summary>
/// カーソルに表示座標をセット
/// </summary>
/// <param name=""> X座標 </param>
/// <param name=""> Y座標 </param>
/// <returns></returns>
void BattleLevel::OnSetCursorPosition(int x_pos, int y_pos) {

  EnemyCursor* enemy_cursor = cursor_manager_->GetEnemyCursor();
  enemy_cursor->SetPositionX(x_pos);
  enemy_cursor->SetPositionY(y_pos);
}

/// <summary>
/// ゲーム終了
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnFinishGame() {

  // 現在のフェーズを「ゲーム終了」に変更
  ChangeCurrentPhase(PhaseType::kFinishUiDisplay);
}

/// <summary>
/// UI終了のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnFinishDisplayUi() {

  switch (current_phase_) {
  case PhaseType::kStartUiFinishWait: {
    // スタートUIをタスクマネージャから降ろす
    TaskId task_id = start_ui_->GetTaskId();
    Task* release_task = task_manager_.ReleaseTask(task_id);

    // 現在のフェーズを「プレイ中」に変更
    ChangeCurrentPhase(PhaseType::kPlay);
    // 各種カーソルを表示する
    OnShowCursor();

    break;
  }
  case PhaseType::kFinishUiFinishWait: {
    // フィニッシュUIをタスクマネージャから降ろす
    TaskId task_id = finish_ui_->GetTaskId();
    Task* release_task = task_manager_.ReleaseTask(task_id);

    // 背景の現在のフェーズを「終了フェーズ」に変更
    // 背景の描画フェーズを「終了フェーズ」に変更
    back_screen_->ChangePhaseType(BackScreen::PhaseType::kFinalize);
    back_screen_->ChangeDrawPhase(BackScreen::DrawPhaseType::kFinish);
    // 石の描画フェーズを「終了フェーズ」に変更
    stone_manager_->SetStoneDrawPhaseFinish();
    // フィールド処理の描画フェーズを「終了フェーズ」に変更
    field_->ChangeDrawPhase(Field::DrawPhaseType::kFinish);
    // HUD情報の描画フェーズを「終了フェーズ」に変更
    battle_info_ui_->ChangeDrawPhase(BattleInfoUi::DrawPhaseType::kFinish);
    break;
  }
  }
}


/// <summary>
/// バトルレベルのフェーズが「プレイ中」かどうか
/// </summary>
/// <param name=""></param>
/// <returns></returns>
bool BattleLevel::OnCheckBattleLevelPhasePlay() {

  return current_phase_ == PhaseType::kPlay;
}

/// <summary>
/// プレイヤーの先攻/後攻をランダムで決定する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::DecideFirstAttackRandom() {

  // プレイヤー先攻を初期値として設定
  Stone::StoneType player_stone = Stone::StoneType::kBlack;
  Stone::StoneType cpu_stone = Stone::StoneType::kWhite;
  GameMode::PlayerTurn player_turn = GameMode::PlayerTurn::kPlayer;

  if (Common::first_attack_decide) {
    // ランダム生成器を作成
    std::random_device seed_gen;
    std::mt19937 engine(seed_gen());
    // ランダムに取得する値の範囲(実数)
    std::uniform_int_distribution<int> dist(static_cast<int>(Stone::StoneType::kBlack),
                                            static_cast<int>(Stone::StoneType::kWhite));

    // 先攻/後攻を取得する
    int order = dist(engine);

    switch (static_cast<Stone::StoneType>(order)) {
      // 白色の石だった場合は情報を書き換える
    case Stone::StoneType::kWhite: {
      player_stone = Stone::StoneType::kWhite;
      cpu_stone = Stone::StoneType::kBlack;
      player_turn = GameMode::PlayerTurn::kCpu;
      break;
    }
    }
  }

  // プレイヤーと石の情報の設定
  game_mode_->SetPlayerData(GameMode::PlayerTurn::kPlayer, player_stone);
  battle_info_ui_->SetGameData(GameMode::PlayerTurn::kPlayer, player_stone);
  game_mode_->SetPlayerData(GameMode::PlayerTurn::kCpu, cpu_stone);
  battle_info_ui_->SetGameData(GameMode::PlayerTurn::kCpu, cpu_stone);
  // ターンの設定
  game_mode_->SetPlayerTurn(player_turn);
  battle_info_ui_->SetCurrentTurn(player_turn);
}

/// <summary>
/// 石の初期配置処理
/// </summary>
/// <param name="stone_type"> 石の種類 </param>
/// <returns></returns>
void BattleLevel::PutStoneInitialPlace(Stone::StoneType stone_type) {

  // 石を取り出す
  Stone* stone = stone_manager_->GetUnplacedStone(stone_type);
  // フィールド処理から石の配置場所を取得する
  bool check_success = field_->PutInitialPositionStone(*stone);
  if (check_success) {
    // 配置済みの石としてセット
    stone->SetPlaced();
    stone_manager_->SetPlacedStone(stone_type);
  }
}

/// <summary>
/// マウスの座標取得イベント
/// </summary>
/// <param name="x_pos"> X座標 </param>
/// <param name="y_pos"> Y座標 </param>
/// <param name="is_push"> 押されているかどうか </param>
/// <returns></returns>
void BattleLevel::OnGetMousePosition(int x_pos, int y_pos, bool is_push) {

  // 現在のフェーズが「手番選択」以外なら処理終了
  if (current_phase_ != PhaseType::kSelectTurn &&
      current_phase_ != PhaseType::kSelectedTurn) {
    return;
  }

  // ボタン上にマウスカーソルが存在するか確認
  bool is_exists = turn_select_menu_->CheckMouseCursorPosition(x_pos, y_pos, is_push);
  //if (is_exists) {
  //  std::cout << "ボタン上あり！：座標(" << x_pos << "," << y_pos << ")" << std::endl;
  //}
  //else {
  //  std::cout << "ボタン上なし：座標(" << x_pos << "," << y_pos << ")" << std::endl;
  //}
}

/// <summary>
/// マウスの左クリックが押された時のイベント
/// </summary>
/// <param name="x_pos"> X座標 </param>
/// <param name="y_pos"> Y座標 </param>
/// <returns></returns>
void BattleLevel::OnPushMouseLeftButton(int x_pos, int y_pos) {

  // 現在のフェーズが「手番選択」以外なら処理終了
  if (current_phase_ != PhaseType::kSelectTurn) {
    return;
  }

  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // 手番の種類を取得
  GameInfo::TurnType turn_type = turn_select_menu_->GetExistsCursorButton();

  // 何も選択されていない場合は処理終了
  if (turn_type == GameInfo::TurnType::kNone) {
    return;
  }

  switch (turn_type) {
  case GameInfo::TurnType::kFirstAttack: {
    // ゲーム情報のプレイヤー手番を先攻でセットする
    game_info->SetTurnType(GameInfo::TurnType::kFirstAttack);
    break;
  }
  case GameInfo::TurnType::kSecondAttack: {
    // ゲーム情報のプレイヤー手番を後攻でセットする
    game_info->SetTurnType(GameInfo::TurnType::kSecondAttack);
    break;
  }
  case GameInfo::TurnType::kRandom: {
    // ゲーム情報のプレイヤー手番をランダムでセットする
    game_info->SetTurnType(GameInfo::TurnType::kRandom);
    break;
  }
  }
  // 現在のフェーズを「手番選択終了」に変更
  ChangeCurrentPhase(PhaseType::kSelectedTurn);
  // ボタンがクリックされたことにする
  turn_select_menu_->SetButtonClick();
}

/// <summary>
/// プレイヤーの先攻/後攻を決定する
/// </summary>
/// <param name=""> 手番の種類 </param>
/// <returns></returns>
void BattleLevel::DecideTurnType(GameInfo::TurnType button_type) {

  switch (button_type) {
  case GameInfo::TurnType::kFirstAttack: {
    //-------------------------------------
    //先攻を選択
    //-------------------------------------
    // プレイヤーと石の情報の設定
    game_mode_->SetPlayerData(GameMode::PlayerTurn::kPlayer, Stone::StoneType::kBlack);
    battle_info_ui_->SetGameData(GameMode::PlayerTurn::kPlayer, Stone::StoneType::kBlack);
    game_mode_->SetPlayerData(GameMode::PlayerTurn::kCpu, Stone::StoneType::kWhite);
    battle_info_ui_->SetGameData(GameMode::PlayerTurn::kCpu, Stone::StoneType::kWhite);
    // ターンの設定
    game_mode_->SetPlayerTurn(GameMode::PlayerTurn::kPlayer);
    battle_info_ui_->SetCurrentTurn(GameMode::PlayerTurn::kPlayer);
    break;
  }
  case GameInfo::TurnType::kSecondAttack: {
    //-------------------------------------
    //後攻を選択
    //-------------------------------------
    // プレイヤーと石の情報の設定
    game_mode_->SetPlayerData(GameMode::PlayerTurn::kPlayer, Stone::StoneType::kWhite);
    battle_info_ui_->SetGameData(GameMode::PlayerTurn::kPlayer, Stone::StoneType::kWhite);
    game_mode_->SetPlayerData(GameMode::PlayerTurn::kCpu, Stone::StoneType::kBlack);
    battle_info_ui_->SetGameData(GameMode::PlayerTurn::kCpu, Stone::StoneType::kBlack);
    // ターンの設定
    game_mode_->SetPlayerTurn(GameMode::PlayerTurn::kCpu);
    battle_info_ui_->SetCurrentTurn(GameMode::PlayerTurn::kCpu);
    break;
  }
  case GameInfo::TurnType::kRandom: {
    //-----------------------------------
    //先攻/後攻をランダムで決定する
    //-----------------------------------
    DecideFirstAttackRandom();
    break;
  }
  }
}

/// <summary>
/// 終了処理通知
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnFinishLevelEvent() {

  // 現在のフェーズを「終了処理」に変更
  ChangeCurrentPhase(PhaseType::kFinalize);
}

