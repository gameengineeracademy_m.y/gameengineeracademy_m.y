﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "Game/UiEventInterface.h"
#include "Game/GameInfo.h"

class StartUi : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kDisplay,          // 表示中
    kDisplayFinish,    // 表示終了
    kPhaseMaxIndex     // フェーズ数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> UIイベントインターフェース </param>
  /// <returns></returns>
  StartUi(UiEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~StartUi();

  /// <summary>
  /// グラフィックハンドルをセットする
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandle(int graphic_handle) { start_ui_handle_ = graphic_handle; };

  /// <summary>
  /// グラフィックハンドルを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandle() { return start_ui_handle_; };

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;


private:

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; };


private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// UIイベントインターフェース
  /// </summary>
  UiEventInterface& ui_event_interface_;

  /// <summary>
  /// グラフィックハンドル
  /// </summary>
  int start_ui_handle_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// 描画 横方向移動距離
  /// </summary>
  int move_x_pos_;

  /// <summary>
  /// 描画 透過率
  /// </summary>
  int alpha_;
};