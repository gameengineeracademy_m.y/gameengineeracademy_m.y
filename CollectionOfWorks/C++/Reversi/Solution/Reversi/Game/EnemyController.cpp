﻿#include "Game/EnemyController.h"

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> CPUコントローラのイベントインターフェース </param>
/// <param name=""> カーソル </param>
/// <returns></returns>
EnemyController::EnemyController(EnemyControllerEventInterface& enemy_controller_event_interface,
                                 Cursor* cursor)
  : enemy_controller_event_interface_(enemy_controller_event_interface)
  , cursor_(cursor) {

  // コンソールに出力
  std::cout << "EnemyController コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
EnemyController::~EnemyController() {

  // コンソールに出力
  std::cout << "~EnemyController デストラクタ" << std::endl;
}

/// <summary>
/// 左クリック
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void EnemyController::PushMouseLeftButton() {

  // カーソルの現在の表示位置を取得
  int x_pos = cursor_->GetPositionX();
  int y_pos = cursor_->GetPositionY();
  // 左クリック押下をイベント通知
  enemy_controller_event_interface_.OnPushMouseLeftButton(cursor_, x_pos, y_pos);
}
