﻿#include "Game/EnemyCursor.h"

namespace {

  /// <summary>
  /// 待機時間
  /// </summary>
  const float kWaitingTime = 0.75f;

  /// <summary>
  /// リセット時間
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// カーソルID
  /// </summary>
  const int kCursorId = 0;

  /// <summary>
  /// 画像読み込み時のエラー値
  /// </summary>
  const int kLoadError = -1;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
EnemyCursor::EnemyCursor()
  : Cursor(kCursorId, CursorType::kEnemy)
  , cursor_handle_()
  , size_type_(SizeType::kSizeMax)
  , accumulate_time(0.0f) {

  // コンソールに出力
  std::cout << "EnemyCursor コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
EnemyCursor::~EnemyCursor() {

  // コンソールに出力
  std::cout << "~EnemyCursor デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void EnemyCursor::Update(float process_time) {

  // 累積時間に加算
  accumulate_time += process_time;

  if (accumulate_time >= kWaitingTime) {
    // 累積時間をリセットする
    accumulate_time = kResetTime;
    // 描画するイメージの選択
    switch (size_type_) {
    case SizeType::kSizeMax: {
      size_type_ = SizeType::kSizeMin;
      break;
    }
    case SizeType::kSizeMin: {
      size_type_ = SizeType::kSizeMax;
      break;
    }
    }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void EnemyCursor::Render() {

  // 表示有無を取得し、非表示なら処理終了
  bool is_display = GetDisplay();
  if (!is_display) {
    return;
  }

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();

  if (game_info == nullptr) {
    return;
  }
  // マップ表示位置を取得
  int x_adjustment = game_info->GetMapPositionX();
  int y_adjustment = game_info->GetMapPositionY();

  int x_pos = GetPositionX();
  int y_pos = GetPositionY();
  int image_width = GetImageWidth();
  int image_height = GetImageHeight();

  // 描画処理
  DrawGraph(x_adjustment + x_pos * image_width, y_adjustment + y_pos * image_height, cursor_handle_.at(static_cast<int>(size_type_)), true);
}

/// <summary>
/// グラフィックハンドルを取得する
/// </summary>
/// <param name=""> 配列のインデックス </param>
/// <returns></returns>
int EnemyCursor::GetGraphicHandleCursor(int index) {

  if (index >= cursor_handle_.size()) {
    return kLoadError;
  }
  return cursor_handle_.at(index);
}