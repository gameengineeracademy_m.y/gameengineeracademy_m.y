﻿#pragma once

#include <iostream>

/// <summary>
/// ストーンマネージャイベントインターフェース
/// </summary>
class StoneManagerEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> 起動レベル </param>
  /// <returns></returns>
  StoneManagerEventInterface() {

    // コンソールに出力
    std::cout << "StoneManagerEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~StoneManagerEventInterface() {

    // コンソールに出力
    std::cout << "~StoneManagerEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// 返す石のセット
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnTurnOverStone(){};


private:

};