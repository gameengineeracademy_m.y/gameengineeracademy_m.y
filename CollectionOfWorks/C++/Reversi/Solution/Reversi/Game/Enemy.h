﻿#pragma once

#include "System/Task.h"
#include "Game/GameInfo.h"
#include "Game/EnemyController.h"
#include "Game/EnemyEventInterface.h"
#include <iostream>
#include <random>
#include <vector>

/// <summary>
/// 思考処理
/// </summary>
class Enemy : public Task {
public:

  /// <summary>
  /// 挙動のフェーズ
  /// </summary>
  enum class PhaseType {
    kNone,             // 何もしないフェーズ
    kThink,            // 思考フェーズ
    kThinking,         // 思考中フェーズ
    kFinishThink,      // 思考終了フェーズ
    kPutStone,         // 石を配置するフェーズ
    kPhaseMaxIndex     // フェーズ数
  };

  /// <summary>
  /// 思考処理パターン
  /// </summary>
  enum class ThinkingPattern {
    kNotSet,             // 未設定
    kNormal,             // 普通
    kPatternMaxIndex     // パターン数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> エネミーイベントインターフェース </param>
  /// <param name="enemy_event_interface"> エネミーコントローライベントインターフェース </param>
  /// <param name=""> カーソルの種類 </param>
  /// <returns></returns>
  Enemy(EnemyEventInterface&, EnemyControllerEventInterface&, Cursor*);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~Enemy();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// 思考パターンのセット
  /// </summary>
  /// <param name=""> 思考処理パターン </param>
  /// <returns></returns>
  void SetThinkingPattern(ThinkingPattern pattern) { pattern_ = pattern; }

  /// <summary>
  /// 現在のフェーズを変更
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType);

  /// <summary>
  /// X座標をセットする
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <returns></returns>
  void SetPositionX(int x_pos) { x_pos_ = x_pos; }

  /// <summary>
  /// Y座標をセットする
  /// </summary>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  void SetPositionY(int y_pos) { y_pos_ = y_pos; }

  /// <summary>
  /// 思考処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ThinkPlacement();

  /// <summary>
  /// 思考処理 難易度：普通
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ThinkPlacementLevelNormal();


private:

  /// <summary>
  /// 待機時間をランダムで生成する
  /// </summary>
  /// <param></param>
  /// <returns> 待機時間[sec] </returns>
  float GetRandomWaitTime();

private:

  /// <summary>
  /// エネミーイベントインターフェース
  /// </summary>
  EnemyEventInterface& enemy_event_interface_;

  /// <summary>
  /// CPUコントローラ
  /// </summary>
  EnemyController enemy_controller_;

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 思考処理パターン
  /// </summary>
  ThinkingPattern pattern_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// 思考時間(待機時間)
  /// </summary>
  float thinking_time_;

  /// <summary>
  /// 石を置いてからの待機時間
  /// </summary>
  float waiting_time_;

  /// <summary>
  /// 石の配置座標 X座標
  /// </summary>
  int x_pos_;

  /// <summary>
  /// 石の配置座標 Y座標
  /// </summary>
  int y_pos_;
};