﻿#include "Game/StoneBase.h"

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
StoneBase::StoneBase(int stone_id)
  : stone_id_(stone_id)
  , is_release_(false) {

  // コンソールに出力
  std::cout << "StoneBase コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
StoneBase::~StoneBase() {

  // コンソールに出力
  std::cout << "~StoneBase デストラクタ" << std::endl;
}