﻿#include "Game/TitleLevel.h"

namespace {

  /// <summary>
  /// 累積時間のリセット値
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// タイトルレベルへ遷移する時間 [sec]
  /// </summary>
  const float kWaitTimeSecond = 1.0f;

  /// <summary>
  /// タイトル情報 出力位置 Y座標
  /// </summary>
  const int kInfoOutputPosY = 350;

  /// <summary>
  /// 操作情報 出力位置調整 Y座標
  /// </summary>
  const int kAdjastPosY = 45;

  /// <summary>
  /// 図形の色
  /// </summary>
  const int kStringBackColorGray = GetColor(125, 125, 125);     // 灰色

  /// <summary>
  /// 図形の透過度
  /// </summary>
  const int kShapeAlpha = 200;

  /// <summary>
  /// 図形の表示位置 X座標
  /// </summary>
  const int kShapeDispPosX = 180;

  /// <summary>
  /// 図形の表示位置 Y座標(上)
  /// </summary>
  const int kShapeDispUpperPosY = 75;

  /// <summary>
  /// 図形の表示位置 Y座標(下)
  /// </summary>
  const int kShapeDispLowerPosY = 100;

  /// <summary>
  /// 表示文字列
  /// </summary>
  const char* kDisplayTitleString = "Reversi";

  /// <summary>
  /// 表示文字列
  /// </summary>
  const char* kDisplayString = "Please Click Screen !!";

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 透過率 起動時調整値
  /// </summary>
  const int kAlphaAdjastStart = 30;

  /// <summary>
  /// 透過率 終了時調整値
  /// </summary>
  const int kAlphaAdjastFinish = 5;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 背景 表示倍率
  /// </summary>
  const float kMagnification = 0.8f;

  /// <summary>
  /// 背景 表示角度 [rad]
  /// </summary>
  const float kAngle = 0.0f;

  /// <summary>
  /// 背景画像パス
  /// </summary>
  const char* kImagePathBackScreen = "Assets/Image/title_backscreen.png";

  /// <summary>
  /// 表示文字列の描画有無間隔
  /// </summary>
  const float kDisplayInterval = 0.6f;

  /// <summary>
  /// タイトル文字フォントサイズ
  /// </summary>
  const int kTitleFontSize = 60;

  /// <summary>
  /// タイトル文字幅
  /// </summary>
  const int kTitleFontWidth = 10;

  /// <summary>
  /// 進行説明文字フォントサイズ
  /// </summary>
  const int kFontSize = 30;

  /// <summary>
  /// 進行説明文字幅
  /// </summary>
  const int kFontWidth = 8;

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Meiryo UI";

  /// <summary>
  /// タイトルレベル BGMファイルパス
  /// </summary>
  const char* kImagePathBgm = "Assets/Sound/carnelian.mp3";

  /// <summary>
  /// タイトルレベル SEファイルパス
  /// </summary>
  const char* kImagePathSe = "Assets/Sound/game_start.mp3";

  /// <summary>
  /// 画像読み込み時のエラー値
  /// </summary>
  const int kLoadError = -1;

  /// <summary>
  /// 1000 単位変換に使用
  /// </summary>
  const float kThousand = 1000.0f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> タスクID </param>
/// <param name=""> タスクマネージャ </param>
/// <returns></returns>
TitleLevel::TitleLevel(TaskId task_id, TaskManager& task_manager)
  : Level(task_id, task_manager)
  , accumulate_time_(0.0f)
  , menu_controller_(nullptr)
  , back_screen_(nullptr)
  , sound_manager_(nullptr)
  , current_phase_(DrawPhaseType::kStart)
  , finalize_phase_(FinalizePhaseType::kUnloadTask)
  , alpha_(0)
  , alpha_shape_(0)
  , is_display(true) {

  // コンソールに出力
  std::cout << "TitleLevel コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
TitleLevel::~TitleLevel() {

  // コンソールに出力
  std::cout << "~TitleLevel デストラクタ" << std::endl;
}

/// <summary>
/// 初期化処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool TitleLevel::InitializePhase(float process_time) {

  bool is_success = false;

  // 処理時間をリセットしておく
  accumulate_time_ = kResetTime;

  // 透過度をリセットしておく
  alpha_ = kAlphaMin;
  alpha_shape_ = kAlphaMin;

  // メニューコントローラ 初期設定
  is_success = InitializeMenuController();
  if (!is_success) {
    return false;
  }

  // 背景 初期設定
  is_success = InitializeBackScreen();
  if (!is_success) {
    return false;
  }

  // サウンドマネージャ 初期設定
  is_success = InitializeSoundManager();
  if (!is_success) {
    return false;
  }

  // サウンドSEを流す
  sound_manager_->SetPlaySoundList(SoundId::kTittleBgm);

  return true;
}

/// <summary>
/// 毎フレームの処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool TitleLevel::UpdatePhase(float process_time) {

  accumulate_time_ += process_time;

  switch (current_phase_) {
  case DrawPhaseType::kStart: {
    // 透過率に調整値を加算
    alpha_ += kAlphaAdjastStart;
    alpha_shape_ += kAlphaAdjastStart;
    if (alpha_ >= kAlphaMax) {
      alpha_ = kAlphaMax;
    }
    if (alpha_shape_ >= kShapeAlpha) {
      alpha_shape_ = kShapeAlpha;
    }
    break;
  }
  case DrawPhaseType::kFinish: {
    // 透過率に調整値を減算
    alpha_ -= kAlphaAdjastFinish;
    alpha_shape_ -= kAlphaAdjastFinish;
    if (alpha_ <= kAlphaMin) {
      alpha_ = kAlphaMin;
    }
    if (alpha_shape_ <= kAlphaMin) {
      alpha_shape_ = kAlphaMin;
    }
    break;
  }
  }

  if (accumulate_time_ >= kDisplayInterval) {
    // 累積時間をリセット
    accumulate_time_ = kResetTime;
    // 表示フラグを切り替える
    is_display = !is_display;
  }

  return false;
}

/// <summary>
/// 毎フレームの描画処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleLevel::RenderPhase() {
    
  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // マップ中央の位置を取得(マップ表示時の調整値)
  int x_center = game_info->GetCenterPointX();
  int y_center = game_info->GetCenterPointY();
  
  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_shape_);

  // 図形を描画
  DrawBox(x_center - kShapeDispPosX, y_center - kShapeDispUpperPosY,
          x_center + kShapeDispPosX, y_center + kShapeDispLowerPosY, kStringBackColorGray, true);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

  SetFontSize(kTitleFontSize);
  SetFontThickness(kTitleFontWidth);
  ChangeFont(kFontName);
  ChangeFontType(DX_FONTTYPE_ANTIALIASING);

  // 文字サイズ(幅)を取得
  int string_width = GetDrawStringWidth(kDisplayTitleString, static_cast<int>(strlen(kDisplayTitleString)));
  // 文字色
  int string_forecolor = GetColor(alpha_, alpha_, alpha_);     // 白色

  // 文字列の描画処理
  DrawString(x_center - (string_width / kHalfValue), kInfoOutputPosY, kDisplayTitleString, string_forecolor);

  SetFontSize(kFontSize);
  SetFontThickness(kFontWidth);
  ChangeFont(kFontName);
  ChangeFontType(DX_FONTTYPE_ANTIALIASING);

  int string_width_ex = GetDrawStringWidth(kDisplayString, static_cast<int>(strlen(kDisplayString)));

  // 一定時間ごとに文字列を描画する
  if (is_display) {
    DrawString(x_center - (string_width_ex / kHalfValue), y_center + kAdjastPosY, kDisplayString, string_forecolor);
  }

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
}

/// <summary>
/// 終了処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool TitleLevel::FinishPhase(float process_time) {

  switch (finalize_phase_) {
  case FinalizePhaseType::kUnloadTask: {
    //-------------------------------
    //タスクを降ろすフェーズ
    //-------------------------------
    // 画像のリソースを破棄
    if (back_screen_ != nullptr) {
      DeleteGraph(back_screen_->GetGraphicHandle());       // 背景
    }
    // 音の破棄を行う
    if (sound_manager_ != nullptr) {
      sound_manager_->DisposeSound();
    }

    // タスクIDを取得
    TaskId task_id;
    Task* release_task = nullptr;

    // メニューコントローラをタスクマネージャから降ろす
    if (menu_controller_ != nullptr) {
      task_id = menu_controller_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // 背景をタスクマネージャから降ろす
    if (back_screen_ != nullptr) {
      task_id = back_screen_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // サウンドマネージャをタスクマネージャから降ろす
    if (sound_manager_ != nullptr) {
      task_id = sound_manager_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }

    // 終了フェーズをタスク破棄フェーズに変更
    finalize_phase_ = FinalizePhaseType::kDisposeTask;
    return false;
  
    break;
  }
  case FinalizePhaseType::kDisposeTask: {
    //-------------------------------
    //タスクを破棄するフェーズ
    //-------------------------------
    if (sound_manager_ != nullptr) {
      delete sound_manager_;
      sound_manager_ = nullptr;
    }
    if (back_screen_ != nullptr) {
      delete back_screen_;
      back_screen_ = nullptr;
    }
    if (menu_controller_ != nullptr) {
      delete menu_controller_;
      menu_controller_ = nullptr;
    }
    break;
  }
  }

  return true;
}

/// <summary>
/// マウスの左クリックが押された時のイベント
/// </summary>
/// <param name="x_pos"> X座標 </param>
/// <param name="y_pos"> Y座標 </param>
/// <returns></returns>
void TitleLevel::OnPushMouseLeftButton(int x_pos, int y_pos) {

  // 現在のフェーズを「終了」フェーズに変更
  ChangeCurrentPhase(DrawPhaseType::kFinish);

  // 背景の現在のフェーズを「終了フェーズ」に変更
  // 背景の描画フェーズを「終了フェーズ」に変更
  back_screen_->ChangePhaseType(BackScreen::PhaseType::kFinalize);
  back_screen_->ChangeDrawPhase(BackScreen::DrawPhaseType::kFinish);

  // サウンドBGMを止める
  Sound* sound = sound_manager_->GetSound(SoundId::kTittleBgm);
  if (sound != nullptr) {
    sound->SoundPause();
  }

  // サウンドSEを流す
  sound_manager_->SetPlaySoundList(SoundId::kGameStart);
}

/// <summary>
/// マウスの座標取得イベント
/// </summary>
/// <param name="x_pos"> X座標 </param>
/// <param name="y_pos"> Y座標 </param>
/// <param name="is_push"> 押されているかどうか </param>
/// <returns></returns>
void TitleLevel::OnGetMousePosition(int x_pos, int y_pos, bool is_push) {

}

/// <summary>
/// メニューコントローラ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool TitleLevel::InitializeMenuController() {

  // メニューコントローラの生成
  menu_controller_ = new MenuController(*this);
  if (menu_controller_ == nullptr) {
    std::cout << "TitleLevel InitializeMenuController：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // タスクマネージャにタスクを積む
  task_manager_.AddTask(menu_controller_);

  return true;
}

/// <summary>
/// 背景 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool TitleLevel::InitializeBackScreen() {

  // 画像のロード
  int graphic_handle = LoadGraph(kImagePathBackScreen);
  if (graphic_handle == kLoadError) {
    std::cout << "TitleLevel InitializeBackScreen：画像読み込み失敗" << std::endl;
    OccurredError();
    return false;
  }

  // 背景 生成
  back_screen_ = new BackScreen(kMagnification, kAngle, *this);
  if (back_screen_ == nullptr) {
    std::cout << "TitleLevel InitializeBackScreen：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // グラフィックハンドルを渡す
  back_screen_->SetGraphicHandle(graphic_handle);
  // 透過率の変化量の指定
  back_screen_->SetAlphaAdjustValue(kAlphaAdjastStart, kAlphaAdjastFinish);
  // タスクマネージャにタスクを積む
  task_manager_.AddTask(back_screen_);

  return true;
}

/// <summary>
/// サウンドマネージャ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool TitleLevel::InitializeSoundManager() {

  // サウンドマネージャ 生成
  sound_manager_ = new SoundManager();
  if (sound_manager_ == nullptr) {
    std::cout << "TitleLevel InitializeSoundManager：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // タスクマネージャにタスクを積む
  task_manager_.AddTask(sound_manager_);

  std::cout << "TitleLevel InitializeSoundManager サウンド生成" << std::endl;
  int start_time = GetNowCount();

  // サウンドを生成
  bool is_success = CreateSound();

  int end_time = GetNowCount();
  float diff_time = (end_time - start_time) / kThousand;
  std::cout << "TitleLevel InitializeSoundManager CreateSound処理時間：" << diff_time << std::endl;

  return true;
}

/// <summary>
/// サウンド生成
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool TitleLevel::CreateSound() {

  Sound* sound = nullptr;
  int start_time, end_time;
  float diff_time;

  start_time = GetNowCount();

  // 音をロードする
  int sound_handle = LoadSoundMem(kImagePathBgm);

  end_time = GetNowCount();
  diff_time = (end_time - start_time) / kThousand;
  std::cout << "TitleLevel CreateSound ロード処理時間：" << diff_time << std::endl;
  std::cout << "TitleLevel CreateSound ロード戻り値：" << sound_handle << std::endl;
  if (sound_handle != kLoadError) {
    // 「タイトルレベルBGM」をサウンドマネージャに積む
    sound = sound_manager_->AddSound(SoundId::kTittleBgm);
    if (sound != nullptr) {
      // サウンドハンドル、サウンドの種類、サウンド効果の種類を設定する
      sound->SetSoundHandle(sound_handle);
      sound->SetSoundPlayType(Sound::PlaySoundType::kBackGroundMusic);
      sound->SetEffectType(Sound::EffectType::kNone);
      sound->SetSoundVolume();
    }
  }

  start_time = GetNowCount();

  // 音をロードする
  sound_handle = LoadSoundMem(kImagePathSe);

  end_time = GetNowCount();
  diff_time = (end_time - start_time) / kThousand;
  std::cout << "TitleLevel CreateSound ロード処理時間：" << diff_time << std::endl;
  std::cout << "TitleLevel CreateSound ロード戻り値：" << sound_handle << std::endl;
  if (sound_handle != kLoadError) {
    // 「タイトルレベルSE」をサウンドマネージャに積む
    sound = sound_manager_->AddSound(SoundId::kGameStart);
    if (sound != nullptr) {
      // サウンドハンドル、サウンドの種類、サウンド効果の種類を設定する
      sound->SetSoundHandle(sound_handle);
      sound->SetSoundPlayType(Sound::PlaySoundType::kSoundEffect);
      sound->SetEffectType(Sound::EffectType::kNone);
      sound->SetSoundVolume();
    }
  }

  return true;
}

/// <summary>
/// 終了処理通知
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleLevel::OnFinishLevelEvent() {

  // バトルレベルへ遷移
  ChangeLevel(TaskId::kBattleLevel);
}