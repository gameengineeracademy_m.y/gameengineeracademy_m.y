﻿#pragma once

#include "System/Cursor.h"
#include "Game/EnemyControllerEventInterfac.h"
#include <iostream>

class EnemyController {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> CPUコントローラのイベントインターフェース </param>
  /// <param name=""> カーソル </param>
  /// <returns></returns>
  EnemyController(EnemyControllerEventInterface&, Cursor*);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~EnemyController();

  /// <summary>
  /// 左クリック
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void PushMouseLeftButton();

  /// <summary>
  /// カーソルの種類をセット
  /// </summary>
  /// <param name=""> カーソル </param>
  /// <returns></returns>
  void SetCursor(Cursor* cursor) { cursor_ = cursor; };

private:

  /// <summary>
  /// CPUコントローラのイベントインターフェース
  /// </summary>
  EnemyControllerEventInterface& enemy_controller_event_interface_;

  /// <summary>
  /// カーソルの種類
  /// </summary>
  Cursor* cursor_;

};