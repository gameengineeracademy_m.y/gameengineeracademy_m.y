﻿#pragma once

#include "DxLib.h"
#include "System/Level.h"
#include "System/MenuController.h"
#include "System/BackScreen.h"
#include "System/SoundManager.h"
#include "Game/TitleLevelEventInterface.h"
#include "Game/GameInfo.h"

/// <summary>
/// タイトルレベル
/// </summary>
/// <remarks>
/// タイトル画面のタスク
/// </remarks>
class TitleLevel : public Level, public TitleLevelEventInterface {
public:

  /// <summary>
  /// 描画フェーズ
  /// </summary>
  enum class DrawPhaseType {
    kStart,              // 開始フェーズ
    kFinish,             // 終了フェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

  /// <summary>
  /// 終了フェーズの種類
  /// </summary>
  enum class FinalizePhaseType {
    kUnloadTask,         // タスクを降ろすフェーズ
    kDisposeTask,        // タスクを破棄するフェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> タスクID </param>
  /// <param name=""> タスクマネージャ </param>
  /// <returns></returns>
  TitleLevel(TaskId, TaskManager&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~TitleLevel();

  /// <summary>
  /// マウスの左クリックが押された時のイベント
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  void OnPushMouseLeftButton(int, int) override;

  /// <summary>
  /// マウスの座標取得イベント
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <param name=""> 押されているかどうか </param>
  /// <returns></returns>
  void OnGetMousePosition(int, int, bool) override;

  /// <summary>
  /// 終了処理通知
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnFinishLevelEvent() override;


protected:

  /// <summary>
  /// 初期化処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool InitializePhase(float) override;

  /// <summary>
  /// 毎フレームの処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool UpdatePhase(float) override;

  /// <summary>
  /// 毎フレームの描画処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void RenderPhase() override;

  /// <summary>
  /// 終了処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool FinishPhase(float) override;


private:

  /// <summary>
  /// フェーズを終了フェーズに遷移
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeCurrentPhase(DrawPhaseType phase_type) { current_phase_ = phase_type; };

  /// <summary>
  /// メニューコントローラ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeMenuController();

  /// <summary>
  /// 背景 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeBackScreen();

  /// <summary>
  /// サウンドマネージャ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeSoundManager();

  /// <summary>
  /// サウンド生成
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool CreateSound();

private:

  /// <summary>
  /// 累積処理時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// メニューコントローラ
  /// </summary>
  MenuController* menu_controller_;

  /// <summary>
  /// 背景
  /// </summary>
  BackScreen* back_screen_;

  /// <summary>
  /// サウンドマネージャ
  /// </summary>
  SoundManager* sound_manager_;

  /// <summary>
  /// フェーズの種類
  /// </summary>
  DrawPhaseType current_phase_;

  /// <summary>
  /// 終了フェーズの種類
  /// </summary>
  FinalizePhaseType finalize_phase_;

  /// <summary>
  /// 描画 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 描画 図形透過率
  /// </summary>
  int alpha_shape_;

  /// <summary>
  /// 文字描画フラグ
  /// </summary>
  bool is_display;
};