﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "Game/GameMode.h"
#include <array>
#include <string>

/// <summary>
/// バトル情報(HUD情報)
/// </summary>
class BattleInfoUi : public Task {
public:

  /// <summary>
  /// 描画フェーズ
  /// </summary>
  enum class DrawPhaseType {
    kStart,              // 開始フェーズ
    kFinish,             // 終了フェーズ
    kDrawPhaseMaxIndex   // フェーズ項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  BattleInfoUi();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~BattleInfoUi();

  /// <summary>
  /// 毎フレーム更新処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 毎フレーム描画処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// パネル1のグラフィックハンドルを設定する
  /// </summary>
  /// <param name="graphic_handle"> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandlePanel01(int graphic_handle) { panel_01_handle_ = graphic_handle; };

  /// <summary>
  /// パネル1のグラフィックハンドルを取得する
  /// </summary>
  /// <param></param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandlePanel01() { return panel_01_handle_; }

  /// <summary>
  /// パネル2のグラフィックハンドルを設定する
  /// </summary>
  /// <param name="graphic_handle"> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandlePanel02(int graphic_handle) { panel_02_handle_ = graphic_handle; };

  /// <summary>
  /// パネル2のグラフィックハンドルを取得する
  /// </summary>
  /// <param></param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandlePanel02() { return panel_02_handle_; }

  /// <summary>
   /// 黒色の石のグラフィックハンドルを設定する
   /// </summary>
   /// <param name="graphic_handle"> グラフィックハンドル </param>
   /// <returns></returns>
  void SetGraphicHandleStoneBlack(int graphic_handle) { stone_black_handle_ = graphic_handle; };

  /// <summary>
  /// 黒色の石のグラフィックハンドルを取得する
  /// </summary>
  /// <param></param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandleStoneBlack() { return stone_black_handle_; }

  /// <summary>
  /// 白色の石のグラフィックハンドルを設定する
  /// </summary>
  /// <param name="graphic_handle"> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandleStoneWhite(int graphic_handle) { stone_white_handle_ = graphic_handle; };

  /// <summary>
  /// 白色の石のグラフィックハンドルを取得する
  /// </summary>
  /// <param></param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandleStoneWhite() { return stone_white_handle_; }

  /// <summary>
  /// ターンフレームのグラフィックハンドルを設定する
  /// </summary>
  /// <param name="graphic_handle"> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandleTurnFrame(int graphic_handle) { current_turn_handle_ = graphic_handle; };

  /// <summary>
  /// ターンフレームのグラフィックハンドルを取得する
  /// </summary>
  /// <param></param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandleTurnFrame() { return current_turn_handle_; }

  /// <summary>
  /// 盤上の石の数を取得する
  /// </summary>
  /// <param name=""> 黒色の石の数 </param>
  /// <param name=""> 白色の石の数 </param>
  /// <returns></returns>
  void SetStoneCount(int, int);

  /// <summary>
  /// ゲーム情報をセットする
  /// </summary>
  /// <param name=""> ターンの種類 </param>
  /// <param name=""> 石の種類 </param>
  /// <returns></returns>
  void SetGameData(GameMode::PlayerTurn turn, Stone::StoneType stone_type) { game_data_[turn] = stone_type; };

  /// <summary>
  /// 現在のターンをセットする
  /// </summary>
  /// <param name=""> 現在のターン </param>
  /// <returns></returns>
  void SetCurrentTurn(GameMode::PlayerTurn player_turn) { current_turn_ = player_turn; };

  /// <summary>
  /// 描画フェーズを変更する
  /// </summary>
  /// <param name=""> 描画フェーズ </param>
  /// <returns></returns>
  void ChangeDrawPhase(DrawPhaseType);

private:

  /// <summary>
  /// 画像のサイズを取得する
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <param name=""> 画像の幅 </param>
  /// <param name=""> 画像の高さ </param>
  /// <returns></returns>
  void SetImageSize(int, int&, int&);


private:

  /// <summary>
  /// 現在の描画フェーズ
  /// </summary>
  DrawPhaseType current_draw_phase_;

  /// <summary>
  /// パネル1 グラフィックハンドル
  /// </summary>
  int panel_01_handle_;

  /// <summary>
  /// パネル2 グラフィックハンドル
  /// </summary>
  int panel_02_handle_;

  /// <summary>
  /// 黒色の石 グラフィックハンドル
  /// </summary>
  int stone_black_handle_;

  /// <summary>
  /// 白色の石 グラフィックハンドル
  /// </summary>
  int stone_white_handle_;

  /// <summary>
  /// 現在のターンを表す枠
  /// </summary>
  int current_turn_handle_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// 盤上の黒色の石の数
  /// </summary>
  int black_stone_count_;

  /// <summary>
  /// 盤上の白色の石の数
  /// </summary>
  int white_stone_count_;

  /// <summary>
  /// ゲーム情報
  /// </summary>
  std::unordered_map<GameMode::PlayerTurn, Stone::StoneType> game_data_;

  /// <summary>
  /// 現在のターン
  /// </summary>
  GameMode::PlayerTurn current_turn_;

  /// <summary>
  /// 描画 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// パネル X座標
  /// </summary>
  int x_pos_black_panel_;

  /// <summary>
  /// パネル Y座標
  /// </summary>
  int y_pos_black_panel_;

  /// <summary>
  /// 黒色の石 X座標
  /// </summary>
  int x_pos_black_stone_;

  /// <summary>
  /// 黒色の石 Y座標
  /// </summary>
  int y_pos_black_stone_;

  /// <summary>
  /// 黒色の石の数 X座標
  /// </summary>
  int x_pos_black_count_;

  /// <summary>
  /// 黒色の石の数 Y座標
  /// </summary>
  int y_pos_black_count_;

  /// <summary>
  /// パネル X座標
  /// </summary>
  int x_pos_white_panel_;

  /// <summary>
  /// パネル Y座標
  /// </summary>
  int y_pos_white_panel_;

  /// <summary>
  /// 白色の石 X座標
  /// </summary>
  int x_pos_white_stone_;

  /// <summary>
  /// 白色の石 Y座標
  /// </summary>
  int y_pos_white_stone_;

  /// <summary>
  /// 白色の石の数 X座標
  /// </summary>
  int x_pos_white_count_;

  /// <summary>
  /// 白色の石の数 Y座標
  /// </summary>
  int y_pos_white_count_;

  /// <summary>
  /// プレイヤー文字色
  /// </summary>
  int fontcolor_player_;

  /// <summary>
  /// CPU文字色
  /// </summary>
  int fontcolor_cpu_;

  /// <summary>
  /// CPU文字色
  /// </summary>
  int black_text_width_;

  /// <summary>
  /// CPU文字色
  /// </summary>
  int white_text_width_;

  /// <summary>
  /// ターンフレーム X座標
  /// </summary>
  int x_pos_turn_frame_;

  /// <summary>
  /// ターンフレーム Y座標
  /// </summary>
  int y_pos_turn_frame_;
};