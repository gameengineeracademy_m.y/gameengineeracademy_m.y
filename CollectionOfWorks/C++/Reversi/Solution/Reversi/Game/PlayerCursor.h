﻿#pragma once

#include "System/Cursor.h"
#include "Game/GameInfo.h"
#include <array>

/// <summary>
/// プレイヤーカーソル
/// </summary>
class PlayerCursor : public Cursor {
public:

  /// <summary>
  /// プレイヤーカーソルのサイズ種類
  /// </summary>
  enum class SizeType {
    kSizeMax,         // カーソル最大
    kSizeMin,         // カーソル最小
    kSizeMaxIndex     // カーソルサイズの種類数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  PlayerCursor();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~PlayerCursor();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float);

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render();

  /// <summary>
  /// グラフィックハンドルをセット
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandle(std::array<int, static_cast<int>(SizeType::kSizeMaxIndex)> cursor_handle) { cursor_handle_ = cursor_handle; }

  /// <summary>
  /// グラフィックハンドルを取得する
  /// </summary>
  /// <param name=""> 配列のインデックス </param>
  /// <returns></returns>
  int GetGraphicHandleCursor(int index);


private:

  /// <summary>
  /// グラフィックハンドル
  /// </summary>
  std::array<int,static_cast<int>(SizeType::kSizeMaxIndex)> cursor_handle_;

  /// <summary>
  /// グラフィックの種類
  /// </summary>
  SizeType size_type_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time;
};