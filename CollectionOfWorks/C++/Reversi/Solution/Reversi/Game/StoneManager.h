﻿#pragma once

#include "System/Task.h"
#include "Game/GameMode.h"
#include "Game/StoneManagerEventInterface.h"
#include <unordered_map>
#include <stack>


/// <summary>
/// ストーンマネージャ
/// </summary>
/// <remarks>
/// 生成した石の管理を行うクラス
/// </remarks>
class StoneManager : public Task {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> ストーンマネージャイベントインターフェース </param>
  /// <returns></returns>
  StoneManager(StoneManagerEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~StoneManager();

  /// <summary>
  /// 石を生成する
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool CreateStone();

  /// <summary>
  /// 石を破棄する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DisposeStone();

  /// <summary>
  /// 石を積む
  /// </summary>
  /// <param name=""> 石 </param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool AddStone(Stone*);

  /// <summary>
  /// 石を降ろす
  /// </summary>
  /// <param name=""> ストーンID </param>
  /// <returns> 石 </returns>
  Stone* ReleaseStone(int);

  /// <summary>
  /// 石の1フレームの処理を実行する
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// 石の1フレームの描画処理を実行する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// 未配置の石を取得する
  /// </summary>
  /// <param name=""> 石の種類 </param>
  /// <returns> 未配置の先頭の石 </returns>
  Stone* GetUnplacedStone(Stone::StoneType stone_type) { return unplaced_stone_[stone_type].top(); }

  /// <summary>
  /// 配置済みの石をセットする
  /// </summary>
  /// <param name=""> 石の種類 </param>
  /// <returns></returns>
  void SetPlacedStone(Stone::StoneType stone_type);

  /// <summary>
  /// 直近配置した石を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  Stone* GetPlacedStone() { return placed_stone_.top().first; }

  /// <summary>
  /// 未配置の石があるかどうか確認する
  /// </summary>
  /// <param name=""> 石の種類 </param>
  /// <returns> true:未配置の石がある, false:未配置の石がない </returns>
  bool IsUnPlacedStone(Stone::StoneType stone_type) { return !unplaced_stone_[stone_type].empty(); }

  /// <summary>
  /// 石の総数
  /// </summary>
  /// <param name="index"></param>
  /// <returns> 石の総数 </returns>
  int GetStoneTotalNumber() { return static_cast<int>(stone_list_.size()); }

  /// <summary>
  /// リストから石を取得する
  /// </summary>
  /// <param name="index"> インデックス </param>
  /// <returns> 石の種類 </returns>
  Stone* GetStone(int index) { return stone_list_[index]; }

  /// <summary>
  /// 石の描画フェーズを「終了フェーズ」に変更する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetStoneDrawPhaseFinish();


private:

  /// <summary>
  /// ストーンマネージャイベントインターフェース
  /// </summary>
  StoneManagerEventInterface& stone_manager_event_interface_;

  /// <summary>
  /// 石リスト
  /// </summary>
  std::unordered_map<int, Stone*> stone_list_;

  /// <summary>
  /// 石 未配置管理スタック
  /// </summary>
  std::unordered_map<Stone::StoneType, std::stack<Stone*>> unplaced_stone_;

  /// <summary>
  /// 配置済み管理スタック
  /// </summary>
  /// <remarks> スタック第1引数：石, 第2引数：設置した際の石の種類 </remarks>
  std::stack<std::pair<Stone*, Stone::StoneType>> placed_stone_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;
};