﻿#pragma once

#include "DxLib.h"
#include "System/Common.h"
#include "System/Level.h"
#include "System/CursorManager.h"
#include "System/MenuController.h"
#include "System/PlayerController.h"
#include "System/BackScreen.h"
#include "System/SoundManager.h"
#include "Game/EnemyController.h"
#include "Game/Field.h"
#include "Game/StoneManager.h"
#include "Game/BattleLevelEventInterface.h"
#include "Game/PlaceableCursor.h"
#include "Game/TurnOverCursor.h"
#include "Game/GameMode.h"
#include "Game/Enemy.h"
#include "Game/BattleInfoUi.h"
#include "Game/StartUi.h"
#include "Game/FinishUi.h"
#include "Game/TurnSelectMenu.h"
#include <array>
#include <random>
#include <map>

/// <summary>
/// バトルレベル
/// </summary>
/// <remarks>
/// 実際にゲームを扱うバトル画面のタスク
/// </remarks>
class BattleLevel : public Level, public BattleLevelEventInterface {
public:

  /// <summary>
  /// フェーズの種類一覧
  /// </summary>
  enum class PhaseType {
    kPrepareWait,        // 準備待機フェーズ
    kPrepare,            // 準備中フェーズ
    kPrepared,           // 準備完了フェーズ
    kSelectTurn,         // 手番選択フェーズ
    kSelectedTurn,       // 手番選択終了フェーズ
    kSwitchGameStart,    // ゲーム開始切り替えフェーズ
    kStartUiDisplay,     // スタートUI表示フェーズ
    kStartUiFinishWait,  // スタートUI終了待機フェーズ
    kPlay,               // プレイ中フェーズ
    kFinishUiDisplay,    // フィニッシュUI表示フェーズ
    kFinishUiFinishWait, // フィニッシュUI終了待機フェーズ
    kFinalize,           // 終了処理フェーズ
    kFinalized,          // 終了処理済みフェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

  /// <summary>
  /// 描画フェーズ
  /// </summary>
  enum class DrawPhaseType {
    kStart,              // 開始フェーズ
    kProcess,            // 処理中フェーズ
    kFinish,             // 終了フェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

  /// <summary>
  /// 終了フェーズの種類一覧
  /// </summary>
  enum class FinalizePhaseType {
    kUnloadTask,         // タスクを降ろすフェーズ
    kDisposeTask,        // タスクを破棄するフェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> タスクID </param>
  /// <param name=""> タスクマネージャ </param>
  /// <returns></returns>
  BattleLevel(TaskId, TaskManager&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~BattleLevel();

  /// <summary>
  /// マウスの座標取得イベント
  /// </summary>
  /// <param name=""> カーソル </param>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  void OnGetMousePosition(Cursor*, int, int) override;

  /// <summary>
  /// マウスの左クリックが押された時のイベント
  /// </summary>
  /// <param name=""> カーソル </param>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  void OnPushMouseLeftButton(Cursor*, int, int) override;

  /// <summary>
  /// 未配置の石があるかどうかの確認
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:未配置の石がある, false:未配置の石はない </returns>
  bool OnIsUnplaceStone() override;

  /// <summary>
  /// カーソルを非表示にする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnHideCursor() override;

  /// <summary>
  /// カーソルを表示する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnShowCursor() override;

  /// <summary>
  /// 返す石のセット
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnTurnOverStone() override;

  /// <summary>
  /// 配置可能場所の確認
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:配置可能場所あり, false:配置可能場所なし </returns>
  bool OnCheckPlaceablePlace() override;

  /// <summary>
  /// CPUの思考処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnThinkPlacement() override;

  /// <summary>
  /// カーソルに表示座標をセット
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  void OnSetCursorPosition(int, int) override;

  /// <summary>
  /// ゲーム終了
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnFinishGame() override;

  /// <summary>
  /// UI終了のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnFinishDisplayUi() override;

  /// <summary>
  /// バトルレベルのフェーズが「プレイ中」かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  bool OnCheckBattleLevelPhasePlay() override;

  /// <summary>
  /// マウスの左クリックが押された時のイベント
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  void OnPushMouseLeftButton(int, int) override;

  /// <summary>
  /// マウスの座標取得イベント
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <param name=""> 押されているかどうか </param>
  /// <returns></returns>
  void OnGetMousePosition(int, int, bool) override;

  /// <summary>
  /// 終了処理通知
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnFinishLevelEvent() override;


protected:

  /// <summary>
  /// 初期化処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool InitializePhase(float) override;

  /// <summary>
  /// 毎フレームの処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool UpdatePhase(float) override;

  /// <summary>
  /// 毎フレームの描画処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void RenderPhase() override;

  /// <summary>
  /// 終了処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool FinishPhase(float) override;


private:

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType);

  /// <summary>
  /// フィールド処理 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeField();

  /// <summary>
  /// ストーンマネージャ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeStoneManager();

  /// <summary>
  /// カーソルマネージャ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeCursorManager();

  /// <summary>
  /// メニューコントローラ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeMenuController();

  /// <summary>
  /// プレイヤーコントローラ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializePlayerController();

  /// <summary>
  /// CPU 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeEnemy();

  /// <summary>
  /// ゲームモード 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeGameMode();

  /// <summary>
  /// 背景 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeBackScreen();

  /// <summary>
  /// HUD情報 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeBattleInfoUi();

  /// <summary>
  /// スタートUI 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeStartUi();

  /// <summary>
  /// フィニッシュUI 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeFinishUi();

  /// <summary>
  /// サウンドマネージャ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeSoundManager();

  /// <summary>
  /// 手番選択メニュー 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeTurnSelectMenu();

  /// <summary>
  /// サウンド生成
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool CreateSound();

  /// <summary>
  /// 準備フェーズ
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:異常終了 </returns>
  bool Preparate();

  /// <summary>
  /// プレイヤーの先攻/後攻を決定する
  /// </summary>
  /// <param name=""> 手番の種類 </param>
  /// <returns></returns>
  void DecideTurnType(GameInfo::TurnType);

  /// <summary>
  /// プレイヤーの先攻/後攻をランダムで決定する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DecideFirstAttackRandom();

  /// <summary>
  /// 石の初期配置処理
  /// </summary>
  /// <param name=""> 石の種類 </param>
  /// <returns></returns>
  void PutStoneInitialPlace(Stone::StoneType);

  /// <summary>
  /// ひっくり返す石のカーソルを表示する
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <param name=""> 表示状態 </param>
  /// <returns></returns>
  void ChangeStatusTurnOverCursor(int, int, bool);

  /// <summary>
  /// 配置可能箇所表示カーソルの表示状態を変更
  /// </summary>
  /// <param name=""> 表示状態 </param>
  /// <returns></returns>
  void ChangeStatusPlaceableCursor(bool);

  /// <summary>
  /// プレイヤー、CPUカーソルの表示状態を変更
  /// </summary>
  /// <param name=""> 表示状態 </param>
  /// <returns></returns>
  void ChangeStatusPlayerEnemyCursor();

  /// <summary>
  /// マウスカーソルが存在する座標(盤面の座標)を取得する
  /// </summary>
  /// <param name=""> カーソル </param>
  /// <param name=""> マウスカーソルのX座標 </param>
  /// <param name=""> マウスカーソルのY座標 </param>
  /// <param name=""> 盤面のX座標 </param>
  /// <param name=""> 盤面のY座標 </param>
  /// <returns> true:座標取得, false:座標取得不可 </returns>
  bool LocateMouseCursorPosition(Cursor& cursor, int x_pos, int y_pos, int& x_pos_board, int& y_pos_board);


private:

  /// <summary>
  /// フィールド処理
  /// </summary>
  Field* field_;

  /// <summary>
  /// ストーンマネージャ
  /// </summary>
  StoneManager* stone_manager_;

  /// <summary>
  /// カーソルマネージャ
  /// </summary>
  CursorManager* cursor_manager_;

  /// <summary>
  /// メニューコントローラ
  /// </summary>
  MenuController* menu_controller_;

  /// <summary>
  /// プレイヤーコントローラ
  /// </summary>
  PlayerController* player_controller_;

  /// <summary>
  /// CPU
  /// </summary>
  Enemy* enemy_;

  /// <summary>
  /// ゲームモード
  /// </summary>
  GameMode* game_mode_;

  /// <summary>
  /// 背景
  /// </summary>
  BackScreen* back_screen_;

  /// <summary>
  /// HUD情報
  /// </summary>
  BattleInfoUi* battle_info_ui_;

  /// <summary>
  /// スタートUI
  /// </summary>
  StartUi* start_ui_;

  /// <summary>
  /// フィニッシュUI
  /// </summary>
  FinishUi* finish_ui_;

  /// <summary>
  /// サウンドマネージャ
  /// </summary>
  SoundManager* sound_manager_;

  /// <summary>
  /// 手番選択メニュー
  /// </summary>
  TurnSelectMenu* turn_select_menu_;

  /// <summary>
  /// 現在の終了フェーズ
  /// </summary>
  FinalizePhaseType finalize_phase_;

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;
};