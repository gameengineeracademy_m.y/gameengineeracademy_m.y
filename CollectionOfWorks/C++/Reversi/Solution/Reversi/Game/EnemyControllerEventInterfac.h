﻿#pragma once

#include "System/Cursor.h"
#include <iostream>
#include <vector>

/// <summary>
/// エネミーコントローライベントインターフェース
/// </summary>
class EnemyControllerEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  EnemyControllerEventInterface() {

    // コンソールに出力
    std::cout << "EnemyControllerEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~EnemyControllerEventInterface() {

    // コンソールに出力
    std::cout << "~EnemyControllerEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// 左ボタン押下時の処理
  /// </summary>
  /// <param name=""> カーソル </param>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  virtual void OnPushMouseLeftButton(Cursor*, int, int) = 0;


private:

};