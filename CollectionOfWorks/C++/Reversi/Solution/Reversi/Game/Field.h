﻿#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif

#pragma once

#include "DxLib.h"
#include "System/Common.h"
#include "System/Task.h"
#include "System/Cursor.h"
#include "Game/GameInfo.h"
#include "Game/Stone.h"
#include <vector>
#include <unordered_map>
#include <map>
#include <queue>
#include <stack>
#include <random>
#include <math.h>

namespace {

  /// <summary>
  /// 盤面の画像パス
  /// </summary>
  const char* kImagePathBoard = "Assets/Image/board.png";

  /// <summary>
  /// 盤の枠の画像パス
  /// </summary>
  const char* kImagePathFrame = "Assets/Image/frame.png";

  /// <summary>
  /// 盤枠の種類数
  /// </summary>
  const int kBoardFrame = 2;

  /// <summary>
  /// 盤枠 外枠
  /// </summary>
  const int kOuterFrame = 0;

  /// <summary>
  /// 盤枠 角
  /// </summary>
  const int kCornerFrame = 1;

  /// <summary>
  /// 盤枠の画像の総分割数
  /// </summary>
  const int kFrameDivTotalNum = 2;

  /// <summary>
  /// 盤枠の画像の横方向の分割数
  /// </summary>
  const int kFrameDivXNum = 1;

  /// <summary>
  /// 盤枠の画像の縦方向の分割数
  /// </summary>
  const int kFrameDivYNum = 2;
}

/// <summary>
/// フィールド
/// </summary>
class Field :public Task {
public:

  /// <summary>
  /// フェーズの種類一覧
  /// </summary>
  enum class PhaseType {
    kInitialize,         // 初期化フェーズ
    kProcess,            // 処理フェーズ
    kFinalize,           // 終了フェーズ
    kFinalized,          // 終了済みフェーズ
    kPhaseTypeMaxIndex   // フェーズ数
  };

  /// <summary>
  /// フィールド要素一覧
  /// </summary>
  enum class FieldElement {
    kBoard,              // 盤
    kBlack,              // 黒色の石
    kWhite,              // 白色の石
    kElementMaxIndex     // 要素数
  };

  /// <summary>
  /// 方向指定
  /// </summary>
  enum class FieldDirection {
    kUp,                 // 上
    kRight,              // 右
    kDown,               // 下
    kLeft,               // 左
    kUpperRight,         // 右上
    kLowerRight,         // 右下
    kLowerLeft,          // 左下
    kUpperLeft,          // 左上
    kDirectionMaxIndex   // 方向数
  };

  /// <summary>
  /// 盤上のマスの状態要素一覧
  /// </summary>
  enum class SetStone {
    kNone,               // 何も置かれていない
    kBlack,              // 黒色の石が置かれている
    kWhite,              // 白色の石が置かれている
    kElementMaxIndex     // 要素数
  };

  /// <summary>
  /// 描画フェーズ
  /// </summary>
  enum class DrawPhaseType {
    kStart,              // 開始フェーズ
    kPlay,               // ゲーム中フェーズ
    kFinish,             // 終了フェーズ
    kDrawPhaseMaxIndex   // フェーズ項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  Field();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~Field();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// 盤面のグラフィックハンドルをセットする
  /// </summary>
  /// <param name="handle"> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandleBoard(int handle) { board_handle_ = handle; }

  /// <summary>
  /// 盤面のグラフィックハンドルを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandleBoard() { return board_handle_; }

  /// <summary>
  /// 盤の枠のグラフィックハンドルをセットする
  /// </summary>
  /// <param name="handle"> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandleFrame(std::array<int, kBoardFrame> handle) { frame_handle_ = handle; }

  /// <summary>
  /// 盤の枠のグラフィックハンドルを取得する
  /// </summary>
  /// <param name="index"> インデックス </param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandleFrame(int index) { return frame_handle_.at(index); }

  /// <summary>
  /// 処理フェーズかどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理中フェーズ, false:それ以外 </returns>
  bool IsProcess() { return current_phase_ == PhaseType::kProcess; }

  /// <summary>
  /// 石の初期配置位置にセット
  /// </summary>
  /// <param name=""> 石 </param>
  /// <returns> true:セット成功, false:セット失敗 </returns>
  bool PutInitialPositionStone(Stone&);

  /// <summary>
  /// 石の初期配置位置を取得
  /// </summary>
  /// <param name=""> 石 </param>
  /// <param name=""> 初期位置のX座標 </param>
  /// <param name=""> 初期位置のY座標 </param>
  /// <returns> true:取得成功, false:取得失敗 </returns>
  bool SearchInitialPositionStone(Stone&, int&, int&);

  /// <summary>
  /// マウスカーソルが盤上にあるかチェック
  /// </summary>
  /// <param name=""> マウスのX座標 </param>
  /// <param name=""> マウスのY座標 </param>
  /// <returns> true:盤上にある, false:盤上にない </returns>
  bool CheckBoardDisplayRange(int, int);

  /// <summary>
  /// 指定座標に石が配置済みかどうか確認
  /// </summary>
  /// <param name=""> 盤面上のX座標 </param>
  /// <param name=""> 盤面上のY座標 </param>
  /// <returns> true:配置可能, false:配置不可 </returns>
  bool CheckCanPut(int, int);

  /// <summary>
  /// 石の配置
  /// </summary>
  /// <param name=""> 石 </param>
  /// <param name=""> 盤面上のX座標 </param>
  /// <param name=""> 盤面上のY座標 </param>
  /// <returns></returns>
  void PutStone(Stone&, int, int);

  /// <summary>
  /// はさまれた石をひっくり返す
  /// </summary>
  /// <param name=""> 石設置座標 </param>
  /// <returns> true:処理完了, false:処理未完了 </returns>
  bool TurnOverStone(std::pair<int, int>);

  /// <summary>
  /// 配置可能場所があるかどうか
  /// </summary>
  /// <param name=""> 石の種類 </param>
  /// <returns> true:配置可能場所あり, false:配置可能場所なし </returns>
  bool IsPlaceablePlace(Stone::StoneType);

  /// <summary>
  /// 配置可能場所の探索
  /// </summary>
  /// <param name=""> 石の種類 </param>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <returns> true:配置可能, false:配置可能 </returns>
  bool SearchPlaceablePlace(Stone::StoneType, int, int);

  /// <summary>
  /// 配置可能場所の個数を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 配置可能場所の数 </returns>
  int GetPlaceablePlaceCount() { return static_cast<int>(placeable_place_.size()); }

  /// <summary>
  /// 配置可能場所の座標を取得する
  /// </summary>
  /// <param name="index"> 配列インデックス </param>
  /// <returns> 配置可能座標 </returns>
  std::pair<int, int> GetPlaceablePlacePosition(int index) { return placeable_place_.at(index); }

  /// <summary>
  /// ひっくり返す石の個数を取得する
  /// </summary>
  /// <param name=""> 石を置く座標 </param>
  /// <returns> 配置可能場所の数 </returns>
  int GetTurnOverCount(std::pair<int, int> pos) { return static_cast<int>(turn_over_place_[pos].size()); }

  /// <summary>
  /// ひっくり返す石の座標を取得する
  /// </summary>
  /// <param name="x_pos"> 石を置く座標 </param>
  /// <param name="y_pos"> 配列インデックス </param>
  /// <returns> ひっくり返す石の座標 </returns>
  std::pair<int, int> GetTurnOverPosition(std::pair<int, int> pos, int index) { return turn_over_place_[pos].at(index); }

  /// <summary>
  /// スコア計算
  /// </summary>
  /// <param name=""> 設置する石の種類 </param>
  /// <param name=""> 設置するX座標 </param>
  /// <param name=""> 設置するY座標 </param>
  /// <returns></returns>
  void CalculateScore(Stone::StoneType, int&, int&);

  /// <summary>
  /// 石の数を数える
  /// </summary>
  /// <param name=""> 黒色の石の数 </param>
  /// <param name=""> 白色の石の数 </param>
  /// <returns></returns>
  void CountStone(int&, int&);

  /// <summary>
  /// 描画フェーズを変更する
  /// </summary>
  /// <param name=""> 描画フェーズ </param>
  /// <returns></returns>
  void ChangeDrawPhase(DrawPhaseType draw_phase) { current_draw_phase_ = draw_phase; };


private:

  /// <summary>
  /// フェーズの変更
  /// </summary>
  /// <param name="phase_type"> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; };

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  bool Initialize();

  /// <summary>
  /// 処理中
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  bool Process();

  /// <summary>
  /// 終了処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  bool Finalize();

  /// <summary>
  /// 盤上の評価計算処理
  /// </summary>
  /// <param name=""> フィールド情報(マスごとの重み) </param>
  /// <param name=""> フィールド情報(数値版) </param>
  /// <param name=""> 盤上のマスの状態 </param>
  /// <returns> 評価値 </returns>
  int CalculateBoardScore(std::vector<std::vector<int>>,
                          std::vector<std::vector<SetStone>>,
                          SetStone);

  /// <summary>
  /// 確定石を数える
  /// </summary>
  /// <param name=""> フィールド情報(数値版) </param>
  /// <param name=""> 盤上のマスの状態 </param>
  /// <returns> 評価値 </returns>
  int CountConfirmationStone(std::vector<std::vector<SetStone>>, SetStone);


private:

  /// <summary>
  /// フィールド情報(マップチップ用)
  /// </summary>
  std::vector<std::vector<int>> field_data_;

  /// <summary>
  /// フィールド情報(石配置用)
  /// </summary>
  std::vector<std::vector<Stone*>> field_data_stone_;

  /// <summary>
  /// 配置可能座標
  /// </summary>
  std::vector<std::pair<int,int>> placeable_place_;

  /// <summary>
  /// ひっくり返す座標
  /// </summary>
  std::map<std::pair<int, int>, std::vector<std::pair<int, int>>> turn_over_place_;

  /// <summary>
  /// 方向情報
  /// </summary>
  std::unordered_map<FieldDirection, std::pair<int, int>> direction_data_;

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 現在の描画フェーズ
  /// </summary>
  DrawPhaseType current_draw_phase_;

  /// <summary>
  /// 盤面のグラフィックハンドル
  /// </summary>
  int board_handle_;

  /// <summary>
  /// 盤の枠のグラフィックハンドル
  /// </summary>
  std::array<int, kBoardFrame> frame_handle_;

  /// <summary>
  /// 返す石の範囲
  /// </summary>
  int search_rate_;

  /// <summary>
  /// 描画 透過率
  /// </summary>
  int alpha_;
};