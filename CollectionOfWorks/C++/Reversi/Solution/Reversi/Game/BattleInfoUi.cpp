﻿#include "Game/BattleInfoUi.h"

namespace {

  /// <summary>
  /// プレイヤーテキスト
  /// </summary>
  const char* kPlayerText = "YOU";

  /// <summary>
  /// CPUテキスト
  /// </summary>
  const char* kCpuText = "CPU";

  /// <summary>
  /// 文字の色 黒色
  /// </summary>
  const int kTextForeColorBlack = GetColor(0, 0, 0);   // 黒色

  /// <summary>
  /// 文字の色 白色
  /// </summary>
  const int kTextForeColorWhite = GetColor(255, 255, 255);   // 白色

  /// <summary>
  /// 表示位置 横方向 調整値
  /// </summary>
  const int kDispPosX = 415;

  /// <summary>
  /// 表示位置 プレイヤー側縦方向 調整値
  /// </summary>
  const int kDispPlayerPanelPosY = 170;

  /// <summary>
  /// 表示位置 CPU側縦方向 調整値
  /// </summary>
  const int kDispCpuPanelPosY = 165;

  /// <summary>
  /// プレイヤー情報 表示位置 横方向 調整値
  /// </summary>
  const int kPlayerDataDispPosX = 70;

  /// <summary>
  /// プレイヤー情報 表示位置 縦方向 調整値
  /// </summary>
  const int kPlayerDataDispPosY = 105;

  /// <summary>
  /// CPU情報 表示位置 横方向 調整値
  /// </summary>
  const int kCpuDataDispPosX = 70;

  /// <summary>
  /// CPU情報 表示位置 縦方向 調整値
  /// </summary>
  const int kCpuDataDispPosY = 235;

  /// <summary>
  /// 石 表示位置 プレイヤー側横方向 調整値
  /// </summary>
  const int kPlayerStoneDispPosX = 40;

  /// <summary>
  /// 石 表示位置 プレイヤー側縦方向 調整値
  /// </summary>
  const int kPlayerStoneDispPosY = 40;

  /// <summary>
  /// 石 表示位置 CPU側横方向 調整値
  /// </summary>
  const int kCpuStoneDispPosX = 40;

  /// <summary>
  /// 石 表示位置 CPU側縦方向 調整値
  /// </summary>
  const int kCpuStoneDispPosY = 35;

  /// <summary>
  /// 石の数 表示位置 プレイヤー側横方向 調整値
  /// </summary>
  const int kPlayerCountDispPosX = 40;

  /// <summary>
  /// 石の数 表示位置 プレイヤー側縦方向 調整値
  /// </summary>
  const int kPlayerCountDispPosY = 15;

  /// <summary>
  /// 石の数 表示位置 CPU側横方向 調整値
  /// </summary>
  const int kCpuCountDispPosX = 35;

  /// <summary>
  /// 石の数 表示位置 CPU側縦方向 調整値
  /// </summary>
  const int kCpuCountDispPosY = 10;

  /// <summary>
  /// パネル1 表示倍率
  /// </summary>
  const float kPanel01DispMagnification = 1.0f;

  /// <summary>
  /// パネル2 表示倍率
  /// </summary>
  const float kPanel02DispMagnification = 0.3f;

  /// <summary>
  /// パネル 表示角度
  /// </summary>
  const float kPanelDispAngle = 0.0f;

  /// <summary>
  /// 石 表示倍率
  /// </summary>
  const float kStoneDispMagnification = 0.2f;

  /// <summary>
  /// 石 表示角度
  /// </summary>
  const float kStoneDispAngle = 0.0f;

  /// <summary>
  /// 符号の切り替え
  /// </summary>
  const int kSwitchSign = -1;

  /// <summary>
  /// 累積時間 リセット値
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 透過率 起動時調整値
  /// </summary>
  const int kAlphaAdjustStart = 15;

  /// <summary>
  /// 透過率 終了時調整値
  /// </summary>
  const int kAlphaAdjustFinish = 10;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSize = 50;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 10;

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Meiryo UI";

  /// <summary>
  /// フレーム 表示倍率
  /// </summary>
  const float kFrameMagnification = 1.0f;

  /// <summary>
  /// フレーム 表示角度
  /// </summary>
  const float kFrameDispAngle = 0.0f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param></param>
/// <returns></returns>
BattleInfoUi::BattleInfoUi()
  : Task(TaskId::kBattleInfoUi)
  , current_draw_phase_(DrawPhaseType::kStart)
  , panel_01_handle_(0)
  , panel_02_handle_(0)
  , stone_black_handle_(0)
  , stone_white_handle_(0)
  , current_turn_handle_(0)
  , accumulate_time_(0.0f)
  , black_stone_count_(0)
  , white_stone_count_(0)
  , game_data_()
  , current_turn_()
  , alpha_(0)
  , x_pos_black_panel_(0)
  , y_pos_black_panel_(0)
  , x_pos_black_stone_(0)
  , y_pos_black_stone_(0)
  , x_pos_black_count_(0)
  , y_pos_black_count_(0)
  , x_pos_white_panel_(0)
  , y_pos_white_panel_(0)
  , x_pos_white_stone_(0)
  , y_pos_white_stone_(0)
  , x_pos_white_count_(0)
  , y_pos_white_count_(0)
  , fontcolor_player_(0)
  , fontcolor_cpu_(0)
  , black_text_width_(0)
  , white_text_width_(0)
  , x_pos_turn_frame_(0)
  , y_pos_turn_frame_(0) {

  // コンソールに出力
  std::cout << "BattleInfoUi コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param></param>
/// <returns></returns>
BattleInfoUi::~BattleInfoUi() {

  // コンソールに出力
  std::cout << "~BattleInfoUi デストラクタ" << std::endl;
}

/// <summary>
/// 毎フレーム更新処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void BattleInfoUi::Update(float process_time) {

  accumulate_time_ += process_time;

  switch (current_draw_phase_) {
  case DrawPhaseType::kStart: {
    // 透過率に調整値を加算
    alpha_ += kAlphaAdjustStart;
    if (alpha_ >= kAlphaMax) {
      alpha_ = kAlphaMax;
    }
    break;
  }
  case DrawPhaseType::kFinish: {
    // 透過率に調整値を減算
    alpha_ -= kAlphaAdjustFinish;
    if (alpha_ <= kAlphaMin) {
      alpha_ = kAlphaMin;
    }
    break;
  }
  }

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // 画面の中心座標を取得
  int x_pos_center = game_info->GetCenterPointX();
  int y_pos_center = game_info->GetCenterPointY();

  //-----------------------------------
  // 描画準備
  //-----------------------------------
  // 文字列の描画処理
  SetFontSize(kFontSize);
  SetFontThickness(kFontWidth);
  ChangeFont(kFontName);
  ChangeFontType(DX_FONTTYPE_ANTIALIASING);

  // 文字サイズ(幅)を取得
  int text_player_width = GetDrawStringWidth(kPlayerText, static_cast<int>(strlen(kPlayerText)));
  int text_cpu_width = GetDrawStringWidth(kCpuText, static_cast<int>(strlen(kCpuText)));

  // プレイヤーが使用している石の種類を取得
  Stone::StoneType stone_type = game_data_[GameMode::PlayerTurn::kPlayer];

  // プレイヤーが使用していた石を画面左側へ設定する
  switch (stone_type) {
  case Stone::StoneType::kBlack: {
    // パネル
    x_pos_black_panel_ = x_pos_center - kDispPosX;
    y_pos_black_panel_ = y_pos_center + kDispPlayerPanelPosY;
    x_pos_white_panel_ = x_pos_center + kDispPosX;
    y_pos_white_panel_ = y_pos_center - kDispCpuPanelPosY;
    // 石
    x_pos_black_stone_ = x_pos_black_panel_ - kPlayerStoneDispPosX;
    y_pos_black_stone_ = y_pos_black_panel_ + kPlayerStoneDispPosY;
    x_pos_black_count_ = x_pos_black_panel_ + kPlayerCountDispPosX;
    y_pos_black_count_ = y_pos_black_panel_ + kPlayerCountDispPosY;
    x_pos_white_stone_ = x_pos_white_panel_ - kCpuStoneDispPosX;
    y_pos_white_stone_ = y_pos_white_panel_ + kCpuStoneDispPosY;
    x_pos_white_count_ = x_pos_white_panel_ + kCpuCountDispPosX;
    y_pos_white_count_ = y_pos_white_panel_ + kCpuCountDispPosY;

    fontcolor_player_ = kTextForeColorBlack;
    fontcolor_cpu_ = kTextForeColorWhite;
    break;
  }
  case Stone::StoneType::kWhite: {
    // パネル
    x_pos_black_panel_ = x_pos_center + kDispPosX;
    y_pos_black_panel_ = y_pos_center - kDispCpuPanelPosY;
    x_pos_white_panel_ = x_pos_center - kDispPosX;
    y_pos_white_panel_ = y_pos_center + kDispPlayerPanelPosY;
    // 石
    x_pos_black_stone_ = x_pos_black_panel_ - kPlayerStoneDispPosX;
    y_pos_black_stone_ = y_pos_black_panel_ + kPlayerStoneDispPosY;
    x_pos_black_count_ = x_pos_black_panel_ + kPlayerCountDispPosX;
    y_pos_black_count_ = y_pos_black_panel_ + kPlayerCountDispPosY;
    x_pos_white_stone_ = x_pos_white_panel_ - kPlayerStoneDispPosX;
    y_pos_white_stone_ = y_pos_white_panel_ + kPlayerStoneDispPosY;
    x_pos_white_count_ = x_pos_white_panel_ + kPlayerCountDispPosX;
    y_pos_white_count_ = y_pos_white_panel_ + kPlayerCountDispPosY;

    fontcolor_player_ = kTextForeColorWhite;
    fontcolor_cpu_ = kTextForeColorBlack;
    break;
  }
  }

  //-----------------------------------
  // 石の数の描画準備
  //-----------------------------------
  std::string black_stone_text = std::to_string(black_stone_count_);
  // 文字サイズ(幅)を取得
  black_text_width_ = GetDrawStringWidth(black_stone_text.c_str(), static_cast<int>(strlen(black_stone_text.c_str())));
  std::string white_stone_text = std::to_string(white_stone_count_);
  // 文字サイズ(幅)を取得
  white_text_width_ = GetDrawStringWidth(white_stone_text.c_str(), static_cast<int>(strlen(white_stone_text.c_str())));

  //-----------------------------------
  // ターンフレームの描画準備
  //-----------------------------------
  // 現在のターンの石の種類を取得
  stone_type = game_data_[current_turn_];

  switch (stone_type) {
  case Stone::StoneType::kBlack: {
    x_pos_turn_frame_ = x_pos_black_panel_;
    y_pos_turn_frame_ = y_pos_black_panel_;
    break;
  }
  case Stone::StoneType::kWhite: {
    x_pos_turn_frame_ = x_pos_white_panel_;
    y_pos_turn_frame_ = y_pos_white_panel_;
    break;
  }
  }
}

/// <summary>
/// 毎フレーム描画処理
/// </summary>
/// <param></param>
/// <returns></returns>
void BattleInfoUi::Render() {

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // 画面の中心座標を取得
  int x_pos_center = game_info->GetCenterPointX();
  int y_pos_center = game_info->GetCenterPointY();

  // 石の数を取得する
  std::string black_stone_text = std::to_string(black_stone_count_);
  std::string white_stone_text = std::to_string(white_stone_count_);

  // 文字列の描画処理
  SetFontSize(kFontSize);
  SetFontThickness(kFontWidth);
  ChangeFont(kFontName);
  ChangeFontType(DX_FONTTYPE_ANTIALIASING);

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

  // パネルの描画
  DrawRotaGraph(x_pos_black_panel_, y_pos_black_panel_, kPanel02DispMagnification,
    kPanelDispAngle, panel_02_handle_, true);
  DrawRotaGraph(x_pos_white_panel_, y_pos_white_panel_, kPanel01DispMagnification,
    kPanelDispAngle, panel_01_handle_, true);

  // プレイヤー情報の描画
  DrawString(x_pos_center - kDispPosX - kPlayerDataDispPosX, y_pos_center + kPlayerDataDispPosY, kPlayerText, fontcolor_player_);
  DrawString(x_pos_center + kDispPosX - kCpuDataDispPosX, y_pos_center - kCpuDataDispPosY, kCpuText, fontcolor_cpu_);

  // 石の描画
  DrawRotaGraph(x_pos_black_stone_, y_pos_black_stone_, kStoneDispMagnification,
    kStoneDispAngle, stone_black_handle_, true);
  DrawRotaGraph(x_pos_white_stone_, y_pos_white_stone_, kStoneDispMagnification,
    kStoneDispAngle, stone_white_handle_, true);

  // 石の数の描画
  DrawString(x_pos_black_count_ - (black_text_width_ / kHalfValue), y_pos_black_count_, black_stone_text.c_str(), kTextForeColorBlack);
  DrawString(x_pos_white_count_ - (white_text_width_ / kHalfValue), y_pos_white_count_, white_stone_text.c_str(), kTextForeColorWhite);

  // フレームの描画
  DrawRotaGraph(x_pos_turn_frame_, y_pos_turn_frame_, kFrameMagnification,
    kFrameDispAngle, current_turn_handle_, true);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
}

/// <summary>
/// 描画フェーズを変更する
/// </summary>
/// <param name=""> 描画フェーズ </param>
/// <returns></returns>
void BattleInfoUi::ChangeDrawPhase(DrawPhaseType draw_phase) {

  current_draw_phase_ = draw_phase;
}

/// <summary>
/// 画像のサイズを取得する
/// </summary>
/// <param name=""> グラフィックハンドル </param>
/// <param name=""> 画像の幅 </param>
/// <param name=""> 画像の高さ </param>
/// <returns></returns>
void BattleInfoUi::SetImageSize(int graphic_handle, int& image_width, int& image_height) {

  // グラフィックハンドルから画像のサイズを取得
  GetGraphSize(graphic_handle, &image_width, &image_height);
}

/// <summary>
/// 盤上の石の数を取得する
/// </summary>
/// <param name=""> 黒色の石の数 </param>
/// <param name=""> 白色の石の数 </param>
/// <returns></returns>
void BattleInfoUi::SetStoneCount(int black_stone_count, int white_stone_count) {

  black_stone_count_ = black_stone_count;
  white_stone_count_ = white_stone_count;
}