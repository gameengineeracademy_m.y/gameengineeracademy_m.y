﻿#include "Game/FinishUi.h"

namespace {

  /// <summary>
  /// 累積時間 リセット
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 待機時間
  /// </summary>
  const float kWaitTime = 1.5f;

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 拡大率
  /// </summary>
  const float kMagnification = 0.3f;

  /// <summary>
  /// 角度 [rad]
  /// </summary>
  const float kAngle = 0.0f;

  /// <summary>
  /// UI表示位置 縦方向調整値
  /// </summary>
  const int kUiDisplayY = 65;

  /// <summary>
  /// パネル表示開始時間
  /// </summary>
  const float kShowStart = 0.3f;

  /// <summary>
  /// パネル表示終了開始時間
  /// </summary>
  const float kHideStart = 1.2f;

  /// <summary>
  /// パネル表示時間
  /// </summary>
  const float kShowSkipMessage = 1.5f;

  /// <summary>
  /// 透過率 起動時調整値
  /// </summary>
  const int kAlphaStartAdjast = 20;

  /// <summary>
  /// 透過率 終了時調整値
  /// </summary>
  const int kAlphaFinishAdjast = 15;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 描画 水平方向初期値
  /// </summary>
  const int kInitialPosX = 30;

  /// <summary>
  /// 描画 水平方向移動量
  /// </summary>
  const int kMoveValue = 1;

  /// <summary>
  /// 描画 水平方向最小値
  /// </summary>
  const int kMoveMin = 0;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> UIイベントインターフェース </param>
/// <returns></returns>
FinishUi::FinishUi(UiEventInterface& ui_event_interface)
  : Task(TaskId::kFinishUi)
  , current_phase_(PhaseType::kDisplay)
  , ui_event_interface_(ui_event_interface)
  , finish_ui_handle_(0)
  , accumulate_time_(0.0f)
  , move_x_pos_(kInitialPosX)
  , alpha_(0) {

  // コンソールに出力
  std::cout << "FinishUi コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
FinishUi::~FinishUi() {

  // コンソールに出力
  std::cout << "~FinishUi デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void FinishUi::Update(float process_time) {

  // 累積時間に加算
  accumulate_time_ += process_time;

  switch (current_phase_) {
  case PhaseType::kDisplay: {
    //--------------------------
    //スタートUI表示中フェーズ
    //--------------------------
      // ターンパネル透過率、表示位置計算処理
    if (accumulate_time_ < kShowStart) {
      // 透過率に調整値を加算
      alpha_ += kAlphaStartAdjast;
      if (alpha_ >= kAlphaMax) {
        alpha_ = kAlphaMax;
      }
      // 横方向座標に調整値減算
      move_x_pos_ -= kMoveValue;
      if (move_x_pos_ <= kMoveMin) {
        move_x_pos_ = kMoveMin;
      }
    }
    if (accumulate_time_ >= kHideStart) {
      // 透過率に調整値を減算
      alpha_ -= kAlphaFinishAdjast;
      if (alpha_ <= kAlphaMin) {
        alpha_ = kAlphaMin;
      }
    }

    // 累積時間が指定時間を超えたらUI表示を終了する
    if (accumulate_time_ >= kWaitTime) {
      // 累積時間をリセットする
      accumulate_time_ = kResetTime;
      // 現在のフェーズを「表示終了」に変更する
      ChangeCurrentPhase(PhaseType::kDisplayFinish);

      // バトルレベルへUI表示終了イベントを通知する
      ui_event_interface_.OnFinishDisplayUi();
    }
    break;
  }
  case PhaseType::kDisplayFinish: {
    //--------------------------
    //スタートUI表示終了フェーズ
    //--------------------------
    break;
  }
  }

}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void FinishUi::Render() {

  // 表示中フェーズ以外なら処理終了
  if (current_phase_ != PhaseType::kDisplay) {
    return;
  }

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // 画面の中心座標を取得
  int x_pos_center = game_info->GetCenterPointX();
  int y_pos_center = game_info->GetCenterPointY();

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

  DrawRotaGraph(x_pos_center - move_x_pos_, y_pos_center + kUiDisplayY, kMagnification, kAngle, finish_ui_handle_, true);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
}