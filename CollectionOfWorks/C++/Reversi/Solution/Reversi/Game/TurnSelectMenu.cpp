﻿#include "Game/TurnSelectMenu.h"

namespace {

  /// <summary>
  /// メニュー 幅
  /// </summary>
  const int kMenuWidth = 400;

  /// <summary>
  /// メニュー 高さ
  /// </summary>
  const int kMenuHeight = 375;

  /// <summary>
  /// ボタンUI 幅
  /// </summary>
  const int kButtonUiWidth = 250;

  /// <summary>
  /// ボタンUI 高さ
  /// </summary>
  const int kButtonUiHeight = 60;

  /// <summary>
  /// 先攻ボタンUI 表示位置調整 垂直
  /// </summary>
  const int kFirstAttackButtonUiAdjustVirtical = -50;

  /// <summary>
  /// 後攻ボタンUI 表示位置調整 垂直
  /// </summary>
  const int kSecondAttackButtonUiAdjustVirtical = 40;

  /// <summary>
  /// ランダムボタンUI 表示位置調整 垂直
  /// </summary>
  const int kRandomButtonUiAdjustVirtical = 130;

  /// <summary>
  /// メニュー 背景色
  /// </summary>
  const int kMenuColor = GetColor(0, 66, 255);   // 青色

  /// <summary>
  /// ボタンの色
  /// </summary>
  const int kShapeColorBlue = GetColor(0, 0, 255);   // 青色

  /// <summary>
  /// ボタンの色
  /// </summary>
  const int kShapeColorMiddleBlue = GetColor(15, 60, 255);   // 青色

  /// <summary>
  /// ボタンの色
  /// </summary>
  const int kShapeColorWhiteBlue = GetColor(0, 130, 255);   // 青色

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// メニューテキスト
  /// </summary>
  const char* kMenuText = "Please Select Your Turn";

  /// <summary>
  /// ボタンテキスト
  /// </summary>
  const char* kFirstAttackButtonText = "First Attack";

  /// <summary>
  /// ボタンテキスト
  /// </summary>
  const char* kSecondAttackButtonText = "Second Attack";

  /// <summary>
  /// ボタンテキスト
  /// </summary>
  const char* kRandomButtonText = "Random";

  /// <summary>
  /// メニューテキスト 表示位置調整 垂直
  /// </summary>
  const int kMenuTextAdjustVirtical = 150;

  /// <summary>
  /// ボタンテキスト 表示位置調整 垂直
  /// </summary>
  const int kButtonTextAdjustVirtical = 15;

  /// <summary>
  /// 文字の色
  /// </summary>
  const int kTextForeColorWhite = GetColor(255, 255, 255);   // 白色

  /// <summary>
  /// 透過率 起動時調整値
  /// </summary>
  const int kAlphaAdjastStart = 30;

  /// <summary>
  /// 透過率 終了時調整値
  /// </summary>
  const int kAlphaAdjastFinish = 5;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// メニューの透過度
  /// </summary>
  const int kMenuAlpha = 150;

  /// <summary>
  /// ボタンの透過度
  /// </summary>
  const int kButtonAlpha = 225;

  /// <summary>
  /// テキストの透過度
  /// </summary>
  const int kTextAlpha = 255;

  /// <summary>
  /// 押されてない時のボタン位置調整値
  /// </summary>
  const int kInitialPosAdjust = 0;

  /// <summary>
  /// 押されている時のボタン位置調整値
  /// </summary>
  const int kPushPosAdjust = 3;

  /// <summary>
  /// ボタンの影 位置調整値
  /// </summary>
  const int kShadowPosAdjust = 6;

  /// <summary>
  /// 影の色
  /// </summary>
  const int kShadowColor = GetColor(0, 0, 0);   // 黒色

  /// <summary>
  /// メニューの影の透過度
  /// </summary>
  const int kMenuShadowAlpha = 100;

  /// <summary>
  /// 影の透過度
  /// </summary>
  const int kShadowAlpha = 150;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSize = 30;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 8;

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Meiryo UI";
}


/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
TurnSelectMenu::TurnSelectMenu()
  : Task(TaskId::kTurnSelectMenu)
  , exists_button_(GameInfo::TurnType::kNone)
  , x_pos_menu_left_(0)
  , y_pos_menu_left_(0)
  , x_pos_menu_right_(0)
  , y_pos_menu_right_(0)
  , x_pos_first_attack_button_left_(0)
  , y_pos_first_attack_button_left_(0)
  , x_pos_first_attack_button_right_(0)
  , y_pos_first_attack_button_right_(0)
  , x_pos_second_attack_button_left_(0)
  , y_pos_second_attack_button_left_(0)
  , x_pos_second_attack_button_right_(0)
  , y_pos_second_attack_button_right_(0)
  , x_pos_random_button_left_(0)
  , y_pos_random_button_left_(0)
  , x_pos_random_button_right_(0)
  , y_pos_random_button_right_(0)
  , x_pos_first_attack_shadow_left_(0)
  , y_pos_first_attack_shadow_left_(0)
  , x_pos_first_attack_shadow_right_(0)
  , y_pos_first_attack_shadow_right_(0)
  , x_pos_second_attack_shadow_left_(0)
  , y_pos_second_attack_shadow_left_(0)
  , x_pos_second_attack_shadow_right_(0)
  , y_pos_second_attack_shadow_right_(0)
  , x_pos_random_shadow_left_(0)
  , y_pos_random_shadow_left_(0)
  , x_pos_random_shadow_right_(0)
  , y_pos_random_shadow_right_(0)
  , x_pos_first_attack_button_adjust_(0)
  , y_pos_first_attack_button_adjust_(0)
  , x_pos_second_attack_button_adjust_(0)
  , y_pos_second_attack_button_adjust_(0)
  , x_pos_random_button_adjust_(0)
  , y_pos_random_button_adjust_(0)
  , x_pos_menu_text_(0)
  , y_pos_menu_text_(0)
  , x_pos_first_attack_button_text_(0)
  , y_pos_first_attack_button_text_(0)
  , x_pos_second_attack_button_text_(0)
  , y_pos_second_attack_button_text_(0)
  , x_pos_random_button_text_(0)
  , y_pos_random_button_text_(0)
  , menu_color_(kMenuColor)
  , first_attack_button_color_(0)
  , second_attack_button_color_(0)
  , random_button_color_(0)
  , alpha_text_(kTextAlpha)
  , alpha_menu_(kMenuAlpha)
  , alpha_button_(kButtonAlpha)
  , is_button_click_(false) {

  // コンソールに出力
  std::cout << "TurnSelectMenu コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
TurnSelectMenu::~TurnSelectMenu() {

  // コンソールに出力
  std::cout << "~TurnSelectMenu デストラクタ" << std::endl;
}

/// <summary>
/// 毎フレーム更新処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void TurnSelectMenu::Update(float process_time) {

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // 画面の中心座標を取得
  int x_pos_center = game_info->GetCenterPointX();
  int y_pos_center = game_info->GetCenterPointY();

  //-----------------------------------
  // メニューの描画準備
  //-----------------------------------
  x_pos_menu_left_ = x_pos_center - (kMenuWidth / kHalfValue);
  y_pos_menu_left_ = y_pos_center - (kMenuHeight / kHalfValue);
  x_pos_menu_right_ = x_pos_menu_left_ + kMenuWidth;
  y_pos_menu_right_ = y_pos_menu_left_ + kMenuHeight;

  //-----------------------------------
  // 先攻ボタンの描画準備
  //-----------------------------------
  x_pos_first_attack_button_left_ = x_pos_center - (kButtonUiWidth / kHalfValue) + x_pos_first_attack_button_adjust_;
  y_pos_first_attack_button_left_ = y_pos_center + kFirstAttackButtonUiAdjustVirtical - (kButtonUiHeight / kHalfValue) + y_pos_first_attack_button_adjust_;
  x_pos_first_attack_button_right_ = x_pos_first_attack_button_left_ + kButtonUiWidth + x_pos_first_attack_button_adjust_;
  y_pos_first_attack_button_right_ = y_pos_first_attack_button_left_ + kButtonUiHeight + y_pos_first_attack_button_adjust_;

  //-----------------------------------
  // 後攻ボタンの描画準備
  //-----------------------------------
  x_pos_second_attack_button_left_ = x_pos_center - (kButtonUiWidth / kHalfValue) + x_pos_second_attack_button_adjust_;
  y_pos_second_attack_button_left_ = y_pos_center + kSecondAttackButtonUiAdjustVirtical - (kButtonUiHeight / kHalfValue) + y_pos_second_attack_button_adjust_;
  x_pos_second_attack_button_right_ = x_pos_second_attack_button_left_ + kButtonUiWidth + x_pos_second_attack_button_adjust_;
  y_pos_second_attack_button_right_ = y_pos_second_attack_button_left_ + kButtonUiHeight + y_pos_second_attack_button_adjust_;

  //-----------------------------------
  // ランダムボタンの描画準備
  //-----------------------------------
  x_pos_random_button_left_ = x_pos_center - (kButtonUiWidth / kHalfValue) + x_pos_random_button_adjust_;
  y_pos_random_button_left_ = y_pos_center + kRandomButtonUiAdjustVirtical - (kButtonUiHeight / kHalfValue) + y_pos_random_button_adjust_;
  x_pos_random_button_right_ = x_pos_random_button_left_ + kButtonUiWidth + x_pos_random_button_adjust_;
  y_pos_random_button_right_ = y_pos_random_button_left_ + kButtonUiHeight + y_pos_random_button_adjust_;

  //-----------------------------------
  // 先攻ボタンの影の描画準備
  //-----------------------------------
  x_pos_first_attack_shadow_left_ = x_pos_first_attack_button_left_ + kShadowPosAdjust - x_pos_first_attack_button_adjust_;
  y_pos_first_attack_shadow_left_ = y_pos_first_attack_button_left_ + kShadowPosAdjust - y_pos_first_attack_button_adjust_;
  x_pos_first_attack_shadow_right_ = x_pos_first_attack_button_right_ + kShadowPosAdjust - x_pos_first_attack_button_adjust_;
  y_pos_first_attack_shadow_right_ = y_pos_first_attack_button_right_ + kShadowPosAdjust - y_pos_first_attack_button_adjust_;

  //-----------------------------------
  // 後攻ボタンの影の描画準備
  //-----------------------------------
  x_pos_second_attack_shadow_left_ = x_pos_second_attack_button_left_ + kShadowPosAdjust - x_pos_second_attack_button_adjust_;
  y_pos_second_attack_shadow_left_ = y_pos_second_attack_button_left_ + kShadowPosAdjust - y_pos_second_attack_button_adjust_;
  x_pos_second_attack_shadow_right_ = x_pos_second_attack_button_right_ + kShadowPosAdjust - x_pos_second_attack_button_adjust_;
  y_pos_second_attack_shadow_right_ = y_pos_second_attack_button_right_ + kShadowPosAdjust - y_pos_second_attack_button_adjust_;

  //-----------------------------------
  // ランダムボタンの影の描画準備
  //-----------------------------------
  x_pos_random_shadow_left_ = x_pos_random_button_left_ + kShadowPosAdjust - x_pos_random_button_adjust_;
  y_pos_random_shadow_left_ = y_pos_random_button_left_ + kShadowPosAdjust - y_pos_random_button_adjust_;
  x_pos_random_shadow_right_ = x_pos_random_button_right_ + kShadowPosAdjust - x_pos_random_button_adjust_;
  y_pos_random_shadow_right_ = y_pos_random_button_right_ + kShadowPosAdjust - y_pos_random_button_adjust_;

  // 文字列の描画処理
  SetFontSize(kFontSize);
  SetFontThickness(kFontWidth);
  ChangeFont(kFontName);
  ChangeFontType(DX_FONTTYPE_ANTIALIASING);
  //-----------------------------------
  // メニューテキストの描画準備
  //-----------------------------------
  int text_width = GetDrawStringWidth(kMenuText, static_cast<int>(strlen(kMenuText)));
  x_pos_menu_text_ = x_pos_menu_left_ + (kMenuWidth / kHalfValue) - (text_width / kHalfValue);
  y_pos_menu_text_ = y_pos_menu_left_ + (kMenuHeight / kHalfValue) - kMenuTextAdjustVirtical;


  //-----------------------------------
  // 先攻ボタンテキストの描画準備
  //-----------------------------------
  text_width = GetDrawStringWidth(kFirstAttackButtonText, static_cast<int>(strlen(kFirstAttackButtonText)));
  x_pos_first_attack_button_text_ = x_pos_first_attack_button_left_ + (kButtonUiWidth / kHalfValue) - (text_width / kHalfValue);
  y_pos_first_attack_button_text_ = y_pos_first_attack_button_left_ + (kButtonUiHeight / kHalfValue) - kButtonTextAdjustVirtical;

  //-----------------------------------
  // 後攻ボタンテキストの描画準備
  //-----------------------------------
  text_width = GetDrawStringWidth(kSecondAttackButtonText, static_cast<int>(strlen(kSecondAttackButtonText)));
  x_pos_second_attack_button_text_ = x_pos_second_attack_button_left_ + (kButtonUiWidth / kHalfValue) - (text_width / kHalfValue);
  y_pos_second_attack_button_text_ = y_pos_second_attack_button_left_ + (kButtonUiHeight / kHalfValue) - kButtonTextAdjustVirtical;

  //-----------------------------------
  // ランダムボタンテキストの描画準備
  //-----------------------------------
  text_width = GetDrawStringWidth(kRandomButtonText, static_cast<int>(strlen(kRandomButtonText)));
  x_pos_random_button_text_ = x_pos_random_button_left_ + (kButtonUiWidth / kHalfValue) - (text_width / kHalfValue);
  y_pos_random_button_text_ = y_pos_random_button_left_ + (kButtonUiHeight / kHalfValue) - kButtonTextAdjustVirtical;
}

/// <summary>
/// 毎フレーム描画処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TurnSelectMenu::Render() {

  SetFontSize(kFontSize);
  SetFontThickness(kFontWidth);
  ChangeFont(kFontName);
  ChangeFontType(DX_FONTTYPE_ANTIALIASING);

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, kShadowAlpha);

  //-----------------------------------
  // メニューの影
  //-----------------------------------
  DrawBox(x_pos_menu_left_ + kShadowPosAdjust, y_pos_menu_left_ + kShadowPosAdjust, x_pos_menu_right_ + kShadowPosAdjust, y_pos_menu_right_ + kShadowPosAdjust, kShadowColor, true);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_menu_);

  //-----------------------------------
  // メニュー
  //-----------------------------------
  DrawBox(x_pos_menu_left_, y_pos_menu_left_, x_pos_menu_right_, y_pos_menu_right_, menu_color_, true);
  
  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, kShadowAlpha);

  //-----------------------------------
  // ボタンの影
  //-----------------------------------
  DrawBox(x_pos_first_attack_shadow_left_, y_pos_first_attack_shadow_left_, x_pos_first_attack_shadow_right_, y_pos_first_attack_shadow_right_, kShadowColor, true);
  DrawBox(x_pos_second_attack_shadow_left_, y_pos_second_attack_shadow_left_, x_pos_second_attack_shadow_right_, y_pos_second_attack_shadow_right_, kShadowColor, true);
  DrawBox(x_pos_random_shadow_left_, y_pos_random_shadow_left_, x_pos_random_shadow_right_, y_pos_random_shadow_right_, kShadowColor, true);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_button_);

  //-----------------------------------
  // 先攻 / 後攻 / ランダムボタン
  //-----------------------------------
  DrawBox(x_pos_first_attack_button_left_, y_pos_first_attack_button_left_, x_pos_first_attack_button_right_, y_pos_first_attack_button_right_, first_attack_button_color_, true);
  DrawBox(x_pos_second_attack_button_left_, y_pos_second_attack_button_left_, x_pos_second_attack_button_right_, y_pos_second_attack_button_right_, second_attack_button_color_, true);
  DrawBox(x_pos_random_button_left_, y_pos_random_button_left_, x_pos_random_button_right_, y_pos_random_button_right_, random_button_color_, true);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_text_);

  //-----------------------------------
  // 各テキスト
  //-----------------------------------
  DrawString(x_pos_menu_text_, y_pos_menu_text_, kMenuText, kTextForeColorWhite);
  DrawString(x_pos_first_attack_button_text_, y_pos_first_attack_button_text_, kFirstAttackButtonText, kTextForeColorWhite);
  DrawString(x_pos_second_attack_button_text_, y_pos_second_attack_button_text_, kSecondAttackButtonText, kTextForeColorWhite);
  DrawString(x_pos_random_button_text_, y_pos_random_button_text_, kRandomButtonText, kTextForeColorWhite);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
}

/// <summary>
/// マウスカーソルがボタン上に存在するか確認
/// </summary>
/// <param name=""> X座標 </param>
/// <param name=""> Y座標 </param>
/// <param name=""> 押されているかどうか </param>
/// <returns> true:存在する, false:存在しない </returns>
bool TurnSelectMenu::CheckMouseCursorPosition(int x_pos, int y_pos, bool is_push) {

  first_attack_button_color_ = kShapeColorBlue;
  second_attack_button_color_ = kShapeColorBlue;
  random_button_color_ = kShapeColorBlue;
  x_pos_first_attack_button_adjust_ = kInitialPosAdjust;
  y_pos_first_attack_button_adjust_ = kInitialPosAdjust;
  x_pos_second_attack_button_adjust_ = kInitialPosAdjust;
  y_pos_second_attack_button_adjust_ = kInitialPosAdjust;
  x_pos_random_button_adjust_ = kInitialPosAdjust;
  y_pos_random_button_adjust_ = kInitialPosAdjust;
  exists_button_ = GameInfo::TurnType::kNone;

  // 先攻ボタン上に存在するかチェック
  if ((x_pos >= x_pos_first_attack_button_left_ && x_pos <= x_pos_first_attack_button_right_) &&
      (y_pos >= y_pos_first_attack_button_left_ && y_pos <= y_pos_first_attack_button_right_)) {

    if (!IsButtonClick()) {
      exists_button_ = GameInfo::TurnType::kFirstAttack;
    }
    // ボタンが押されている場合、ボタンの色を少し濃い灰色に設定
    // ボタンが押されていない場合、ボタンの色を白に近い灰色に設定
    if (is_push) {
      first_attack_button_color_ = kShapeColorMiddleBlue;
      x_pos_first_attack_button_adjust_ = kPushPosAdjust;
      y_pos_first_attack_button_adjust_ = kPushPosAdjust;
    }
    else {
      first_attack_button_color_ = kShapeColorWhiteBlue;
    }
    return true;
  }

  // 後攻ボタン上に存在するかチェック
  if ((x_pos >= x_pos_second_attack_button_left_ && x_pos <= x_pos_second_attack_button_right_) &&
      (y_pos >= y_pos_second_attack_button_left_ && y_pos <= y_pos_second_attack_button_right_)) {

    if (!IsButtonClick()) {
      exists_button_ = GameInfo::TurnType::kSecondAttack;
    }

    // ボタンが押されている場合、ボタンの色を少し濃い灰色に設定
    // ボタンが押されていない場合、ボタンの色を白に近い灰色に設定
    if (is_push) {
      second_attack_button_color_ = kShapeColorMiddleBlue;
      x_pos_second_attack_button_adjust_ = kPushPosAdjust;
      y_pos_second_attack_button_adjust_ = kPushPosAdjust;
    }
    else {
      second_attack_button_color_ = kShapeColorWhiteBlue;
    }
    return true;
  }

  // ランダムボタン上に存在するかチェック
  if ((x_pos >= x_pos_random_button_left_ && x_pos <= x_pos_random_button_right_) &&
      (y_pos >= y_pos_random_button_left_ && y_pos <= y_pos_random_button_right_)) {

    if (!IsButtonClick()) {
      exists_button_ = GameInfo::TurnType::kRandom;
    }

    // ボタンが押されている場合、ボタンの色を少し濃い灰色に設定
    // ボタンが押されていない場合、ボタンの色を白に近い灰色に設定
    if (is_push) {
      random_button_color_ = kShapeColorMiddleBlue;
      x_pos_random_button_adjust_ = kPushPosAdjust;
      y_pos_random_button_adjust_ = kPushPosAdjust;
    }
    else {
      random_button_color_ = kShapeColorWhiteBlue;
    }
    return true;
  }
  return false;
}