﻿#include "Game/GameInfo.h"


namespace {

  /// <summary>
  /// 解像度 横方向
  /// </summary>
  const int kResolutionHorizontalValue = 1100;

  /// <summary>
  /// 解像度 縦方向
  /// </summary>
  const int kResolutionVerticalValue = 825;

  /// <summary>
  /// 半分
  /// </summary>
  const int kValueToHalve = 2;

  /// <summary>
  /// 石の初期設定値
  /// </summary>
  const int kInitialSetStone = 32;
}

//-------------------------------
// データメンバの定義 
//-------------------------------
/// <summary>
/// ゲーム情報のインスタンス
/// </summary>
GameInfo* GameInfo::game_info_instance_ = nullptr;

/// <summary>
/// コンストラクタ
/// </summary>
/// <param></param>
/// <returns></returns>
GameInfo::GameInfo()
  : resolution_horizontal_(0)
  , resolution_vertical_(0)
  , center_point_x_(0)
  , center_point_y_(0)
  , map_width_(0)
  , map_height_(0)
  , map_position_x_(0)
  , map_position_y_(0)
  , operation_mode_(OperationMode::kManual)
  , game_data_()
  , battle_result_(BattleResult::kDraw)
  , stone_black_count_(kInitialSetStone)
  , stone_white_count_(kInitialSetStone)
  , is_debug_(false)
  , turn_type_(TurnType::kNone) {

}

/// <summary>
/// デストラクタ
/// </summary>
/// <param></param>
/// <returns></returns>
GameInfo::~GameInfo() {

}

/// <summary>
/// GameInfo インスタンスを作成する
/// </summary>
/// <param></param>
/// <returns></returns>
void GameInfo::CreateGameInfo() {

  if (game_info_instance_ == nullptr) {
    game_info_instance_ = new GameInfo;
  }
}

/// <summary>
/// GameInfo インスタンスを解放する
/// </summary>
/// <param></param>
/// <returns></returns>
void GameInfo::ReleaseGameInfo() {

  if (game_info_instance_ != nullptr) {
    delete game_info_instance_;
    game_info_instance_ = nullptr;
  }
}

/// <summary>
/// ゲーム情報を初期化する
/// </summary>
/// <param></param>
/// <returns></returns>
void GameInfo::InitializeGameInfo() {

  // 解像度の設定
  resolution_horizontal_ = kResolutionHorizontalValue;
  resolution_vertical_ = kResolutionVerticalValue;

  // 画面の中心を計算から求める
  center_point_x_ = resolution_horizontal_ / kValueToHalve;
  center_point_y_ = resolution_vertical_ / kValueToHalve;

  // デバッグで起動されているかどうか
#ifdef _DEBUG
  is_debug_ = true;
#else
  is_debug_ = false;
#endif

}

/// <summary>
/// マップのサイズをセットする
/// </summary>
/// <param> マップのXサイズ </param>
/// <param> マップのYサイズ </param>
/// <returns></returns>
void GameInfo::SetMapSize(int map_width, int map_height) {

  // マップサイズをセット
  map_width_ = map_width;
  map_height_ = map_height;

  // マップの左上の座標を取得
  map_position_x_ = center_point_x_ - (map_width_ / kValueToHalve);
  map_position_y_ = center_point_y_ - (map_height_ / kValueToHalve);
}

/// <summary>
/// 黒色の石の数をセット
/// </summary>
/// <param name=""> 黒色の石の数 </param>
/// <param name=""> 白色の石の数 </param>
/// <returns></returns>
void GameInfo::SetStoneCount(int stone_black_count, int stone_white_count) {

  stone_black_count_ = stone_black_count;
  stone_white_count_ = stone_white_count;
}