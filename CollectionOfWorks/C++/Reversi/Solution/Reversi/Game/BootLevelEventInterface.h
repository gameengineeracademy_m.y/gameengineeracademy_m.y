﻿#pragma once

#include "System/BackScreenEventInterface.h"
#include <iostream>

/// <summary>
/// ブートレベルイベントインターフェース
/// </summary>
class BootLevelEventInterface : public BackScreenEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  BootLevelEventInterface() {

    // コンソールに出力
    std::cout << "BootLevelEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~BootLevelEventInterface() {

    // コンソールに出力
    std::cout << "~BootLevelEventInterface デストラクタ" << std::endl;
  }


private:

};