﻿#if defined(_WIN64) || defined(_WIN32)
#include <windows.h>
#endif

#include "DxLib.h"
#include "System/LogConsole.h"
#include "System/TaskManager.h"
#include "System/LevelChanger.h"
#include "Game/GameInfo.h"

namespace {
  //====================================================
  //定数定義
  //====================================================
  /// <summary>
  /// カラービット数
  /// </summary>
  const int kColorBitNum = 32;

  /// <summary>
  /// 戻り値 正常終了
  /// </summary>
  const int kReturnSuccess = 0;

  /// <summary>
  /// 戻り値 異常終了
  /// </summary>
  const int kReturnError = -1;

  /// <summary>
  /// 1000倍を表す ※単位変換で使用
  /// </summary>
  const float kThousandTimes = 1000.0f;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSize = 30;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 8;

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Meiryo UI";
}

/// <summary>
/// プログラムは WinMain から始まります
/// </summary>
/// <param name="hInstance">アプリケーションの現在のインスタンスハンドル</param>
/// <param name="hPrevInstance">Win16 の産物（常にNULL）</param>
/// <param name="lpCmdLine">コマンドライン引数</param>
/// <param name="nCmdShow">ウィンドウの表示状態</param>
/// <returns>アプリケーションの終了コード</returns>
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {

#if defined(_WIN64) || defined(_WIN32)
  _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

  int start_time_milisec = 0;
  int finish_time_milisec = 0;
  int diff_time_milisec = 0;
  float diff_time_sec = 0.0f;
  LogConsole log_console;

  // ゲーム情報生成
  GameInfo::CreateGameInfo();
  // インスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    // ゲーム情報作成失敗のため終了
    DxLib_End();
    return kReturnError;
  }

  // 初期化する
  game_info->InitializeGameInfo();

  if (game_info->IsDebug()) {
    // ログコンソールを生成する
    log_console.CreateConsole();
  }

  // 解像度の幅を取得する
  int resolution_width = game_info->GetResolutionWidth();
  // 解像度の高さを取得する
  int resolution_height = game_info->GetResolutionHeight();

  //====================================================
  //DXライブラリの初期化前の設定
  //====================================================
  ChangeWindowMode(TRUE);           // ウィンドウモードに設定
  int result = SetGraphMode(resolution_width, resolution_height, kColorBitNum);

  //====================================================
  //DXライブラリの初期化処理
  //====================================================
  if (DxLib_Init() == kReturnError) {
      return kReturnError;          // エラーが起きたら直ちに終了
  }

  //====================================================
  //文字フォントの設定
  //====================================================
  SetFontSize(kFontSize);
  SetFontThickness(kFontWidth);
  ChangeFont(kFontName);
  ChangeFontType(DX_FONTTYPE_ANTIALIASING);

  // 描画先を裏画面に設定
  if (SetDrawScreen(DX_SCREEN_BACK) == -1) {
    return kReturnError;            // エラーが起きたら直ちに終了
  }

  // タスクマネージャの生成
  TaskManager* task_manager = new TaskManager();

  // タスクマネージャの生成に失敗した場合、処理終了
  if (task_manager == nullptr) {
    DxLib_End();
    return kReturnError;          // エラーが起きたら直ちに終了
  }

  // レベルチェンジャーの生成
  LevelChanger* level_changer = new LevelChanger(*task_manager);

  // レベルチェンジャーの生成に失敗した場合、処理終了
  if (level_changer == nullptr) {
    delete task_manager;
    task_manager = nullptr;
    DxLib_End();
    return kReturnError;          // エラーが起きたら直ちに終了
  }

  // タスクマネージャにレベルチェンジャーを積む
  bool check_result = task_manager->AddTask(level_changer);

  // レベルチェンジャーが正常に積めなかった場合は処理終了
  if (!check_result) {
    delete level_changer;
    level_changer = nullptr;
    delete task_manager;
    task_manager = nullptr;
    DxLib_End();
    return kReturnError;          // エラーが起きたら直ちに終了
  }

  //====================================================
  //メッセージループ
  //メッセージが終了でない間ループする
  //====================================================
  MSG msg = { 0 };

  while (true) {
    //====================================================
    //終了メッセージを受け取ったなら、ループを抜ける
    //====================================================
    if (msg.message == WM_QUIT) {
      // 画面終了する前にレベルチェンジャー内のタスクを終了フェーズにして終了させる
      level_changer->SetPhaseFinalize();
      msg = { 0 };
    }
    else if (level_changer->IsLevelChangerFinalized()) {
      // レベルチェンジャー内のタスクが終了処理済みフェーズの場合、ループを抜ける
      break;
    }

    //====================================================
    //メッセージが存在するなら
    //メッセージ処理を実行する
    //====================================================
    if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
      //====================================================
      //仮想キーメッセージを文字メッセージへ変換
      //====================================================
      TranslateMessage(&msg);

      //====================================================
      //ウィンドウプロシージャにメッセージを転送
      //====================================================
      DispatchMessage(&msg);
    }
    //====================================================
    //メッセージが来ていない時はメインループ処理
    //====================================================
    else {

      // 開始時間を取得
      start_time_milisec = GetNowCount();

      //--------------------------------------------
      //更新処理
      //--------------------------------------------
      // タスク実行処理 更新処理
      task_manager->UpdateTask(diff_time_sec);

      //--------------------------------------------
      //描画処理
      //--------------------------------------------
      // 画面のクリア
      ClearDrawScreen();

      // タスク実行処理 描画処理
      task_manager->RenderTask();

      // 画面に描画
      ScreenFlip();

      //--------------------------------------------
      //処理時間計算
      //--------------------------------------------
      // 処理時間を取得する
      finish_time_milisec = GetNowCount();
      diff_time_milisec = finish_time_milisec - start_time_milisec;
      diff_time_sec = diff_time_milisec / kThousandTimes;

      // 開始時間に終了時間を設定する
      start_time_milisec = finish_time_milisec;
    }
  }

  // レベルチェンジャーを降ろす
  task_manager->ReleaseTask(TaskId::kLevelChanger);

  // レベルチェンジャーを破棄
  delete level_changer;
  level_changer = nullptr;
  // タスクマネージャを破棄
  delete task_manager;
  task_manager = nullptr;

  if (game_info->IsDebug()) {
    // ログコンソールの破棄
    log_console.ReleaseConsole();
  }

  // ゲーム情報の解放
  game_info->ReleaseGameInfo();

  //====================================================
  //ＤＸライブラリ使用の終了処理
  //====================================================
  DxLib_End();

  //====================================================
  //ソフトの終了
  //====================================================
  return kReturnSuccess;
}