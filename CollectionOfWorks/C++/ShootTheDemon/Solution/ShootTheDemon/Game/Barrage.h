﻿#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif

#pragma once

#include "System/Character.h"
#include "Game/BarrageId.h"
#include "Game/BarrageEventInterface.h"
#include <iostream>
#include <math.h>

/// <summary>
/// バレッジ処理
/// </summary>
class Barrage {
public:

  /// <summary>
  /// 狙う目標の種類
  /// </summary>
  enum class AimType {
    kNone,
    kSpecified,
    kPlayer,
    kTypeMaxIndex
  };

  /// <summary>
  /// 弾幕の弾に関する座標
  /// </summary>
  struct Pos {
    float x;            // 表示座標(中心座標)
    float y;            // 表示座標(中心座標)
    float x_goal;       // 目標座標
    float y_goal;       // 目標座標
  };

  /// <summary>
  /// 弾幕の弾配置時の中心からの距離
  /// </summary>
  struct Distance {
    float x;            // X方向
    float y;            // Y方向
  };

  /// <summary>
  /// 弾幕の弾の数
  /// </summary>
  struct Count {
    int deploy;         // 配置数
    int generate;       // 1回当たりの生成数
    int execute;        // 生成回数
  };

  /// <summary>
  /// 弾幕の弾配置時の中心からの距離
  /// </summary>
  struct Angle {
    float value;        // 角度
    float adjust;       // 毎回の変化量
  };

  /// <summary>
  /// 弾幕の弾の詳細情報
  /// </summary>
  struct Detail {
    BarrageId barrage_id; // 弾幕の種類
    BulletId bullet_id;   // 弾の種類
    AimType aim_type;     // 目標の種類
  };

  /// <summary>
  /// 割合
  /// </summary>
  struct Rate {
    float move;           // 移動係数
    float acce_x;         // 加速度 X方向
    float acce_y;         // 加速度 Y方向
  };

  /// <summary>
  /// 拡大・縮小
  /// </summary>
  struct Expa {
    float value;          // 拡大率
    float adjust;         // 調整値
    float max;            // 最大値
    float min;            // 最小値
  };

  /// <summary>
  /// 透過率
  /// </summary>
  struct Alpha {
    int value;          // 拡大率
    int adjust;         // 調整値
    int max;            // 最大値
    int min;            // 最小値
  };

  /// <summary>
  /// 弾幕の実行間隔
  /// </summary>
  struct Interval {
    float exec;         // 弾幕生成の実行間隔
    float wait;         // 弾生成の実行間隔
  };

  /// <summary>
  /// 弾の移動量最大・最小(フレーム当たり)
  /// </summary>
  struct Speed {
    float x_max;    // X方向最大値
    float x_min;    // X方向最小値
    float y_max;    // Y方向最大値
    float y_min;    // Y方向最小値
  };

  /// <summary>
  /// 弾本体の待機時間
  /// </summary>
  struct Time {
    float wait_start;     // 動作待機時間
    float wait_accel;     // 加速待機時間
    float wait_expan;     // 拡大待機時間
  };

  /// <summary>
  /// 弾幕の情報
  /// </summary>
  struct BarrageData {
    Pos pos;
    Distance distance;
    Count count;
    Angle angle;
    Detail detail;
    Rate rate;
    Expa expa;
    Alpha alpha;
    Interval interval;
    Speed speed;
    Time time;
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> バレッジID </param>
  /// <returns></returns>
  Barrage(BarrageId);

  /// <summary>
  /// コピーコンストラクタ
  /// </summary>
  /// <param name=""> コピー元クラス </param>
  /// <returns></returns>
  Barrage(const Barrage&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~Barrage();

  /// <summary>
  /// バレッジクローン生成
  /// </summary>
  /// <param name=""></param>
  /// <returns> バレッジ </returns>
  Barrage* GenerateClone();

  /// <summary>
  /// バレッジイベントインターフェースをセット
  /// </summary>
  /// <param name="barrage_event_interface"> バレッジイベントインターフェース </param>
  /// <returns></returns>
  void SetEventInterface(BarrageEventInterface* barrage_event_interface) { barrage_event_interface_ = barrage_event_interface; }

  /// <summary>
  /// 目標の種類をセット
  /// </summary>
  /// <param name="move_x"> 目標の種類 </param>
  /// <returns></returns>
  void SetAimType(AimType aim_type) { barrage_data_.detail.aim_type = aim_type; }

  /// <summary>
  /// 目標の種類を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 目標の種類 </returns>
  AimType GetAimType() { return barrage_data_.detail.aim_type; }

  /// <summary>
  /// バレッジIDを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> バレッジID </returns>
  BarrageId GetBarrageId() { return barrage_id_; }

  /// <summary>
  /// キャラクターをセット
  /// </summary>
  /// <param name="character"> キャラクター </param>
  /// <returns></returns>
  void SetCharacter(Character* character) { character_ = character; }

  /// <summary>
  /// キャラクターを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> キャラクター </returns>
  Character* GetCharacter() { return character_; }

  /// <summary>
  /// バレッジIDをセット
  /// </summary>
  /// <param name="bullet_id"> バレッジID </param>
  /// <returns></returns>
  void SetDataBarrageId(BarrageId barrage_id) { barrage_data_.detail.barrage_id = barrage_id; }

  /// <summary>
  /// バレッジIDを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> バレッジID </returns>
  BarrageId GetDataBarrageId() { return barrage_data_.detail.barrage_id; }

  /// <summary>
  /// バレットIDをセット
  /// </summary>
  /// <param name="bullet_id"> バレットID </param>
  /// <returns></returns>
  void SetBulletId(BulletId bullet_id) { barrage_data_.detail.bullet_id = bullet_id; }

  /// <summary>
  /// バレットIDを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> バレットID </returns>
  BulletId GetBulletId() { return barrage_data_.detail.bullet_id; }

  /// <summary>
  /// 中心のX座標をセット
  /// </summary>
  /// <param name="x_pos"> X座標 </param>
  /// <returns></returns>
  void SetPositionX(float x_pos) { barrage_data_.pos.x = x_pos; }

  /// <summary>
  /// 中心のX座標を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> バレットID </returns>
  float GetPositionX() { return barrage_data_.pos.x; }

  /// <summary>
  /// 中心のY座標をセット
  /// </summary>
  /// <param name="y_pos"> Y座標 </param>
  /// <returns></returns>
  void SetPositionY(float y_pos) { barrage_data_.pos.y = y_pos; }

  /// <summary>
  /// 中心のY座標を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> Y座標 </returns>
  float GetPositionY() { return barrage_data_.pos.y; }

  /// <summary>
  /// 中心から離す量(X方向)をセット
  /// </summary>
  /// <param name="distance_x"> 中心から離す量 </param>
  /// <returns></returns>
  void SetDistanceX(float distance_x) { barrage_data_.distance.x = distance_x; }

  /// <summary>
  /// 中心から離す量(X方向)を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 中心から離す量 </returns>
  float GetDistanceX() { return barrage_data_.distance.x; }

  /// <summary>
  /// 中心から離す量(Y方向)をセット
  /// </summary>
  /// <param name="distance_y"> 中心から離す量 </param>
  /// <returns></returns>
  void SetDistanceY(float distance_y) { barrage_data_.distance.y = distance_y; }

  /// <summary>
  /// 中心から離す量(Y方向)を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 中心から離す量 </returns>
  float GetDistanceY() { return barrage_data_.distance.y; }

  /// <summary>
  /// 目標のX座標をセット
  /// </summary>
  /// <param name="x_pos"> X座標 </param>
  /// <returns></returns>
  void SetGoalPositionX(float x_pos) { barrage_data_.pos.x_goal = x_pos; }

  /// <summary>
  /// 目標のX座標を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> バレットID </returns>
  float GetGoalPositionX() { return barrage_data_.pos.x_goal; }

  /// <summary>
  /// 目標のY座標をセット
  /// </summary>
  /// <param name="x_pos"> X座標 </param>
  /// <returns></returns>
  void SetGoalPositionY(float y_pos) { barrage_data_.pos.y_goal = y_pos; }

  /// <summary>
  /// 目標のY座標を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> バレットID </returns>
  float GetGoalPosition() { return barrage_data_.pos.y_goal; }

  /// <summary>
  /// 弾の移動係数をセット
  /// </summary>
  /// <param name="move_x"> 弾の移動係数 </param>
  /// <returns></returns>
  void SetMoveRate(float move_rate) { barrage_data_.rate.move = move_rate; }

  /// <summary>
  /// 弾の移動係数を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 弾の移動係数 </returns>
  float GetMoveRate() { return barrage_data_.rate.move; }

  /// <summary>
  /// 弾の拡大率をセット
  /// </summary>
  /// <param name=""> 弾の拡大率 </param>
  /// <returns></returns>
  void SetExpaValue(float expa_value) { barrage_data_.expa.value = expa_value; }

  /// <summary>
  /// 弾の拡大率を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 弾の拡大率 </returns>
  float GetExpaValue() { return barrage_data_.expa.value; }

  /// <summary>
  /// 弾の拡大率調整値をセット
  /// </summary>
  /// <param name=""> 弾の拡大率調整値 </param>
  /// <returns></returns>
  void SetExpaAdjust(float expa_adjust) { barrage_data_.expa.adjust = expa_adjust; }

  /// <summary>
  /// 拡大率調整値を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 弾の拡大率調整値 </returns>
  float GetExpaAdjust() { return barrage_data_.expa.adjust; }

  /// <summary>
  /// 弾の拡大率最大値をセット
  /// </summary>
  /// <param name=""> 弾の拡大率最大値 </param>
  /// <returns></returns>
  void SetExpaMax(float expa_max) { barrage_data_.expa.max = expa_max; }

  /// <summary>
  /// 弾の拡大率最大値を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 弾の拡大率最小値 </returns>
  float GetExpaMax() { return barrage_data_.expa.max; }

  /// <summary>
  /// 弾の拡大率最小値をセット
  /// </summary>
  /// <param name=""> 弾の拡大率最小値 </param>
  /// <returns></returns>
  void SetExpaMin(float expa_min) { barrage_data_.expa.min = expa_min; }

  /// <summary>
  /// 弾の拡大率最小値を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 弾の拡大率最小値 </returns>
  float GetExpaMin() { return barrage_data_.expa.min; }

  /// <summary>
  /// 弾の透過率をセット
  /// </summary>
  /// <param name=""> 弾の透過率 </param>
  /// <returns></returns>
  void SetAlphaValue(int alpha_value) { barrage_data_.alpha.value = alpha_value; }

  /// <summary>
  /// 弾の透過率を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 弾の透過率 </returns>
  int GetAlphaValue() { return barrage_data_.alpha.value; }

  /// <summary>
  /// 弾の透過率調整値をセット
  /// </summary>
  /// <param name=""> 弾の透過率調整値 </param>
  /// <returns></returns>
  void SetAlphaAdjust(int alpha_adjust) { barrage_data_.alpha.adjust = alpha_adjust; }

  /// <summary>
  /// 透過率調整値を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 弾の透過率調整値 </returns>
  int GetAlphaAdjust() { return barrage_data_.alpha.adjust; }

  /// <summary>
  /// 弾の透過率最大値をセット
  /// </summary>
  /// <param name=""> 弾の透過率最大値 </param>
  /// <returns></returns>
  void SetAlphaMax(int alpha_max) { barrage_data_.alpha.max = alpha_max; }

  /// <summary>
  /// 弾の透過率最大値を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 弾の透過率最小値 </returns>
  int GetAlphaMax() { return barrage_data_.alpha.max; }

  /// <summary>
  /// 弾の透過率最小値をセット
  /// </summary>
  /// <param name=""> 弾の透過率最小値 </param>
  /// <returns></returns>
  void SetAlphaMin(int alpha_min) { barrage_data_.alpha.min = alpha_min; }

  /// <summary>
  /// 弾の透過率最小値を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 弾の透過率最小値 </returns>
  int GetAlphaMin() { return barrage_data_.alpha.min; }

  /// <summary>
  /// 弾の配置数をセット
  /// </summary>
  /// <param name="count"> 弾の配置数 </param>
  /// <returns></returns>
  void SetDeployCount(int count) { barrage_data_.count.deploy = count; }

  /// <summary>
  /// 弾の配置数を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 弾の配置数 </returns>
  int GetDeployCount() { return barrage_data_.count.deploy; }

  /// <summary>
  /// 1回で生成する弾の個数をセット
  /// </summary>
  /// <param name="count"> 生成する弾の個数 </param>
  /// <returns></returns>
  void SetGenerateCount(int count) { barrage_data_.count.generate = count; }

  /// <summary>
  /// 1回で生成する弾の個数を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 生成する弾の個数 </returns>
  int GetGenerateCount() { return barrage_data_.count.generate; }

  /// <summary>
  /// 角度をセット
  /// </summary>
  /// <param name="angle"> 角度 </param>
  /// <returns></returns>
  void SetAngle(float angle) { barrage_data_.angle.value = angle; }

  /// <summary>
  /// 角度を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 角度 </returns>
  float GetAngle() { return barrage_data_.angle.value; }

  /// <summary>
  /// 角度の調整量をセット
  /// </summary>
  /// <param name="angle"> 角度 </param>
  /// <returns></returns>
  void SetAngleAdjust(float adjust) { barrage_data_.angle.adjust = adjust; }

  /// <summary>
  /// 角度の調整量を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 角度 </returns>
  float GetAngleAdjust() { return barrage_data_.angle.adjust; }

  /// <summary>
  /// 実行回数をセット
  /// </summary>
  /// <param name="count"> 実行回数 </param>
  /// <returns></returns>
  void SetExecCount(int count) { barrage_data_.count.execute = count; }

  /// <summary>
  /// 実行回数を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 実行回数 </returns>
  int GetExecCount() { return barrage_data_.count.execute; }

  /// <summary>
  /// 実行間隔をセット
  /// </summary>
  /// <param name="exec_interval"> 実行間隔 </param>
  /// <returns></returns>
  void SetExecInterval(float exec_interval) { barrage_data_.interval.exec = exec_interval; }

  /// <summary>
  /// 実行間隔を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 実行間隔 </returns>
  float GetExecInterval() { return barrage_data_.interval.exec; }

  /// <summary>
  /// 待機間隔をセット
  /// </summary>
  /// <param name="exec_interval"> 待機間隔 </param>
  /// <returns></returns>
  void SetWaitInterval(float wait_interval) { barrage_data_.interval.wait = wait_interval; }

  /// <summary>
  /// 待機間隔を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 待機間隔 </returns>
  float GetWaitInterval() { return barrage_data_.interval.wait; }

  /// <summary>
  /// 加速度(X方向)をセット
  /// </summary>
  /// <param name="accele_x"> 加速度(X方向) </param>
  /// <returns></returns>
  void SetAcceleRateX(float accele_x) { barrage_data_.rate.acce_x = accele_x; }

  /// <summary>
  /// 加速度(Y方向)をセット
  /// </summary>
  /// <param name="accele_y"> 加速度(Y方向) </param>
  /// <returns></returns>
  void SetAcceleRateY(float accele_y) { barrage_data_.rate.acce_y = accele_y; }

  /// <summary>
  /// 速度(X方向)最大値をセット
  /// </summary>
  /// <param name="speed_x"> 速度 </param>
  /// <returns></returns>
  void SetSpeedXMax(float speed_x) { barrage_data_.speed.x_max = speed_x; }

  /// <summary>
  /// 加速度(Y方向)最大値をセット
  /// </summary>
  /// <param name="speed_y"> 速度 </param>
  /// <returns></returns>
  void SetSpeedYMax(float speed_y) { barrage_data_.speed.y_max = speed_y; }

  /// <summary>
  /// 速度(X方向)最小値をセット
  /// </summary>
  /// <param name="speed_x"> 速度 </param>
  /// <returns></returns>
  void SetSpeedXMin(float speed_x) { barrage_data_.speed.x_min = speed_x; }

  /// <summary>
  /// 加速度(Y方向)最小値をセット
  /// </summary>
  /// <param name="speed_y"> 速度 </param>
  /// <returns></returns>
  void SetSpeedYMin(float speed_y) { barrage_data_.speed.y_min = speed_y; }

  /// <summary>
  /// 弾の動作待機時間をセット
  /// </summary>
  /// <param name="wait_time"> 待機時間 </param>
  /// <returns></returns>
  void SetWaitTimeMoveStart(float wait_time) { barrage_data_.time.wait_start = wait_time; }

  /// <summary>
  /// 弾の加速待機時間をセット
  /// </summary>
  /// <param name="wait_time"> 待機時間 </param>
  /// <returns></returns>
  void SetWaitTimeAcceleration(float wait_time) { barrage_data_.time.wait_accel = wait_time; }

  /// <summary>
  /// 弾の拡大待機時間をセット
  /// </summary>
  /// <param name="wait_time"> 待機時間 </param>
  /// <returns></returns>
  void SetWaitTimeExpantion(float wait_time) { barrage_data_.time.wait_expan = wait_time; }

  /// <summary>
  /// 処理終了フラグをセット
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetFinish() { is_finish_ = true; }

  /// <summary>
  /// 処理終了するかどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了, false:継続 </returns>
  bool IsFinish() { return is_finish_; }

  /// <summary>
  /// 実行後終了フラグをセット
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetExecFinish() { is_exec_finish_ = true; }

  /// <summary>
  /// 実行後終了するかどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:実行後終了, false:継続処理 </returns>
  bool IsExecFinish() { return is_exec_finish_; }

  /// <summary>
  /// 弾幕 全方向
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AllDirection(float);

  /// <summary>
  /// 弾幕 回転
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Rotation(float);

  /// <summary>
  /// 弾幕 連続射撃
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool ContinuousShoot(float);

  /// <summary>
  /// 弾の生成と移動量の計算
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void CreateBullet();

  /// <summary>
  /// 弾幕 単発射撃
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool SingleShoot(float);

  /// <summary>
  /// 弾幕 2倍射撃
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool DoubleShoot(float);

  /// <summary>
  /// 弾幕 3倍射撃
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool TripleShoot(float);

  /// <summary>
  /// 弾幕 4倍射撃
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool QuardShoot(float);


private:

  /// <summary>
  /// バレッジID
  /// </summary>
  BarrageId barrage_id_;

  /// <summary>
  /// バレッジイベントインターフェース
  /// </summary>
  BarrageEventInterface* barrage_event_interface_;

  /// <summary>
  /// キャラクター
  /// </summary>
  Character* character_;

  /// <summary>
  /// 弾幕のデータ
  /// </summary>
  BarrageData barrage_data_;

  /// <summary>
  /// 実行した回数
  /// </summary>
  int executed_count_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// 処理終了フラグ
  /// </summary>
  bool is_finish_;

  /// <summary>
  /// 実行後終了するかどうか
  /// </summary>
  bool is_exec_finish_;
};