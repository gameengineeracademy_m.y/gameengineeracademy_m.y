﻿#include "Game/EnemyB.h"

namespace {

  /// <summary>
  /// 透過率 登場時調整値
  /// </summary>
  const int kAlphaAdjustStart = 5;

  /// <summary>
  /// 透過率 死亡時調整値
  /// </summary>
  const int kAlphaAdjustEnd = -30;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 拡大率
  /// </summary>
  const float kExtRate = 1.0f;

  /// <summary>
  /// リロード時間 敵キャラB
  /// </summary>
  const float kEnemyBReload = 0.5f;

  /// <summary>
  /// 累積時間のリセット値
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 移動待機時間 初期値
  /// </summary>
  const float kMoveTime = 4.0f;

  /// <summary>
  /// 移動待機時間
  /// </summary>
  const float kFourthPhaseMoveWaitTime = 8.0f;

  /// <summary>
  /// 第3フェーズ待機時間
  /// </summary>
  const float kThirdPhaseWaitTime = 0.8f;

  /// <summary>
  /// 第5フェーズ待機時間
  /// </summary>
  const float kFifthPhaseWaitTime = 1.10f;

  /// <summary>
  /// 第6フェーズ移動待機時間
  /// </summary>
  const float kSixthPhaseMoveWaitTime = 2.0f;

  /// <summary>
  /// 第6フェーズ待機時間
  /// </summary>
  const float kSixthPhaseWaitTime = 1.0f;

  /// <summary>
  /// 終了前待機時間
  /// </summary>
  const float kFinishWaitTime = 1.0f;

  /// <summary>
  /// エフェクト終了待機時間
  /// </summary>
  const float kEffectWaitTime = 1.0f;

  /// <summary>
  /// アイテム表示待機時間
  /// </summary>
  const float kDispItemWaitTime = 0.5f;

  /// <summary>
  /// カットイン待機時間
  /// </summary>
  const float kCutInWaitTime = 0.3f;

  /// <summary>
  /// HPバー表示までの待機時間
  /// </summary>
  const float kCreateHpBarWaitTime = 0.6f;

  /// <summary>
  /// HPバー非表示までの待機時間
  /// </summary>
  const float kDisposeHpBarWaitTime = 0.5f;

  /// <summary>
  /// 次のボスへの遷移待機時間
  /// </summary>
  const float kNextBossWaitTime = 2.0f;

  /// <summary>
  /// 爆破エフェクト待機時間
  /// </summary>
  const float kExplosionWaitTime = 0.08f;

  /// <summary>
  /// 始点 X座標
  /// </summary>
  const int kStartPositionX = 300;

  /// <summary>
  /// 終点 X座標
  /// </summary>
  const int kGoalPositionX = 725;

  /// <summary>
  /// 次のフェーズに遷移するHP
  /// </summary>
  const int kTransPhaseLimit = 0;

  /// <summary>
  /// 第１フェーズ
  /// </summary>
  const int kFirstPhase = 1;

  /// <summary>
  /// 第２フェーズ
  /// </summary>
  const int kSecondPhase = 2;

  /// <summary>
  /// 第３フェーズ
  /// </summary>
  const int kThirdPhase = 3;

  /// <summary>
  /// 第４フェーズ
  /// </summary>
  const int kFourthPhase = 4;

  /// <summary>
  /// 第５フェーズ
  /// </summary>
  const int kFifthPhase = 5;

  /// <summary>
  /// 第６フェーズ
  /// </summary>
  const int kSixthPhase = 6;

  /// <summary>
  /// 第７フェーズ
  /// </summary>
  const int kSeventhPhase = 7;

  /// <summary>
  /// 第８フェーズ
  /// </summary>
  const int kEighthPhase = 8;

  /// <summary>
  /// 回数リセット
  /// </summary>
  const int kResetCount = 0;

  /// <summary>
  /// エフェクト実行回数
  /// </summary>
  const int kExecCount = 5;

  /// <summary>
  /// 最終フェーズのエフェクト実行回数
  /// </summary>
  const int kFinalPhaseExecCount = 15;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> エネミーイベントインターフェース </param>
/// <param name=""> キャラクターコントローライベントインターフェース </param>
/// <returns></returns>
EnemyB::EnemyB(EnemyEventInterface& enemy_event_interface,
               CharacterControllerEventInterface& character_controller_event_interface,
               CharacterEventInterface& character_event_interface)
  : EnemyBase(TaskId::kEnemyB, Character::CharacterType::kEnemyB,
    enemy_event_interface, character_controller_event_interface,
    character_event_interface)
  , current_phase_(PhaseType::kInitialize)
  , status_phase_(StatusPhase::kAlive)
  , action_phase_(ActionPhase::kStart)
  , move_type_(MoveType::kStop)
  , specified_phase_(SpecifiedPhase::kBeforeMove)
  , barrage_phase_(BarragePhase::kNone)
  , disp_item_phase_(DispItemPhase::kCreate)
  , cut_in_phase_(CutInPhase::kCreate)
  , phase_data_()
  , direction_(DirectionType::kRight)
  , accumulate_time_(0.0f)
  , move_accum_time_(0.0f)
  , move_wait_time_(0.0f)
  , exec_effect_count_(0) {

  // コンソールに出力
  std::cout << "EnemyB コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
EnemyB::~EnemyB() {

  // コンソールに出力
  std::cout << "~EnemyB デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void EnemyB::Update(float process_time) {

  bool is_finish = false;

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //--------------------------------
    //初期化処理フェーズ
    //--------------------------------
    is_finish = Initialize();
    if (is_finish) {
      // 現在のフェーズを「起動」に変更
      ChangeCurrentPhase(PhaseType::kStartUp);
    }
    break;
  }
  case PhaseType::kStartUp: {
    //--------------------------------
    //起動フェーズ
    //--------------------------------
    // フェードイン処理完了後、現在のフェーズを「プレイ中」に変更
    is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 透過率調整値の符号を反転しておく
      ChangeAlphaAdjustSign();
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //--------------------------------
    //処理中フェーズ
    //--------------------------------
    // 発射判定処理
    ReleaseIsShoot(process_time);
    is_finish = Process(process_time);
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kBeforeFinalize);
    }
    break;
  }
  case PhaseType::kStop: {
    //--------------------------------
    //停止中フェーズ
    //--------------------------------
    break;
  }
  case PhaseType::kBeforeFinalize: {
    //--------------------------------
    //終了処理前フェーズ
    //--------------------------------
     // フェードアウト処理完了後、現在のフェーズを「終了処理」に変更
    is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 累積時間をリセットする
      accumulate_time_ = kResetTime;
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinalize);
      // サウンドBGMの停止
      enemy_event_interface_.OnFinishSoundBGM();
    }
    break;
  }
  case PhaseType::kFinalize: {
    //--------------------------------
    //終了処理フェーズ
    //--------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kNextBossWaitTime) {
      // バトルレベルへ敵キャラB終了を通知する
      enemy_event_interface_.OnFinish(this);
      // 現在のフェーズを「終了処理済み」に変更
      ChangeCurrentPhase(PhaseType::kFinalized);
    }
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void EnemyB::Render() {

  // 指定のフェーズ以外なら処理終了
  if (current_phase_ != PhaseType::kStartUp &&
      current_phase_ != PhaseType::kProcessWait &&
      current_phase_ != PhaseType::kProcess &&
      current_phase_ != PhaseType::kStop &&
      current_phase_ != PhaseType::kBeforeFinalize) {
    return;
  }

  EnemyBase::Render();
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool EnemyB::Initialize() {

  //------------------------------------
  //透過率設定
  //------------------------------------
  // 透過率 初期設定
  SetAlpha(kAlphaMin, kAlphaAdjustStart, kAlphaMax, kAlphaMin);

  //------------------------------------
  //キャラクター設定
  //------------------------------------
  int graphic_handle = GetGraphicHandle();
  // 画像のサイズをセットする
  SetImageSize(graphic_handle);
  // 画像の表示倍率をセットする
  SetImageDispRate(kExtRate);
  // 表示位置のセット
  SetPositionX(GetInitialPositionX());
  SetPositionY(GetInitialPositionY());
  // リロード時間をセット
  SetReloadTime(kEnemyBReload);
  // 弾を発射可能にする
  SetIsShoot(true);

  //------------------------------------
  //行動設定
  //------------------------------------
  SetStartPositionX(kStartPositionX);
  SetStartPositionY(GetPositionY());
  SetGoalPositionX(kGoalPositionX);
  SetGoalPositionY(GetPositionY());

  return true;
}

/// <summary>
/// 処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool EnemyB::Process(float process_time) {

  bool is_finish = false;

  switch (status_phase_) {
  case StatusPhase::kAlive: {
    //------------------------------------
    //生存フェーズ
    //------------------------------------
    is_finish = ActionEnemyB(process_time);
    if (is_finish) {
      // 状態フェーズを「敗北処理」に変更
      ChangeStatusPhase(StatusPhase::kDefeat);
    }
    break;
  }
  case StatusPhase::kDefeat: {
    //------------------------------------
    //敗北フェーズ
    //------------------------------------
    accumulate_time_ = kResetTime;
    // 状態フェーズを「終了処理」に変更
    ChangeStatusPhase(StatusPhase::kFinish);
    break;
  }
  case StatusPhase::kFinish: {
    //------------------------------------
    //終了処理フェーズ
    //------------------------------------
    // 透過率 初期設定
    SetAlpha(kAlphaMin, kAlphaAdjustEnd, kAlphaMax, kAlphaMin);
    // 現在のフェーズを「終了前処理」に変更
    ChangeCurrentPhase(PhaseType::kBeforeFinalize);
    break;
  }
  }

  return false;
}

/// <summary>
/// 行動処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool EnemyB::ActionEnemyB(float process_time) {

  // 当たり判定チェック
  enemy_event_interface_.OnCheckCollisionEnemy(this);

  // 現在のHPを取得する
  int hit_point = GetHitPoint();

  switch (action_phase_) {
  case ActionPhase::kStart: {
    //------------------------------------
    //開始フェーズ
    //------------------------------------
    // バトルレベルの敵フェーズを変更する
    enemy_event_interface_.OnChangeBattlePhase(this);
    // 行動フェーズを「第１フェーズカットイン」に変更
    ChangeActionPhase(ActionPhase::kFirstPhaseCutIn);
    ChangeCutInPhase(CutInPhase::kCreate);
    break;
  }
  case ActionPhase::kFirstPhaseCutIn: {
    //------------------------------------
    //第１フェーズ カットイン
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    bool is_finish = DisplayCutIn(kFirstPhase, CutInManager::ColorType::kGreen);
    if (is_finish) {
      // 行動フェーズを「第１フェーズ」に変更
      ChangeActionPhase(ActionPhase::kFirstPhase);
      // 生存状態にする
      SetAlive();
    }
    break;
  }
  case ActionPhase::kFirstPhase: {
    //------------------------------------
    //第１フェーズ
    //------------------------------------
    // 現在のHPが0以下なら次のフェーズへ変更
    if (hit_point <= kTransPhaseLimit) {
      // 弾幕表示フェーズを「次のフェーズへ」に変更
      ChangeBarragePhase(BarragePhase::kNextPhase);
    }

    // 累積時間を加算
    accumulate_time_ += process_time;
    // 第１フェーズの行動
    bool is_finish = ActionFirstPhase();
    if (is_finish) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 現在表示されている弾を消す
      character_event_interface_.OnFinishDisplayBullet(this);
      // ラップタイムをセット
      enemy_event_interface_.OnSetRapTime(this, kFirstPhase);
      // 行動フェーズを「第１フェーズ終了エフェクト」に変更
      ChangeActionPhase(ActionPhase::kFirstPhaseEffect);
      // 弾幕表示フェーズを「何もしない」に変更
      ChangeBarragePhase(BarragePhase::kNone);
    }
    break;
  }
  case ActionPhase::kFirstPhaseEffect: {
    //------------------------------------
    //第１フェーズ終了エフェクト
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kExplosionWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 爆破エフェクトを実行
      enemy_event_interface_.OnExecuteEnemyEffect(this);
      // 実行回数をインクリメント
      ++exec_effect_count_;
    }

    if (exec_effect_count_ >= kExecCount) {
      // 実行回数をリセットする
      exec_effect_count_ = kResetCount;
      // 行動フェーズを「第１フェーズ終了」に変更
      ChangeActionPhase(ActionPhase::kFirstPhaseFinish);
    }

    break;
  }
  case ActionPhase::kFirstPhaseFinish: {
    //------------------------------------
    //第１フェーズ終了
    //------------------------------------
    // HPバーを消す
    enemy_event_interface_.OnDisposeHpBar(this);
    // 行動フェーズを「第２フェーズ開始待機」に変更
    ChangeActionPhase(ActionPhase::kSecondStartWait);
    break;
  }
  case ActionPhase::kSecondStartWait: {
    //------------------------------------
    //第２フェーズ開始待機
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kEffectWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 行動フェーズを「第２フェーズカットイン」に変更
      ChangeActionPhase(ActionPhase::kSecondPhaseCutIn);
      ChangeCutInPhase(CutInPhase::kCreate);
    }
    break;
  }
  case ActionPhase::kSecondPhaseCutIn: {
    //------------------------------------
    //第２フェーズ カットイン
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    bool is_finish = DisplayCutIn(kSecondPhase, CutInManager::ColorType::kRed);
    if (is_finish) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 行動フェーズを「第２フェーズ」に変更
      ChangeActionPhase(ActionPhase::kSecondPhase);
      // 生存状態にする
      SetAlive();
    }
    break;
  }
  case ActionPhase::kSecondPhase: {
    //------------------------------------
    //第２フェーズ
    //------------------------------------
    // 現在のHPが0以下なら次のフェーズへ変更
    if (hit_point <= kTransPhaseLimit) {
      // 弾幕表示フェーズを「次のフェーズへ」に変更
      ChangeBarragePhase(BarragePhase::kNextPhase);
    }

    // 累積時間を加算
    accumulate_time_ += process_time;
    // 第２フェーズの行動
    bool is_finish = ActionSecondPhase();
    if (is_finish) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 現在表示されている弾を消す
      character_event_interface_.OnFinishDisplayBullet(this);
      // ラップタイムをセット
      enemy_event_interface_.OnSetRapTime(this, kSecondPhase);
      // 行動フェーズを「第２フェーズ終了エフェクト」に変更
      ChangeActionPhase(ActionPhase::kSecondPhaseEffect);
      // 弾幕表示フェーズを「何もしない」に変更
      ChangeBarragePhase(BarragePhase::kNone);
    }
    break;
  }
  case ActionPhase::kSecondPhaseEffect: {
    //------------------------------------
    //第２フェーズ終了エフェクト
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kExplosionWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 爆破エフェクトを実行
      enemy_event_interface_.OnExecuteEnemyEffect(this);
      // 実行回数をインクリメント
      ++exec_effect_count_;
    }

    if (exec_effect_count_ >= kExecCount) {
      // 実行回数をリセットする
      exec_effect_count_ = kResetCount;
      // 行動フェーズを「第２フェーズ終了」に変更
      ChangeActionPhase(ActionPhase::kSecondPhaseFinish);
    }

    break;
  }
  case ActionPhase::kSecondPhaseFinish: {
    //------------------------------------
    //第２フェーズ終了
    //------------------------------------
    // HPバーを消す
    enemy_event_interface_.OnDisposeHpBar(this);
    // 行動フェーズを「第３フェーズ開始待機」に変更
    ChangeActionPhase(ActionPhase::kThirdStartWait);
    break;
  }
  case ActionPhase::kThirdStartWait: {
    //------------------------------------
    //第３フェーズ開始待機
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kEffectWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 行動フェーズを「第３フェーズカットイン」に変更
      ChangeActionPhase(ActionPhase::kThirdPhaseCutIn);
      ChangeCutInPhase(CutInPhase::kCreate);
    }
    break;
  }
  case ActionPhase::kThirdPhaseCutIn: {
    //------------------------------------
    //第３フェーズ カットイン
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    bool is_finish = DisplayCutIn(kThirdPhase, CutInManager::ColorType::kYellow);
    if (is_finish) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 行動フェーズを「第３フェーズ」に変更
      ChangeActionPhase(ActionPhase::kThirdPhase);
      // 生存状態にする
      SetAlive();
    }
    break;
  }
  case ActionPhase::kThirdPhase: {
    //------------------------------------
    //第３フェーズ
    //------------------------------------
    // 現在のHPが0以下なら次のフェーズへ変更
    if (hit_point <= kTransPhaseLimit) {
      // 弾幕表示フェーズを「次のフェーズへ」に変更
      ChangeBarragePhase(BarragePhase::kNextPhase);
    }

    // 累積時間を加算
    accumulate_time_ += process_time;
    // 第３フェーズの行動
    bool is_finish = ActionThirdPhase();
    if (is_finish) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 現在表示されている弾を消す
      character_event_interface_.OnFinishDisplayBullet(this);
      // ラップタイムをセット
      enemy_event_interface_.OnSetRapTime(this, kThirdPhase);
      // 行動フェーズを「第３フェーズ終了エフェクト」に変更
      ChangeActionPhase(ActionPhase::kThirdPhaseEffect);
      // 弾幕表示フェーズを「何もしない」に変更
      ChangeBarragePhase(BarragePhase::kNone);
    }
    break;
  }
  case ActionPhase::kThirdPhaseEffect: {
    //------------------------------------
    //第３フェーズ終了エフェクト
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kExplosionWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 爆破エフェクトを実行
      enemy_event_interface_.OnExecuteEnemyEffect(this);
      // 実行回数をインクリメント
      ++exec_effect_count_;
    }

    if (exec_effect_count_ >= kExecCount) {
      // 実行回数をリセットする
      exec_effect_count_ = kResetCount;
      // 行動フェーズを「第３フェーズ終了」に変更
      ChangeActionPhase(ActionPhase::kThirdPhaseFinish);
    }
    break;
  }
  case ActionPhase::kThirdPhaseFinish: {
    //------------------------------------
    //第３フェーズ終了
    //------------------------------------
    // HPバーを消す
    enemy_event_interface_.OnDisposeHpBar(this);
    // 行動フェーズを「第４フェーズ開始待機」に変更
    ChangeActionPhase(ActionPhase::kFourthStartWait);
    break;
  }
  case ActionPhase::kFourthStartWait: {
    //------------------------------------
    //第４フェーズ開始待機
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kEffectWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 行動フェーズを「第４フェーズ切り替え準備」に変更
      ChangeActionPhase(ActionPhase::kFourthPhasePrepare);
      // アイテム生成フェーズを「生成処理」に変更
      ChangeDispItemPhase(DispItemPhase::kCreate);
    }
    break;
  }
  case ActionPhase::kFourthPhasePrepare: {
    //------------------------------------
    //第４フェーズ切り替え準備
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    // アイテム生成処理
    bool is_finish = DisplayItem(CharacterType::kItemB);
    if (is_finish) {
      // 行動フェーズを「第４フェーズカットイン」に変更
      ChangeActionPhase(ActionPhase::kFourthPhaseCutIn);
      ChangeCutInPhase(CutInPhase::kCreate);
    }
    break;
  }
  case ActionPhase::kFourthPhaseCutIn: {
    //------------------------------------
    //第４フェーズ カットイン
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    bool is_finish = DisplayCutIn(kFourthPhase, CutInManager::ColorType::kBlue);
    if (is_finish) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 行動フェーズを「第４フェーズ」に変更
      ChangeActionPhase(ActionPhase::kFourthPhase);
      // 生存状態にする
      SetAlive();
    }
    break;
  }
  case ActionPhase::kFourthPhase: {
    //------------------------------------
    //第４フェーズ
    //------------------------------------
    // 現在のHPが0以下なら次のフェーズへ変更
    if (hit_point <= kTransPhaseLimit) {
      // 弾幕表示フェーズを「終了」に変更
      ChangeBarragePhase(BarragePhase::kNextPhase);
    }

    // 累積時間を加算
    accumulate_time_ += process_time;
    // 第４フェーズの行動
    bool is_finish = ActionFourthPhase(process_time);
    if (is_finish) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 現在表示されている弾を消す
      character_event_interface_.OnFinishDisplayBullet(this);
      // ラップタイムをセット
      enemy_event_interface_.OnSetRapTime(this, kFourthPhase);
      // 行動フェーズを「第４フェーズ終了エフェクト」に変更
      ChangeActionPhase(ActionPhase::kFourthPhaseEffect);
      // 弾幕表示フェーズを「何もしない」に変更
      ChangeBarragePhase(BarragePhase::kNone);
    }
    break;
  }
  case ActionPhase::kFourthPhaseEffect: {
    //------------------------------------
    //第４フェーズ終了エフェクト
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kExplosionWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 爆破エフェクトを実行
      enemy_event_interface_.OnExecuteEnemyEffect(this);
      // 実行回数をインクリメント
      ++exec_effect_count_;
    }

    if (exec_effect_count_ >= kExecCount) {
      // 実行回数をリセットする
      exec_effect_count_ = kResetCount;
      // 行動フェーズを「第４フェーズ終了」に変更
      ChangeActionPhase(ActionPhase::kFourthPhaseFinish);
    }
    break;
  }
  case ActionPhase::kFourthPhaseFinish: {
    //------------------------------------
    //第４フェーズ終了
    //------------------------------------
    // HPバーを消す
    enemy_event_interface_.OnDisposeHpBar(this);
    // 行動フェーズを「第５フェーズ開始待機」に変更
    ChangeActionPhase(ActionPhase::kFifthStartWait);
    break;
  }
  case ActionPhase::kFifthStartWait: {
    //------------------------------------
    //第５フェーズ開始待機
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kEffectWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 現在の行動フェーズを第５フェーズに変更
      ChangeActionPhase(ActionPhase::kFifthPhaseCutIn);
      ChangeCutInPhase(CutInPhase::kCreate);
    }
    break;
  }
  case ActionPhase::kFifthPhaseCutIn: {
    //------------------------------------
    //第５フェーズ カットイン
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    bool is_finish = DisplayCutIn(kFifthPhase, CutInManager::ColorType::kPurple);
    if (is_finish) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 行動フェーズを「第５フェーズ」に変更
      ChangeActionPhase(ActionPhase::kFifthPhase);
      // 生存状態にする
      SetAlive();
    }
    break;
  }
  case ActionPhase::kFifthPhase: {
    //------------------------------------
    //第５フェーズ
    //------------------------------------
    // 現在のHPが0以下なら次のフェーズへ変更
    if (hit_point <= kTransPhaseLimit) {
      // 弾幕表示フェーズを「次のフェーズへ」に変更
      ChangeBarragePhase(BarragePhase::kNextPhase);
    }

    // 累積時間を加算
    accumulate_time_ += process_time;
    // 第５フェーズの行動
    bool is_finish = ActionFifthPhase();
    if (is_finish) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 現在表示されている弾を消す
      character_event_interface_.OnFinishDisplayBullet(this);
      // ラップタイムをセット
      enemy_event_interface_.OnSetRapTime(this, kFifthPhase);
      // 行動フェーズを「第５フェーズ終了エフェクト」に変更
      ChangeActionPhase(ActionPhase::kFifthPhaseEffect);
      // 弾幕表示フェーズを「何もしない」に変更
      ChangeBarragePhase(BarragePhase::kNone);
    }
    break;
  }
  case ActionPhase::kFifthPhaseEffect: {
    //------------------------------------
    //第５フェーズ終了エフェクト
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kExplosionWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 爆破エフェクトを実行
      enemy_event_interface_.OnExecuteEnemyEffect(this);
      // 実行回数をインクリメント
      ++exec_effect_count_;
    }

    if (exec_effect_count_ >= kExecCount) {
      // 実行回数をリセットする
      exec_effect_count_ = kResetCount;
      // 行動フェーズを「第５フェーズ終了」に変更
      ChangeActionPhase(ActionPhase::kFifthPhaseFinish);
    }
    break;
  }
  case ActionPhase::kFifthPhaseFinish: {
    //------------------------------------
    //第５フェーズ終了
    //------------------------------------
    // HPバーを消す
    enemy_event_interface_.OnDisposeHpBar(this);
    // 行動フェーズを「第６フェーズ開始待機」に変更
    ChangeActionPhase(ActionPhase::kSixthStartWait);
    break;
  }
  case ActionPhase::kSixthStartWait: {
    //------------------------------------
    //第６フェーズ開始待機
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kEffectWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 行動フェーズを「第６フェーズカットイン」に変更
      ChangeActionPhase(ActionPhase::kSixthPhasePrepare);
      // アイテム生成フェーズを「生成処理」に変更
      ChangeDispItemPhase(DispItemPhase::kCreate);
    }
    break;
  }
  case ActionPhase::kSixthPhasePrepare: {
    //------------------------------------
    //第６フェーズ切り替え準備
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    // アイテム生成処理
    bool is_finish = DisplayItem(CharacterType::kItemA);
    if (is_finish) {
      // 行動フェーズを「第６フェーズカットイン」に変更
      ChangeActionPhase(ActionPhase::kSixthPhaseCutIn);
      ChangeCutInPhase(CutInPhase::kCreate);
    }
    break;
  }
  case ActionPhase::kSixthPhaseCutIn: {
    //------------------------------------
    //第６フェーズ カットイン
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    bool is_finish = DisplayCutIn(kSixthPhase, CutInManager::ColorType::kWhite);
    if (is_finish) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 行動フェーズを「第６フェーズ」に変更
      ChangeActionPhase(ActionPhase::kSixthPhase);
      // 生存状態にする
      SetAlive();
    }
    break;
  }
  case ActionPhase::kSixthPhase: {
    //------------------------------------
    //第６フェーズ
    //------------------------------------
    // 現在のHPが0以下なら次のフェーズへ変更
    if (hit_point <= kTransPhaseLimit) {
      // 弾幕表示フェーズを「次のフェーズへ」に変更
      ChangeBarragePhase(BarragePhase::kNextPhase);
    }

    // 累積時間を加算
    accumulate_time_ += process_time;
    // 第６フェーズの行動
    bool is_finish = ActionSixthPhase(process_time);
    if (is_finish) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 現在表示されている弾を消す
      character_event_interface_.OnFinishDisplayBullet(this);
      // ラップタイムをセット
      enemy_event_interface_.OnSetRapTime(this, kSixthPhase);
      // 行動フェーズを「第６フェーズ終了エフェクト」に変更
      ChangeActionPhase(ActionPhase::kSixthPhaseEffect);
      // 弾幕表示フェーズを「何もしない」に変更
      ChangeBarragePhase(BarragePhase::kNone);
    }
    break;
  }
  case ActionPhase::kSixthPhaseEffect: {
    //------------------------------------
    //第６フェーズ終了エフェクト
    //------------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kExplosionWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 爆破エフェクトを実行
      enemy_event_interface_.OnExecuteEnemyEffect(this);
      // 実行回数をインクリメント
      ++exec_effect_count_;
    }

    if (exec_effect_count_ >= kFinalPhaseExecCount) {
      // 実行回数をリセットする
      exec_effect_count_ = kResetCount;
      // 行動フェーズを「第６フェーズ終了」に変更
      ChangeActionPhase(ActionPhase::kSixthPhaseFinish);
    }
    break;
  }
  case ActionPhase::kSixthPhaseFinish: {
    //------------------------------------
    //第６フェーズ終了
    //------------------------------------
    // HPバーを消す
    enemy_event_interface_.OnDisposeHpBar(this);
    // 敵を破裂させる
    character_event_interface_.OnExecuteEffect(EffectId::kEnemyRupture, this);
    // 行動フェーズを「ゲーム終了」に変更
    ChangeActionPhase(ActionPhase::kGameFinish);
    break;
  }
  case ActionPhase::kGameFinish: {
    //------------------------------------
    //ゲーム終了
    //------------------------------------
    return true;
  }
  }

  return false;
}

/// <summary>
/// 第１フェーズの行動
/// </summary>
/// <param name=""></param>
/// <returns> true:終了, false:処理中 </returns>
bool EnemyB::ActionFirstPhase() {

  switch (barrage_phase_) {
  case BarragePhase::kNone: {
    //------------------------------------
    //何もしないフェーズ
    //------------------------------------
    // 弾幕表示フェーズを「開始」に変更
    ChangeBarragePhase(BarragePhase::kStart);
    break;
  }
  case BarragePhase::kStart: {
    //-----------------------------
    //開始フェーズ
    //-----------------------------
    // データを取得
    Barrage::BarrageData barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kFirstPhase_01];
    // 弾のセット
    SetBulletId(barrage_data.detail.bullet_id);
    // 弾幕処理開始
    Barrage* barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);
    barrage->SetBulletId(GetBulletId());                     // バレットID
    barrage->SetPositionX(GetFloatPositionX());              // 中心座標 X座標
    barrage->SetPositionY(GetFloatPositionY());              // 中心座標 Y座標
    barrage->SetGoalPositionX(GetFloatPositionX());          // 目標座標 X座標

    // データを取得
    barrage = nullptr;
    barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kFirstPhase_02];
    // 弾のセット
    SetBulletId(barrage_data.detail.bullet_id);
    // 弾幕処理開始
    barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);
    barrage->SetBulletId(GetBulletId());                     // バレットID
    barrage->SetPositionX(GetFloatPositionX());              // 中心座標 X座標
    barrage->SetPositionY(GetFloatPositionY());              // 中心座標 Y座標
    barrage->SetGoalPositionX(GetFloatPositionX());          // 目標座標 X座標

    // 累積時間をリセット
    accumulate_time_ = kResetTime;
    // 弾幕表示フェーズを「処理中」に変更
    ChangeBarragePhase(BarragePhase::kProcess);
    break;
  }
  case BarragePhase::kProcess: {
    //-----------------------------
    //処理中フェーズ
    //-----------------------------
    break;
  }
  case BarragePhase::kFinish: {
    //-----------------------------
    //終了フェーズ
    //-----------------------------
    break;
  }
  case BarragePhase::kWait: {
    //-----------------------------
    //待機フェーズ
    //-----------------------------
    break;
  }
  case BarragePhase::kNextPhase: {
    //-----------------------------
    //次のフェーズへ
    //-----------------------------
        // 弾幕処理終了
    enemy_event_interface_.OnFinishBarrage();
    return true;
  }
  }

  return false;
}

/// <summary>
/// 第２フェーズの行動
/// </summary>
/// <param name=""></param>
/// <returns> true:終了, false:処理中 </returns>
bool EnemyB::ActionSecondPhase() {

  switch (barrage_phase_) {
  case BarragePhase::kNone: {
    //------------------------------------
    //何もしないフェーズ
    //------------------------------------
    // 弾幕表示フェーズを「開始」に変更
    ChangeBarragePhase(BarragePhase::kStart);
    break;
  }
  case BarragePhase::kStart: {
    //-----------------------------
    //開始フェーズ
    //-----------------------------
    // データを取得
    Barrage::BarrageData barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kSecondPhase_01];
    // 弾のセット
    SetBulletId(barrage_data.detail.bullet_id);
    // 弾幕処理開始
    Barrage* barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);
    barrage->SetPositionX(GetFloatPositionX());              // 中心座標 X座標
    barrage->SetPositionY(GetFloatPositionY());              // 中心座標 Y座標

    // 累積時間をリセット
    accumulate_time_ = kResetTime;
    // 弾幕表示フェーズを「処理中」に変更
    ChangeBarragePhase(BarragePhase::kProcess);
    break;
  }
  case BarragePhase::kProcess: {
    //-----------------------------
    //処理中フェーズ
    //-----------------------------
    if (accumulate_time_ >= kFifthPhaseWaitTime) {
      // 弾幕処理終了
      enemy_event_interface_.OnFinishBarrage();
      // 弾幕表示フェーズを「終了」に変更
      ChangeBarragePhase(BarragePhase::kFinish);
    }
    break;
  }
  case BarragePhase::kFinish: {
    //-----------------------------
    //終了フェーズ
    //-----------------------------
    // データを取得
    Barrage::BarrageData barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kSecondPhase_02];
    // 弾のセット
    SetBulletId(barrage_data.detail.bullet_id);
    // 弾幕処理開始
    Barrage* barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);
    barrage->SetPositionX(GetFloatPositionX());              // 中心座標 X座標
    barrage->SetPositionY(GetFloatPositionY());              // 中心座標 Y座標

    // 累積時間をリセット
    accumulate_time_ = kResetTime;
    // 弾幕表示フェーズを「待機」に変更
    ChangeBarragePhase(BarragePhase::kWait);
    break;
  }
  case BarragePhase::kWait: {
    //-----------------------------
    //待機フェーズ
    //-----------------------------
    if (accumulate_time_ >= kFifthPhaseWaitTime) {
      // 弾幕処理終了
      enemy_event_interface_.OnFinishBarrage();
      // 弾幕表示フェーズを「開始」に変更
      ChangeBarragePhase(BarragePhase::kStart);
    }
    break;
  }
  case BarragePhase::kNextPhase: {
    //-----------------------------
    //次のフェーズへ
    //-----------------------------
        // 弾幕処理終了
    enemy_event_interface_.OnFinishBarrage();
    return true;
  }
  }

  return false;

}

/// <summary>
/// 第３フェーズの行動
/// </summary>
/// <param name=""></param>
/// <returns> true:終了, false:処理中 </returns>
bool EnemyB::ActionThirdPhase() {

  switch (barrage_phase_) {
  case BarragePhase::kNone: {
    //------------------------------------
    //何もしないフェーズ
    //------------------------------------
    // 弾幕表示フェーズを「開始」に変更
    ChangeBarragePhase(BarragePhase::kStart);
    break;
  }
  case BarragePhase::kStart: {
    //-----------------------------
    //開始フェーズ
    //-----------------------------
    bool is_dead = enemy_event_interface_.OnCheckPlayerDead();
    if (is_dead) {
      break;
    }

    // データを取得
    Barrage::BarrageData barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kThirdPhase_01];
    // 弾のセット
    SetBulletId(barrage_data.detail.bullet_id);
    // 弾幕処理開始
    Barrage* barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);
    barrage->SetPositionX(GetFloatPositionX());              // 中心座標 X座標
    barrage->SetPositionY(GetFloatPositionY());              // 中心座標 Y座標

    // データを取得
    barrage = nullptr;
    barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kThirdPhase_02];
    // 弾のセット
    SetBulletId(barrage_data.detail.bullet_id);
    // 弾幕処理開始
    barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);
    barrage->SetPositionX(GetFloatPositionX());              // 中心座標 X座標
    barrage->SetPositionY(GetFloatPositionY());              // 中心座標 Y座標

    // 累積時間をリセット
    accumulate_time_ = kResetTime;
    // 弾幕表示フェーズを「処理中」に変更
    ChangeBarragePhase(BarragePhase::kProcess);
    break;
  }
  case BarragePhase::kProcess: {
    //-----------------------------
    //処理中フェーズ
    //-----------------------------
    // プレイヤーが死亡しているか確認
    bool is_dead = enemy_event_interface_.OnCheckPlayerDead();
    if (is_dead) {
      // 弾幕表示フェーズを「終了処理」に変更
      ChangeBarragePhase(BarragePhase::kFinish);
    }
    break;
  }
  case BarragePhase::kFinish: {
    //-----------------------------
    //終了フェーズ
    //-----------------------------
    if (accumulate_time_ >= kThirdPhaseWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 弾幕処理終了
      enemy_event_interface_.OnFinishBarrage();
      // 現在表示されている弾を消す
      character_event_interface_.OnFinishDisplayBullet(this);
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 弾幕表示フェーズを「待機」に変更
      ChangeBarragePhase(BarragePhase::kWait);
    }
    break;
  }
  case BarragePhase::kWait: {
    //-----------------------------
    //待機フェーズ
    //-----------------------------
    // 弾幕表示フェーズを「開始」に変更
    ChangeBarragePhase(BarragePhase::kStart);
    break;
  }
  case BarragePhase::kNextPhase: {
    //-----------------------------
    //次のフェーズへ
    //-----------------------------
    // 弾幕処理終了
    enemy_event_interface_.OnFinishBarrage();
    return true;
  }
  }

  return false;
}

/// <summary>
/// 第４フェーズの行動
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true:終了, false:処理中 </returns>
bool EnemyB::ActionFourthPhase(float process_time) {

  switch (barrage_phase_) {
  case BarragePhase::kNone: {
    //------------------------------------
    //何もしないフェーズ
    //------------------------------------
    // 弾幕表示フェーズを「開始」に変更
    ChangeBarragePhase(BarragePhase::kStart);
    break;
  }
  case BarragePhase::kStart: {
    //-----------------------------
    //開始フェーズ
    //-----------------------------
    // データを取得
    Barrage::BarrageData barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kFourthPhase_01];
    // 弾のセット
    SetBulletId(barrage_data.detail.bullet_id);
    // 弾幕処理開始
    Barrage* barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);
    barrage->SetPositionX(GetFloatPositionX());              // 中心座標 X座標
    barrage->SetPositionY(GetFloatPositionY());              // 中心座標 Y座標

    // データを取得
    barrage = nullptr;
    barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kFourthPhase_02];
    // 弾のセット
    SetBulletId(barrage_data.detail.bullet_id);
    // 弾幕処理開始
    barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);

    // データを取得
    barrage = nullptr;
    barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kFourthPhase_03];
    // 弾のセット
    SetBulletId(barrage_data.detail.bullet_id);
    // 弾幕処理開始
    barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);

    // データを取得
    barrage = nullptr;
    barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kFourthPhase_04];
    // 弾のセット
    SetBulletId(barrage_data.detail.bullet_id);
    // 弾幕処理開始
    barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);

    // データを取得
    barrage = nullptr;
    barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kFourthPhase_05];
    // 弾のセット
    SetBulletId(barrage_data.detail.bullet_id);
    // 弾幕処理開始
    barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);

    // 累積時間をリセット
    accumulate_time_ = kResetTime;
    // 弾幕表示フェーズを「処理中」に変更
    ChangeBarragePhase(BarragePhase::kProcess);
    break;
  }
  case BarragePhase::kProcess: {
    //-----------------------------
    //処理中フェーズ
    //-----------------------------
    break;
  }
  case BarragePhase::kFinish: {
    //-----------------------------
    //終了フェーズ
    //-----------------------------
    break;
  }
  case BarragePhase::kWait: {
    //-----------------------------
    //待機フェーズ
    //-----------------------------
    break;
  }
  case BarragePhase::kNextPhase: {
    //-----------------------------
    //次のフェーズへ
    //-----------------------------
    // 弾幕処理終了
    enemy_event_interface_.OnFinishBarrage();
    return true;
  }
  }

  return false;
}

/// <summary>
/// 第５フェーズの行動
/// </summary>
/// <param name=""></param>
/// <returns> true:終了, false:処理中 </returns>
bool EnemyB::ActionFifthPhase() {

  switch (barrage_phase_) {
  case BarragePhase::kNone: {
    //------------------------------------
    //何もしないフェーズ
    //------------------------------------
    // 弾幕表示フェーズを「開始」に変更
    ChangeBarragePhase(BarragePhase::kStart);
    break;
  }
  case BarragePhase::kStart: {
    //-----------------------------
    //開始フェーズ
    //-----------------------------
    // データを取得
    Barrage::BarrageData barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kFifthPhase_01];
    // 弾のセット
    SetBulletId(barrage_data.detail.bullet_id);
    // 弾幕処理開始
    Barrage* barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);

    // データを取得
    barrage = nullptr;
    barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kFifthPhase_02];
    // 弾のセット
    SetBulletId(barrage_data.detail.bullet_id);
    // 弾幕処理開始
    barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);

    // データを取得
    barrage = nullptr;
    barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kFifthPhase_03];
    // 弾のセット
    SetBulletId(barrage_data.detail.bullet_id);
    // 弾幕処理開始
    barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);

    // データを取得
    barrage = nullptr;
    barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kFifthPhase_04];
    // 弾のセット
    SetBulletId(barrage_data.detail.bullet_id);
    // 弾幕処理開始
    barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);

    // 累積時間をリセット
    accumulate_time_ = kResetTime;
    // 弾幕表示フェーズを「処理中」に変更
    ChangeBarragePhase(BarragePhase::kProcess);
    break;
  }
  case BarragePhase::kProcess: {
    //-----------------------------
    //処理中フェーズ
    //-----------------------------
    break;
  }
  case BarragePhase::kFinish: {
    //-----------------------------
    //終了フェーズ
    //-----------------------------
    break;
  }
  case BarragePhase::kWait: {
    //-----------------------------
    //待機フェーズ
    //-----------------------------
    break;
  }
  case BarragePhase::kNextPhase: {
    //-----------------------------
    //次のフェーズへ
    //-----------------------------
    // 弾幕処理終了
    enemy_event_interface_.OnFinishBarrage();
    return true;
  }
  }

  return false;
}

/// <summary>
/// 第６フェーズの行動
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true:終了, false:処理中 </returns>
bool EnemyB::ActionSixthPhase(float process_time) {

  switch (barrage_phase_) {
  case BarragePhase::kNone: {
    //------------------------------------
    //何もしないフェーズ
    //------------------------------------
    // 弾幕表示フェーズを「開始」に変更
    ChangeBarragePhase(BarragePhase::kStart);
    break;
  }
  case BarragePhase::kStart: {
    //-----------------------------
    //開始フェーズ
    //-----------------------------
    // データを取得
    Barrage::BarrageData barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kSixthPhase_01];
    // 弾幕処理開始
    Barrage* barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);

    // データを取得
    barrage = nullptr;
    barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kSixthPhase_02];
    // 弾幕処理開始
    barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);

    // データを取得
    barrage = nullptr;
    barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kSixthPhase_03];
    // 弾幕処理開始
    barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);

    // データを取得
    barrage = nullptr;
    barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kSixthPhase_04];
    // 弾幕処理開始
    barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);

    // データを取得
    barrage = nullptr;
    barrage_data = {};
    barrage_data = phase_data_[ActionBarrageType::kSixthPhase_05];
    // 弾幕処理開始
    barrage = enemy_event_interface_.OnStartBarrage(barrage_data.detail.barrage_id);
    if (barrage == nullptr) {
      break;
    }
    // 弾幕の各種設定
    SetBarrageData(barrage, barrage_data);

    // 累積時間をリセット
    accumulate_time_ = kResetTime;
    // 弾幕表示フェーズを「処理中」に変更
    ChangeBarragePhase(BarragePhase::kProcess);
    break;
  }
  case BarragePhase::kProcess: {
    //-----------------------------
    //処理中フェーズ
    //-----------------------------
    break;
  }
  case BarragePhase::kFinish: {
    //-----------------------------
    //終了フェーズ
    //-----------------------------
    break;
  }
  case BarragePhase::kWait: {
    //-----------------------------
    //待機フェーズ
    //-----------------------------
    break;
  }
  case BarragePhase::kNextPhase: {
    //-----------------------------
    //次のフェーズへ
    //-----------------------------
    // 弾幕処理終了
    enemy_event_interface_.OnFinishBarrage();
    return true;
  }
  }

  return false;
}

/// <summary>
/// アイテム生成処理
/// </summary>
/// <param name="character_type"> キャラクターの種類 </param>
/// <returns> true:終了, false:処理中 </returns>
bool EnemyB::DisplayItem(CharacterType character_type) {

  switch (disp_item_phase_) {
  case DispItemPhase::kCreate: {
    //--------------------------------
    //生成フェーズ
    //--------------------------------
    enemy_event_interface_.OnCreateItem(character_type);
    // フェーズを「表示」に変更する
    ChangeDispItemPhase(DispItemPhase::kDisplay);
    break;
  }
  case DispItemPhase::kDisplay: {
    //--------------------------------
    //表示フェーズ
    //--------------------------------
    break;
  }
  case DispItemPhase::kFinishNotice: {
    //--------------------------------
    //終了通知フェーズ
    //--------------------------------
    // 累積時間をリセットする
    accumulate_time_ = kResetTime;
    // フェーズを「終了処理」に変更する
    ChangeDispItemPhase(DispItemPhase::kFinish);
    break;
  }
  case DispItemPhase::kFinish: {
    //--------------------------------
    //終了処理フェーズ
    //--------------------------------
    if (accumulate_time_ >= kDispItemWaitTime) {
      // フェーズを「終了処理済み」に変更する
      ChangeDispItemPhase(DispItemPhase::kFinished);
      return true;
    }
    break;
  }
  }

  return false;
}

/// <summary>
/// カットイン処理
/// </summary>
/// <param name="phase_num"> フェーズ数 </param>
/// <param name="color_type"> カットインの色 </param>
/// <returns> true:終了, false:処理中 </returns>
bool EnemyB::DisplayCutIn(int phase_num, CutInManager::ColorType color_type) {

  switch (cut_in_phase_) {
  case CutInPhase::kCreate: {
    //--------------------------------
    //生成フェーズ
    //--------------------------------
    enemy_event_interface_.OnDisplayCutIn(phase_num, color_type);
    // 累積時間をリセットする
    accumulate_time_ = kResetTime;
    // HPをセットし直す
    SetEnemyBPhaseHitPoint();
    // フェーズを「HPバー生成」に変更する
    ChangeCutInPhase(CutInPhase::kHpBarCreate);
    break;
  }
  case CutInPhase::kHpBarCreate: {
    //--------------------------------
    //HPバー生成フェーズ
    //--------------------------------
    if (accumulate_time_ >= kCreateHpBarWaitTime) {
      // 累積時間をリセットする
      accumulate_time_ = kResetTime;
      // HPバーの生成処理
      enemy_event_interface_.OnCreateHpBar(this);
      // フェーズを「待機」に変更する
      ChangeCutInPhase(CutInPhase::kWait);
    }
    break;
  }
  case CutInPhase::kWait: {
    //--------------------------------
    //待機フェーズ
    //--------------------------------
    break;
  }
  case CutInPhase::kFinishNotice: {
    //--------------------------------
    //終了通知フェーズ
    //--------------------------------
    // 累積時間をリセットする
    accumulate_time_ = kResetTime;
    // フェーズを「終了処理」に変更する
    ChangeCutInPhase(CutInPhase::kFinish);
    break;
  }
  case CutInPhase::kFinish: {
    //--------------------------------
    //終了処理フェーズ
    //--------------------------------
    if (accumulate_time_ >= kCutInWaitTime) {
      // フェーズを「終了処理済み」に変更する
      ChangeCutInPhase(CutInPhase::kFinished);
      return true;
    }
    break;
  }
  }

  return false;
}

/// <summary>
/// 次の移動先を計算処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void EnemyB::CalcMovePosition(float process_time) {

  if (current_phase_ != PhaseType::kProcess) {
    return;
  }

  switch (move_type_) {
  case MoveType::kStop: {
    //------------------------------
    //停止フェーズ
    //------------------------------
    break;
  }
  case MoveType::kLinear: {
    //------------------------------
    //直線移動フェーズ
    //------------------------------
    CalcLinearMovement();
    break;
  }
  case MoveType::kCurve: {
    //------------------------------
    //曲線移動フェーズ
    //------------------------------
    break;
  }
  }
}

/// <summary>
/// 直線移動の計算
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void EnemyB::CalcLinearMovement() {

  int move_x = 0;
  int move_y = 0;

  // 現在の位置座標を取得
  int x_pos = GetPositionX();
  int y_pos = GetPositionY();

  Character::CharaData chara_data = GetCharacterData();
  float move_rate = chara_data.rate.move;

  // 移動方向を取得
  Character::DirectionType direction_type = GetDirectionType();
  switch (direction_type) {
  case DirectionType::kRight: {
    // 次の移動座標
    int x_pos_move = x_pos + static_cast<int>((x_pos_goal_ - x_pos_start_) * move_rate);
    int y_pos_move = y_pos + static_cast<int>((y_pos_goal_ - y_pos_start_) * move_rate);
    // 移動量を求める
    move_x = x_pos_move - x_pos;
    move_y = y_pos_move - y_pos;
    break;
  }
  case DirectionType::kLeft: {
    // 次の移動座標
    int x_pos_move = x_pos - static_cast<int>((x_pos_goal_ - x_pos_start_) * move_rate);
    int y_pos_move = y_pos - static_cast<int>((y_pos_goal_ - y_pos_start_) * move_rate);
    // 移動量を求める
    move_x = x_pos - x_pos_move;
    move_y = y_pos - y_pos_move;
    break;
  }
  }

  // 移動量をセットする
  SetMoveValueX(move_x);
  SetMoveValueY(move_y);
}

/// <summary>
/// 移動処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void EnemyB::MoveEnemy(float process_time) {

  // 累積時間を加算
  move_accum_time_ += process_time;

  // 現在の移動方向を取得
  Character::DirectionType current_direction = GetDirectionType();
  // 現在の移動方向が1フレーム前の移動方向と異なる場合は移動累積時間をリセットする
  if (direction_ != current_direction) {
    move_accum_time_ = kResetTime;
    direction_ = current_direction;
  }

  if (move_accum_time_ < move_wait_time_) {
    return;
  }

  // 移動方向を取得
  Character::DirectionType direction_type = GetDirectionType();
  switch (direction_type) {
  case DirectionType::kLeft: {
    enemy_controller_.MoveLeft();
    break;
  }
  case DirectionType::kRight: {
    enemy_controller_.MoveRight();
    break;
  }
  }
}

/// <summary>
/// 指定位置へ移動処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
bool EnemyB::MoveSpecifiedEnemy() {

  switch (specified_phase_) {
  case SpecifiedPhase::kBeforeMove: {
    //------------------------------
    //移動前フェーズ
    //------------------------------
    // フェードアウト処理完了後、指定位置移動フェーズを「移動」に変更
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      ChangeSpecifiedPhase(SpecifiedPhase::kMove);
    }
    break;
  }
  case SpecifiedPhase::kMove: {
    //------------------------------
    //移動フェーズ
    //------------------------------
    SetPositionX(GetInitialPositionX());
    SetPositionY(GetInitialPositionY());
    // 透過率調整値の符号を反転しておく
    ChangeAlphaAdjustSign();
    // 指定位置移動フェーズを「移動後処理」に変更
    ChangeSpecifiedPhase(SpecifiedPhase::kAfterMove);
    break;
  }
  case SpecifiedPhase::kAfterMove: {
    //------------------------------
    //移動後フェーズ
    //------------------------------
    // フェードイン処理完了後、指定位置移動フェーズを「終了処理」に変更
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 透過率調整値の符号を反転しておく
      ChangeAlphaAdjustSign();
      ChangeSpecifiedPhase(SpecifiedPhase::kFinsh);
    }
    break;
  }
  case SpecifiedPhase::kFinsh: {
    // 指定位置移動フェーズを「終了処理済み」に変更
    ChangeSpecifiedPhase(SpecifiedPhase::kFinshed);
    return true;
  }
  }

  return false;
}

/// <summary>
/// 行動フェーズ中の処理をセットする
/// </summary>
/// <param name="type"> 行動フェーズ中の処理の種類 </param>
/// <param name="barrage_data"> フェーズデータ </param>
/// <returns></returns>
void EnemyB::SetActionBarrageType(ActionBarrageType type, Barrage::BarrageData barrage_data) {

  // 既にリストに登録済みの場合は処理終了
  if (phase_data_.find(type) != phase_data_.end()) {
    return;
  }

  phase_data_[type] = barrage_data;
}

/// <summary>
/// 弾幕の設定
/// </summary>
/// <param name="barrage"> バレッジ </param>
/// <param name="barrage_data"> 弾幕情報 </param>
/// <returns></returns>
void EnemyB::SetBarrageData(Barrage* barrage, Barrage::BarrageData barrage_data) {

  barrage->SetDataBarrageId(barrage_data.detail.barrage_id);  // 弾幕の種類
  barrage->SetBulletId(barrage_data.detail.bullet_id);     // 弾の種類
  barrage->SetAimType(barrage_data.detail.aim_type);       // 目標の設定
  barrage->SetCharacter(this);                             // キャラクター
  barrage->SetDeployCount(barrage_data.count.deploy);      // 弾の配置数
  barrage->SetGenerateCount(barrage_data.count.generate);  // 生成する弾の数
  barrage->SetBulletId(barrage_data.detail.bullet_id);     // バレットID
  barrage->SetPositionX(barrage_data.pos.x);               // 中心座標 X座標
  barrage->SetPositionY(barrage_data.pos.y);               // 中心座標 Y座標
  barrage->SetExecInterval(barrage_data.interval.exec);    // 実行間隔
  barrage->SetWaitInterval(barrage_data.interval.wait);    // 待機間隔
  barrage->SetDistanceX(barrage_data.distance.x);          // 中心から離す距離 X方向
  barrage->SetDistanceY(barrage_data.distance.y);          // 中心から離す距離 Y方向
  barrage->SetGoalPositionX(barrage_data.pos.x_goal);      // 目標座標 X座標
  barrage->SetGoalPositionY(barrage_data.pos.y_goal);      // 目標座標 Y座標
  barrage->SetMoveRate(barrage_data.rate.move);            // 弾の移動割合(移動量の係数)
  barrage->SetExpaValue(barrage_data.expa.value);          // 弾の拡大率
  barrage->SetExpaAdjust(barrage_data.expa.adjust);        // 弾の拡大率調整値
  barrage->SetExpaMax(barrage_data.expa.max);              // 弾の拡大率最大値
  barrage->SetExpaMin(barrage_data.expa.min);              // 弾の拡大率最小値
  barrage->SetAlphaValue(barrage_data.alpha.value);        // 弾の透過率
  barrage->SetAlphaAdjust(barrage_data.alpha.adjust);      // 弾の透過率調整値
  barrage->SetAlphaMax(barrage_data.alpha.max);            // 弾の透過率最大値
  barrage->SetAlphaMin(barrage_data.alpha.min);            // 弾の透過率最小値
  barrage->SetAngle(barrage_data.angle.value);             // 角度
  barrage->SetAngleAdjust(barrage_data.angle.adjust);      // 角度の調整量
  barrage->SetExecCount(barrage_data.count.execute);       // 実行回数
  barrage->SetAcceleRateX(barrage_data.rate.acce_x);       // 加速度(X方向)
  barrage->SetAcceleRateY(barrage_data.rate.acce_y);       // 加速度(Y方向)
  barrage->SetSpeedXMax(barrage_data.speed.x_max);         // 速度(X方向)最大値
  barrage->SetSpeedYMax(barrage_data.speed.y_max);         // 速度(Y方向)最大値
  barrage->SetSpeedXMin(barrage_data.speed.x_min);         // 速度(X方向)最小値
  barrage->SetSpeedYMin(barrage_data.speed.y_min);         // 速度(Y方向)最小値
  barrage->SetWaitTimeMoveStart(barrage_data.time.wait_start);    // 動作待機時間
  barrage->SetWaitTimeAcceleration(barrage_data.time.wait_accel); // 加速待機時間
  barrage->SetWaitTimeExpantion(barrage_data.time.wait_expan);    // 拡大待機時間
}

/// <summary>
/// フェーズごとのHPの設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void EnemyB::SetEnemyBPhaseHitPoint() {

  // ヒットポイント情報を取得
  HitPoint hit_point = GetPhaseHitPoint();

  switch (action_phase_) {
  case ActionPhase::kFirstPhaseCutIn: {
    //---------------------------------
    //第１フェーズ
    //---------------------------------
    SetHitPoint(hit_point.phase_first);
    SetInitialHitPoint(hit_point.phase_first);
    break;
  }
  case ActionPhase::kSecondPhaseCutIn: {
    //---------------------------------
    //第２フェーズ
    //---------------------------------
    SetHitPoint(hit_point.phase_second);
    SetInitialHitPoint(hit_point.phase_second);
    break;
  }
  case ActionPhase::kThirdPhaseCutIn: {
    //---------------------------------
    //第３フェーズ
    //---------------------------------
    SetHitPoint(hit_point.phase_third);
    SetInitialHitPoint(hit_point.phase_third);
    break;
  }
  case ActionPhase::kFourthPhaseCutIn: {
    //---------------------------------
    //第４フェーズ
    //---------------------------------
    SetHitPoint(hit_point.phase_fourth);
    SetInitialHitPoint(hit_point.phase_fourth);
    break;
  }
  case ActionPhase::kFifthPhaseCutIn: {
    //---------------------------------
    //第５フェーズ
    //---------------------------------
    SetHitPoint(hit_point.phase_fifth);
    SetInitialHitPoint(hit_point.phase_fifth);
    break;
  }
  case ActionPhase::kSixthPhaseCutIn: {
    //---------------------------------
    //第６フェーズ
    //---------------------------------
    SetHitPoint(hit_point.phase_sixth);
    SetInitialHitPoint(hit_point.phase_sixth);
    break;
  }
  }

}