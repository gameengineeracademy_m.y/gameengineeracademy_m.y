﻿#pragma once

#include "System/Character.h"
#include <iostream>

/// <summary>
/// アイテムイベントインターフェース
/// </summary>
class ItemEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ItemEventInterface() {

    // コンソールに出力
    std::cout << "ItemEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~ItemEventInterface() {

    // コンソールに出力
    std::cout << "~ItemEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// アイテム処理の終了を通知する
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  virtual void OnFinishItem(class Character*) = 0;
};