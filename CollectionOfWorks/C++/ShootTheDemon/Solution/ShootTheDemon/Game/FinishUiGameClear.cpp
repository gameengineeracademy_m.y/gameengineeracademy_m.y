﻿#include "Game/FinishUiGameClear.h"

namespace {

  /// <summary>
  /// ゲームクリアテキスト
  /// </summary>
  const char* kGameClearText = "Game Clear!";

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Reggae One";

  /// <summary>
  /// タイトル文字フォントサイズ
  /// </summary>
  const int kFontSize = 85;

  /// <summary>
  /// タイトル文字幅
  /// </summary>
  const int kFontWidth = 1;

  /// <summary>
  /// メニュー画面 テキスト文字色
  /// </summary>
  const int kTextForeColor = GetColor(255, 255, 55);

  /// <summary>
  /// テキスト 表示位置 調整値
  /// </summary>
  const int kTextPosY = 150;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjust = 5;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 演出待機時間
  /// </summary>
  const float kProductionWaitTime = 5.0f;

  /// <summary>
  /// 演出終了時間
  /// </summary>
  const float kProductionEndWaitTime = 0.5f;

  /// <summary>
  /// 花火の打ち上げ間隔
  /// </summary>
  const float kFireworkShootTime = 1.5f;

  /// <summary>
  /// リセット時間
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 半分
  /// </summary>
  const int kHalfValue = 2;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name="event_interface"> ゲームクリア処理イベントインターフェース </param>
/// <returns></returns>
FinishUiGameClear::FinishUiGameClear(FinishUiGameClearEventInterface& event_interface)
  : Task(TaskId::kFinishUi)
  , current_phase_(PhaseType::kInitialize)
  , game_clear_(GameClearPhase::kClearTextView)
  , firework_type_(FireworkType::kType1)
  , game_clear_event_interface_(event_interface)
  , text_()
  , alpha_text_()
  , accumulate_time_(0.0f)
  , firework_time_(0.0f)
  , font_handle_(0) {

  // コンソールに出力
  std::cout << "FinishUiGameClear コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
FinishUiGameClear::~FinishUiGameClear() {

  // コンソールに出力
  std::cout << "~FinishUiGameClear デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void FinishUiGameClear::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //--------------------------
    //初期化フェーズ
    //--------------------------
    Initialize();
    // 現在のフェーズを「起動」に変更
    ChangeCurrentPhase(PhaseType::kStartUp);
    break;
  }
  case PhaseType::kStartUp: {
    //--------------------------
    //起動フェーズ
    //--------------------------
    // 現在のフェーズを「処理中」に変更
    ChangeCurrentPhase(PhaseType::kProcess);
    break;
  }
  case PhaseType::kProcess: {
    //--------------------------
    //処理中フェーズ
    //--------------------------
    bool is_finish = Process(process_time);
    if (is_finish) {
      // 現在のフェーズを「終了処理前」に変更
      ChangeCurrentPhase(PhaseType::kFinalizeBefore);
    }
    break;
  }
  case PhaseType::kFinalizeBefore: {
    //--------------------------
    //終了処理前フェーズ
    //--------------------------
    // 現在のフェーズを「終了処理」に変更
    ChangeCurrentPhase(PhaseType::kFinalize);
    break;
  }
  case PhaseType::kFinalize: {
    //--------------------------
    //終了処理フェーズ
    //--------------------------
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void FinishUiGameClear::Render() {

  if (current_phase_ != PhaseType::kProcess) {
    return;
  }

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_text_.value);

  DrawStringToHandle(text_.x, text_.y, kGameClearText, kTextForeColor, font_handle_);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool FinishUiGameClear::Initialize() {

  // ゲーム情報取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return false;
  }
  // 画面中央座標を取得
  int x_pos_center = game_info->GetCenterPointX();
  int y_pos_center = game_info->GetCenterPointY();

  // フォント情報生成
  FontInfo* font_info = FontInfo::GetFontInfoInstance();
  if (font_info == nullptr) {
    return false;
  }
  font_handle_ = font_info->GetFontInfo(kFontName, kFontSize, kFontWidth, DX_FONTTYPE_NORMAL);

  // 文字テキスト表示位置
  int text_width = GetDrawStringWidthToHandle(kGameClearText, static_cast<int>(strlen(kGameClearText)), font_handle_);
  text_.x = x_pos_center - (text_width / kHalfValue);
  text_.y = y_pos_center - kTextPosY;

  // テキストの透過率の設定
  alpha_text_.value = kAlphaMin;
  alpha_text_.adjust = kAlphaAdjust;
  alpha_text_.max = kAlphaMax;
  alpha_text_.min = kAlphaMin;

  return true;
}

/// <summary>
/// 処理中
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool FinishUiGameClear::Process(float process_time) {

  switch (game_clear_) {
  case GameClearPhase::kProductionWait:
  case GameClearPhase::kClearTextFinish:
  case GameClearPhase::kWait:
  case GameClearPhase::kBeforeFinalize: {
    //----------------------------------
    //花火の演出
    //----------------------------------
    ShootFirework(process_time);
    break;
  }
  }

  switch (game_clear_) {
  case GameClearPhase::kClearTextView: {
    //----------------------------------
    //ゲームクリアテキスト表示
    //----------------------------------
    bool is_finish = ViewClearText();
    if (is_finish) {
      // フェーズを「演出を楽しみながら待機」に変更
      ChangeGameClearPhase(GameClearPhase::kProductionWait);
    }
    break;
  }
  case GameClearPhase::kProductionWait: {
    //----------------------------------
    //演出を楽しみながら待機
    //----------------------------------
    // 累積時間に加算する
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kProductionWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // フェーズを「クリアテキスト表示終了」に変更
      ChangeGameClearPhase(GameClearPhase::kClearTextFinish);
    }
    break;
  }
  case GameClearPhase::kClearTextFinish: {
    //----------------------------------
    //ゲームクリアテキスト表示終了
    //----------------------------------
    bool is_finish = HideClearText();
    if (is_finish) {
      // テキスト非表示終了通知
      game_clear_event_interface_.OnFinishHideGameClearText();
      // フェーズを「待機」に変更
      ChangeGameClearPhase(GameClearPhase::kWait);
    }
    break;
  }
  case GameClearPhase::kWait: {
    //----------------------------------
    //待機
    //----------------------------------
    break;
  }
  case GameClearPhase::kBeforeFinalize: {
    //----------------------------------
    //終了処理前フェーズ
    //----------------------------------
    // 累積時間に加算する
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kProductionEndWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // フェーズを「クリアテキスト表示終了」に変更
      ChangeGameClearPhase(GameClearPhase::kFinalize);
    }
    break;
  }
  case GameClearPhase::kFinalize: {
    //----------------------------------
    //終了処理フェーズ
    //----------------------------------
    // フェーズを「終了処理済み」に変更
    ChangeGameClearPhase(GameClearPhase::kFinalized);
    return true;
  }
  }

  return false;
}

/// <summary>
/// テキスト表示
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool FinishUiGameClear::ViewClearText() {

  // 透過率を加算
  alpha_text_.value += alpha_text_.adjust;

  if (alpha_text_.value >= alpha_text_.max) {
    alpha_text_.value = alpha_text_.max;
    return true;
  }

  return false;
}

/// <summary>
/// テキスト非表示
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool FinishUiGameClear::HideClearText() {

  // 透過率を減算
  alpha_text_.value -= alpha_text_.adjust;

  if (alpha_text_.value <= alpha_text_.min) {
    alpha_text_.value = alpha_text_.min;
    return true;
  }

  return false;
}

/// <summary>
/// 花火打ち上げ処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void FinishUiGameClear::ShootFirework(float process_time) {

  // 花火打ち上げ
  switch (firework_type_) {
  case FireworkType::kType1: {
    //----------------------------------
    //タイプ１
    //----------------------------------
    firework_time_ += process_time;
    if (firework_time_ >= kFireworkShootTime) {
      // 累積時間をリセット
      firework_time_ = kResetTime;

      game_clear_event_interface_.OnShootFirework1();
      // 花火の種類を変更する
      ChangeFireworkType(FireworkType::kType2);
    }
    break;
  }
  case FireworkType::kType2: {
    //----------------------------------
    //タイプ２
    //----------------------------------
    firework_time_ += process_time;
    if (firework_time_ >= kFireworkShootTime) {
      // 累積時間をリセット
      firework_time_ = kResetTime;

      game_clear_event_interface_.OnShootFirework2();
      // 花火の種類を変更する
      ChangeFireworkType(FireworkType::kType3);
    }
    break;
  }
  case FireworkType::kType3: {
    //----------------------------------
    //タイプ３
    //----------------------------------
    firework_time_ += process_time;
    if (firework_time_ >= kFireworkShootTime) {
      // 累積時間をリセット
      firework_time_ = kResetTime;

      game_clear_event_interface_.OnShootFirework3();
      // 花火の種類を変更する
      ChangeFireworkType(FireworkType::kType1);
    }
    break;
  }
  }
}