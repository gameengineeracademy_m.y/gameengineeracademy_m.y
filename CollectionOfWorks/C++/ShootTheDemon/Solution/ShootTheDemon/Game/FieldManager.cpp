﻿#include "Game/FieldManager.h"

namespace {

  /// <summary>
  /// フィールド画像保存パス
  /// </summary>
  const char* kImageBackgroundPath = "Asset/Image/background.png";

  /// <summary>
  /// 総分割数
  /// </summary>
  const int kTotalDivNum = 234;

  /// <summary>
  /// 分割数 横方向
  /// </summary>
  const int kDivNumX = 18;

  /// <summary>
  /// 分割数 縦方向
  /// </summary>
  const int kDivNumY = 13;

  /// <summary>
  /// 分割後 画像サイズ
  /// </summary>
  const int kImageSize = 32;

  /// <summary>
  /// エラー
  /// </summary>
  const int kLoadError = -1;

  /// <summary>
  /// 高さ格納数
  /// </summary>
  const int kArrayHeight = 2 * kDivNumY;

  /// <summary>
  /// スクロール X方向移動量
  /// </summary>
  const int kMoveValueX = 0;

  /// <summary>
  /// スクロール Y方向移動量
  /// </summary>
  const int kMoveValueY = 1;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
FieldManager::FieldManager()
  : Task(TaskId::kFieldManager)
  , current_phase_(PhaseType::kInitialize)
  , field_list_() {

  // コンソールに出力
  std::cout << "FieldManager コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
FieldManager::~FieldManager() {

  // コンソールに出力
  std::cout << "~FieldManager デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void FieldManager::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //------------------------------
    //初期化フェーズ
    //------------------------------
    bool is_finish = Initialize();
    if (is_finish) {
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kPrepare);
    }
    break;
  }
  case PhaseType::kPrepare: {
    bool is_finish = Prepare();
    if (is_finish) {
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //------------------------------
    //処理中フェーズ
    //------------------------------
    // 背景処理(毎フレーム)
    UpdateField();
    // 現在のフェーズを「処理待機」に変更
    ChangeCurrentPhase(PhaseType::kProcessWait);
    break;
  }
  case PhaseType::kProcessWait: {
    //------------------------------
    //処理待機フェーズ
    //------------------------------
    // 現在のフェーズを「処理中」に変更
    ChangeCurrentPhase(PhaseType::kProcess);
    break;
  }
  case PhaseType::kStop: {
    //------------------------------
    //停止中フェーズ
    //------------------------------
    break;
  }
  case PhaseType::kFinalize: {
    //------------------------------
    //終了処理フェーズ
    //------------------------------
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool FieldManager::Initialize() {

  // 配列の空情報を作成
  std::vector<Field*> initial_set_array;

  for (int x = 0; x < kDivNumX; ++x) {
    initial_set_array.push_back(nullptr);
  }

  // フィールドリストを空情報で初期化する
  field_list_.resize(kArrayHeight);

  for (int y = 0; y < kArrayHeight; ++y) {
    field_list_[y].resize(kDivNumX);
    field_list_[y] = initial_set_array;
  }

  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return false;
  }
  // フィールド情報からマップの横幅と高さを取得
  int field_width = static_cast<int>(field_list_.at(0).size()) * kImageSize;
  int field_height = static_cast<int>(field_list_.size()) * kImageSize;

  // ゲーム情報にマップサイズをセット
  game_info->SetMapSize(field_width, field_height);

  return true;
}

/// <summary>
/// 準備処理
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool FieldManager::Prepare() {

  int graphic_handle[kTotalDivNum];
  // 画像のロード
  int result = LoadDivGraph(kImageBackgroundPath, kTotalDivNum, kDivNumX, kDivNumY, kImageSize, kImageSize, graphic_handle);
  if (result == kLoadError) {
    return false;
  }

  // ゲーム情報からマップ表示位置を取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return false;
  }
  int disp_pos_x = game_info->GetMapPositionX();
  int disp_pos_y = game_info->GetMapPositionY();

  int index = 0;
  // 背景の生成
  for (int y = 0; y < kArrayHeight; ++y) {
    for (int x = 0; x < kDivNumX; ++x) {
      // 背景の生成
      Field* field = new Field();
      // フィールド処理に表示座標、移動量のセット
      field->SetPositionX(disp_pos_x + x * kImageSize);
      field->SetPositionY(disp_pos_y + y * kImageSize);
      field->SetMoveValueX(kMoveValueX);
      field->SetMoveValueY(kMoveValueY);
      // グラフィックハンドルを渡す
      field->SetGraphicHandle(graphic_handle[index]);
      // インデックスをインクリメント
      ++index;
      if (index >= kTotalDivNum) {
        index = 0;
      }
      // リストに格納
      field_list_.at(y).at(x) = field;
    }
  }

  return true;
}

/// <summary>
/// フィールド処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void FieldManager::UpdateField() {

  // 描画位置の設定
  for (int y = 0; y < static_cast<int>(field_list_.size()); ++y) {
    for (int x = 0; x < static_cast<int>(field_list_.at(0).size()); ++x) {
      if (field_list_.at(y).at(x) != nullptr) {
        field_list_.at(y).at(x)->Update();
      }
    }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void FieldManager::Render() {

  // 描画処理
  for (int y = 0; y < static_cast<int>(field_list_.size()); ++y) {
    for (int x = 0; x < static_cast<int>(field_list_.at(0).size()); ++x) {
      if (field_list_.at(y).at(x) != nullptr) {
        field_list_.at(y).at(x)->Render();
      }
    }
  }
}

/// <summary>
/// フィールド処理のフェーズを変更する
/// </summary>
/// <param name=""> フィールドのフェーズの種類 </param>
/// <returns></returns>
void FieldManager::ChangeFieldPhase(Field::PhaseType phase_type) {

  for (int y = 0; y < static_cast<int>(field_list_.size()); ++y) {
    for (int x = 0; x < static_cast<int>(field_list_.at(0).size()); ++x) {
      if (field_list_.at(y).at(x) != nullptr) {
        field_list_.at(y).at(x)->ChangeCurrentPhase(phase_type);
      }
    }
  }
}

/// <summary>
/// フィールドの破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void FieldManager::FieldDispose() {

  for (int y = 0; y < static_cast<int>(field_list_.size()); ++y) {
    for (int x = 0; x < static_cast<int>(field_list_.at(0).size()); ++x) {

      if (field_list_.at(y).at(x) != nullptr) {
        if (x < kDivNumX && y < kDivNumY) {
          // 画像リソースを取得し、破棄する
          int graphic_handle = field_list_.at(y).at(x)->GetGraphicHandle();
          DeleteGraph(graphic_handle);
        }
        // 背景の破棄
        delete field_list_.at(y).at(x);
        field_list_.at(y).at(x) = nullptr;
      }
    }
  }
}