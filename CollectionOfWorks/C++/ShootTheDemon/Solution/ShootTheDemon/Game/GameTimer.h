﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "System/FontInfo.h"
#include <array>
#include <vector>

namespace {

  /// <summary>
  /// 画像分割数
  /// </summary>
  const int kImageNumberDivNum = 13;
}

/// <summary>
/// ゲームタイマー処理
/// </summary>
class GameTimer : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,          // 初期化処理フェーズ
    kStartWait,           // 起動待機フェーズ
    kStartUp,             // 起動フェーズ
    kProcessWait,         // 処理待機フェーズ
    kProcess,             // 処理中フェーズ
    kStop,                // 停止フェーズ
    kBeforeFinalize,      // 終了処理前フェーズ
    kFinalize,            // 終了処理フェーズ
    kFinalized,           // 終了処理済みフェーズ
    kPhaseMaxIndex        // フェーズ数
  };

  /// <summary>
  /// 敵フェーズの種類
  /// </summary>
  enum class EnemyPhase {
    kEnemyA,              // 敵キャラA
    kEnemyB,              // 敵キャラB
    kPhaseMaxIndex        // フェーズ数
  };

  /// <summary>
  /// フォントサイズの種類
  /// </summary>
  enum class FontSize {
    k18,               // サイズ18
    k20,               // サイズ20
    k25,               // サイズ25
    kSizeMaxIndex      // 種類数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  GameTimer();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~GameTimer();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// グラフィックハンドルをセットする
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandle(std::array<int, kImageNumberDivNum> handle) { graphic_handle_ = handle; }

  /// <summary>
  /// グラフィックハンドルを取得する
  /// </summary>
  /// <param name="index"> インデックス </param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandle(int index) { return graphic_handle_.at(index); }

  /// <summary>
  /// ラップタイムをセット
  /// </summary>
  /// <param name=""> 敵の種類 </param>
  /// <param name=""> フェーズ数 </param>
  /// <returns></returns>
  void SetRapTime(EnemyPhase, int);

  /// <summary>
  /// タイマーを止める
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void StopTime() { current_phase_ = PhaseType::kStop; }

  /// <summary>
  /// タイマーの値を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> タイム </returns>
  float GetTime() { return timer_; }

  /// <summary>
  /// 現在のフェーズを変更
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Initialize();

  /// <summary>
  /// 処理中
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Process(float);

  /// <summary>
  /// 透過率調整処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AdjustAlphaValue();


private:

  /// <summary>
  /// インデックス
  /// </summary>
  struct Time {
    int min_tens;
    int min_ones;
    int sec_tens;
    int sec_ones;
    int msec_huns;
    int msec_tens;
    int msec_ones;
    int colon;
  };

  /// <summary>
  /// 座標
  /// </summary>
  struct Pos {
    int x;
    int y;
  };

  /// <summary>
  /// 表示座標
  /// </summary>
  struct TimeDispPos {
    Pos min_tens;
    Pos min_ones;
    Pos min_cln_01;
    Pos min_cln_02;
    Pos sec_tens;
    Pos sec_ones;
    Pos sec_period;
    Pos msec_huns;
    Pos msec_tens;
    Pos msec_ones;
  };

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// グラフィックハンドル
  /// </summary>
  std::array<int, kImageNumberDivNum> graphic_handle_;

  /// <summary>
  /// グラフィックハンドル ゲームタイム
  /// </summary>
  Time time_;

  /// <summary>
  /// グラフィックハンドル ラップタイム
  /// </summary>
  std::vector<Time> enemy_a_phase_time_;

  /// <summary>
  /// グラフィックハンドル ラップタイム
  /// </summary>
  std::vector<Time> enemy_b_phase_time_;

  /// <summary>
  /// 表示位置 ゲームタイム
  /// </summary>
  TimeDispPos time_pos_;

  /// <summary>
  /// 表示位置 敵キャラAフェーズ タイム
  /// </summary>
  std::vector<TimeDispPos> enemy_a_phase_time_pos_;

  /// <summary>
  /// 表示位置 敵キャラBフェーズ タイム
  /// </summary>
  std::vector<TimeDispPos> enemy_b_phase_time_pos_;

  /// <summary>
  /// タイマー ゲームタイム
  /// </summary>
  float timer_;

  /// <summary>
  /// タイマー 現フェーズタイム
  /// </summary>
  float rap_time_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// フォントハンドル
  /// </summary>
  std::vector<int> font_handle_;
};