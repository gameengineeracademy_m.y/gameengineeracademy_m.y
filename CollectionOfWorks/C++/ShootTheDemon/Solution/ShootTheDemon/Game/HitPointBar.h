﻿#pragma once

#include "DxLib.h"
#include "System/Character.h"
#include "Game/GameInfo.h"
#include "Game/HitPointBarId.h"

/// <summary>
/// ヒットポイントバー
/// </summary>
class HitPointBar {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kStartWait,       // 起動待機フェーズ
    kPrepare,         // 準備処理フェーズ
    kProcess,         // 処理中フェーズ
    kFinalizeBefore,  // 終了処理前フェーズ
    kFinalize,        // 終了処理フェーズ
    kFinalized,       // 終了処理済みフェーズ
    kPhaseMaxIndex    // フェーズ数
  };

  /// <summary>
  /// 画面揺らし
  /// </summary>
  enum class ShakePhase {
    kNone,            // 何もしない
    kExecute,         // 画面を揺らす
    kPhaseMaxIndex    // フェーズ数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> ヒットポイントバーID </param>
  /// <returns></returns>
  HitPointBar(HitPointBarId);

  /// <summary>
  /// コピーコンストラクタ
  /// </summary>
  /// <param name=""> コピー元クラス </param>
  /// <returns></returns>
  HitPointBar(const HitPointBar&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~HitPointBar();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Update(float);

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render();

  /// <summary>
  /// HPバークローン生成
  /// </summary>
  /// <param name=""></param>
  /// <returns> HPバー </returns>
  HitPointBar* GenerateClone();

  /// <summary>
  /// キャラクターをセット
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  void SetCharacter(Character* character) { character_ = character; }

  /// <summary>
  /// キャラクターを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> キャラクター </returns>
  Character* GetCharacter() { return character_; }

  /// <summary>
  /// リストIDをセット
  /// </summary>
  /// <param name=""> リストID </param>
  /// <returns></returns>
  void SetListId(int list_id) { list_id_ = list_id; }

  /// <summary>
  /// リストIDを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> リストID </returns>
  int GetListId() { return list_id_; };

  /// <summary>
  /// ボスキャラの初期表示座標のセット
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetInitialBossPosition();

  /// <summary>
  /// 表示座標のセット
  /// </summary>
  /// <param name="x_pos"> X座標 </param>
  /// <param name="y_pos"> Y座標 </param>
  /// <param name="half_height"> 画像の半分の高さ </param>
  /// <returns></returns>
  void SetPosition(float, float, float);

  /// <summary>
  /// HPバーの初期値セット
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetInitialHpBar();

  /// <summary>
  /// サイズのセット
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetSize();

  /// <summary>
  /// 画面を揺らす量のセット
  /// </summary>
  /// <param name=""> X方向 </param>
  /// <param name=""> Y方向 </param>
  /// <returns></returns>
  void SetShakeValue(float, float);

  /// <summary>
  /// キャラクターヒットポイントをセット
  /// </summary>
  /// <param name=""> ヒットポイント </param>
  /// <returns></returns>
  void SetHitPoint(int hit_point) { hit_point_ = hit_point; }

  /// <summary>
  /// 処理終了フラグをセット
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetFinish() { is_finish_ = true; }

  /// <summary>
  /// 処理終了フラグを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了, false:継続 </returns>
  bool GetFinish() { return is_finish_; }

  /// <summary>
  /// 終了しているかどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了, false:継続 </returns>
  bool IsFinish() { return is_finish_ == true; }

  /// <summary>
  /// 現在のフェーズを変更
  /// </summary>
  /// <param name="phase_type"> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }

  /// <summary>
  /// 画面揺らしを開始する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void StartScreenShake();

  /// <summary>
  /// 画面揺らしを停止する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void StopScreenShake();


private:

  /// <summary>
  /// 開始準備処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Prepare();

  /// <summary>
  /// 処理中
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Process(float);

  /// <summary>
  /// 透過率変更処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:継続処理中 </returns>
  bool AdjustAlphaValue();


private:

  /// <summary>
  /// 表示座標
  /// </summary>
  struct Pos {
    float x;
    float y;
  };

  /// <summary>
  /// サイズ
  /// </summary>
  struct Size {
    float width;
    float height;
  };

  /// <summary>
  /// フレーム
  /// </summary>
  struct Frame {
    Pos left;
    Pos right;
    Pos shake;
    Size size;
  };

  /// <summary>
  /// HPバー
  /// </summary>
  struct HpBar {
    Pos left;
    Pos right;
  };

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 画面揺らしのフェーズ
  /// </summary>
  ShakePhase shake_phase_;

  /// <summary>
  /// ヒットポイントバーID
  /// </summary>
  HitPointBarId hit_point_bar_id_;

  /// <summary>
  /// キャラクター
  /// </summary>
  Character* character_;

  /// <summary>
  /// HPバーの枠
  /// </summary>
  Frame frame_;

  /// <summary>
  /// HPバー
  /// </summary>
  HpBar hp_bar_;

  /// <summary>
  /// 生成リストのID
  /// </summary>
  int list_id_;

  /// <summary>
  /// HPバー調整値
  /// </summary>
  float hp_bar_adjust_;

  /// <summary>
  /// HPバーの色
  /// </summary>
  int hp_bar_color_;

  /// <summary>
  /// フレームの1/8の長さ
  /// </summary>
  float width_one_eighth_;

  /// <summary>
  /// 1フレームの前のキャラクターのHP
  /// </summary>
  int hit_point_;

  /// <summary>
  /// 画面を揺らす際の量
  /// </summary>
  std::vector<Pos> shake_value_;

  /// <summary>
  /// 画面揺らしインデックス
  /// </summary>
  int shake_index_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  int alpha_adjust_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// 処理終了フラグ
  /// </summary>
  bool is_finish_;
};