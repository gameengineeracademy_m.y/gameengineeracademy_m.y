﻿#include "Game/ResultLevel.h"

namespace {

  /// <summary>
  /// 累積時間のリセット値
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjustInit = 5;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 背景 画像
  /// </summary>
  const char* kImageBackGroundPath = "Asset/Image/backgroundside.png";

  /// <summary>
  /// 画像ロードエラー
  /// </summary>
  const int kLoadError = -1;

  /// <summary>
  /// フォントファイルパス
  /// </summary>
  const char* kFontFilePath = "Asset/Font/ReggaeOne-Regular.ttf";

  /// <summary>
  /// サウンドデータファイル
  /// </summary>
  const char* kSoundDataFilePath = "File/SoundData.txt";

  /// <summary>
  /// スコアデータファイル
  /// </summary>
  const char* kScoreDataFilePath = "File/ScoreData.txt";

  /// <summary>
  /// ボタン選択音源パス
  /// </summary>
  const char* kSoundButtonSelectPath = "Asset/Sound/maou_se_system45.mp3";

  /// <summary>
  /// ボタン押下音源パス
  /// </summary>
  const char* kSoundButtonPushPath = "Asset/Sound/game_start.mp3";

  /// <summary>
  /// BGM音源パス
  /// </summary>
  const char* kSoundResultBgmPath = "Asset/Sound/EDIT.mp3";

  /// <summary>
  /// サウンドボリューム初期値
  /// </summary>
  const int kSoundInitialVolume = 128;

  /// <summary>
  /// サウンドボリューム調整値
  /// </summary>
  const int kSoundAdjustVolume = 10;

  /// <summary>
  /// スコア 1位 初期値
  /// </summary>
  const int kFirstScoreInit = 300000;

  /// <summary>
  /// スコア 2位 初期値
  /// </summary>
  const int kSecondScoreInit = 330000;

  /// <summary>
  /// スコア 3位 初期値
  /// </summary>
  const int kThirdScoreInit = 360000;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> タスクID </param>
/// <param name=""> タスクマネージャ </param>
/// <returns></returns>
ResultLevel::ResultLevel(TaskId task_id, TaskManager& task_manager)
  : Level(task_id, task_manager)
  , current_phase_(PhaseType::kLoadFile)
  , sound_phase_(RegisterSoundPhase::kButtonSelect)
  , finalize_phase_(FinalizePhaseType::kUnloadTask)
  , back_ground_(nullptr)
  , score_board_(nullptr)
  , info_ui_(nullptr)
  , menu_controller_(nullptr)
  , sound_manager_(nullptr)
  , sound_volume_list_()
  , accumulate_time_(0.0f)
  , alpha_(0)
  , alpha_adjust_(0)
  , transition_level_(TaskId::kTitleLevel) {

  // コンソールに出力
  std::cout << "ResultLevel コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ResultLevel::~ResultLevel() {

  // コンソールに出力
  std::cout << "~ResultLevel デストラクタ" << std::endl;
}

/// <summary>
/// 初期化処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool ResultLevel::InitializePhase(float process_time) {

  bool is_success = false;

  // 処理時間をリセットしておく
  accumulate_time_ = kResetTime;
  // 透過率 初期設定
  alpha_ = kAlphaMin;
  alpha_adjust_ = kAlphaAdjustInit;

  // フォント情報生成
  FontInfo* font_info = FontInfo::GetFontInfoInstance();
  if (font_info == nullptr) {
    return false;
  }
 int result = font_info->CreateFontInfo(kFontFilePath);

  // 背景 初期設定
  is_success = InitializeBackGround();
  if (!is_success) {
    return false;
  }

  // スコアボード 初期設定
  is_success = InitializeScoreBoard();
  if (!is_success) {
    return false;
  }

  // UI情報 初期設定
  is_success = InitializeResultInfoUi();
  if (!is_success) {
    return false;
  }

  // メニューコントローラ 初期設定
  is_success = InitializeMenuController();
  if (!is_success) {
    return false;
  }

  // サウンドマネージャ 初期設定
  is_success = InitializeSoundManager();
  if (!is_success) {
    return false;
  }

  return true;
}

/// <summary>
/// 毎フレームの処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool ResultLevel::UpdatePhase(float process_time) {

  switch (current_phase_) {
  case PhaseType::kLoadFile: {
    //------------------------------
    //設定ファイル読み込みフェーズ
    //------------------------------
    bool is_finish = LoadSetupFile();
    if (is_finish) {
      // 各クラスのフェーズを「初期化処理」に変更
      back_ground_->ChangeCurrentPhase(ResultBackGround::PhaseType::kInitialize);
      score_board_->ChangeCurrentPhase(ScoreBoard::PhaseType::kInitialize);
      info_ui_->ChangeCurrentPhase(ResultInfoUi::PhaseType::kInitialize);
      // 現在のフェーズを「準備」に変更
      ChangeCurrentPhase(PhaseType::kPrepare);
    }
    break;
  }
  case PhaseType::kPrepare: {
    //------------------------------
    //準備フェーズ
    //------------------------------
    bool is_finish = Prepare();
    if (is_finish) {
      // 現在のフェーズを「起動待機」に変更
      ChangeCurrentPhase(PhaseType::kStartWait);
    }
    break;
  }
  case PhaseType::kStartWait: {
    //------------------------------
    //起動待機フェーズ
    //------------------------------
    // 各クラスのフェーズを「起動」に変更
    back_ground_->ChangeCurrentPhase(ResultBackGround::PhaseType::kStartUp);
    score_board_->ChangeCurrentPhase(ScoreBoard::PhaseType::kStartUp);
    info_ui_->ChangeCurrentPhase(ResultInfoUi::PhaseType::kStartUp);

    // 現在のフェーズを「起動」に変更
    ChangeCurrentPhase(PhaseType::kStartUp);
    break;
  }
  case PhaseType::kStartUp: {
    //------------------------------
    //起動フェーズ
    //------------------------------
    // フェードイン処理完了後、現在のフェーズを「遷移待機」に変更
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 処理時間をリセットしておく
      accumulate_time_ = kResetTime;
      // BGMを生成
      sound_manager_->CreateSound(SoundId::kResultBgm);
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //------------------------------
    //処理中フェーズ
    //------------------------------
    break;
  }
  case PhaseType::kBeforeTransition: {
    //------------------------------
    //レベル遷移前フェーズ
    //------------------------------
    // 各クラスのフェーズを「終了処理前」に変更
    back_ground_->ChangeCurrentPhase(ResultBackGround::PhaseType::kBeforeFinalize);
    score_board_->ChangeCurrentPhase(ScoreBoard::PhaseType::kBeforeFinalize);
    info_ui_->ChangeCurrentPhase(ResultInfoUi::PhaseType::kBeforeFinalize);

    // 現在のフェーズを「レベル遷移フェーズ」に変更
    ChangeCurrentPhase(PhaseType::kLevelTransition);
    break;
  }
  case PhaseType::kLevelTransition: {
    //------------------------------
    //レベル遷移フェーズ
    //------------------------------
     // フェードアウト処理完了後、現在のフェーズを「終了処理」に変更
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //------------------------------
    //終了処理フェーズ
    //------------------------------
    // 指定のレベルに切り替える
    ChangeLevel(transition_level_);
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }

  return false;
}

/// <summary>
/// 毎フレームの描画処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ResultLevel::RenderPhase() {

}

/// <summary>
/// 終了処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool ResultLevel::FinishPhase(float process_time) {

  switch (finalize_phase_) {
  case FinalizePhaseType::kUnloadTask: {
    //-------------------------------
    //タスクを降ろすフェーズ
    //-------------------------------
    // フォント情報生成
    FontInfo* font_info = FontInfo::GetFontInfoInstance();
    if (font_info == nullptr) {
      return false;
    }
    // フォント破棄
    font_info->ReleaseFontInfo(kFontFilePath);

    // サウンドの破棄
    if (sound_manager_ != nullptr) {
      sound_manager_->DisposeSound();
    }

    // 画像の破棄
    DeleteGraph(back_ground_->GetGraphicHandle());   // 背景

    TaskId task_id;
    Task* release_task = nullptr;

    // 背景処理をタスクマネージャから降ろす
    if (back_ground_ != nullptr) {
      task_id = back_ground_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // スコアボードをタスクマネージャから降ろす
    if (score_board_ != nullptr) {
      task_id = score_board_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // UI情報をタスクマネージャから降ろす
    if (info_ui_ != nullptr) {
      task_id = info_ui_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // メニューコントローラをタスクマネージャから降ろす
    if (menu_controller_ != nullptr) {
      task_id = menu_controller_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // サウンドマネージャをタスクマネージャから降ろす
    if (sound_manager_ != nullptr) {
      task_id = sound_manager_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }

    // 終了フェーズをタスク破棄フェーズに変更
    ChangeFinalizePhase(FinalizePhaseType::kDisposeTask);
    return false;
  }
  case FinalizePhaseType::kDisposeTask: {
    //-------------------------------
    //タスクを破棄するフェーズ
    //-------------------------------
    if (sound_manager_ != nullptr) {
      delete sound_manager_;
      sound_manager_ = nullptr;
    }
    if (menu_controller_ != nullptr) {
      delete menu_controller_;
      menu_controller_ = nullptr;
    }
    if (info_ui_ != nullptr) {
      delete info_ui_;
      info_ui_ = nullptr;
    }
    if (score_board_ != nullptr) {
      delete score_board_;
      score_board_ = nullptr;
    }
    if (back_ground_ != nullptr) {
      delete back_ground_;
      back_ground_ = nullptr;
    }
    break;
  }
  }

  return true;
}

/// <summary>
/// 背景 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool ResultLevel::InitializeBackGround() {

  // 画像のロード
  int graphic_handle = LoadGraph(kImageBackGroundPath);
  if (graphic_handle == kLoadError) {
    return false;
  }
  // バックグラウンド 生成
  back_ground_ = new ResultBackGround();
  if (back_ground_ == nullptr) {
    std::cout << "BattleLevel InitializeBackGround：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // グラフィックハンドルを渡す
  back_ground_->SetGraphicHandle(graphic_handle);
  // タスクマネージャに積む
  task_manager_.AddTask(back_ground_);

  return true;
}

/// <summary>
/// スコアボード 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool ResultLevel::InitializeScoreBoard() {

  // スコアボード 生成
  score_board_ = new ScoreBoard();
  if (score_board_ == nullptr) {
    std::cout << "BattleLevel InitializeScoreBoard：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // タスクマネージャに積む
  task_manager_.AddTask(score_board_);

  // クリアタイムを取得する
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return true;
  }
  int clear_time = game_info->GetClearTime();
  // クリアタイムをセットする
  score_board_->SetTime(ScoreBoard::TimeType::kResultTime, clear_time);

  return true;
}

/// <summary>
/// UI情報 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool ResultLevel::InitializeResultInfoUi() {

  // UI情報 生成
  info_ui_ = new ResultInfoUi();
  if (info_ui_ == nullptr) {
    std::cout << "BattleLevel InitializeResultInfoUi：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // タスクマネージャに積む
  task_manager_.AddTask(info_ui_);

  return true;
}

/// <summary>
/// メニューコントローラ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool ResultLevel::InitializeMenuController() {

  // メニューコントローラ 生成
  menu_controller_ = new MenuController(*this);
  if (menu_controller_ == nullptr) {
    std::cout << "BattleLevel InitializeMenuController：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // タスクマネージャに積む
  task_manager_.AddTask(menu_controller_);

  return true;
}

/// <summary>
/// サウンドマネージャ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool ResultLevel::InitializeSoundManager() {

  // サウンドマネージャ 生成
  sound_manager_ = new SoundManager();
  if (sound_manager_ == nullptr) {
    std::cout << "BattleLevel InitializeSoundManager：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // タスクマネージャに積む
  task_manager_.AddTask(sound_manager_);

  return true;
}

/// <summary>
/// 準備処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool ResultLevel::Prepare() {

  switch (sound_phase_) {
  case RegisterSoundPhase::kButtonSelect: {
    //---------------------------------------------
    //ボタン選択音登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundButtonSelectPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return false;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kButtonSelect, Sound::SoundType::kSoundEffect);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kSoundEffect]);
      // サウンドハンドル、総再生時間、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「ボタン押下の登録」に変更
    ChangeSoundPhase(RegisterSoundPhase::kButtonPush);
    break;
  }
  case RegisterSoundPhase::kButtonPush: {
    //---------------------------------------------
    //ボタン押下音登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundButtonPushPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return false;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kButtonPush, Sound::SoundType::kSoundEffect);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kSoundEffect]);
      // サウンドハンドル、総再生時間、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「BGM登録」に変更
    ChangeSoundPhase(RegisterSoundPhase::kBgm);
    break;
  }
  case RegisterSoundPhase::kBgm: {
    //---------------------------------------------
    //バトルBGM登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundResultBgmPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return false;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kResultBgm, Sound::SoundType::kBackGroundMusic);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kBackGroundMusic]);
      // サウンドハンドル、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「終了処理」に変更
    ChangeSoundPhase(RegisterSoundPhase::kFinish);
    break;
  }
  case  RegisterSoundPhase::kFinish: {
    //---------------------------------------------
    //終了処理
    //---------------------------------------------
    return true;
  }
  }

  return false;
}

/// <summary>
/// 透過率変更処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool ResultLevel::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率の調整
  switch (current_phase_) {
  case PhaseType::kStartUp: {
    // 起動フェーズ
    alpha_ += alpha_adjust_;
    break;
  }
  case PhaseType::kLevelTransition: {
    // 終了処理前フェーズ
    alpha_ -= alpha_adjust_;
    break;
  }
  }

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= kAlphaMax) {
    alpha_ = kAlphaMax;
    is_finish = true;
  }
  else if (alpha_ <= kAlphaMin) {
    alpha_ = kAlphaMin;
    is_finish = true;
  }

  return is_finish;
}

/// <summary>
/// Enterキーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ResultLevel::OnPushMenuControllerEnterKey() {

  // 決定操作
  DecideOperation();
  // 決定サウンド
  sound_manager_->CreateSound(SoundId::kButtonPush);
}

/// <summary>
/// Zキーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ResultLevel::OnPushMenuControllerZ() {

  // 決定操作
  DecideOperation();
  // 決定サウンド
  sound_manager_->CreateSound(SoundId::kButtonPush);
}

/// <summary>
/// 右矢印キーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ResultLevel::OnPushMenuControllerRight() {

  // 選択操作
  info_ui_->OperationCursorLeftRight();
  // 決定サウンド
  sound_manager_->CreateSound(SoundId::kButtonSelect);
}

/// <summary>
/// 左矢印キーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ResultLevel::OnPushMenuControllerLeft() {

  // 選択操作
  info_ui_->OperationCursorLeftRight();
  // 決定サウンド
  sound_manager_->CreateSound(SoundId::kButtonSelect);
}

/// <summary>
/// 決定操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ResultLevel::DecideOperation() {

  // ボタンの種類を取得
  ResultInfoUi::ButtonType button_type = info_ui_->GetButtonType();
  switch (button_type) {
  case ResultInfoUi::ButtonType::kRetry: {
    //------------------------------
    //リトライ
    //------------------------------
    transition_level_ = TaskId::kBattleLevel;
    // 現在のフェーズを「終了処理前」に変更
    ChangeCurrentPhase(PhaseType::kBeforeTransition);
    break;
  }
  case ResultInfoUi::ButtonType::kReturnTitle: {
    //------------------------------
    //タイトルへ戻る
    //------------------------------
    transition_level_ = TaskId::kTitleLevel;
    // 現在のフェーズを「終了処理前」に変更
    ChangeCurrentPhase(PhaseType::kBeforeTransition);
    break;
  }
  }
}

/// <summary>
/// 設定ファイル読み込み
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool ResultLevel::LoadSetupFile() {

  //-----------------------------------------
  //サウンドファイル
  //-----------------------------------------
  std::string file_path = kSoundDataFilePath;
  // ファイルをロードし、データを取得する
  bool is_finish = LoadFileSoundData(file_path);
  if (!is_finish) {
    return false;
  }

  //-----------------------------------------
  //スコアファイル
  //-----------------------------------------
  file_path = kScoreDataFilePath;
  // ファイルをロードし、データを取得する
  is_finish = LoadFileScoreData(file_path);
  if (!is_finish) {
    return false;
  }

  return true;
}

/// <summary>
/// ロードファイル サウンドデータ
/// </summary>
/// <param name="file_path"> ファイルパス </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool ResultLevel::LoadFileSoundData(std::string file_path) {

  // ファイルストリームを宣言と同時にファイルを開く
  std::ifstream stream(file_path, std::ios::in);
  //ファイルが開けないならエラー
  if (!stream) {
    // リストに値をセット
    sound_volume_list_[Sound::SoundType::kBackGroundMusic] = kSoundInitialVolume;
    sound_volume_list_[Sound::SoundType::kSoundEffect] = kSoundInitialVolume;
    // ファイルストリームを宣言と同時にファイルを開く
    std::ofstream stream(file_path, std::ios::out);
    std::string text = "BGMSoundVolume=" + std::to_string(kSoundInitialVolume);
    stream << text << std::endl;
    text = "SESoundVolume=" + std::to_string(kSoundInitialVolume);
    stream << text << std::endl;
    //ファイルを閉じる
    stream.close();
    return true;
  }

  //ファイルから読み込む
  std::string line;
  //１行ずつ読み出す
  while (std::getline(stream, line)) {
    std::cout << line << std::endl;
    int equal_pos = 0;
    if (line.find("BGMSoundVolume=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      sound_volume_list_[Sound::SoundType::kBackGroundMusic] = std::stof(str_value);
    }
    else if (line.find("SESoundVolume=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      sound_volume_list_[Sound::SoundType::kSoundEffect] = std::stof(str_value);
    }
  }

  //ファイルを閉じる
  stream.close();
  line.clear();
  stream.clear();

  return true;
}

/// <summary>
/// ロードファイル スコアデータ
/// </summary>
/// <param name="file_path"> ファイルパス </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool ResultLevel::LoadFileScoreData(std::string file_path) {

  // ファイルストリームを宣言と同時にファイルを開く
  std::ifstream stream(file_path, std::ios::in);
  //ファイルが開けないならエラー
  if (!stream) {
    // ファイルストリームを宣言と同時にファイルを開く
    std::ofstream stream(file_path, std::ios::out);
    std::string text = "First=" + std::to_string(kFirstScoreInit);
    stream << text << std::endl;
    text = "Second=" + std::to_string(kSecondScoreInit);
    stream << text << std::endl;
    text = "Third=" + std::to_string(kThirdScoreInit);
    stream << text << std::endl;

    // 設定値セット
    score_board_->SetTime(ScoreBoard::TimeType::kRankingFirst, kFirstScoreInit);
    score_board_->SetTime(ScoreBoard::TimeType::kRankingSecond, kSecondScoreInit);
    score_board_->SetTime(ScoreBoard::TimeType::kRankingThird, kThirdScoreInit);

    //ファイルを閉じる
    stream.close();
    return true;
  }

  //ファイルから読み込む
  std::string line;
  //１行ずつ読み出す
  while (std::getline(stream, line)) {
    std::cout << line << std::endl;
    int equal_pos = 0;
    if (line.find("First=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      score_board_->SetTime(ScoreBoard::TimeType::kRankingFirst, std::stoi(str_value));
    }
    else if (line.find("Second=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      score_board_->SetTime(ScoreBoard::TimeType::kRankingSecond, std::stoi(str_value));
    }
    else if (line.find("Third=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      score_board_->SetTime(ScoreBoard::TimeType::kRankingThird, std::stoi(str_value));
    }
  }

  //ファイルを閉じる
  stream.close();
  line.clear();
  stream.clear();

  return true;
}