﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "Game/Field.h"
#include <vector>
#include <array>

/// <summary>
/// フィールドマネージャ
/// </summary>
class FieldManager : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,      // 初期化フェーズ
    kPrepare,         // 準備フェーズ
    kProcess,         // 処理中フェーズ
    kProcessWait,     // 処理待機フェーズ
    kStop,            // 停止中フェーズ
    kFinalize,        // 終了処理フェーズ
    kFinalized,       // 終了処理済みフェーズ
    kPhaseMaxIndex    // フェーズ数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  FieldManager();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~FieldManager();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// フィールド処理のフェーズを変更する
  /// </summary>
  /// <param name=""> フィールドのフェーズの種類 </param>
  /// <returns></returns>
  void ChangeFieldPhase(Field::PhaseType);

  /// <summary>
  /// フィールドの破棄
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void FieldDispose();

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; };


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool Initialize();

  /// <summary>
  /// 準備処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool Prepare();

  /// <summary>
  /// フィールド処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void UpdateField();


private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 背景リスト
  /// </summary>
  std::vector<std::vector<Field*>> field_list_;
};