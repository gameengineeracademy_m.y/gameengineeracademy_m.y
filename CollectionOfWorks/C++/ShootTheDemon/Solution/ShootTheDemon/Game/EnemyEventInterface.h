﻿#pragma once

#include "System/SoundId.h"
#include "System/Character.h"
#include "System/CutInManager.h"
#include "Game/Barrage.h"
#include <iostream>

/// <summary>
/// エネミーイベントインターフェース
/// </summary>
class EnemyEventInterface {
public:

  /// <summary>
  /// コンストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  EnemyEventInterface() {

    // コンソールに出力
    std::cout << "EnemyEventInterface コンストラクタ " << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~EnemyEventInterface() {

    // コンソールに出力
    std::cout << "~EnemyEventInterface デストラクタ " << std::endl;
  }

  /// <summary>
  /// BGM終了通知
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnFinishSoundBGM() = 0;

  /// <summary>
  /// キャラクター終了通知
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  virtual void OnFinish(Character*) = 0;

  /// <summary>
  /// HPバー生成処理
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  virtual void OnCreateHpBar(Character*) = 0;

  /// <summary>
  /// HPバー破棄処理
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  virtual void OnDisposeHpBar(Character*) = 0;

  /// <summary>
  /// 本体の当たり判定
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  virtual void OnCheckCollisionEnemy(Character*) = 0;

  /// <summary>
  /// 弾幕表示開始
  /// </summary>
  /// <param name=""> バレッジID </param>
  /// <returns></returns>
  virtual Barrage* OnStartBarrage(BarrageId) = 0;

  /// <summary>
  /// 弾幕表示終了
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnFinishBarrage() = 0;

  /// <summary>
  /// プレイヤーの座標を取得
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  virtual void OnGetPlayerPosition(float&, float&) = 0;

  /// <summary>
  /// アイテム生成
  /// </summary>
  /// <param name=""> キャラクターの種類 </param>
  /// <returns></returns>
  virtual void OnCreateItem(Character::CharacterType) = 0;

  /// <summary>
  /// カットイン処理
  /// </summary>
  /// <param name=""> フェーズ数 </param>
  /// <param name=""> カットインの色 </param>
  /// <returns></returns>
  virtual void OnDisplayCutIn(int, CutInManager::ColorType) = 0;

  /// <summary>
  /// バトルフェーズの変更
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  virtual void OnChangeBattlePhase(Character*) = 0;

  /// <summary>
  /// 爆破用エフェクトを実行
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  virtual void OnExecuteEnemyEffect(Character*) = 0;

  /// <summary>
  /// バトルレベルのフェーズ確認
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:プレイ中, false:それ以外 </returns>
  virtual bool OnCheckBattleLevelPhase() = 0;

  /// <summary>
  /// プレイヤーが死亡しているかどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:死亡, false:生存 </returns>
  virtual bool OnCheckPlayerDead() = 0;

  /// <summary>
  /// ラップタイムセット
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <param name=""> フェーズ数 </param>
  /// <returns></returns>
  virtual void OnSetRapTime(Character*, int) = 0;
};