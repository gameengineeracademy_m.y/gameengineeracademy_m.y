﻿#pragma once

#include <iostream>

/// <summary>
/// ゲームオーバー処理イベントインターフェース
/// </summary>
class FinishUiGameOverEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  FinishUiGameOverEventInterface() {

    // コンソールに出力
    std::cout << "FinishUiGameOverEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~FinishUiGameOverEventInterface() {

    // コンソールに出力
    std::cout << "~FinishUiGameOverEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// SE生成
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPlayGameOverSound() = 0;

  /// <summary>
  /// リトライする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnTryAgain() = 0;

  /// <summary>
  /// タイトルへ遷移する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnGoToTitle() = 0;


private:

};