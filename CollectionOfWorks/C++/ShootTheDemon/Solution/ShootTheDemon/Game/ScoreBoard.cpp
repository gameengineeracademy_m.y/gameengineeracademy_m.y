﻿#include "Game/ScoreBoard.h"

namespace {

  /// <summary>
  /// スコアデータファイル
  /// </summary>
  const char* kScoreDataFilePath = "File/ScoreData.txt";

  /// <summary>
  /// 10000
  /// </summary>
  const int kTenThousand = 10000;

  /// <summary>
  /// 1000
  /// </summary>
  const float kThousandf = 1000.0f;

  /// <summary>
  /// 1000
  /// </summary>
  const int kThousand = 1000;

  /// <summary>
  /// 100
  /// </summary>
  const int kHundred = 100;

  /// <summary>
  /// 10
  /// </summary>
  const int kTen = 10;

  /// <summary>
  /// 1
  /// </summary>
  const int kOne = 1;

  /// <summary>
  /// 60秒 = 1分 変換
  /// </summary>
  const int kChangeMinutes = 60;

  /// <summary>
  /// 表示時刻最大値
  /// </summary>
  const char* kMaxTimeText = "99:59.999";

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjust = 5;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaBoardMax = 150;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaGuideBoardMax = 200;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaTextMax = 255;

  /// <summary>
  /// タイトルボード表示位置 X座標
  /// </summary>
  const int kTitleBoardPosX = 130;

  /// <summary>
  /// タイトルボード表示位置 Y座標
  /// </summary>
  const int kTitleBoardPosY = 75;

  /// <summary>
  /// タイトルボード幅
  /// </summary>
  const int kTitleBoardWidth = 775;

  /// <summary>
  /// タイトルボード高さ
  /// </summary>
  const int kTitleBoardHeight = 100;

  /// <summary>
  /// リザルトボード表示位置 X座標
  /// </summary>
  const int kResultBoardPosX = 130;

  /// <summary>
  /// リザルトボード表示位置 Y座標
  /// </summary>
  const int kResultBoardPosY = 200;

  /// <summary>
  /// リザルトボード幅
  /// </summary>
  const int kResultBoardWidth = 350;

  /// <summary>
  /// リザルトボード高さ
  /// </summary>
  const int kResultBoardHeight = 350;

  /// <summary>
  /// ランキングボード表示位置 X座標
  /// </summary>
  const int kRankingBoardPosX = 505;

  /// <summary>
  /// ランキングボード表示位置 Y座標
  /// </summary>
  const int kRankingBoardPosY = 200;

  /// <summary>
  /// ランキングボード幅
  /// </summary>
  const int kRankingBoardWidth = 400;

  /// <summary>
  /// ランキングボード高さ
  /// </summary>
  const int kRankingBoardHeight = 350;

  /// <summary>
  /// テキスト表示位置 Y座標
  /// </summary>
  const int kTextPosY = 17;

  /// <summary>
  /// 案内ボード表示位置 X座標
  /// </summary>
  const int kGuideBoardPosY = 565;

  /// <summary>
  /// 案内ボード 幅
  /// </summary>
  const int kGuideBoardWidth = 500;

  /// <summary>
  /// 案内ボード 高さ
  /// </summary>
  const int kGuideBoardHeight = 80;
  
  /// <summary>
  /// ボード 色
  /// </summary>
  const int kBoardColor = GetColor(200, 0, 255);

  /// <summary>
  /// ボード 色
  /// </summary>
  const int kGuideBoardColor = GetColor(255, 255, 255);

  /// <summary>
  /// 文字 色
  /// </summary>
  const int kTitleTextColor = GetColor(255, 255, 0);

  /// <summary>
  /// 文字 色
  /// </summary>
  const int kTimeTextColor = GetColor(255, 255, 255);

  /// <summary>
  /// 文字 色
  /// </summary>
  const int kSpecialTimeTextColor = GetColor(255, 255, 0);

  /// <summary>
  /// 文字 High Score 色
  /// </summary>
  const int kHighScoreTextColor = GetColor(255, 100, 0);

  /// <summary>
  /// ボード 色
  /// </summary>
  const int kGuideTextColor = GetColor(0, 0, 0);

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Reggae One";

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kGuideFontSize = 20;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kClearTimeFontSize = 30;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kHighScoreFontSize = 35;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kRankingTitleTextFontSize = 40;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kTimeFontSize = 50;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kTitleFontSize = 60;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 1;

  /// <summary>
  /// タイトル Clear Time 表示位置
  /// </summary>
  const int kClearTimePosX = 155;

  /// <summary>
  /// タイトル Clear Time 表示位置
  /// </summary>
  const int kClearTimePosY = 300;

  /// <summary>
  /// タイトル HIGH SCORE 表示位置
  /// </summary>
  const int kHighScorePosY = 225;

  /// <summary>
  /// タイトル Ranking 表示位置
  /// </summary>
  const int kRankingTextPosY = 40;

  /// <summary>
  /// タイトル クリアタイム 表示位置
  /// </summary>
  const int kClearTimeTextPosY = 395;

  /// <summary>
  /// タイトル ランキング1位タイム 表示位置
  /// </summary>
  const int kRankingFirstTextPosY = 315;

  /// <summary>
  /// タイトル ランキング2位タイム 表示位置
  /// </summary>
  const int kRankingSecondTextPosY = 385;

  /// <summary>
  /// タイトル ランキング3位タイム 表示位置
  /// </summary>
  const int kRankingThirdTextPosY = 455;

  /// <summary>
  /// タイトル 案内文 表示位置 調整値
  /// </summary>
  const int kGuideTextPosX = 15;

  /// <summary>
  /// タイトル Ranking 表示位置 調整値
  /// </summary>
  const int kGuideTextPosY = 15;

  /// <summary>
  /// タイトル Ranking 表示位置 
  /// </summary>
  const int kGuideTextAdjustPosY = 30;

  /// <summary>
  /// タイトル
  /// </summary>
  const char* kTitle01Text = "GAME CLEAR !";

  /// <summary>
  /// タイトル
  /// </summary>
  const char* kTitle02Text = "CONGRATULATION!";

  /// <summary>
  /// タイトル Clear Time
  /// </summary>
  const char* kClearTimeText = "CLEAR TIME";

  /// <summary>
  /// タイトル HIGH SCORE
  /// </summary>
  const char* kHighScoreText = "HIGH SCORE";

  /// <summary>
  /// タイトル RANKING
  /// </summary>
  const char* kRankingText = "RANKING BOARD";

  /// <summary>
  /// タイトル RANKING 1st
  /// </summary>
  const char* kRankingFirstText = "1st : ";

  /// <summary>
  /// タイトル RANKING 2nd
  /// </summary>
  const char* kRankingSecondText = "2nd : ";

  /// <summary>
  /// タイトル RANKING 3rd
  /// </summary>
  const char* kRankingThirdText = "3rd : ";

  /// <summary>
  /// 案内文
  /// </summary>
  const char* kGuidance01Text = "Button Select : LEFT or RIGHT ARROW KEY";

  /// <summary>
  /// 案内文
  /// </summary>
  const char* kGuidance02Text = "Decision : ENTER or Z KEY";

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 累積時間のリセット値
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 演出の待機時間
  /// </summary>
  const float kProduceWaitTime = 0.8f;

  /// <summary>
  /// 描画の待機時間
  /// </summary>
  const float kWaitTime = 0.8f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ScoreBoard::ScoreBoard()
  : Task(TaskId::kScoreBoard)
  , current_phase_(PhaseType::kNone)
  , prepare_phase_(PreparePhase::kSetRanking)
  , process_phase_(ProcessPhase::kWait)
  , time_()
  , time_pos_()
  , time_forecolor_()
  , title_board_()
  , result_board_()
  , ranking_board_()
  , guide_board_()
  , text_title_()
  , text_clear_()
  , text_high_score_()
  , text_ranking_()
  , text_ranking_first_()
  , text_ranking_second_()
  , text_ranking_third_()
  , text_guide_01_()
  , text_guide_02_()
  , title_()
  , alpha_board_(0)
  , alpha_text_title_(0)
  , alpha_text_result_(0)
  , alpha_text_ranking_(0)
  , alpha_high_score_(0)
  , alpha_guide_(0)
  , alpha_text_guide_(0)
  , font_handle_()
  , is_high_score_(false)
  , is_first_(false)
  , accumulate_time_(0.0f) {

  // コンソールに出力
  std::cout << "ScoreBoard コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ScoreBoard::~ScoreBoard() {

  // コンソールに出力
  std::cout << "~ScoreBoard デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void ScoreBoard::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kNone: {
    //-------------------------------
    //何もしないフェーズ
    //-------------------------------
    break;
  }
  case PhaseType::kInitialize: {
    //-------------------------------
    //初期化処理フェーズ
    //-------------------------------
    bool is_finish = Initialize();
    if (is_finish) {
      // 現在のフェーズを「起動待機」に変更
      ChangeCurrentPhase(PhaseType::kStartWait);
    }
    break;
  }
  case PhaseType::kStartWait: {
    //-------------------------------
    //起動待機フェーズ
    //-------------------------------
    break;
  }
  case PhaseType::kStartUp: {
    //-------------------------------
    //起動フェーズ
    //-------------------------------
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //-------------------------------
    //処理中フェーズ
    //-------------------------------
    Process(process_time);
    break;
  }
  case PhaseType::kBeforeFinalize: {
    //-------------------------------
    //終了処理前フェーズ
    //-------------------------------
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //-------------------------------
    //終了処理フェーズ
    //-------------------------------
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ScoreBoard::Render() {

  switch (current_phase_) {
  case PhaseType::kStartUp:
  case PhaseType::kProcess:
  case PhaseType::kBeforeFinalize: {

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_board_);

    DrawBox(title_board_.left.x, title_board_.left.y,
            title_board_.right.x, title_board_.right.y, kBoardColor, true);
    DrawBox(result_board_.left.x, result_board_.left.y,
            result_board_.right.x, result_board_.right.y, kBoardColor, true);
    DrawBox(ranking_board_.left.x, ranking_board_.left.y,
            ranking_board_.right.x, ranking_board_.right.y, kBoardColor, true);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_text_title_);

    DrawStringToHandle(text_title_.x, text_title_.y, title_.c_str(),
                       kTitleTextColor, font_handle_[static_cast<int>(FontSize::k60)]);
    DrawStringToHandle(text_clear_.x, text_clear_.y, kClearTimeText,
                       kTitleTextColor, font_handle_[static_cast<int>(FontSize::k40)]);
    DrawStringToHandle(text_ranking_.x, text_ranking_.y, kRankingText,
                       kTitleTextColor, font_handle_[static_cast<int>(FontSize::k35)]);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_text_result_);

    DrawStringToHandle(time_pos_[TimeType::kResultTime].x, time_pos_[TimeType::kResultTime].y,
                       time_text_[TimeType::kResultTime].c_str(), time_forecolor_[TimeType::kResultTime],
                       font_handle_[static_cast<int>(FontSize::k50)]);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_text_ranking_);

    DrawStringToHandle(time_pos_[TimeType::kRankingFirst].x, time_pos_[TimeType::kRankingFirst].y,
                       time_text_[TimeType::kRankingFirst].c_str(), time_forecolor_[TimeType::kRankingFirst],
                       font_handle_[static_cast<int>(FontSize::k40)]);

    DrawStringToHandle(time_pos_[TimeType::kRankingSecond].x, time_pos_[TimeType::kRankingSecond].y,
                       time_text_[TimeType::kRankingSecond].c_str(), time_forecolor_[TimeType::kRankingSecond],
                       font_handle_[static_cast<int>(FontSize::k40)]);

    DrawStringToHandle(time_pos_[TimeType::kRankingThird].x, time_pos_[TimeType::kRankingThird].y,
                       time_text_[TimeType::kRankingThird].c_str(), time_forecolor_[TimeType::kRankingThird],
                       font_handle_[static_cast<int>(FontSize::k40)]);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);


    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_guide_);

    DrawBox(guide_board_.left.x, guide_board_.left.y,
            guide_board_.right.x, guide_board_.right.y, kGuideBoardColor, true);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);


    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_text_guide_);

    DrawStringToHandle(text_guide_01_.x, text_guide_01_.y, kGuidance01Text, kGuideTextColor,
                       font_handle_[static_cast<int>(FontSize::k20)]);
    DrawStringToHandle(text_guide_02_.x, text_guide_02_.y, kGuidance02Text, kGuideTextColor,
                       font_handle_[static_cast<int>(FontSize::k20)]);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);


    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_high_score_);

    DrawStringToHandle(text_high_score_.x, text_high_score_.y, kHighScoreText,
                       kHighScoreTextColor, font_handle_[static_cast<int>(FontSize::k40)]);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    break;
  }
  }
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param names=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool ScoreBoard::Initialize() {

  switch (prepare_phase_) {
  case PreparePhase::kSetRanking: {
    //-------------------------------------------
    // 順位付けフェーズ
    //-------------------------------------------
    // ハイスコアフラグを降ろしておく
    is_high_score_ = false;
    // 文字色を全て白色に設定
    time_forecolor_[TimeType::kResultTime] = kTimeTextColor;
    time_forecolor_[TimeType::kRankingFirst] = kTimeTextColor;
    time_forecolor_[TimeType::kRankingSecond] = kTimeTextColor;
    time_forecolor_[TimeType::kRankingThird] = kTimeTextColor;

    // クリアタイムとランキングのタイムを比較して順位を確定
    int first_time = time_[TimeType::kRankingFirst];
    int second_time = time_[TimeType::kRankingSecond];
    int third_time = time_[TimeType::kRankingThird];
    int clear_time = time_[TimeType::kResultTime];

    if (clear_time < time_[TimeType::kRankingFirst]) {
      third_time = second_time;
      second_time = first_time;
      first_time = clear_time;
      // ハイスコアフラグを立てる
      is_high_score_ = true;
      // クリアタイムの文字色を黄色にセット
      time_forecolor_[TimeType::kResultTime] = kSpecialTimeTextColor;
      time_forecolor_[TimeType::kRankingFirst] = kSpecialTimeTextColor;
    }
    else if (clear_time < time_[TimeType::kRankingSecond]) {
      third_time = second_time;
      second_time = clear_time;
      time_forecolor_[TimeType::kRankingSecond] = kSpecialTimeTextColor;
    }
    else if (clear_time < time_[TimeType::kRankingThird]) {
      third_time = clear_time;
      time_forecolor_[TimeType::kRankingThird] = kSpecialTimeTextColor;
    }

    // ランキングの再作成
    time_[TimeType::kRankingFirst] = first_time;
    time_[TimeType::kRankingSecond] = second_time;
    time_[TimeType::kRankingThird] = third_time;

    // テキストファイルにデータを書き込む
    UpdateScoreDataFile();

    // 各タイムを描画用に変換する
    for (int i = 0; i < static_cast<int>(TimeType::kTypeMaxIndex); ++i) {

      TimeType time_type = static_cast<TimeType>(i);
      
      // タイマーの時間を「分・秒・ミリ秒」に変換
      int minutes_value = static_cast<int>(time_[time_type] / (kChangeMinutes * kThousand));
      if (minutes_value >= kHundred) {
        time_text_[time_type] = kMaxTimeText;
        continue;
      }
      int minutes_tens = minutes_value / kTen % kTen;
      int minutes_ones = minutes_value / kOne % kTen;

      int seconds_value = time_[time_type] - minutes_value * kChangeMinutes * kThousand;

      int seconds_tens = seconds_value / kTenThousand % kTen;
      int seconds_ones = seconds_value / kThousand % kTen;
      int milliseconds_hundreds = seconds_value / kHundred % kTen;
      int milliseconds_tens = seconds_value / kTen % kTen;
      int milliseconds_ones = seconds_value / kOne % kTen;

      // タイムをテキストでセットする
      time_text_[time_type] = std::to_string(minutes_tens) + std::to_string(minutes_ones) + ":" +
                              std::to_string(seconds_tens) + std::to_string(seconds_ones) + "." +
                              std::to_string(milliseconds_hundreds) +
                              std::to_string(milliseconds_tens) + std::to_string(milliseconds_ones);
    }

    // 準備フェーズを「初期化処理」に変更
    ChangePreparePhase(PreparePhase::kInitialize);
    break;
  }
  case PreparePhase::kInitialize: {
    //-------------------------------
    //初期化処理フェーズ
    //-------------------------------
    // 透過率の設定
    alpha_board_ = kAlphaMin;
    alpha_text_title_ = kAlphaMin;

    // フォント情報生成
    FontInfo* font_info = FontInfo::GetFontInfoInstance();
    if (font_info == nullptr) {
      return false;
    }
    font_handle_[static_cast<int>(FontSize::k20)] = font_info->GetFontInfo(kFontName, kGuideFontSize, kFontWidth, DX_FONTTYPE_NORMAL);
    font_handle_[static_cast<int>(FontSize::k35)] = font_info->GetFontInfo(kFontName, kHighScoreFontSize, kFontWidth, DX_FONTTYPE_NORMAL);
    font_handle_[static_cast<int>(FontSize::k40)] = font_info->GetFontInfo(kFontName, kRankingTitleTextFontSize, kFontWidth, DX_FONTTYPE_NORMAL);
    font_handle_[static_cast<int>(FontSize::k50)] = font_info->GetFontInfo(kFontName, kTimeFontSize, kFontWidth, DX_FONTTYPE_NORMAL);
    font_handle_[static_cast<int>(FontSize::k60)] = font_info->GetFontInfo(kFontName, kTitleFontSize, kFontWidth, DX_FONTTYPE_NORMAL);

    //-------------------------------------------
    // ボード
    //-------------------------------------------
    title_board_.size.width = kTitleBoardWidth;
    title_board_.size.height = kTitleBoardHeight;
    title_board_.left.x = kTitleBoardPosX;
    title_board_.left.y = kTitleBoardPosY;
    title_board_.right.x = title_board_.left.x + title_board_.size.width;
    title_board_.right.y = title_board_.left.y + title_board_.size.height;

    result_board_.size.width = kResultBoardWidth;
    result_board_.size.height = kResultBoardHeight;
    result_board_.left.x = kResultBoardPosX;
    result_board_.left.y = kResultBoardPosY;
    result_board_.right.x = result_board_.left.x + result_board_.size.width;
    result_board_.right.y = result_board_.left.y + result_board_.size.height;

    ranking_board_.size.width = kRankingBoardWidth;
    ranking_board_.size.height = kRankingBoardHeight;
    ranking_board_.left.x = kRankingBoardPosX;
    ranking_board_.left.y = kRankingBoardPosY;
    ranking_board_.right.x = ranking_board_.left.x + ranking_board_.size.width;
    ranking_board_.right.y = ranking_board_.left.y + ranking_board_.size.height;


    //-------------------------------------------
    // 表示テキスト
    //-------------------------------------------
    title_ = kTitle01Text;
    int text_width = GetDrawStringWidthToHandle(title_.c_str(), static_cast<int>(strlen(title_.c_str())), font_handle_[static_cast<int>(FontSize::k60)]);
    text_title_.x = (title_board_.left.x + title_board_.right.x) / kHalfValue - text_width / kHalfValue;
    text_title_.y = title_board_.left.y + kTextPosY;

    text_width = GetDrawStringWidthToHandle(kClearTimeText, static_cast<int>(strlen(kClearTimeText)), font_handle_[static_cast<int>(FontSize::k40)]);
    text_clear_.x = kClearTimePosX;
    text_clear_.y = kClearTimePosY;

    text_width = GetDrawStringWidthToHandle(kHighScoreText, static_cast<int>(strlen(kHighScoreText)), font_handle_[static_cast<int>(FontSize::k40)]);
    text_high_score_.x = (result_board_.left.x + result_board_.right.x) / kHalfValue - text_width / kHalfValue;
    text_high_score_.y = kHighScorePosY;

    text_width = GetDrawStringWidthToHandle(kRankingText, static_cast<int>(strlen(kRankingText)), font_handle_[static_cast<int>(FontSize::k35)]);
    text_ranking_.x = (ranking_board_.left.x + ranking_board_.right.x) / kHalfValue - text_width / kHalfValue;;
    text_ranking_.y = ranking_board_.left.y + kRankingTextPosY;


    //-------------------------------------------
    // タイム
    //-------------------------------------------
    text_width = GetDrawStringWidthToHandle(time_text_[TimeType::kResultTime].c_str(),
                                            static_cast<int>(strlen(time_text_[TimeType::kResultTime].c_str())),
                                            font_handle_[static_cast<int>(FontSize::k50)]);
    time_pos_[TimeType::kResultTime].x = (result_board_.left.x + result_board_.right.x) / kHalfValue - text_width / kHalfValue;
    time_pos_[TimeType::kResultTime].y = kClearTimeTextPosY;

    time_text_[TimeType::kRankingFirst] = kRankingFirstText + time_text_[TimeType::kRankingFirst];
    text_width = GetDrawStringWidthToHandle(time_text_[TimeType::kRankingFirst].c_str(),
                                            static_cast<int>(strlen(time_text_[TimeType::kRankingFirst].c_str())),
                                            font_handle_[static_cast<int>(FontSize::k40)]);
    time_pos_[TimeType::kRankingFirst].x = (ranking_board_.left.x + ranking_board_.right.x) / kHalfValue - text_width / kHalfValue;
    time_pos_[TimeType::kRankingFirst].y = kRankingFirstTextPosY;

    time_text_[TimeType::kRankingSecond] = kRankingSecondText + time_text_[TimeType::kRankingSecond];
    text_width = GetDrawStringWidthToHandle(time_text_[TimeType::kRankingSecond].c_str(),
                                            static_cast<int>(strlen(time_text_[TimeType::kRankingSecond].c_str())),
                                            font_handle_[static_cast<int>(FontSize::k40)]);
    time_pos_[TimeType::kRankingSecond].x = (ranking_board_.left.x + ranking_board_.right.x) / kHalfValue - text_width / kHalfValue;
    time_pos_[TimeType::kRankingSecond].y = kRankingSecondTextPosY;

    time_text_[TimeType::kRankingThird] = kRankingThirdText + time_text_[TimeType::kRankingThird];
    text_width = GetDrawStringWidthToHandle(time_text_[TimeType::kRankingThird].c_str(),
                                            static_cast<int>(strlen(time_text_[TimeType::kRankingThird].c_str())),
                                            font_handle_[static_cast<int>(FontSize::k40)]);
    time_pos_[TimeType::kRankingThird].x = (ranking_board_.left.x + ranking_board_.right.x) / kHalfValue - text_width / kHalfValue;
    time_pos_[TimeType::kRankingThird].y = kRankingThirdTextPosY;


    //-------------------------------------------
    // ガイド
    //-------------------------------------------
    GameInfo* game_info = GameInfo::GetGameInfoInstance();
    if (game_info == nullptr) {
      return false;
    }
    int x_pos_center = game_info->GetCenterPointX();
    
    guide_board_.size.width = kGuideBoardWidth;
    guide_board_.size.height = kGuideBoardHeight;
    guide_board_.left.x = x_pos_center - kGuideBoardWidth / kHalfValue;
    guide_board_.left.y = kGuideBoardPosY;
    guide_board_.right.x = guide_board_.left.x + guide_board_.size.width;
    guide_board_.right.y = guide_board_.left.y + guide_board_.size.height;

    text_guide_01_.x = guide_board_.left.x + kGuideTextPosX;
    text_guide_01_.y = guide_board_.left.y + kGuideTextPosY;
    text_guide_02_.x = text_guide_01_.x;
    text_guide_02_.y = text_guide_01_.y + kGuideTextAdjustPosY;

    // 初回フラグをONにする
    is_first_ = true;

    // 準備フェーズを「初期化処理済み」に変更
    ChangePreparePhase(PreparePhase::kInitialized);

    return true;
  }
  }

  return false;
}

/// <summary>
/// 処理中
/// </summary>
/// <param names="process_time"> 処理時間 </param>
/// <returns></returns>
void ScoreBoard::Process(float process_time) {

  switch (process_phase_) {
  case ProcessPhase::kWait: {
    //----------------------------------
    //待機フェーズ
    //----------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kProduceWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // 処理中フェーズを「クリアタイム表示」に変更
      ChangeProcessPhase(ProcessPhase::kClearTime);
    }    
    break;
  }
  case ProcessPhase::kClearTime: {
    //----------------------------------
    //クリアタイム表示フェーズ
    //----------------------------------
    bool is_finish = AdjustAlphaValueClearTime();
    if (is_finish) {
      // 処理中フェーズを「ランキング表示」に変更
      ChangeProcessPhase(ProcessPhase::kRanking);
    }
    break;
  }
  case ProcessPhase::kRanking: {
    //----------------------------------
    //ランキング表示フェーズ
    //----------------------------------
    bool is_finish = AdjustAlphaValueRanking();
    if (is_finish) {

      // ハイスコアの場合はタイトルの記述を変更する
      if (is_high_score_) {
        title_ = kTitle02Text;
        int text_width = GetDrawStringWidthToHandle(title_.c_str(), static_cast<int>(strlen(title_.c_str())), font_handle_[static_cast<int>(FontSize::k60)]);
        text_title_.x = (title_board_.left.x + title_board_.right.x) / kHalfValue - text_width / kHalfValue;
        text_title_.y = title_board_.left.y + kTextPosY;
      }

      // 処理中フェーズを「リザルト表示中」に変更
      ChangeProcessPhase(ProcessPhase::kResult);
    }
    break;
  }
  case ProcessPhase::kResult: {
    //----------------------------------
    //リザルト表示中フェーズ
    //----------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kWaitTime) {

      // 累積時間をリセット
      accumulate_time_ = kResetTime;

      // クリアタイムの表示を点滅させる処理
      if (alpha_text_result_ == kAlphaMin) {
        alpha_text_result_ = kAlphaTextMax;
      }
      else {
        alpha_text_result_ = kAlphaMin;
      }


      // ハイスコアでない場合は処理終了
      if (!is_high_score_) {
        return;
      }
      // 初回実行時は処理終了
      // スコアタイムの点滅と合わせるため
      if (is_first_) {
        is_first_ = false;
        return;
      }

      // ハイスコアの表示を点滅させる処理
      if (alpha_high_score_ == kAlphaMin) {
        alpha_high_score_ = kAlphaTextMax;
      }
      else {
        alpha_high_score_ = kAlphaMin;
      }
    }
    break;
  }
  }
}


/// <summary>
/// タイムのセット
/// </summary>
/// <param name="time_type"> タイムの種類 </param>
/// <param name="time"> タイム </param>
/// <returns></returns>
void ScoreBoard::SetTime(TimeType time_type, int time) {

  time_[time_type] = time;
}

/// <summary>
/// スコアデータファイルの更新処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ScoreBoard::UpdateScoreDataFile() {

  int first_time = time_[TimeType::kRankingFirst];
  int second_time = time_[TimeType::kRankingSecond];
  int third_time = time_[TimeType::kRankingThird];

  // ファイルストリームを宣言と同時にファイルを開く
  std::ofstream stream(kScoreDataFilePath, std::ios::out);
  std::string text = "First=" + std::to_string(first_time);
  stream << text << std::endl;
  text = "Second=" + std::to_string(second_time);
  stream << text << std::endl;
  text = "Third=" + std::to_string(third_time);
  stream << text << std::endl;

  //ファイルを閉じる
  stream.close();
}


/// <summary>
/// 透過率の調整
/// </summary>
/// <param names=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool ScoreBoard::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率に調整値を加算
  switch (current_phase_) {
  case PhaseType::kStartUp: {
    // 起動フェーズ
    alpha_board_ += kAlphaAdjust;
    alpha_text_title_ += kAlphaAdjust;
    break;
  }
  case PhaseType::kBeforeFinalize: {
    // 終了処理前フェーズ
    alpha_board_ -= kAlphaAdjust;
    alpha_text_title_ -= kAlphaAdjust;
    alpha_text_result_ -= kAlphaAdjust;
    alpha_text_ranking_ -= kAlphaAdjust;
    alpha_high_score_ -= kAlphaAdjust;
    alpha_guide_ -= kAlphaAdjust;
    alpha_text_guide_ -= kAlphaAdjust;
    break;
  }
  }

  //-------------------------------------------
  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  //-------------------------------------------
  // ボードの透過率
  if (alpha_board_ >= kAlphaBoardMax) {
    alpha_board_ = kAlphaBoardMax;
  }
  else if (alpha_board_ <= kAlphaMin) {
    alpha_board_ = kAlphaMin;
  }
  // リザルトの透過率
  if (alpha_text_result_ <= kAlphaMin) {
    alpha_text_result_ = kAlphaMin;
  }
  // ハイスコアの透過率
  if (alpha_high_score_ <= kAlphaMin) {
    alpha_high_score_ = kAlphaMin;
  }
  // 案内ボードの透過率
  if (alpha_guide_ <= kAlphaMin) {
    alpha_guide_ = kAlphaMin;
  }
  // 案内文の透過率
  if (alpha_text_guide_ <= kAlphaMin) {
    alpha_text_guide_ = kAlphaMin;
  }
  // タイトルの透過率
  if (alpha_text_title_ >= kAlphaTextMax) {
    alpha_text_title_ = kAlphaTextMax;
    is_finish = true;
  }
  else if (alpha_text_title_ <= kAlphaMin) {
    alpha_text_title_ = kAlphaMin;
    is_finish = true;
  }

  return is_finish;
}

/// <summary>
/// 透過率の調整
/// </summary>
/// <param names=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool ScoreBoard::AdjustAlphaValueClearTime() {


  alpha_text_result_ += kAlphaAdjust;

  //-------------------------------------------
  // 透過率が最大値よりも大きくならないよう調整
  //-------------------------------------------
  // ボードの透過率
  if (alpha_text_result_ >= kAlphaTextMax) {
    alpha_text_result_ = kAlphaTextMax;
    return true;
  }

  return false;
}

/// <summary>
/// 透過率の調整
/// </summary>
/// <param names=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool ScoreBoard::AdjustAlphaValueRanking() {

  alpha_text_ranking_ += kAlphaAdjust;

  //-------------------------------------------
// 透過率が最大値よりも大きくならないよう調整
//-------------------------------------------
// ボードの透過率
  if (alpha_text_ranking_ >= kAlphaTextMax) {
    alpha_text_ranking_ = kAlphaTextMax;

    // 案内表示の透過率調整値を最大にしておく
    alpha_guide_ = kAlphaGuideBoardMax;
    alpha_text_guide_ = kAlphaTextMax;
    return true;
  }

  return false;
}