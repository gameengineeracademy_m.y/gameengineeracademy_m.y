﻿#pragma once

/// <summary>
/// ヒットポイントバーID
/// </summary>
enum class HitPointBarId {
  kBoss,                // ボス
  kEnemy,               // 敵(雑魚)
  kTypeMaxIndex         // 種類数
};