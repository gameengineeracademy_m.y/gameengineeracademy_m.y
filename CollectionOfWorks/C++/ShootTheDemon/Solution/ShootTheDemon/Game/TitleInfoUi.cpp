﻿#include "Game/TitleInfoUi.h"

namespace {

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjust = 5;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 透過率 ボード 最大値
  /// </summary>
  const int kAlphaBoardMax = 150;

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Reggae One";

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSize25 = 25;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSize54 = 54;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 1;

  /// <summary>
  /// ボタンサイズ 幅
  /// </summary>
  const int kButtonWidth = 220;

  /// <summary>
  /// ボタンサイズ 高さ
  /// </summary>
  const int kButtonHeight = 50;

  /// <summary>
  /// ボタン ゲームスタート
  /// </summary>
  const int kButtonStartPosY = 60;

  /// <summary>
  /// ボタン オプション
  /// </summary>
  const int kButtonOptionPosY = 130;

  /// <summary>
  /// ボタン スコア
  /// </summary>
  const int kButtonScorePosY = 200;

  /// <summary>
  /// ボタン ゲーム終了
  /// </summary>
  const int kButtonEndPosY = 270;

  /// <summary>
  /// ボタンテキスト Y座標 調整値
  /// </summary>
  const int kButtonTextPosY = 12;

  /// <summary>
  /// ゲームタイトル Y座標 調整値
  /// </summary>
  const int kGameTitleTextPosY = 175;

  /// <summary>
  /// ゲームタイトル
  /// </summary>
  const char* kGameTitleText = "SHOOT THE DEMON";

  /// <summary>
  /// ボタンテキスト ゲームスタート
  /// </summary>
  const char* kButtonStartText = "GAME START";

  /// <summary>
  /// ボタンテキスト オプション
  /// </summary>
  const char* kButtonOptionText = "OPTION";

  /// <summary>
  /// ボタンテキスト ランキング
  /// </summary>
  const char* kButtonScoreText = "SCORE";

  /// <summary>
  /// ボタンテキスト ゲーム終了
  /// </summary>
  const char* kButtonEndText = "EXIT";

  /// <summary>
  /// ボタン色 選択中
  /// </summary>
  const int kButtonSelectColor = GetColor(190, 90, 255);

  /// <summary>
  /// ボタン色 選択外
  /// </summary>
  const int kButtonNoneColor = GetColor(130, 0, 170);

  /// <summary>
  /// ボタン テキスト 色
  /// </summary>
  const int kButtonTextColor = GetColor(255, 255, 255);

  /// <summary>
  /// カーソル 色
  /// </summary>
  const int kCursorColor = GetColor(0, 255, 0);

  /// <summary>
  /// カーソル調整値 固定値
  /// </summary>
  const float kCursorAdjust = 1.0f;

  /// <summary>
  /// カーソル調整値 X方向
  /// </summary>
  const float kCursorPlusAdjustX = 1.0f;

  /// <summary>
  /// カーソル調整値 Y方向
  /// </summary>
  const float kCursorPlusAdjustY = 1.0f;

  /// <summary>
  /// カーソル調整値 X方向
  /// </summary>
  const float kCursorMinusAdjustX = -1.0f;

  /// <summary>
  /// カーソル調整値 Y方向
  /// </summary>
  const float kCursorMinusAdjustY = -1.0f;

  /// <summary>
  /// カーソル 幅
  /// </summary>
  const float kCursorThick = 3.0f;

  /// <summary>
  /// 半分
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 待機時間
  /// </summary>
  const float kWaitTime = 0.7f;

  /// <summary>
  /// リセット時間
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 案内文
  /// </summary>
  const char* kGuidance01Text = "Select : LEFT or RIGHT ARROW KEY";

  /// <summary>
  /// 案内文
  /// </summary>
  const char* kGuidance02Text = "Please Push ENTER or Z KEY ";

  /// <summary>
  /// 案内文
  /// </summary>
  const char* kGuidance03Text = "Return : Esc KEY";

  /// <summary>
  /// 案内文 文字色
  /// </summary>
  const int kGuideTextColor = GetColor(255, 255, 255);
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
TitleInfoUi::TitleInfoUi()
  : Task(TaskId::kTitleInfoUi)
  , current_phase_(PhaseType::kNone)
  , button_type_(ButtonType::kGameStart)
  , button_start_()
  , button_option_()
  , button_score_()
  , button_end_()
  , title_()
  , text_button_start_()
  , text_button_option_()
  , text_button_score_()
  , text_button_end_()
  , text_title_()
  , cursor_()
  , cursor_adjust_()
  , alpha_(0)
  , alpha_cursor_(0)
  , accumulate_time_(0.0f)
  , button_color_()
  , font_handle_() {

  // コンソールに出力
  std::cout << "TitleLevelInfoUi コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
TitleInfoUi::~TitleInfoUi() {

  // コンソールに出力
  std::cout << "~TitleLevelInfoUi デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void TitleInfoUi::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kNone: {
    //-------------------------------
    //何もしないフェーズ
    //-------------------------------
    break;
  }
  case PhaseType::kInitialize: {
    //-------------------------------
    //初期化処理フェーズ
    //-------------------------------
    bool is_finish = Initialize();
    if (is_finish) {
      // 現在のフェーズを「起動待機」に変更
      ChangeCurrentPhase(PhaseType::kStartWait);
    }
    break;
  }
  case PhaseType::kStartWait: {
    //-------------------------------
    //起動待機フェーズ
    //-------------------------------
    break;
  }
  case PhaseType::kStartUp: {
    //-------------------------------
    //起動フェーズ
    //-------------------------------
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //-------------------------------
    //処理中フェーズ
    //-------------------------------
    Process(process_time);
    break;
  }
  case PhaseType::kBeforeFinalize: {
    //-------------------------------
    //終了処理前フェーズ
    //-------------------------------
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //-------------------------------
    //終了処理フェーズ
    //-------------------------------
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleInfoUi::Render() {

  switch (current_phase_) {
  case PhaseType::kStartUp:
  case PhaseType::kProcess:
  case PhaseType::kSoundSetting:
  case PhaseType::kScore:
  case PhaseType::kBeforeFinalize: {

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

    //-------------------------------------------
    // タイトル
    //-------------------------------------------
    DrawStringToHandle(text_title_.x, text_title_.y, title_.c_str(), kButtonTextColor, font_handle_[static_cast<int>(FontSize::k54)]);


    //-------------------------------------------
    // ボタン
    //-------------------------------------------
    DrawBox(button_start_.left.x, button_start_.left.y,
            button_start_.right.x, button_start_.right.y,
            button_color_[static_cast<int>(ButtonType::kGameStart)], true);
    DrawBox(button_option_.left.x, button_option_.left.y,
            button_option_.right.x, button_option_.right.y,
            button_color_[static_cast<int>(ButtonType::kOption)], true);
    DrawBox(button_score_.left.x, button_score_.left.y,
            button_score_.right.x, button_score_.right.y,
            button_color_[static_cast<int>(ButtonType::kScore)], true);
    DrawBox(button_end_.left.x, button_end_.left.y,
            button_end_.right.x, button_end_.right.y,
            button_color_[static_cast<int>(ButtonType::kGameEnd)], true);

    //-------------------------------------------
    // ボタンテキスト
    //-------------------------------------------
    DrawStringToHandle(text_button_start_.x, text_button_start_.y, kButtonStartText, kButtonTextColor, font_handle_[static_cast<int>(FontSize::k25)]);
    DrawStringToHandle(text_button_option_.x, text_button_option_.y, kButtonOptionText, kButtonTextColor, font_handle_[static_cast<int>(FontSize::k25)]);
    DrawStringToHandle(text_button_score_.x, text_button_score_.y, kButtonScoreText, kButtonTextColor, font_handle_[static_cast<int>(FontSize::k25)]);
    DrawStringToHandle(text_button_end_.x, text_button_end_.y, kButtonEndText, kButtonTextColor, font_handle_[static_cast<int>(FontSize::k25)]);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_cursor_);

    //-------------------------------------------
    // カーソル
    //-------------------------------------------
    // 左
    DrawLineAA(cursor_.left.x, cursor_.left.y, cursor_.left.x,
               cursor_.right.y, kCursorColor, kCursorThick);
    // 右
    DrawLineAA(cursor_.right.x - kCursorAdjust, cursor_.left.y,
               cursor_.right.x - kCursorAdjust, cursor_.right.y, kCursorColor, kCursorThick);
    // 上
    DrawLineAA(cursor_.left.x - kCursorAdjust, cursor_.left.y,
               cursor_.right.x + kCursorAdjust, cursor_.left.y, kCursorColor, kCursorThick);
    // 下
    DrawLineAA(cursor_.left.x - kCursorAdjust, cursor_.right.y,
               cursor_.right.x + kCursorAdjust, cursor_.right.y, kCursorColor, kCursorThick);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    break;
  }
  }
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param names=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool TitleInfoUi::Initialize() {

  // 透過率の設定
  alpha_ = kAlphaMin;
  alpha_cursor_ = kAlphaMin;

  // 累積時間をリセット
  accumulate_time_ = kResetTime;

  // ゲーム情報から画面の中心位置を取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return false;
  }
  int x_pos_center = game_info->GetCenterPointX();
  int y_pos_center = game_info->GetCenterPointY();

  // フォント情報生成
  FontInfo* font_info = FontInfo::GetFontInfoInstance();
  if (font_info == nullptr) {
    return false;
  }
  font_handle_[static_cast<int>(FontSize::k25)] = font_info->GetFontInfo(kFontName, kFontSize25, kFontWidth, DX_FONTTYPE_NORMAL);
  font_handle_[static_cast<int>(FontSize::k54)] = font_info->GetFontInfo(kFontName, kFontSize54, kFontWidth, DX_FONTTYPE_NORMAL);


  //---------------------------------------------
  // タイトル
  //---------------------------------------------
  title_ = kGameTitleText;
  int text_width = GetDrawStringWidthToHandle(title_.c_str(), static_cast<int>(strlen(title_.c_str())), font_handle_[static_cast<int>(FontSize::k54)]);
  text_title_.x = x_pos_center - (text_width / kHalfValue);
  text_title_.y = y_pos_center - kGameTitleTextPosY;


  //---------------------------------------------
  // ボタン
  //---------------------------------------------
  button_start_.size.width = kButtonWidth;
  button_start_.size.height = kButtonHeight;
  button_start_.left.x = x_pos_center - (button_start_.size.width / kHalfValue);
  button_start_.left.y = y_pos_center + kButtonStartPosY;
  button_start_.right.x = button_start_.left.x + button_start_.size.width;
  button_start_.right.y = button_start_.left.y + button_start_.size.height;

  button_option_.size.width = kButtonWidth;
  button_option_.size.height = kButtonHeight;
  button_option_.left.x = x_pos_center - (button_option_.size.width / kHalfValue);
  button_option_.left.y = y_pos_center + kButtonOptionPosY;
  button_option_.right.x = button_option_.left.x + button_option_.size.width;
  button_option_.right.y = button_option_.left.y + button_option_.size.height;

  button_score_.size.width = kButtonWidth;
  button_score_.size.height = kButtonHeight;
  button_score_.left.x = x_pos_center - (button_score_.size.width / kHalfValue);
  button_score_.left.y = y_pos_center + kButtonScorePosY;
  button_score_.right.x = button_score_.left.x + button_score_.size.width;
  button_score_.right.y = button_score_.left.y + button_score_.size.height;

  button_end_.size.width = kButtonWidth;
  button_end_.size.height = kButtonHeight;
  button_end_.left.x = x_pos_center - (button_end_.size.width / kHalfValue);
  button_end_.left.y = y_pos_center + kButtonEndPosY;
  button_end_.right.x = button_end_.left.x + button_end_.size.width;
  button_end_.right.y = button_end_.left.y + button_end_.size.height;

  button_color_[static_cast<int>(ButtonType::kGameStart)] = kButtonSelectColor;
  button_color_[static_cast<int>(ButtonType::kOption)] = kButtonNoneColor;
  button_color_[static_cast<int>(ButtonType::kScore)] = kButtonNoneColor;
  button_color_[static_cast<int>(ButtonType::kGameEnd)] = kButtonNoneColor;


  //---------------------------------------------
  // ボタンテキスト
  //---------------------------------------------
  text_width = GetDrawStringWidthToHandle(kButtonStartText, static_cast<int>(strlen(kButtonStartText)), font_handle_[static_cast<int>(FontSize::k25)]);
  text_button_start_.x = (button_start_.left.x + button_start_.right.x) / kHalfValue - text_width / kHalfValue;
  text_button_start_.y = button_start_.left.y + kButtonTextPosY;
  text_width = GetDrawStringWidthToHandle(kButtonOptionText, static_cast<int>(strlen(kButtonOptionText)), font_handle_[static_cast<int>(FontSize::k25)]);
  text_button_option_.x = (button_option_.left.x + button_option_.right.x) / kHalfValue - text_width / kHalfValue;
  text_button_option_.y = button_option_.left.y + kButtonTextPosY;
  text_width = GetDrawStringWidthToHandle(kButtonScoreText, static_cast<int>(strlen(kButtonScoreText)), font_handle_[static_cast<int>(FontSize::k25)]);
  text_button_score_.x = (button_score_.left.x + button_score_.right.x) / kHalfValue - text_width / kHalfValue;
  text_button_score_.y = button_score_.left.y + kButtonTextPosY;
  text_width = GetDrawStringWidthToHandle(kButtonEndText, static_cast<int>(strlen(kButtonEndText)), font_handle_[static_cast<int>(FontSize::k25)]);
  text_button_end_.x = (button_end_.left.x + button_end_.right.x) / kHalfValue - text_width / kHalfValue;;
  text_button_end_.y = button_end_.left.y + kButtonTextPosY;


  //---------------------------------------------
  // カーソル
  //---------------------------------------------
  cursor_.left.x = static_cast<float>(button_start_.left.x);
  cursor_.left.y = static_cast<float>(button_start_.left.y);
  cursor_.right.x = static_cast<float>(cursor_.left.x + kButtonWidth);
  cursor_.right.y = static_cast<float>(cursor_.left.y + kButtonHeight);

  cursor_adjust_.x = kCursorMinusAdjustX;
  cursor_adjust_.y = kCursorMinusAdjustY;


  return true;
}

/// <summary>
/// 処理中
/// </summary>
/// <param names=""> 処理時間 </param>
/// <returns> true:処理終了, false:処理中 </returns>
void TitleInfoUi::Process(float process_time) {

  if (current_phase_ == PhaseType::kSoundSetting) {
    return;
  }

  // 累積時間に加算
  accumulate_time_ += process_time;
  if (accumulate_time_ >= kWaitTime) {
    // 累積時間をリセット
    accumulate_time_ = kResetTime;

    cursor_.left.x -= cursor_adjust_.x;
    cursor_.left.y -= cursor_adjust_.y;
    cursor_.right.x += cursor_adjust_.x;
    cursor_.right.y += cursor_adjust_.y;

    if (cursor_adjust_.x == kCursorPlusAdjustX) {
      cursor_adjust_.x = kCursorMinusAdjustX;
      cursor_adjust_.y = kCursorMinusAdjustY;
    }
    else {
      cursor_adjust_.x = kCursorPlusAdjustX;
      cursor_adjust_.y = kCursorPlusAdjustY;
    }
  }
}

/// <summary>
/// カーソル表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleInfoUi::ShowCursor() {

  alpha_cursor_ = kAlphaMax;
}

/// <summary>
/// カーソル操作 左右移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleInfoUi::OperationCursorUp() {

  if (current_phase_ != PhaseType::kProcess) {
    return;
  }

  switch (button_type_) {
  case ButtonType::kGameStart: {
    button_type_ = ButtonType::kGameEnd;
    break;
  }
  case ButtonType::kOption: {
    button_type_ = ButtonType::kGameStart;
    break;
  }
  case ButtonType::kScore: {
    button_type_ = ButtonType::kOption;
    break;
  }
  case ButtonType::kGameEnd: {
    button_type_ = ButtonType::kScore;
    break;
  }
  }

  // カーソルを移動する
  OperationCursor();
}

/// <summary>
/// カーソル操作 下移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleInfoUi::OperationCursorDown() {

  switch (button_type_) {
  case ButtonType::kGameStart: {
    button_type_ = ButtonType::kOption;
    break;
  }
  case ButtonType::kOption: {
    button_type_ = ButtonType::kScore;
    break;
  }
  case ButtonType::kScore: {
    button_type_ = ButtonType::kGameEnd;
    break;
  }
  case ButtonType::kGameEnd: {
    button_type_ = ButtonType::kGameStart;
    break;
  }
  }

  // カーソルを移動する
  OperationCursor();
}

/// <summary>
/// カーソル操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleInfoUi::OperationCursor() {

  float x_pos_left = 0.0f;
  float y_pos_left = 0.0f;
  float x_pos_right = 0.0f;
  float y_pos_right = 0.0f;

  switch (button_type_) {
  case ButtonType::kGameStart: {
    x_pos_left = static_cast<float>(button_start_.left.x);
    y_pos_left = static_cast<float>(button_start_.left.y);
    x_pos_right = static_cast<float>(button_start_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_start_.left.y + kButtonHeight);
    break;
  }
  case ButtonType::kOption: {
    x_pos_left = static_cast<float>(button_option_.left.x);
    y_pos_left = static_cast<float>(button_option_.left.y);
    x_pos_right = static_cast<float>(button_option_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_option_.left.y + kButtonHeight);
    break;
  }
  case ButtonType::kScore: {
    x_pos_left = static_cast<float>(button_score_.left.x);
    y_pos_left = static_cast<float>(button_score_.left.y);
    x_pos_right = static_cast<float>(button_score_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_score_.left.y + kButtonHeight);
    break;
  }
  case ButtonType::kGameEnd: {
    x_pos_left = static_cast<float>(button_end_.left.x);
    y_pos_left = static_cast<float>(button_end_.left.y);
    x_pos_right = static_cast<float>(button_end_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_end_.left.y + kButtonHeight);
    break;
  }
  }

  cursor_.left.x = x_pos_left;
  cursor_.left.y = y_pos_left;
  cursor_.right.x = x_pos_right;
  cursor_.right.y = y_pos_right;

  for (int i = 0; i < static_cast<int>(ButtonType::kTypeMaxIndex); ++i) {
    if (i == static_cast<int>(button_type_)) {
      button_color_[i] = kButtonSelectColor;
    }
    else {
      button_color_[i] = kButtonNoneColor;
    }
  }
}

/// <summary>
/// 透過率の調整
/// </summary>
/// <param names=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool TitleInfoUi::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率に調整値を加算
  switch (current_phase_) {
  case PhaseType::kStartUp: {
    // 起動フェーズ
    alpha_ += kAlphaAdjust;
    break;
  }
  case PhaseType::kBeforeFinalize: {
    // 終了処理前フェーズ
    alpha_ -= kAlphaAdjust;
    alpha_cursor_ -= kAlphaAdjust;
    break;
  }
  }

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= kAlphaMax) {
    alpha_ = kAlphaMax;
    is_finish = true;
  }
  else if (alpha_ <= kAlphaMin) {
    alpha_ = kAlphaMin;
    alpha_cursor_ = kAlphaMin;
    is_finish = true;
  }

  return is_finish;
}