﻿#include "Game/BulletManager.h"

namespace {

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// リストID 最大値
  /// </summary>
  const int kMaxListId = 3000;

  /// <summary>
  /// リストID リセット値
  /// </summary>
  const int kResetListId = 0;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
BulletManager::BulletManager()
  : Task(TaskId::kBulletManager)
  , current_phase_(PhaseType::kProcess)
  , bullet_list_()
  , generate_bullet_list_()
  , release_id_list_()
  , list_id_max_(0)
  , alpha_(0)
  , alpha_adjust_(0) {

  // コンソールに出力
  std::cout << "BulletManager コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
BulletManager::~BulletManager() {

  // コンソールに出力
  std::cout << "~BulletManager デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void BulletManager::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kStop: {
    //---------------------------------
    //停止中フェーズ
    //---------------------------------
    return;
  }
  case PhaseType::kBeforeFinalize: {
    //---------------------------------
    //終了処理前フェーズ
    //---------------------------------
    bool is_finish = AdjustAlphaValue();
    break;
  }
  }

  // 描画終了処理が立っている弾を破棄する
  for (auto index : release_id_list_) {
    if (generate_bullet_list_.find(index) == generate_bullet_list_.end()) {
      continue;
    }
    Bullet* bullet = generate_bullet_list_[index];
    bool is_finish = bullet->IsFinish();
    if (!is_finish) {
      continue;
    }
    std::cout << "~Bullet破棄 : " << index << std::endl;
    generate_bullet_list_.erase(index);
    if (bullet != nullptr) {
      delete bullet;
      bullet = nullptr;
    }
  }

  // リリースリストをクリア
  release_id_list_.clear();

  // 弾の更新処理
  for (auto bullet : generate_bullet_list_) {

    if (bullet.second == nullptr) {
      continue;
    }
    // 終了処理前フェーズなら弾に透過率をセットする
    if (current_phase_ == PhaseType::kBeforeFinalize) {
      bullet.second->SetAlpha(alpha_, kAlphaMin, kAlphaMax, kAlphaMin);
    }

    // 更新処理を実行し、画面外に到達した弾のインデックスをリストにセットする
    bool is_finish = bullet.second->Update(process_time);
    if (is_finish) {
      release_id_list_.push_back(bullet.first);
    }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BulletManager::Render() {

  Bullet* bullet = nullptr;
  for (int i = 0; i < kMaxListId; ++i) {
    if (generate_bullet_list_.find(i) == generate_bullet_list_.end()) {
      continue;
    }
    bullet = generate_bullet_list_[i];
    if (bullet == nullptr) {
      continue;
    }
    // 描画処理
    bullet->Render();
  }
}

/// <summary>
/// 弾の生成
/// </summary>
/// <param name=""> バレットID </param>
/// <returns> バレット </returns>
Bullet* BulletManager::CreateBullet(BulletId bullet_id) {

  Bullet* bullet = nullptr;

  // 指定のバレットIDが弾のリストに存在するか確認
  // 存在しない場合は弾を生成してリストに登録
  if (bullet_list_.find(bullet_id) == bullet_list_.end()) {
    bullet = new Bullet(bullet_id);
    if (bullet == nullptr) {
      return nullptr;
    }
    bullet_list_[bullet_id] = bullet;
    return bullet;
  }

  // 対象の弾のクローンを生成
  bullet = bullet_list_[bullet_id]->GenerateClone();
  if (bullet == nullptr) {
    return nullptr;
  }

  // 現在のリストIDの最大値をセット
  int index = list_id_max_;
  // 生成リストの空きインデックスを調べ、生体リストに登録
  while (true) {
    if (generate_bullet_list_.find(index) == generate_bullet_list_.end()) {
      generate_bullet_list_[index] = bullet;
      list_id_max_ = ++index;
      break;
    }
    ++index;
  }
  if (list_id_max_ >= kMaxListId) {
    list_id_max_ = kResetListId;
  }

  return bullet;
}

/// <summary>
/// 弾の描画を終了する
/// </summary>
/// <param name="character"> キャラクター </param>
/// <returns></returns>
void BulletManager::FinishBullet(Character* character) {

  // キャラクターの種類を取得
  Character::CharacterType chara_type = character->GetCharacterType();

  // 生成リストに残っている弾をすべてリストから降ろし、破棄する
  if (!generate_bullet_list_.empty()) {
    // 生成リスト内のインデックスをすべて取得
    for (auto bullet : generate_bullet_list_) {
      BulletId bullet_id = bullet.second->GetBulletId();

      switch (chara_type) {
      case Character::CharacterType::kEnemyA:
      case Character::CharacterType::kEnemyB: {
        //----------------------------------
        // 敵キャラクター
        //----------------------------------
        if (bullet_id == BulletId::kNormal) {
          continue;
        }
        break;
      }
      case Character::CharacterType::kPlayer: {
        //----------------------------------
        // プレイヤー
        //----------------------------------
        if (bullet_id != BulletId::kNormal) {
          continue;
        }
        break;
      }
      }
      // 弾の描画を終了
      bullet.second->FinishDraw();
    }
  }

  // リストIDの最大値を初期値に戻す
  list_id_max_ = kResetListId;
}

/// <summary>
/// 弾の破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BulletManager::DisposeBullet() {

  std::vector<int> release_index;
  std::vector<BulletId> release_id;

  // 生成リストに弾が残っている場合はすべてリストから降ろし、破棄する
  if (!generate_bullet_list_.empty()) {
    // 生成リスト内のインデックスをすべて取得
    for (auto bullet : generate_bullet_list_) {
      release_index.push_back(bullet.first);
    }
    // 生成リストから降ろし、弾を破棄する
    for (auto index : release_index) {
      Bullet* bullet = generate_bullet_list_[index];
      generate_bullet_list_.erase(index);
      if (bullet != nullptr) {
        delete bullet;
        bullet = nullptr;
      }
    }
  }
  // リストのクリア
  release_index.clear();

  // バレットリストの破棄
  if (!bullet_list_.empty()) {
    // リスト内のインデックスをすべて取得
    for (auto bullet : bullet_list_) {
      release_id.push_back(bullet.first);
    }
    // 画像リソースの破棄後、バレットリストから降ろし、弾を破棄する
    for (auto index : release_id) {
      Bullet* bullet = bullet_list_[index];
      // 画像リソースの破棄
      DeleteGraph(bullet->GetGraphicHandle());
      // バレットリストから降ろし、弾の破棄
      bullet_list_.erase(index);
      if (bullet != nullptr) {
        delete bullet;
        bullet = nullptr;
      }
    }
  }
  // リストのクリア
  release_id.clear();
}

/// <summary>
/// 透過率をセットする
/// </summary>
/// <param name="value"> 透過率 </param>
/// <returns></returns>
void BulletManager::SetAlpha(int value) {

  alpha_ = value;
}

/// <summary>
/// 透過率の調整値をセットする
/// </summary>
/// <param name="adjust"> 透過率 調整値 </param>
/// <returns></returns>
void BulletManager::SetAlphaAdjust(int adjust) {

  alpha_adjust_ = adjust;
}

/// <summary>
/// 透過率変更処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BulletManager::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率に調整値を加算
  alpha_ += alpha_adjust_;

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= kAlphaMax) {
    alpha_ = kAlphaMax;
    is_finish = true;
  }
  else if (alpha_ <= kAlphaMin) {
    alpha_ = kAlphaMin;
    is_finish = true;
  }

  return is_finish;
}