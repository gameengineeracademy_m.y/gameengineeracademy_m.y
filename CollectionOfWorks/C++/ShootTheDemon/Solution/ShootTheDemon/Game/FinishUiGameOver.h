﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "System/FontInfo.h"
#include "Game/GameInfo.h"
#include "Game/FinishUiGameOverEventInterface.h"
#include <vector>

/// <summary>
/// ゲームオーバー時のフィニッシュUI
/// </summary>
class FinishUiGameOver : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,       // 初期化フェーズ
    kStartUp,          // 起動フェーズ
    kProcess,          // 処理中フェーズ
    kFinalizeBefore,   // 終了処理前フェーズ
    kFinalize,         // 終了処理フェーズ
    kFinalized,        // 終了処理済みフェーズ
    kPhaseMaxIndex     // フェーズ数
  };

  /// <summary>
  /// ゲームオーバー時のフェーズ
  /// </summary>
  enum class GameOverPhase {
    kScreenView,       // スクリーン表示フェーズ
    kTitleTextView,    // テキスト表示フェーズ
    kButtonView,       // ボタン表示フェーズ
    kSelectWait,       // 選択待機フェーズ
    kFinalize,         // 終了処理フェーズ
    kFinalized,        // 終了処理済みフェーズ
    kPhaseMaxIndex     // フェーズ数
  };

  /// <summary>
  /// ボタンの種類
  /// </summary>
  enum class ButtonType {
    kRetry,
    kTitle,
    kTypeMaxIndex      // 種類数
  };

  /// <summary>
  /// フォントサイズの種類
  /// </summary>
  enum class FontSize {
    k20,               // サイズ20
    k60,               // サイズ60
    kSizeMaxIndex      // 種類数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> ゲームオーバー処理イベントインターフェース </param>
  /// <returns></returns>
  FinishUiGameOver(FinishUiGameOverEventInterface& );

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~FinishUiGameOver();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// ボタン選択切り替え
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeSelectButton();

  /// <summary>
  /// ボタン選択決定処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DecideSelectButton();

  /// <summary>
  /// 現在のフェーズを変更
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }

  /// <summary>
  /// ゲームオーバーのフェーズを変更
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeGameOverPhase(GameOverPhase phase_type) { game_over_ = phase_type; }


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Initialize();

  /// <summary>
  /// 処理中
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Process(float);

  /// <summary>
  /// スクリーン表示
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool ViewScreen();

  /// <summary>
  /// テキスト表示
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool ViewTitleText();

  /// <summary>
  /// ボタン表示
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool ViewButton();

  /// <summary>
  /// 待機中処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void WaitProcess(float);


private:

  /// <summary>
  /// 表示座標
  /// </summary>
  struct Pos {
    int x;
    int y;
  };

  /// <summary>
  /// 表示座標 浮動小数
  /// </summary>
  struct Posf {
    float x;
    float y;
  };

  /// <summary>
  /// サイズ
  /// </summary>
  struct Size {
    int width;
    int height;
  };

  /// <summary>
  /// 図形
  /// </summary>
  struct Figure {
    Pos left;
    Pos right;
    Size size;
  };

  /// <summary>
  /// 図形 浮動小数
  /// </summary>
  struct Figuref {
    Posf left;
    Posf right;
  };

  /// <summary>
  /// 透過度
  /// </summary>
  struct Alpha {
    int value;
    int adjust;
    int max;
    int min;
  };

  /// <summary>
  /// ボタンの背景色
  /// </summary>
  struct Color {
    int retry;     // リトライ
    int title;     // タイトル
  };

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// ゲームオーバーのフェーズ
  /// </summary>
  GameOverPhase game_over_;

  /// <summary>
  /// ボタンの種類
  /// </summary>
  ButtonType button_type_;

  /// <summary>
  /// ゲームオーバー時のフィニッシュUI
  /// </summary>
  FinishUiGameOverEventInterface& finish_ui_event_interface_;

  /// <summary>
  /// メニュー画面 フレーム
  /// </summary>
  Figure frame_;

  /// <summary>
  /// メニュー画面 本体
  /// </summary>
  Figure screen_;

  /// <summary>
  /// メニュー画面 文字隠し
  /// </summary>
  Figure text_hide_;

  /// <summary>
  /// メニュー画面 リトライボタン
  /// </summary>
  Figure button_retry_;

  /// <summary>
  /// メニュー画面 タイトルボタン
  /// </summary>
  Figure button_title_;

  /// <summary>
  /// メニュー画面 カーソル
  /// </summary>
  Figuref cursor_;

  /// <summary>
  /// メニュー画面 カーソル調整値
  /// </summary>
  Posf cursor_adjust_;

  /// <summary>
  /// メニュー画面 文字表示位置
  /// </summary>
  Pos title_text_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_button_retry_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_button_title_;

  /// <summary>
  /// 透過度 フレーム
  /// </summary>
  Alpha alpha_frame_;

  /// <summary>
  /// 透過度 本体
  /// </summary>
  Alpha alpha_screen_;

  /// <summary>
  /// 透過度 文字隠し
  /// </summary>
  Alpha alpha_hide_;

  /// <summary>
  /// 透過度 ボタン
  /// </summary>
  Alpha alpha_button_;

  /// <summary>
  /// 透過度 タイトル
  /// </summary>
  Alpha alpha_text_;

  /// <summary>
  /// ボタンの色
  /// </summary>
  Color button_color_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// フォントハンドル
  /// </summary>
  std::vector<int> font_handle_;
};