﻿#pragma once

#include "System/Character.h"
#include "Game/ItemController.h"
#include "Game/ItemEventInterface.h"

/// <summary>
/// アイテムベース
/// </summary>
class ItemBase : public Character {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> タスクID </param>
  /// <param name=""> キャラクターの種類 </param>
  /// <param name=""> キャラクターコントローライベントインターフェース </param>
  /// <param name=""> アイテムイベントインターフェース </param>
  /// <param name=""> キャラクターイベントインターフェース </param>
  /// <returns></returns>
  ItemBase(TaskId, CharacterType, CharacterControllerEventInterface&, ItemEventInterface&,
           CharacterEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~ItemBase();

  /// <summary>
  /// 毎フレームの描画処理
  /// </summary>
  /// <param names=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// グラフィックハンドルをセットする
  /// </summary>
  /// <param names="handle"> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandle(int handle) { graphic_handle_ = handle; }

  /// <summary>
  /// グラフィックハンドルを渡す
  /// </summary>
  /// <param names=""></param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandle() { return graphic_handle_; }

  /// <summary>
  /// 透過率の設定
  /// </summary>
  /// <param names=""> 透過率 </param>
  /// <param names=""> 調整値 </param>
  /// <param names=""> 最大値 </param>
  /// <param names=""> 最小値 </param>
  /// <returns></returns>
  void SetAlpha(int, int, int, int);

  /// <summary>
  /// 描画フラグを立てる
  /// </summary>
  /// <param names=""></param>
  /// <returns></returns>
  void SetFinish();

  /// <summary>
  /// 描画フラグの設定をリセットする
  /// </summary>
  /// <param names=""></param>
  /// <returns></returns>
  void ResetFinish() { is_finish_ = false; }

  /// <summary>
  /// 描画終了かどうかを確認する
  /// </summary>
  /// <param names=""></param>
  /// <returns> true:終了, false:描画中 </returns>
  bool IsFinish() { return is_finish_ == true; }

  /// <summary>
  /// 透過率変更処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AdjustAlphaValue();

  /// <summary>
  /// 透過率の符号を変更
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeAlphaAdjustSign();


protected:

  /// <summary>
  /// アイテムイベントインターフェース
  /// </summary>
  ItemEventInterface& item_event_interfase_;

  /// <summary>
  /// アイテムコントローラ
  /// </summary>
  ItemController item_controller_;

  /// <summary>
  /// グラフィックハンドル
  /// </summary>
  int graphic_handle_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  int alpha_adjust_;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  int alpha_max_;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  int alpha_min_;

  /// <summary>
  /// 描画終了フラグ
  /// </summary>
  bool is_finish_;
};