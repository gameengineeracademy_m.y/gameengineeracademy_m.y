﻿#pragma once

#include "DxLib.h"
#include "System/Level.h"
#include "System/PlayerController.h"
#include "System/MenuController.h"
#include "System/SoundManager.h"
#include "System/CutInManager.h"
#include "System/BackGroundSide.h"
#include "System/EffectManager.h"
#include "System/SoundSetup.h"
#include "System/DebugMenu.h"
#include "System/VolumeController.h"
#include "Game/FieldManager.h"
#include "Game/Player.h"
#include "Game/EnemyA.h"
#include "Game/EnemyB.h"
#include "Game/EnemyController.h"
#include "Game/BulletManager.h"
#include "Game/BarrageManager.h"
#include "Game/BattleLevelEventInterface.h"
#include "Game/HitPointBarManager.h"
#include "Game/GameMode.h"
#include "Game/ItemExtend.h"
#include "Game/ItemPowerUp.h"
#include "Game/FinishUiGameOver.h"
#include "Game/FinishUiGameClear.h"
#include "Game/GameTimer.h"
#include "Game/PauseUi.h"
#include <fstream>
#include <string>
#include <math.h>
#include <vector>
#include <unordered_map>
#include <stdio.h>

namespace {

  /// <summary>
  /// ロード表示文字列
  /// </summary>
  const int kLoadTextMaxSize = 4;
}

/// <summary>
/// バトルレベル
/// </summary>
/// <remarks>
/// 実際にゲームを扱うバトル画面のタスク
/// </remarks>
class BattleLevel : public Level, public BattleLevelEventInterface {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kNone,                // 何もしない
    kLoadFile,            // 設定ファイル読み込みフェーズ
    kPrepare,             // 準備フェーズ
    kStartPrepare,        // 起動準備フェーズ
    kStartUp,             // 起動フェーズ
    kPlayWait,            // プレイ待機フェーズ
    kStartPlay,           // プレイ開始フェーズ
    kPlay,                // プレイ中フェーズ
    kStopPrepare,         // プレイ停止準備フェーズ
    kGameOver,            // ゲームオーバーフェーズ
    kGameClear,           // ゲームクリアフェーズ
    kPause,               // ポーズ処理フェーズ
    kPauseWait,           // ポーズ処理待機フェーズ
    kSoundSetting,        // サウンド設定フェーズ
    kDebugMenu,           // デバッグメニューフェーズ
    kDebugMenuWait,       // デバッグメニュー操作フェーズ
    kBeforeTransition,    // レベル遷移前フェーズ
    kLevelTransition,     // レベル遷移フェーズ
    kFinalize,            // 終了処理フェーズ
    kFinalized,           // 終了処理済みフェーズ
    kPhaseMaxIndex        // フェーズ数
  };

  /// <summary>
  /// ロードファイルの順番
  /// </summary>
  enum class LoadFilePhase {
    kGameData,            // ゲームデータフェーズ
    kSoundData,           // サウンドデータフェーズ
    kBullet,              // 弾の種類フェーズ
    kBarrage,             // 弾幕種類フェーズ
    kEffect,              // エフェクトの種類フェーズ
    kPlayer,              // プレイヤーの設定フェーズ
    kEnemyA,              // 敵キャラAの設定フェーズ
    kEnemyB,              // 敵キャラBの設定フェーズ
    kItemA,               // アイテムAの設定フェーズ
    kItemB,               // アイテムBの設定フェーズ
    kEnemyAFirstPhase,    // 敵キャラAの行動フェーズ 1
    kEnemyASecondPhase,   // 敵キャラAの行動フェーズ 2
    kEnemyAThirdPhase,    // 敵キャラAの行動フェーズ 3
    kEnemyAFourthPhase,   // 敵キャラAの行動フェーズ 4
    kEnemyAFifthPhase,    // 敵キャラAの行動フェーズ 5
    kEnemyASixthPhase,    // 敵キャラAの行動フェーズ 6
    kEnemyBFirstPhase,    // 敵キャラBの行動フェーズ 1
    kEnemyBSecondPhase,   // 敵キャラBの行動フェーズ 2
    kEnemyBThirdPhase,    // 敵キャラBの行動フェーズ 3
    kEnemyBFourthPhase,   // 敵キャラBの行動フェーズ 4
    kEnemyBFifthPhase,    // 敵キャラBの行動フェーズ 5
    kEnemyBSixthPhase,    // 敵キャラBの行動フェーズ 6
    kFinish,              // 処理終了フェーズ
    kPhaseMaxIndex        // フェーズ数
  };

  /// <summary>
  /// 準備フェーズの種類
  /// </summary>
  enum class PreparePhase {
    kRegisterBullet,      // 弾の登録フェーズ
    kRegisterBarrage,     // 弾幕の登録フェーズ
    kRegisterSound,       // 音楽の登録フェーズ
    kRegisterHpBar,       // HPバーの登録フェーズ
    kRegisterEffect,      // エフェクトの登録フェーズ
    kFinish,              // 処理終了フェーズ
    kPhaseMaxIndex        // フェーズ数
  };

  /// <summary>
  /// 弾の登録フェーズ
  /// </summary>
  enum class RegisterBulletPhase {
    kNormal,              // 通常弾の登録フェーズ
    kBlueBall,            // 青弾の登録フェーズ
    kPurpleBall,          // 紫弾の登録フェーズ
    kRedBall,             // 赤弾の登録フェーズ
    kYellowBall,          // 黄弾の登録フェーズ
    kGreenBall,           // 緑弾の登録フェーズ
    kFinish,              // 処理終了フェーズ
    kPhaseMaxIndex        // フェーズ数
  };

  /// <summary>
  /// 弾幕の登録フェーズ
  /// </summary>
  enum class RegisterBarragePhase {
    kAllDirection,        // 全方向の登録フェーズ
    kRotation,            // 回転の登録フェーズ
    kContinuous,          // 連射の登録フェーズ
    kSingle,              // 単発射撃の登録フェーズ
    kDouble,              // 2倍射撃の登録フェーズ
    kTriple,              // 3倍射撃の登録フェーズ
    kQuadruple,           // 4倍射撃の登録フェーズ
    kFinish,              // 処理終了フェーズ
    kPhaseMaxIndex        // フェーズ数
  };

  /// <summary>
  /// 音楽登録フェーズ
  /// </summary>
  enum class RegisterSoundPhase {
    kButtonSelect,        // ボタン選択音の登録フェーズ
    kButtonPush,          // ボタン押下音の登録フェーズ
    kShoot,               // 射撃音の登録フェーズ
    kPlayerDeath,         // プレイヤー死亡音の登録フェーズ
    kPlayerExplosion,     // プレイヤー爆発音の登録フェーズ
    kPlayerReborn,        // プレイヤー復活音の登録フェーズ
    kEnemyExplosion,      // 敵の爆破音の登録フェーズ
    kCutIn,               // カットイン音の登録フェーズ
    kGetItem,             // アイテム獲得音の登録フェーズ
    kGameOver,            // ゲームオーバーの登録フェーズ
    kGameClear,           // ゲームクリアの登録フェーズ
    kBgm,                 // BGMの登録フェーズ
    kTestBgm,             // BGM(テスト用)の登録フェーズ
    kFinish,              // 処理終了フェーズ
    kPhaseMaxIndex        // フェーズ数
  };

  /// <summary>
  /// エフェクト登録フェーズ
  /// </summary>
  enum class RegisterEffectPhase {
    kPlayerDeath,         // プレイヤー死亡の登録フェーズ
    kPlayerExplosion,     // プレイヤー爆発の登録フェーズ
    kPlayerReborn,        // プレイヤー復活の登録フェーズ
    kDisappearNormal,     // 通常弾消滅の登録フェーズ
    kDisappearBlueBall,   // 青弾消滅の登録フェーズ
    kDisappearPurpleBall, // 紫弾消滅の登録フェーズ
    kDisappearRedBall,    // 赤弾消滅の登録フェーズ
    kDisappearYellowBall, // 黄弾消滅の登録フェーズ
    kDisappearGreenBall,  // 緑弾消滅の登録フェーズ
    kEnemyExplosion,      // 敵の爆発の登録フェーズ
    kEnemyRupture,        // 敵の破裂の登録フェーズ
    kItemSummon,          // アイテム生成の登録フェーズ
    kFinish,              // 処理終了フェーズ
    kPhaseMaxIndex        // フェーズ数
  };

  /// <summary>
  /// 戦闘フェーズ
  /// </summary>
  enum class BattlePhase {
    kTransitionEnemyA,    // 敵キャラAへの遷移処理
    kEnemyA,              // 敵キャラA戦闘フェーズ
    kTransitionEnemyB,    // 敵キャラBへの遷移処理
    kEnemyB,              // 敵キャラB戦闘フェーズ
    kFinish,              // 処理終了フェーズ
    kPhaseMaxIndex        // フェーズ数
  };

  /// <summary>
  /// ゲームオーバーフェーズ
  /// </summary>
  enum class GameOverPhase {
    kStopEnemy,           // 敵の行動を止める
    kTaskAdd,             // タスクを積むフェーズ
    kWait,                // 待機フェーズ
    kPhaseMaxIndex        // フェーズ数
  };

  /// <summary>
  /// ゲームクリアフェーズ
  /// </summary>
  enum class GameClearPhase {
    kTaskAdd,             // タスクを積むフェーズ
    kProduction,          // 演出待機フェーズ
    kTaskRelease,         // タスクを降ろすフェーズ
    kPlayerInit,          // プレイヤー初期位置配置フェーズ
    kPlayerMove,          // プレイヤー移動フェーズ
    kFinish,              // 終了処理フェーズ
    kFinished,            // 終了処理済みフェーズ
    kPhaseMaxIndex        // フェーズ数
  };

  /// <summary>
  /// クリア後のプレイヤー移動フェーズ
  /// </summary>
  enum class PlayerMovePhase {
    kBackward1,           // 後退フェーズ1
    kBackward2,           // 後退フェーズ2
    kForward,             // 前進フェーズ
    kPhaseMaxIndex        // フェーズ数
  };

  /// <summary>
  /// 終了フェーズの種類一覧
  /// </summary>
  enum class FinalizePhaseType {
    kUnloadTask,         // タスクを降ろすフェーズ
    kDisposeTask,        // タスクを破棄するフェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };


public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> タスクID </param>
  /// <param name=""> タスクマネージャ </param>
  /// <returns></returns>
  BattleLevel(TaskId, TaskManager&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~BattleLevel();

  /// <summary>
  /// 上矢印キー押下イベント
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <param name=""> 左Shiftキー押下有無 </param>
  /// <returns></returns>
  void OnPushUpKey(Character*, bool) override;

  /// <summary>
  /// 下矢印キー押下イベント
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <param name=""> 左Shiftキー押下有無 </param>
  /// <returns></returns>
  void OnPushDownKey(Character*, bool) override;

  /// <summary>
  /// 左矢印キー押下イベント
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <param name=""> 左Shiftキー押下有無 </param>
  /// <returns></returns>
  void OnPushLeftKey(Character*, bool) override;

  /// <summary>
  /// 右矢印キー押下イベント
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <param name=""> 左Shiftキー押下有無 </param>
  /// <returns></returns>
  void OnPushRightKey(Character*, bool) override;

  /// <summary>
  /// Zキー押下イベント
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  void OnPushZKey(Character*) override;

  /// <summary>
  /// Escキー押下イベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushEscapeKey() override;

  /// <summary>
  /// Oキー押下イベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushOKey() override;

  /// <summary>
  /// Pキー押下イベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushPKey() override;

  /// <summary>
  /// Lキー押下イベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushLKey() override;

  /// <summary>
  /// Iキー押下イベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushIKey() override;

  /// <summary>
  /// Qキー押下イベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushQKey() override;

  /// <summary>
  /// BGM終了通知
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnFinishSoundBGM() override;

  /// <summary>
  /// キャラクター終了通知
  /// </summary>
  /// <param> キャラクター </param>
  /// <returns></returns>
  void OnFinish(Character*) override;

  /// <summary>
  /// HPバー生成処理
  /// </summary>
  /// <param> キャラクター </param>
  /// <returns></returns>
  void OnCreateHpBar(Character*) override;

  /// <summary>
  /// HPバー破棄処理
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  void OnDisposeHpBar(Character*) override;

  /// <summary>
  /// 当たり判定チェック
  /// </summary>
  /// <param name=""> バレット </param>
  /// <returns></returns>
  bool OnCheckCollisionBullet(Bullet*) override;

  /// <summary>
  /// 弾の生成
  /// </summary>
  /// <param name=""> バレットID </param>
  /// <returns></returns>
  Bullet* OnCreateBullet(BulletId) override;

  /// <summary>
  /// 弾幕表示開始
  /// </summary>
  /// <param name=""> バレッジID </param>
  /// <returns></returns>
  Barrage* OnStartBarrage(BarrageId) override;

  /// <summary>
  /// 弾幕表示終了
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnFinishBarrage() override;

  /// <summary>
  /// 弾の表示終了
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  void OnFinishDisplayBullet(Character*) override;

  /// <summary>
  /// 弾の表示終了
  /// </summary>
  /// <param name=""> バレット </param>
  /// <returns></returns>
  void OnFinishBullet(Bullet*) override;

  /// <summary>
  /// プレイヤーの座標を取得
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  void OnGetPlayerPosition(float&, float&) override;

  /// <summary>
  /// バトルフェーズの変更
  /// </summary>
  /// <param name="character"> キャラクター </param>
  /// <returns></returns>
  void OnChangeBattlePhase(Character*);

  /// <summary>
  /// アイテム生成
  /// </summary>
  /// <param name=""> キャラクターの種類 </param>
  /// <returns></returns>
  void OnCreateItem(Character::CharacterType) override;

  /// <summary>
  /// アイテム処理の終了を通知する
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  void OnFinishItem(Character*) override;

  /// <summary>
  /// カットイン処理
  /// </summary>
  /// <param name=""> フェーズ数 </param>
  /// <param name=""> カットインの色 </param>
  /// <returns></returns>
  void OnDisplayCutIn(int, CutInManager::ColorType) override;

  /// <summary>
  /// カットイン処理終了通知
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnFinishCutIn() override;

  /// <summary>
  /// バトル再開
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnResumeBattle() override;

  /// <summary>
  /// エフェクト処理
  /// </summary>
  /// <param name=""> エフェクトID </param>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  void OnExecuteEffect(EffectId, Character*) override;

  /// <summary>
  /// 残機のチェック
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:ゲームオーバー, false:再開 </returns>
  bool OnCheckLeftLife() override;

  /// <summary>
  /// 爆破用エフェクトを実行
  /// </summary>
  /// <param name="character"> キャラクター </param>
  /// <returns></returns>
  void OnExecuteEnemyEffect(Character*) override;

  /// <summary>
  /// 本体の当たり判定
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  void OnCheckCollisionEnemy(Character*) override;

  /// <summary>
  /// 花火の打ち上げ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnShootFirework1() override;

  /// <summary>
  /// 花火の打ち上げ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnShootFirework2() override;

  /// <summary>
  /// 花火の打ち上げ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnShootFirework3() override;

  /// <summary>
  /// テキスト非表示終了通知
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnFinishHideGameClearText() override;

  /// <summary>
  /// SE生成
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPlayGameOverSound() override;

  /// <summary>
  /// リトライする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnTryAgain() override;

  /// <summary>
  /// タイトルへ遷移する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnGoToTitle() override;

  /// <summary>
  /// Enterキーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushMenuControllerEnterKey() override;

  /// <summary>
  /// Escapeキーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushMenuControllerEscapeKey() override;

  /// <summary>
  /// Zキーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushMenuControllerZ() override;

  /// <summary>
  /// Xキーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushMenuControllerX() override;

  /// <summary>
  /// Qキーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushMenuControllerQ() override;

  /// <summary>
  /// 右矢印キーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushMenuControllerRight() override;

  /// <summary>
  /// 左矢印キーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushMenuControllerLeft() override;

  /// <summary>
  /// 上矢印キーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushMenuControllerUp() override;

  /// <summary>
  /// 下矢印キーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushMenuControllerDown() override;

  /// <summary>
  /// 右矢印キーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushVolumeControllerRight() override;

  /// <summary>
  /// 左矢印キーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushVolumeControllerLeft() override;

  /// <summary>
  /// バトルレベルのフェーズ確認
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:プレイ中, false:それ以外 </returns>
  bool OnCheckBattleLevelPhase() override;

  /// <summary>
  /// プレイヤーの生存しているかどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:死亡, false:生存 </returns>
  bool OnCheckPlayerDead() override;

  /// <summary>
  /// ラップタイムセット
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <param name=""> フェーズ数 </param>
  /// <returns></returns>
  void OnSetRapTime(Character*, int) override;

  /// <summary>
  /// BGMの再生
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPlaySoundBGM() override;

  /// <summary>
  /// BGMの破棄
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnDisposeSoundBGM() override;


protected:

  /// <summary>
  /// 初期化処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool InitializePhase(float) override;

  /// <summary>
  /// 毎フレームの処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool UpdatePhase(float) override;

  /// <summary>
  /// 毎フレームの描画処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void RenderPhase() override;

  /// <summary>
  /// 終了処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool FinishPhase(float) override;


private:

  /// <summary>
  /// ロード処理 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeLoadScreen();

  /// <summary>
  /// フィールドマネージャ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeFieldManager();

  /// <summary>
  /// プレイヤー 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializePlayer();

  /// <summary>
  /// プレイヤーコントローラ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializePlayerController();

  /// <summary>
  /// バレットマネージャ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeBulletManager();

  /// <summary>
  /// サウンドマネージャ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeSoundManager();

  /// <summary>
  /// 敵キャラA 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeEnemyA();

  /// <summary>
  /// 敵キャラB 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeEnemyB();

  /// <summary>
  /// バレッジマネージャ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeBarrageManager();

  /// <summary>
  /// ヒットポイントバーマネージャ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeHitPointBarManager();

  /// <summary>
  /// ゲームモード 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeGameMode();

  /// <summary>
  /// 残機UPアイテム 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeItemExtend();

  /// <summary>
  /// パワーUPアイテム 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeItemPowerUp();

  /// <summary>
  /// カットインマネージャ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeCutInManager();

  /// <summary>
  /// バックグラウンドサイド 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeBackGroundSide();

  /// <summary>
  /// エフェクトマネージャ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeEffectManager();

  /// <summary>
  /// ゲームオーバー処理 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeGameOver();

  /// <summary>
  /// ゲームクリア処理 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeGameClear();

  /// <summary>
  /// メニューコントローラ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeMenuController();

  /// <summary>
  /// タイマー 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeGameTimer();

  /// <summary>
  /// ポーズUI処理 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializePauseUi();

  /// <summary>
  /// サウンド設定処理 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeSoundSetup();

  /// <summary>
  /// ボリュームコントローラ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeVolumeController();

  /// <summary>
  /// デバッグメニュー 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeDebugMenu();

  /// <summary>
  /// 設定ファイル読み込み
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool LoadSetupFile();

  /// <summary>
  /// 準備処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:処理継続中 </returns>
  bool PrepareGame();

  /// <summary>
  /// 弾の登録処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:処理継続中 </returns>
  bool RegisterBullet();

  /// <summary>
  /// 弾の設定
  /// </summary>
  /// <param name=""> バレット </param>
  /// <param name=""> 弾情報 </param>
  /// <returns></returns>
  void SetBulletData(Bullet*, Bullet::BulletData);

  /// <summary>
  /// 弾幕の登録処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:処理継続中 </returns>
  bool RegisterBarrage();

  /// <summary>
  /// 弾幕の設定
  /// </summary>
  /// <param name=""> バレッジ </param>
  /// <param name=""> 弾幕情報 </param>
  /// <returns></returns>
  void SetBarrageData(Barrage*, Barrage::BarrageData);

  /// <summary>
  /// サウンドの登録処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:処理継続中 </returns>
  bool RegisterSound();

  /// <summary>
  /// エフェクトの登録処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:処理継続中 </returns>
  bool RegisterEffect();

  /// <summary>
  /// HPバーの登録処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:処理継続中 </returns>
  bool RegisterHpBar();

  /// <summary>
  /// プレイヤー死亡 画像ファイル取得
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadImageHandlePlayerDeath(std::vector<int>&);

  /// <summary>
  /// プレイヤー爆発 画像ファイル取得
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadImageHandlePlayerExplosion(std::vector<int>&);

  /// <summary>
  /// プレイヤー復活 画像ファイル取得
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadImageHandlePlayerReborn(std::vector<int>&);

  /// <summary>
  /// 通常弾消滅 画像ファイル取得
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadImageHandleDisappearNormal(std::vector<int>&);

  /// <summary>
  /// 弾消滅 画像ファイル取得
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <param name=""> エフェクトID </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadImageHandleDisappearBall(std::vector<int>&, EffectId);

  /// <summary>
  /// 敵の爆破 画像ファイル取得
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadImageHandleEnemyExplosion(std::vector<int>&);

  /// <summary>
  /// 敵の破裂 画像ファイル取得
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadImageHandleEnemyRupture(std::vector<int>&);

  /// <summary>
  /// アイテム生成 画像ファイル取得
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadImageHandleItemSummon(std::vector<int>&);

  /// <summary>
  /// 当たり判定
  /// </summary>
  /// <param name=""> 攻撃側のX座標 </param>
  /// <param name=""> 攻撃側のY座標 </param>
  /// <param name=""> 攻撃側の半径 </param>
  /// <param name=""> 攻撃側の攻撃力 </param>
  /// <param name=""> 攻撃側のキャラクターの種類 </param>
  /// <returns> true:バトル区切り, false:バトル継続 </returns>
  bool CheckCollision(float, float, float, int, Character::CharacterType);

  /// <summary>
  /// ダメージ計算処理
  /// </summary>
  /// <param name=""> 相手のキャラクター </param>
  /// <param name=""> 攻撃力 </param>
  /// <returns> キャラクターのヒットポイント </returns>
  int CalcDamage(Character*, int);

  /// <summary>
  /// ゲーム遷移処理
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  void SwitchGamePhase(Character*);

  /// <summary>
  /// エフェクト処理
  /// </summary>
  /// <param name=""> エフェクトID </param>
  /// <param name=""> X座標</param>
  /// <param name=""> Y座標</param>
  /// <returns></returns>
  void ExecuteEffect(EffectId, int, int);

  /// <summary>
  /// ゲームオーバー処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void ExecuteGameOver(float);

  /// <summary>
  /// ゲームクリア処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void ExecuteGameClear(float);

  /// <summary>
  /// プレイヤーを初期位置に戻す
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool ResetPlayerPosition();

  /// <summary>
  /// プレイヤーを画面外に移動させていく
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool MovePlayer(float);

  /// <summary>
  /// アクティブフラグがONかどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:アクティブ, false:非アクティブ </returns>
  bool IsActive() { return is_active_ == true; }

  /// <summary>
  /// デバッグメニュー決定操作
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DecideDebugMenu();

  /// <summary>
  /// ポーズメニュー決定操作
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DecidePauseMenu();

  /// <summary>
  /// サウンドメニュー決定操作
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DecideSoundMenu();

  /// <summary>
  /// メニュー操作終了
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void FinishMenu();

  /// <summary>
  /// ロードファイル ゲームデータ
  /// </summary>
  /// <param name=""> ファイルパス </param>
  /// <param name=""> ゲーム情報 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadFileGameData(std::string, GameMode::GameData&);

  /// <summary>
  /// ロードファイル サウンドデータ
  /// </summary>
  /// <param name=""> ファイルパス </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadFileSoundData(std::string);

  /// <summary>
  /// ロードファイル 弾情報
  /// </summary>
  /// <param name=""> ファイルパス </param>
  /// <param name=""> 弾の情報 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadFileBullet(std::string, Bullet::BulletData&);

  /// <summary>
  /// ロードファイル 弾幕情報
  /// </summary>
  /// <param name=""> ファイルパス </param>
  /// <param name=""> 弾幕の情報 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadFileBarrage(std::string, Barrage::BarrageData&);

  /// <summary>
  /// ロードファイル エフェクト情報
  /// </summary>
  /// <param name=""> ファイルパス </param>
  /// <param name=""> 弾幕の情報 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadFileEffect(std::string, Effect::EffectData&);

  /// <summary>
  /// ロードファイル キャラクター情報
  /// </summary>
  /// <param name=""> ファイルパス </param>
  /// <param name=""> キャラクターの情報 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadFileCharacter(std::string, Character::CharaData&);

  /// <summary>
  /// ロードファイル ヒットポイント情報
  /// </summary>
  /// <param name=""> ファイルパス </param>
  /// <param name=""> ヒットポイント </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadFilePhaseHitPoint(std::string, EnemyBase::HitPoint&);

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }

  /// <summary>
  /// ロードファイルフェーズを変更する
  /// </summary>
  /// <param name=""> ロードファイルフェーズの種類 </param>
  /// <returns></returns>
  void ChangeLoadFilePhase(LoadFilePhase phase_type) { load_file_phase_ = phase_type; }

  /// <summary>
  /// 準備フェーズを変更する
  /// </summary>
  /// <param name=""> 準備フェーズの種類 </param>
  /// <returns></returns>
  void ChangePreparePhase(PreparePhase phase_type) { prepare_phase_ = phase_type; }

  /// <summary>
  /// 弾の登録フェーズを変更する
  /// </summary>
  /// <param name=""> 弾の登録フェーズの種類 </param>
  /// <returns></returns>
  void ChangeBulletPhase(RegisterBulletPhase phase_type) { bullet_phase_ = phase_type; }

  /// <summary>
  /// 弾幕の登録フェーズを変更する
  /// </summary>
  /// <param name=""> 弾幕の登録フェーズの種類 </param>
  /// <returns></returns>
  void ChangeBarragePhase(RegisterBarragePhase phase_type) { barrage_phase_ = phase_type; }

  /// <summary>
  /// 音楽登録フェーズを変更する
  /// </summary>
  /// <param name=""> 音楽登録フェーズの種類 </param>
  /// <returns></returns>
  void ChangeSoundPhase(RegisterSoundPhase phase_type) { sound_phase_ = phase_type; }

  /// <summary>
  /// エフェクト登録フェーズを変更する
  /// </summary>
  /// <param name=""> エフェクト登録フェーズの種類 </param>
  /// <returns></returns>
  void ChangeEffectPhase(RegisterEffectPhase phase_type) { effect_phase_ = phase_type; }

  /// <summary>
  /// バトルフェーズを変更する
  /// </summary>
  /// <param name=""> バトルフェーズの種類 </param>
  /// <returns></returns>
  void ChangeBattlePhase(BattlePhase battle_phase) { battle_phase_ = battle_phase; }

  /// <summary>
  /// ゲームオーバーフェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeGameOverPhase(GameOverPhase game_over_phase) { game_over_phase_ = game_over_phase; }

  /// <summary>
  /// ゲームクリアフェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeGameClearPhase(GameClearPhase game_clear_phase) { game_clear_phase_ = game_clear_phase; }

  /// <summary>
  /// プレイヤー移動フェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangePlayerMovePhase(PlayerMovePhase phase_type) { move_phase_ = phase_type; }

  /// <summary>
  /// 現在の終了フェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeFinalizePhase(FinalizePhaseType finalize_phase) { finalize_phase_ = finalize_phase; }

  /// <summary>
  /// 透過率変更処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AdjustAlphaValue();

 
private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// ロードファイルのフェーズ
  /// </summary>
  LoadFilePhase load_file_phase_;

  /// <summary>
  /// 準備のフェーズ
  /// </summary>
  PreparePhase prepare_phase_;

  /// <summary>
  /// 弾の登録フェーズ
  /// </summary>
  RegisterBulletPhase bullet_phase_;

  /// <summary>
  /// 弾幕の登録フェーズ
  /// </summary>
  RegisterBarragePhase barrage_phase_;

  /// <summary>
  /// サウンド登録フェーズ
  /// </summary>
  RegisterSoundPhase sound_phase_;

  /// <summary>
  /// エフェクト登録フェーズ
  /// </summary>
  RegisterEffectPhase effect_phase_;

  /// <summary>
  /// バトルフェーズ
  /// </summary>
  BattlePhase battle_phase_;

  /// <summary>
  /// ゲームオーバーフェーズ
  /// </summary>
  GameOverPhase game_over_phase_;

  /// <summary>
  /// ゲームクリアフェーズ
  /// </summary>
  GameClearPhase game_clear_phase_;

  /// <summary>
  /// プレイヤー移動フェーズ
  /// </summary>
  PlayerMovePhase move_phase_;

  /// <summary>
  /// 現在の終了フェーズ
  /// </summary>
  FinalizePhaseType finalize_phase_;

  /// <summary>
  /// フィールドマネージャ
  /// </summary>
  FieldManager* field_manager_;

  /// <summary>
  /// プレイヤー
  /// </summary>
  Player* player_;

  /// <summary>
  /// プレイヤーコントローラ
  /// </summary>
  PlayerController* player_controller_;

  /// <summary>
  /// バレットマネージャ
  /// </summary>
  BulletManager* bullet_manager_;

  /// <summary>
  /// サウンドマネージャ
  /// </summary>
  SoundManager* sound_manager_;

  /// <summary>
  /// 敵キャラA
  /// </summary>
  EnemyA* enemy_a_;

  /// <summary>
  /// 敵キャラB
  /// </summary>
  EnemyB* enemy_b_;

  /// <summary>
  /// バレッジマネージャ
  /// </summary>
  BarrageManager* barrage_manager_;

  /// <summary>
  /// ヒットポイントバーマネージャ
  /// </summary>
  HitPointBarManager* hp_bar_manager_;

  /// <summary>
  /// ゲームモード
  /// </summary>
  GameMode* game_mode_;

  /// <summary>
  /// 残機UPアイテム
  /// </summary>
  ItemExtend* item_extend_;

  /// <summary>
  /// パワーUPアイテム
  /// </summary>
  ItemPowerUp* item_power_up_;

  /// <summary>
  /// カットインマネージャ
  /// </summary>
  CutInManager* cut_in_manager_;

  /// <summary>
  /// バックグラウンド
  /// </summary>
  BackGroundSide* back_ground_;

  /// <summary>
  /// エフェクトマネージャ
  /// </summary>
  EffectManager* effect_manager_;

  /// <summary>
  /// ゲームオーバー
  /// </summary>
  FinishUiGameOver* game_over_;

  /// <summary>
  /// ゲームクリア
  /// </summary>
  FinishUiGameClear* game_clear_;

  /// <summary>
  /// メニューコントローラ
  /// </summary>
  MenuController* menu_controller_;

  /// <summary>
  /// タイマー
  /// </summary>
  GameTimer* game_timer_;

  /// <summary>
  /// ポーズUI処理
  /// </summary>
  PauseUi* pause_ui_;

  /// <summary>
  /// サウンド設定処理
  /// </summary>
  SoundSetup* sound_setup_;

  /// <summary>
  /// ボリュームコントローラ
  /// </summary>
  VolumeController* volume_controller_;

  /// <summary>
  /// デバッグメニュー
  /// </summary>
  DebugMenu* debug_menu_;

  /// <summary>
  /// 設定値
  /// </summary>
  std::unordered_map<Sound::SoundType, float> sound_volume_list_;

  /// <summary>
  /// 設定値
  /// </summary>
  std::unordered_map<BulletId, Bullet::BulletData> bullet_list_;

  /// <summary>
  /// 設定値
  /// </summary>
  std::unordered_map<BarrageId, Barrage::BarrageData> barrage_list_;

  /// <summary>
  /// 設定値
  /// </summary>
  std::unordered_map<EffectId, Effect::EffectData> effect_list_;

  /// <summary>
  /// 管理表 敵キャラA
  /// </summary>
  std::unordered_map<LoadFilePhase, EnemyA::ActionBarrageType> enemy_a_phase_;

  /// <summary>
  /// 管理表 敵キャラB
  /// </summary>
  std::unordered_map<LoadFilePhase, EnemyB::ActionBarrageType> enemy_b_phase_;

  /// <summary>
  /// ロード時表示文字列
  /// </summary>
  std::string display_string_[kLoadTextMaxSize];

  /// <summary>
  /// プレイヤーグラフィックハンドル
  /// </summary>
  std::vector<int> player_handle_;

  /// <summary>
  /// ロード時表示文字列インデックス
  /// </summary>
  int string_index_;

  /// <summary>
  /// プレイヤーグラフィックハンドル
  /// </summary>
  int handle_index_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// 移動回数
  /// </summary>
  int move_count_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  int alpha_adjust_;

  /// <summary>
  /// フォントハンドル
  /// </summary>
  int font_handle_;

  /// <summary>
  /// アクティブフラグ
  /// </summary>
  bool is_active_;

  /// <summary>
  /// 遷移するレベル
  /// </summary>
  TaskId transition_level_;
};