﻿#include "Game/Player.h"

namespace {

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjustInit = 10;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 透過率 死亡時
  /// </summary>
  const int kAlphaDeath = 175;

  /// <summary>
  /// 拡大率
  /// </summary>
  const float kExtRate = 0.5f;

  /// <summary>
  /// 角度
  /// </summary>
  const float kAngle = 0.0f;

  /// <summary>
  /// リロード時間 プレイヤー
  /// </summary>
  const float kPlayerReload = 0.1f;

  /// <summary>
  /// 核の色
  /// </summary>
  const int kColor = GetColor(255, 0, 0);

  /// <summary>
  /// バトル停止待機時間
  /// </summary>
  const float kBattleStopWaitTime = 0.8f;

  /// <summary>
  /// 死亡演出待機時間
  /// </summary>
  const float kDeathEffectWaitTime = 3.0f;

  /// <summary>
  /// 復活演出待機時間
  /// </summary>
  const float kRebornEffectWaitTime = 0.8f;

  /// <summary>
  /// プレイヤーの状態を戻すまでの待機時間
  /// </summary>
  const float kChangeStateWaitTime = 3.0f;

  /// <summary>
  /// リセット時間
  /// </summary>
  const float kResetTime = 0.0f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name="character_event_interface"> キャラクターイベントインターフェース </param>
/// <returns></returns>
Player::Player(CharacterEventInterface& character_event_interface)
  : Character(TaskId::kPlayer, Character::CharacterType::kPlayer,
              character_event_interface)
  , current_phase_(PhaseType::kInitialize)
  , special_phase_(SpecialPhase::kNone)
  , graphic_handle_(0)
  , alpha_(0)
  , alpha_adjust_(0)
  , is_display_(true)
  , accumulate_time_(0.0f) {

  // コンソールに出力
  std::cout << "Player コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Player::~Player() {

  // コンソールに出力
  std::cout << "~Player デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void Player::Update(float process_time) {

  bool is_finish = false;

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //--------------------------------
    //初期化処理フェーズ
    //--------------------------------
    is_finish = Initialize();
    if (is_finish) {
      // 現在のフェーズを「起動待機」に変更
      ChangeCurrentPhase(PhaseType::kStartWait);
    }
    break;
  }
  case PhaseType::kStartWait: {
    //--------------------------------
    //起動待機フェーズ
    //--------------------------------
    break;
  }
  case PhaseType::kStartUp: {
    //--------------------------------
    //起動フェーズ
    //--------------------------------
    // フェードイン処理完了後、現在のフェーズを「プレイ中」に変更
    is_finish = AdjustAlphaValue();
    if (is_finish) {
      ChangeCurrentPhase(PhaseType::kProcessWait);
    }
    break;
  }
  case PhaseType::kProcessWait: {
    //--------------------------------
    //処理待機フェーズ
    //--------------------------------
    break;
  }
  case PhaseType::kProcess: {
    //--------------------------------
    //処理中フェーズ
    //--------------------------------
    // 発射判定処理
    ReleaseIsShoot(process_time);
    bool is_finish = Process(process_time);
    if (is_finish) {
      // 現在のフェーズを「終了処理前」に変更
      ChangeCurrentPhase(PhaseType::kBeforeFinalize);
    }
    break;
  }
  case PhaseType::kStop: {
    // 停止中処理
    bool is_finish = StopProcess(process_time);
    if (is_finish) {
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kBeforeFinalize: {
    //--------------------------------
    //終了処理前フェーズ
    //--------------------------------
     // フェードアウト処理完了後、現在のフェーズを「終了処理」に変更
    is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //--------------------------------
    //終了処理フェーズ
    //--------------------------------
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Player::Render() {

  // 指定のフェーズ以外なら処理終了
  if (current_phase_ != PhaseType::kStartUp &&
      current_phase_ != PhaseType::kStartWait &&
      current_phase_ != PhaseType::kProcessWait &&
      current_phase_ != PhaseType::kProcess &&
      current_phase_ != PhaseType::kStop &&
      current_phase_ != PhaseType::kStopTime &&
      current_phase_ != PhaseType::kBeforeFinalize) {
    return;
  }

  // 非表示状態になっているなら処理終了
  if (!IsShowState()) {
    return;
  }

  int x_pos = GetPositionX();
  int y_pos = GetPositionY();
  float disp_rate = GetImageDispRate();
  int radius = static_cast<int>(GetRadius());

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

  // 画像の描画
  DrawRotaGraph(x_pos, y_pos, disp_rate, kAngle, graphic_handle_, true);
  // 核の描画
  DrawCircle(x_pos, y_pos, radius, kColor, true);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool Player::Initialize() {

  // 透過率 初期設定
  alpha_ = kAlphaMin;
  alpha_adjust_ = kAlphaAdjustInit;

  // 画像のサイズをセットする
  SetImageSize(graphic_handle_);
  // リロード時間をセット
  SetReloadTime(kPlayerReload);
  // 弾を発射可能にする
  SetIsShoot(true);
  // 生きていることにする
  SetAlive();

  return true;
}

/// <summary>
/// 処理中
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool Player::Process(float process_time) {

  switch (special_phase_) {
  case SpecialPhase::kPlayerReborn: {
    //--------------------------------
    //プレイヤー復活フェーズ
    //--------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kChangeStateWaitTime) {
      // 累積時間をリセットする
      accumulate_time_ = kResetTime;
      // 特殊フェーズを「復活待機」に変更する
      ChangeSpecialPhase(SpecialPhase::kRebornWait);
      // 透過率を設定する
      alpha_ = kAlphaMax;
    }
    break;
  }
  case SpecialPhase::kRebornWait: {
    //--------------------------------
    //復活待機フェーズ
    //--------------------------------
    // 生きていることにする
    SetAlive();
    // 特殊フェーズを「何もしない」に変更する
    ChangeSpecialPhase(SpecialPhase::kNone);
    break;
  }
  }

  return false;
}

/// <summary>
/// 停止中処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool Player::StopProcess(float process_time) {

  switch (special_phase_) {
  case SpecialPhase::kPlayerDeath: {
    //--------------------------------
    //プレイヤー死亡フェーズ
    //--------------------------------
    // 累積時間をリセットする
    accumulate_time_ = kResetTime;
    // 特殊フェーズを「死亡演出」に変更する
    ChangeSpecialPhase(SpecialPhase::kDeathEffect);
    // 弾が撃てないようにセット
    SetIsShoot(false);
    // プレイヤーの位置に円を表示
    character_event_interface_.OnExecuteEffect(EffectId::kPlayerDeath, this);
    break;
  }
  case SpecialPhase::kDeathEffect: {
    //--------------------------------
    //死亡演出フェーズ
    //--------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    // 指定時間待機後、再開の通知を行う
    if (accumulate_time_ >= kBattleStopWaitTime) {
      // 累積時間をリセットする
      accumulate_time_ = kResetTime;
      // イベント通知
      character_event_interface_.OnResumeBattle();
      // プレイヤーを非表示にする
      SetHideState();
      // 特殊フェーズを「死亡待機」に変更する
      ChangeSpecialPhase(SpecialPhase::kDeathWait);
      // プレイヤーの位置で爆発表示
      character_event_interface_.OnExecuteEffect(EffectId::kExplosion, this);
      // プレイヤーが発射した弾を消滅させる
      character_event_interface_.OnFinishDisplayBullet(this);
    }
    break;
  }
  case SpecialPhase::kDeathWait: {
    //--------------------------------
    //死亡待機フェーズ
    //--------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kDeathEffectWaitTime) {
      // 累積時間をリセットする
      accumulate_time_ = kResetTime;

      // 残機を確認し、残機がゼロならゲーム終了処理
      bool is_game_over = character_event_interface_.OnCheckLeftLife();
      if (is_game_over) {
        // 特殊フェーズを「何もしない」に変更
        ChangeSpecialPhase(SpecialPhase::kNone);
        return false;
      }
      // 特殊フェーズを「復活演出」に変更する
      ChangeSpecialPhase(SpecialPhase::kRebornEffect);
      // プレイヤーの位置で爆発表示
      character_event_interface_.OnExecuteEffect(EffectId::kReborn, this);
    }
    break;
  }
  case SpecialPhase::kRebornEffect: {
    //--------------------------------
    //復活演出フェーズ
    //--------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kRebornEffectWaitTime) {
      // 累積時間をリセットする
      accumulate_time_ = kResetTime;
      // プレイヤーを表示にする
      SetShowState();
      // 特殊フェーズを「プレイヤー復活」に変更する
      ChangeSpecialPhase(SpecialPhase::kPlayerReborn);
      // 透過率を設定する
      alpha_ = kAlphaDeath;
      return true;
    }
    break;
  }
  }

  return false;
}

/// <summary>
/// 透過率変更処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool Player::AdjustAlphaValue() {

  bool is_finish = false;

  switch (current_phase_) {
  case PhaseType::kStartUp: {
    // 透過率に調整値を加算
    alpha_ += alpha_adjust_;
    break;
  }
  case PhaseType::kBeforeFinalize: {
    // 透過率に調整値を減算
    alpha_ -= alpha_adjust_;
    break;
  }
  }
  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= kAlphaMax) {
    alpha_ = kAlphaMax;
    is_finish = true;
  }
  else if (alpha_ <= kAlphaMin) {
    alpha_ = kAlphaMin;
    is_finish = true;
  }

  return is_finish;
}

/// <summary>
/// 射撃種類を変更する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Player::ChangeShootType() {

  BarrageId barrage_id = GetBarrageId();
  BarrageId next_barrage_id = BarrageId::kQuadruple;
  switch (barrage_id) {
  case BarrageId::kSingle: {
    next_barrage_id = BarrageId::kDouble;
    break;
  }
  case BarrageId::kDouble: {
    next_barrage_id = BarrageId::kTriple;
    break;
  }
  case BarrageId::kTriple: {
    next_barrage_id = BarrageId::kQuadruple;
    break;
  }
  }
  SetBarrageId(next_barrage_id);
}

/// <summary>
/// 時間停止からの復帰処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Player::ReturnTimeStop() {

  switch (special_phase_) {
  case SpecialPhase::kNone:
  case SpecialPhase::kPlayerReborn:
  case SpecialPhase::kRebornWait: {

    // 現在のフェーズを「処理中」に変更
    ChangeCurrentPhase(PhaseType::kProcess);
    break;
  }
  case SpecialPhase::kPlayerDeath:
  case SpecialPhase::kDeathEffect:
  case SpecialPhase::kDeathWait:
  case SpecialPhase::kRebornEffect: {

    // 現在のフェーズを「停止」に変更
    ChangeCurrentPhase(PhaseType::kStop);
    break;
  }
  }
}