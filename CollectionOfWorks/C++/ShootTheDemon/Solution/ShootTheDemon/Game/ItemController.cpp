﻿#include "Game/ItemController.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ItemController::ItemController(CharacterControllerEventInterface& character_controller_event_interface,
                               Character* character)
  : character_controller_event_interface_(character_controller_event_interface)
  , character_(character) {

  // コンソールに出力
  std::cout << "ItemController コンストラクタ " << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ItemController::~ItemController() {

  // コンソールに出力
  std::cout << "~ItemController デストラクタ " << std::endl;
}

/// <summary>
/// 下に移動する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ItemController::MoveDown() {

  character_controller_event_interface_.OnPushDownKey(character_, false);
}

