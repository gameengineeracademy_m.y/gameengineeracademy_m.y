﻿#include "Game/TitleLevel.h"

namespace {

  /// <summary>
  /// フォントファイルパス
  /// </summary>
  const char* kFontFilePath = "Asset/Font/ReggaeOne-Regular.ttf";

  /// <summary>
  /// サウンドデータファイル
  /// </summary>
  const char* kSoundDataFilePath = "File/SoundData.txt";

  /// <summary>
  /// ボタン選択音源パス
  /// </summary>
  const char* kSoundButtonSelectPath = "Asset/Sound/maou_se_system45.mp3";

  /// <summary>
  /// ボタン押下音源パス
  /// </summary>
  const char* kSoundButtonPushPath = "Asset/Sound/game_start.mp3";

  /// <summary>
  /// ゲームスタート音源パス
  /// </summary>
  const char* kSoundGameStartPath = "Asset/Sound/maou_se_magic_water07.mp3";
  
  /// <summary>
  /// BGMファイルパス
  /// </summary>
  const char* kSoundTitleBgmFilePath = "Asset/Sound/";

  /// <summary>
  /// バトルBGM音源パス
  /// </summary>
  const char* kSoundBattleBgmPath = "Asset/Sound/maou_bgm_orchestra12.mp3";

  /// <summary>
  /// サウンドボリューム初期値
  /// </summary>
  const int kSoundInitialVolume = 128;

  /// <summary>
  /// サウンドボリューム調整値
  /// </summary>
  const int kSoundAdjustVolume = 10;

  /// <summary>
  /// 累積時間のリセット値
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 待機時間
  /// </summary>
  const float kWaitTime = 1.0f;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSize = 25;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 1;

  /// <summary>
  /// 文字の色
  /// </summary>
  const int kStringForeColor = GetColor(255, 255, 255);   // 白色

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 画像読み込み時のエラー値
  /// </summary>
  const int kLoadError = -1;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjustInit = 5;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 背景 表示位置 X座標
  /// </summary>
  const int kBackPosX = 224;

  /// <summary>
  /// 背景 表示位置 Y座標
  /// </summary>
  const int kBackPosY = 0;

  /// <summary>
  /// 背景 幅
  /// </summary>
  const int kBackWidth = 576;

  /// <summary>
  /// 背景 高さ
  /// </summary>
  const int kBackHeight = 768;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> タスクID </param>
/// <param name=""> タスクマネージャ </param>
/// <returns></returns>
TitleLevel::TitleLevel(TaskId task_id, TaskManager& task_manager)
  : Level(task_id, task_manager)
  , current_phase_(PhaseType::kLoadFile)
  , sound_phase_(RegisterSoundPhase::kButtonSelect)
  , finalize_phase_(FinalizePhaseType::kUnloadTask)
  , info_ui_(nullptr)
  , menu_controller_(nullptr)
  , sound_manager_(nullptr)
  , sound_setup_(nullptr)
  , volume_controller_(nullptr)
  , score_(nullptr)
  , sound_volume_list_()
  , accumulate_time_(0.0f)
  , is_game_end_(false)
  , alpha_(0)
  , alpha_adjust_(0) {

  // コンソールに出力
  std::cout << "TitleLevel コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
TitleLevel::~TitleLevel() {

  // コンソールに出力
  std::cout << "~TitleLevel デストラクタ" << std::endl;
}

/// <summary>
/// 初期化処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool TitleLevel::InitializePhase(float process_time) {

  bool is_success = false;

  // 処理時間をリセットしておく
  accumulate_time_ = kResetTime;
  // 透過率 初期設定
  alpha_ = kAlphaMin;
  alpha_adjust_ = kAlphaAdjustInit;

  // フォント情報生成
  FontInfo* font_info = FontInfo::GetFontInfoInstance();
  if (font_info == nullptr) {
    return false;
  }
  int result = font_info->CreateFontInfo(kFontFilePath);

  // UI情報 初期設定
  is_success = InitializeTitleInfoUi();
  if (!is_success) {
    return false;
  }

  // メニューコントローラ 初期設定
  is_success = InitializeMenuController();
  if (!is_success) {
    return false;
  }

  // サウンドマネージャ 初期設定
  is_success = InitializeSoundManager();
  if (!is_success) {
    return false;
  }

  // サウンド設定 初期設定
  is_success = InitializeSoundSetup();
  if (!is_success) {
    return false;
  }

  // ボリュームコントローラ 初期設定
  is_success = InitializeVolumeController();
  if (!is_success) {
    return false;
  }

  // スコア 初期設定
  is_success = InitializeScore();
  if (!is_success) {
    return false;
  }

  return true;
}

/// <summary>
/// 毎フレームの処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool TitleLevel::UpdatePhase(float process_time) {

  switch (current_phase_) {
  case PhaseType::kLoadFile: {
    //-------------------------------
    //準備フェーズ
    //-------------------------------
    bool is_finish = LoadSetupFile();
    if (is_finish) {
      // UI情報の現在のフェーズを「初期化処理」に変更
      info_ui_->ChangeCurrentPhase(TitleInfoUi::PhaseType::kInitialize);
      // 現在のフェーズを「準備」に変更する
      ChangeCurrentPhase(PhaseType::kPrepare);
    }
    break;
  }
  case PhaseType::kPrepare: {
    //-------------------------------
    //準備フェーズ
    //-------------------------------
    bool is_success = Prepare();
    if (is_success) {
      // UI情報の現在のフェーズを「初期化処理」に変更
      info_ui_->ChangeCurrentPhase(TitleInfoUi::PhaseType::kStartUp);
      // 現在のフェーズを「起動」に変更する
      ChangeCurrentPhase(PhaseType::kStartUp);
    }
    break;
  }
  case PhaseType::kStartUp: {
    //-------------------------------
    //起動フェーズ
    //-------------------------------
    // フェードイン処理完了後、現在のフェーズを「処理中」に変更
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // UI情報のカーソルを表示する
      info_ui_->ShowCursor();
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess:
  case PhaseType::kSoundSetting: {
    //-------------------------------
    //処理中/音量設定フェーズ
    //-------------------------------
    break;
  }
  case PhaseType::kBeforeTransition: {
    //-------------------------------
    //遷移前フェーズ
    //-------------------------------
    // UI情報の現在のフェーズを「終了処理前」に変更
    info_ui_->ChangeCurrentPhase(TitleInfoUi::PhaseType::kBeforeFinalize);
    // 現在のフェーズを「レベル遷移」に変更する
    ChangeCurrentPhase(PhaseType::kLevelTransition);
    break;
  }
  case PhaseType::kLevelTransition: {
    //-------------------------------
    //レベル遷移フェーズ
    //-------------------------------
    // フェードアウト処理完了後、現在のフェーズを「終了処理」に変更
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更する
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //-------------------------------
    //終了処理フェーズ
    //-------------------------------
    // バトルレベルへ切り替える
    ChangeLevel(TaskId::kBattleLevel);
    // 現在のフェーズを「終了処理済み」に変更する
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }

  return is_game_end_;
}

/// <summary>
/// 毎フレームの描画処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleLevel::RenderPhase() {

}

/// <summary>
/// 終了処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool TitleLevel::FinishPhase(float process_time) {

  switch (finalize_phase_) {
  case FinalizePhaseType::kUnloadTask: {
    //-------------------------------
    //タスクを降ろすフェーズ
    //-------------------------------
    // フォント情報生成
    FontInfo* font_info = FontInfo::GetFontInfoInstance();
    if (font_info == nullptr) {
      return false;
    }
    // フォント破棄
    font_info->ReleaseFontInfo(kFontFilePath);

    // サウンドの破棄
    if (sound_manager_ != nullptr) {
      sound_manager_->DisposeSound();
    }

    // タスクIDを取得
    TaskId task_id;
    Task* release_task = nullptr;

    // UI情報をタスクマネージャから降ろす
    if (info_ui_ != nullptr) {
      task_id = info_ui_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // メニューコントローラをタスクマネージャから降ろす
    if (menu_controller_ != nullptr) {
      task_id = menu_controller_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // サウンドマネージャをタスクマネージャから降ろす
    if (sound_manager_ != nullptr) {
      task_id = sound_manager_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // サウンド設定をタスクマネージャから降ろす
    if (sound_setup_ != nullptr) {
      task_id = sound_setup_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // ボリュームコントローラをタスクマネージャから降ろす
    if (volume_controller_ != nullptr) {
      task_id = volume_controller_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // スコアをタスクマネージャから降ろす
    if (score_ != nullptr) {
      task_id = score_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }

    // 終了フェーズをタスク破棄フェーズに変更
    ChangeFinalizePhase(FinalizePhaseType::kDisposeTask);

    return false;
  }
  case FinalizePhaseType::kDisposeTask: {
    //-------------------------------
    //タスクを破棄するフェーズ
    //-------------------------------
    if (score_ != nullptr) {
      delete score_;
      score_ = nullptr;
    }
    if (volume_controller_ != nullptr) {
      delete volume_controller_;
      volume_controller_ = nullptr;
    }
    if (sound_setup_ != nullptr) {
      delete sound_setup_;
      sound_setup_ = nullptr;
    }
    if (sound_manager_ != nullptr) {
      delete sound_manager_;
      sound_manager_ = nullptr;
    }
    if (menu_controller_ != nullptr) {
      delete menu_controller_;
      menu_controller_ = nullptr;
    }
    if (info_ui_ != nullptr) {
      delete info_ui_;
      info_ui_ = nullptr;
    }
    break;
  }
  }

  return true;
}

/// <summary>
/// 透過率変更処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool TitleLevel::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率の調整
  switch (current_phase_) {
  case PhaseType::kStartUp: {
    // 起動フェーズ
    alpha_ += alpha_adjust_;
    break;
  }
  case PhaseType::kLevelTransition: {
    // 終了処理前フェーズ
    alpha_ -= alpha_adjust_;
    break;
  }
  }

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= kAlphaMax) {
    alpha_ = kAlphaMax;
    is_finish = true;
  }
  else if (alpha_ <= kAlphaMin) {
    alpha_ = kAlphaMin;
    is_finish = true;
  }

  return is_finish;
}

/// <summary>
/// タイトルUI情報 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool TitleLevel::InitializeTitleInfoUi() {

  // UI情報 生成
  info_ui_ = new TitleInfoUi();
  if (info_ui_ == nullptr) {
    std::cout << "BattleLevel InitializeTitleInfoUi：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // タスクマネージャに積む
  task_manager_.AddTask(info_ui_);

  return true;
}

/// <summary>
/// メニューコントローラ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool TitleLevel::InitializeMenuController() {

  // メニューコントローラの生成
  menu_controller_ = new MenuController(*this);
  if (menu_controller_ == nullptr) {
    std::cout << "TitleLevel InitializeMenuController：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // タスクマネージャにタスクを積む
  task_manager_.AddTask(menu_controller_);

  return true;
}

/// <summary>
/// サウンドマネージャ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool TitleLevel::InitializeSoundManager() {

  // サウンドマネージャ 生成
  sound_manager_ = new SoundManager();
  if (sound_manager_ == nullptr) {
    std::cout << "TitleLevel InitializeSoundManager：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // タスクマネージャに積む
  task_manager_.AddTask(sound_manager_);

  return true;
}

/// <summary>
/// サウンド設定処理 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool TitleLevel::InitializeSoundSetup() {

  // サウンド設定処理 生成
  sound_setup_ = new SoundSetup(*this);
  if (sound_setup_ == nullptr) {
    std::cout << "TitleLevel InitializeSoundSetup：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  return true;
}

/// <summary>
/// ボリュームコントローラ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool TitleLevel::InitializeVolumeController() {

  // ボリュームコントローラ 生成
  volume_controller_ = new VolumeController(*this);
  if (volume_controller_ == nullptr) {
    std::cout << "TitleLevel InitializeVolumeController：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  return true;
}

/// <summary>
/// スコア表 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool TitleLevel::InitializeScore() {

  // スコア表 生成
  score_ = new Score();
  if (score_ == nullptr) {
    std::cout << "TitleLevel InitializeScore：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  return true;
}

/// <summary>
/// 準備処理
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool TitleLevel::Prepare() {

  switch (sound_phase_) {
  case RegisterSoundPhase::kButtonSelect: {
    //---------------------------------------------
    //ボタン選択音登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundButtonSelectPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return false;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kButtonSelect, Sound::SoundType::kSoundEffect);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kSoundEffect]);
      // サウンドハンドル、総再生時間、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「ボタン押下の登録」に変更
    ChangeSoundPhase(RegisterSoundPhase::kButtonPush);
    break;
  }
  case RegisterSoundPhase::kButtonPush: {
    //---------------------------------------------
    //ボタン押下音登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundButtonPushPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return false;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kButtonPush, Sound::SoundType::kSoundEffect);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kSoundEffect]);
      // サウンドハンドル、総再生時間、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「ゲームスタート音登録」に変更
    ChangeSoundPhase(RegisterSoundPhase::kGameStart);
    break;
  }
  case RegisterSoundPhase::kGameStart: {
    //---------------------------------------------
    //ゲームスタート音登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundGameStartPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return false;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kGameStart, Sound::SoundType::kSoundEffect);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kSoundEffect]);
      // サウンドハンドル、総再生時間、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「BGM(テスト用)登録」に変更
    ChangeSoundPhase(RegisterSoundPhase::kTestBgm);
    break;
  }
  case RegisterSoundPhase::kTestBgm: {
    //---------------------------------------------
    //BGM(テスト用)登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundBattleBgmPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return false;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kBattleBgmTest, Sound::SoundType::kBackGroundMusic);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kBackGroundMusic]);
      // サウンドハンドル、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }

    // 音楽登録フェーズを「終了処理」に変更
    ChangeSoundPhase(RegisterSoundPhase::kFinish);
    break;
  }
  case RegisterSoundPhase::kFinish: {
    //---------------------------------------------
    //終了処理
    //---------------------------------------------
    return true;
  }
  }

  return false;
}

/// <summary>
/// Enterキーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleLevel::OnPushMenuControllerEnterKey() {

  switch (current_phase_) {
  case PhaseType::kProcess: {
    // 決定操作
    DecideOperation();
    break;
  }
  case PhaseType::kSoundSetting: {
    // 現在選択しているボタンを取得
    SoundSetup::ButtonType button_type = sound_setup_->GetButtonType();
    switch (button_type) {
    case SoundSetup::ButtonType::kDone:
    case SoundSetup::ButtonType::kReturn: {
      // 決定操作
      DecideSoundMenu();
      // 決定サウンド
      sound_manager_->CreateSound(SoundId::kButtonPush);
      break;
    }
    }
    break;
  }
  }
}

/// <summary>
/// Escapeキーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleLevel::OnPushMenuControllerEscapeKey() {

  switch (current_phase_) {
  case PhaseType::kSoundSetting: {
    //------------------------------
    //音量設定
    //------------------------------
    // BGMの停止処理
    OnDisposeSoundBGM();

    // サウンド設定、ボリュームコントローラをタスクマネージャから降ろす
    TaskId task_id = sound_setup_->GetTaskId();
    task_manager_.ReleaseTask(task_id);
    task_id = volume_controller_->GetTaskId();
    task_manager_.ReleaseTask(task_id);

    // UI情報のフェーズを変更
    info_ui_->ChangeCurrentPhase(TitleInfoUi::PhaseType::kProcess);

    // 現在のフェーズを「処理中」に変更
    ChangeCurrentPhase(PhaseType::kProcess);
    break;
  }
  case PhaseType::kScore: {
    //------------------------------
    //スコア
    //------------------------------
    // スコアをタスクマネージャから降ろす
    TaskId task_id = score_->GetTaskId();
    task_manager_.ReleaseTask(task_id);

    // UI情報のフェーズを変更
    info_ui_->ChangeCurrentPhase(TitleInfoUi::PhaseType::kProcess);

    // 現在のフェーズを「処理中」に変更
    ChangeCurrentPhase(PhaseType::kProcess);
    break;
  }
  }
}

/// <summary>
/// Xキーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleLevel::OnPushMenuControllerX() {

  switch (current_phase_) {
  case PhaseType::kSoundSetting: {
    //------------------------------
    //音量設定
    //------------------------------
    // BGMの停止処理
    OnDisposeSoundBGM();

    // サウンド設定、ボリュームコントローラをタスクマネージャから降ろす
    TaskId task_id = sound_setup_->GetTaskId();
    task_manager_.ReleaseTask(task_id);
    task_id = volume_controller_->GetTaskId();
    task_manager_.ReleaseTask(task_id);

    // UI情報のフェーズを変更
    info_ui_->ChangeCurrentPhase(TitleInfoUi::PhaseType::kProcess);

    // 現在のフェーズを「処理中」に変更
    ChangeCurrentPhase(PhaseType::kProcess);
    break;
  }
  case PhaseType::kScore: {
    //------------------------------
    //スコア
    //------------------------------
    // スコアをタスクマネージャから降ろす
    TaskId task_id = score_->GetTaskId();
    task_manager_.ReleaseTask(task_id);

    // UI情報のフェーズを変更
    info_ui_->ChangeCurrentPhase(TitleInfoUi::PhaseType::kProcess);

    // 現在のフェーズを「処理中」に変更
    ChangeCurrentPhase(PhaseType::kProcess);
    break;
  }
  }
}

/// <summary>
/// Zキーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleLevel::OnPushMenuControllerZ() {

  switch (current_phase_) {
  case PhaseType::kProcess: {
    // 決定操作
    DecideOperation();
    break;
  }
  case PhaseType::kSoundSetting: {
    // 現在選択しているボタンを取得
    SoundSetup::ButtonType button_type = sound_setup_->GetButtonType();
    switch (button_type) {
    case SoundSetup::ButtonType::kDone:
    case SoundSetup::ButtonType::kReturn: {
      // 決定操作
      DecideSoundMenu();
      // 決定サウンド
      sound_manager_->CreateSound(SoundId::kButtonPush);
      break;
    }
    }
    break;
  }
  }
}

/// <summary>
/// 上矢印キーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleLevel::OnPushMenuControllerUp() {

  switch (current_phase_) {
  case PhaseType::kProcess: {

    info_ui_->OperationCursorUp();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonSelect);
    break;
  }
  case PhaseType::kSoundSetting: {

    // 選択操作
    sound_setup_->OperationCursorUp();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonSelect);
    break;
  }
  }
}

/// <summary>
/// 下矢印キーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleLevel::OnPushMenuControllerDown() {

  switch (current_phase_) {
  case PhaseType::kProcess: {

    info_ui_->OperationCursorDown();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonSelect);
    break;
  }
  case PhaseType::kSoundSetting: {

    // 選択操作
    sound_setup_->OperationCursorDown();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonSelect);
    break;
  }
  }
}

/// <summary>
/// 左矢印キーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleLevel::OnPushMenuControllerLeft() {

  if (current_phase_ != PhaseType::kSoundSetting) {
    return;
  }

  // 現在選択しているボタンを取得
  SoundSetup::ButtonType button_type = sound_setup_->GetButtonType();
  switch (button_type) {
  case SoundSetup::ButtonType::kDone:
  case SoundSetup::ButtonType::kReturn: {
    // 選択操作
    sound_setup_->OperationCursorLeft();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonSelect);
    break;
  }
  }
}

/// <summary>
/// 右矢印キーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleLevel::OnPushMenuControllerRight() {

  if (current_phase_ != PhaseType::kSoundSetting) {
    return;
  }

  // 現在選択しているボタンを取得
  SoundSetup::ButtonType button_type = sound_setup_->GetButtonType();
  switch (button_type) {
  case SoundSetup::ButtonType::kDone:
  case SoundSetup::ButtonType::kReturn: {
    // 選択操作
    sound_setup_->OperationCursorRight();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonSelect);
    break;
  }
  }
}

/// <summary>
/// 右矢印キーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleLevel::OnPushVolumeControllerRight() {

  if (current_phase_ != PhaseType::kSoundSetting) {
    return;
  }

  // 現在選択しているボタンを取得
  SoundSetup::ButtonType button_type = sound_setup_->GetButtonType();
  switch (button_type) {
  case SoundSetup::ButtonType::kBackGroundMusic: {
    // 選択操作
    sound_setup_->OperationCursorRight();
    // 指定サウンドを取得
    int list_id = sound_setup_->GetBgmListId();
    Sound* sound = sound_manager_->GetSoundBgm(list_id);
    if (sound == nullptr) {
      break;
    }
    // ボリュームを変更
    int volume = static_cast<int>(sound_setup_->GetSoundVolumeBgm());
    sound->SetSoundVolume(volume);
    sound->ChangeSoundVolume();
    break;
  }
  case SoundSetup::ButtonType::kSoundEffect: {
    // 選択操作
    sound_setup_->OperationCursorRight();
    // 選択サウンド
    Sound* sound = sound_manager_->CreateSound(SoundId::kButtonSelect);
    if (sound == nullptr) {
      break;
    }
    // ボリュームを変更
    int volume = static_cast<int>(sound_setup_->GetSoundVolumeSe());
    sound->SetSoundVolume(volume);
    sound->ChangeSoundVolume();
    break;
  }
  }

}

/// <summary>
/// 左矢印キーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleLevel::OnPushVolumeControllerLeft() {

  if (current_phase_ != PhaseType::kSoundSetting) {
    return;
  }

  // 現在選択しているボタンを取得
  SoundSetup::ButtonType button_type = sound_setup_->GetButtonType();
  switch (button_type) {
  case SoundSetup::ButtonType::kBackGroundMusic: {
    // 選択操作
    sound_setup_->OperationCursorLeft();
    // 指定サウンドを取得
    int list_id = sound_setup_->GetBgmListId();
    Sound* sound = sound_manager_->GetSoundBgm(list_id);
    if (sound == nullptr) {
      break;
    }
    // ボリュームを変更
    int volume = static_cast<int>(sound_setup_->GetSoundVolumeBgm());
    sound->SetSoundVolume(volume);
    sound->ChangeSoundVolume();
    break;
  }
  case SoundSetup::ButtonType::kSoundEffect: {
    // 選択操作
    sound_setup_->OperationCursorLeft();
    // 選択サウンド
    Sound* sound = sound_manager_->CreateSound(SoundId::kButtonSelect);
    if (sound == nullptr) {
      break;
    }
    // ボリュームを変更
    int volume = static_cast<int>(sound_setup_->GetSoundVolumeSe());
    sound->SetSoundVolume(volume);
    sound->ChangeSoundVolume();
    break;
  }
  }
}

/// <summary>
/// BGMの再生
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleLevel::OnPlaySoundBGM() {

  Sound* sound = sound_manager_->CreateSound(SoundId::kBattleBgmTest);
  if (sound == nullptr) {
    return;
  }
  int list_id = sound->GetSoundListId();
  sound_setup_->SetBgmListId(list_id);
  // ボリュームを変更
  int volume = static_cast<int>(sound_setup_->GetSoundVolumeBgm());
  sound->SetSoundVolume(volume);
  sound->ChangeSoundVolume();
}

/// <summary>
/// BGMの破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleLevel::OnDisposeSoundBGM() {

  int list_id = sound_setup_->GetBgmListId();
  Sound* sound = sound_manager_->GetSoundBgm(list_id);
  if (sound == nullptr) {
    return;
  }
  sound->ChangeCurrentPhase(Sound::PhaseType::kFinish);
}

/// <summary>
/// 決定操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleLevel::DecideOperation() {

  TitleInfoUi::ButtonType button_type = info_ui_->GetButtonType();
  switch (button_type) {
  case TitleInfoUi::ButtonType::kGameStart: {
    //------------------------------
    //ゲームスタート
    //------------------------------
    // 現在のフェーズを「終了処理前」に変更
    ChangeCurrentPhase(PhaseType::kBeforeTransition);
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kGameStart);
    break;
  }
  case TitleInfoUi::ButtonType::kOption: {
    //------------------------------
    //音量設定
    //------------------------------
    // UI情報のフェーズを変更
    info_ui_->ChangeCurrentPhase(TitleInfoUi::PhaseType::kSoundSetting);

    // サウンド設定、ボリュームコントローラをタスクマネージャに積む
    task_manager_.AddTask(sound_setup_);
    task_manager_.AddTask(volume_controller_);

    sound_setup_->ChangeCurrentPhase(SoundSetup::PhaseType::kInitialize);

    // サウンドボリュームを渡す
    sound_setup_->SetSoundVolumeBgm(sound_volume_list_[Sound::SoundType::kBackGroundMusic]);
    sound_setup_->SetSoundVolumeSe(sound_volume_list_[Sound::SoundType::kSoundEffect]);

    // ゲーム画面を暗くするための背景のサイズをセット
    sound_setup_->SetBackPosAndSize(kBackWidth, kBackHeight, kBackPosX, kBackPosY);

    // 現在のフェーズを「音量設定」に変更
    ChangeCurrentPhase(PhaseType::kSoundSetting);

    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonPush);
    break;
  }
  case TitleInfoUi::ButtonType::kScore: {
    //------------------------------
    //スコア
    //------------------------------
    // UI情報のフェーズを変更
    info_ui_->ChangeCurrentPhase(TitleInfoUi::PhaseType::kScore);

    // スコアをタスクマネージャに積む
    task_manager_.AddTask(score_);
    score_->ChangeCurrentPhase(Score::PhaseType::kInitialize);
    score_->ChangePreparePhase(Score::PreparePhase::kSetRanking);

    // 現在のフェーズを「スコア」に変更
    ChangeCurrentPhase(PhaseType::kScore);

    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonPush);
    break;
  }
  case TitleInfoUi::ButtonType::kGameEnd: {
    //------------------------------
    //ゲーム終了
    //------------------------------
    is_game_end_ = true;

    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonPush);
    break;
  }
  }
}

/// <summary>
/// サウンドメニュー決定操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void TitleLevel::DecideSoundMenu() {

  // 現在選択しているボタンを取得
  SoundSetup::ButtonType button_type = sound_setup_->GetButtonType();
  switch (button_type) {
  case SoundSetup::ButtonType::kDone: {
    // サウンドボリュームを取得
    float volume_bgm = sound_setup_->GetSoundVolumeBgm();
    float volume_se = sound_setup_->GetSoundVolumeSe();

    sound_volume_list_[Sound::SoundType::kBackGroundMusic] = volume_bgm;
    sound_volume_list_[Sound::SoundType::kSoundEffect] = volume_se;

    // サウンド全てに設定を反映
    sound_manager_->ChangeSoundVolumeBgm(static_cast<int>(volume_bgm));
    sound_manager_->ChangeSoundVolumeSe(static_cast<int>(volume_se));

    // ファイルに書き込む
    std::ofstream stream(kSoundDataFilePath, std::ios::out);
    std::string text = "BGMSoundVolume=" + std::to_string(volume_bgm);
    stream << text << std::endl;
    text = "SESoundVolume=" + std::to_string(volume_se);
    stream << text << std::endl;
    //ファイルを閉じる
    stream.close();
    break;
  }
  }

  // BGMの停止処理
  OnDisposeSoundBGM();

  // サウンド設定、ボリュームコントローラをタスクマネージャから降ろす
  TaskId task_id = sound_setup_->GetTaskId();
  task_manager_.ReleaseTask(task_id);
  task_id = volume_controller_->GetTaskId();
  task_manager_.ReleaseTask(task_id);

  // UI情報のフェーズを変更
  info_ui_->ChangeCurrentPhase(TitleInfoUi::PhaseType::kProcess);

  // 現在のフェーズを「処理中」に変更
  ChangeCurrentPhase(PhaseType::kProcess);
}

/// <summary>
/// 設定ファイル読み込み
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool TitleLevel::LoadSetupFile() {

  //-----------------------------------------
  //サウンドファイル
  //-----------------------------------------
  std::string file_path = kSoundDataFilePath;
  // ファイルをロードし、データを取得する
  bool is_finish = LoadFileSoundData(file_path);
  if (!is_finish) {
    return false;
  }

  return true;
}

/// <summary>
/// ロードファイル サウンドデータ
/// </summary>
/// <param name=""> ファイルパス </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool TitleLevel::LoadFileSoundData(std::string file_path) {

  // ファイルストリームを宣言と同時にファイルを開く
  std::ifstream stream(file_path, std::ios::in);
  //ファイルが開けないならエラー
  if (!stream) {
    // リストに値をセット
    sound_volume_list_[Sound::SoundType::kBackGroundMusic] = kSoundInitialVolume;
    sound_volume_list_[Sound::SoundType::kSoundEffect] = kSoundInitialVolume;
    // ファイルストリームを宣言と同時にファイルを開く
    std::ofstream stream(file_path, std::ios::out);
    std::string text = "BGMSoundVolume=" + std::to_string(kSoundInitialVolume);
    stream << text << std::endl;
    text = "SESoundVolume=" + std::to_string(kSoundInitialVolume);
    stream << text << std::endl;
    //ファイルを閉じる
    stream.close();
    return true;
  }

  //ファイルから読み込む
  std::string line;
  //１行ずつ読み出す
  while (std::getline(stream, line)) {
    std::cout << line << std::endl;
    int equal_pos = 0;
    if (line.find("BGMSoundVolume=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      sound_volume_list_[Sound::SoundType::kBackGroundMusic] = std::stof(str_value);
    }
    else if (line.find("SESoundVolume=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      sound_volume_list_[Sound::SoundType::kSoundEffect] = std::stof(str_value);
    }
  }

  //ファイルを閉じる
  stream.close();
  line.clear();
  stream.clear();

  return true;
}