﻿#include "Game/HitPointBarManager.h"

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
HitPointBarManager::HitPointBarManager()
  : Task(TaskId::kHitPointBarManager)
  , current_phase_(PhaseType::kProcess)
  , hit_point_bar_list_()
  , generate_hit_point_bar_list_() {

  // コンソールに出力
  std::cout << "HitPointBarManager コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
HitPointBarManager::~HitPointBarManager() {

  // コンソールに出力
  std::cout << "~HitPointBarManager デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void HitPointBarManager::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kProcess: {
    //----------------------------------------
    //処理中フェーズ
    //----------------------------------------
    bool is_finish = false;
    std::vector<int> release_list;

    //ヒットポイントバーの更新処理
    for (auto hp_bar : generate_hit_point_bar_list_) {
      is_finish = hp_bar.second->Update(process_time);
      if (is_finish) {
        release_list.push_back(hp_bar.first);
      }
    }

    // 処理終了済みのヒットポイントバーをリストから削除
    for (auto index : release_list) {
      HitPointBar* hit_point_bar = generate_hit_point_bar_list_[index];
      generate_hit_point_bar_list_.erase(index);
      if (hit_point_bar != nullptr) {
        delete hit_point_bar;
        hit_point_bar = nullptr;
      }
    }

    break;
  }
  case PhaseType::kStop: {
    //----------------------------------------
    //停止フェーズ
    //----------------------------------------
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void HitPointBarManager::Render() {

  bool is_finish = false;
  //----------------------------------------
  //ヒットポイントバーの描画処理
  //----------------------------------------
  for (auto hp_bar : generate_hit_point_bar_list_) {
    hp_bar.second->Render();
  }
}

/// <summary>
/// ヒットポイントバーのリスト登録
/// </summary>
/// <param name="hp_bar_id"> ヒットポイントバーID </param>
/// <returns> ヒットポイントバー </returns>
HitPointBar* HitPointBarManager::RegisterHitPointBar(HitPointBarId hp_bar_id) {

  HitPointBar* hp_bar = nullptr;
  // 指定のヒットポイントバーIDがヒットポイントバーリストに存在するか確認
  // 存在しない場合はヒットポイントバーを生成してリストに登録
  if (hit_point_bar_list_.find(hp_bar_id) != hit_point_bar_list_.end()) {
    return nullptr;
  }
  hp_bar = new HitPointBar(hp_bar_id);
  if (hp_bar == nullptr) {
    return nullptr;
  }
  hit_point_bar_list_[hp_bar_id] = hp_bar;
  return hp_bar;
}

/// <summary>
/// ヒットポイントバーの生成
/// </summary>
/// <param name="hp_bar_id"> ヒットポイントバーID </param>
/// <returns> ヒットポイントバー </returns>
HitPointBar* HitPointBarManager::CreateHitPointBar(HitPointBarId hp_bar_id) {

  HitPointBar* hp_bar = nullptr;
  // リストに存在しないHPバーなら処理終了
  // リストに存在するならHPバーを生成し、生成リストに追加する
  if (hit_point_bar_list_.find(hp_bar_id) == hit_point_bar_list_.end()) {
    return nullptr;
  }

  hp_bar = hit_point_bar_list_[hp_bar_id]->GenerateClone();
  if (hp_bar == nullptr) {
    return nullptr;
  }

  int index = 0;
  // 生成リストの空きインデックスを調べ、生成リストに登録
  while (true) {
    if (generate_hit_point_bar_list_.find(index) == generate_hit_point_bar_list_.end()) {
      generate_hit_point_bar_list_[index] = hp_bar;
      hp_bar->SetListId(index);
      break;
    }
    ++index;
  }

  return hp_bar;
}

/// <summary>
/// 指定のヒットポイントバーの終了
/// </summary>
/// <param name=""> リストID </param>
/// <returns></returns>
void HitPointBarManager::FinishHitPointBar(int list_id) {

  // 生成リストに存在しない場合は処理終了
  if (generate_hit_point_bar_list_.find(list_id) == generate_hit_point_bar_list_.end()) {
    return;
  }

  // 指定のHPバーを取得し、終了状態にする
  HitPointBar* hp_bar = generate_hit_point_bar_list_[list_id];
  if (hp_bar == nullptr) {
    return;
  }
  if (hp_bar->IsFinish()) {
    return;
  }
  hp_bar->SetFinish();
}

/// <summary>
/// 全てのヒットポイントバーの終了処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void HitPointBarManager::FinishAllHitPointBar() {

  // 生成リスト内のHPバーを全て終了にする
  if (!generate_hit_point_bar_list_.empty()) {
    for (auto hp_bar : generate_hit_point_bar_list_) {
      if (hp_bar.second == nullptr) {
        continue;
      }
      if (hp_bar.second->IsFinish()) {
        continue;
      }
      hp_bar.second->SetFinish();
    }
  }
}

/// <summary>
/// ヒットポイントバーリストの破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void HitPointBarManager::DisposeHitPointBar() {

  std::vector<int> release_index;
  std::vector<HitPointBarId> release_id;

  // 生成リストに要素が残っている場合はすべてリストから降ろし、破棄する
  if (!generate_hit_point_bar_list_.empty()) {
    // 生成リスト内のインデックスをすべて取得
    for (auto hp_bar : generate_hit_point_bar_list_) {
      release_index.push_back(hp_bar.first);
    }
    // 生成リストから降ろし、ヒットポイントバーを破棄する
    for (auto index : release_index) {
      HitPointBar* hp_bar = generate_hit_point_bar_list_[index];
      generate_hit_point_bar_list_.erase(index);
      if (hp_bar != nullptr) {
        delete hp_bar;
        hp_bar = nullptr;
      }
    }
  }
  // インデックスリストクリア
  release_index.clear();

  // バレッジリストの破棄
  if (!hit_point_bar_list_.empty()) {
    // リスト内のインデックスをすべて取得
    for (auto hp_bar : hit_point_bar_list_) {
      release_id.push_back(hp_bar.first);
    }
    // バレッジリストから降ろし、バレッジを破棄する
    for (auto index : release_id) {
      HitPointBar* hp_bar = hit_point_bar_list_[index];
      hit_point_bar_list_.erase(index);
      if (hp_bar != nullptr) {
        delete hp_bar;
        hp_bar = nullptr;
      }
    }
  }
  // IDリストクリア
  release_id.clear();
}