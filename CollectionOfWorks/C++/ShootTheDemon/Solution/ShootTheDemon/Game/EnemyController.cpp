﻿#include "Game/EnemyController.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""> キャラクターコントローライベントインターフェース </param>
/// <param name=""> キャラクター </param>
/// <returns></returns>
EnemyController::EnemyController(CharacterControllerEventInterface& character_controller_event_interface,
                                 Character* character)
  : character_controller_event_interface_(character_controller_event_interface)
  , character_(character) {

  // コンソールに出力
  std::cout << "EnemyController コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
EnemyController::~EnemyController() {

  // コンソールに出力
  std::cout << "~EnemyController デストラクタ" << std::endl;
}

/// <summary>
/// 上に移動する
/// </summary>
/// <param name=""></param>
/// <returns> true：移動成功, false：移動失敗 </returns>
bool EnemyController::MoveUp() {

  character_controller_event_interface_.OnPushUpKey(character_, false);
  return true;
}

/// <summary>
/// 下に移動する
/// </summary>
/// <param name=""></param>
/// <returns> true：移動成功, false：移動失敗 </returns>
bool EnemyController::MoveDown() {

  character_controller_event_interface_.OnPushDownKey(character_, false);
  return true;
}

/// <summary>
/// 左に移動する
/// </summary>
/// <param name=""></param>
/// <returns> true：移動成功, false：移動失敗 </returns>
bool EnemyController::MoveLeft() {

  character_controller_event_interface_.OnPushLeftKey(character_, false);
  return true;
}

/// <summary>
/// 右に移動する
/// </summary>
/// <param name=""></param>
/// <returns> true：移動成功, false：移動失敗 </returns>
bool EnemyController::MoveRight() {

  character_controller_event_interface_.OnPushRightKey(character_, false);
  return true;
}

/// <summary>
/// 弾幕を撃つ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void EnemyController::ShootBullet() {

  character_controller_event_interface_.OnPushZKey(character_);
}