﻿#pragma once

#include "Game/Bullet.h"
#include <iostream>

/// <summary>
/// バレッジイベントインターフェース
/// </summary>
class BarrageEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  BarrageEventInterface() {

    // コンソールに出力
    std::cout << "BattleLevelEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~BarrageEventInterface() {

    // コンソールに出力
    std::cout << "~BattleLevelEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// 弾の生成
  /// </summary>
  /// <param name=""> バレットID </param>
  /// <returns> 弾 </returns>
  virtual Bullet* OnCreateBullet(BulletId) = 0;

  /// <summary>
  /// プレイヤーの座標を取得
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  virtual void OnGetPlayerPosition(float&, float&) = 0;


private:

};