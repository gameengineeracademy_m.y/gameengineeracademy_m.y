﻿#include "Game/BarrageManager.h"

namespace {

  /// <summary>
  /// リストID 最大値
  /// </summary>
  const int kMaxListId = 2000;

  /// <summary>
  /// リストID リセット値
  /// </summary>
  const int kResetListId = 0;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
BarrageManager::BarrageManager()
  : Task(TaskId::kBarrageManager)
  , barrage_list_()
  , generate_barrage_list_()
  , current_phase_(PhaseType::kPlay)
  , list_id_max_(0) {

  // コンソールに出力
  std::cout << "BarrageManager コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
BarrageManager::~BarrageManager() {

  // コンソールに出力
  std::cout << "~BarrageManager デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void BarrageManager::Update(float process_time) {

  bool is_finish = false;
  std::vector<int> release_list;

  // 停止中なら処理終了
  if (current_phase_ == PhaseType::kStop) {
    return;
  }

  //----------------------------------------
  // 弾幕 初期配置
  //----------------------------------------
  for (auto barrage : generate_barrage_list_) {

    BarrageId barrage_id = barrage.second->GetBarrageId();
    switch (barrage_id) {
    case BarrageId::kAllDirection: {
      //---------------------------------
      //全方向
      //---------------------------------
      is_finish = barrage.second->AllDirection(process_time);
      if (is_finish) {
        barrage.second->SetFinish();
        release_list.push_back(barrage.first);
      }
      break;
    }
    case BarrageId::kRotation: {
      //---------------------------------
      //回転
      //---------------------------------
      is_finish = barrage.second->Rotation(process_time);
      if (is_finish) {
        barrage.second->SetFinish();
        release_list.push_back(barrage.first);
      }
      break;
    }
    case BarrageId::kContinuous: {
      //---------------------------------
      //連射
      //---------------------------------
      is_finish = barrage.second->ContinuousShoot(process_time);
      if (is_finish) {
        barrage.second->SetFinish();
        release_list.push_back(barrage.first);
      }
      break;
    }
    case BarrageId::kSingle: {
      //---------------------------------
      //単発射撃
      //---------------------------------
      is_finish = barrage.second->SingleShoot(process_time);
      if (is_finish) {
        barrage.second->SetFinish();
        release_list.push_back(barrage.first);
      }
      break;
    }
    case BarrageId::kDouble: {
      //---------------------------------
      //2倍射撃
      //---------------------------------
      is_finish = barrage.second->DoubleShoot(process_time);
      if (is_finish) {
        barrage.second->SetFinish();
        release_list.push_back(barrage.first);
      }
      break;
    }
    case BarrageId::kTriple: {
      //---------------------------------
      //3倍射撃
      //---------------------------------
      is_finish = barrage.second->TripleShoot(process_time);
      if (is_finish) {
        barrage.second->SetFinish();
        release_list.push_back(barrage.first);
      }
      break;
    }
    case BarrageId::kQuadruple: {
      //---------------------------------
      //4倍射撃
      //---------------------------------
      is_finish = barrage.second->QuardShoot(process_time);
      if (is_finish) {
        barrage.second->SetFinish();
        release_list.push_back(barrage.first);
      }
      break;
    }
    }
  }

  //----------------------------------------
  // 処理終了済みの弾幕をリストから削除
  //----------------------------------------
  for (auto index : release_list) {
    Barrage* barrage = generate_barrage_list_[index];
    generate_barrage_list_.erase(index);
    if (barrage != nullptr) {
      delete barrage;
      barrage = nullptr;
    }
  }
}

/// <summary>
/// 弾幕(バレッジ)のリスト登録
/// </summary>
/// <param name=""> バレッジID </param>
/// <returns> バレッジ </returns>
Barrage* BarrageManager::RegisterBarrage(BarrageId barrage_id) {

  Barrage* barrage = nullptr;
  // 指定のバレッジIDがバレッジリストに存在するか確認
  // 存在しない場合はバレッジを生成してリストに登録
  if (barrage_list_.find(barrage_id) != barrage_list_.end()) {
    return nullptr;
  }
  barrage = new Barrage(barrage_id);
  if (barrage == nullptr) {
    return nullptr;
  }
  barrage_list_[barrage_id] = barrage;
  return barrage;
}

/// <summary>
/// 弾幕の初期配置
/// </summary>
/// <param name=""> バレッジID </param>
/// <returns> バレッジ </returns>
Barrage* BarrageManager::DeployBarrageBullet(BarrageId barrage_id) {

  Barrage* barrage = nullptr;

  // リストに存在しない弾幕なら処理終了
  // リストに存在するなら弾幕を生成し、生成リストに追加する
  if (barrage_list_.find(barrage_id) == barrage_list_.end()) {
    return nullptr;
  }

  barrage = barrage_list_[barrage_id]->GenerateClone();
  if (barrage == nullptr) {
    return nullptr;
  }

  // 現在のリストIDの最大値をセット
  int index = list_id_max_;
  // 生成リストの空きインデックスを調べ、生成リストに登録
  while (true) {
    if (generate_barrage_list_.find(index) == generate_barrage_list_.end()) {
      generate_barrage_list_[index] = barrage;
      list_id_max_ = ++index;
      break;
    }
    ++index;
  }
  if (list_id_max_ >= kMaxListId) {
    list_id_max_ = kResetListId;
  }

  return barrage;
}

/// <summary>
/// 生成した弾幕をすべて終了させる
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BarrageManager::FinishGenerateBarrage() {

  for (auto barrage : generate_barrage_list_) {
    barrage.second->SetFinish();
  }
}

/// <summary>
/// 生成した弾幕の破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BarrageManager::DisposeGenerateBarrage() {

  std::vector<int> release_index;

  // 生成リストに要素が残っている場合はすべてリストから降ろし、破棄する
  if (!generate_barrage_list_.empty()) {
    // 生成リスト内のインデックスをすべて取得
    for (auto barrage : generate_barrage_list_) {
      release_index.push_back(barrage.first);
    }
    // 生成リストから降ろし、弾幕生成処理を破棄する
    for (auto index : release_index) {
      Barrage* barrage = generate_barrage_list_[index];
      generate_barrage_list_.erase(index);
      if (barrage != nullptr) {
        delete barrage;
        barrage = nullptr;
      }
    }
  }
  // インデックスリストクリア
  release_index.clear();
}

/// <summary>
/// 弾幕リストの破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BarrageManager::DisposeBarrageList() {

  std::vector<BarrageId> release_id;

  // バレッジリストの破棄
  if (!barrage_list_.empty()) {
    // リスト内のインデックスをすべて取得
    for (auto barrage : barrage_list_) {
      release_id.push_back(barrage.first);
    }
    // バレッジリストから降ろし、バレッジを破棄する
    for (auto index : release_id) {
      Barrage* barrage = barrage_list_[index];
      barrage_list_.erase(index);
      if (barrage != nullptr) {
        delete barrage;
        barrage = nullptr;
      }
    }
  }
  // IDリストクリア
  release_id.clear();
}