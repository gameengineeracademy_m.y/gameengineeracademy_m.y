﻿#include "Game/GameTimer.h"

namespace {

  /// <summary>
  /// プレイタイム テキスト
  /// </summary>
  const char* kPlayTimeText = "PLAY TIME";

  /// <summary>
  /// ラップタイム テキスト
  /// </summary>
  const char* kRapTimeText = "RAP TIME";

  /// <summary>
  /// 敵キャラA テキスト
  /// </summary>
  const char* kEnemyAText = "<Mid-Boss>";

  /// <summary>
  /// 敵キャラB テキスト
  /// </summary>
  const char* kEnemyBText = "<Final Boss>";

  /// <summary>
  /// フェーズ１ テキスト
  /// </summary>
  const char* kPhase01Text = "PHASE1";

  /// <summary>
  /// フェーズ２ テキスト
  /// </summary>
  const char* kPhase02Text = "PHASE2";

  /// <summary>
  /// フェーズ３ テキスト
  /// </summary>
  const char* kPhase03Text = "PHASE3";

  /// <summary>
  /// フェーズ４ テキスト
  /// </summary>
  const char* kPhase04Text = "PHASE4";

  /// <summary>
  /// フェーズ５ テキスト
  /// </summary>
  const char* kPhase05Text = "PHASE5";

  /// <summary>
  /// フェーズ６ テキスト
  /// </summary>
  const char* kPhase06Text = "PHASE6";

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSizeTitle = 25;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 1;

  /// <summary>
  /// 文字フォントサイズ フェーズ
  /// </summary>
  const int kFontSize = 25;

  /// <summary>
  /// 文字フォントサイズ ボス名称
  /// </summary>
  const int kFontSizeBossText = 20;

  /// <summary>
  /// 文字フォントサイズ フェーズ
  /// </summary>
  const int kFontSizePhaseText = 18;

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Reggae One";

  /// <summary>
  /// 文字の色
  /// </summary>
  const int kTextForeColorWhite = GetColor(255, 255, 255);   // 白色

  /// <summary>
  /// 文字の色
  /// </summary>
  const int kTextForeColorAqua = GetColor(0, 200, 255);      // 水色

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjustPlus = 10;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjustMinus = -10;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 累積時間のリセット値
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 拡大率 最大値
  /// </summary>
  const float kExRateMax = 1.3f;

  /// <summary>
  /// 拡大率 最小値
  /// </summary>
  const float kExRateMin = 0.7f;

  /// <summary>
  /// 角度
  /// </summary>
  const float kAngle = 0.0f;

  /// <summary>
  /// インデックス初期値
  /// </summary>
  const int kInitIndex = 0;

  /// <summary>
  /// 画像のサイズ 幅
  /// </summary>
  const int kImageWidth = 16;

  /// <summary>
  /// 画像のサイズ 高さ
  /// </summary>
  const int kImageHeight = 32;

  /// <summary>
  /// 時間コロン描画位置 Y方向
  /// </summary>
  const int kTimeColon01PosY = 18;

  /// <summary>
  /// 時間コロン描画位置 Y方向
  /// </summary>
  const int kTimeColon02PosY = 7;

  /// <summary>
  /// 時間描画位置 Y方向
  /// </summary>
  const int kTimeDrawPosY = 55;

  /// <summary>
  /// 時間描画位置 X方向
  /// </summary>
  const int kMinuteTensTimePosX = 850;

  /// <summary>
  /// ラップタイム描画位置 X方向
  /// </summary>
  const int kMinuteTensRapTimePosX = 915;

  /// <summary>
  /// ラップタイムコロン描画位置 Y方向
  /// </summary>
  const int kRapTimeColon01PosY = 15;

  /// <summary>
  /// ラップタイムコロン描画位置 Y方向
  /// </summary>
  const int kRapTimeColon02PosY = 5;

  /// <summary>
  /// ラップタイム描画位置 Y方向
  /// </summary>
  const int kRapTimeEnemyAPosY = 180;

  /// <summary>
  /// ラップタイム描画位置 Y方向
  /// </summary>
  const int kRapTimeEnemyBPosY = 390;

  /// <summary>
  /// ラップタイム描画位置 Y方向 調整分
  /// </summary>
  const int kRapTimeDrawPosYAdjust = 30;

  /// <summary>
  /// テキスト表示位置 PLAY TIME
  /// </summary>
  const int kPlayTimeTextPosX = 810;

  /// <summary>
  /// テキスト表示位置 PLAY TIME
  /// </summary>
  const int kPlayTimeTextPosY = 18;

  /// <summary>
  /// テキスト表示位置 RAP TIME
  /// </summary>
  const int kRapTimeTextPosX = 810;

  /// <summary>
  /// テキスト表示位置 RAP TIME
  /// </summary>
  const int kRapTimeTextPosY = 98;

  /// <summary>
  /// テキスト表示位置 中ボス
  /// </summary>
  const int kEnemyTextPosX = 815;

  /// <summary>
  /// テキスト表示位置 中ボス
  /// </summary>
  const int kEnemyATextPosY = 135;

  /// <summary>
  /// テキスト表示位置 大ボス
  /// </summary>
  const int kEnemyBTextPosY = 350;

  /// <summary>
  /// テキスト表示位置 フェーズ
  /// </summary>
  const int kEnemyPhaseTextPosX = 815;

  /// <summary>
  /// テキスト表示位置 中ボス フェーズ1
  /// </summary>
  const int kEnemyAPhase01TextPosY = 168;

  /// <summary>
  /// テキスト表示位置 中ボス フェーズ2
  /// </summary>
  const int kEnemyAPhase02TextPosY = 198;

  /// <summary>
  /// テキスト表示位置 中ボス フェーズ3
  /// </summary>
  const int kEnemyAPhase03TextPosY = 228;

  /// <summary>
  /// テキスト表示位置 中ボス フェーズ4
  /// </summary>
  const int kEnemyAPhase04TextPosY = 258;

  /// <summary>
  /// テキスト表示位置 中ボス フェーズ5
  /// </summary>
  const int kEnemyAPhase05TextPosY = 288;

  /// <summary>
  /// テキスト表示位置 中ボス フェーズ6
  /// </summary>
  const int kEnemyAPhase06TextPosY = 318;

  /// <summary>
  /// テキスト表示位置 大ボス フェーズ1
  /// </summary>
  const int kEnemyBPhase01TextPosY = 378;

  /// <summary>
  /// テキスト表示位置 大ボス フェーズ2
  /// </summary>
  const int kEnemyBPhase02TextPosY = 408;

  /// <summary>
  /// テキスト表示位置 大ボス フェーズ3
  /// </summary>
  const int kEnemyBPhase03TextPosY = 438;

  /// <summary>
  /// テキスト表示位置 大ボス フェーズ4
  /// </summary>
  const int kEnemyBPhase04TextPosY = 468;

  /// <summary>
  /// テキスト表示位置 大ボス フェーズ5
  /// </summary>
  const int kEnemyBPhase05TextPosY = 498;

  /// <summary>
  /// テキスト表示位置 大ボス フェーズ6
  /// </summary>
  const int kEnemyBPhase06TextPosY = 528;

  /// <summary>
  /// 画像ピリオド マイナス
  /// </summary>
  const int kImageMinusIndex = 11;

  /// <summary>
  /// 画像ピリオド インデックス
  /// </summary>
  const int kImagePeriodIndex = 12;

  /// <summary>
  /// 10000
  /// </summary>
  const int kTenThousand = 10000;

  /// <summary>
  /// 1000
  /// </summary>
  const float kThousandf = 1000.0f;

  /// <summary>
  /// 1000
  /// </summary>
  const int kThousand = 1000;

  /// <summary>
  /// 100
  /// </summary>
  const int kHundred = 100;

  /// <summary>
  /// 10
  /// </summary>
  const int kTen = 10;

  /// <summary>
  /// 1
  /// </summary>
  const int kOne = 1;

  /// <summary>
  /// 60秒 = 1分 変換
  /// </summary>
  const int kChangeMinutes = 60;

  /// <summary>
  /// フェーズ数 最大値
  /// </summary>
  const int kEnemyPhaseMax = 6;

  /// <summary>
  /// フェーズ数 最大値インデックス
  /// </summary>
  const int kEnemyPhaseMaxIndex = 5;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
GameTimer::GameTimer()
  : Task(TaskId::kGameTimer)
  , current_phase_(PhaseType::kInitialize)
  , graphic_handle_()
  , time_()
  , enemy_a_phase_time_()
  , enemy_b_phase_time_()
  , time_pos_()
  , enemy_a_phase_time_pos_()
  , enemy_b_phase_time_pos_()
  , timer_(0.0f)
  , rap_time_(0.0f)
  , alpha_(0)
  , font_handle_() {

  // コンソールに出力
  std::cout << "GameTimer コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
GameTimer::~GameTimer() {

  // コンソールに出力
  std::cout << "~GameTimer デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void GameTimer::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //--------------------------
    //初期化処理フェーズ
    //--------------------------
    Initialize();
    // 現在のフェーズを「起動待機」に変更
    ChangeCurrentPhase(PhaseType::kStartWait);
    break;
  }
  case PhaseType::kStartWait: {
    //--------------------------
    //起動待機フェーズ
    //--------------------------
    break;
  }
  case PhaseType::kStartUp: {
    //--------------------------
    //起動フェーズ
    //--------------------------
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「処理待機」に変更
      ChangeCurrentPhase(PhaseType::kProcessWait);
    }
    break;
  }
  case PhaseType::kProcessWait: {
    //--------------------------
    //処理待機フェーズ
    //--------------------------
    break;
  }
  case PhaseType::kProcess: {
    //--------------------------
    //処理中フェーズ
    //--------------------------
    Process(process_time);
    break;
  }
  case PhaseType::kStop: {
    //--------------------------
    //停止フェーズ
    //--------------------------
    break;
  }
  case PhaseType::kBeforeFinalize: {
    //--------------------------
    //終了処理前フェーズ
    //--------------------------
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //--------------------------
    //終了処理フェーズ
    //--------------------------
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void GameTimer::Render() {

  switch (current_phase_) {
  case PhaseType::kStartUp:
  case PhaseType::kProcessWait:
  case PhaseType::kProcess:
  case PhaseType::kStop:
  case PhaseType::kBeforeFinalize: {

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

    // プレイ時間
    DrawGraph(time_pos_.min_tens.x, time_pos_.min_tens.y, graphic_handle_.at(time_.min_tens), true);
    DrawGraph(time_pos_.min_ones.x, time_pos_.min_ones.y, graphic_handle_.at(time_.min_ones), true);

    DrawGraph(time_pos_.min_cln_01.x, time_pos_.min_cln_01.y, graphic_handle_.at(time_.colon), true);
    DrawGraph(time_pos_.min_cln_02.x, time_pos_.min_cln_02.y, graphic_handle_.at(time_.colon), true);

    DrawGraph(time_pos_.sec_tens.x, time_pos_.sec_tens.y, graphic_handle_.at(time_.sec_tens), true);
    DrawGraph(time_pos_.sec_ones.x, time_pos_.sec_ones.y, graphic_handle_.at(time_.sec_ones), true);

    DrawGraph(time_pos_.sec_period.x, time_pos_.sec_period.y, graphic_handle_.at(time_.colon), true);
    
    DrawGraph(time_pos_.msec_huns.x, time_pos_.msec_huns.y, graphic_handle_.at(time_.msec_huns), true);
    DrawGraph(time_pos_.msec_tens.x, time_pos_.msec_tens.y, graphic_handle_.at(time_.msec_tens), true);
    DrawGraph(time_pos_.msec_ones.x, time_pos_.msec_ones.y, graphic_handle_.at(time_.msec_ones), true);

    // ラップタイム
    for (int i = 0; i < kEnemyPhaseMax; ++i) {

      DrawRotaGraph(enemy_a_phase_time_pos_.at(i).min_tens.x, enemy_a_phase_time_pos_.at(i).min_tens.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_a_phase_time_.at(i).min_tens), true);
      DrawRotaGraph(enemy_a_phase_time_pos_.at(i).min_ones.x, enemy_a_phase_time_pos_.at(i).min_ones.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_a_phase_time_.at(i).min_ones), true);

      DrawRotaGraph(enemy_a_phase_time_pos_.at(i).min_cln_01.x, enemy_a_phase_time_pos_.at(i).min_cln_01.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_a_phase_time_.at(i).colon), true);
      DrawRotaGraph(enemy_a_phase_time_pos_.at(i).min_cln_02.x, enemy_a_phase_time_pos_.at(i).min_cln_02.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_a_phase_time_.at(i).colon), true);

      DrawRotaGraph(enemy_a_phase_time_pos_.at(i).sec_tens.x, enemy_a_phase_time_pos_.at(i).sec_tens.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_a_phase_time_.at(i).sec_tens), true);
      DrawRotaGraph(enemy_a_phase_time_pos_.at(i).sec_ones.x, enemy_a_phase_time_pos_.at(i).sec_ones.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_a_phase_time_.at(i).sec_ones), true);

      DrawRotaGraph(enemy_a_phase_time_pos_.at(i).sec_period.x, enemy_a_phase_time_pos_.at(i).sec_period.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_a_phase_time_.at(i).colon), true);

      DrawRotaGraph(enemy_a_phase_time_pos_.at(i).msec_huns.x, enemy_a_phase_time_pos_.at(i).msec_huns.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_a_phase_time_.at(i).msec_huns), true);
      DrawRotaGraph(enemy_a_phase_time_pos_.at(i).msec_tens.x, enemy_a_phase_time_pos_.at(i).msec_tens.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_a_phase_time_.at(i).msec_tens), true);
      DrawRotaGraph(enemy_a_phase_time_pos_.at(i).msec_ones.x, enemy_a_phase_time_pos_.at(i).msec_ones.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_a_phase_time_.at(i).msec_ones), true);

      DrawRotaGraph(enemy_b_phase_time_pos_.at(i).min_tens.x, enemy_b_phase_time_pos_.at(i).min_tens.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_b_phase_time_.at(i).min_tens), true);
      DrawRotaGraph(enemy_b_phase_time_pos_.at(i).min_ones.x, enemy_b_phase_time_pos_.at(i).min_ones.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_b_phase_time_.at(i).min_ones), true);

      DrawRotaGraph(enemy_b_phase_time_pos_.at(i).min_cln_01.x, enemy_b_phase_time_pos_.at(i).min_cln_01.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_b_phase_time_.at(i).colon), true);
      DrawRotaGraph(enemy_b_phase_time_pos_.at(i).min_cln_02.x, enemy_b_phase_time_pos_.at(i).min_cln_02.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_b_phase_time_.at(i).colon), true);

      DrawRotaGraph(enemy_b_phase_time_pos_.at(i).sec_tens.x, enemy_b_phase_time_pos_.at(i).sec_tens.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_b_phase_time_.at(i).sec_tens), true);
      DrawRotaGraph(enemy_b_phase_time_pos_.at(i).sec_ones.x, enemy_b_phase_time_pos_.at(i).sec_ones.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_b_phase_time_.at(i).sec_ones), true);

      DrawRotaGraph(enemy_b_phase_time_pos_.at(i).sec_period.x, enemy_b_phase_time_pos_.at(i).sec_period.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_b_phase_time_.at(i).colon), true);

      DrawRotaGraph(enemy_b_phase_time_pos_.at(i).msec_huns.x, enemy_b_phase_time_pos_.at(i).msec_huns.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_b_phase_time_.at(i).msec_huns), true);
      DrawRotaGraph(enemy_b_phase_time_pos_.at(i).msec_tens.x, enemy_b_phase_time_pos_.at(i).msec_tens.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_b_phase_time_.at(i).msec_tens), true);
      DrawRotaGraph(enemy_b_phase_time_pos_.at(i).msec_ones.x, enemy_b_phase_time_pos_.at(i).msec_ones.y,
                    kExRateMin, kAngle, graphic_handle_.at(enemy_b_phase_time_.at(i).msec_ones), true);
    }

    // テキストの描画
    DrawStringToHandle(kPlayTimeTextPosX, kPlayTimeTextPosY, kPlayTimeText, kTextForeColorAqua, font_handle_.at(static_cast<int>(FontSize::k25)));
    DrawStringToHandle(kRapTimeTextPosX, kRapTimeTextPosY, kRapTimeText, kTextForeColorAqua, font_handle_.at(static_cast<int>(FontSize::k25)));

    DrawStringToHandle(kEnemyTextPosX, kEnemyATextPosY, kEnemyAText, kTextForeColorWhite, font_handle_.at(static_cast<int>(FontSize::k20)));
    DrawStringToHandle(kEnemyTextPosX, kEnemyBTextPosY, kEnemyBText, kTextForeColorWhite, font_handle_.at(static_cast<int>(FontSize::k20)));

    DrawStringToHandle(kEnemyPhaseTextPosX, kEnemyAPhase01TextPosY, kPhase01Text, kTextForeColorWhite, font_handle_.at(static_cast<int>(FontSize::k18)));
    DrawStringToHandle(kEnemyPhaseTextPosX, kEnemyAPhase02TextPosY, kPhase02Text, kTextForeColorWhite, font_handle_.at(static_cast<int>(FontSize::k18)));
    DrawStringToHandle(kEnemyPhaseTextPosX, kEnemyAPhase03TextPosY, kPhase03Text, kTextForeColorWhite, font_handle_.at(static_cast<int>(FontSize::k18)));
    DrawStringToHandle(kEnemyPhaseTextPosX, kEnemyAPhase04TextPosY, kPhase04Text, kTextForeColorWhite, font_handle_.at(static_cast<int>(FontSize::k18)));
    DrawStringToHandle(kEnemyPhaseTextPosX, kEnemyAPhase05TextPosY, kPhase05Text, kTextForeColorWhite, font_handle_.at(static_cast<int>(FontSize::k18)));
    DrawStringToHandle(kEnemyPhaseTextPosX, kEnemyAPhase06TextPosY, kPhase06Text, kTextForeColorWhite, font_handle_.at(static_cast<int>(FontSize::k18)));

    DrawStringToHandle(kEnemyPhaseTextPosX, kEnemyBPhase01TextPosY, kPhase01Text, kTextForeColorWhite, font_handle_.at(static_cast<int>(FontSize::k18)));
    DrawStringToHandle(kEnemyPhaseTextPosX, kEnemyBPhase02TextPosY, kPhase02Text, kTextForeColorWhite, font_handle_.at(static_cast<int>(FontSize::k18)));
    DrawStringToHandle(kEnemyPhaseTextPosX, kEnemyBPhase03TextPosY, kPhase03Text, kTextForeColorWhite, font_handle_.at(static_cast<int>(FontSize::k18)));
    DrawStringToHandle(kEnemyPhaseTextPosX, kEnemyBPhase04TextPosY, kPhase04Text, kTextForeColorWhite, font_handle_.at(static_cast<int>(FontSize::k18)));
    DrawStringToHandle(kEnemyPhaseTextPosX, kEnemyBPhase05TextPosY, kPhase05Text, kTextForeColorWhite, font_handle_.at(static_cast<int>(FontSize::k18)));
    DrawStringToHandle(kEnemyPhaseTextPosX, kEnemyBPhase06TextPosY, kPhase06Text, kTextForeColorWhite, font_handle_.at(static_cast<int>(FontSize::k18)));

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    break;
  }
  }
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void GameTimer::Initialize() {

  // 透過率 初期設定
  alpha_ = kAlphaMin;

  // フォント情報生成
  FontInfo* font_info = FontInfo::GetFontInfoInstance();
  if (font_info == nullptr) {
    return;
  }
  int font_handle = font_info->GetFontInfo(kFontName, kFontSizePhaseText, kFontWidth, DX_FONTTYPE_NORMAL);
  font_handle_.push_back(font_handle); 
  font_handle = font_info->GetFontInfo(kFontName, kFontSizeBossText, kFontWidth, DX_FONTTYPE_NORMAL);
  font_handle_.push_back(font_handle); 
  font_handle = font_info->GetFontInfo(kFontName, kFontSize, kFontWidth, DX_FONTTYPE_NORMAL);
  font_handle_.push_back(font_handle);  

  // インデックス初期化
  time_.min_tens = kInitIndex;
  time_.min_ones = kInitIndex;
  time_.sec_tens = kInitIndex;
  time_.sec_ones = kInitIndex;
  time_.msec_huns = kInitIndex;
  time_.msec_tens = kInitIndex;
  time_.msec_ones = kInitIndex;
  time_.colon = kImagePeriodIndex;

  // 時間表示位置 分(miniutes)
  time_pos_.min_tens.x = kMinuteTensTimePosX;
  time_pos_.min_tens.y = kTimeDrawPosY;
  time_pos_.min_ones.x = time_pos_.min_tens.x + kImageWidth;
  time_pos_.min_ones.y = time_pos_.min_tens.y;
  // 時間表示位置 コロン
  time_pos_.min_cln_01.x = time_pos_.min_ones.x + kImageWidth;
  time_pos_.min_cln_01.y = time_pos_.min_tens.y - kTimeColon01PosY;
  time_pos_.min_cln_02.x = time_pos_.min_ones.x + kImageWidth;
  time_pos_.min_cln_02.y = time_pos_.min_tens.y - kTimeColon02PosY;
  // 時間表示位置 秒(seconds)
  time_pos_.sec_tens.x = time_pos_.min_cln_01.x + kImageWidth;
  time_pos_.sec_tens.y = time_pos_.min_tens.y;
  time_pos_.sec_ones.x = time_pos_.sec_tens.x + kImageWidth;
  time_pos_.sec_ones.y = time_pos_.min_tens.y;
  // 時間表示位置 ピリオド
  time_pos_.sec_period.x = time_pos_.sec_ones.x + kImageWidth;
  time_pos_.sec_period.y = time_pos_.min_tens.y;
  // 時間表示位置 ミリ秒(milliminiutes)
  time_pos_.msec_huns.x = time_pos_.sec_period.x + kImageWidth;
  time_pos_.msec_huns.y = time_pos_.min_tens.y;
  time_pos_.msec_tens.x = time_pos_.msec_huns.x + kImageWidth;
  time_pos_.msec_tens.y = time_pos_.min_tens.y;
  time_pos_.msec_ones.x = time_pos_.msec_tens.x + kImageWidth;
  time_pos_.msec_ones.y = time_pos_.min_tens.y;

  int y_pos_enemy_a = kRapTimeEnemyAPosY;
  int y_pos_enemy_b = kRapTimeEnemyBPosY;

  // ラップタイム
  for (int i = 0; i < kEnemyPhaseMax; ++i) {

    Time time;
    TimeDispPos disp_pos;
    enemy_a_phase_time_.push_back(time);
    enemy_a_phase_time_pos_.push_back(disp_pos);
    enemy_b_phase_time_.push_back(time);
    enemy_b_phase_time_pos_.push_back(disp_pos);

    enemy_a_phase_time_.at(i).min_tens = kImageMinusIndex;
    enemy_a_phase_time_.at(i).min_ones = kImageMinusIndex;
    enemy_a_phase_time_.at(i).sec_tens = kImageMinusIndex;
    enemy_a_phase_time_.at(i).sec_ones = kImageMinusIndex;
    enemy_a_phase_time_.at(i).msec_huns = kImageMinusIndex;
    enemy_a_phase_time_.at(i).msec_tens = kImageMinusIndex;
    enemy_a_phase_time_.at(i).msec_ones = kImageMinusIndex;
    enemy_a_phase_time_.at(i).colon = kImagePeriodIndex;

    enemy_a_phase_time_pos_.at(i).min_tens.x = kMinuteTensRapTimePosX;
    enemy_a_phase_time_pos_.at(i).min_tens.y = y_pos_enemy_a;
    enemy_a_phase_time_pos_.at(i).min_ones.x = enemy_a_phase_time_pos_.at(i).min_tens.x + static_cast<int>(kImageWidth * kExRateMin);
    enemy_a_phase_time_pos_.at(i).min_ones.y = enemy_a_phase_time_pos_.at(i).min_tens.y;
    enemy_a_phase_time_pos_.at(i).min_cln_01.x = enemy_a_phase_time_pos_.at(i).min_ones.x + static_cast<int>(kImageWidth * kExRateMin);
    enemy_a_phase_time_pos_.at(i).min_cln_01.y = enemy_a_phase_time_pos_.at(i).min_tens.y - kRapTimeColon01PosY;
    enemy_a_phase_time_pos_.at(i).min_cln_02.x = enemy_a_phase_time_pos_.at(i).min_ones.x + static_cast<int>(kImageWidth * kExRateMin);
    enemy_a_phase_time_pos_.at(i).min_cln_02.y = enemy_a_phase_time_pos_.at(i).min_tens.y - kRapTimeColon02PosY;
    enemy_a_phase_time_pos_.at(i).sec_tens.x = enemy_a_phase_time_pos_.at(i).min_cln_01.x + static_cast<int>(kImageWidth * kExRateMin);
    enemy_a_phase_time_pos_.at(i).sec_tens.y = enemy_a_phase_time_pos_.at(i).min_tens.y;
    enemy_a_phase_time_pos_.at(i).sec_ones.x = enemy_a_phase_time_pos_.at(i).sec_tens.x + static_cast<int>(kImageWidth * kExRateMin);
    enemy_a_phase_time_pos_.at(i).sec_ones.y = enemy_a_phase_time_pos_.at(i).min_tens.y;
    enemy_a_phase_time_pos_.at(i).sec_period.x = enemy_a_phase_time_pos_.at(i).sec_ones.x + static_cast<int>(kImageWidth * kExRateMin);
    enemy_a_phase_time_pos_.at(i).sec_period.y = enemy_a_phase_time_pos_.at(i).min_tens.y;
    enemy_a_phase_time_pos_.at(i).msec_huns.x = enemy_a_phase_time_pos_.at(i).sec_period.x + static_cast<int>(kImageWidth * kExRateMin);
    enemy_a_phase_time_pos_.at(i).msec_huns.y = enemy_a_phase_time_pos_.at(i).min_tens.y;
    enemy_a_phase_time_pos_.at(i).msec_tens.x = enemy_a_phase_time_pos_.at(i).msec_huns.x + static_cast<int>(kImageWidth * kExRateMin);
    enemy_a_phase_time_pos_.at(i).msec_tens.y = enemy_a_phase_time_pos_.at(i).min_tens.y;
    enemy_a_phase_time_pos_.at(i).msec_ones.x = enemy_a_phase_time_pos_.at(i).msec_tens.x + static_cast<int>(kImageWidth * kExRateMin);
    enemy_a_phase_time_pos_.at(i).msec_ones.y = enemy_a_phase_time_pos_.at(i).min_tens.y;

    enemy_b_phase_time_.at(i).min_tens = kImageMinusIndex;
    enemy_b_phase_time_.at(i).min_ones = kImageMinusIndex;
    enemy_b_phase_time_.at(i).sec_tens = kImageMinusIndex;
    enemy_b_phase_time_.at(i).sec_ones = kImageMinusIndex;
    enemy_b_phase_time_.at(i).msec_huns = kImageMinusIndex;
    enemy_b_phase_time_.at(i).msec_tens = kImageMinusIndex;
    enemy_b_phase_time_.at(i).msec_ones = kImageMinusIndex;
    enemy_b_phase_time_.at(i).colon = kImagePeriodIndex;

    enemy_b_phase_time_pos_.at(i).min_tens.x = kMinuteTensRapTimePosX;
    enemy_b_phase_time_pos_.at(i).min_tens.y = y_pos_enemy_b;
    enemy_b_phase_time_pos_.at(i).min_ones.x = enemy_b_phase_time_pos_.at(i).min_tens.x + static_cast<int>(kImageWidth * kExRateMin);
    enemy_b_phase_time_pos_.at(i).min_ones.y = enemy_b_phase_time_pos_.at(i).min_tens.y;
    enemy_b_phase_time_pos_.at(i).min_cln_01.x = enemy_b_phase_time_pos_.at(i).min_ones.x + static_cast<int>(kImageWidth * kExRateMin);
    enemy_b_phase_time_pos_.at(i).min_cln_01.y = enemy_b_phase_time_pos_.at(i).min_tens.y - kRapTimeColon01PosY;
    enemy_b_phase_time_pos_.at(i).min_cln_02.x = enemy_b_phase_time_pos_.at(i).min_ones.x + static_cast<int>(kImageWidth * kExRateMin);
    enemy_b_phase_time_pos_.at(i).min_cln_02.y = enemy_b_phase_time_pos_.at(i).min_tens.y - kRapTimeColon02PosY;
    enemy_b_phase_time_pos_.at(i).sec_tens.x = enemy_b_phase_time_pos_.at(i).min_cln_01.x + static_cast<int>(kImageWidth * kExRateMin);
    enemy_b_phase_time_pos_.at(i).sec_tens.y = enemy_b_phase_time_pos_.at(i).min_tens.y;
    enemy_b_phase_time_pos_.at(i).sec_ones.x = enemy_b_phase_time_pos_.at(i).sec_tens.x + static_cast<int>(kImageWidth * kExRateMin);
    enemy_b_phase_time_pos_.at(i).sec_ones.y = enemy_b_phase_time_pos_.at(i).min_tens.y;
    enemy_b_phase_time_pos_.at(i).sec_period.x = enemy_b_phase_time_pos_.at(i).sec_ones.x + static_cast<int>(kImageWidth * kExRateMin);
    enemy_b_phase_time_pos_.at(i).sec_period.y = enemy_b_phase_time_pos_.at(i).min_tens.y;
    enemy_b_phase_time_pos_.at(i).msec_huns.x = enemy_b_phase_time_pos_.at(i).sec_period.x + static_cast<int>(kImageWidth * kExRateMin);
    enemy_b_phase_time_pos_.at(i).msec_huns.y = enemy_b_phase_time_pos_.at(i).min_tens.y;
    enemy_b_phase_time_pos_.at(i).msec_tens.x = enemy_b_phase_time_pos_.at(i).msec_huns.x + static_cast<int>(kImageWidth * kExRateMin);
    enemy_b_phase_time_pos_.at(i).msec_tens.y = enemy_b_phase_time_pos_.at(i).min_tens.y;
    enemy_b_phase_time_pos_.at(i).msec_ones.x = enemy_b_phase_time_pos_.at(i).msec_tens.x + static_cast<int>(kImageWidth * kExRateMin);
    enemy_b_phase_time_pos_.at(i).msec_ones.y = enemy_b_phase_time_pos_.at(i).min_tens.y;

    y_pos_enemy_a += kRapTimeDrawPosYAdjust;
    y_pos_enemy_b += kRapTimeDrawPosYAdjust;
  }
}

/// <summary>
/// 処理中
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void GameTimer::Process(float process_time) {

  if (current_phase_ != PhaseType::kProcess) {
    return;
  }

  // タイマーに処理時間(単位：秒)を加算
  timer_ += process_time;
  rap_time_ += process_time;

  // タイマーの時間を「分・秒・ミリ秒」に変換
  int minutes_value = static_cast<int>(timer_ / kChangeMinutes);
  if (minutes_value >= kHundred) {
    return;
  }
  int minutes_tens = minutes_value / kTen % kTen;
  int minutes_ones = minutes_value / kOne % kTen;

  time_.min_tens = minutes_tens;
  time_.min_ones = minutes_ones;

  float seconds = static_cast<float>(timer_ - static_cast<float>(minutes_value * kChangeMinutes));
  int seconds_value = static_cast<int>(seconds * kThousandf);

  int seconds_tens = seconds_value / kTenThousand % kTen;
  int seconds_ones = seconds_value / kThousand % kTen;
  int milliseconds_hundreds = seconds_value / kHundred % kTen;
  int milliseconds_tens = seconds_value / kTen % kTen;
  int milliseconds_ones = seconds_value / kOne % kTen;

  time_.sec_tens = seconds_tens;
  time_.sec_ones = seconds_ones;
  time_.msec_huns = milliseconds_hundreds;
  time_.msec_tens = milliseconds_tens;
  time_.msec_ones = milliseconds_ones;
}

/// <summary>
/// 透過率調整処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool GameTimer::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率に調整値を加算
  switch (current_phase_) {
  case PhaseType::kStartUp: {
    // 起動フェーズ
    alpha_ += kAlphaAdjustPlus;
    break;
  }
  case PhaseType::kBeforeFinalize: {
    // 終了処理前フェーズ
    alpha_ += kAlphaAdjustMinus;
    break;
  }
  }

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= kAlphaMax) {
    alpha_ = kAlphaMax;
    is_finish = true;
  }
  else if (alpha_ <= kAlphaMin) {
    alpha_ = kAlphaMin;
    is_finish = true;
  }

  return is_finish;
}

/// <summary>
/// ラップタイムをセット
/// </summary>
/// <param name=""> 敵の種類 </param>
/// <param name=""> フェーズ数 </param>
/// <returns></returns>
void GameTimer::SetRapTime(EnemyPhase enemy_phase, int phase) {

  // タイマーの時間を「分・秒・ミリ秒」に変換
  int minutes_value = static_cast<int>(rap_time_ / kChangeMinutes);
  if (minutes_value >= kHundred) {
    return;
  }
  int minutes_tens = minutes_value / kTen % kTen;
  int minutes_ones = minutes_value / kOne % kTen;

  float seconds = static_cast<float>(rap_time_ - static_cast<float>(minutes_value * kChangeMinutes));
  int seconds_value = static_cast<int>(seconds * kThousandf);

  int seconds_tens = seconds_value / kTenThousand % kTen;
  int seconds_ones = seconds_value / kThousand % kTen;
  int milliseconds_hundreds = seconds_value / kHundred % kTen;
  int milliseconds_tens = seconds_value / kTen % kTen;
  int milliseconds_ones = seconds_value / kOne % kTen;

  switch (enemy_phase) {
  case EnemyPhase::kEnemyA: {

    enemy_a_phase_time_.at(phase).min_tens = minutes_tens;
    enemy_a_phase_time_.at(phase).min_ones = minutes_ones;
    enemy_a_phase_time_.at(phase).sec_tens = seconds_tens;
    enemy_a_phase_time_.at(phase).sec_ones = seconds_ones;
    enemy_a_phase_time_.at(phase).msec_huns = milliseconds_hundreds;
    enemy_a_phase_time_.at(phase).msec_tens = milliseconds_tens;
    enemy_a_phase_time_.at(phase).msec_ones = milliseconds_ones;

    break;
  }
  case EnemyPhase::kEnemyB: {

    enemy_b_phase_time_.at(phase).min_tens = minutes_tens;
    enemy_b_phase_time_.at(phase).min_ones = minutes_ones;
    enemy_b_phase_time_.at(phase).sec_tens = seconds_tens;
    enemy_b_phase_time_.at(phase).sec_ones = seconds_ones;
    enemy_b_phase_time_.at(phase).msec_huns = milliseconds_hundreds;
    enemy_b_phase_time_.at(phase).msec_tens = milliseconds_tens;
    enemy_b_phase_time_.at(phase).msec_ones = milliseconds_ones;

    if (phase == kEnemyPhaseMaxIndex) {
      // タイマーを止める
      StopTime();
    }

    break;
  }
  }

  // ラップタイムをリセット
  rap_time_ = kResetTime;
}