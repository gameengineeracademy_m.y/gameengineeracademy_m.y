﻿#pragma once

#include "System/Character.h"

/// <summary>
/// プレイヤー処理
/// </summary>
class Player : public Character {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,       // 初期化処理フェーズ
    kStartWait,        // 起動待機フェーズ
    kStartUp,          // 起動フェーズ
    kProcessWait,      // 待機フェーズ
    kProcess,          // 処理中フェーズ
    kStop,             // 停止フェーズ
    kStopTime,         // 時間停止フェーズ
    kBeforeFinalize,   // 終了処理前フェーズ
    kFinalize,         // 終了処理フェーズ
    kFinalized,        // 終了処理済みフェーズ
    kPhaseMaxIndex     // フェーズ数
  };

  /// <summary>
  /// 生存・死亡フェーズ
  /// </summary>
  enum class SpecialPhase {
    kNone,              // 何もしない
    kPlayerDeath,       // プレイヤー死亡フェーズ
    kDeathEffect,       // 死亡演出フェーズ
    kDeathWait,         // 死亡待機フェーズ
    kRebornEffect,      // 復活演出フェーズ
    kPlayerReborn,      // プレイヤー復活フェーズ
    kRebornWait,        // 復活待機フェーズ
    kPhaseMaxIndex      // フェーズ数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> キャラクターイベントインターフェース </param>
  /// <returns></returns>
  Player(CharacterEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~Player();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// グラフィックハンドルをセット
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandle(int graphic_handle) { graphic_handle_ = graphic_handle; }

  /// <summary>
  /// グラフィックハンドルを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandle() { return graphic_handle_; }

  /// <summary>
  /// 表示状態にする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetShowState() { is_display_ = true; }

  /// <summary>
  /// 非表示状態にする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetHideState() { is_display_ = false; }

  /// <summary>
  /// 表示中かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  bool IsShowState() { return is_display_ == true; }

  /// <summary>
  /// 停止中かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:停止中, false:停止中以外 </returns>
  bool IsStop() { return current_phase_ == PhaseType::kStop; }

  /// <summary>
  /// 射撃種類を変更する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeShootType();

  /// <summary>
  /// 時間停止からの復帰処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ReturnTimeStop();

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }

  /// <summary>
  /// 特殊フェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeSpecialPhase(SpecialPhase phase_type) { special_phase_ = phase_type; }


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Initialize();

  /// <summary>
  /// 処理中
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Process(float);

  /// <summary>
  /// 停止中処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool StopProcess(float);

  /// <summary>
  /// 透過率変更処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AdjustAlphaValue();


private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 特殊フェーズ
  /// </summary>
  SpecialPhase special_phase_;

  /// <summary>
  /// グラフィックハンドル
  /// </summary>
  int graphic_handle_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  int alpha_adjust_;

  /// <summary>
  /// 表示・非表示フラグ
  /// </summary>
  bool is_display_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;
};