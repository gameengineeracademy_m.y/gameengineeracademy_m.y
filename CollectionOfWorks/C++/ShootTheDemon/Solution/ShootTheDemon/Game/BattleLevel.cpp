﻿#include "Game/BattleLevel.h"

namespace {

  /// <summary>
  /// 累積時間のリセット値
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 開始、終了時の待機時間
  /// </summary>
  const float kWaitTime = 1.5f;

  /// <summary>
  /// ロード時の文字フォントサイズ
  /// </summary>
  const int kFontSize = 35;

  /// <summary>
  /// ロード時の文字幅
  /// </summary>
  const int kFontWidth = 1;

  /// <summary>
  /// ロード時のテキスト表示位置
  /// </summary>
  const int kLoadStringPosX = 620;

  /// <summary>
  /// ロード時のテキスト表示位置
  /// </summary>
  const int kLoadStringPosY = 675;

  /// <summary>
  /// ロード時表示文字列
  /// </summary>
  const char* kDisplayLoadText = "Now Loading";

  /// <summary>
  /// ロード時表示ピリオド
  /// </summary>
  const char* kDisplayPeriod = ".";

  /// <summary>
  /// フォントファイルパス
  /// </summary>
  const char* kFontFilePath = "Asset/Font/ReggaeOne-Regular.ttf";

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Reggae One";

  /// <summary>
  /// 文字の色
  /// </summary>
  const int kStringForeColor = GetColor(255, 255, 255);   // 白色

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjustInit = 5;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaBulletAdjust = -5;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 符号反転
  /// </summary>
  const int kMinusSign = -1;

  /// <summary>
  /// 原点 X座標
  /// </summary>
  const int kMathOriginX = 0;

  /// <summary>
  /// 原点 Y座標
  /// </summary>
  const int kMathOriginY = 0;

  /// <summary>
  /// 色
  /// </summary>
  const int kBackColor = GetColor(0, 0, 255);   // 青色

  /// <summary>
  /// プレイヤー画像パス
  /// </summary>
  const char* kImagePlayerPath = "Asset/Image/player.png";

  /// <summary>
  /// 敵キャラA画像パス
  /// </summary>
  const char* kImageEnemyAPath = "Asset/Image/enemy_a.png";

  /// <summary>
  /// 敵キャラB画像パス
  /// </summary>
  const char* kImageEnemyBPath = "Asset/Image/enemy_b.png";

  /// <summary>
  /// 残機UPアイテム画像パス
  /// </summary>
  const char* kImageItemExtendPath = "Asset/Image/heart.png";

  /// <summary>
  /// レベルアップアイテム画像パス
  /// </summary>
  const char* kImageItemPowerUpPath = "Asset/Image/missile.png";

  /// <summary>
  /// 背景画像パス
  /// </summary>
  const char* kImageBackGroundSidePath = "Asset/Image/backgroundside.png";

  /// <summary>
  /// 画像ロードエラー
  /// </summary>
  const int kLoadError = -1;

  /// <summary>
  /// プレイヤー初期位置 調整値
  /// </summary>
  const int kPlayerInitPosY = 75;

  /// <summary>
  /// 敵キャラ初期位置 調整値
  /// </summary>
  const int kEnemyInitPosY = 225;

  /// <summary>
  /// ゲームデータ ファイルパス
  /// </summary>
  const char* kGameDataFilePath = "File/GameData.txt";

  /// <summary>
  /// サウンドデータ ファイルパス
  /// </summary>
  const char* kSoundDataFilePath = "File/SoundData.txt";

  /// <summary>
  /// プレイヤー死亡エフェクト 情報ファイルパス
  /// </summary>
  const char* kPlayerDeathFilePath = "File/BattleLevel_Effect_PlayerDeath.txt";

  /// <summary>
  /// 爆発エフェクト 情報ファイルパス
  /// </summary>
  const char* kExplosionFilePath = "File/BattleLevel_Effect_Explosion.txt";

  /// <summary>
  /// 復活エフェクト 情報ファイルパス
  /// </summary>
  const char* kRebornFilePath = "File/BattleLevel_Effect_Reborn.txt";

  /// <summary>
  /// 敵の爆破エフェクト 情報ファイルパス
  /// </summary>
  const char* kEnemyExplosionFilePath = "File/BattleLevel_Effect_EnemyExplosion.txt";

  /// <summary>
  /// 敵の破裂エフェクト 情報ファイルパス
  /// </summary>
  const char* kEnemyRuptureFilePath = "File/BattleLevel_Effect_EnemyRupture.txt";

  /// <summary>
  /// アイテム生成エフェクト 情報ファイルパス
  /// </summary>
  const char* kItemSummonFilePath = "File/BattleLevel_Effect_ItemSummon.txt";

  /// <summary>
  /// 通常弾消滅エフェクト 情報ファイルパス
  /// </summary>
  const char* kDisappearNormalFilePath = "File/BattleLevel_Effect_DisappearNormal.txt";

  /// <summary>
  /// 青弾消滅エフェクト 情報ファイルパス
  /// </summary>
  const char* kDisappearBlueBallFilePath = "File/BattleLevel_Effect_DisappearBlueBall.txt";

  /// <summary>
  /// 紫弾消滅エフェクト 情報ファイルパス
  /// </summary>
  const char* kDisappearPurpleBallFilePath = "File/BattleLevel_Effect_DisappearPurpleBall.txt";

  /// <summary>
  /// 赤弾消滅エフェクト 情報ファイルパス
  /// </summary>
  const char* kDisappearRedBallFilePath = "File/BattleLevel_Effect_DisappearRedBall.txt";

  /// <summary>
  /// 黄弾消滅エフェクト 情報ファイルパス
  /// </summary>
  const char* kDisappearYellowBallFilePath = "File/BattleLevel_Effect_DisappearYellowBall.txt";

  /// <summary>
  /// 緑弾消滅エフェクト 情報ファイルパス
  /// </summary>
  const char* kDisappearGreenBallFilePath = "File/BattleLevel_Effect_DisappearGreenBall.txt";

  /// <summary>
  /// 通常弾 情報ファイルパス
  /// </summary>
  const char* kNormalBulletFilePath = "File/BattleLevel_Bullet_Normal.txt";

  /// <summary>
  /// 白い弾 情報ファイルパス
  /// </summary>
  const char* kWhiteBallBulletFilePath = "File/BattleLevel_Bullet_WhiteBall.txt";

  /// <summary>
  /// 青い弾 情報ファイルパス
  /// </summary>
  const char* kBlueBallBulletFilePath = "File/BattleLevel_Bullet_BlueBall.txt";

  /// <summary>
  /// 紫弾 情報ファイルパス
  /// </summary>
  const char* kPurpleBallBulletFilePath = "File/BattleLevel_Bullet_PurpleBall.txt";

  /// <summary>
  /// 赤弾 情報ファイルパス
  /// </summary>
  const char* kRedBallBulletFilePath = "File/BattleLevel_Bullet_RedBall.txt";

  /// <summary>
  /// 黄弾 情報ファイルパス
  /// </summary>
  const char* kYellowBallBulletFilePath = "File/BattleLevel_Bullet_YellowBall.txt";

  /// <summary>
  /// 緑弾 情報ファイルパス
  /// </summary>
  const char* kGreenBallBulletFilePath = "File/BattleLevel_Bullet_GreenBall.txt";

  /// <summary>
  /// 単発射撃 情報ファイルパス
  /// </summary>
  const char* kSingleShootFilePath = "File/BattleLevel_Barrage_Single.txt";

  /// <summary>
  /// 2倍射撃 情報ファイルパス
  /// </summary>
  const char* kDoubleShootFilePath = "File/BattleLevel_Barrage_Double.txt";

  /// <summary>
  /// 3倍射撃 情報ファイルパス
  /// </summary>
  const char* kTripleShootFilePath = "File/BattleLevel_Barrage_Triple.txt";

  /// <summary>
  /// 4倍射撃 情報ファイルパス
  /// </summary>
  const char* kQuadShootFilePath = "File/BattleLevel_Barrage_Quadruple.txt";

  /// <summary>
  /// 全方向弾幕 情報ファイルパス
  /// </summary>
  const char* kAllDirectionFilePath = "File/BattleLevel_Barrage_AllDirection.txt";

  /// <summary>
  /// 回転 情報ファイルパス
  /// </summary>
  const char* kRotationFilePath = "File/BattleLevel_Barrage_Rotation.txt";

  /// <summary>
  /// 連射 情報ファイルパス
  /// </summary>
  const char* kContinuousFilePath = "File/BattleLevel_Barrage_Continuous.txt";

  /// <summary>
  /// プレイヤー ファイルパス
  /// </summary>
  const char* kPlayerFilePath = "File/BattleLevel_Character_Player.txt";

  /// <summary>
  /// 敵キャラA ファイルパス
  /// </summary>
  const char* kEnemyAFilePath = "File/BattleLevel_Character_EnemyA.txt";

  /// <summary>
  /// 敵キャラB ファイルパス
  /// </summary>
  const char* kEnemyBFilePath = "File/BattleLevel_Character_EnemyB.txt";

  /// <summary>
  /// アイテムA ファイルパス
  /// </summary>
  const char* kItemAFilePath = "File/BattleLevel_Character_ItemExtend.txt";

  /// <summary>
  /// アイテムB ファイルパス
  /// </summary>
  const char* kItemBFilePath = "File/BattleLevel_Character_ItemPowerUp.txt";

  /// <summary>
  /// 敵キャラA フェーズごとのHP ファイルパス
  /// </summary>
  const char* kEnemyAHitPointFilePath = "File/BattleLevel_Character_EnemyA_HitPonit.txt";

  /// <summary>
  /// 敵キャラB フェーズごとのHP ファイルパス
  /// </summary>
  const char* kEnemyBHitPointFilePath = "File/BattleLevel_Character_EnemyB_HitPonit.txt";

  /// <summary>
  /// 敵キャラA 第１フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyAFirstPhase01FilePath = "File/BattleLevel_EnemyA_First_01.txt";

  /// <summary>
  /// 敵キャラA 第１フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyAFirstPhase02FilePath = "File/BattleLevel_EnemyA_First_02.txt";

  /// <summary>
  /// 敵キャラA 第２フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyASecondPhase01FilePath = "File/BattleLevel_EnemyA_Second_01.txt";

  /// <summary>
  /// 敵キャラA 第２フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyASecondPhase02FilePath = "File/BattleLevel_EnemyA_Second_02.txt";

  /// <summary>
  /// 敵キャラA 第３フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyAThirdPhase01FilePath = "File/BattleLevel_EnemyA_Third_01.txt";

  /// <summary>
  /// 敵キャラA 第３フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyAThirdPhase02FilePath = "File/BattleLevel_EnemyA_Third_02.txt";

  /// <summary>
  /// 敵キャラA 第４フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyAFourthPhase01FilePath = "File/BattleLevel_EnemyA_Fourth_01.txt";

  /// <summary>
  /// 敵キャラA 第４フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyAFourthPhase02FilePath = "File/BattleLevel_EnemyA_Fourth_02.txt";

  /// <summary>
  /// 敵キャラA 第５フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyAFifthPhase01FilePath = "File/BattleLevel_EnemyA_Fifth_01.txt";

  /// <summary>
  /// 敵キャラA 第６フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyASixthPhase01FilePath = "File/BattleLevel_EnemyA_Sixth_01.txt";

  /// <summary>
  /// 敵キャラA 第６フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyASixthPhase02FilePath = "File/BattleLevel_EnemyA_Sixth_02.txt";

  /// <summary>
  /// 敵キャラB 第１フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBFirstPhase01FilePath = "File/BattleLevel_EnemyB_First_01.txt";

  /// <summary>
  /// 敵キャラB 第１フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBFirstPhase02FilePath = "File/BattleLevel_EnemyB_First_02.txt";

  /// <summary>
  /// 敵キャラB 第２フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBSecondPhase01FilePath = "File/BattleLevel_EnemyB_Second_01.txt";

  /// <summary>
  /// 敵キャラB 第２フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBSecondPhase02FilePath = "File/BattleLevel_EnemyB_Second_02.txt";

  /// <summary>
  /// 敵キャラB 第３フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBThirdPhase01FilePath = "File/BattleLevel_EnemyB_Third_01.txt";

  /// <summary>
  /// 敵キャラB 第３フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBThirdPhase02FilePath = "File/BattleLevel_EnemyB_Third_02.txt";

  /// <summary>
  /// 敵キャラB 第４フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBFourthPhase01FilePath = "File/BattleLevel_EnemyB_Fourth_01.txt";

  /// <summary>
  /// 敵キャラB 第４フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBFourthPhase02FilePath = "File/BattleLevel_EnemyB_Fourth_02.txt";

  /// <summary>
  /// 敵キャラB 第４フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBFourthPhase03FilePath = "File/BattleLevel_EnemyB_Fourth_03.txt";

  /// <summary>
  /// 敵キャラB 第４フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBFourthPhase04FilePath = "File/BattleLevel_EnemyB_Fourth_04.txt";

  /// <summary>
  /// 敵キャラB 第４フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBFourthPhase05FilePath = "File/BattleLevel_EnemyB_Fourth_05.txt";

  /// <summary>
  /// 敵キャラB 第５フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBFifthPhase01FilePath = "File/BattleLevel_EnemyB_Fifth_01.txt";

  /// <summary>
  /// 敵キャラB 第５フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBFifthPhase02FilePath = "File/BattleLevel_EnemyB_Fifth_02.txt";

  /// <summary>
  /// 敵キャラB 第５フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBFifthPhase03FilePath = "File/BattleLevel_EnemyB_Fifth_03.txt";

  /// <summary>
  /// 敵キャラB 第５フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBFifthPhase04FilePath = "File/BattleLevel_EnemyB_Fifth_04.txt";

  /// <summary>
  /// 敵キャラB 第６フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBSixthPhase01FilePath = "File/BattleLevel_EnemyB_Sixth_01.txt";

  /// <summary>
  /// 敵キャラB 第６フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBSixthPhase02FilePath = "File/BattleLevel_EnemyB_Sixth_02.txt";

  /// <summary>
  /// 敵キャラB 第６フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBSixthPhase03FilePath = "File/BattleLevel_EnemyB_Sixth_03.txt";

  /// <summary>
  /// 敵キャラB 第６フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBSixthPhase04FilePath = "File/BattleLevel_EnemyB_Sixth_04.txt";

  /// <summary>
  /// 敵キャラB 第６フェーズ情報ファイルパス
  /// </summary>
  const char* kEnemyBSixthPhase05FilePath = "File/BattleLevel_EnemyB_Sixth_05.txt";

  /// <summary>
  /// 通常弾画像パス
  /// </summary>
  const char* kImageNormalBulletPath = "Asset/Image/bullet_normal.png";

  /// <summary>
  /// 白い弾画像パス
  /// </summary>
  const char* kImageWhiteBallBulletPath = "Asset/Image/bullet_whiteball.png";

  /// <summary>
  /// 青弾画像パス
  /// </summary>
  const char* kImageBlueBallBulletPath = "Asset/Image/bullet_blueball.png";

  /// <summary>
  /// 紫弾画像パス
  /// </summary>
  const char* kImagePurpleBallBulletPath = "Asset/Image/bullet_purpleball.png";

  /// <summary>
  /// 赤弾画像パス
  /// </summary>
  const char* kImageRedBallBulletPath = "Asset/Image/bullet_redball.png";

  /// <summary>
  /// 黄弾画像パス
  /// </summary>
  const char* kImageYellowBallBulletPath = "Asset/Image/bullet_yellowball.png";

  /// <summary>
  /// 緑弾画像パス
  /// </summary>
  const char* kImageGreenBallBulletPath = "Asset/Image/bullet_greenball.png";

  /// <summary>
  /// プレイヤー死亡演出 画像パス
  /// </summary>
  const char* kImagePlayerDeathPath = "Asset/Image/white_circle.png";

  /// <summary>
  /// 爆発 画像パス
  /// </summary>
  const char* kImageExplosionPath = "Asset/Image/explosion.png";

  /// <summary>
  /// プレイヤー復活 画像パス
  /// </summary>
  const char* kImageRebornPath = "Asset/Image/player_reborn.png";

  /// <summary>
  /// 敵の爆破 画像パス
  /// </summary>
  const char* kImageEnemyExplosionPath = "Asset/Image/enemy_explosion.png";

  /// <summary>
  /// 敵の破裂 画像パス
  /// </summary>
  const char* kImageEnemyRupturePath = "Asset/Image/enemy_rupture.png";

  /// <summary>
  /// アイテム生成 画像パス
  /// </summary>
  const char* kImageItemSummonPath = "Asset/Image/item_summon.png";

  /// <summary>
  /// 通常弾消滅 画像パス
  /// </summary>
  const char* kImageDisappearNormalPath = "Asset/Image/disappear_normal.png";

  /// <summary>
  /// 青弾消滅 画像パス
  /// </summary>
  const char* kImageDisappearBlueBallPath = "Asset/Image/disappear_blueball.png";

  /// <summary>
  /// 紫弾消滅 画像パス
  /// </summary>
  const char* kImageDisappearPurpleBallPath = "Asset/Image/disappear_purpleball.png";

  /// <summary>
  /// 赤弾消滅 画像パス
  /// </summary>
  const char* kImageDisappearRedBallPath = "Asset/Image/disappear_redball.png";

  /// <summary>
  /// 黄弾消滅 画像パス
  /// </summary>
  const char* kImageDisappearYellowBallPath = "Asset/Image/disappear_yellowball.png";

  /// <summary>
  /// 緑弾消滅 画像パス
  /// </summary>
  const char* kImageDisappearGreenBallPath = "Asset/Image/disappear_greenball.png";

  /// <summary>
  /// 数字・記号 画像パス
  /// </summary>
  const char* kImageNumberPath = "Asset/Image/number.png";

  /// <summary>
  /// ボタン選択音源パス
  /// </summary>
  const char* kSoundButtonSelectPath = "Asset/Sound/maou_se_system45.mp3";

  /// <summary>
  /// ボタン押下音源パス
  /// </summary>
  const char* kSoundButtonPushPath = "Asset/Sound/game_start.mp3";

  /// <summary>
  /// カットイン音源パス
  /// </summary>
  const char* kSoundCutInPath = "Asset/Sound/maou_se_magical10.mp3";

  /// <summary>
  /// 射撃音源パス
  /// </summary>
  const char* kSoundShootPath = "Asset/Sound/laser1.mp3";

  /// <summary>
  /// プレイヤー死亡直前 音源パス
  /// </summary>
  const char* kSoundPlayerDeathPath = "Asset/Sound/maou_se_magical13.mp3";

  /// <summary>
  /// プレイヤー爆発 音源パス
  /// </summary>
  const char* kSoundPlayerExplosionPath = "Asset/Sound/maou_se_magical12.mp3";

  /// <summary>
  /// プレイヤー復活 音源パス
  /// </summary>
  const char* kSoundPlayerRebornPath = "Asset/Sound/strange_wave.mp3";

  /// <summary>
  /// 敵の爆破 音源パス
  /// </summary>
  const char* kSoundEnemyExplosionPath = "Asset/Sound/大パンチ.mp3";

  /// <summary>
  /// アイテム獲得 音源パス
  /// </summary>
  const char* kSoundGetItemPath = "Asset/Sound/maou_se_magic_fire08.mp3";

  /// <summary>
  /// ゲームオーバー 音源パス
  /// </summary>
  const char* kSoundGameOverPath = "Asset/Sound/maou_se_inst_bass13.mp3";

  /// <summary>
  /// ゲームクリア 音源パス
  /// </summary>
  const char* kSoundGameClearPath = "Asset/Sound/maou_game_jingle05.mp3";

  /// <summary>
  /// バトルBGM音源パス
  /// </summary>
  const char* kSoundBattleBgmPath = "Asset/Sound/maou_bgm_orchestra12.mp3";

  /// <summary>
  /// カットイン用　フェーズ名 中ボス
  /// </summary>
  const char* kBossPhaseNameA = "Mid-Boss";

  /// <summary>
  /// カットイン用　フェーズ名 大ボス
  /// </summary>
  const char* kBossPhaseNameB = "Final Boss";

  /// <summary>
  /// サウンドボリューム初期値
  /// </summary>
  const int kSoundInitialVolume = 128;

  /// <summary>
  /// サウンドボリューム調整値
  /// </summary>
  const int kSoundAdjustVolume = 10;

  /// <summary>
  /// プレイヤーキャラ 実際の幅
  /// </summary>
  const int kPlayerWidth = 8;

  /// <summary>
  /// 敵キャラA 実際の幅
  /// </summary>
  const int kEnemyAWidth = 204;

  /// <summary>
  /// 敵キャラB 実際の幅
  /// </summary>
  const int kEnemyBWidth = 222;

  /// <summary>
  /// 残機UPアイテム 実際の幅
  /// </summary>
  const int kItemAWidth = 56;

  /// <summary>
  /// パワーUPアイテム 実際の幅
  /// </summary>
  const int kItemBWidth = 56;

  /// <summary>
  /// ヒットポイントが「0」
  /// </summary>
  const int kCharacterDeath = 0;

  /// <summary>
  /// べき乗
  /// </summary>
  const float kSquared = 2.0f;

  /// <summary>
  /// 12.5%
  /// </summary>
  const float kOneEighth = 0.125f;

  /// <summary>
  /// キャラクター情報 インデックス
  /// </summary>
  const int kCharacterDataFile = 0;

  /// <summary>
  /// ヒットポイント情報 インデックス
  /// </summary>
  const int kHitPointFile = 1;

  /// <summary>
  /// プレイヤー総分割数
  /// </summary>
  const int kPlayerDivTotalNum = 12;

  /// <summary>
  /// プレイヤー分割数 横方向
  /// </summary>
  const int kPlayerDivX = 3;

  /// <summary>
  /// プレイヤー分割数 縦方向
  /// </summary>
  const int kPlayerDivY = 4;

  /// <summary>
  /// プレイヤーサイズ 幅
  /// </summary>
  const int kPlayerSizeWidth = 64;

  /// <summary>
  /// プレイヤーサイズ 高さ
  /// </summary>
  const int kPlayerSizeHeight = 64;

  /// <summary>
  /// プレイヤー 使用インデックス
  /// </summary>
  const int kPlayerIndex = 10;

  /// <summary>
  /// プレイヤー 使用インデックス
  /// </summary>
  const int kPlayerFrontIndex = 1;

  /// <summary>
  /// プレイヤー ロード時使用インデックス最小値
  /// </summary>
  const int kPlayerMinIndex = 3;

  /// <summary>
  /// プレイヤー ロード時使用インデックス最大値
  /// </summary>
  const int kPlayerMaxIndex = 5;

  /// <summary>
  /// 数字分割数 横方向
  /// </summary>
  const int kNumberDivX = 13;

  /// <summary>
  /// 数字分割数 縦方向
  /// </summary>
  const int kNumberDivY = 1;

  /// <summary>
  /// 数字 幅
  /// </summary>
  const int kNumberWidth = 16;

  /// <summary>
  /// 数字 高さ
  /// </summary>
  const int kNumberHeight = 32;

  /// <summary>
  /// テキストインデックス最小値
  /// </summary>
  const int kStringMinIndex = 0;

  /// <summary>
  /// ロード時の画像表示位置
  /// </summary>
  const int kLoadHandlePosX = 925;

  /// <summary>
  /// ロード時の画像表示位置
  /// </summary>
  const int kLoadHandlePosY = 685;

  /// <summary>
  /// 画像表示拡大率
  /// </summary>
  const float kExRate = 0.75f;

  /// <summary>
  /// 画像表示角度
  /// </summary>
  const float kAngle = 0.0f;

  /// <summary>
  /// ロード時インデックス変更待機時間
  /// </summary>
  const float kLoadWaitTime = 0.3f;

  /// <summary>
  /// プレイヤー 発射位置調整
  /// </summary>
  const float kPlayerShootPosY = 45.0f;

  /// <summary>
  /// 画面揺らし量
  /// </summary>
  const float kScreenShakeValue = 5.0f;

  /// <summary>
  /// 敵爆破エフェクト 調整値
  /// </summary>
  const int kEnemyExplosionPos01X = 65;

  /// <summary>
  /// 敵爆破エフェクト 調整値
  /// </summary>
  const int kEnemyExplosionPos02X = 50;

  /// <summary>
  /// 敵爆破エフェクト 調整値
  /// </summary>
  const int kEnemyExplosionPos01Y = 30;

  /// <summary>
  /// 敵爆破エフェクト 調整値
  /// </summary>
  const int kEnemyExplosionPos02Y = 35;

  /// <summary>
  /// 敵爆破エフェクト 拡大率調整値
  /// </summary>
  const float kEnemyExplosionExRateAdjust = 0.1f;

  /// <summary>
  /// 敵爆破エフェクト 拡大率最大値
  /// </summary>
  const float kEnemyExplosionExRateMax = 6.0f;

  /// <summary>
  /// ゲームクリア後のプレイヤー移動量
  /// </summary>
  const int kPlayerMoveValueX = 6;

  /// <summary>
  /// ゲームクリア後のプレイヤー移動量
  /// </summary>
  const int kPlayerMoveValueY = 6;

  /// <summary>
  /// ゲームクリア後のプレイヤー移動量
  /// </summary>
  const int kPlayerBackward1 = 6;

  /// <summary>
  /// ゲームクリア後のプレイヤー移動量 微量
  /// </summary>
  const int kPlayerBackward2 = 2;

  /// <summary>
  /// ゲームクリア後のプレイヤー移動量
  /// </summary>
  const int kPlayerForward = 12;

  /// <summary>
  /// 後退処理待機時間
  /// </summary>
  const float kBackwardWaitTime = 0.2f;

  /// <summary>
  /// 最大後退回数
  /// </summary>
  const int kBackwardMaxCount = 4;

  /// <summary>
  /// ゲームオーバー処理待機時間
  /// </summary>
  const float kGameOverWaitTime = 0.5f;

  /// <summary>
  /// 最大値をインデックスに変換
  /// </summary>
  const int kOne = 1;

  /// <summary>
  /// ファイルが開くことができた
  /// </summary>
  const int kFileOpen = 0;

  /// <summary>
  /// 1000倍
  /// </summary>
  const float kThousand = 1000.0f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> タスクID </param>
/// <param name=""> タスクマネージャ </param>
/// <returns></returns>
BattleLevel::BattleLevel(TaskId task_id, TaskManager& task_manager)
  : Level(task_id, task_manager)
  , current_phase_(PhaseType::kNone)
  , load_file_phase_(LoadFilePhase::kGameData)
  , prepare_phase_(PreparePhase::kRegisterBullet)
  , bullet_phase_(RegisterBulletPhase::kNormal)
  , barrage_phase_(RegisterBarragePhase::kAllDirection)
  , sound_phase_(RegisterSoundPhase::kButtonSelect)
  , effect_phase_(RegisterEffectPhase::kPlayerDeath)
  , battle_phase_(BattlePhase::kTransitionEnemyA)
  , game_over_phase_(GameOverPhase::kStopEnemy)
  , game_clear_phase_(GameClearPhase::kTaskAdd)
  , move_phase_(PlayerMovePhase::kBackward1)
  , finalize_phase_(FinalizePhaseType::kUnloadTask)
  , field_manager_(nullptr)
  , player_(nullptr)
  , player_controller_(nullptr)
  , bullet_manager_(nullptr)
  , sound_manager_(nullptr)
  , enemy_a_(nullptr)
  , enemy_b_(nullptr)
  , barrage_manager_(nullptr)
  , hp_bar_manager_(nullptr)
  , game_mode_(nullptr)
  , item_extend_(nullptr)
  , item_power_up_(nullptr)
  , cut_in_manager_(nullptr)
  , back_ground_(nullptr)
  , effect_manager_(nullptr)
  , game_over_(nullptr)
  , game_clear_(nullptr)
  , menu_controller_(nullptr)
  , game_timer_(nullptr)
  , pause_ui_(nullptr)
  , sound_setup_(nullptr)
  , volume_controller_(nullptr)
  , debug_menu_(nullptr)
  , sound_volume_list_()
  , bullet_list_()
  , barrage_list_()
  , enemy_a_phase_()
  , enemy_b_phase_()
  , display_string_()
  , player_handle_()
  , string_index_(0)
  , handle_index_(0)
  , accumulate_time_(0.0f)
  , move_count_(0)
  , alpha_(0)
  , alpha_adjust_(0)
  , font_handle_(0)
  , is_active_(true)
  , transition_level_(TaskId::kResultLevel) {

  // コンソールに出力
  std::cout << "BattleLevel コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
BattleLevel::~BattleLevel() {

  // コンソールに出力
  std::cout << "~BattleLevel デストラクタ" << std::endl;
}

/// <summary>
/// 初期化処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializePhase(float process_time) {

  bool is_success = false;

  // 処理時間をリセットしておく
  accumulate_time_ = kResetTime;
  // 透過率 初期設定
  alpha_ = kAlphaMin;
  alpha_adjust_ = kAlphaAdjustInit;

  // フォント情報生成
  FontInfo* font_info = FontInfo::GetFontInfoInstance();
  if (font_info == nullptr) {
    return false;
  }
  int result = font_info->CreateFontInfo(kFontFilePath);
  font_handle_ = font_info->GetFontInfo(kFontName, kFontSize, kFontWidth, DX_FONTTYPE_NORMAL);

  // ロード処理 初期設定
  is_success = InitializeLoadScreen();
  if (!is_success) {
    return false;
  }

  // フィールドマネージャ 初期設定
  is_success = InitializeFieldManager();
  if (!is_success) {
    return false;
  }

  // プレイヤー 初期設定
  is_success = InitializePlayer();
  if (!is_success) {
    return false;
  }

  // プレイヤーコントローラ 初期設定
  is_success = InitializePlayerController();
  if (!is_success) {
    return false;
  }

  // バレットマネージャ 初期設定
  is_success = InitializeBulletManager();
  if (!is_success) {
    return false;
  }

  // サウンドマネージャ 初期設定
  is_success = InitializeSoundManager();
  if (!is_success) {
    return false;
  }

  // 敵キャラA 初期設定
  is_success = InitializeEnemyA();
  if (!is_success) {
    return false;
  }

  // 敵キャラB 初期設定
  is_success = InitializeEnemyB();
  if (!is_success) {
    return false;
  }

  // バレッジマネージャ 初期設定
  is_success = InitializeBarrageManager();
  if (!is_success) {
    return false;
  }

  // ヒットポイントバーマネージャ 初期設定
  is_success = InitializeHitPointBarManager();
  if (!is_success) {
    return false;
  }

  // 残機UPアイテム 初期設定
  is_success = InitializeItemExtend();
  if (!is_success) {
    return false;
  }

  // パワーUPアイテム 初期設定
  is_success = InitializeItemPowerUp();
  if (!is_success) {
    return false;
  }

  // ゲームモード 初期設定
  is_success = InitializeGameMode();
  if (!is_success) {
    return false;
  }

  // カットインマネージャ 初期設定
  is_success = InitializeCutInManager();
  if (!is_success) {
    return false;
  }

  // バックグラウンドサイド 初期設定
  is_success = InitializeBackGroundSide();
  if (!is_success) {
    return false;
  }

  // エフェクトマネージャ 初期設定
  is_success = InitializeEffectManager();
  if (!is_success) {
    return false;
  }

  // ゲームオーバー処理 初期設定
  is_success = InitializeGameOver();
  if (!is_success) {
    return false;
  }

  // ゲームクリア処理 初期設定
  is_success = InitializeGameClear();
  if (!is_success) {
    return false;
  }

  // メニューコントローラ 初期設定
  is_success = InitializeMenuController();
  if (!is_success) {
    return false;
  }

  // ゲームタイマー 初期設定
  is_success = InitializeGameTimer();
  if (!is_success) {
    return false;
  }

  // ポーズUI処理 初期設定
  is_success = InitializePauseUi();
  if (!is_success) {
    return false;
  }

  // サウンド設定 初期設定
  is_success = InitializeSoundSetup();
  if (!is_success) {
    return false;
  }

  // ボリュームコントローラ 初期設定
  is_success = InitializeVolumeController();
  if (!is_success) {
    return false;
  }

  // デバッグメニュー 初期設定
  is_success = InitializeDebugMenu();
  if (!is_success) {
    return false;
  }

  // 現在のフェーズを「ロード処理」に変更
  ChangeCurrentPhase(PhaseType::kLoadFile);
  return true;
}

/// <summary>
/// 毎フレームの処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool BattleLevel::UpdatePhase(float process_time) {

  bool is_finish = false;

  //-----------------------------
  //ロード時専用処理
  //-----------------------------
  switch (current_phase_) {
  case PhaseType::kLoadFile:
  case PhaseType::kPrepare:
  case PhaseType::kStartPrepare: {

    // 累積時間を加算する
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kLoadWaitTime) {

      // 累積時間をリセット
      accumulate_time_ = kResetTime;

      // プレイヤー画像インデックス変更
      switch (handle_index_) {
      case kPlayerMinIndex: {
        handle_index_ = kPlayerMaxIndex;
        break;
      }
      case kPlayerMaxIndex: {
        handle_index_ = kPlayerMinIndex;
        break;
      }
      }
      // 表示文字列のインデックス変更
      ++string_index_;
      if (string_index_ >= kLoadTextMaxSize) {
        string_index_ = kStringMinIndex;
      }
    }
    break;
  }
  }

  switch (current_phase_) {
  case PhaseType::kLoadFile: {
    //------------------------------
    //設定ファイル読み込みフェーズ
    //------------------------------
    is_finish = LoadSetupFile();
    if (is_finish) {
      // 現在のフェーズを「準備」に変更
      ChangeCurrentPhase(PhaseType::kPrepare);
    }
    break;
  }
  case PhaseType::kPrepare: {
    //------------------------------
    //準備フェーズ
    //------------------------------
    is_finish = PrepareGame();
    if (is_finish) {
      // 現在のフェーズを「起動準備」に変更
      ChangeCurrentPhase(PhaseType::kStartPrepare);
    }
    break;
  }
  case PhaseType::kStartPrepare: {
    //------------------------------
    //起動準備フェーズ
    //------------------------------
    // 処理時間をリセットしておく
    accumulate_time_ = kResetTime;

    // すべてを「起動フェーズ」に変更する
    field_manager_->ChangeFieldPhase(Field::PhaseType::kStartUp);
    player_->ChangeCurrentPhase(Player::PhaseType::kStartUp);
    enemy_a_->ChangeCurrentPhase(EnemyA::PhaseType::kStartUp);
    game_mode_->ChangeCurrentPhase(GameMode::PhaseType::kStartUp);
    back_ground_->ChangeCurrentPhase(BackGroundSide::PhaseType::kStartUp);
    game_timer_->ChangeCurrentPhase(GameTimer::PhaseType::kStartUp);
    // 現在のフェーズを「起動」に変更
    ChangeCurrentPhase(PhaseType::kStartUp);
    break;
  }
  case PhaseType::kStartUp: {
    //------------------------------
    //起動フェーズ
    //------------------------------
    // フェードイン処理完了後、現在のフェーズを「プレイ待機」に変更
    is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 処理時間をリセットしておく
      accumulate_time_ = kResetTime;
      // 透過率調整値の符号を反転しておく
      alpha_adjust_ = kMinusSign * alpha_adjust_;

      // バトル中BGMサウンド生成
      sound_manager_->CreateSound(SoundId::kBattleBgm);
      // 現在のフェーズを「プレイ開始」に変更
      ChangeCurrentPhase(PhaseType::kStartPlay);
    }
    break;
  }
  case PhaseType::kStartPlay: {
    //------------------------------
    //プレイ開始フェーズ
    //------------------------------
    player_->ChangeCurrentPhase(Player::PhaseType::kProcess);
    enemy_a_->ChangeCurrentPhase(EnemyA::PhaseType::kProcess);
    // タイマー開始
    game_timer_->ChangeCurrentPhase(GameTimer::PhaseType::kProcess);
    // 現在のフェーズを「プレイ中」に変更
    ChangeCurrentPhase(PhaseType::kPlay);
    break;
  }
  case PhaseType::kPlay: {
    //------------------------------
    //プレイ中フェーズ
    //------------------------------
    bool is_check = GetWindowActiveFlag();
    if (!is_check) {

      // アクティブフラグをOFFにする
      is_active_ = false;

      std::cout << "BattleLevel Playフェーズ" << std::endl;

      // ポーズ処理実行
      // プレイヤーコントローラをタスクマネージャから降ろす
      Task* task = task_manager_.ReleaseTask(TaskId::kPlayerController);
      // タスクマネージャにポーズメニューを積む
      bool is_check = task_manager_.AddTask(pause_ui_);
      if (!is_check) {
        return false;
      }
      // ポーズメニューのフェーズを「初期化処理」に変更
      pause_ui_->ChangeCurrentPhase(PauseUi::PhaseType::kInitialize);

      // タスクマネージャにメニューコントローラを積む
      task_manager_.AddTask(menu_controller_);

      // サウンド停止
      sound_manager_->PausePlaySound();

      // 現在のフェーズを「ポーズメニュー」に変更
      ChangeCurrentPhase(PhaseType::kPause);

      break;
    }

    // アクティブフラグをONにする
    is_active_ = true;

    break;
  }
  case PhaseType::kStopPrepare: {
    //------------------------------
    //停止準備フェーズ
    //------------------------------
    // ゲーム画面上のすべての時間を停止する
    barrage_manager_->ChangeCurrentPhase(BarrageManager::PhaseType::kStop);
    bullet_manager_->ChangeCurrentPhase(BulletManager::PhaseType::kStop);
    field_manager_->ChangeCurrentPhase(FieldManager::PhaseType::kStop);
    enemy_a_->ChangeCurrentPhase(EnemyA::PhaseType::kStop);
    enemy_b_->ChangeCurrentPhase(EnemyB::PhaseType::kStop);
    // 現在のフェーズを「プレイ中」に変更
    ChangeCurrentPhase(PhaseType::kPlay);
    break;
  }
  case PhaseType::kPause: 
  case PhaseType::kDebugMenu: {
    //------------------------------
    //ポーズ・デバッグメニューフェーズ
    //------------------------------

    std::cout << "BattleLevel Pauseフェーズ" << std::endl;

    // ゲーム画面上のすべての時間を停止する
    barrage_manager_->ChangeCurrentPhase(BarrageManager::PhaseType::kStop);
    bullet_manager_->ChangeCurrentPhase(BulletManager::PhaseType::kStop);
    field_manager_->ChangeCurrentPhase(FieldManager::PhaseType::kStop);
    cut_in_manager_->ChangeCurrentPhase(CutInManager::PhaseType::kStop);
    hp_bar_manager_->ChangeCurrentPhase(HitPointBarManager::PhaseType::kStop);
    effect_manager_->ChangeCurrentPhase(EffectManager::PhaseType::kStop);
    game_timer_->ChangeCurrentPhase(GameTimer::PhaseType::kStop);
    enemy_a_->ChangeCurrentPhase(EnemyA::PhaseType::kStop);
    enemy_b_->ChangeCurrentPhase(EnemyB::PhaseType::kStop);
    player_->ChangeCurrentPhase(Player::PhaseType::kStopTime);

    is_finish = item_extend_->IsFinish();
    if (!is_finish) {
      item_extend_->ChangeCurrentPhase(ItemExtend::PhaseType::kStop);
    }
    is_finish = item_power_up_->IsFinish();
    if (!is_finish) {
      item_power_up_->ChangeCurrentPhase(ItemPowerUp::PhaseType::kStop);
    }

    switch (current_phase_) {
    case PhaseType::kPause: {
      // 現在のフェーズを「ポーズメニュー操作」に変更
      ChangeCurrentPhase(PhaseType::kPauseWait);
      break;
    }
    case PhaseType::kDebugMenu: {
      // 現在のフェーズを「デバッグメニュー操作」に変更
      ChangeCurrentPhase(PhaseType::kDebugMenuWait);
      break;
    }
    }
    break;
  }
  case PhaseType::kPauseWait:
  case PhaseType::kSoundSetting:
  case PhaseType::kDebugMenuWait: {
    //------------------------------
    //デバッグメニュー操作フェーズ
    //------------------------------
    bool is_check = GetWindowActiveFlag();
    if (!is_check) {
      // アクティブフラグをOFFにする
      is_active_ = false;
    }
    else {
      // アクティブフラグをONにする
      is_active_ = true;
    }

    break;
  }
  case PhaseType::kGameOver: {
    //------------------------------
    //ゲームオーバーフェーズ
    //------------------------------
    ExecuteGameOver(process_time);
    break;
  }
  case PhaseType::kGameClear: {
    //------------------------------
    //ゲームクリアフェーズ
    //------------------------------
    ExecuteGameClear(process_time);
    break;
  }
  case PhaseType::kBeforeTransition: {
    //------------------------------
    //レベル遷移前フェーズ
    //------------------------------
    barrage_manager_->FinishGenerateBarrage();
    field_manager_->ChangeCurrentPhase(FieldManager::PhaseType::kProcess);
    field_manager_->ChangeFieldPhase(Field::PhaseType::kLevelTransition);
    player_->ChangeCurrentPhase(Player::PhaseType::kBeforeFinalize);
    bullet_manager_->ChangeCurrentPhase(BulletManager::PhaseType::kBeforeFinalize);
    game_mode_->ChangeCurrentPhase(GameMode::PhaseType::kBeforeFinalize);
    back_ground_->ChangeCurrentPhase(BackGroundSide::PhaseType::kFinalizeBefore);
    game_timer_->ChangeCurrentPhase(GameTimer::PhaseType::kBeforeFinalize);
    hp_bar_manager_->ChangeCurrentPhase(HitPointBarManager::PhaseType::kProcess);
    hp_bar_manager_->FinishAllHitPointBar();
    effect_manager_->ChangeCurrentPhase(EffectManager::PhaseType::kFinalize);
    sound_manager_->FinishSoundBgm();

    bool is_finalized = enemy_a_->IsFinalized();
    if (!is_finalized) {
      // 敵キャラAを終了処理前フェーズに遷移させる
      enemy_a_->ChangeCurrentPhase(EnemyA::PhaseType::kBeforeFinalize);
    }
    is_finalized = enemy_b_->IsFinalized();
    if (!is_finalized) {
      // 敵キャラBを終了処理前フェーズに遷移させる
      enemy_b_->ChangeCurrentPhase(EnemyB::PhaseType::kBeforeFinalize);
    }
    is_finalized = item_extend_->IsFinalized();
    if (!is_finalized) {
      // アイテムを終了処理前フェーズに遷移させる
      item_extend_->ChangeCurrentPhase(ItemExtend::PhaseType::kBeforeFinalize);
    }
    is_finalized = item_power_up_->IsFinalized();
    if (!is_finalized) {
      // アイテムを終了処理前フェーズに遷移させる
      item_power_up_->ChangeCurrentPhase(ItemPowerUp::PhaseType::kBeforeFinalize);
    }
    is_finalized = cut_in_manager_->IsDrawFinalized();
    if (!is_finalized) {
      cut_in_manager_->ChangeCurrentPhase(CutInManager::PhaseType::kProcess);
      cut_in_manager_->ChangeDrawPhase(CutInManager::DrawPhase::kFinalizeBefore);
    }

    // 現在のフェーズを「レベル遷移前」に変更
    ChangeCurrentPhase(PhaseType::kLevelTransition);

    GameInfo* game_info = GameInfo::GetGameInfoInstance();
    if (game_info == nullptr) {
      break;
    }
    if (!game_info->GetGameClear()) {
      break;
    }
    // ゲームクリア処理のフェーズを「終了処理前」に変更
    game_clear_->ChangeGameClearPhase(FinishUiGameClear::GameClearPhase::kBeforeFinalize);

    break;
  }
  case PhaseType::kLevelTransition: {
    //------------------------------
    //レベル遷移フェーズ
    //------------------------------
    // フェードアウト処理完了後、現在のフェーズを「終了処理」に変更
    is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //------------------------------
    //終了処理フェーズ
    //------------------------------
    // 指定のレベルに切り替える
    ChangeLevel(transition_level_);
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
  
  return false;
}

/// <summary>
/// 毎フレームの描画処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::RenderPhase() {

  switch (current_phase_) {
  case PhaseType::kLoadFile:
  case PhaseType::kPrepare:
  case PhaseType::kStartPrepare: {

    // 文字列の描画処理
    DrawStringToHandle(kLoadStringPosX, kLoadStringPosY, display_string_[string_index_].c_str(), kStringForeColor, font_handle_);
    // 画像の描画処理
    DrawRotaGraph(kLoadHandlePosX, kLoadHandlePosY, kExRate, kAngle, player_handle_.at(handle_index_), true);

    break;
  }
  }
}

/// <summary>
/// 終了処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool BattleLevel::FinishPhase(float process_time) {

  switch (finalize_phase_) {
  case FinalizePhaseType::kUnloadTask: {
    //-------------------------------
    //タスクを降ろすフェーズ
    //-------------------------------
    // フィールド処理の破棄
    if (field_manager_ != nullptr) {
      field_manager_->FieldDispose();
    }
    // 弾の破棄
    if (bullet_manager_ != nullptr) {
      bullet_manager_->DisposeBullet();
    }
    // サウンドの破棄
    if (sound_manager_ != nullptr) {
      sound_manager_->DisposeSound();
    }
    // バレッジの破棄
    if (barrage_manager_ != nullptr) {
      barrage_manager_->DisposeGenerateBarrage();
      barrage_manager_->DisposeBarrageList();
    }
    // ヒットポイントバーの破棄
    if (hp_bar_manager_ != nullptr) {
      hp_bar_manager_->DisposeHitPointBar();
    }
    // カットインの破棄
    if (cut_in_manager_ != nullptr) {
      cut_in_manager_->DisposeCutIn();
    }
    // エフェクトの破棄
    if (effect_manager_ != nullptr) {
      effect_manager_->DisposeEffect();
    }

    // フォント情報生成
    FontInfo* font_info = FontInfo::GetFontInfoInstance();
    if (font_info == nullptr) {
      return false;
    }
    // フォント破棄
    font_info->ReleaseFontInfo(kFontFilePath);

    // 画像の破棄
    for (int i = 0; i < kPlayerDivTotalNum; ++i) {
      DeleteGraph(player_handle_[i]);                // プレイヤー
    }
    for (int i = 0; i < kImageNumberDivNum; ++i) {
      DeleteGraph(game_timer_->GetGraphicHandle(i)); // 数字・記号
    }
    DeleteGraph(enemy_a_->GetGraphicHandle());       // 敵キャラA
    DeleteGraph(enemy_b_->GetGraphicHandle());       // 敵キャラB
    DeleteGraph(item_extend_->GetGraphicHandle());   // アイテム
    DeleteGraph(item_power_up_->GetGraphicHandle()); // アイテム
    DeleteGraph(back_ground_->GetGraphicHandle());   // 背景

    TaskId task_id;
    Task* release_task = nullptr;

    // フィールドマネージャをタスクマネージャから降ろす
    if (field_manager_ != nullptr) {
      task_id = field_manager_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // プレイヤーをタスクマネージャから降ろす
    if (player_ != nullptr) {
      task_id = player_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // プレイヤーコントローラをタスクマネージャから降ろす
    if (player_controller_ != nullptr) {
      task_id = player_controller_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // バレットマネージャをタスクマネージャから降ろす
    if (bullet_manager_ != nullptr) {
      task_id = bullet_manager_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // サウンドマネージャをタスクマネージャから降ろす
    if (sound_manager_ != nullptr) {
      task_id = sound_manager_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // 敵キャラAをタスクマネージャから降ろす
    if (enemy_a_ != nullptr) {
      task_id = enemy_a_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // 敵キャラBをタスクマネージャから降ろす
    if (enemy_b_ != nullptr) {
      task_id = enemy_b_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // バレッジマネージャをタスクマネージャから降ろす
    if (barrage_manager_ != nullptr) {
      task_id = barrage_manager_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // ヒットポイントバーをタスクマネージャから降ろす
    if (hp_bar_manager_ != nullptr) {
      task_id = hp_bar_manager_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // ゲームモードをタスクマネージャから降ろす
    if (game_mode_ != nullptr) {
      task_id = game_mode_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // 残機UPアイテムをタスクマネージャから降ろす
    if (item_extend_ != nullptr) {
      task_id = item_extend_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // パワーUPアイテムをタスクマネージャから降ろす
    if (item_power_up_ != nullptr) {
      task_id = item_power_up_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // カットインマネージャをタスクマネージャから降ろす
    if (cut_in_manager_ != nullptr) {
      task_id = cut_in_manager_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // 背景をタスクマネージャから降ろす
    if (back_ground_ != nullptr) {
      task_id = back_ground_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // エフェクトマネージャをタスクマネージャから降ろす
    if (effect_manager_ != nullptr) {
      task_id = effect_manager_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // ゲームオーバーの破棄
    if (game_over_ != nullptr) {
      task_id = game_over_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // ゲームクリアの破棄
    if (game_clear_ != nullptr) {
      task_id = game_clear_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // メニューコントローラの破棄
    if (menu_controller_ != nullptr) {
      task_id = menu_controller_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // タイマーの破棄
    if (game_timer_ != nullptr) {
      task_id = game_timer_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // ポーズUIの破棄
    if (pause_ui_ != nullptr) {
      task_id = pause_ui_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // サウンド設定の破棄
    if (sound_setup_ != nullptr) {
      task_id = sound_setup_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // ボリュームコントローラの破棄
    if (volume_controller_ != nullptr) {
      task_id = volume_controller_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }
    // デバッグメニューの破棄
    if (debug_menu_ != nullptr) {
      task_id = debug_menu_->GetTaskId();
      release_task = task_manager_.ReleaseTask(task_id);
    }

    // 終了フェーズを「タスクを破棄する」に変更
    ChangeFinalizePhase(FinalizePhaseType::kDisposeTask);

    return false;
  }
  case FinalizePhaseType::kDisposeTask: {
    //-------------------------------
    //タスクを破棄するフェーズ
    //-------------------------------
    if (debug_menu_ != nullptr) {
      delete debug_menu_;
      debug_menu_ = nullptr;
    }
    if (volume_controller_ != nullptr) {
      delete volume_controller_;
      volume_controller_ = nullptr;
    }
    if (sound_setup_ != nullptr) {
      delete sound_setup_;
      sound_setup_ = nullptr;
    }
    if (pause_ui_ != nullptr) {
      delete pause_ui_;
      pause_ui_ = nullptr;
    }
    if (game_timer_ != nullptr) {
      delete game_timer_;
      game_timer_ = nullptr;
    }
    if (menu_controller_ != nullptr) {
      delete menu_controller_;
      menu_controller_ = nullptr;
    }
    if (game_clear_ != nullptr) {
      delete game_clear_;
      game_clear_ = nullptr;
    }
    if (game_over_ != nullptr) {
      delete game_over_;
      game_over_ = nullptr;
    }
    if (effect_manager_ != nullptr) {
      delete effect_manager_;
      effect_manager_ = nullptr;
    }
    if (back_ground_ != nullptr) {
      delete back_ground_;
      back_ground_ = nullptr;
    }
    if (cut_in_manager_ != nullptr) {
      delete cut_in_manager_;
      cut_in_manager_ = nullptr;
    }
    if (item_power_up_ != nullptr) {
      delete item_power_up_;
      item_power_up_ = nullptr;
    }
    if (item_extend_ != nullptr) {
      delete item_extend_;
      item_extend_ = nullptr;
    }
    if (game_mode_ != nullptr) {
      delete game_mode_;
      game_mode_ = nullptr;
    }
    if (hp_bar_manager_ != nullptr) {
      delete hp_bar_manager_;
      hp_bar_manager_ = nullptr;
    }
    if (barrage_manager_ != nullptr) {
      delete barrage_manager_;
      barrage_manager_ = nullptr;
    }
    if (enemy_b_ != nullptr) {
      delete enemy_b_;
      enemy_b_ = nullptr;
    }
    if (enemy_a_ != nullptr) {
      delete enemy_a_;
      enemy_a_ = nullptr;
    }
    if (sound_manager_ != nullptr) {
      delete sound_manager_;
      sound_manager_ = nullptr;
    }
    if (bullet_manager_ != nullptr) {
      delete bullet_manager_;
      bullet_manager_ = nullptr;
    }
    if (player_controller_ != nullptr) {
      delete player_controller_;
      player_controller_ = nullptr;
    }
    if (player_ != nullptr) {
      delete player_;
      player_ = nullptr;
    }
    if (field_manager_ != nullptr) {
      delete field_manager_;
      field_manager_ = nullptr;
    }
    break;
  }
  }

  return true;
}

/// <summary>
/// ロード処理 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeLoadScreen() {

  //------------------------------------
  //プレイヤーグラフィックハンドル 準備
  //------------------------------------
  // グラフィックハンドル
  int graphic_handle[kPlayerDivTotalNum];
  // 画像をロードする
  int result = LoadDivGraph(kImagePlayerPath, kPlayerDivTotalNum, kPlayerDivX, kPlayerDivY,
    kPlayerSizeWidth, kPlayerSizeHeight, graphic_handle, true);
  if (result == kLoadError) {
    return false;
  }

  // プレイヤーのグラフィックハンドルをセット
  for (int i = 0; i < kPlayerDivTotalNum; ++i) {
    player_handle_.push_back(graphic_handle[i]);
  }

  //------------------------------------
  //画面表示文字列 準備
  //------------------------------------
  std::string period = "";
  // 配列の作成
  for (int i = 0; i < kLoadTextMaxSize; ++i) {
    display_string_[i] = kDisplayLoadText + period;
    // ピリオドの数を追加
    period += static_cast<std::string>(kDisplayPeriod);
  }

  // インデックス初期化
  string_index_ = kStringMinIndex;
  handle_index_ = kPlayerMinIndex;

  return true;
}

/// <summary>
/// フィールドマネージャ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeFieldManager() {

  // フィールドマネージャ 生成
  field_manager_ = new FieldManager();
  if (field_manager_ == nullptr) {
    std::cout << "BattleLevel InitializeFieldManager：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // タスクマネージャに積む
  task_manager_.AddTask(field_manager_);

  return true;
}

/// <summary>
/// プレイヤー 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializePlayer() {

  // プレイヤー 生成
  player_ = new Player(*this);
  if (player_ == nullptr) {
    std::cout << "BattleLevel InitializePlayer：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  float radius = static_cast<float>(kPlayerWidth) / static_cast<float>(kHalfValue);
  // プレイヤーの各種設定
  player_->SetGraphicHandle(player_handle_[kPlayerIndex]);   // グラフィックハンドル
  player_->SetRadius(radius);                                // 半径
  player_->SetBulletId(BulletId::kNormal);                   // 弾の種類
  player_->SetBarrageId(BarrageId::kSingle);                 // 弾幕の種類

  // タスクマネージャに積む
  task_manager_.AddTask(player_);

  return true;
}

/// <summary>
/// プレイヤーコントローラ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializePlayerController() {

  // プレイヤーコントローラ 生成
  player_controller_ = new PlayerController(*this, player_);
  if (player_controller_ == nullptr) {
    std::cout << "BattleLevel InitializePlayerController：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // タスクマネージャに積む
  task_manager_.AddTask(player_controller_);

  return true;
}

/// <summary>
/// バレットマネージャ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeBulletManager() {

  // バレットマネージャ 生成
  bullet_manager_ = new BulletManager();
  if (bullet_manager_ == nullptr) {
    std::cout << "BattleLevel InitializeBulletManager：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // 透過率、調整値の設定
  bullet_manager_->SetAlpha(kAlphaMax);
  bullet_manager_->SetAlphaAdjust(kAlphaBulletAdjust);

  // タスクマネージャに積む
  task_manager_.AddTask(bullet_manager_);

  return true;
}

/// <summary>
/// サウンドマネージャ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeSoundManager() {

  // サウンドマネージャ 生成
  sound_manager_ = new SoundManager();
  if (sound_manager_ == nullptr) {
    std::cout << "BattleLevel InitializeSoundManager：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // タスクマネージャに積む
  task_manager_.AddTask(sound_manager_);

  return true;
}

/// <summary>
/// 敵キャラA 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeEnemyA() {

  // 画像のロード
  int graphic_handle = LoadGraph(kImageEnemyAPath);
  if (graphic_handle == kLoadError) {
    return false;
  }

  // 敵キャラA 生成
  enemy_a_ = new EnemyA(*this, *this, *this);
  if (enemy_a_ == nullptr) {
    std::cout << "BattleLevel InitializeEnemyA：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  float radius = static_cast<float>(kEnemyAWidth) / static_cast<float>(kHalfValue);
  // 敵キャラAの各種設定
  enemy_a_->SetGraphicHandle(graphic_handle);                     // グラフィックハンドル
  enemy_a_->SetRadius(radius);                                    // 半径
  enemy_a_->SetBulletId(BulletId::kBlueBall);                     // 弾の種類

  // タスクマネージャに積む
  task_manager_.AddTask(enemy_a_);

  // ロードファイル用管理表作成
  enemy_a_phase_[LoadFilePhase::kEnemyAFirstPhase] = EnemyA::ActionBarrageType::kFirstPhase_01;
  enemy_a_phase_[LoadFilePhase::kEnemyASecondPhase] = EnemyA::ActionBarrageType::kSecondPhase_01;
  enemy_a_phase_[LoadFilePhase::kEnemyAThirdPhase] = EnemyA::ActionBarrageType::kThirdPhase_01;
  enemy_a_phase_[LoadFilePhase::kEnemyAFourthPhase] = EnemyA::ActionBarrageType::kFourthPhase_01;
  enemy_a_phase_[LoadFilePhase::kEnemyAFifthPhase] = EnemyA::ActionBarrageType::kFifthPhase_01;
  enemy_a_phase_[LoadFilePhase::kEnemyASixthPhase] = EnemyA::ActionBarrageType::kSixthPhase_01;

  return true;
}

/// <summary>
/// 敵キャラB 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeEnemyB() {

  // 画像のロード
  int graphic_handle = LoadGraph(kImageEnemyBPath);
  if (graphic_handle == kLoadError) {
    return false;
  }

  // 敵キャラB 生成
  enemy_b_ = new EnemyB(*this, *this, *this);
  if (enemy_b_ == nullptr) {
    std::cout << "BattleLevel InitializeEnemyB：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  float radius = static_cast<float>(kEnemyBWidth) / static_cast<float>(kHalfValue);
  // 敵キャラBの各種設定
  enemy_b_->SetGraphicHandle(graphic_handle);                     // グラフィックハンドル
  enemy_b_->SetRadius(radius);                                    // 半径
  enemy_b_->SetBulletId(BulletId::kBlueBall);                     // 弾の種類

  // ロードファイル用管理表作成
  enemy_b_phase_[LoadFilePhase::kEnemyBFirstPhase] = EnemyB::ActionBarrageType::kFirstPhase_01;
  enemy_b_phase_[LoadFilePhase::kEnemyBSecondPhase] = EnemyB::ActionBarrageType::kSecondPhase_01;
  enemy_b_phase_[LoadFilePhase::kEnemyBThirdPhase] = EnemyB::ActionBarrageType::kThirdPhase_01;
  enemy_b_phase_[LoadFilePhase::kEnemyBFourthPhase] = EnemyB::ActionBarrageType::kFourthPhase_01;
  enemy_b_phase_[LoadFilePhase::kEnemyBFifthPhase] = EnemyB::ActionBarrageType::kFifthPhase_01;
  enemy_b_phase_[LoadFilePhase::kEnemyBSixthPhase] = EnemyB::ActionBarrageType::kSixthPhase_01;

  return true;
}

/// <summary>
/// バレッジマネージャ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeBarrageManager() {

  // バレッジマネージャ 生成
  barrage_manager_ = new BarrageManager();
  if (barrage_manager_ == nullptr) {
    std::cout << "BattleLevel InitializeBarrageManager：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // タスクマネージャに積む
  task_manager_.AddTask(barrage_manager_);

  return true;
}

/// <summary>
/// ヒットポイントバー 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeHitPointBarManager() {

  // ヒットポイントバー 生成
  hp_bar_manager_ = new HitPointBarManager();
  if (hp_bar_manager_ == nullptr) {
    return false;
  }
  // タスクマネージャに積む
  task_manager_.AddTask(hp_bar_manager_);

  return true;
}

/// <summary>
/// 残機UPアイテム 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeItemExtend() {

  // 画像のロード
  int graphic_handle = LoadGraph(kImageItemExtendPath);
  if (graphic_handle == kLoadError) {
    return false;
  }

  // 残機UPアイテム 生成
  item_extend_ = new ItemExtend(*this, *this, *this);
  if (item_extend_ == nullptr) {
    std::cout << "BattleLevel InitializeItemExtend：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  float radius = static_cast<float>(kItemAWidth) / static_cast<float>(kHalfValue);
  // グラフィックハンドルを渡す
  item_extend_->SetGraphicHandle(graphic_handle);
  item_extend_->SetRadius(radius);                   // 半径
  return true;
}

/// <summary>
/// パワーUPアイテム 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeItemPowerUp() {

  // 画像のロード
  int graphic_handle = LoadGraph(kImageItemPowerUpPath);
  if (graphic_handle == kLoadError) {
    return false;
  }

  // パワーUPアイテム 生成
  item_power_up_ = new ItemPowerUp(*this, *this, * this);
  if (item_extend_ == nullptr) {
    std::cout << "BattleLevel InitializeItemPowerUp：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  float radius = static_cast<float>(kItemBWidth) / static_cast<float>(kHalfValue);
  // グラフィックハンドルを渡す
  item_power_up_->SetGraphicHandle(graphic_handle);
  item_power_up_->SetRadius(radius);                 // 半径

  return true;
}

/// <summary>
/// ゲームモード 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeGameMode() {

  // ゲームモード 生成
  game_mode_ = new GameMode();
  if (game_mode_ == nullptr) {
    std::cout << "BattleLevel InitializeGameMode：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // グラフィックハンドルを取得し、渡す
  int graphic_handle = player_handle_.at(kPlayerFrontIndex);
  game_mode_->SetGraphicHandleLife(graphic_handle);
  graphic_handle = item_power_up_->GetGraphicHandle();
  game_mode_->SetGraphicHandlePowerUp(graphic_handle);

  // タスクマネージャに積む
  task_manager_.AddTask(game_mode_);

  return true;
}

/// <summary>
/// カットインマネージャ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeCutInManager() {

  // 画像のロード
  int graphic_handle = LoadGraph(kImageItemPowerUpPath);
  if (graphic_handle == kLoadError) {
    return false;
  }

  // カットインマネージャ 生成
  cut_in_manager_ = new CutInManager(*this);
  if (cut_in_manager_ == nullptr) {
    std::cout << "BattleLevel InitializeCutInManager：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // タスクマネージャに積む
  task_manager_.AddTask(cut_in_manager_);

  return true;
}

/// <summary>
/// バックグラウンドサイド 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeBackGroundSide() {

  // 画像のロード
  int graphic_handle = LoadGraph(kImageBackGroundSidePath);
  if (graphic_handle == kLoadError) {
    return false;
  }
  // バックグラウンドサイド 生成
  back_ground_ = new BackGroundSide();
  if (back_ground_ == nullptr) {
    std::cout << "BattleLevel InitializeBackGroundSide：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // グラフィックハンドルを渡す
  back_ground_->SetGraphicHandle(graphic_handle);

  // タスクマネージャに積む
  task_manager_.AddTask(back_ground_);

  return true;
}

/// <summary>
/// エフェクトマネージャ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeEffectManager() {

  // エフェクトマネージャの生成
  effect_manager_ = new EffectManager();
  if (effect_manager_ == nullptr) {
    std::cout << "BattleLevel InitializeEffectManager：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // タスクマネージャにタスクを積む
  task_manager_.AddTask(effect_manager_);

  return true;
}

/// <summary>
/// ゲームオーバー処理 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeGameOver() {

  // ゲームオーバー処理 生成
  game_over_ = new FinishUiGameOver(*this);
  if (game_over_ == nullptr) {
    std::cout << "BattleLevel InitializeGameOver：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  return true;
}

/// <summary>
/// ゲームクリア処理 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeGameClear() {

  // ゲームクリア処理 生成
  game_clear_ = new FinishUiGameClear(*this);
  if (game_clear_ == nullptr) {
    std::cout << "BattleLevel InitializeGameClear：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  return true;
}

/// <summary>
/// メニューコントローラ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeMenuController() {

  // メニューコントローラ 生成
  menu_controller_ = new MenuController(*this);
  if (menu_controller_ == nullptr) {
    std::cout << "BattleLevel InitializeMenuController：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  return true;
}

/// <summary>
/// タイマー 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeGameTimer() {

  std::array<int, kImageNumberDivNum> graphic_handle;

  // 画像のロード
  int result = LoadDivGraph(kImageNumberPath, kImageNumberDivNum,
                            kNumberDivX, kNumberDivY,
                            kNumberWidth, kNumberHeight, graphic_handle.data());
  if (result == kLoadError) {
    return false;
  }
  // ゲームタイマー 生成
  game_timer_ = new GameTimer();
  if (game_timer_ == nullptr) {
    std::cout << "BattleLevel InitializeGameTimer：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // グラフィックハンドルを渡す
  game_timer_->SetGraphicHandle(graphic_handle);

  // タスクマネージャに積む
  task_manager_.AddTask(game_timer_);

  return true;
}

/// <summary>
/// ポーズUI処理 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializePauseUi() {

  // ポーズUI処理 生成
  pause_ui_ = new PauseUi();
  if (pause_ui_ == nullptr) {
    std::cout << "BattleLevel InitializePauseUi：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  return true;
}

/// <summary>
/// サウンド設定処理 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeSoundSetup() {

  // サウンド設定処理 生成
  sound_setup_ = new SoundSetup(*this);
  if (sound_setup_ == nullptr) {
    std::cout << "BattleLevel InitializeSoundSetup：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  return true;
}

/// <summary>
/// ボリュームコントローラ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeVolumeController() {

  // ボリュームコントローラ 生成
  volume_controller_ = new VolumeController(*this);
  if (volume_controller_ == nullptr) {
    std::cout << "BattleLevel InitializeVolumeController：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  return true;
}

/// <summary>
/// デバッグメニュー 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::InitializeDebugMenu() {

  // デバッグメニュー 生成
  debug_menu_ = new DebugMenu();
  if (debug_menu_ == nullptr) {
    std::cout << "BattleLevel InitializeDebugMenu：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  return true;
}

/// <summary>
/// 準備処理
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:処理継続中 </returns>
bool BattleLevel::PrepareGame() {

  bool is_finish = false;

  switch (prepare_phase_) {
  case PreparePhase::kRegisterBullet: {
    //---------------------------------------------
    //弾の生成
    //---------------------------------------------
    is_finish = RegisterBullet();
    if (is_finish) {
      // 準備フェーズを「サウンド登録」に変更
      ChangePreparePhase(PreparePhase::kRegisterBarrage);
    }
    break;
  }
  case PreparePhase::kRegisterBarrage: {
    //---------------------------------------------
    //弾幕の生成
    //---------------------------------------------
    is_finish = RegisterBarrage();
    if (is_finish) {
      // 準備フェーズを「サウンド登録」に変更
      ChangePreparePhase(PreparePhase::kRegisterSound);
    }
    break;
  }
  case PreparePhase::kRegisterSound: {
    //---------------------------------------------
    //サウンドの生成
    //---------------------------------------------
    is_finish = RegisterSound();
    if (is_finish) {
      // 準備フェーズを「HPバーの登録」に変更
      ChangePreparePhase(PreparePhase::kRegisterHpBar);
    }
    break;
  }
  case PreparePhase::kRegisterHpBar: {
    //---------------------------------------------
    //HPバーの生成
    //---------------------------------------------
    is_finish = RegisterHpBar();
    if (is_finish) {
      // 準備フェーズを「エフェクトの生成」に変更
      ChangePreparePhase(PreparePhase::kRegisterEffect);
    }
    break;
  }
  case PreparePhase::kRegisterEffect: {
    //---------------------------------------------
    //エフェクトの生成
    //---------------------------------------------
    is_finish = RegisterEffect();
    if (is_finish) {
      // 準備フェーズを「終了処理」に変更
      ChangePreparePhase(PreparePhase::kFinish);
    }
    break;
  }
  case PreparePhase::kFinish: {
    //---------------------------------------------
    //準備終了処理
    //---------------------------------------------
    return true;
  }
  }

  return false;
}

/// <summary>
/// 弾の登録処理
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:処理継続中 </returns>
bool BattleLevel::RegisterBullet() {

  std::string path_image;
  BulletId bullet_id = BulletId::kNormal;
  RegisterBulletPhase next_phase = RegisterBulletPhase::kFinish;

  switch (bullet_phase_) {
  case RegisterBulletPhase::kNormal: {
    //-------------------------------------------
    //通常弾
    //-------------------------------------------
    path_image = kImageNormalBulletPath;
    bullet_id = BulletId::kNormal;
    next_phase = RegisterBulletPhase::kBlueBall;
    break;
  }
  case RegisterBulletPhase::kBlueBall: {
    //-------------------------------------------
    //青い弾
    //-------------------------------------------
    path_image = kImageBlueBallBulletPath;
    bullet_id = BulletId::kBlueBall;
    next_phase = RegisterBulletPhase::kPurpleBall;
    break;
  }
  case RegisterBulletPhase::kPurpleBall: {
    //-------------------------------------------
    //紫弾
    //-------------------------------------------
    path_image = kImagePurpleBallBulletPath;
    bullet_id = BulletId::kPurpleBall;
    next_phase = RegisterBulletPhase::kRedBall;
    break;
  }
  case RegisterBulletPhase::kRedBall: {
    //-------------------------------------------
    //赤弾
    //-------------------------------------------
    path_image = kImageRedBallBulletPath;
    bullet_id = BulletId::kRedBall;
    next_phase = RegisterBulletPhase::kYellowBall;
    break;
  }
  case RegisterBulletPhase::kYellowBall: {
    //-------------------------------------------
    //黄弾
    //-------------------------------------------
    path_image = kImageYellowBallBulletPath;
    bullet_id = BulletId::kYellowBall;
    next_phase = RegisterBulletPhase::kGreenBall;
    break;
  }
  case RegisterBulletPhase::kGreenBall: {
    //-------------------------------------------
    //白い弾
    //-------------------------------------------
    path_image = kImageGreenBallBulletPath;
    bullet_id = BulletId::kGreenBall;
    next_phase = RegisterBulletPhase::kFinish;
    break;
  }
  case  RegisterBulletPhase::kFinish: {
    //---------------------------------------------
    //終了処理
    //---------------------------------------------
    return true;
  }
  }

  int graphic_handle = LoadGraph(path_image.c_str());
  if (graphic_handle == kLoadError) {
    return false;
  }
  Bullet* bullet = nullptr;
  Bullet::BulletData bullet_data = bullet_list_[bullet_id];
  // 弾の生成
  bullet = bullet_manager_->CreateBullet(bullet_id);
  if (bullet == nullptr) {
    std::cout << "BattleLevel RegisterBullet：弾クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // イベントインターフェースを渡す
  bullet->SetBulletEventInterface(this);
  // グラフィックハンドルを渡す
  bullet->SetGraphicHandle(graphic_handle);
  // 各種設定
  SetBulletData(bullet, bullet_data);

  // 弾の登録フェーズを次のフェーズに変更する
  ChangeBulletPhase(next_phase);

  return false;
}

/// <summary>
/// 弾の設定
/// </summary>
/// <param name=""> バレット </param>
/// <param name=""> 弾情報 </param>
/// <returns></returns>
void BattleLevel::SetBulletData(Bullet* bullet, Bullet::BulletData bullet_data) {

  bullet->SetSpeed(bullet_data.speed.x, bullet_data.speed.y);
  bullet->SetSize(bullet_data.size.width, bullet_data.size.height);
  bullet->SetSpec(bullet_data.spec.atttack);
  bullet->SetExtRate(bullet_data.ext_rate.value, bullet_data.ext_rate.adjust,
                     bullet_data.ext_rate.max, bullet_data.ext_rate.min);
  bullet->SetAngle(bullet_data.angle.value, bullet_data.angle.adjust,
                   bullet_data.angle.max, bullet_data.angle.min);
  bullet->SetAlpha(bullet_data.alpha.value, bullet_data.alpha.adjust,
                   bullet_data.alpha.max, bullet_data.alpha.min);
  float radius = static_cast<float>(bullet_data.size.width / kHalfValue);
  bullet->SetRadius(radius);
}

/// <summary>
/// 弾幕の登録処理
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:処理継続中 </returns>
bool BattleLevel::RegisterBarrage() {

  switch (barrage_phase_) {
  case RegisterBarragePhase::kAllDirection: {
    //---------------------------------------------
    //全方向
    //---------------------------------------------
    Barrage* barrage = nullptr;
    Barrage::BarrageData barrage_data = barrage_list_[BarrageId::kAllDirection];
    barrage = barrage_manager_->RegisterBarrage(BarrageId::kAllDirection);
    if (barrage == nullptr) {
      std::cout << "BattleLevel RegisterBarrage：弾クラス生成失敗" << std::endl;
      OccurredError();
      return false;
    }
    // 各種設定値のセット
    SetBarrageData(barrage, barrage_data);
    barrage->SetEventInterface(this);

    // 弾幕の登録フェーズを「回転」に変更する
    ChangeBarragePhase(RegisterBarragePhase::kRotation);
    break;
  }
  case RegisterBarragePhase::kRotation: {
    //---------------------------------------------
    //回転
    //---------------------------------------------
    Barrage* barrage = nullptr;
    Barrage::BarrageData barrage_data = barrage_list_[BarrageId::kRotation];
    barrage = barrage_manager_->RegisterBarrage(BarrageId::kRotation);
    if (barrage == nullptr) {
      std::cout << "BattleLevel RegisterBarrage：弾クラス生成失敗" << std::endl;
      OccurredError();
      return false;
    }
    // 各種設定値のセット
    SetBarrageData(barrage, barrage_data);
    barrage->SetEventInterface(this);

    // 弾幕の登録フェーズを「連射」に変更する
    ChangeBarragePhase(RegisterBarragePhase::kContinuous);
    break;
  }
  case RegisterBarragePhase::kContinuous: {
    //---------------------------------------------
    //連射
    //---------------------------------------------
    Barrage* barrage = nullptr;
    Barrage::BarrageData barrage_data = barrage_list_[BarrageId::kContinuous];
    barrage = barrage_manager_->RegisterBarrage(BarrageId::kContinuous);
    if (barrage == nullptr) {
      std::cout << "BattleLevel RegisterBarrage：弾クラス生成失敗" << std::endl;
      OccurredError();
      return false;
    }
    // 各種設定値のセット
    SetBarrageData(barrage, barrage_data);
    barrage->SetEventInterface(this);

    // 弾幕の登録フェーズを「単発射撃」に変更する
    ChangeBarragePhase(RegisterBarragePhase::kSingle);
    break;
  }
  case RegisterBarragePhase::kSingle: {
    //---------------------------------------------
    //単発射撃
    //---------------------------------------------
    Barrage* barrage = nullptr;
    Barrage::BarrageData barrage_data = barrage_list_[BarrageId::kSingle];
    barrage = barrage_manager_->RegisterBarrage(BarrageId::kSingle);
    if (barrage == nullptr) {
      std::cout << "BattleLevel RegisterBarrage：弾クラス生成失敗" << std::endl;
      OccurredError();
      return false;
    }
    // 各種設定値のセット
    SetBarrageData(barrage, barrage_data);
    barrage->SetEventInterface(this);

    // 弾幕の登録フェーズを「2倍射撃」に変更する
    ChangeBarragePhase(RegisterBarragePhase::kDouble);
    break;
  }
  case RegisterBarragePhase::kDouble: {
    //---------------------------------------------
    //2倍射撃
    //---------------------------------------------
    Barrage* barrage = nullptr;
    Barrage::BarrageData barrage_data = barrage_list_[BarrageId::kDouble];
    barrage = barrage_manager_->RegisterBarrage(BarrageId::kDouble);
    if (barrage == nullptr) {
      std::cout << "BattleLevel RegisterBarrage：弾クラス生成失敗" << std::endl;
      OccurredError();
      return false;
    }
    // 各種設定値のセット
    SetBarrageData(barrage, barrage_data);
    barrage->SetEventInterface(this);

    // 弾幕の登録フェーズを「3倍射撃」に変更する
    ChangeBarragePhase(RegisterBarragePhase::kTriple);
    break;
  }
  case RegisterBarragePhase::kTriple: {
    //---------------------------------------------
    //3倍射撃
    //---------------------------------------------
    Barrage* barrage = nullptr;
    Barrage::BarrageData barrage_data = barrage_list_[BarrageId::kTriple];
    barrage = barrage_manager_->RegisterBarrage(BarrageId::kTriple);
    if (barrage == nullptr) {
      std::cout << "BattleLevel RegisterBarrage：弾クラス生成失敗" << std::endl;
      OccurredError();
      return false;
    }
    // 各種設定値のセット
    SetBarrageData(barrage, barrage_data);
    barrage->SetEventInterface(this);

    // 弾幕の登録フェーズを「4倍射撃」に変更する
    ChangeBarragePhase(RegisterBarragePhase::kQuadruple);
    break;
  }
  case RegisterBarragePhase::kQuadruple: {
    //---------------------------------------------
    //4倍射撃
    //---------------------------------------------
    Barrage* barrage = nullptr;
    Barrage::BarrageData barrage_data = barrage_list_[BarrageId::kQuadruple];
    barrage = barrage_manager_->RegisterBarrage(BarrageId::kQuadruple);
    if (barrage == nullptr) {
      std::cout << "BattleLevel RegisterBarrage：弾クラス生成失敗" << std::endl;
      OccurredError();
      return false;
    }
    // 各種設定値のセット
    SetBarrageData(barrage, barrage_data);
    barrage->SetEventInterface(this);


    // 弾幕の登録フェーズを「終了処理」に変更する
    ChangeBarragePhase(RegisterBarragePhase::kFinish);
    break;
  }
  case RegisterBarragePhase::kFinish: {
    //---------------------------------------------
    //終了処理
    //---------------------------------------------
    return true;
  }
  }

  return false;
}

/// <summary>
/// 弾幕の設定
/// </summary>
/// <param name="barrage"> バレッジ </param>
/// <param name="barrage_data"> 弾幕情報 </param>
/// <returns></returns>
void BattleLevel::SetBarrageData(Barrage* barrage, Barrage::BarrageData barrage_data) {

  barrage->SetDataBarrageId(barrage_data.detail.barrage_id);  // 弾幕の種類
  barrage->SetBulletId(barrage_data.detail.bullet_id);     // 弾の種類
  barrage->SetAimType(barrage_data.detail.aim_type);       // 目標の設定
  barrage->SetDeployCount(barrage_data.count.deploy);      // 弾の配置数
  barrage->SetGenerateCount(barrage_data.count.generate);  // 生成する弾の数
  barrage->SetBulletId(barrage_data.detail.bullet_id);     // バレットID
  barrage->SetPositionX(barrage_data.pos.x);               // 中心座標 X座標
  barrage->SetPositionY(barrage_data.pos.y);               // 中心座標 Y座標
  barrage->SetExecInterval(barrage_data.interval.exec);    // 実行間隔
  barrage->SetWaitInterval(barrage_data.interval.wait);    // 待機間隔
  barrage->SetDistanceX(barrage_data.distance.x);          // 中心から離す距離 X方向
  barrage->SetDistanceY(barrage_data.distance.y);          // 中心から離す距離 Y方向
  barrage->SetGoalPositionX(barrage_data.pos.x_goal);      // 目標座標 X座標
  barrage->SetGoalPositionY(barrage_data.pos.y_goal);      // 目標座標 Y座標
  barrage->SetMoveRate(barrage_data.rate.move);            // 弾の移動割合(移動量の係数)
  barrage->SetExpaValue(barrage_data.expa.value);          // 弾の拡大率
  barrage->SetExpaAdjust(barrage_data.expa.adjust);        // 弾の拡大率調整値
  barrage->SetExpaMax(barrage_data.expa.max);              // 弾の拡大率最大値
  barrage->SetExpaMin(barrage_data.expa.min);              // 弾の拡大率最小値
  barrage->SetAlphaValue(barrage_data.alpha.value);        // 弾の透過率
  barrage->SetAlphaAdjust(barrage_data.alpha.adjust);      // 弾の透過率調整値
  barrage->SetAlphaMax(barrage_data.alpha.max);            // 弾の透過率最大値
  barrage->SetAlphaMin(barrage_data.alpha.min);            // 弾の透過率最小値
  barrage->SetAngle(barrage_data.angle.value);             // 角度
  barrage->SetAngleAdjust(barrage_data.angle.adjust);      // 角度の調整量
  barrage->SetExecCount(barrage_data.count.execute);       // 実行回数
  barrage->SetAcceleRateX(barrage_data.rate.acce_x);       // 加速度(X方向)
  barrage->SetAcceleRateY(barrage_data.rate.acce_y);       // 加速度(Y方向)
  barrage->SetSpeedXMax(barrage_data.speed.x_max);         // 速度(X方向)最大値
  barrage->SetSpeedYMax(barrage_data.speed.y_max);         // 速度(Y方向)最大値
  barrage->SetSpeedXMin(barrage_data.speed.x_min);         // 速度(X方向)最小値
  barrage->SetSpeedYMin(barrage_data.speed.y_min);         // 速度(Y方向)最小値
  barrage->SetWaitTimeMoveStart(barrage_data.time.wait_start);    // 動作待機時間
  barrage->SetWaitTimeAcceleration(barrage_data.time.wait_accel); // 加速待機時間
  barrage->SetWaitTimeExpantion(barrage_data.time.wait_expan);    // 加速待機時間
}

/// <summary>
/// サウンドの登録処理
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:処理継続中 </returns>
bool BattleLevel::RegisterSound() {

  switch (sound_phase_) {
  case RegisterSoundPhase::kButtonSelect: {
    //---------------------------------------------
    //ボタン選択音登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundButtonSelectPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return true;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kButtonSelect, Sound::SoundType::kSoundEffect);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kSoundEffect]);
      // サウンドハンドル、総再生時間、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「ボタン押下の登録」に変更
    ChangeSoundPhase(RegisterSoundPhase::kButtonPush);
    break;
  }
  case RegisterSoundPhase::kButtonPush: {
    //---------------------------------------------
    //ボタン押下音登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundButtonPushPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return true;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kButtonPush, Sound::SoundType::kSoundEffect);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kSoundEffect]);
      // サウンドハンドル、総再生時間、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「射撃音の登録」に変更
    ChangeSoundPhase(RegisterSoundPhase::kShoot);
    break;
  }
  case RegisterSoundPhase::kShoot: {
    //---------------------------------------------
    //射撃音登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundShootPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return true;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kShoot, Sound::SoundType::kSoundEffect);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kSoundEffect]);
      // サウンドハンドル、総再生時間、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「プレイヤー死亡音の登録」に変更
    ChangeSoundPhase(RegisterSoundPhase::kPlayerDeath);
    break;
  }
  case RegisterSoundPhase::kPlayerDeath: {
    //---------------------------------------------
    //プレイヤー死亡音の登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundPlayerDeathPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return true;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kPlayerDeath, Sound::SoundType::kSoundEffect);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kSoundEffect]);
      // サウンドハンドル、総再生時間、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「プレイヤー爆発音の登録」に変更
    ChangeSoundPhase(RegisterSoundPhase::kPlayerExplosion);
    break;
  }
  case RegisterSoundPhase::kPlayerExplosion: {
    //---------------------------------------------
    //プレイヤー爆発音登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundPlayerExplosionPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return true;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kPlayerExplosion, Sound::SoundType::kSoundEffect);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kSoundEffect]);
      // サウンドハンドル、総再生時間、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「プレイヤー復活音の登録」に変更
    ChangeSoundPhase(RegisterSoundPhase::kPlayerReborn);
    break;
  }
  case RegisterSoundPhase::kPlayerReborn: {
    //---------------------------------------------
    //プレイヤー復活音登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundPlayerRebornPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return true;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kPlayerReborn, Sound::SoundType::kSoundEffect);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kSoundEffect]);
      // サウンドハンドル、総再生時間、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「敵の爆破音登録」に変更
    ChangeSoundPhase(RegisterSoundPhase::kEnemyExplosion);
    break;
  }
  case RegisterSoundPhase::kEnemyExplosion: {
    //---------------------------------------------
    //敵の爆破音登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundEnemyExplosionPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return true;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kEnemyExplosion, Sound::SoundType::kSoundEffect);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kSoundEffect]);
      // サウンドハンドル、総再生時間、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「カットイン登録」に変更
    ChangeSoundPhase(RegisterSoundPhase::kCutIn);
    break;
  }
  case RegisterSoundPhase::kCutIn: {
    //---------------------------------------------
    //カットイン音登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundCutInPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return true;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kCutIn, Sound::SoundType::kSoundEffect);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kSoundEffect]);
      // サウンドハンドル、総再生時間、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「アイテム獲得音登録」に変更
    ChangeSoundPhase(RegisterSoundPhase::kGetItem);
    break;
  }
  case RegisterSoundPhase::kGetItem: {
    //---------------------------------------------
    //アイテム獲得音登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundGetItemPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return true;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kGetItem, Sound::SoundType::kSoundEffect);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kSoundEffect]);
      // サウンドハンドル、総再生時間、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「ゲームオーバー登録」に変更
    ChangeSoundPhase(RegisterSoundPhase::kGameOver);
    break;
  }
  case RegisterSoundPhase::kGameOver: {
    //---------------------------------------------
    //ゲームオーバー音登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundGameOverPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return true;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kGameOver, Sound::SoundType::kSoundEffect);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kSoundEffect]);
      // サウンドハンドル、総再生時間、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「ゲームクリア音登録」に変更
    ChangeSoundPhase(RegisterSoundPhase::kGameClear);
    break;
  }
  case RegisterSoundPhase::kGameClear: {
    //---------------------------------------------
    //ゲームクリア音登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundGameClearPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return true;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kGameClear, Sound::SoundType::kBackGroundMusic);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kBackGroundMusic]);
      // サウンドハンドル、総再生時間、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「バトルBGM登録」に変更
    ChangeSoundPhase(RegisterSoundPhase::kBgm);
    break;
  }
  case RegisterSoundPhase::kBgm: {
    //---------------------------------------------
    //バトルBGM登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundBattleBgmPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return true;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kBattleBgm, Sound::SoundType::kBackGroundMusic);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kBackGroundMusic]);
      // サウンドハンドル、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「BGM(テスト用)」に変更
    ChangeSoundPhase(RegisterSoundPhase::kTestBgm);
    break;
  }
  case RegisterSoundPhase::kTestBgm: {
    //---------------------------------------------
    //バトルBGM(テスト用)登録
    //---------------------------------------------
    // 音をロードする
    int sound_handle = LoadSoundMem(kSoundBattleBgmPath);
    if (sound_handle == kLoadError) {
      std::cout << "BattleLevel RegisterSound：サウンドロード失敗" << std::endl;
      return true;
    }
    // サウンドの生成
    Sound* sound = sound_manager_->RegisterSound(SoundId::kBattleBgmTest, Sound::SoundType::kBackGroundMusic);
    if (sound != nullptr) {
      // サウンドボリュームの変換
      int volume = static_cast<int>(sound_volume_list_[Sound::SoundType::kBackGroundMusic]);
      // サウンドハンドル、サウンドボリュームを設定する
      sound->SetSoundHandle(sound_handle);
      sound->GetTotalTime();
      sound->SetSoundVolume(volume);
      sound->SetSoundVolumeAdjust(kSoundAdjustVolume);
      sound->ChangeSoundVolume();
    }
    // 音楽登録フェーズを「終了処理」に変更
    ChangeSoundPhase(RegisterSoundPhase::kFinish);
    break;
  }
  case  RegisterSoundPhase::kFinish: {
    //---------------------------------------------
    //終了処理
    //---------------------------------------------
    return true;
  }
  }

  return false;
}

/// <summary>
/// HPバーの登録処理
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:処理継続中 </returns>
bool BattleLevel::RegisterHpBar() {

  HitPointBar* hp_bar = nullptr;

  // ボス用のHPバーの生成
  hp_bar = hp_bar_manager_->RegisterHitPointBar(HitPointBarId::kBoss);
  if (hp_bar != nullptr) {
    hp_bar->SetSize();
  }

  // 敵キャラ用のHPバーの生成
  hp_bar = hp_bar_manager_->RegisterHitPointBar(HitPointBarId::kEnemy);
  if (hp_bar != nullptr) {
    hp_bar->SetSize();
  }

  return true;
}

/// <summary>
/// エフェクトの登録処理
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:処理継続中 </returns>
bool BattleLevel::RegisterEffect() {

  EffectId effect_id = EffectId::kEffectMaxIndex;
  RegisterEffectPhase next_phase = RegisterEffectPhase::kFinish;
  std::vector<int> image_handle;

  switch (effect_phase_) {
  case RegisterEffectPhase::kPlayerDeath: {
    //-----------------------------------
    //プレイヤーの死
    //-----------------------------------
    effect_id = EffectId::kPlayerDeath;
    next_phase = RegisterEffectPhase::kPlayerExplosion;
    // グラフィックハンドルを取得
    bool is_finish = LoadImageHandlePlayerDeath(image_handle);
    if (!is_finish) {
      std::cout << "BattleLevel RegisterEffect：エフェクト画像取得失敗" << std::endl;
      return false;
    }
    break;
  }
  case RegisterEffectPhase::kPlayerExplosion: {
    //-----------------------------------
    //プレイヤー爆発
    //-----------------------------------
    effect_id = EffectId::kExplosion;
    next_phase = RegisterEffectPhase::kPlayerReborn;
    // グラフィックハンドルを取得
    bool is_finish = LoadImageHandlePlayerExplosion(image_handle);
    if (!is_finish) {
      std::cout << "BattleLevel RegisterEffect：エフェクト画像取得失敗" << std::endl;
      return false;
    }
    break;
  }
  case RegisterEffectPhase::kPlayerReborn: {
    //-----------------------------------
    //プレイヤー復活
    //-----------------------------------
    effect_id = EffectId::kReborn;
    next_phase = RegisterEffectPhase::kDisappearNormal;
    // グラフィックハンドルを取得
    bool is_finish = LoadImageHandlePlayerReborn(image_handle);
    if (!is_finish) {
      std::cout << "BattleLevel RegisterEffect：エフェクト画像取得失敗" << std::endl;
      return false;
    }
    break;
  }
  case RegisterEffectPhase::kDisappearNormal: {
    //-----------------------------------
    //通常弾消滅
    //-----------------------------------
    effect_id = EffectId::kDisappearNormal;
    next_phase = RegisterEffectPhase::kDisappearBlueBall;
    // グラフィックハンドルを取得
    bool is_finish = LoadImageHandleDisappearNormal(image_handle);
    if (!is_finish) {
      std::cout << "BattleLevel RegisterEffect：エフェクト画像取得失敗" << std::endl;
      return false;
    }
    break;
  }
  case RegisterEffectPhase::kDisappearBlueBall:
  case RegisterEffectPhase::kDisappearPurpleBall:
  case RegisterEffectPhase::kDisappearRedBall:
  case RegisterEffectPhase::kDisappearYellowBall:
  case RegisterEffectPhase::kDisappearGreenBall: {
    //-----------------------------------
    //弾消滅
    //-----------------------------------
    switch (effect_phase_) {
    case RegisterEffectPhase::kDisappearBlueBall: {
      effect_id = EffectId::kDisappearBlueBall;
      next_phase = RegisterEffectPhase::kDisappearPurpleBall;
      break;
    }
    case RegisterEffectPhase::kDisappearPurpleBall: {
      effect_id = EffectId::kDisappearPurpleBall;
      next_phase = RegisterEffectPhase::kDisappearRedBall;
      break;
    }
    case RegisterEffectPhase::kDisappearRedBall: {
      effect_id = EffectId::kDisappearRedBall;
      next_phase = RegisterEffectPhase::kDisappearYellowBall;
      break;
    }
    case RegisterEffectPhase::kDisappearYellowBall: {
      effect_id = EffectId::kDisappearYellowBall;
      next_phase = RegisterEffectPhase::kDisappearGreenBall;
      break;
    }
    case RegisterEffectPhase::kDisappearGreenBall: {
      effect_id = EffectId::kDisappearGreenBall;
      next_phase = RegisterEffectPhase::kEnemyExplosion;
      break;
    }
    }
    // グラフィックハンドルを取得
    bool is_finish = LoadImageHandleDisappearBall(image_handle, effect_id);
    if (!is_finish) {
      std::cout << "BattleLevel RegisterEffect：エフェクト画像取得失敗" << std::endl;
      return false;
    }
    break;
  }
  case RegisterEffectPhase::kEnemyExplosion: {
    //-----------------------------------
    //敵の爆発処理
    //-----------------------------------
    effect_id = EffectId::kEnemyExplosion;
    next_phase = RegisterEffectPhase::kEnemyRupture;
    // グラフィックハンドルを取得
    bool is_finish = LoadImageHandleEnemyExplosion(image_handle);
    if (!is_finish) {
      std::cout << "BattleLevel RegisterEffect：エフェクト画像取得失敗" << std::endl;
      return false;
    }
    break;
  }
  case RegisterEffectPhase::kEnemyRupture: {
    //-----------------------------------
    //敵の破裂処理
    //-----------------------------------
    effect_id = EffectId::kEnemyRupture;
    next_phase = RegisterEffectPhase::kItemSummon;
    // グラフィックハンドルを取得
    bool is_finish = LoadImageHandleEnemyRupture(image_handle);
    if (!is_finish) {
      std::cout << "BattleLevel RegisterEffect：エフェクト画像取得失敗" << std::endl;
      return false;
    }
    break;
  }
  case RegisterEffectPhase::kItemSummon: {
    //-----------------------------------
    //アイテム生成処理
    //-----------------------------------
    effect_id = EffectId::kItemSummon;
    next_phase = RegisterEffectPhase::kFinish;
    // グラフィックハンドルを取得
    bool is_finish = LoadImageHandleItemSummon(image_handle);
    if (!is_finish) {
      std::cout << "BattleLevel RegisterEffect：エフェクト画像取得失敗" << std::endl;
      return false;
    }
    break;
  }
  case RegisterEffectPhase::kFinish: {
    //-----------------------------------
    //終了処理
    //-----------------------------------
    return true;
  }
  }

  // エフェクトデータの取得
  Effect::EffectData effect_data = effect_list_[effect_id];
  // エフェクトの生成
  Effect* effect = effect_manager_->RegisterEffect(effect_id, effect_data.detail.type);
  if (effect == nullptr) {
    std::cout << "BattleLevel RegisterEffect：エフェクトクラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // グラフィックハンドルをセット
  for (int i = 0; i < static_cast<int>(image_handle.size()); ++i) {
    effect->SetEffectHandle(image_handle.at(i));
  }

  // 各種設定
  effect->SetAlpha(effect_data.alpha.value, effect_data.alpha.adjust,
                   effect_data.alpha.max, effect_data.alpha.min);
  effect->SetLoadDivData(effect_data.div.total, effect_data.div.x, effect_data.div.y,
                         effect_data.div.width, effect_data.div.height);
  effect->SetWaitTime(effect_data.detail.wait_time);
  effect->SetExtRateX(effect_data.ex_rate_x.value, effect_data.ex_rate_x.adjust,
                      effect_data.ex_rate_x.max, effect_data.ex_rate_x.min);
  effect->SetExtRateY(effect_data.ex_rate_y.value, effect_data.ex_rate_y.adjust,
                      effect_data.ex_rate_y.max, effect_data.ex_rate_y.min);
  effect->SetAngle(effect_data.angle.value, effect_data.angle.adjust,
                   effect_data.angle.max, effect_data.angle.min);

  // 表示座標を渡す
  effect->SetPosX(kMathOriginX);
  effect->SetPosY(kMathOriginY);

  // エフェクト登録フェーズを変更する
  ChangeEffectPhase(next_phase);

  return false;
}

/// <summary>
/// プレイヤー死亡 画像ファイル取得
/// </summary>
/// <param name="image_handle"> グラフィックハンドル </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::LoadImageHandlePlayerDeath(std::vector<int>& image_handle) {

  Effect::EffectData effect_data = effect_list_[EffectId::kPlayerDeath];

  int graphic_handle[kPlayerDeathDivCount];
  // 画像のロード
  int result = LoadDivGraph(kImagePlayerDeathPath, effect_data.div.total,
                            effect_data.div.x, effect_data.div.y,
                            effect_data.div.width, effect_data.div.height, graphic_handle);
  if (result == kLoadError) {
    return false;
  }

  for (int i = 0; i < effect_data.div.total; ++i) {
    image_handle.push_back(graphic_handle[i]);
  }

  return true;
}

/// <summary>
/// プレイヤー爆発 画像ファイル取得
/// </summary>
/// <param name="image_handle"> グラフィックハンドル </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::LoadImageHandlePlayerExplosion(std::vector<int>& image_handle) {

  Effect::EffectData effect_data = effect_list_[EffectId::kExplosion];

  int graphic_handle[kPlayerExplosionDivCount];
  // 画像のロード
  int result = LoadDivGraph(kImageExplosionPath, effect_data.div.total,
                            effect_data.div.x, effect_data.div.y,
                            effect_data.div.width, effect_data.div.height, graphic_handle);
  if (result == kLoadError) {
    return false;
  }

  for (int i = 0; i < effect_data.div.total; ++i) {
    image_handle.push_back(graphic_handle[i]);
  }

  return true;
}

/// <summary>
/// プレイヤー復活 画像ファイル取得
/// </summary>
/// <param name="image_handle"> グラフィックハンドル </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::LoadImageHandlePlayerReborn(std::vector<int>& image_handle) {

  Effect::EffectData effect_data = effect_list_[EffectId::kReborn];

  int graphic_handle[kPlayerRebornDivCount];
  // 画像のロード
  int result = LoadDivGraph(kImageRebornPath, effect_data.div.total,
                            effect_data.div.x, effect_data.div.y,
                            effect_data.div.width, effect_data.div.height, graphic_handle);
  if (result == kLoadError) {
    return false;
  }

  for (int i = 0; i < effect_data.div.total; ++i) {
    image_handle.push_back(graphic_handle[i]);
  }

  return true;
}

/// <summary>
/// 通常弾消滅 画像ファイル取得
/// </summary>
/// <param name="image_handle"> グラフィックハンドル </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::LoadImageHandleDisappearNormal(std::vector<int>& image_handle) {

  Effect::EffectData effect_data = effect_list_[EffectId::kDisappearNormal];

  int graphic_handle[kDisappearNormalDivCount];
  // 画像のロード
  int result = LoadDivGraph(kImageDisappearNormalPath, effect_data.div.total,
    effect_data.div.x, effect_data.div.y,
    effect_data.div.width, effect_data.div.height, graphic_handle);
  if (result == kLoadError) {
    return false;
  }

  for (int i = 0; i < effect_data.div.total; ++i) {
    image_handle.push_back(graphic_handle[i]);
  }

  return true;
}

/// <summary>
/// 弾消滅 画像ファイル取得
/// </summary>
/// <param name="image_handle"> グラフィックハンドル </param>
/// <param name="effect_id"> エフェクトID </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::LoadImageHandleDisappearBall(std::vector<int>& image_handle, EffectId effect_id) {

  Effect::EffectData effect_data = effect_list_[effect_id];
  std::string image_path;
  switch (effect_id) {
  case EffectId::kDisappearBlueBall: {
    image_path = kImageDisappearBlueBallPath;
    break;
  }
  case EffectId::kDisappearPurpleBall: {
    image_path = kImageDisappearPurpleBallPath;
    break;
  }
  case EffectId::kDisappearRedBall: {
    image_path = kImageDisappearRedBallPath;
    break;
  }
  case EffectId::kDisappearYellowBall: {
    image_path = kImageDisappearYellowBallPath;
    break;
  }
  case EffectId::kDisappearGreenBall: {
    image_path = kImageDisappearGreenBallPath;
    break;
  }
  }

  int graphic_handle[kDisappearBallDivCount];
  // 画像のロード
  int result = LoadDivGraph(image_path.c_str(), effect_data.div.total,
                            effect_data.div.x, effect_data.div.y,
                            effect_data.div.width, effect_data.div.height, graphic_handle);
  if (result == kLoadError) {
    return false;
  }

  for (int i = 0; i < effect_data.div.total; ++i) {
    image_handle.push_back(graphic_handle[i]);
  }

  return true;
}

/// <summary>
/// 敵の爆破 画像ファイル取得
/// </summary>
/// <param name="image_handle"> グラフィックハンドル </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::LoadImageHandleEnemyExplosion(std::vector<int>& image_handle) {

  Effect::EffectData effect_data = effect_list_[EffectId::kEnemyExplosion];

  int graphic_handle[kEnemyExplosionDivCount];
  // 画像のロード
  int result = LoadDivGraph(kImageEnemyExplosionPath, effect_data.div.total,
                            effect_data.div.x, effect_data.div.y,
                            effect_data.div.width, effect_data.div.height, graphic_handle);
  if (result == kLoadError) {
    return false;
  }

  for (int i = 0; i < effect_data.div.total; ++i) {
    image_handle.push_back(graphic_handle[i]);
  }

  return true;
}

/// <summary>
/// 敵の破裂 画像ファイル取得
/// </summary>
/// <param name="image_handle"> グラフィックハンドル </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::LoadImageHandleEnemyRupture(std::vector<int>& image_handle) {

  Effect::EffectData effect_data = effect_list_[EffectId::kEnemyRupture];

  int graphic_handle[kEnemyRuptureDivCount];
  // 画像のロード
  int result = LoadDivGraph(kImageEnemyRupturePath, effect_data.div.total,
    effect_data.div.x, effect_data.div.y,
    effect_data.div.width, effect_data.div.height, graphic_handle);
  if (result == kLoadError) {
    return false;
  }

  for (int i = 0; i < effect_data.div.total; ++i) {
    image_handle.push_back(graphic_handle[i]);
  }

  return true;
}

/// <summary>
/// アイテム生成 画像ファイル取得
/// </summary>
/// <param name=""> グラフィックハンドル </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::LoadImageHandleItemSummon(std::vector<int>& image_handle) {

  Effect::EffectData effect_data = effect_list_[EffectId::kItemSummon];

  int graphic_handle[kItemSummonDivCount];
  // 画像のロード
  int result = LoadDivGraph(kImageItemSummonPath, effect_data.div.total,
                            effect_data.div.x, effect_data.div.y,
                            effect_data.div.width, effect_data.div.height, graphic_handle);
  if (result == kLoadError) {
    return false;
  }

  for (int i = 0; i < effect_data.div.total; ++i) {
    image_handle.push_back(graphic_handle[i]);
  }

  return true;
}

/// <summary>
/// 透過率変更処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率に調整値を加算
  alpha_ += alpha_adjust_;

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= kAlphaMax) {
    alpha_ = kAlphaMax;
    is_finish = true;
  }
  else if (alpha_ <= kAlphaMin) {
    alpha_ = kAlphaMin;
    is_finish = true;
  }

  return is_finish;
}

/// <summary>
/// 上矢印キー押下イベント
/// </summary>
/// <param name="character"> キャラクター </param>
/// <param name="is_push"> 左Shiftキー押下有無 </param>
/// <returns></returns>
void BattleLevel::OnPushUpKey(Character* character, bool is_push) {

  // 処理中フェーズ以外なら処理終了
  if (current_phase_ != PhaseType::kPlay &&
      current_phase_ != PhaseType::kGameClear) {
    return;
  }

  // 対象のキャラクターが死んでいる場合は処理終了
  Character::CharacterType chara_type = character->GetCharacterType();
  switch (chara_type) {
  case Character::CharacterType::kPlayer: {
    if (player_->IsStop()) {
      return;
    }
    break;
  }
  default: {
    if (character->IsDead()) {
      return;
    }
    break;
  }
  }

  // 画面最上部の位置をセット
  int map_top = kMathOriginY;

  // キャラクター情報から現在の座標、移動量、画像の半分のサイズ、表示倍率を取得
  int y_pos = character->GetPositionY();
  int move_value = character->GetMoveValueY();
  int half_size = character->GetImageHalfHeight();
  float disp_rate = character->GetImageDispRate();

  // 移動量を減算 ※左Shiftキー押下有無に応じて移動量変化
  if (is_push) {
    y_pos -= move_value / kHalfValue;
  }
  else {
    y_pos -= move_value;
  }

  // 移動後の座標が画面外に出る場合は処理終了
  if (y_pos - half_size * disp_rate <= map_top) {
    return;
  }
  // 減算後の座標をキャラクター
  character->SetPositionY(y_pos);

  //===============================================
  //当たり判定処理
  //===============================================
  // 対象キャラクターの情報を取得
  float x_pos_character = character->GetFloatPositionX();
  float y_pos_character = character->GetFloatPositionY();
  float radius_character = character->GetRadius();
  float ext_rate_chara = character->GetImageDispRate();
  radius_character = radius_character * ext_rate_chara;
  int attack_character = character->GetAttack();

  // 当たり判定処理
  CheckCollision(x_pos_character, y_pos_character, radius_character,
                 attack_character, chara_type);
}

/// <summary>
/// 下矢印キー押下イベント
/// </summary>
/// <param name="character"> キャラクター </param>
/// <param name="is_push"> 左Shiftキー押下有無 </param>
/// <returns></returns>
void BattleLevel::OnPushDownKey(Character* character, bool is_push) {

  // 処理中フェーズ以外なら処理終了
  if (current_phase_ != PhaseType::kPlay &&
      current_phase_ != PhaseType::kGameClear) {
    return;
  }

  // 対象のキャラクターが死んでいる場合は処理終了
  Character::CharacterType chara_type = character->GetCharacterType();
  switch (chara_type) {
  case Character::CharacterType::kPlayer: {
    if (player_->IsStop()) {
      return;
    }
    break;
  }
  default: {
    if (character->IsDead()) {
      return;
    }
    break;
  }
  }

  // ゲーム情報から画面最下部の位置を取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  int map_bottom = game_info->GetResolutionHeight();

  // キャラクター情報から現在の座標、移動量、画像の半分のサイズ、表示倍率を取得
  int y_pos = character->GetPositionY();
  int move_value = character->GetMoveValueY();
  int half_size = character->GetImageHalfHeight();
  float disp_rate = character->GetImageDispRate();

  // 移動量を加算 ※左Shiftキー押下有無に応じて移動量変化
  if (is_push) {
    y_pos += static_cast<int>(move_value / kHalfValue);
  }
  else {
    y_pos += move_value;
  }

  // 移動後の座標が画面外に出る場合は処理終了
  if (y_pos + half_size * disp_rate >= map_bottom) {
    switch (character->GetCharacterType()) {
    case Character::CharacterType::kItemA: {
      item_extend_->SetFinish();
      break;
    }
    case Character::CharacterType::kItemB: {
      item_power_up_->SetFinish();
      break;
    }
    }
    return;
  }
  // 加算後の座標をキャラクター
  character->SetPositionY(y_pos);

  //===============================================
  //当たり判定処理
  //===============================================
  // 対象キャラクターの情報を取得
  float x_pos_character = static_cast<float>(character->GetPositionX());
  float y_pos_character = static_cast<float>(character->GetPositionY());
  float radius_character = character->GetRadius();
  float ext_rate_chara = character->GetImageDispRate();
  radius_character = radius_character * ext_rate_chara;
  int attack_character = character->GetAttack();

  // 当たり判定処理
  bool is_check = CheckCollision(x_pos_character, y_pos_character, radius_character,
                                 attack_character, chara_type);
  if (is_check) {
    // 当たった判定で対象キャラクターがアイテムの場合、
    // 終了処理とエフェクト処理を実行する
    switch (character->GetCharacterType()) {
    case Character::CharacterType::kItemA: {
      item_extend_->SetFinish();
      break;
    }
    case Character::CharacterType::kItemB: {
      item_power_up_->SetFinish();
      break;
    }
    }
  }
}

/// <summary>
/// 左矢印キー押下イベント
/// </summary>
/// <param name="character"> キャラクター </param>
/// <param name="is_push"> 左Shiftキー押下有無 </param>
/// <returns></returns>
void BattleLevel::OnPushLeftKey(Character* character, bool is_push) {

  // 処理中フェーズ以外なら処理終了
  if (current_phase_ != PhaseType::kPlay &&
      current_phase_ != PhaseType::kGameClear) {
    return;
  }

  // 対象のキャラクターが死んでいる場合は処理終了
  Character::CharacterType chara_type = character->GetCharacterType();
  switch (chara_type) {
  case Character::CharacterType::kPlayer: {
    if (player_->IsStop()) {
      return;
    }
    break;
  }
  default: {
    if (character->IsDead()) {
      return;
    }
    break;
  }
  }

  // ゲーム情報から画面最左端の位置を取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  int map_left = game_info->GetMapPositionX();

  // キャラクター情報から現在の座標、移動量、画像の半分のサイズ、表示倍率を取得
  int x_pos = character->GetPositionX();
  int move_value = character->GetMoveValueX();
  int half_size = character->GetImageHalfWidth();
  float disp_rate = character->GetImageDispRate();

  // 移動量を減算 ※左Shiftキー押下有無に応じて移動量変化
  if (is_push) {
    x_pos -= static_cast<int>(move_value / kHalfValue);
  }
  else {
    x_pos -= move_value;
  }

  // 移動後の座標が画面外に出る場合は処理終了
  if (x_pos - half_size * disp_rate <= map_left) {
    switch (character->GetCharacterType()) {
    case Character::CharacterType::kEnemyA:
    case Character::CharacterType::kEnemyB: {
      character->ChangeDirectionType(Character::DirectionType::kRight);
      break;
    }
    }
    return;
  }
  // 減算後の座標をキャラクター
  character->SetPositionX(x_pos);

  //===============================================
  //当たり判定処理
  //===============================================
  // 対象キャラクターの情報を取得
  float x_pos_character = character->GetFloatPositionX();
  float y_pos_character = character->GetFloatPositionY();
  float radius_character = character->GetRadius();
  float ext_rate_chara = character->GetImageDispRate();
  radius_character = radius_character * ext_rate_chara;
  int attack_character = character->GetAttack();

  // 当たり判定処理
  CheckCollision(x_pos_character, y_pos_character, radius_character,
                 attack_character, chara_type);
}

/// <summary>
/// 右矢印キー押下イベント
/// </summary>
/// <param name="character"> キャラクター </param>
/// <param name="is_push"> 左Shiftキー押下有無 </param>
/// <returns></returns>
void BattleLevel::OnPushRightKey(Character* character, bool is_push) {

  // 処理中フェーズ以外なら処理終了
  if (current_phase_ != PhaseType::kPlay &&
      current_phase_ != PhaseType::kGameClear) {
    return;
  }

  // 対象のキャラクターが死んでいる場合は処理終了
  Character::CharacterType chara_type = character->GetCharacterType();
  switch (chara_type) {
  case Character::CharacterType::kPlayer: {
    if (player_->IsStop()) {
      return;
    }
    break;
  }
  default: {
    if (character->IsDead()) {
      return;
    }
    break;
  }
  }

  // ゲーム情報から画面最右端の位置を取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  int map_left = game_info->GetMapPositionX();
  int map_size = game_info->GetMapWidth();

  // キャラクター情報から現在の座標、移動量、画像の半分のサイズ、表示倍率を取得
  int x_pos = character->GetPositionX();
  int move_value = character->GetMoveValueX();
  int half_size = character->GetImageHalfHeight();
  float disp_rate = character->GetImageDispRate();

  // 移動量を加算 ※左Shiftキー押下有無に応じて移動量変化
  if (is_push) {
    x_pos += static_cast<int>(move_value / kHalfValue);
  }
  else {
    x_pos += move_value;
  }

  // 移動後の座標が画面外に出る場合は処理終了
  if (x_pos + half_size * disp_rate > map_left + map_size) {
    switch (character->GetCharacterType()) {
    case Character::CharacterType::kEnemyA:
    case Character::CharacterType::kEnemyB: {
      character->ChangeDirectionType(Character::DirectionType::kLeft);
      break;
    }
    }
    return;
  }

  // 加算後の座標をキャラクター
  character->SetPositionX(x_pos);

  //===============================================
  //当たり判定処理
  //===============================================
  // 対象キャラクターの情報を取得
  float x_pos_character = character->GetFloatPositionX();
  float y_pos_character = character->GetFloatPositionY();
  float radius_character = character->GetRadius();
  float ext_rate_chara = character->GetImageDispRate();
  radius_character = radius_character * ext_rate_chara;
  int attack_character = character->GetAttack();

  // 当たり判定処理
  CheckCollision(x_pos_character, y_pos_character, radius_character,
                 attack_character, chara_type);
}

/// <summary>
/// Zキー押下イベント
/// </summary>
/// <param name="character"> キャラクター </param>
/// <returns></returns>
void BattleLevel::OnPushZKey(Character* character) {

  std::cout << "Zキー押下" << std::endl;

  // 処理中フェーズ以外なら処理終了
  if (current_phase_ != PhaseType::kPlay) {
    return;
  }

  Character::CharacterType chara_type = character->GetCharacterType();
  // 発射キャラがプレイヤー以外なら処理終了
  if (chara_type != Character::CharacterType::kPlayer) {
    return;
  }

  // 弾を発射可能か判定
  if (!character->IsShoot()) {
    std::cout << "BattleLevel OnPushZKey：リロード中" << std::endl;
    return;
  }

  // 弾を発射したことにする
  character->SetIsShoot(false);

  // セットされている弾を取得
  BarrageId barrage_id = character->GetBarrageId();
  // 弾幕の生成
  Barrage* barrage = barrage_manager_->DeployBarrageBullet(barrage_id);
  if (barrage == nullptr) {
    std::cout << "BattleLevel OnPushZKey：弾幕クラス生成失敗" << std::endl;
    OccurredError();
    return;
  }
  // キャラクター情報から座標を取得
  float x_pos = character->GetFloatPositionX();
  float y_pos = character->GetFloatPositionY();

  // キャラクターにセットされた弾の種類を取得
  BulletId bullet_id = character->GetBulletId();

  barrage->SetAimType(Barrage::AimType::kSpecified); // 目標の設定
  barrage->SetCharacter(player_);                    // キャラクター
  barrage->SetBulletId(bullet_id);                   // バレットID
  barrage->SetPositionX(x_pos);                      // 中心座標 X座標
  barrage->SetPositionY(y_pos - kPlayerShootPosY);   // 中心座標 Y座標
  barrage->SetGoalPositionX(x_pos);                  // 目標座標 X座標

  // 射撃音の生成
  sound_manager_->CreateSound(SoundId::kShoot);
}

/// <summary>
/// Escキー押下イベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushEscapeKey() {

  std::cout << "プレイヤーコントローラ Escキー押下" << std::endl;

  switch (current_phase_) {
  case PhaseType::kPauseWait: {
    //-------------------------------
    //ポーズメニュー操作中
    //-------------------------------
    FinishMenu();

    // 敵キャラのフェーズを元に戻す
    enemy_a_->ChangeCurrentPhase(EnemyA::PhaseType::kProcess);
    enemy_b_->ChangeCurrentPhase(EnemyB::PhaseType::kProcess);
    break;
  }
  case PhaseType::kPlay: {
    //-------------------------------
    //プレイ中
    //-------------------------------
    // プレイヤーコントローラをタスクマネージャから降ろす
    Task* task = task_manager_.ReleaseTask(TaskId::kPlayerController);
    // タスクマネージャにポーズメニューを積む
    bool is_check = task_manager_.AddTask(pause_ui_);
    if (!is_check) {
      return;
    }
    // ポーズメニューのフェーズを「初期化処理」に変更
    pause_ui_->ChangeCurrentPhase(PauseUi::PhaseType::kInitialize);

    // タスクマネージャにメニューコントローラを積む
    task_manager_.AddTask(menu_controller_);
    menu_controller_->InitializePushKeyFlag();

    // 現在のフェーズを「デバッグメニュー」に変更
    ChangeCurrentPhase(PhaseType::kPause);
    break;
  }
  default: {
    //-------------------------------
    //それ以外
    //-------------------------------
    break;
  }
  }

  // サウンド停止
  sound_manager_->PausePlaySound();
}

/// <summary>
/// Oキー押下イベント　※デバッグ時のみ有効
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushOKey() {

  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // デバッグ時のみ実行
  if (!game_info->IsDebug()) {
    return;
  }

  // バトルレベルに切り替える
  ChangeLevel(TaskId::kBattleLevel);
}

/// <summary>
/// Pキー押下イベント　※デバッグ時のみ有効
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushPKey() {

  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // デバッグ時のみ実行
  if (!game_info->IsDebug()) {
    return;
  }

  // 戦闘中以外は処理終了
  if (battle_phase_ != BattlePhase::kEnemyA &&
      battle_phase_ != BattlePhase::kEnemyB) {
    return;
  }

  Character* character = nullptr;
  // キャラクターを取得
  switch (battle_phase_) {
  case BattlePhase::kEnemyA: {
    character = enemy_a_->GetCharacter();
    break;
  }
  case BattlePhase::kEnemyB: {
    character = enemy_b_->GetCharacter();
    break;
  }
  }
  if (character == nullptr) {
    return;
  }

  // 現在のHPをゼロにする
  character->SetHitPoint(kCharacterDeath);
}

/// <summary>
/// Lキー押下イベント　※デバッグ時のみ有効
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushLKey() {

  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // デバッグ時のみ実行
  if (!game_info->IsDebug()) {
    return;
  }

  // 戦闘中以外は処理終了
  if (battle_phase_ != BattlePhase::kEnemyA &&
      battle_phase_ != BattlePhase::kEnemyB) {
    return;
  }

  const int kMaxIndex = 3;

  // バレッジIDを取得
  BarrageId barrage_id = player_->GetBarrageId();
  // ひとつ加算する
  int int_barrage_id = static_cast<int>(barrage_id);
  ++int_barrage_id;
  if (int_barrage_id > kMaxIndex) {
    barrage_id = BarrageId::kSingle;
  }
  else {
    barrage_id = static_cast<BarrageId>(int_barrage_id);
  }
  // バレッジIDをセットする
  player_->SetBarrageId(barrage_id);
  // 射撃レベルを加算する
  game_mode_->RaiseShootLevel();
}

/// <summary>
/// Iキー押下イベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushIKey() {

  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // デバッグ時のみ実行
  if (!game_info->IsDebug()) {
    return;
  }

  // 戦闘中以外は処理終了
  if (battle_phase_ != BattlePhase::kEnemyA &&
      battle_phase_ != BattlePhase::kEnemyB) {
    return;
  }
   
  if (battle_phase_ == BattlePhase::kEnemyA) {
    // フェーズ強制変更
    enemy_a_->ChangeActionPhase(EnemyA::ActionPhase::kSixthPhase);
    // 現在のHPをゼロにする
    enemy_a_->SetHitPoint(kCharacterDeath);
  }
  else {
    // フェーズ強制変更
    enemy_b_->ChangeActionPhase(EnemyB::ActionPhase::kSixthPhase);
    // 現在のHPをゼロにする
    enemy_b_->SetHitPoint(kCharacterDeath);
  }
}

/// <summary>
/// Qキー押下イベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushQKey() {

  std::cout << "プレイヤーコントローラ Qキー押下" << std::endl;

  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // デバッグ時のみ実行
  if (!game_info->IsDebug()) {
    return;
  }

  switch (current_phase_) {
  case PhaseType::kDebugMenuWait: {
    //-------------------------------
    //デバッグメニュー操作中
    //-------------------------------
    FinishMenu();

    // 敵キャラのフェーズを元に戻す
    enemy_a_->ChangeCurrentPhase(EnemyA::PhaseType::kProcess);
    enemy_b_->ChangeCurrentPhase(EnemyB::PhaseType::kProcess);

    break;
  }
  case PhaseType::kPlay: {
    //-------------------------------
    //プレイ中
    //-------------------------------
    // プレイヤーコントローラをタスクマネージャから降ろす
    Task* task = task_manager_.ReleaseTask(TaskId::kPlayerController);
    // タスクマネージャにデバッグメニューを積む
    bool is_check = task_manager_.AddTask(debug_menu_);
    if (!is_check) {
      return;
    }
    // タスクマネージャにメニューコントローラを積む
    task_manager_.AddTask(menu_controller_);
    menu_controller_->InitializePushKeyFlag();

    // 現在のフェーズを「デバッグメニュー」に変更
    ChangeCurrentPhase(PhaseType::kDebugMenu);

    // サウンド停止
    sound_manager_->PausePlaySound();
    break;
  }
  default: {
    //-------------------------------
    //それ以外
    //-------------------------------
    break;
  }
  }
}

/// <summary>
/// メニュー操作終了
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::FinishMenu() {

  std::cout << "BattleLevel FinishMenu" << std::endl;

  switch (current_phase_) {
  case PhaseType::kDebugMenuWait: {
    // デバッグメニューをタスクマネージャから降ろす
    Task* task = task_manager_.ReleaseTask(TaskId::kDebugMenu);
    break;
  }
  case PhaseType::kPauseWait: {
    // ポーズメニューをタスクマネージャから降ろす
    Task* task = task_manager_.ReleaseTask(TaskId::kPauseUi);
    break;
  }
  }

  // プレイヤーコントローラをタスクマネージャから降ろす
  Task* task = task_manager_.ReleaseTask(TaskId::kMenuController);
  // タスクマネージャにプレイヤーコントローラを積む
  task_manager_.AddTask(player_controller_);
  player_controller_->InitializePushKeyFlag();

  // ゲームを再開する
  barrage_manager_->ChangeCurrentPhase(BarrageManager::PhaseType::kPlay);
  bullet_manager_->ChangeCurrentPhase(BulletManager::PhaseType::kProcess);
  field_manager_->ChangeCurrentPhase(FieldManager::PhaseType::kProcess);
  cut_in_manager_->ChangeCurrentPhase(CutInManager::PhaseType::kProcess);
  effect_manager_->ChangeCurrentPhase(EffectManager::PhaseType::kProcess);
  hp_bar_manager_->ChangeCurrentPhase(HitPointBarManager::PhaseType::kProcess);
  game_timer_->ChangeCurrentPhase(GameTimer::PhaseType::kProcess);
  player_->ReturnTimeStop();

  bool is_finish = item_extend_->IsFinish();
  if (!is_finish) {
    item_extend_->ChangeCurrentPhase(ItemExtend::PhaseType::kProcess);
  }
  is_finish = item_power_up_->IsFinish();
  if (!is_finish) {
    item_power_up_->ChangeCurrentPhase(ItemPowerUp::PhaseType::kProcess);
  }

  // サウンド再開
  sound_manager_->ResumePlaySound();

  // 現在のフェーズを「プレイ中」に変更
  ChangeCurrentPhase(PhaseType::kPlay);
}

/// <summary>
/// BGM終了通知
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnFinishSoundBGM() {

  sound_manager_->FinishSoundBgm();
}

/// <summary>
/// キャラクター終了通知
/// </summary>
/// <param name="character"> キャラクター </param>
/// <returns></returns>
void BattleLevel::OnFinish(Character* character) {

  Character::CharacterType chara_type = character->GetCharacterType();
  switch (chara_type) {
  case Character::CharacterType::kEnemyA: {
    //----------------------------
    //敵キャラA
    //----------------------------
    // 敵キャラAをタスクマネージャから降ろす
    TaskId task_id = enemy_a_->GetTaskId();
    Task* release_task = task_manager_.ReleaseTask(task_id);

    // 敵キャラBをタスクマネージャに乗せる
    task_manager_.AddTask(enemy_b_);
    enemy_b_->ChangeCurrentPhase(EnemyB::PhaseType::kInitialize);
    enemy_b_->ChangeActionPhase(EnemyB::ActionPhase::kStart);
    break;
  }
  case Character::CharacterType::kEnemyB: {
    //----------------------------
    //敵キャラB
    //----------------------------
    // 現在のフェーズを「レベル遷移前」に変更
    ChangeCurrentPhase(PhaseType::kGameClear);

    // 敵キャラBをタスクマネージャから降ろす
    TaskId task_id = enemy_b_->GetTaskId();
    Task* release_task = task_manager_.ReleaseTask(task_id);

    // ゲーム情報にゲームクリアを渡す
    GameInfo* game_info = GameInfo::GetGameInfoInstance();
    if (game_info == nullptr) {
      break;
    }
    game_info->SetGameClear();

    break;
  }
  }
}

/// <summary>
/// HPバー生成処理
/// </summary>
/// <param> キャラクター </param>
/// <returns></returns>
void BattleLevel::OnCreateHpBar(Character* character) {

  // キャラクターの現在のHPを取得
  int hit_point = character->GetHitPoint();

  // HPバーの生成
  HitPointBar* hp_bar = hp_bar_manager_->CreateHitPointBar(HitPointBarId::kBoss);
  if (hp_bar == nullptr) {
    // 現在のフェーズを「プレイ開始」に変更
    ChangeCurrentPhase(PhaseType::kStartPlay);
    return;
  }
  hp_bar->SetCharacter(character);
  hp_bar->SetHitPoint(hit_point);
  hp_bar->SetInitialBossPosition();
  // 画面揺らし量の設定
  hp_bar->SetShakeValue(kMinusSign * kScreenShakeValue, kMinusSign * kScreenShakeValue);
  hp_bar->SetShakeValue(kMinusSign * kScreenShakeValue, kScreenShakeValue);
  hp_bar->SetShakeValue(kScreenShakeValue, kScreenShakeValue);
  hp_bar->SetShakeValue(kScreenShakeValue, kMinusSign * kScreenShakeValue);

  // リストIDを取得し、キャラクターにセットする
  int list_id = hp_bar->GetListId();
  character->SetHpBarListId(list_id);
}

/// <summary>
/// HPバー破棄処理
/// </summary>
/// <param name=""> キャラクター </param>
/// <returns></returns>
void BattleLevel::OnDisposeHpBar(Character* character) {

  // HPバーのリストIDを取得し、対象のHPバーを破棄する
  int list_id = character->GetHpBarListId();
  hp_bar_manager_->FinishHitPointBar(list_id);
}

/// <summary>
/// 弾とキャラクターの当たり判定チェック
/// </summary>
/// <param name=""> バレット </param>
/// <returns> true:当たった, false:当たっていない </returns>
bool BattleLevel::OnCheckCollisionBullet(Bullet* bullet) {

  // 弾の座標を取得
  float x_pos_bullet = bullet->GetPositionX();
  float y_pos_bullet = bullet->GetPositionY();
  // 対象の弾を発射したキャラクターを取得する
  Character* shoot_character = bullet->GetCharacter();
  Character::CharacterType chara_type = shoot_character->GetCharacterType();
  // 弾の攻撃力と弾の半径を取得
  Bullet::BulletData bullet_data = bullet->GetBulletData();
  int attack_bullet = bullet_data.spec.atttack;
  float radius_bullet = bullet->GetRadius();
  float expa_bullet = bullet_data.ext_rate.value;
  radius_bullet = radius_bullet * expa_bullet;
  // 当たり判定処理
  bool is_check = CheckCollision(x_pos_bullet, y_pos_bullet, radius_bullet,
                                 attack_bullet, chara_type);

  return is_check;
}

/// <summary>
/// 本体の当たり判定
/// </summary>
/// <param name=""> キャラクター </param>
/// <returns></returns>
void BattleLevel::OnCheckCollisionEnemy(Character* character) {

  // キャラクターの座標を取得
  float x_pos = character->GetFloatPositionX();
  float y_pos = character->GetFloatPositionY();
  // キャラクターの種類を取得
  Character::CharacterType chara_type = character->GetCharacterType();
  // 弾の攻撃力と弾の半径を取得
  Character::CharaData character_data = character->GetCharacterData();
  int attack = character_data.status.attack;
  float radius = character->GetRadius();
  float ex_rate = character_data.rate.display;
  radius = radius * ex_rate;
  // 当たり判定処理
  bool is_check = CheckCollision(x_pos, y_pos, radius, attack, chara_type);
}

/// <summary>
/// 当たり判定
/// </summary>
/// <param name="x_pos"> 攻撃側のX座標 </param>
/// <param name="y_pos"> 攻撃側のY座標 </param>
/// <param name="radius"> 攻撃側の半径 </param>
/// <param name="attack"> 攻撃側の攻撃力 </param>
/// <param name="chara_type"> 攻撃側のキャラクターの種類 </param>
/// <returns> true:当たった, false:当たってない </returns>
bool BattleLevel::CheckCollision(float x_pos, float y_pos, float radius, int attack,
                                 Character::CharacterType chara_type) {

  if (current_phase_ != PhaseType::kPlay) {
    return false;
  }

  float x_pos_opponent = 0;
  float y_pos_opponent = 0;
  float radius_opponent = 0.0f;
  float ex_rate_opponent = 0.0f;
  Character* chara_opponent = nullptr;
  bool is_dead = false;

  // 攻撃を行う側のキャラクターの種類を取得する
  switch (chara_type) {
  case Character::CharacterType::kPlayer: {
    //---------------------------------
    //プレイヤー
    //---------------------------------
    // 敵との当たり判定
    switch (battle_phase_) {
    case BattlePhase::kEnemyA: {
      // 敵キャラの現在の位置と実際のサイズを取得
      x_pos_opponent = enemy_a_->GetFloatPositionX();
      y_pos_opponent = enemy_a_->GetFloatPositionY();
      chara_opponent = enemy_a_->GetCharacter();
      radius_opponent = enemy_a_->GetRadius();
      ex_rate_opponent = enemy_a_->GetImageDispRate();
      radius_opponent = radius_opponent * ex_rate_opponent;
      is_dead = enemy_a_->IsDead();
      break;
    }
    case BattlePhase::kEnemyB: {
      // 敵キャラの現在の位置と実際のサイズを取得
      x_pos_opponent = enemy_b_->GetFloatPositionX();
      y_pos_opponent = enemy_b_->GetFloatPositionY();
      chara_opponent = enemy_b_->GetCharacter();
      radius_opponent = enemy_b_->GetRadius();
      ex_rate_opponent = enemy_b_->GetImageDispRate();
      radius_opponent = radius_opponent * ex_rate_opponent;
      is_dead = enemy_b_->IsDead();
      break;
    }
    }

    //====================================================================
    // 敵の座標を中心とした円と弾の座標を中心とした円を考え、
    // 2点間の座標の距離が2つの円の半径の和より小さい場合、当たったとする
    //====================================================================
    // 2点間の座標の距離
    float differ_x = std::abs(x_pos_opponent - x_pos);
    float differ_y = std::abs(y_pos_opponent - y_pos);

    // 円の半径の和の2乗
    float circle_distance = std::powf(radius_opponent + radius, kSquared);
    // 2点間の距離の座標の2乗の和
    float coor_distance = std::powf(differ_x, kSquared) + std::powf(differ_y, kSquared);
    // 三平方の定理を用いて距離の比較を行う
    if (coor_distance < circle_distance) {
      // プレイヤーのダメージ計算処理し、
      // ヒットポイントが「0」以下 かつ 生存中 ならゲームの遷移を行う
      int hit_point = CalcDamage(chara_opponent, attack);
      if ((hit_point <= kCharacterDeath) && (is_dead == false)) {
        SwitchGamePhase(chara_opponent);
      }
      return true;
    }
    break;
  }
  case Character::CharacterType::kEnemyA:
  case Character::CharacterType::kEnemyB: {
    //---------------------------------
    //敵キャラ
    //---------------------------------
    // プレイヤーが死亡していたら処理終了
    if (player_->IsDead()) {
      return false;
    }

    // プレイヤーの現在の位置と実際のサイズを取得
    x_pos_opponent = player_->GetFloatPositionX();
    y_pos_opponent = player_->GetFloatPositionY();
    chara_opponent = player_->GetCharacter();
    radius_opponent = player_->GetRadius();

    //====================================================================
    // 敵の座標を中心とした円と弾の座標を中心とした円を考え、
    // 2点間の座標の距離が2つの円の半径の和より小さい場合、当たったとする
    //====================================================================
    // 2点間の座標の距離
    float differ_x = std::abs(x_pos_opponent - x_pos);
    float differ_y = std::abs(y_pos_opponent - y_pos);

    // 円の半径の和の2乗
    float circle_distance = std::powf(radius_opponent + radius, kSquared);
    // 2点間の距離の座標の2乗の和
    float coor_distance = std::powf(differ_x, kSquared) + std::powf(differ_y, kSquared);
    // 三平方の定理を用いて距離の比較を行う
    if (coor_distance < circle_distance) {
      // プレイヤーのダメージ計算処理し、
      // ヒットポイントが「0」以下ならゲームの遷移を行う
      int hit_point = CalcDamage(chara_opponent, attack);
      if (hit_point <= kCharacterDeath) {
        SwitchGamePhase(chara_opponent);
      }
      return true;
    }
    break;
  }
  case Character::CharacterType::kItemA:
  case Character::CharacterType::kItemB: {
    //---------------------------------
    //アイテム
    //---------------------------------
    // プレイヤーの現在の位置と実際のサイズを取得
    x_pos_opponent = player_->GetFloatPositionX();
    y_pos_opponent = player_->GetFloatPositionY();
    chara_opponent = player_->GetCharacter();
    radius_opponent = static_cast<float>(player_->GetImageHalfHeight());

    //====================================================================
    // 敵の座標を中心とした円と弾の座標を中心とした円を考え、
    // 2点間の座標の距離が2つの円の半径の和より小さい場合、当たったとする
    //====================================================================
    // 2点間の座標の距離
    float differ_x = std::abs(x_pos_opponent - x_pos);
    float differ_y = std::abs(y_pos_opponent - y_pos);

    // 円の半径の和の2乗
    float circle_distance = std::powf(radius_opponent + radius, kSquared);
    // 2点間の距離の座標の2乗の和
    float coor_distance = std::powf(differ_x, kSquared) + std::powf(differ_y, kSquared);
    // 三平方の定理を用いて距離の比較を行う
    if (coor_distance < circle_distance) {

      // アイテム獲得サウンドを生成
      sound_manager_->CreateSound(SoundId::kGetItem);

      // 接触している場合はアイテムを獲得する
      switch (chara_type) {
      case Character::CharacterType::kItemA: {
        game_mode_->IncreaseLeftLife();
        break;
      }
      case Character::CharacterType::kItemB: {
        game_mode_->RaiseShootLevel();
        player_->ChangeShootType();
        break;
      }
      }
      return true;
    }
    break;
  }
  }

  return false;
}

/// <summary>
/// ダメージ計算処理
/// </summary>
/// <param name="character"> 相手のキャラクター </param>
/// <param name="attack"> 攻撃力 </param>
/// <returns> キャラクターのヒットポイント </returns>
int BattleLevel::CalcDamage(Character* character, int attack) {

  // キャラクターの防御力を取得
  int character_defense = character->GetDefense();
  // キャラクターのヒットポイントを取得
  int hit_point = character->GetHitPoint();

  // 相手キャラクターが死んでいるなら処理終了
  if (character->IsDead()) {
    return hit_point;
  }

  // 対象の攻撃力と相対するキャラクターの防御力の差分をダメージとする
  if (attack > character_defense) {
    int damage = attack - character_defense;
    hit_point -= damage;
    // ヒットポイントをセットする
    character->SetHitPoint(hit_point);
  }

  return hit_point;
}

/// <summary>
/// ゲーム遷移処理
/// </summary>
/// <param name="character"> キャラクター </param>
/// <returns></returns>
void BattleLevel::SwitchGamePhase(Character* character) {

  // 対象キャラクターが死亡しているなら処理終了
  if (character->IsDead()) {
    return;
  }

  // キャラクターを死亡したことにする
  character->SetDead();

  // キャラクターの種類を取得
  Character::CharacterType chara_type = character->GetCharacterType();
  switch (chara_type) {
  case Character::CharacterType::kPlayer: {
    //-------------------------------------
    //プレイヤー
    //-------------------------------------
    // プレイヤーのフェーズを「停止」に変更する
    player_->ChangeCurrentPhase(Player::PhaseType::kStop);
    // プレイヤーの特殊フェーズを「死亡フェーズ」に変更する
    player_->ChangeSpecialPhase(Player::SpecialPhase::kPlayerDeath);
    // バトルレベルの現在のフェーズを「停止準備」に変更する
    ChangeCurrentPhase(PhaseType::kStopPrepare);
    break;
  }
  case Character::CharacterType::kEnemyA: {
    //-------------------------------------
    //敵キャラA
    //-------------------------------------
    // HPバーを揺らす
    int list_id = enemy_a_->GetHpBarListId();
    HitPointBar* hp_bar = hp_bar_manager_->GetHitPointBar(list_id);
    hp_bar->StartScreenShake();

    // 最終フェーズかどうか確認
    bool is_check = enemy_a_->IsFinalPhase();
    if (is_check) {
      // バトルフェーズを「敵キャラB表示遷移」に変更
      ChangeBattlePhase(BattlePhase::kTransitionEnemyB);
      break;
    }
    break;
  }
  case Character::CharacterType::kEnemyB: {
    //-------------------------------------
    //敵キャラB
    //-------------------------------------
    // HPバーを揺らす
    int list_id = enemy_b_->GetHpBarListId();
    HitPointBar* hp_bar = hp_bar_manager_->GetHitPointBar(list_id);
    hp_bar->StartScreenShake();

    // 最終フェーズかどうか確認
    bool is_check = enemy_b_->IsFinalPhase();
    if (is_check) {
      // バトルフェーズを「終了」に変更
      ChangeBattlePhase(BattlePhase::kFinish);
      break;
    }
    break;
  }
  }
}

/// <summary>
/// エフェクト処理
/// </summary>
/// <param name="effect_id"> エフェクトID </param>
/// <param name="x_pos"> X座標</param>
/// <param name="y_pos"> Y座標</param>
/// <returns></returns>
void BattleLevel::ExecuteEffect(EffectId effect_id, int x_pos, int y_pos) {

  // エフェクトの生成
  Effect* effect = effect_manager_->CreateEffect(effect_id);
  if (effect == nullptr) {
    return;
  }

  // 表示座標のセット
  effect->SetPosX(x_pos);
  effect->SetPosY(y_pos);

  // サウンド処理
  switch (effect_id) {
  case EffectId::kPlayerDeath: {
    sound_manager_->CreateSound(SoundId::kPlayerDeath);
    break;
  }
  case EffectId::kExplosion: {
    sound_manager_->CreateSound(SoundId::kPlayerExplosion);
    break;
  }
  case EffectId::kReborn: {
    sound_manager_->CreateSound(SoundId::kPlayerReborn);
    break;
  }
  }
}

/// <summary>
/// 弾幕表示開始
/// </summary>
/// <param name = ""> バレッジID < / param>
/// <returns></returns>
Barrage* BattleLevel::OnStartBarrage(BarrageId barrage_id) {

  Barrage* barrage = barrage_manager_->DeployBarrageBullet(barrage_id);
  if (barrage == nullptr) {
    return nullptr;
  }
  return barrage;
}

/// <summary>
/// 弾幕表示終了
/// </summary>
/// <param></param>
/// <returns></returns>
void BattleLevel::OnFinishBarrage() {

  barrage_manager_->FinishGenerateBarrage();
}

/// <summary>
/// 弾の表示終了
/// </summary>
/// <param name="bullet"> キャラクター </param>
/// <returns></returns>
void BattleLevel::OnFinishDisplayBullet(Character* character) {

  // 描画中の対象キャラクターの弾を全て終了状態にする
  bullet_manager_->FinishBullet(character);
}

/// <summary>
/// 弾の表示終了
/// </summary>
/// <param name="bullet"> バレット </param>
/// <returns></returns>
void BattleLevel::OnFinishBullet(Bullet* bullet) {

  // エフェクトID
  EffectId effect_id = EffectId::kEffectMaxIndex;
  // 弾の種類を取得する
  BulletId bullet_id = bullet->GetBulletId();
  switch (bullet_id) {
  case BulletId::kNormal: {
    effect_id = EffectId::kDisappearNormal;
    break;
  }
  case BulletId::kBlueBall: {
    effect_id = EffectId::kDisappearBlueBall;
    break;
  }
  case BulletId::kPurpleBall: {
    effect_id = EffectId::kDisappearPurpleBall;
    break;
  }
  case BulletId::kRedBall: {
    effect_id = EffectId::kDisappearRedBall;
    break;
  }
  case BulletId::kYellowBall: {
    effect_id = EffectId::kDisappearYellowBall;
    break;
  }
  case BulletId::kGreenBall: {
    effect_id = EffectId::kDisappearGreenBall;
    break;
  }
  }

  // エフェクトIDが存在しない場合は処理終了
  if (effect_id == EffectId::kEffectMaxIndex) {
    return;
  }
  // 表示座標を取得
  int x_pos = static_cast<int>(bullet->GetPositionX());
  int y_pos = static_cast<int>(bullet->GetPositionY());
  // 弾消滅エフェクトを実行
  Effect* effect = effect_manager_->CreateEffect(effect_id);
  // 座標のセット
  effect->SetPosX(x_pos);
  effect->SetPosY(y_pos);
}

/// <summary>
/// 弾の生成
/// </summary>
/// <param name="bullet_id"> バレットID </param>
/// <returns></returns>
Bullet* BattleLevel::OnCreateBullet(BulletId bullet_id) {

  // 弾の生成
  Bullet* bullet = bullet_manager_->CreateBullet(bullet_id);
  if (bullet == nullptr) {
    std::cout << "BattleLevel OnPushZKey：弾クラス生成失敗" << std::endl;
    OccurredError();
    return nullptr;
  }

  return bullet;
}

/// <summary>
/// プレイヤーの座標を取得
/// </summary>
/// <param name="x_pos"> X座標 </param>
/// <param name="y_pos"> Y座標 </param>
/// <returns></returns>
void BattleLevel::OnGetPlayerPosition(float& x_pos, float& y_pos) {

  x_pos = player_->GetFloatPositionX();
  y_pos = player_->GetFloatPositionY();
}

/// <summary>
/// バトルフェーズの変更
/// </summary>
/// <param name="character"> キャラクター </param>
/// <returns></returns>
void BattleLevel::OnChangeBattlePhase(Character* character) {

  Character::CharacterType chara_type = character->GetCharacterType();
  switch (chara_type) {
  case Character::CharacterType::kEnemyA: {
    //----------------------------
    //敵キャラA
    //----------------------------
    // バトルフェーズを「敵キャラA」に変更する
    ChangeBattlePhase(BattlePhase::kEnemyA);
    //// 現在のフェーズを「プレイ開始」に変更する
    //ChangeCurrentPhase(PhaseType::kStartPlay);
    break;
  }
  case Character::CharacterType::kEnemyB: {
    //----------------------------
    //敵キャラB
    //----------------------------
    // バトルフェーズを「敵キャラB」に変更する
    ChangeBattlePhase(BattlePhase::kEnemyB);
    // 敵キャラBのフェーズを「処理中」に変更する
    enemy_b_->ChangeCurrentPhase(EnemyB::PhaseType::kProcess);
    break;
  }
  }
}

/// <summary>
/// アイテム生成
/// </summary>
/// <param name="character_type"> キャラクターの種類 </param>
/// <returns></returns>
void BattleLevel::OnCreateItem(Character::CharacterType character_type) {

  int x_pos = 0;
  int y_pos = 0;

  switch (character_type) {
  case Character::CharacterType::kItemA: {
    //-----------------------------------
    //残機UPアイテム
    //-----------------------------------
    // タスクマネージャにタスクを乗せる
    task_manager_.AddTask(item_extend_);
    // タスクのフェーズを初期化フェーズにセット
    item_extend_->ChangeCurrentPhase(ItemExtend::PhaseType::kInitialize);
    // 表示座標を取得
    x_pos = item_extend_->GetInitialPositionX();
    y_pos = item_extend_->GetInitialPositionY();
    break;
  }
  case Character::CharacterType::kItemB: {
    //-----------------------------------
    //パワーUPアイテム
    //-----------------------------------
    // タスクマネージャにタスクを乗せる
    task_manager_.AddTask(item_power_up_);
    // タスクのフェーズを初期化フェーズにセット
    item_power_up_->ChangeCurrentPhase(ItemPowerUp::PhaseType::kInitialize);
    // 表示座標を取得
    x_pos = item_power_up_->GetInitialPositionX();
    y_pos = item_power_up_->GetInitialPositionY();
    break;
  }
  }

  // アイテム生成のエフェクト実行
  ExecuteEffect(EffectId::kItemSummon, x_pos, y_pos);
}

/// <summary>
/// アイテム処理の終了を通知する
/// </summary>
/// <param name=""> キャラクター </param>
/// <returns></returns>
void BattleLevel::OnFinishItem(Character* character) {

  Character::CharacterType character_type = character->GetCharacterType();
  switch (character_type) {
  case Character::CharacterType::kItemA: {
    //-----------------------------------
    //残機UPアイテム
    //-----------------------------------
    // タスクマネージャからタスクを降ろす
    TaskId task_id = item_extend_->GetTaskId();
    Task* task = task_manager_.ReleaseTask(task_id);
    break;
  }
  case Character::CharacterType::kItemB: {
    //-----------------------------------
    //パワーUPアイテム
    //-----------------------------------
    // タスクマネージャからタスクを降ろす
    TaskId task_id = item_power_up_->GetTaskId();
    Task* task = task_manager_.ReleaseTask(task_id);
    break;
  }
  }

  // 敵フェーズの変更処理
  switch (battle_phase_) {
  case BattlePhase::kEnemyA: {
    enemy_a_->ChangeDispItemPhase(EnemyA::DispItemPhase::kFinishNotice);
    break;
  }
  case BattlePhase::kEnemyB: {
    enemy_b_->ChangeDispItemPhase(EnemyB::DispItemPhase::kFinishNotice);
    break;
  }
  }
}

/// <summary>
/// カットイン処理
/// </summary>
/// <param name="phase_num"> フェーズ数 </param>
/// <param name="color_type"> カットインの色 </param>
/// <returns></returns>
void BattleLevel::OnDisplayCutIn(int phase_num, CutInManager::ColorType color_type) {

  std::string phase_name;
  switch (battle_phase_) {
  case BattlePhase::kTransitionEnemyA:
  case BattlePhase::kEnemyA: {
    phase_name = kBossPhaseNameA;
    break;
  }
  case BattlePhase::kTransitionEnemyB:
  case BattlePhase::kEnemyB: {
    phase_name = kBossPhaseNameB;
    break;
  }
  }

  // ボスフェーズ名
  cut_in_manager_->SetBossPhaseName(phase_name);
  // 実行カットインに指定のカットインをセット
  cut_in_manager_->SetExecCutIn(color_type);
  // カットインマネージャの描画フェーズを初期化にセット
  cut_in_manager_->ChangeDrawPhase(CutInManager::DrawPhase::kInitialize);
  // カットインのフェーズを初期化にセット
  cut_in_manager_->ChangeCutInPhase(CutIn::PhaseType::kInitialize);
  // カットインにフェーズ数をセット
  cut_in_manager_->SetActionPhaseNumber(phase_num);

  // カットインのサウンド生成
  sound_manager_->CreateSound(SoundId::kCutIn);
}

/// <summary>
/// カットイン処理終了通知
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnFinishCutIn() {

  // 敵フェーズの変更処理
  switch (battle_phase_) {
  case BattlePhase::kEnemyA: {
    enemy_a_->ChangeCutInPhase(EnemyA::CutInPhase::kFinishNotice);
    break;
  }
  case BattlePhase::kEnemyB: {
    enemy_b_->ChangeCutInPhase(EnemyB::CutInPhase::kFinishNotice);
    break;
  }
  }
}

/// <summary>
/// バトル再開
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnResumeBattle() {

  // ゲーム画面上のすべての時間を再開する
  barrage_manager_->ChangeCurrentPhase(BarrageManager::PhaseType::kPlay);
  bullet_manager_->ChangeCurrentPhase(BulletManager::PhaseType::kProcess);
  field_manager_->ChangeCurrentPhase(FieldManager::PhaseType::kProcess);
  enemy_a_->ChangeCurrentPhase(EnemyA::PhaseType::kProcess);
  enemy_b_->ChangeCurrentPhase(EnemyB::PhaseType::kProcess);
}

/// <summary>
/// エフェクト処理
/// </summary>
/// <param name="effect_id"> エフェクトID </param>
/// <returns></returns>
void BattleLevel::OnExecuteEffect(EffectId effect_id, Character* character) {

  int x_pos = character->GetPositionX();
  int y_pos = character->GetPositionY();

  // エフェクト実行
  ExecuteEffect(effect_id, x_pos, y_pos);
}

/// <summary>
/// 爆破用エフェクトを実行
/// </summary>
/// <param name="character"> キャラクター </param>
/// <returns></returns>
void BattleLevel::OnExecuteEnemyEffect(Character* character) {

  // 座標の取得
  int x_pos = character->GetPositionX();
  int y_pos = character->GetPositionY();

  // 敵の爆破エフェクト実行
  Effect* effect = effect_manager_->CreateEffect(EffectId::kEnemyExplosion);
  if (effect == nullptr) {
    return;
  }
  // 表示座標のセット
  effect->SetPosX(x_pos - kEnemyExplosionPos01X);
  effect->SetPosY(y_pos - kEnemyExplosionPos02Y);

  effect = nullptr;

  // 敵の爆破エフェクト実行
  effect = effect_manager_->CreateEffect(EffectId::kEnemyExplosion);
  if (effect == nullptr) {
    return;
  }

  // 表示座標のセット
  effect->SetPosX(x_pos + kEnemyExplosionPos02X);
  effect->SetPosY(y_pos + kEnemyExplosionPos02Y);
  effect->SetExtRateX(effect_list_[EffectId::kEnemyExplosion].ex_rate_x.value,
                      kEnemyExplosionExRateAdjust,
                      kEnemyExplosionExRateMax,
                      effect_list_[EffectId::kEnemyExplosion].ex_rate_x.min);

  // 爆破サウンドを生成
  sound_manager_->CreateSound(SoundId::kEnemyExplosion);
}

/// <summary>
/// 残機のチェック
/// </summary>
/// <param name=""></param>
/// <returns> true:ゲームオーバー, false:再開 </returns>
bool BattleLevel::OnCheckLeftLife() {

  // 残機をチェックし、残機がゼロならゲームオーバー
  bool is_finish = game_mode_->IsLeftLifeZero();
  if (is_finish) {
    // 現在のフェーズを「ゲームオーバー」に変更
    ChangeCurrentPhase(PhaseType::kGameOver);
    // タイマーの停止
    game_timer_->StopTime();
    // バトルBGMを停止する
    sound_manager_->FinishSoundBgm();
    return true;
  }

  // 残機を減らす
  game_mode_->DecreaseLeftLife();

  return false;
}

/// <summary>
/// ゲームクリア処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void BattleLevel::ExecuteGameClear(float process_time) {

  switch (game_clear_phase_) {
  case GameClearPhase::kTaskAdd: {
    //----------------------------
    //タスクを積む
    //----------------------------
    // 終了UIをタスクマネージャに積む
    task_manager_.AddTask(game_clear_);
    // ゲームクリアフェーズを「演出待機」に変更
    ChangeGameClearPhase(GameClearPhase::kProduction);
    // ゲームクリア時のサウンド生成
    sound_manager_->CreateSound(SoundId::kGameClear);

    // クリアタイムをゲーム情報に渡す
    float clear_time = game_timer_->GetTime();
    int clear_time_millisec = static_cast<int>(clear_time * kThousand);

    GameInfo* game_info = GameInfo::GetGameInfoInstance();
    if (game_info == nullptr) {
      break;
    }
    game_info->SetClearTime(clear_time_millisec);

    break;
  }
  case GameClearPhase::kProduction: {
    //----------------------------
    //演出待機
    //----------------------------
    break;
  }
  case GameClearPhase::kTaskRelease: {
    //----------------------------
    //タスクを降ろす
    //----------------------------
    // プレイヤーコントローラをタスクマネージャから降ろす
    TaskId task_id = player_controller_->GetTaskId();
    task_manager_.ReleaseTask(task_id);
    // ゲームクリアフェーズを「プレイヤー初期位置セット」に変更
    ChangeGameClearPhase(GameClearPhase::kPlayerInit);
    break;
  }
  case GameClearPhase::kPlayerInit: {
    //----------------------------
    //プレイヤー初期位置セット
    //----------------------------
    bool is_finish = ResetPlayerPosition();
    if (is_finish) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      // ゲームクリアフェーズを「プレイヤー移動」に変更
      ChangeGameClearPhase(GameClearPhase::kPlayerMove);
    }
    break;
  }
  case GameClearPhase::kPlayerMove: {
    //----------------------------
    //プレイヤー移動
    //----------------------------
    bool is_finish = MovePlayer(process_time);
    if (is_finish) {
      // ゲームクリアフェーズを「終了処理」に変更
      ChangeGameClearPhase(GameClearPhase::kFinish);
    }
    break;
  }
  case GameClearPhase::kFinish: {
    //----------------------------
    //終了処理
    //----------------------------
  // 遷移するレベルにリザルトレベルをセットする
    transition_level_ = TaskId::kResultLevel;
    // 現在のフェーズを「レベル遷移前」に変更
    ChangeCurrentPhase(PhaseType::kBeforeTransition);
    // ゲームクリアフェーズを「終了処理済み」に変更
    ChangeGameClearPhase(GameClearPhase::kFinished);
    break;
  }
  }
}

/// <summary>
/// 花火の打ち上げ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnShootFirework1() {

  // 表示座標
  const int kPosX01 = 300;
  const int kPosY01 = 675;
  const int kPosX02 = 725;
  const int kPosY02 = 75;
  const int kPosX03 = 350;
  const int kPosY03 = 280;
  const int kPosX04 = 600;
  const int kPosY04 = 380;
  const int kPosX05 = 750;
  const int kPosY05 = 725;

  // 拡大率
  const float kExRateAdjust01 = 0.1f;
  const float kExRateMax01 = 4.0f;
  const float kExRateAdjust02 = 0.1f;
  const float kExRateMax02 = 5.0f;
  const float kExRateAdjust03 = 0.1f;
  const float kExRateMax03 = 7.0f;
  const float kExRateAdjust04 = 0.1f;
  const float kExRateMax04 = 10.0f;
  const float kExRateAdjust05 = 0.1f;
  const float kExRateMax05 = 3.0f;

  // 敵の爆破エフェクト実行
  Effect* effect = effect_manager_->CreateEffect(EffectId::kDisappearYellowBall);
  if (effect == nullptr) {
    return;
  }
  // 表示座標のセット
  effect->SetPosX(kPosX01);
  effect->SetPosY(kPosY01);
  effect->SetExtRateX(effect_list_[EffectId::kDisappearYellowBall].ex_rate_x.value,
                      kExRateAdjust01,
                      kExRateMax01,
                      effect_list_[EffectId::kDisappearYellowBall].ex_rate_x.min);


  effect = nullptr;

  // 敵の爆破エフェクト実行
  effect = effect_manager_->CreateEffect(EffectId::kDisappearGreenBall);
  if (effect == nullptr) {
    return;
  }

  // 表示座標のセット
  effect->SetPosX(kPosX02);
  effect->SetPosY(kPosY02);
  effect->SetExtRateX(effect_list_[EffectId::kDisappearGreenBall].ex_rate_x.value,
                      kExRateAdjust02,
                      kExRateMax02,
                      effect_list_[EffectId::kDisappearGreenBall].ex_rate_x.min);


  effect = nullptr;

  // 敵の爆破エフェクト実行
  effect = effect_manager_->CreateEffect(EffectId::kDisappearBlueBall);
  if (effect == nullptr) {
    return;
  }

  // 表示座標のセット
  effect->SetPosX(kPosX03);
  effect->SetPosY(kPosY03);
  effect->SetExtRateX(effect_list_[EffectId::kDisappearBlueBall].ex_rate_x.value,
                      kExRateAdjust03,
                      kExRateMax03,
                      effect_list_[EffectId::kDisappearBlueBall].ex_rate_x.min);

  effect = nullptr;

  // 敵の爆破エフェクト実行
  effect = effect_manager_->CreateEffect(EffectId::kDisappearRedBall);
  if (effect == nullptr) {
    return;
  }

  // 表示座標のセット
  effect->SetPosX(kPosX04);
  effect->SetPosY(kPosY04);
  effect->SetExtRateX(effect_list_[EffectId::kDisappearRedBall].ex_rate_x.value,
                      kExRateAdjust04,
                      kExRateMax04,
                      effect_list_[EffectId::kDisappearRedBall].ex_rate_x.min);


  effect = nullptr;

  // 敵の爆破エフェクト実行
  effect = effect_manager_->CreateEffect(EffectId::kDisappearPurpleBall);
  if (effect == nullptr) {
    return;
  }

  // 表示座標のセット
  effect->SetPosX(kPosX05);
  effect->SetPosY(kPosY05);
  effect->SetExtRateX(effect_list_[EffectId::kDisappearPurpleBall].ex_rate_x.value,
                      kExRateAdjust05,
                      kExRateMax05,
                      effect_list_[EffectId::kDisappearPurpleBall].ex_rate_x.min);
};

/// <summary>
/// 花火の打ち上げ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnShootFirework2() {

  // 表示座標
  const int kPosX01 = 475;
  const int kPosY01 = 100;
  const int kPosX02 = 400;
  const int kPosY02 = 380;
  const int kPosX03 = 300;
  const int kPosY03 = 650;
  const int kPosX04 = 700;
  const int kPosY04 = 430;
  const int kPosX05 = 650;
  const int kPosY05 = 725;

  // 拡大率
  const float kExRateAdjust01 = 0.1f;
  const float kExRateMax01 = 7.0f;
  const float kExRateAdjust02 = 0.1f;
  const float kExRateMax02 = 4.0f;
  const float kExRateAdjust03 = 0.1f;
  const float kExRateMax03 = 10.0f;
  const float kExRateAdjust04 = 0.1f;
  const float kExRateMax04 = 5.0f;
  const float kExRateAdjust05 = 0.1f;
  const float kExRateMax05 = 3.0f;

  // 敵の爆破エフェクト実行
  Effect* effect = effect_manager_->CreateEffect(EffectId::kDisappearYellowBall);
  if (effect == nullptr) {
    return;
  }
  // 表示座標のセット
  effect->SetPosX(kPosX01);
  effect->SetPosY(kPosY01);
  effect->SetExtRateX(effect_list_[EffectId::kDisappearYellowBall].ex_rate_x.value,
                      kExRateAdjust01,
                      kExRateMax01,
                      effect_list_[EffectId::kDisappearYellowBall].ex_rate_x.min);


  effect = nullptr;

  // 敵の爆破エフェクト実行
  effect = effect_manager_->CreateEffect(EffectId::kDisappearRedBall);
  if (effect == nullptr) {
    return;
  }

  // 表示座標のセット
  effect->SetPosX(kPosX02);
  effect->SetPosY(kPosY02);
  effect->SetExtRateX(effect_list_[EffectId::kDisappearRedBall].ex_rate_x.value,
                      kExRateAdjust02,
                      kExRateMax02,
                      effect_list_[EffectId::kDisappearRedBall].ex_rate_x.min);


  effect = nullptr;

  // 敵の爆破エフェクト実行
  effect = effect_manager_->CreateEffect(EffectId::kDisappearPurpleBall);
  if (effect == nullptr) {
    return;
  }

  // 表示座標のセット
  effect->SetPosX(kPosX03);
  effect->SetPosY(kPosY03);
  effect->SetExtRateX(effect_list_[EffectId::kDisappearPurpleBall].ex_rate_x.value,
                      kExRateAdjust03,
                      kExRateMax03,
                      effect_list_[EffectId::kDisappearPurpleBall].ex_rate_x.min);

  effect = nullptr;

  // 敵の爆破エフェクト実行
  effect = effect_manager_->CreateEffect(EffectId::kDisappearBlueBall);
  if (effect == nullptr) {
    return;
  }

  // 表示座標のセット
  effect->SetPosX(kPosX04);
  effect->SetPosY(kPosY04);
  effect->SetExtRateX(effect_list_[EffectId::kDisappearBlueBall].ex_rate_x.value,
                      kExRateAdjust04,
                      kExRateMax04,
                      effect_list_[EffectId::kDisappearBlueBall].ex_rate_x.min);

  effect = nullptr;

  // 敵の爆破エフェクト実行
  effect = effect_manager_->CreateEffect(EffectId::kDisappearGreenBall);
  if (effect == nullptr) {
    return;
  }

  // 表示座標のセット
  effect->SetPosX(kPosX05);
  effect->SetPosY(kPosY05);
  effect->SetExtRateX(effect_list_[EffectId::kDisappearGreenBall].ex_rate_x.value,
                      kExRateAdjust05,
                      kExRateMax05,
                      effect_list_[EffectId::kDisappearGreenBall].ex_rate_x.min);
};

/// <summary>
/// 花火の打ち上げ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnShootFirework3() {

  // 表示座標
  const int kPosX01 = 275;
  const int kPosY01 = 75;
  const int kPosX02 = 580;
  const int kPosY02 = 200;
  const int kPosX03 = 325;
  const int kPosY03 = 400;
  const int kPosX04 = 700;
  const int kPosY04 = 650;
  const int kPosX05 = 320;
  const int kPosY05 = 690;

  // 拡大率
  const float kExRateAdjust01 = 0.1f;
  const float kExRateMax01 = 3.0f;
  const float kExRateAdjust02 = 0.1f;
  const float kExRateMax02 = 10.0f;
  const float kExRateAdjust03 = 0.1f;
  const float kExRateMax03 = 5.0f;
  const float kExRateAdjust04 = 0.1f;
  const float kExRateMax04 = 7.0f;
  const float kExRateAdjust05 = 0.1f;
  const float kExRateMax05 = 4.0f;

  // 敵の爆破エフェクト実行
  Effect* effect = effect_manager_->CreateEffect(EffectId::kDisappearRedBall);
  if (effect == nullptr) {
    return;
  }
  // 表示座標のセット
  effect->SetPosX(kPosX01);
  effect->SetPosY(kPosY01);
  effect->SetExtRateX(effect_list_[EffectId::kDisappearRedBall].ex_rate_x.value,
                      kExRateAdjust01,
                      kExRateMax01,
                      effect_list_[EffectId::kDisappearRedBall].ex_rate_x.min);

  effect = nullptr;

  // 敵の爆破エフェクト実行
  effect = effect_manager_->CreateEffect(EffectId::kDisappearPurpleBall);
  if (effect == nullptr) {
    return;
  }

  // 表示座標のセット
  effect->SetPosX(kPosX02);
  effect->SetPosY(kPosY02);
  effect->SetExtRateX(effect_list_[EffectId::kDisappearPurpleBall].ex_rate_x.value,
                      kExRateAdjust02,
                      kExRateMax02,
                      effect_list_[EffectId::kDisappearPurpleBall].ex_rate_x.min);

  effect = nullptr;

  // 敵の爆破エフェクト実行
  effect = effect_manager_->CreateEffect(EffectId::kDisappearBlueBall);
  if (effect == nullptr) {
    return;
  }

  // 表示座標のセット
  effect->SetPosX(kPosX03);
  effect->SetPosY(kPosY03);
  effect->SetExtRateX(effect_list_[EffectId::kDisappearBlueBall].ex_rate_x.value,
                      kExRateAdjust03,
                      kExRateMax03,
                      effect_list_[EffectId::kDisappearBlueBall].ex_rate_x.min);

  effect = nullptr;

  // 敵の爆破エフェクト実行
  effect = effect_manager_->CreateEffect(EffectId::kDisappearYellowBall);
  if (effect == nullptr) {
    return;
  }

  // 表示座標のセット
  effect->SetPosX(kPosX04);
  effect->SetPosY(kPosY04);
  effect->SetExtRateX(effect_list_[EffectId::kDisappearYellowBall].ex_rate_x.value,
                      kExRateAdjust04,
                      kExRateMax04,
                      effect_list_[EffectId::kDisappearYellowBall].ex_rate_x.min);


  effect = nullptr;

  // 敵の爆破エフェクト実行
  effect = effect_manager_->CreateEffect(EffectId::kDisappearGreenBall);
  if (effect == nullptr) {
    return;
  }

  // 表示座標のセット
  effect->SetPosX(kPosX05);
  effect->SetPosY(kPosY05);
  effect->SetExtRateX(effect_list_[EffectId::kDisappearGreenBall].ex_rate_x.value,
                      kExRateAdjust05,
                      kExRateMax05,
                      effect_list_[EffectId::kDisappearGreenBall].ex_rate_x.min);

};

/// <summary>
/// テキスト非表示終了通知
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnFinishHideGameClearText() {

  // ゲームクリアフェーズを「タスクを降ろす」に変更
  ChangeGameClearPhase(GameClearPhase::kTaskRelease);
}

/// <summary>
/// プレイヤーを初期位置に戻す
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::ResetPlayerPosition() {

  int x_pos_init = player_->GetInitialPositionX();
  int y_pos_init = player_->GetInitialPositionY();
  int x_pos = player_->GetPositionX();
  int y_pos = player_->GetPositionY();

  // 初期位置よりも画面左側に存在する場合
  if (x_pos < x_pos_init) {
    // プレイヤーを右方向へ移動
    x_pos += kPlayerMoveValueX;
    // プレイヤーが移動しすぎた場合は調整する
    if (x_pos >= x_pos_init) {
      x_pos = x_pos_init;
    }
    // プレイヤーのX座標位置をセット
    player_->SetPositionX(x_pos);
  }
  // 初期位置よりも画面右側に存在する場合
  else if (x_pos > x_pos_init) {
    // プレイヤーを左方向へ移動
    x_pos -= kPlayerMoveValueX;
    // プレイヤーが移動しすぎた場合は調整する
    if (x_pos <= x_pos_init) {
      x_pos = x_pos_init;
    }
    // プレイヤーのX座標位置をセット
    player_->SetPositionX(x_pos);
  }

  // 初期位置よりも画面上側に存在する場合
  if (y_pos < y_pos_init) {
    // プレイヤーを右方向へ移動
    y_pos += kPlayerMoveValueY;
    // プレイヤーが移動しすぎた場合は調整する
    if (y_pos >= y_pos_init) {
      y_pos = y_pos_init;
    }
    // プレイヤーのY座標位置をセット
    player_->SetPositionY(y_pos);
  }
  // 初期位置よりも画面下側に存在する場合
  else if (y_pos > y_pos_init) {
    // プレイヤーを左方向へ移動
    y_pos -= kPlayerMoveValueY;
    // プレイヤーが移動しすぎた場合は調整する
    if (y_pos <= y_pos_init) {
      y_pos = y_pos_init;
    }
    // プレイヤーのY座標位置をセット
    player_->SetPositionY(y_pos);
  }

  // 初期位置に戻ったら処理終了
  if (x_pos == x_pos_init && y_pos == y_pos_init) {
    return true;
  }

  return false;
}

/// <summary>
/// プレイヤーを画面外に移動させていく
/// </summary>
/// <param name="process_time"></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::MovePlayer(float process_time) {

  // プレイヤーのY座標を取得
  int y_pos = player_->GetPositionY();

  switch (move_phase_) {
  case PlayerMovePhase::kBackward1:
  case PlayerMovePhase::kBackward2: {
    //------------------------------
    // 徐々に後退する演出
    //------------------------------
    // 累積時間を加算する
    accumulate_time_ += process_time;

    switch (move_phase_) {
    case PlayerMovePhase::kBackward1: {

      if (accumulate_time_ < kBackwardWaitTime) {
        break;
      }
      // 累積時間をリセット
      accumulate_time_ = kResetTime;

      // プレイヤーの座標に移動量加算
      y_pos += kPlayerBackward1;
      // 移動回数をカウント
      ++move_count_;

      // プレイヤー座標にセット
      player_->SetPositionY(y_pos);

      // プレイヤー移動フェーズを「後退処理その2」に変更
      ChangePlayerMovePhase(PlayerMovePhase::kBackward2);
      break;
    }
    case PlayerMovePhase::kBackward2: {

      if (move_count_ >= kBackwardMaxCount) {
        // 累積時間をリセット
        accumulate_time_ = kResetTime;
        // プレイヤー移動フェーズを「前進処理」に変更
        ChangePlayerMovePhase(PlayerMovePhase::kForward);
        break;
      }

      // 待機時間に満たない場合は処理終了
      if (accumulate_time_ < kBackwardWaitTime) {
        break;
      }
      // 累積時間をリセット
      accumulate_time_ = kResetTime;

      // プレイヤーの座標に移動量加算
      y_pos -= kPlayerBackward2;
      // プレイヤー座標にセット
      player_->SetPositionY(y_pos);

      // プレイヤー移動フェーズを「後退処理その1」に変更
      ChangePlayerMovePhase(PlayerMovePhase::kBackward1);
      break;
    }
    }

    break;
  }
  case PlayerMovePhase::kForward: {
    //------------------------------
    // プレイヤー前進！
    //------------------------------
    // ゲーム情報から画面上部の座標を取得
    GameInfo* game_info = GameInfo::GetGameInfoInstance();
    if (game_info == nullptr) {
      return false;
    }
    int y_pos_top = game_info->GetMapPositionY();
    // プレイヤーの高さ半分を取得
    int image_height_half = player_->GetImageHalfHeight();

    // プレイヤーが画面外に到達していたら処理終了
    if (y_pos <= y_pos_top - image_height_half) {
      player_->ChangeCurrentPhase(Player::PhaseType::kBeforeFinalize);
      return true;
    }

    // プレイヤーの座標に移動量加算
    y_pos -= kPlayerForward;
    // プレイヤー座標にセット
    player_->SetPositionY(y_pos);

    break;
  }
  }

  return false;
}

/// <summary>
/// ゲームオーバー処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void BattleLevel::ExecuteGameOver(float process_time) {

  switch (game_over_phase_) {
  case GameOverPhase::kStopEnemy: {
    //----------------------------
    //敵の行動を停止する
    //----------------------------
    Character* character = nullptr;
    switch (battle_phase_) {
    case BattlePhase::kEnemyA: {
      character = enemy_a_->GetCharacter();
      break;
    }
    case BattlePhase::kEnemyB: {
      character = enemy_b_->GetCharacter();
      break;
    }
    }
    // 敵の弾幕を止める
    barrage_manager_->FinishGenerateBarrage();
    // 描画中の対象キャラクターの弾を全て終了状態にする
    bullet_manager_->FinishBullet(character);
    // 累積時間をリセット
    accumulate_time_ = kResetTime;

    // ゲームオーバーフェーズを「タスクを積む」に変更
    ChangeGameOverPhase(GameOverPhase::kTaskAdd);
    break;
  }
  case GameOverPhase::kTaskAdd: {
    //----------------------------
    //タスクを積む
    //----------------------------
    // 累積時間を加算する
    accumulate_time_ += process_time;
    if (accumulate_time_ < kGameOverWaitTime) {
      return;
    }
    // 累積時間をリセット
    accumulate_time_ = kResetTime;

    // プレイヤーコントローラをタスクマネージャから降ろす
    TaskId task_id = player_controller_->GetTaskId();
    task_manager_.ReleaseTask(task_id);

    // 終了UIをタスクマネージャに積む
    task_manager_.AddTask(game_over_);
    // メニューコントローラをタスクマネージャに積む
    task_manager_.AddTask(menu_controller_);
    menu_controller_->InitializePushKeyFlag();

    // ゲームオーバーフェーズを「待機」に変更
    ChangeGameOverPhase(GameOverPhase::kWait);
    break;
  }
  case GameOverPhase::kWait: {
    //----------------------------
    //待機
    //----------------------------
    break;
  }
  }
}

/// <summary>
/// SE生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPlayGameOverSound() {

  // サウンド生成
  sound_manager_->CreateSound(SoundId::kGameOver);
}

/// <summary>
/// リトライする
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnTryAgain() {

  // 遷移するレベルにバトルレベルをセットする
  transition_level_ = TaskId::kBattleLevel;
  // 現在のフェーズを「レベル遷移前」に変更
  ChangeCurrentPhase(PhaseType::kBeforeTransition);
}

/// <summary>
/// タイトルへ遷移する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnGoToTitle() {

  // 遷移するレベルにタイトルレベルをセットする
  transition_level_ = TaskId::kTitleLevel;
  // 現在のフェーズを「レベル遷移前」に変更
  ChangeCurrentPhase(PhaseType::kBeforeTransition);
}

/// <summary>
/// Enterキーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushMenuControllerEnterKey() {

  std::cout << "BattleLevel OnPushMenuControllerEnterKey" << std::endl;

  switch (current_phase_) {
  case PhaseType::kPauseWait: {
    // 決定操作
    DecidePauseMenu();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonPush);
    break;
  }
  case PhaseType::kSoundSetting: {
    // 現在選択しているボタンを取得
    SoundSetup::ButtonType button_type = sound_setup_->GetButtonType();
    switch (button_type) {
    case SoundSetup::ButtonType::kDone:
    case SoundSetup::ButtonType::kReturn: {
      // 決定操作
      DecideSoundMenu();
      // 決定サウンド
      sound_manager_->CreateSound(SoundId::kButtonPush);
      break;
    }
    }
    break;
  }
  case PhaseType::kGameOver: {
    // 決定操作
    game_over_->DecideSelectButton();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonPush);
    break;
  }
  case PhaseType::kDebugMenuWait: {
    // 決定操作
    DecideDebugMenu();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonPush);
    break;
  }
  }
}

/// <summary>
/// Zキーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushMenuControllerZ() {

  bool is_active = IsActive();
  if (!is_active) {
    return;
  }

  std::cout << "BattleLevel OnPushMenuControllerZ" << std::endl;

  switch (current_phase_) {
  case PhaseType::kPauseWait: {
    // 決定操作
    DecidePauseMenu();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonPush);
    break;
  }
  case PhaseType::kSoundSetting: {
    // 現在選択しているボタンを取得
    SoundSetup::ButtonType button_type = sound_setup_->GetButtonType();
    switch (button_type) {
    case SoundSetup::ButtonType::kDone:
    case SoundSetup::ButtonType::kReturn: {
      // 決定操作
      DecideSoundMenu();
      // 決定サウンド
      sound_manager_->CreateSound(SoundId::kButtonPush);
      break;
    }
    }
    break;
  }
  case PhaseType::kGameOver: {
    // 決定操作
    game_over_->DecideSelectButton();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonPush);
    break;
  }
  case PhaseType::kDebugMenuWait: {
    // 決定操作
    DecideDebugMenu();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonPush);
    break;
  }
  }

}

/// <summary>
/// Xキーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushMenuControllerX() {

  bool is_active = IsActive();
  if (!is_active) {
    return;
  }

  std::cout << "BattleLevel OnPushMenuControllerX" << std::endl;

  switch (current_phase_) {
  case PhaseType::kPauseWait:
  case PhaseType::kDebugMenuWait: {
    // メニュー操作を終了する
    FinishMenu();

    // 敵キャラのフェーズを元に戻す
    enemy_a_->ChangeCurrentPhase(EnemyA::PhaseType::kProcess);
    enemy_b_->ChangeCurrentPhase(EnemyB::PhaseType::kProcess);

    break;
  }
  case PhaseType::kSoundSetting: {

    // BGMの停止処理
    OnDisposeSoundBGM();

    // サウンド設定、ボリュームコントローラをタスクマネージャから降ろす
    TaskId task_id = sound_setup_->GetTaskId();
    task_manager_.ReleaseTask(task_id);
    task_id = volume_controller_->GetTaskId();
    task_manager_.ReleaseTask(task_id);

    // ポーズメニューをタスクマネージャに積む
    task_manager_.AddTask(pause_ui_);

    // 現在のフェーズを「ポーズメニュー待機」に変更
    ChangeCurrentPhase(PhaseType::kPauseWait);
    break;
  }
  }
}

/// <summary>
/// Qキーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushMenuControllerQ() {

  std::cout << "メニューコントローラ Qキー押下" << std::endl;

  switch (current_phase_) {
  case PhaseType::kDebugMenuWait: {
    // メニュー操作を終了する
    FinishMenu();

    // 敵キャラのフェーズを元に戻す
    enemy_a_->ChangeCurrentPhase(EnemyA::PhaseType::kProcess);
    enemy_b_->ChangeCurrentPhase(EnemyB::PhaseType::kProcess);

    break;
  }
  }
}

/// <summary>
/// Escキーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushMenuControllerEscapeKey() {

  std::cout << "メニューコントローラ Escキー押下" << std::endl;

  switch (current_phase_) {
  case PhaseType::kPauseWait: {
    // メニュー操作を終了する
    FinishMenu();

    // 敵キャラのフェーズを元に戻す
    enemy_a_->ChangeCurrentPhase(EnemyA::PhaseType::kProcess);
    enemy_b_->ChangeCurrentPhase(EnemyB::PhaseType::kProcess);

    break;
  }
  case PhaseType::kSoundSetting: {

    // BGMの停止処理
    OnDisposeSoundBGM();

    // サウンド設定、ボリュームコントローラをタスクマネージャから降ろす
    TaskId task_id = sound_setup_->GetTaskId();
    task_manager_.ReleaseTask(task_id);
    task_id = volume_controller_->GetTaskId();
    task_manager_.ReleaseTask(task_id);

    // ポーズメニューをタスクマネージャに積む
    task_manager_.AddTask(pause_ui_);

    // 現在のフェーズを「ポーズメニュー待機」に変更
    ChangeCurrentPhase(PhaseType::kPauseWait);
    break;
  }
  }
}

/// <summary>
/// 右矢印キーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushMenuControllerRight() {

  switch (current_phase_) {
  case PhaseType::kGameOver: {
    // 選択操作
    game_over_->ChangeSelectButton();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonSelect);
  break;
  }
  case PhaseType::kSoundSetting: {
    // 現在選択しているボタンを取得
    SoundSetup::ButtonType button_type = sound_setup_->GetButtonType();
    switch (button_type) {
    case SoundSetup::ButtonType::kDone:
    case SoundSetup::ButtonType::kReturn: {
      // 選択操作
      sound_setup_->OperationCursorRight();
      // 決定サウンド
      sound_manager_->CreateSound(SoundId::kButtonSelect);
      break;
    }
    }
    break;
  }
  case PhaseType::kDebugMenuWait: {
    // 選択操作
    debug_menu_->OperationCursorRight();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonSelect);
    break;
  }
  }
}

/// <summary>
/// 左矢印キーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushMenuControllerLeft() {

  switch (current_phase_) {
  case PhaseType::kGameOver: {
    // 選択操作
    game_over_->ChangeSelectButton();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonSelect);
    break;
  }
  case PhaseType::kSoundSetting: {
    // 現在選択しているボタンを取得
    SoundSetup::ButtonType button_type = sound_setup_->GetButtonType();
    switch (button_type) {
    case SoundSetup::ButtonType::kDone:
    case SoundSetup::ButtonType::kReturn: {
      // 選択操作
      sound_setup_->OperationCursorLeft();
      // 決定サウンド
      sound_manager_->CreateSound(SoundId::kButtonSelect);
      break;
    }
    }
    break;
  }
  case PhaseType::kDebugMenuWait: {
    // 選択操作
    debug_menu_->OperationCursorLeft();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonSelect);
    break;
  }
  }
}

/// <summary>
/// 上矢印キーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushMenuControllerUp() {

  switch (current_phase_) {
  case PhaseType::kPauseWait: {
    // 選択操作
    pause_ui_->OperationCursorUp();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonSelect);
    break;
  }
  case PhaseType::kSoundSetting: {
    // 選択操作
    sound_setup_->OperationCursorUp();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonSelect);
    break;
  }
  case PhaseType::kDebugMenuWait: {
    // 選択操作
    debug_menu_->OperationCursorUp();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonSelect);
    break;
  }
  }
}

/// <summary>
/// 下矢印キーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushMenuControllerDown() {

  switch (current_phase_) {
  case PhaseType::kPauseWait: {
    // 選択操作
    pause_ui_->OperationCursorDown();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonSelect);
    break;
  }
  case PhaseType::kSoundSetting: {
    // 選択操作
    sound_setup_->OperationCursorDown();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonSelect);
    break;
  }
  case PhaseType::kDebugMenuWait: {
    // 選択操作
    debug_menu_->OperationCursorDown();
    // 決定サウンド
    sound_manager_->CreateSound(SoundId::kButtonSelect);
    break;
  }
  }
}

/// <summary>
/// 右矢印キーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushVolumeControllerRight() {

  switch (current_phase_) {
  case PhaseType::kSoundSetting: {
    // 現在選択しているボタンを取得
    SoundSetup::ButtonType button_type = sound_setup_->GetButtonType();
    switch (button_type) {
    case SoundSetup::ButtonType::kBackGroundMusic: {
      // 選択操作
      sound_setup_->OperationCursorRight();
      // 指定サウンドを取得
      int list_id = sound_setup_->GetBgmListId();
      Sound* sound = sound_manager_->GetSoundBgm(list_id);
      if (sound == nullptr) {
        break;
      }
      // ボリュームを変更
      int volume = static_cast<int>(sound_setup_->GetSoundVolumeBgm());
      sound->SetSoundVolume(volume);
      sound->ChangeSoundVolume();
      break;
    }
    case SoundSetup::ButtonType::kSoundEffect: {
      // 選択操作
      sound_setup_->OperationCursorRight();
      // 選択サウンド
      Sound* sound = sound_manager_->CreateSound(SoundId::kShoot);
      if (sound == nullptr) {
        break;
      }
      // ボリュームを変更
      int volume = static_cast<int>(sound_setup_->GetSoundVolumeSe());
      sound->SetSoundVolume(volume);
      sound->ChangeSoundVolume();
      break;
    }
    }
    break;
  }
  }
}

/// <summary>
/// 左矢印キーが押された時のイベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPushVolumeControllerLeft() {

  switch (current_phase_) {
  case PhaseType::kSoundSetting: {
    // 現在選択しているボタンを取得
    SoundSetup::ButtonType button_type = sound_setup_->GetButtonType();
    switch (button_type) {
    case SoundSetup::ButtonType::kBackGroundMusic: {
      // 選択操作
      sound_setup_->OperationCursorLeft();
      // 指定サウンドを取得
      int list_id = sound_setup_->GetBgmListId();
      Sound* sound = sound_manager_->GetSoundBgm(list_id);
      if (sound == nullptr) {
        break;
      }
      // ボリュームを変更
      int volume = static_cast<int>(sound_setup_->GetSoundVolumeBgm());
      sound->SetSoundVolume(volume);
      sound->ChangeSoundVolume();
      break;
    }
    case SoundSetup::ButtonType::kSoundEffect: {
      // 選択操作
      sound_setup_->OperationCursorLeft();
      // 選択サウンド
      Sound* sound = sound_manager_->CreateSound(SoundId::kShoot);
      if (sound == nullptr) {
        break;
      }
      // ボリュームを変更
      int volume = static_cast<int>(sound_setup_->GetSoundVolumeSe());
      sound->SetSoundVolume(volume);
      sound->ChangeSoundVolume();
      break;
    }
    }
    break;
  }
  }
}

/// <summary>
/// BGMの再生
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnPlaySoundBGM() {

  Sound* sound = sound_manager_->CreateSound(SoundId::kBattleBgmTest);
  if (sound == nullptr) {
    return;
  }
  int list_id = sound->GetSoundListId();
  sound_setup_->SetBgmListId(list_id);
  // ボリュームを変更
  int volume = static_cast<int>(sound_setup_->GetSoundVolumeBgm());
  sound->SetSoundVolume(volume);
  sound->ChangeSoundVolume();
}

/// <summary>
/// BGMの破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::OnDisposeSoundBGM() {

  int list_id = sound_setup_->GetBgmListId();
  Sound* sound = sound_manager_->GetSoundBgm(list_id);
  if (sound == nullptr) {
    return;
  }
  sound->ChangeCurrentPhase(Sound::PhaseType::kFinish);
}

/// <summary>
/// ポーズメニュー決定操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::DecidePauseMenu() {

  // ボタンの種類を取得
  PauseUi::ButtonType button_type = pause_ui_->GetButtonType();
  switch (button_type) {
  case PauseUi::ButtonType::kResume: {
    //------------------------------
    //ゲーム再開
    //------------------------------
    // デバッグ操作終了処理
    FinishMenu();

    // 敵キャラのフェーズを元に戻す
    enemy_a_->ChangeCurrentPhase(EnemyA::PhaseType::kProcess);
    enemy_b_->ChangeCurrentPhase(EnemyB::PhaseType::kProcess);

    break;
  }
  case PauseUi::ButtonType::kSetup: {
    //------------------------------
    //設定
    //------------------------------
    // 現在のフェーズを「サウンド設定」に変更
    ChangeCurrentPhase(PhaseType::kSoundSetting);

    // ポーズメニューをタスクマネージャから降ろす
    TaskId task_id = pause_ui_->GetTaskId();
    task_manager_.ReleaseTask(task_id);
     
    // サウンド設定、ボリュームコントローラをタスクマネージャに積む
    task_manager_.AddTask(sound_setup_);
    task_manager_.AddTask(volume_controller_);

    sound_setup_->ChangeCurrentPhase(SoundSetup::PhaseType::kInitialize);
    // サウンドボリュームを渡す
    sound_setup_->SetSoundVolumeBgm(sound_volume_list_[Sound::SoundType::kBackGroundMusic]);
    sound_setup_->SetSoundVolumeSe(sound_volume_list_[Sound::SoundType::kSoundEffect]);

    GameInfo* game_info = GameInfo::GetGameInfoInstance();
    if (game_info == nullptr) {
      return;
    }
    int x_pos_map = game_info->GetMapPositionX();
    int y_pos_map = game_info->GetMapPositionY();
    int map_width = game_info->GetMapWidth();
    int map_height = game_info->GetMapHeight();

    // ゲーム画面を暗くするための背景のサイズをセット
    sound_setup_->SetBackPosAndSize(map_width, map_height, x_pos_map, y_pos_map);

    break;
  }
  case PauseUi::ButtonType::kRetry: {
    //------------------------------
    //リトライ
    //------------------------------
    transition_level_ = TaskId::kBattleLevel;
    // 現在のフェーズを「終了処理前」に変更
    ChangeCurrentPhase(PhaseType::kBeforeTransition);

    // ポーズメニューをタスクマネージャから降ろす
    Task* task = task_manager_.ReleaseTask(TaskId::kPauseUi);
    break;
  }
  case PauseUi::ButtonType::kGoToTitle: {
    //------------------------------
    //タイトルへ戻る
    //------------------------------
    transition_level_ = TaskId::kTitleLevel;
    // 現在のフェーズを「終了処理前」に変更
    ChangeCurrentPhase(PhaseType::kBeforeTransition);

    // ポーズメニューをタスクマネージャから降ろす
    Task* task = task_manager_.ReleaseTask(TaskId::kPauseUi);
    break;
  }
  }
}

/// <summary>
/// サウンドメニュー決定操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::DecideSoundMenu() {

  std::cout << "BattleLevel DecideSoundMenu" << std::endl;

  // 現在選択しているボタンを取得
  SoundSetup::ButtonType button_type = sound_setup_->GetButtonType();
  switch (button_type) {
  case SoundSetup::ButtonType::kDone: {
    // サウンドボリュームを取得
    float volume_bgm = sound_setup_->GetSoundVolumeBgm();
    float volume_se = sound_setup_->GetSoundVolumeSe();

    sound_volume_list_[Sound::SoundType::kBackGroundMusic] = volume_bgm;
    sound_volume_list_[Sound::SoundType::kSoundEffect] = volume_se;

    // サウンド全てに設定を反映
    sound_manager_->ChangeSoundVolumeBgm(static_cast<int>(volume_bgm));
    sound_manager_->ChangeSoundVolumeSe(static_cast<int>(volume_se));

    // ファイルに書き込む
    std::ofstream stream(kSoundDataFilePath, std::ios::out);
    std::string text = "BGMSoundVolume=" + std::to_string(volume_bgm);
    stream << text << std::endl;
    text = "SESoundVolume=" + std::to_string(volume_se);
    stream << text << std::endl;
    //ファイルを閉じる
    stream.close();
    break;
  }
  }

  // BGMの停止処理
  OnDisposeSoundBGM();

  // サウンド設定、ボリュームコントローラをタスクマネージャから降ろす
  TaskId task_id = sound_setup_->GetTaskId();
  task_manager_.ReleaseTask(task_id);
  task_id = volume_controller_->GetTaskId();
  task_manager_.ReleaseTask(task_id);

  // ポーズメニューをタスクマネージャに積む
  task_manager_.AddTask(pause_ui_);

  // 現在のフェーズを「ポーズメニュー待機」に変更
  ChangeCurrentPhase(PhaseType::kPauseWait);
}

/// <summary>
/// 決定操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BattleLevel::DecideDebugMenu() {

  // ボタンの種類を取得
  DebugMenu::ButtonType button_type = debug_menu_->GetButtonType();
  switch (button_type) {
  case DebugMenu::ButtonType::kEnemyAPhase01:
  case DebugMenu::ButtonType::kEnemyAPhase02:
  case DebugMenu::ButtonType::kEnemyAPhase03:
  case DebugMenu::ButtonType::kEnemyAPhase04:
  case DebugMenu::ButtonType::kEnemyAPhase05:
  case DebugMenu::ButtonType::kEnemyAPhase06: {

    if (battle_phase_ == BattlePhase::kEnemyB) {
      // 表示されている弾幕と弾を削除
      barrage_manager_->FinishGenerateBarrage();
      bullet_manager_->FinishBullet(enemy_b_);
      enemy_b_->ChangeBarragePhase(EnemyB::BarragePhase::kNone);
      // HPバーを削除
      int list_id = enemy_b_->GetHpBarListId();
      hp_bar_manager_->FinishHitPointBar(list_id);

      // 敵キャラBをタスクマネージャから降ろす
      Task* task = task_manager_.ReleaseTask(TaskId::kEnemyB);
      // 敵キャラAをタスクマネージャに積む
      task_manager_.AddTask(enemy_a_);
      // 敵キャラAの現在のフェーズを「初期化処理」に変更
      enemy_a_->ChangeCurrentPhase(EnemyA::PhaseType::kInitialize);
      enemy_a_->ChangeBarragePhase(EnemyA::BarragePhase::kNone);
      enemy_a_->ChangeStatusPhase(EnemyA::StatusPhase::kAlive);
      // バトルフェーズを変更
      ChangeBattlePhase(BattlePhase::kEnemyA);
    }
    else if (battle_phase_ == BattlePhase::kEnemyA) {
      // 表示されている弾幕と弾を削除
      barrage_manager_->FinishGenerateBarrage();
      bullet_manager_->FinishBullet(enemy_a_);
      enemy_a_->ChangeBarragePhase(EnemyA::BarragePhase::kNone);
      // HPバーを削除
      int list_id = enemy_a_->GetHpBarListId();
      hp_bar_manager_->FinishHitPointBar(list_id);
    }
    else {
      return;
    }

    // カットイン処理実行
    switch (button_type) {
    case DebugMenu::ButtonType::kEnemyAPhase01: {
      enemy_a_->ChangeActionPhase(EnemyA::ActionPhase::kFirstPhaseCutIn);
      break;
    }
    case DebugMenu::ButtonType::kEnemyAPhase02: {
      enemy_a_->ChangeActionPhase(EnemyA::ActionPhase::kSecondPhaseCutIn);
      break;
    }
    case DebugMenu::ButtonType::kEnemyAPhase03: {
      enemy_a_->ChangeActionPhase(EnemyA::ActionPhase::kThirdPhaseCutIn);
      break;
    }
    case DebugMenu::ButtonType::kEnemyAPhase04: {
      enemy_a_->ChangeActionPhase(EnemyA::ActionPhase::kFourthPhaseCutIn);
      break;
    }
    case DebugMenu::ButtonType::kEnemyAPhase05: {
      enemy_a_->ChangeActionPhase(EnemyA::ActionPhase::kFifthPhaseCutIn);
      break;
    }
    case DebugMenu::ButtonType::kEnemyAPhase06: {
      enemy_a_->ChangeActionPhase(EnemyA::ActionPhase::kSixthPhaseCutIn);
      break;
    }
    }

    if (!enemy_a_->IsInitialize()) {
      enemy_a_->ChangeCurrentPhase(EnemyA::PhaseType::kProcess);
    }
    enemy_a_->ChangeCutInPhase(EnemyA::CutInPhase::kCreate);

    break;
  }
  case DebugMenu::ButtonType::kEnemyBPhase01:
  case DebugMenu::ButtonType::kEnemyBPhase02:
  case DebugMenu::ButtonType::kEnemyBPhase03:
  case DebugMenu::ButtonType::kEnemyBPhase04:
  case DebugMenu::ButtonType::kEnemyBPhase05:
  case DebugMenu::ButtonType::kEnemyBPhase06: {

    if (battle_phase_ == BattlePhase::kEnemyA) {
      // 表示されている弾幕と弾を削除
      barrage_manager_->FinishGenerateBarrage();
      bullet_manager_->FinishBullet(enemy_a_);
      enemy_a_->ChangeBarragePhase(EnemyA::BarragePhase::kNone);
      // HPバーを削除
      int list_id = enemy_a_->GetHpBarListId();
      hp_bar_manager_->FinishHitPointBar(list_id);

      // 敵キャラAをタスクマネージャから降ろす
      Task* task = task_manager_.ReleaseTask(TaskId::kEnemyA);
      // 敵キャラBをタスクマネージャに積む
      task_manager_.AddTask(enemy_b_);
      // 敵キャラBの現在のフェーズを「初期化処理」に変更
      enemy_b_->ChangeCurrentPhase(EnemyB::PhaseType::kInitialize);
      enemy_b_->ChangeBarragePhase(EnemyB::BarragePhase::kNone);
      enemy_b_->ChangeStatusPhase(EnemyB::StatusPhase::kAlive);
      // バトルフェーズを変更
      ChangeBattlePhase(BattlePhase::kEnemyB);
    }
    else if (battle_phase_ == BattlePhase::kEnemyB) {
      // 表示されている弾幕と弾を削除
      barrage_manager_->FinishGenerateBarrage();
      bullet_manager_->FinishBullet(enemy_b_);
      enemy_b_->ChangeBarragePhase(EnemyB::BarragePhase::kNone);
      // HPバーを削除
      int list_id = enemy_b_->GetHpBarListId();
      hp_bar_manager_->FinishHitPointBar(list_id);
    }
    else {
      return;
    }

    switch (button_type) {
    case DebugMenu::ButtonType::kEnemyBPhase01: {
      enemy_b_->ChangeActionPhase(EnemyB::ActionPhase::kFirstPhaseCutIn);
      break;
    }
    case DebugMenu::ButtonType::kEnemyBPhase02: {
      enemy_b_->ChangeActionPhase(EnemyB::ActionPhase::kSecondPhaseCutIn);
      break;
    }
    case DebugMenu::ButtonType::kEnemyBPhase03: {
      enemy_b_->ChangeActionPhase(EnemyB::ActionPhase::kThirdPhaseCutIn);
      break;
    }
    case DebugMenu::ButtonType::kEnemyBPhase04: {
      enemy_b_->ChangeActionPhase(EnemyB::ActionPhase::kFourthPhaseCutIn);
      break;
    }
    case DebugMenu::ButtonType::kEnemyBPhase05: {
      enemy_b_->ChangeActionPhase(EnemyB::ActionPhase::kFifthPhaseCutIn);
      break;
    }
    case DebugMenu::ButtonType::kEnemyBPhase06: {
      enemy_b_->ChangeActionPhase(EnemyB::ActionPhase::kSixthPhaseCutIn);
      break;
    }
    }

    if (!enemy_b_->IsInitialize()) {
      enemy_b_->ChangeCurrentPhase(EnemyB::PhaseType::kProcess);
    }
    enemy_b_->ChangeCutInPhase(EnemyB::CutInPhase::kCreate);

    break;
  }
  }

  // デバッグ操作終了処理
  FinishMenu();
}

/// <summary>
/// バトルレベルのフェーズ確認
/// </summary>
/// <param name=""></param>
/// <returns> true:プレイ中, false:それ以外 </returns>
bool BattleLevel::OnCheckBattleLevelPhase() {

  if (current_phase_ == PhaseType::kPlay) {
    return true;
  }

  return false;
}

/// <summary>
/// プレイヤーの死亡しているかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:死亡, false:生存 </returns>
bool BattleLevel::OnCheckPlayerDead() {

  bool is_dead = player_->IsStop();

  return is_dead;
}

/// <summary>
/// ラップタイムセット
/// </summary>
/// <param name="character"> キャラクター </param>
/// <param name="phase"> フェーズ数 </param>
/// <returns></returns>
void BattleLevel::OnSetRapTime(Character* character, int phase) {

  GameTimer::EnemyPhase enemy_phase = GameTimer::EnemyPhase::kPhaseMaxIndex;
  int phase_index = phase - kOne;

  // キャラクターの種類
  Character::CharacterType chara_type = character->GetCharacterType();
  switch (chara_type) {
  case Character::CharacterType::kEnemyA: {
    enemy_phase = GameTimer::EnemyPhase::kEnemyA;
    break;
  }
  case Character::CharacterType::kEnemyB: {
    enemy_phase = GameTimer::EnemyPhase::kEnemyB;
    break;
  }
  }

  // ラップタイムセット
  game_timer_->SetRapTime(enemy_phase, phase_index);
}

/// <summary>
/// 設定ファイル読み込み
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BattleLevel::LoadSetupFile() {

  std::vector<std::string> file_path;
  LoadFilePhase next_phase = LoadFilePhase::kFinish;
  int start_index = 0;

  switch (load_file_phase_) {
  case LoadFilePhase::kGameData: {
    file_path = { kGameDataFilePath };
    next_phase = LoadFilePhase::kSoundData;
    break;
  }
  case LoadFilePhase::kSoundData: {
    file_path = { kSoundDataFilePath };
    next_phase = LoadFilePhase::kBullet;
    break;
  }
  case LoadFilePhase::kBullet: {
    file_path = { kNormalBulletFilePath, kBlueBallBulletFilePath,
                  kPurpleBallBulletFilePath, kRedBallBulletFilePath,
                  kYellowBallBulletFilePath, kGreenBallBulletFilePath };
    next_phase = LoadFilePhase::kBarrage;
    break;
  }
  case LoadFilePhase::kBarrage: {
    file_path = { kSingleShootFilePath, kDoubleShootFilePath,
                  kTripleShootFilePath, kQuadShootFilePath,
                  kAllDirectionFilePath, kRotationFilePath,
                  kContinuousFilePath};
    next_phase = LoadFilePhase::kEffect;
    break;
  }
  case LoadFilePhase::kEffect: {
    file_path = { kPlayerDeathFilePath, kExplosionFilePath,
                  kRebornFilePath, kDisappearNormalFilePath,
                  kDisappearBlueBallFilePath, kDisappearPurpleBallFilePath,
                  kDisappearRedBallFilePath, kDisappearYellowBallFilePath,
                  kDisappearGreenBallFilePath, kEnemyExplosionFilePath,
                  kEnemyRuptureFilePath, kItemSummonFilePath };
    next_phase = LoadFilePhase::kPlayer;
    break;
  }
  case LoadFilePhase::kPlayer: {
    file_path = { kPlayerFilePath };
    next_phase = LoadFilePhase::kEnemyA;
    break;
  }
  case LoadFilePhase::kEnemyA: {
    file_path = { kEnemyAFilePath, kEnemyAHitPointFilePath };
    next_phase = LoadFilePhase::kEnemyB;
    break;
  }
  case LoadFilePhase::kEnemyB: {
    file_path = { kEnemyBFilePath, kEnemyBHitPointFilePath };
    next_phase = LoadFilePhase::kItemA;
    break;
  }
  case LoadFilePhase::kItemA: {
    file_path = { kItemAFilePath };
    next_phase = LoadFilePhase::kItemB;
    break;
  }
  case LoadFilePhase::kItemB: {
    file_path = { kItemBFilePath };
    next_phase = LoadFilePhase::kEnemyAFirstPhase;
    break;
  }
  case LoadFilePhase::kEnemyAFirstPhase: {
    file_path = { kEnemyAFirstPhase01FilePath, kEnemyAFirstPhase02FilePath };
    next_phase = LoadFilePhase::kEnemyASecondPhase;
    break;
  }
  case LoadFilePhase::kEnemyASecondPhase: {
    file_path = { kEnemyASecondPhase01FilePath, kEnemyASecondPhase02FilePath };
    next_phase = LoadFilePhase::kEnemyAThirdPhase;
    break;
  }
  case LoadFilePhase::kEnemyAThirdPhase: {
    file_path = { kEnemyAThirdPhase01FilePath, kEnemyAThirdPhase02FilePath };
    next_phase = LoadFilePhase::kEnemyAFourthPhase;
    break;
  }
  case LoadFilePhase::kEnemyAFourthPhase: {
    file_path = { kEnemyAFourthPhase01FilePath, kEnemyAFourthPhase02FilePath };
    next_phase = LoadFilePhase::kEnemyAFifthPhase;
    break;
  }
  case LoadFilePhase::kEnemyAFifthPhase: {
    file_path = { kEnemyAFifthPhase01FilePath };
    next_phase = LoadFilePhase::kEnemyASixthPhase;
    break;
  }
  case LoadFilePhase::kEnemyASixthPhase: {
    file_path = { kEnemyASixthPhase01FilePath, kEnemyASixthPhase02FilePath };
    next_phase = LoadFilePhase::kEnemyBFirstPhase;
    break;
  }
  case LoadFilePhase::kEnemyBFirstPhase: {
    file_path = { kEnemyBFirstPhase01FilePath, kEnemyBFirstPhase02FilePath };
    next_phase = LoadFilePhase::kEnemyBSecondPhase;
    break;
  }
  case LoadFilePhase::kEnemyBSecondPhase: {
    file_path = { kEnemyBSecondPhase01FilePath, kEnemyBSecondPhase02FilePath };
    next_phase = LoadFilePhase::kEnemyBThirdPhase;
    break;
  }
  case LoadFilePhase::kEnemyBThirdPhase: {
    file_path = { kEnemyBThirdPhase01FilePath, kEnemyBThirdPhase02FilePath };
    next_phase = LoadFilePhase::kEnemyBFourthPhase;
    break;
  }
  case LoadFilePhase::kEnemyBFourthPhase: {
    file_path = { kEnemyBFourthPhase01FilePath, kEnemyBFourthPhase02FilePath,
                  kEnemyBFourthPhase03FilePath, kEnemyBFourthPhase04FilePath,
                  kEnemyBFourthPhase05FilePath };
    next_phase = LoadFilePhase::kEnemyBFifthPhase;
    break;
  }
  case LoadFilePhase::kEnemyBFifthPhase: {
    file_path = { kEnemyBFifthPhase01FilePath, kEnemyBFifthPhase02FilePath,
                  kEnemyBFifthPhase03FilePath, kEnemyBFifthPhase04FilePath };
    next_phase = LoadFilePhase::kEnemyBSixthPhase;
    break;
  }
  case LoadFilePhase::kEnemyBSixthPhase: {
    file_path = { kEnemyBSixthPhase01FilePath, kEnemyBSixthPhase02FilePath,
                  kEnemyBSixthPhase03FilePath, kEnemyBSixthPhase04FilePath,
                  kEnemyBSixthPhase05FilePath };
    next_phase = LoadFilePhase::kFinish;
    break;
  }
  }

  switch (load_file_phase_) {
  case LoadFilePhase::kGameData: {
    // ゲームデータ
    GameMode::GameData game_data;
    
    game_data = {};
    // ファイルをロードし、データを取得する
    bool is_finish = LoadFileGameData(file_path.at(0), game_data);
    if (!is_finish) {
      return false;
    }
    // データを渡す
    game_mode_->SetGameData(game_data);
    break;
  }
  case LoadFilePhase::kSoundData: {

    // ファイルをロードし、データを取得する
    bool is_finish = LoadFileSoundData(file_path.at(0));
    if (!is_finish) {
      return false;
    }
    break;
  }
  case LoadFilePhase::kBullet: {
    // バレットデータ
    Bullet::BulletData bullet_data;
    for (int index = 0; index < static_cast<int>(file_path.size()); ++index) {
      bullet_data = {};
      // ファイルをロードし、データを取得する
      bool is_finish = LoadFileBullet(file_path.at(index), bullet_data);
      if (!is_finish) {
        return false;
      }
      // リストに追加
      bullet_list_[static_cast<BulletId>(index)] = bullet_data;
    }
    break;
  }
  case LoadFilePhase::kEffect: {
    // エフェクトデータ
    Effect::EffectData effect_data;
    for (int index = 0; index < static_cast<int>(file_path.size()); ++index) {
      effect_data = {};
      // ファイルをロードし、データを取得する
      bool is_finish = LoadFileEffect(file_path.at(index), effect_data);
      if (!is_finish) {
        return false;
      }
      // リストに追加
      effect_list_[static_cast<EffectId>(index)] = effect_data;
    }
    break;
  }
  case LoadFilePhase::kPlayer:
  case LoadFilePhase::kItemA:
  case LoadFilePhase::kItemB: {
    // キャラクターデータ
    Character::CharaData chara_data;
    chara_data = {};
    // ファイルをロードし、データを取得する
    bool is_finish = LoadFileCharacter(file_path.at(kCharacterDataFile), chara_data);
    if (!is_finish) {
      return false;
    }

    switch (load_file_phase_) {
    case LoadFilePhase::kPlayer: {
      // プレイヤー情報を渡す
      player_->SetCharacterData(chara_data);
      break;
    }
    case LoadFilePhase::kItemA: {
      // 残機UPアイテム情報を渡す
      item_extend_->SetCharacterData(chara_data);
      break;
    }
    case LoadFilePhase::kItemB: {
      // 残機UPアイテム情報を渡す
      item_power_up_->SetCharacterData(chara_data);
      break;
    }
    }
    break;
  }
  case LoadFilePhase::kEnemyA:
  case LoadFilePhase::kEnemyB: {
    // キャラクターデータ
    Character::CharaData chara_data;
    EnemyBase::HitPoint phase_hit_point;
    chara_data = {};
    // ファイルをロードし、データを取得する
    bool is_finish = LoadFileCharacter(file_path.at(kCharacterDataFile), chara_data);
    if (!is_finish) {
      return false;
    }
    // ファイルをロードし、フェーズごとのHPを取得する
    is_finish = LoadFilePhaseHitPoint(file_path.at(kHitPointFile), phase_hit_point);
    if (!is_finish) {
      return false;
    }

    switch (load_file_phase_) {
    case LoadFilePhase::kEnemyA: {
      // 敵キャラAの情報を渡す
      enemy_a_->SetCharacterData(chara_data);
      enemy_a_->SetPhaseHitPoint(phase_hit_point);
      break;
    }
    case LoadFilePhase::kEnemyB: {
      // 敵キャラBの情報を渡す
      enemy_b_->SetCharacterData(chara_data);
      enemy_b_->SetPhaseHitPoint(phase_hit_point);
      break;
    }
    }
    break;
  }
  case LoadFilePhase::kBarrage:
  case LoadFilePhase::kEnemyAFirstPhase:
  case LoadFilePhase::kEnemyASecondPhase:
  case LoadFilePhase::kEnemyAThirdPhase:
  case LoadFilePhase::kEnemyAFourthPhase:
  case LoadFilePhase::kEnemyAFifthPhase:
  case LoadFilePhase::kEnemyASixthPhase:
  case LoadFilePhase::kEnemyBFirstPhase:
  case LoadFilePhase::kEnemyBSecondPhase:
  case LoadFilePhase::kEnemyBThirdPhase:
  case LoadFilePhase::kEnemyBFourthPhase:
  case LoadFilePhase::kEnemyBFifthPhase:
  case LoadFilePhase::kEnemyBSixthPhase: {

    // バレッジデータ
    Barrage::BarrageData barrage_data;
    for (int index = 0; index < static_cast<int>(file_path.size()); ++index) {
      barrage_data = {};
      // ファイルをロードし、データを取得する
      bool is_finish = LoadFileBarrage(file_path[index], barrage_data);
      if (!is_finish) {
        return false;
      }
      switch (load_file_phase_) {
      case LoadFilePhase::kBarrage: {
        // リストに追加
        barrage_list_[static_cast<BarrageId>(index)] = barrage_data;
        break;
      }
      case LoadFilePhase::kEnemyAFirstPhase:
      case LoadFilePhase::kEnemyASecondPhase:
      case LoadFilePhase::kEnemyAThirdPhase:
      case LoadFilePhase::kEnemyAFourthPhase:
      case LoadFilePhase::kEnemyAFifthPhase:
      case LoadFilePhase::kEnemyASixthPhase: {
        // 設定するリストの開始番号を取得
        start_index = static_cast<int>(enemy_a_phase_[load_file_phase_]);
        // リストに追加
        enemy_a_->SetActionBarrageType(static_cast<EnemyA::ActionBarrageType>(start_index + index), barrage_data);
        break;
      }
      case LoadFilePhase::kEnemyBFirstPhase:
      case LoadFilePhase::kEnemyBSecondPhase:
      case LoadFilePhase::kEnemyBThirdPhase:
      case LoadFilePhase::kEnemyBFourthPhase:
      case LoadFilePhase::kEnemyBFifthPhase:
      case LoadFilePhase::kEnemyBSixthPhase: {
        // 設定するリストの開始番号を取得
        start_index = static_cast<int>(enemy_b_phase_[load_file_phase_]);
        // リストに追加
        enemy_b_->SetActionBarrageType(static_cast<EnemyB::ActionBarrageType>(start_index + index), barrage_data);
        break;
      }
      }
    }
    break;
  }
  }

  // フェーズの切り替え
  ChangeLoadFilePhase(next_phase);
  if (next_phase == LoadFilePhase::kFinish) {
    return true;
  }

  return false;
}

/// <summary>
/// ロードファイル ゲームデータ
/// </summary>
/// <param name=""> ファイルパス </param>
/// <param name=""> ゲーム情報 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::LoadFileGameData(std::string file_path, GameMode::GameData& game_data) {

  //ファイルストリームを宣言と同時にファイルを開く
  std::ifstream stream(file_path, std::ios::in);
  //ファイルが開けないならエラー
  if (!stream) {
    std::cout << "BattleLevel LoadFileGameData ファイルが開けません" << std::endl;
    return false;
  }

  //ファイルから読み込む
  std::string line;
  //１行ずつ読み出す
  while (std::getline(stream, line)) {
    std::cout << line << std::endl;
    int equal_pos = 0;
    if (line.find("Life=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      game_data.left_life = std::stoi(str_value);
    }
    else if (line.find("ShootLevel=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      game_data.level_shoot = std::stoi(str_value);
    }
    else if (line.find("IsUseItem=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      game_data.is_use_item = std::stoi(str_value);
    }
  }

  //ファイルを閉じる
  stream.close();
  line.clear();
  stream.clear();

  return true;
}

/// <summary>
/// ロードファイル サウンドデータ
/// </summary>
/// <param name=""> ファイルパス </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::LoadFileSoundData(std::string file_path) {

  // ファイルストリームを宣言と同時にファイルを開く
  std::ifstream stream(file_path, std::ios::in);
  //ファイルが開けないならエラー
  if (!stream) {
    // リストに値をセット
    sound_volume_list_[Sound::SoundType::kBackGroundMusic] = kSoundInitialVolume;
    sound_volume_list_[Sound::SoundType::kSoundEffect] = kSoundInitialVolume;
    // ファイルストリームを宣言と同時にファイルを開く
    std::ofstream stream(file_path, std::ios::out);
    std::string text = "BGMSoundVolume=" + std::to_string(kSoundInitialVolume);
    stream << text << std::endl;
    text = "SESoundVolume=" + std::to_string(kSoundInitialVolume);
    stream << text << std::endl;
    //ファイルを閉じる
    stream.close();
    return true;
  }

  //ファイルから読み込む
  std::string line;
  //１行ずつ読み出す
  while (std::getline(stream, line)) {
    std::cout << line << std::endl;
    int equal_pos = 0;
    if (line.find("BGMSoundVolume=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      sound_volume_list_[Sound::SoundType::kBackGroundMusic] = std::stof(str_value);
    }
    else if (line.find("SESoundVolume=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      sound_volume_list_[Sound::SoundType::kSoundEffect] = std::stof(str_value);
    }
  }

  //ファイルを閉じる
  stream.close();
  line.clear();
  stream.clear();

  return true;
}

/// <summary>
/// ロードファイル 弾情報
/// </summary>
/// <param name=""> ファイルパス </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::LoadFileBullet(std::string file_path, Bullet::BulletData& bullet_data) {

  //ファイルストリームを宣言と同時にファイルを開く
  std::ifstream stream(file_path, std::ios::in);
  //ファイルが開けないならエラー
  if (!stream) {
    std::cout << "BattleLevel LoadFileBullet ファイルが開けません" << std::endl;
    return false;
  }

  //ファイルから読み込む
  std::string line;
  //１行ずつ読み出す
  while (std::getline(stream, line)) {
    std::cout << line << std::endl;
    int equal_pos = 0;
    if (line.find("SpeedX=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      bullet_data.speed.x = std::stof(str_value);
    }
    else if (line.find("SpeedY=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      bullet_data.speed.y = std::stof(str_value);
    }
    else if (line.find("SizeWidth=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      bullet_data.size.width = std::stoi(str_value);
    }
    else if (line.find("SizeHeight=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      bullet_data.size.height = std::stoi(str_value);
    }
    else if (line.find("Attack=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      bullet_data.spec.atttack = std::stoi(str_value);
    }
    else if (line.find("ExtRateValue=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      bullet_data.ext_rate.value = std::stof(str_value);
    }
    else if (line.find("ExtRateAdjust=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      bullet_data.ext_rate.adjust = std::stof(str_value);
    }
    else if (line.find("ExtRateMax=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      bullet_data.ext_rate.max = std::stof(str_value);
    }
    else if (line.find("ExtRateMin=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      bullet_data.ext_rate.min = std::stof(str_value);
    }
    else if (line.find("AngleValue=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      bullet_data.angle.value = std::stof(str_value);
    }
    else if (line.find("AngleAdjust=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      bullet_data.angle.adjust = std::stof(str_value);
    }
    else if (line.find("AngleMax=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      bullet_data.angle.max = std::stof(str_value);
    }
    else if (line.find("AngleMin=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      bullet_data.angle.min = std::stof(str_value);
    }
    else if (line.find("AlphaValue=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      bullet_data.alpha.value = std::stoi(str_value);
    }
    else if (line.find("AlphaAdjust=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      bullet_data.alpha.adjust = std::stoi(str_value);
    }
    else if (line.find("AlphaMax=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      bullet_data.alpha.max = std::stoi(str_value);
    }
    else if (line.find("AlphaMin=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      bullet_data.alpha.min = std::stoi(str_value);
    }
  }

  //ファイルを閉じる
  stream.close();
  line.clear();
  stream.clear();

  return true;
}

/// <summary>
/// ロードファイル 弾幕情報
/// </summary>
/// <param name=""> ファイルパス </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::LoadFileBarrage(std::string file_path, Barrage::BarrageData& barrage_data) {

  //ファイルストリームを宣言と同時にファイルを開く
  std::ifstream stream(file_path, std::ios::in);
  //ファイルが開けないならエラー
  if (!stream) {
    std::cout << "BattleLevel LoadFileBarrage ファイルが開けません" << std::endl;
    return false;
  }

  //ファイルから読み込む
  std::string line;
  //１行ずつ読み出す
  while (std::getline(stream, line)) {
    std::cout << line << std::endl;
    int equal_pos = 0;
    if (line.find("BarrageId=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.detail.barrage_id = static_cast<BarrageId>(std::stoi(str_value));
    }
    if (line.find("BulletId=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.detail.bullet_id = static_cast<BulletId>(std::stoi(str_value));
    }
    if (line.find("AimType=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.detail.aim_type = static_cast<Barrage::AimType>(std::stoi(str_value));
    }
    else if (line.find("DeployCount=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.count.deploy = std::stoi(str_value);
    }
    else if (line.find("GenerateCount=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.count.generate = std::stoi(str_value);
    }
    else if (line.find("DispPositionX=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.pos.x = std::stof(str_value);
    }
    else if (line.find("DispPositionY=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.pos.y = std::stof(str_value);
    }
    else if (line.find("ExecInterval=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.interval.exec = std::stof(str_value);
    }
    else if (line.find("WaitInterval=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.interval.wait = std::stof(str_value);
    }
    else if (line.find("CenterDistanceX=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.distance.x = std::stof(str_value);
    }
    else if (line.find("CenterDistanceY=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.distance.y = std::stof(str_value);
    }
    else if (line.find("GoalPositionX=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.pos.x_goal = std::stof(str_value);
    }
    else if (line.find("GoalPositionY=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.pos.y_goal = std::stof(str_value);
    }
    else if (line.find("MoveRate=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.rate.move = std::stof(str_value);
    }
    else if (line.find("ExpaValue=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.expa.value = std::stof(str_value);
    }
    else if (line.find("ExpaAdjust=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.expa.adjust = std::stof(str_value);
    }
    else if (line.find("ExpaMax=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.expa.max = std::stof(str_value);
    }
    else if (line.find("ExpaMin=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.expa.min = std::stof(str_value);
    }
    else if (line.find("AlphaValue=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.alpha.value = std::stoi(str_value);
    }
    else if (line.find("AlphaAdjust=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.alpha.adjust = std::stoi(str_value);
    }
    else if (line.find("AlphaMax=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.alpha.max = std::stoi(str_value);
    }
    else if (line.find("AlphaMin=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.alpha.min = std::stoi(str_value);
    }
    else if (line.find("Angle=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.angle.value = std::stof(str_value);
    }
    else if (line.find("AngleAdjust=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.angle.adjust = std::stof(str_value);
    }
    else if (line.find("ExecCount=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.count.execute = std::stoi(str_value);
    }
    else if (line.find("AcceleRateX=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.rate.acce_x = std::stof(str_value);
    }
    else if (line.find("AcceleRateY=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.rate.acce_y = std::stof(str_value);
    }
    else if (line.find("SpeedXMax=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.speed.x_max = std::stof(str_value);
    }
    else if (line.find("SpeedYMax=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.speed.y_max = std::stof(str_value);
    }
    else if (line.find("SpeedXMin=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.speed.x_min = std::stof(str_value);
    }
    else if (line.find("SpeedYMin=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.speed.y_min = std::stof(str_value);
    }
    else if (line.find("WaitStart=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.time.wait_start = std::stof(str_value);
    }
    else if (line.find("WaitAcceleration=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.time.wait_accel = std::stof(str_value);
    }
    else if (line.find("WaitExpantion=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      barrage_data.time.wait_expan = std::stof(str_value);
    }
  }

  //ファイルを閉じる
  stream.close();
  line.clear();
  stream.clear();

  return true;
}

/// <summary>
/// ロードファイル エフェクト情報
/// </summary>
/// <param name=""> ファイルパス </param>
/// <param name=""> 弾幕の情報 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::LoadFileEffect(std::string file_path, Effect::EffectData& effect_data) {

  //ファイルストリームを宣言と同時にファイルを開く
  std::ifstream stream(file_path, std::ios::in);
  //ファイルが開けないならエラー
  if (!stream) {
    std::cout << "BattleLevel LoadFileEffect ファイルが開けません" << std::endl;
    return false;
  }

  effect_data = {};

  //ファイルから読み込む
  std::string line;
  //１行ずつ読み出す
  while (std::getline(stream, line)) {
    std::cout << line << std::endl;
    int equal_pos = 0;
    if (line.find("PositionX=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.pos.x = std::stoi(str_value);
    }
    else if (line.find("PositionY=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.pos.y = std::stoi(str_value);
    }
    else if (line.find("EffectType=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.detail.type = static_cast<Effect::EffectType>(std::stoi(str_value));
    }
    else if (line.find("WaitTime=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.detail.wait_time = std::stof(str_value);
    }
    else if (line.find("DivTotalNum=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.div.total = std::stoi(str_value);
    }
    else if (line.find("DivX=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.div.x = std::stoi(str_value);
    }
    else if (line.find("DivY=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.div.y = std::stoi(str_value);
    }
    else if (line.find("SizeWidth=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.div.width = std::stoi(str_value);
    }
    else if (line.find("SizeHeight=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.div.height = std::stoi(str_value);
    }
    else if (line.find("AlphaValue=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.alpha.value = std::stoi(str_value);
    }
    else if (line.find("AlphaAdjust=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.alpha.adjust = std::stoi(str_value);
    }
    else if (line.find("AlphaMax=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.alpha.max = std::stoi(str_value);
    }
    else if (line.find("AlphaMin=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.alpha.min = std::stoi(str_value);
    }
    else if (line.find("ExtRateXValue=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.ex_rate_x.value = std::stof(str_value);
    }
    else if (line.find("ExtRateXAdjust=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.ex_rate_x.adjust = std::stof(str_value);
    }
    else if (line.find("ExtRateXMax=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.ex_rate_x.max = std::stof(str_value);
    }
    else if (line.find("ExtRateXMin=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.ex_rate_x.min = std::stof(str_value);
    }
    else if (line.find("ExtRateYValue=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.ex_rate_y.value = std::stof(str_value);
    }
    else if (line.find("ExtRateYAdjust=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.ex_rate_y.adjust = std::stof(str_value);
    }
    else if (line.find("ExtRateYMax=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.ex_rate_y.max = std::stof(str_value);
    }
    else if (line.find("ExtRateYMin=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.ex_rate_y.min = std::stof(str_value);
    }
    else if (line.find("AngleValue=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.angle.value = std::stof(str_value);
    }
    else if (line.find("AngleAdjust=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.angle.adjust = std::stof(str_value);
    }
    else if (line.find("AngleMax=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.angle.max = std::stof(str_value);
    }
    else if (line.find("AngleMin=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.angle.min = std::stof(str_value);
    }
  }

  // 行読み込みデータクリア
  line.clear();
  //ファイルを閉じる
  stream.close();
  stream.clear();

  return true;
}

/// <summary>
/// ロードファイル キャラクター情報
/// </summary>
/// <param name=""> ファイルパス </param>
/// <param name=""> キャラクターの情報 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::LoadFileCharacter(std::string file_path, Character::CharaData& chara_data) {

  //ファイルストリームを宣言と同時にファイルを開く
  std::ifstream stream(file_path, std::ios::in);
  //ファイルが開けないならエラー
  if (!stream) {
    std::cout << "BattleLevel LoadFileCharacter ファイルが開けません" << std::endl;
    return false;
  }

  //ファイルから読み込む
  std::string line;
  //１行ずつ読み出す
  while (std::getline(stream, line)) {
    std::cout << line << std::endl;
    int equal_pos = 0;
    if (line.find("Attack=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      chara_data.status.attack = std::stoi(str_value);
    }
    else if (line.find("Defense=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      chara_data.status.defense = std::stoi(str_value);
    }
    else if (line.find("HitPointInit=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      chara_data.status.hit_point_init = std::stoi(str_value);
    }
    else if (line.find("HitPoint=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      chara_data.status.hit_point = std::stoi(str_value);
    }
    else if (line.find("PositionX=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      chara_data.pos.x = std::stoi(str_value);
    }
    else if (line.find("PositionY=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      chara_data.pos.y = std::stoi(str_value);
    }
    else if (line.find("PositionInitX=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      chara_data.pos.x_init = std::stoi(str_value);
    }
    else if (line.find("PositionInitY=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      chara_data.pos.y_init = std::stoi(str_value);
    }
    else if (line.find("MoveX=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      chara_data.move.x = std::stoi(str_value);
    }
    else if (line.find("MoveY=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      chara_data.move.y = std::stoi(str_value);
    }
    else if (line.find("RateDisplay=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      chara_data.rate.display = std::stof(str_value);
    }
    else if (line.find("RateMove=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      chara_data.rate.move = std::stof(str_value);
    }
  }

  //ファイルを閉じる
  stream.close();
  line.clear();
  stream.clear();

  return true;
}

/// <summary>
/// ロードファイル ヒットポイント情報
/// </summary>
/// <param name=""> ファイルパス </param>
/// <param name=""> ヒットポイント </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BattleLevel::LoadFilePhaseHitPoint(std::string file_path, EnemyBase::HitPoint& hit_point) {

  //ファイルストリームを宣言と同時にファイルを開く
  std::ifstream stream(file_path, std::ios::in);
  //ファイルが開けないならエラー
  if (!stream) {
    std::cout << "BattleLevel LoadFilePhaseHitPoint ファイルが開けません" << std::endl;
    return false;
  }

  //ファイルから読み込む
  std::string line;
  //１行ずつ読み出す
  while (std::getline(stream, line)) {
    std::cout << line << std::endl;
    int equal_pos = 0;
    if (line.find("PhaseFirst=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      hit_point.phase_first = std::stoi(str_value);
    }
    else if (line.find("PhaseSecond=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      hit_point.phase_second = std::stoi(str_value);
    }
    else if (line.find("PhaseThird=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      hit_point.phase_third = std::stoi(str_value);
    }
    else if (line.find("PhaseFourth=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      hit_point.phase_fourth = std::stoi(str_value);
    }
    else if (line.find("PhaseFifth=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      hit_point.phase_fifth = std::stoi(str_value);
    }
    else if (line.find("PhaseSixth=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      hit_point.phase_sixth = std::stoi(str_value);
    }
    else if (line.find("PhaseSeventh=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      hit_point.phase_seventh = std::stoi(str_value);
    }
    else if (line.find("PhaseEighth=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      hit_point.phase_eighth = std::stoi(str_value);
    }
  }

  //ファイルを閉じる
  stream.close();
  line.clear();
  stream.clear();

  return true;
}