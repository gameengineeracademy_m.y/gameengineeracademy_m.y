﻿#pragma once

/// <summary>
/// バレッジID
/// </summary>
enum class BarrageId {
  kSingle,              // 単発射撃
  kDouble,              // 2倍射撃
  kTriple,              // 3倍射撃
  kQuadruple,           // 4倍射撃
  kAllDirection,        // 全方向
  kRotation,            // 回転
  kContinuous,          // 連射
  kTypeMaxIndex         // 種類数
};