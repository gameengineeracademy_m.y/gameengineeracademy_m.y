﻿#pragma once

#include "System/SoundId.h"
#include "System/Character.h"
#include "System/CutInManager.h"
#include "Game/EnemyBase.h"
#include "Game/EnemyController.h"
#include "Game/EnemyEventInterface.h"
#include <unordered_map>

/// <summary>
/// 敵キャラA
/// </summary>
class EnemyA : public EnemyBase {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,       // 初期化処理フェーズ
    kStartWait,        // 起動待機フェーズ
    kStartUp,          // 起動フェーズ
    kProcessWait,      // 処理待機フェーズ
    kProcess,          // 処理中フェーズ
    kStop,             // 停止中フェーズ
    kBeforeFinalize,   // 終了処理前フェーズ
    kFinalize,         // 終了処理フェーズ
    kFinalized,        // 終了処理済みフェーズ
    kPhaseMaxIndex     // フェーズ数
  };

  /// <summary>
  /// 状態フェーズの種類
  /// </summary>
  enum class StatusPhase {
    kAlive,            // 生存フェーズ
    kDefeat,           // 敗北フェーズ
    kFinish,           // 終了処理フェーズ
    kFinished,         // 終了処理済みフェーズ
    kPhaseMaxIndex     // フェーズ数
  };

  /// <summary>
  /// 行動フェーズの種類
  /// </summary>
  enum class ActionPhase {
    kStart,             // 開始フェーズ
    kFirstPhaseCutIn,   // 第１フェーズカットイン
    kFirstPhase,        // 第１フェーズ
    kFirstPhaseEffect,  // 第１フェーズ終了エフェクト
    kFirstPhaseFinish,  // 第１フェーズ終了
    kSecondStartWait,   // 第２フェーズ開始待機
    kSecondPhaseCutIn,  // 第２フェーズカットイン
    kSecondPhase,       // 第２フェーズ
    kSecondPhaseEffect, // 第２フェーズ終了エフェクト
    kSecondPhaseFinish, // 第２フェーズ終了
    kThirdStartWait,    // 第３フェーズ開始待機
    kThirdPhasePrepare, // 第３フェーズ準備
    kThirdPhaseCutIn,   // 第３フェーズカットイン
    kThirdPhase,        // 第３フェーズ
    kThirdPhaseEffect,  // 第３フェーズ終了エフェクト
    kThirdPhaseFinish,  // 第３フェーズ終了
    kFourthStartWait,   // 第４フェーズ開始待機
    kFourthPhasePrepare,// 第４フェーズ準備
    kFourthPhaseCutIn,  // 第４フェーズカットイン
    kFourthPhase,       // 第４フェーズ
    kFourthPhaseEffect, // 第４フェーズ終了エフェクト
    kFourthPhaseFinish, // 第４フェーズ終了
    kFifthStartWait,    // 第５フェーズ開始待機
    kFifthPhaseCutIn,   // 第５フェーズカットイン
    kFifthPhase,        // 第５フェーズ
    kFifthPhaseEffect,  // 第５フェーズ終了エフェクト
    kFifthPhaseFinish,  // 第５フェーズ終了
    kSixthStartWait,    // 第６フェーズ開始待機
    kSixthPhaseCutIn,   // 第６フェーズカットイン
    kSixthPhase,        // 第６フェーズ
    kSixthPhaseEffect,  // 第６フェーズ終了エフェクト
    kSixthPhaseFinish,  // 第６フェーズ終了
    kNextBoss,          // 次のボスへ遷移
    kPhaseMaxIndex      // フェーズ数
  };

  /// <summary>
  /// 移動の種類
  /// </summary>
  enum class MoveType {
    kLinear,           // 直線移動フェーズ
    kCurve,            // 曲線移動フェーズ
    kStop,             // 停止フェーズ
    kPhaseMaxIndex     // フェーズ数
  };

  /// <summary>
  /// 指定位置移動フェーズ
  /// </summary>
  enum class SpecifiedPhase {
    kBeforeMove,       // 初期化処理フェーズ
    kMove,             // 起動フェーズ
    kAfterMove,        // 処理中フェーズ
    kFinsh,            // 終了処理フェーズ
    kFinshed,          // 終了処理済みフェーズ
    kPhaseMaxIndex     // フェーズ数
  };

  /// <summary>
  /// 弾幕表示フェーズ
  /// </summary>
  enum class BarragePhase {
    kNone,             // 何もしない
    kStart,            // 開始フェーズ
    kProcess,          // 処理中フェーズ
    kFinish,           // 終了フェーズ
    kWait,             // 待機フェーズ
    kNextPhase,        // 次のフェーズへ
    kPhaseMaxIndex     // フェーズ数
  };

  /// <summary>
  /// 行動フェーズ中の処理
  /// </summary>
  enum class ActionBarrageType {
    kFirstPhase_01,    // 第１フェーズ その1
    kFirstPhase_02,    // 第１フェーズ その2
    kSecondPhase_01,   // 第２フェーズ その1
    kSecondPhase_02,   // 第２フェーズ その2
    kThirdPhase_01,    // 第３フェーズ その1
    kThirdPhase_02,    // 第３フェーズ その2
    kFourthPhase_01,   // 第４フェーズ その1
    kFourthPhase_02,   // 第４フェーズ その2
    kFifthPhase_01,    // 第５フェーズ その1
    kSixthPhase_01,    // 第６フェーズ その1
    kSixthPhase_02,    // 第６フェーズ その2
    kTypeMaxIndex      // フェーズ数
  };

  /// <summary>
  /// アイテム表示処理フェーズ
  /// </summary>
  enum class DispItemPhase {
    kCreate,            // 生成フェーズ
    kDisplay,           // 表示中フェーズ
    kFinishNotice,      // 終了通知フェーズ
    kFinish,            // 終了処理フェーズ
    kFinished,          // 終了処理済みフェーズ
    kPhaseMaxIndex      // フェーズ数
  };

  /// <summary>
  /// カットイン処理フェーズ
  /// </summary>
  enum class CutInPhase {
    kCreate,            // 生成フェーズ
    kHpBarCreate,       // HPバー表示フェーズ
    kWait,              // 待機フェーズ
    kFinishNotice,      // 終了通知フェーズ
    kFinish,            // 終了処理フェーズ
    kFinished,          // 終了処理済みフェーズ
    kPhaseMaxIndex      // フェーズ数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> エネミーイベントインターフェース </param>
  /// <param name=""> キャラクターコントローライベントインターフェース </param>
  /// <param name=""> キャラクターイベントインターフェイス </param>
  /// <returns></returns>
  EnemyA(EnemyEventInterface&, CharacterControllerEventInterface&,
         CharacterEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~EnemyA();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// 現在のフェーズが初期化処理か確認
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理済み, false:終了処理済み以外 </returns>
  bool IsInitialize() { return current_phase_ == PhaseType::kInitialize; }

  /// <summary>
  /// 現在の行動フェーズが「第６フェーズ」かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:第6フェーズ, false:それ以外 </returns>
  bool IsFinalPhase() { return action_phase_ == ActionPhase::kSixthPhase; }

  /// <summary>
  /// 現在のフェーズが終了処理済みか確認
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理済み, false:終了処理済み以外 </returns>
  bool IsFinalized() { return current_phase_ == PhaseType::kFinalized; }

  /// <summary>
  /// 行動フェーズ中の処理をセットする
  /// </summary>
  /// <param name=""> 行動フェーズ中の処理の種類 </param>
  /// <param name=""> フェーズデータ </param>
  /// <returns></returns>
  void SetActionBarrageType(ActionBarrageType, Barrage::BarrageData);

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }

  /// <summary>
  /// 状態フェーズを変更する
  /// </summary>
  /// <param name=""> 状態フェーズの種類 </param>
  /// <returns></returns>
  void ChangeStatusPhase(StatusPhase status_phase) { status_phase_ = status_phase; }

  /// <summary>
  /// 行動フェーズを変更する
  /// </summary>
  /// <param name=""> 行動フェーズの種類 </param>
  /// <returns></returns>
  void ChangeActionPhase(ActionPhase action_phase) { action_phase_ = action_phase; }

  /// <summary>
  /// 移動フェーズを変更する
  /// </summary>
  /// <param name=""> 移動フェーズの種類 </param>
  /// <returns></returns>
  void ChangeMoveType(MoveType move_type) { move_type_ = move_type; }

  /// <summary>
  /// 指定位置移動フェーズを変更する
  /// </summary>
  /// <param name=""> 移動フェーズの種類 </param>
  /// <returns></returns>
  void ChangeSpecifiedPhase(SpecifiedPhase specified_phase) { specified_phase_ = specified_phase; }

  /// <summary>
  /// 弾幕表示フェーズを変更する
  /// </summary>
  /// <param name=""> 弾幕表示フェーズの種類 </param>
  /// <returns></returns>
  void ChangeBarragePhase(BarragePhase barrage_phase) { barrage_phase_ = barrage_phase; }

  /// <summary>
  /// アイテム表示フェーズを変更する
  /// </summary>
  /// <param name=""> アイテム表示フェーズの種類 </param>
  /// <returns></returns>
  void ChangeDispItemPhase(DispItemPhase phase_type) { disp_item_phase_ = phase_type; }

  /// <summary>
  /// カットインフェーズを変更する
  /// </summary>
  /// <param name=""> カットインフェーズの種類 </param>
  /// <returns></returns>
  void ChangeCutInPhase(CutInPhase phase_type) { cut_in_phase_ = phase_type; }


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Initialize();

  /// <summary>
  /// 処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Process(float);

  /// <summary>
  /// 行動処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool ActionEnemyA(float);

  /// <summary>
  /// 第１フェーズの行動
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了, false:処理中 </returns>
  bool ActionFirstPhase();

  /// <summary>
  /// 第２フェーズの行動
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了, false:処理中 </returns>
  bool ActionSecondPhase();

  /// <summary>
  /// 第３フェーズの行動
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了, false:処理中 </returns>
  bool ActionThirdPhase();

  /// <summary>
  /// 第４フェーズの行動
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了, false:処理中 </returns>
  bool ActionFourthPhase();

  /// <summary>
  /// 第５フェーズの行動
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了, false:処理中 </returns>
  bool ActionFifthPhase();

  /// <summary>
  /// 第６フェーズの行動
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了, false:処理中 </returns>
  bool ActionSixthPhase();

  /// <summary>
  /// アイテム生成処理
  /// </summary>
  /// <param name=""> キャラクターの種類 </param>
  /// <returns> true:終了, false:処理中 </returns>
  bool DisplayItem(CharacterType);

  /// <summary>
  /// カットイン処理
  /// </summary>
  /// <param name=""> フェーズ数 </param>
  /// <param name=""> カットインの色 </param>
  /// <returns> true:終了, false:処理中 </returns>
  bool DisplayCutIn(int, CutInManager::ColorType);

  /// <summary>
  /// 次の移動先を計算処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void CalcMovePosition(float);

  /// <summary>
  /// 直線移動の計算
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void CalcLinearMovement();

  /// <summary>
  /// 移動処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void MoveEnemy();

  /// <summary>
  /// 指定位置へ移動処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool MoveSpecifiedEnemy();

  /// <summary>
  /// 弾幕の設定
  /// </summary>
  /// <param name=""> バレッジ </param>
  /// <param name=""> 弾幕情報 </param>
  /// <returns></returns>
  void SetBarrageData(Barrage*, Barrage::BarrageData);

  /// <summary>
  /// フェーズごとのHPの設定
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetEnemyAPhaseHitPoint();


private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 状態フェーズ
  /// </summary>
  StatusPhase status_phase_;

  /// <summary>
  /// 行動フェーズ
  /// </summary>
  ActionPhase action_phase_;

  /// <summary>
  /// 移動のフェーズ
  /// </summary>
  MoveType move_type_;

  /// <summary>
  /// 指定位置移動のフェーズ
  /// </summary>
  SpecifiedPhase specified_phase_;

  /// <summary>
  /// 弾幕表示のフェーズ
  /// </summary>
  BarragePhase barrage_phase_;

  /// <summary>
  /// アイテム表示フェーズ
  /// </summary>
  DispItemPhase disp_item_phase_;

  /// <summary>
  /// カットインフェーズ
  /// </summary>
  CutInPhase cut_in_phase_;

  /// <summary>
  /// フェーズデータ
  /// </summary>
  std::unordered_map<ActionBarrageType, Barrage::BarrageData> phase_data_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// エフェクト実行回数
  /// </summary>
  int exec_effect_count_;
};