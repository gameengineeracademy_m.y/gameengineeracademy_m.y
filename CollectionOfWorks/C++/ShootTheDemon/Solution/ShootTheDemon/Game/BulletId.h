﻿#pragma once

/// <summary>
/// バレットID
/// </summary>
enum class BulletId {
  kNormal,         // 通常弾 プレイヤー使用弾
  kBlueBall,       // 青弾
  kPurpleBall,     // 紫弾
  kRedBall,        // 赤弾
  kYellowBall,     // 黄弾
  kGreenBall,      // 緑弾
  kBulletMaxIndex  // 弾の種類数
};