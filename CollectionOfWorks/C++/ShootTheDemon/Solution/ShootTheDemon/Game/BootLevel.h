﻿#pragma once

#include "DxLib.h"
#include "System/Level.h"
#include "System/FontInfo.h"
#include "System/EffectManager.h"
#include "Game/GameInfo.h"
#include <vector>
#include <fstream>
#include <string>

namespace {

  /// <summary>
  /// ピリオドの数 最大値
  /// </summary>
  const int kTextMaxNum = 4;
}

/// <summary>
/// ブートレベル
/// </summary>
/// <remarks>
/// 起動した直後に表示される画面のタスク
/// </remarks>
class BootLevel : public Level {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kLoadFile,            // 設定ファイル読み込みフェーズ
    kPrepare,             // 準備フェーズ
    kStartUp,             // 起動フェーズ
    kTransitionWait,      // 遷移前待機フェーズ
    kLevelTransition,     // レベル遷移フェーズ
    kFinalize,            // 終了処理フェーズ
    kFinalized,           // 終了処理済みフェーズ
    kPhaseMaxIndex        // フェーズ数
  };

  /// <summary>
  /// 処理中フェーズの種類
  /// </summary>
  enum class ProcessPhase {
    kPlayerShow,          // プレイヤー表示
    kPlayerWalk,          // プレイヤー1歩前進
    kFinish,              // 終了
    kPhaseMaxIndex        // フェーズ数
  };

  /// <summary>
  /// 終了フェーズの種類
  /// </summary>
  enum class FinalizePhaseType {
    kUnloadTask,         // タスクを降ろすフェーズ
    kDisposeTask,        // タスクを破棄するフェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> タスクID </param>
  /// <param name=""> タスクマネージャ </param>
  /// <returns></returns>
  BootLevel(TaskId, TaskManager&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~BootLevel();


protected:

  /// <summary>
  /// 初期化処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool InitializePhase(float) override;

  /// <summary>
  /// 毎フレームの処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool UpdatePhase(float) override;

  /// <summary>
  /// 毎フレームの描画処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void RenderPhase() override;

  /// <summary>
  /// 終了処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool FinishPhase(float) override;


private:

  /// <summary>
  /// タイトルとテキストの描画 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeTitleText();

  /// <summary>
  /// エフェクトマネージャ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeEffectManager();

  /// <summary>
  /// プレイヤー画像ロード 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool LoadImagePlayer();

  /// <summary>
  /// 設定ファイル読み込み
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool LoadSetupFile();

  /// <summary>
  /// ロードファイル エフェクト情報
  /// </summary>
  /// <param name=""> ファイルパス </param>
  /// <param name=""> 弾幕の情報 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadFileEffect(std::string, Effect::EffectData&);

  /// <summary>
  /// エフェクトの登録処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:処理継続中 </returns>
  bool RegisterEffect();

  /// <summary>
  /// 処理中処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:正常終了, false:処理継続中 </returns>
  bool Process(float);

  /// <summary>
  /// 現在のフェーズの変更
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; };

  /// <summary>
  /// 処理中フェーズの変更
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeProcessPhase(ProcessPhase phase_type) { process_phase_ = phase_type; };

  /// <summary>
  /// 終了処理フェーズの変更
  /// </summary>
  /// <param name=""> 終了フェーズの種類 </param>
  /// <returns></returns>
  void ChangeFinalizePhase(FinalizePhaseType finalize_phase) { finalize_phase_ = finalize_phase; };

  /// <summary>
  /// 透過率変更処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AdjustAlphaValue();


private:

  /// <summary>
  /// 表示座標
  /// </summary>
  struct Pos {
    int x;
    int y;
  };

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 処理中フェーズ
  /// </summary>
  ProcessPhase process_phase_;

  /// <summary>
  /// 現在の終了フェーズ
  /// </summary>
  FinalizePhaseType finalize_phase_;

  /// <summary>
  /// エフェクトマネージャ
  /// </summary>
  EffectManager* effect_manager_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// 累積時間(演出用)
  /// </summary>
  float production_time_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 透過率 プレイヤー
  /// </summary>
  int alpha_player_;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  int alpha_adjust_;

  /// <summary>
  /// 表示ブート文字列
  /// </summary>
  std::string display_text_[kTextMaxNum];

  /// <summary>
  /// 配列インデックス
  /// </summary>
  int text_index_;

  /// <summary>
  /// プレイヤーのグラフィックハンドル
  /// </summary>
  std::vector<int> player_handle_;

  /// <summary>
  /// プレイヤーの描画インデックス
  /// </summary>
  int player_index_;

  /// <summary>
  /// プレイヤーの描画インデックス 調整
  /// </summary>
  int player_index_adjust_;

  /// <summary>
  /// エフェクトデータ
  /// </summary>
  Effect::EffectData effect_data_;

  /// <summary>
  /// 文字列 表示座標
  /// </summary>
  Pos text_pos_;

  /// <summary>
  /// エフェクト 表示座標
  /// </summary>
  Pos effect_pos_;

  /// <summary>
  /// プレイヤー 表示座標
  /// </summary>
  Pos player_pos_;

  /// <summary>
  /// フォントハンドル
  /// </summary>
  int font_handle_;
};