﻿#include "Game/BootLevel.h"

namespace {

  /// <summary>
  /// 累積時間のリセット値
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 指定時間 ピリオド増加
  /// </summary>
  const float kDispWaitTime = 0.32f;

  /// <summary>
  /// 指定時間 プレイヤーインデックス変更(回転)
  /// </summary>
  const float kChangePlayerRotaIndexTime = 0.05f;

  /// <summary>
  /// 指定時間 プレイヤーインデックス変更(歩行)
  /// </summary>
  const float kChangePlayerWalkIndexTime = 0.28f;

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Reggae One";

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSize = 50;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 1;

  /// <summary>
  /// 文字の色
  /// </summary>
  const int kStringForeColor = GetColor(255, 255, 255);   // 白色

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjustInit = 5;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjustPlayerInit = 8;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 符号反転
  /// </summary>
  const int kMinusSign = -1;

  /// <summary>
  /// フォントファイルパス
  /// </summary>
  const char* kFontFilePath = "Asset/Font/ReggaeOne-Regular.ttf";

  /// <summary>
  /// プレイヤー画像パス
  /// </summary>
  const char* kImagePlayerPath = "Asset/Image/player.png";

  /// <summary>
  /// プレイヤー復活 画像パス
  /// </summary>
  const char* kImageRebornPath = "Asset/Image/player_reborn.png";

  /// <summary>
  /// 復活エフェクト 情報ファイルパス
  /// </summary>
  const char* kRebornFilePath = "File/BattleLevel_Effect_Reborn.txt";

  /// <summary>
  /// プレイヤー総分割数
  /// </summary>
  const int kPlayerDivTotalNum = 12;

  /// <summary>
  /// プレイヤー分割数 横方向
  /// </summary>
  const int kPlayerDivX = 3;

  /// <summary>
  /// プレイヤー分割数 縦方向
  /// </summary>
  const int kPlayerDivY = 4;

  /// <summary>
  /// プレイヤーサイズ 幅
  /// </summary>
  const int kPlayerSizeWidth = 64;

  /// <summary>
  /// プレイヤーサイズ 高さ
  /// </summary>
  const int kPlayerSizeHeight = 64;

  /// <summary>
  /// プレイヤーインデックス歩行
  /// </summary>
  const int kPlayerWalk1Index = 0;

  /// <summary>
  /// プレイヤーインデックス 正面直立
  /// </summary>
  const int kPlayerFrontIndex = 1;

  /// <summary>
  /// プレイヤーインデックス歩行
  /// </summary>
  const int kPlayerWalk2Index = 2;

  /// <summary>
  /// 歩行切り替え値
  /// </summary>
  const int kSwitchWalk = -1;

  /// <summary>
  /// プレイヤー 拡大率
  /// </summary>
  const float kExRate = 1.0f;

  /// <summary>
  /// プレイヤー 角度
  /// </summary>
  const float kAngle = 0.0f;

  /// <summary>
  /// 復活画像 総数
  /// </summary>
  const int kRebornImageDivNum = 55;

  /// <summary>
  /// プレイヤー表示位置 X座標
  /// </summary>
  const int kPrayerPosX = 650;

  /// <summary>
  /// プレイヤー表示位置 Y座標
  /// </summary>
  const int kPrayerPosY = 400;

  /// <summary>
  /// エフェクト表示位置 X座標
  /// </summary>
  const int kEffectPosX = 650;

  /// <summary>
  /// エフェクト表示位置 Y座標
  /// </summary>
  const int kEffectPosY = 384;

  /// <summary>
  /// 画像ロードエラー
  /// </summary>
  const int kLoadError = -1;

  /// <summary>
  /// 表示文字列
  /// </summary>
  const char* kDisplayText = "Boot";

  /// <summary>
  /// 表示ピリオド
  /// </summary>
  const char* kDisplayPeriod = ".";

  /// <summary>
  /// 表示文字列
  /// </summary>
  const char* kDisplayMaxText = "Boot...";

  /// <summary>
  /// テキスト 色
  /// </summary>
  const int kTextForeColor = GetColor(255, 255, 255);

  /// <summary>
  /// テキストインデックス最小値
  /// </summary>
  const int kTextMinIndex = 0;

  /// <summary>
  /// テキストインデックス最大値
  /// </summary>
  const int kTextMaxIndex = 3;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> タスクID </param>
/// <param name=""> タスクマネージャ </param>
/// <returns></returns>
BootLevel::BootLevel(TaskId task_id, TaskManager& task_manager)
  : Level(task_id, task_manager)
  , current_phase_(PhaseType::kLoadFile)
  , process_phase_(ProcessPhase::kPlayerShow)
  , finalize_phase_(FinalizePhaseType::kUnloadTask)
  , effect_manager_(nullptr)
  , accumulate_time_(0.0f)
  , production_time_(0.0f)
  , alpha_(0)
  , alpha_player_(0)
  , alpha_adjust_(0)
  , display_text_()
  , text_index_()
  , player_handle_()
  , player_index_()
  , player_index_adjust_()
  , effect_data_()
  , text_pos_()
  , effect_pos_()
  , player_pos_()
  , font_handle_() {

  // コンソールに出力
  std::cout << "BootLevel コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
BootLevel::~BootLevel() {

  // コンソールに出力
  std::cout << "~BootLevel デストラクタ" << std::endl;
}

/// <summary>
/// 初期化処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool BootLevel::InitializePhase(float process_time) {

  bool is_success = false;

  // 処理時間をリセットしておく
  accumulate_time_ = kResetTime;
  // 透過率 初期設定
  alpha_ = kAlphaMin;
  alpha_adjust_ = kAlphaAdjustInit;

  // フォント情報生成
  FontInfo* font_info = FontInfo::GetFontInfoInstance();
  if (font_info == nullptr) {
    return false;
  }
  int result = font_info->CreateFontInfo(kFontFilePath);
  font_handle_ = font_info->GetFontInfo(kFontName, kFontSize, kFontWidth, DX_FONTTYPE_NORMAL);

  // タイトルとテキストの描画準備
  is_success = InitializeTitleText();
  if (!is_success) {
    return false;
  }

  // プレイヤー画像ロード 初期設定
  is_success = LoadImagePlayer();
  if (!is_success) {
    return false;
  }

  // エフェクトマネージャ 初期設定
  is_success = InitializeEffectManager();
  if (!is_success) {
    return false;
  }

  return true;
}

/// <summary>
/// 毎フレームの処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool BootLevel::UpdatePhase(float process_time) {

  bool is_finish = false;

  switch (current_phase_) {
  case PhaseType::kLoadFile: {
    //------------------------------
    //設定ファイル読み込みフェーズ
    //------------------------------
    is_finish = LoadSetupFile();
    if (is_finish) {
      // 現在のフェーズを「準備」に変更
      ChangeCurrentPhase(PhaseType::kPrepare);
    }
    break;
  }
  case PhaseType::kPrepare: {
    //------------------------------
    //準備フェーズ
    //------------------------------
    // エフェクトの登録処理
    is_finish = RegisterEffect();
    if (is_finish) {
      // 現在のフェーズを「起動」に変更
      ChangeCurrentPhase(PhaseType::kStartUp);
      effect_manager_->CreateEffect(EffectId::kReborn);
    }
    break;
  }
  case PhaseType::kStartUp: {
    //------------------------------
    //起動フェーズ
    //------------------------------
    // フェードイン処理完了後、現在のフェーズを「遷移前待機」に変更
    is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 処理時間をリセットする
      accumulate_time_ = kResetTime;
      ChangeCurrentPhase(PhaseType::kTransitionWait);
    }
    break;
  }
  case PhaseType::kTransitionWait: {
    //------------------------------
    //遷移前待機フェーズ
    //------------------------------
    // 処理中処理
    is_finish = Process(process_time);
    if (is_finish) {
      // 現在のフェーズを「レベル遷移」に変更
      ChangeCurrentPhase(PhaseType::kLevelTransition);
    }
    break;
  }
  case PhaseType::kLevelTransition: {
    //------------------------------
    //レベル遷移フェーズ
    //------------------------------
    // フェードアウト処理完了後、現在のフェーズを「終了処理」に変更
    is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //------------------------------
    //終了処理フェーズ
    //------------------------------
    // タイトルレベルへ切り替える
    ChangeLevel(TaskId::kTitleLevel);
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }

  return false;
}

/// <summary>
/// 毎フレームの描画処理フェーズの処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BootLevel::RenderPhase() {

  switch (current_phase_) {
  case PhaseType::kStartUp:
  case PhaseType::kTransitionWait:
  case PhaseType::kLevelTransition: {

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

    // 文字の描画
    DrawStringToHandle(text_pos_.x, text_pos_.y, display_text_[text_index_].c_str(), kTextForeColor, font_handle_);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_player_);

    // 文字の描画
    DrawRotaGraph(player_pos_.x, player_pos_.y, kExRate, kAngle, player_handle_.at(player_index_), true);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    break;
  }
  }
}

/// <summary>
/// 終了処理フェーズの処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool BootLevel::FinishPhase(float process_time) {

  switch (finalize_phase_) {
  case FinalizePhaseType::kUnloadTask: {
    //-------------------------------
    //タスクを降ろすフェーズ
    //-------------------------------
    // エフェクトの破棄
    if (effect_manager_ != nullptr) {
      effect_manager_->DisposeEffect();
    }

    // フォント情報生成
    FontInfo* font_info = FontInfo::GetFontInfoInstance();
    if (font_info == nullptr) {
      return false;
    }
    // フォント破棄
    font_info->ReleaseFontInfo(kFontFilePath);

    // 画像の破棄
    for (int i = 0; i < kPlayerDivTotalNum; ++i) {
      DeleteGraph(player_handle_[i]);                // プレイヤー
    }

    // エフェクトマネージャをタスクマネージャから降ろす
    if (effect_manager_ != nullptr) {
      TaskId task_id = effect_manager_->GetTaskId();
      Task* release_task = task_manager_.ReleaseTask(task_id);
    }

    // 終了フェーズをタスク破棄フェーズに変更
    ChangeFinalizePhase(FinalizePhaseType::kDisposeTask);
    return false;
  }
  case FinalizePhaseType::kDisposeTask: {
    //-------------------------------
    //タスクを破棄するフェーズ
    //-------------------------------
    if (effect_manager_ != nullptr) {
      delete effect_manager_;
      effect_manager_ = nullptr;
    }
    break;
  }
  }

  return true;
}

/// <summary>
/// タイトルとテキストの描画 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BootLevel::InitializeTitleText() {

  // ゲーム情報から画面中央位置を取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return false;
  }
  int x_pos_center = game_info->GetCenterPointX();
  int y_pos_center = game_info->GetCenterPointY();

  //------------------------------------
  //画面表示文字列 準備
  //------------------------------------
  std::string period = "";
  // 配列の作成
  for (int i = 0; i < kTextMaxNum; ++i) {
    display_text_[i] = kDisplayText + period;
    // ピリオドの数を追加
    period += static_cast<std::string>(kDisplayPeriod);
  }
  // インデックス初期化
  text_index_ = kTextMinIndex;

  // 表示座標
  int text_width = GetDrawStringWidthToHandle(kDisplayMaxText, static_cast<int>(strlen(kDisplayMaxText)), font_handle_);
  text_pos_.x = x_pos_center - (text_width / kHalfValue);
  text_pos_.y = y_pos_center;

  return true;
}

/// <summary>
/// エフェクトマネージャ 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BootLevel::InitializeEffectManager() {

  // エフェクトマネージャの生成
  effect_manager_ = new EffectManager();
  if (effect_manager_ == nullptr) {
    std::cout << "BattleLevel InitializeEffectManager：クラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }
  // タスクマネージャにタスクを積む
  task_manager_.AddTask(effect_manager_);

  return true;
}

/// <summary>
/// プレイヤー画像ロード 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BootLevel::LoadImagePlayer() {

  // グラフィックハンドル
  int graphic_handle[kPlayerDivTotalNum];
  // 画像をロードする
  int result = LoadDivGraph(kImagePlayerPath, kPlayerDivTotalNum, kPlayerDivX, kPlayerDivY,
    kPlayerSizeWidth, kPlayerSizeHeight, graphic_handle, true);
  if (result == kLoadError) {
    return false;
  }

  // プレイヤーのグラフィックハンドルをセット
  for (int i = 0; i < kPlayerDivTotalNum; ++i) {
    player_handle_.push_back(graphic_handle[i]);
  }

  // インデックス初期化
  player_index_ = kPlayerFrontIndex;
  player_index_adjust_ = kPlayerFrontIndex;

  // プレイヤー表示座標
  player_pos_.x = kPrayerPosX;
  player_pos_.y = kPrayerPosY;
  // エフェクト表示座標
  effect_pos_.x = kEffectPosX;
  effect_pos_.y = kEffectPosY;

  return true;
}

/// <summary>
/// エフェクトの登録処理
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:処理継続中 </returns>
bool BootLevel::RegisterEffect() {

  //-----------------------------------
  //プレイヤー復活
  //-----------------------------------
  // グラフィックハンドルを取得
  int graphic_handle[kPlayerRebornDivCount];
  // 画像のロード
  int result = LoadDivGraph(kImageRebornPath, effect_data_.div.total,
                            effect_data_.div.x, effect_data_.div.y,
                            effect_data_.div.width, effect_data_.div.height, graphic_handle);
  if (result == kLoadError) {
    return false;
  }

  // エフェクトの生成
  Effect* effect = effect_manager_->RegisterEffect(EffectId::kReborn, effect_data_.detail.type);
  if (effect == nullptr) {
    std::cout << "BattleLevel RegisterEffect：エフェクトクラス生成失敗" << std::endl;
    OccurredError();
    return false;
  }

  // グラフィックハンドルをセット
  for (int i = 0; i < effect_data_.div.total; ++i) {
    effect->SetEffectHandle(graphic_handle[i]);
  }

  // 各種設定
  effect->SetAlpha(alpha_, alpha_adjust_,
                   effect_data_.alpha.max, effect_data_.alpha.min);
  effect->SetLoadDivData(effect_data_.div.total, effect_data_.div.x, effect_data_.div.y,
                         effect_data_.div.width, effect_data_.div.height);
  effect->SetWaitTime(effect_data_.detail.wait_time);
  effect->SetExtRateX(effect_data_.ex_rate_x.value, effect_data_.ex_rate_x.adjust,
                      effect_data_.ex_rate_x.max, effect_data_.ex_rate_x.min);
  effect->SetExtRateY(effect_data_.ex_rate_y.value, effect_data_.ex_rate_y.adjust,
                      effect_data_.ex_rate_y.max, effect_data_.ex_rate_y.min);
  effect->SetAngle(effect_data_.angle.value, effect_data_.angle.adjust,
                   effect_data_.angle.max, effect_data_.angle.min);

  // 表示座標を渡す
  effect->SetPosX(kEffectPosX);
  effect->SetPosY(kEffectPosY);

  return true;
}

/// <summary>
/// 透過率変更処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BootLevel::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率に調整値を加算
  alpha_ += alpha_adjust_;

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= kAlphaMax) {
    alpha_ = kAlphaMax;
    is_finish = true;
  }
  else if (alpha_ <= kAlphaMin) {
    alpha_ = kAlphaMin;
    is_finish = true;
  }

  return is_finish;
}

/// <summary>
/// 処理中処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true:正常終了, false:処理継続中 </returns>
bool BootLevel::Process(float process_time) {

  // 処理時間を累積していく
  accumulate_time_ += process_time;

  if (accumulate_time_ >= kDispWaitTime) {
    // 累積時間をリセット
    accumulate_time_ = kResetTime;

    // 最終インデックス到達確認後、フェーズを切り替える
    if (text_index_ == kTextMaxIndex) {
      return true;
    }
    else {
      // インデックスをインクリメント
      ++text_index_;
    }
  }

  switch (process_phase_) {
  case ProcessPhase::kPlayerShow: {
    //-------------------------------
    // プレイヤー表示
    //-------------------------------
    alpha_player_ += kAlphaAdjustPlayerInit;

    if (alpha_player_ >= kAlphaMax) {

      alpha_player_ = kAlphaMax;

      player_index_ = kPlayerWalk1Index;
      // 処理中フェーズを「プレイヤー前進」に変更
      ChangeProcessPhase(ProcessPhase::kPlayerWalk);
    }
    break;
  }
  case ProcessPhase::kPlayerWalk: {
    //-------------------------------
    // プレイヤー前進
    //-------------------------------
    // 指定時間ごとにインデックスを変更
    production_time_ += process_time;
    if (production_time_ >= kChangePlayerWalkIndexTime) {
      production_time_ = kResetTime;

      //switch (player_index_) {
      //case kPlayerFrontIndex: {
      //  player_index_ += player_index_adjust_;
      //  player_pos_.y += 8;
      //  break;
      //}
      //case kPlayerWalk1Index:
      //case kPlayerWalk2Index: { 
      //  player_index_adjust_ = kSwitchWalk * player_index_adjust_;
      //  player_index_ += player_index_adjust_;
      //  player_pos_.y += 8;
      //  break;
      //}
      //}
      switch (player_index_) {
      case kPlayerWalk1Index: {
        player_index_ = kPlayerWalk2Index;
        player_pos_.y += 8;
        break;
      }
      case kPlayerWalk2Index: {
        player_index_ = kPlayerWalk1Index;
        player_pos_.y += 8;
        break;
      }
      }
    }

    break;
  }
  }

  return false;
}


/// <summary>
/// 設定ファイル読み込み
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool BootLevel::LoadSetupFile() {

  // エフェクトデータ
  Effect::EffectData effect_data = {};
  // ファイルをロードし、データを取得する
  bool is_finish = LoadFileEffect(kRebornFilePath, effect_data);
  if (!is_finish) {
    return false;
  }
  // リストに追加
  effect_data_ = effect_data;

  return true;
}

/// <summary>
/// ロードファイル エフェクト情報
/// </summary>
/// <param name=""> ファイルパス </param>
/// <param name=""> 弾幕の情報 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BootLevel::LoadFileEffect(std::string file_path, Effect::EffectData& effect_data) {

  //ファイルストリームを宣言と同時にファイルを開く
  std::ifstream stream(file_path, std::ios::in);
  //ファイルが開けないならエラー
  if (!stream) {
    std::cout << "BattleLevel LoadFileEffect ファイルが開けません" << std::endl;
    return false;
  }

  effect_data = {};

  //ファイルから読み込む
  std::string line;
  //１行ずつ読み出す
  while (std::getline(stream, line)) {
    std::cout << line << std::endl;
    int equal_pos = 0;
    if (line.find("PositionX=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.pos.x = std::stoi(str_value);
    }
    else if (line.find("PositionY=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.pos.y = std::stoi(str_value);
    }
    else if (line.find("EffectType=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.detail.type = static_cast<Effect::EffectType>(std::stoi(str_value));
    }
    else if (line.find("WaitTime=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.detail.wait_time = std::stof(str_value);
    }
    else if (line.find("DivTotalNum=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.div.total = std::stoi(str_value);
    }
    else if (line.find("DivX=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.div.x = std::stoi(str_value);
    }
    else if (line.find("DivY=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.div.y = std::stoi(str_value);
    }
    else if (line.find("SizeWidth=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.div.width = std::stoi(str_value);
    }
    else if (line.find("SizeHeight=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.div.height = std::stoi(str_value);
    }
    else if (line.find("AlphaValue=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.alpha.value = std::stoi(str_value);
    }
    else if (line.find("AlphaAdjust=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.alpha.adjust = std::stoi(str_value);
    }
    else if (line.find("AlphaMax=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.alpha.max = std::stoi(str_value);
    }
    else if (line.find("AlphaMin=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.alpha.min = std::stoi(str_value);
    }
    else if (line.find("ExtRateXValue=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.ex_rate_x.value = std::stof(str_value);
    }
    else if (line.find("ExtRateXAdjust=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.ex_rate_x.adjust = std::stof(str_value);
    }
    else if (line.find("ExtRateXMax=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.ex_rate_x.max = std::stof(str_value);
    }
    else if (line.find("ExtRateXMin=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.ex_rate_x.min = std::stof(str_value);
    }
    else if (line.find("ExtRateYValue=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.ex_rate_y.value = std::stof(str_value);
    }
    else if (line.find("ExtRateYAdjust=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.ex_rate_y.adjust = std::stof(str_value);
    }
    else if (line.find("ExtRateYMax=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.ex_rate_y.max = std::stof(str_value);
    }
    else if (line.find("ExtRateYMin=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.ex_rate_y.min = std::stof(str_value);
    }
    else if (line.find("AngleValue=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.angle.value = std::stof(str_value);
    }
    else if (line.find("AngleAdjust=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.angle.adjust = std::stof(str_value);
    }
    else if (line.find("AngleMax=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.angle.max = std::stof(str_value);
    }
    else if (line.find("AngleMin=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      effect_data.angle.min = std::stof(str_value);
    }
  }

  // 行読み込みデータクリア
  line.clear();
  //ファイルを閉じる
  stream.close();
  stream.clear();

  return true;
}