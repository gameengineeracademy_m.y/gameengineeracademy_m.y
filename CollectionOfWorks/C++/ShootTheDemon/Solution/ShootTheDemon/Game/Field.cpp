﻿#include "Game/Field.h"

namespace {

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjustInit = 20;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 符号反転
  /// </summary>
  const int kMinusSign = -1;

  /// <summary>
  /// 解像度 高さ
  /// </summary>
  const int kResolutionHeight = 1000;

  /// <summary>
  /// Y座標 リセット値
  /// </summary>
  const int kResetPosY = -32;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Field::Field()
  : current_phase_(PhaseType::kInitialize)
  , graphic_handle_(0)
  , x_pos_(0)
  , y_pos_(0)
  , move_x_value_(0)
  , move_y_value_(0)
  , alpha_(0)
  , alpha_adjust_(0) {

  // コンソールに出力
  std::cout << "Field コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Field::~Field() {

  // コンソールに出力
  std::cout << "~Field デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Field::Update() {

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //------------------------------
    //初期化フェーズ
    //------------------------------
    bool is_finish = Initialize();
    if (is_finish) {
      // 現在のフェーズを「起動待機」に変更
      ChangeCurrentPhase(PhaseType::kStartWait);
    }
    break;
  }
  case PhaseType::kStartWait: {
    //------------------------------
    //起動待機フェーズ
    //------------------------------
    break;
  }
  case PhaseType::kStartUp: {
    //------------------------------
    //起動フェーズ
    //------------------------------
    // フェードイン処理完了後、現在のフェーズを「プレイ中」に変更
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 透過率調整値の符号を反転しておく
      alpha_adjust_ = kMinusSign * alpha_adjust_;
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //------------------------------
    //処理中フェーズ
    //------------------------------
    // 各座標に移動量加算
    PrepareScroll();
    break;
  }
  case PhaseType::kLevelTransition: {
    //------------------------------
    //レベル遷移フェーズ
    //------------------------------
     // フェードアウト処理完了後、現在のフェーズを「終了処理」に変更
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //------------------------------
    //終了処理フェーズ
    //------------------------------
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Field::Render() {

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

  DrawGraph(x_pos_, y_pos_, graphic_handle_, true);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool Field::Initialize() {

  // 透過率 初期設定
  alpha_ = kAlphaMin;
  alpha_adjust_ = kAlphaAdjustInit;

  return true;
}

/// <summary>
/// スクロール準備フェーズ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Field::PrepareScroll() {

  // ゲーム情報から画面の縦サイズを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  int screen_height = game_info->GetResolutionHeight();

  // X座標、Y座標に移動量を加算
  x_pos_ += move_x_value_;
  y_pos_ += move_y_value_;
  // 画面下部より外に出た場合は、画面上部に戻す
  if (y_pos_ >= screen_height) {
    y_pos_ = kResetPosY;
  }
}

/// <summary>
/// 透過率変更処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool Field::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率に調整値を加算
  alpha_ += alpha_adjust_;

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= kAlphaMax) {
    alpha_ = kAlphaMax;
    is_finish = true;
  }
  else if (alpha_ <= kAlphaMin) {
    alpha_ = kAlphaMin;
    is_finish = true;
  }

  return is_finish;
}