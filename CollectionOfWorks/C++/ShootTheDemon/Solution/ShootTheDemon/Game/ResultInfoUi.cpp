﻿#include "Game/ResultInfoUi.h"

namespace {

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjust = 5;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// ボタン表示位置 X座標
  /// </summary>
  const int kButtonPosX = 270;

  /// <summary>
  /// ボタン表示位置 X座標
  /// </summary>
  const int kButtonPosY = 275;

  /// <summary>
  /// ボタンサイズ 幅
  /// </summary>
  const int kButtonWidth = 200;

  /// <summary>
  /// ボタンサイズ 高さ
  /// </summary>
  const int kButtonHeight = 50;

  /// <summary>
  /// ボタンテキスト 高さ調整値
  /// </summary>
  const int kButtonTextPosY = 12;

  /// <summary>
  /// カーソル調整値 固定値
  /// </summary>
  const float kCursorAdjust = 1.0f;

  /// <summary>
  /// カーソル調整値 X方向
  /// </summary>
  const float kCursorPlusAdjustX = 1.0f;

  /// <summary>
  /// カーソル調整値 Y方向
  /// </summary>
  const float kCursorPlusAdjustY = 1.0f;

  /// <summary>
  /// カーソル調整値 X方向
  /// </summary>
  const float kCursorMinusAdjustX = -1.0f;

  /// <summary>
  /// カーソル調整値 Y方向
  /// </summary>
  const float kCursorMinusAdjustY = -1.0f;

  /// <summary>
  /// カーソル 幅
  /// </summary>
  const float kCursorThick = 3.0f;

  /// <summary>
  /// ボタン色 選択中
  /// </summary>
  const int kButtonSelectColor = GetColor(190, 90, 255);

  /// <summary>
  /// ボタン色 選択外
  /// </summary>
  const int kButtonNoneColor = GetColor(130, 0, 170);

  /// <summary>
  /// ボタン テキスト 色
  /// </summary>
  const int kButtonTextColor = GetColor(255, 255, 255);

  /// <summary>
  /// カーソル 色
  /// </summary>
  const int kCursorColor = GetColor(0, 255, 0);

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Reggae One";

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSize = 24;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 1;

  /// <summary>
  /// ボタンテキスト リトライ
  /// </summary>
  const char* kButtonTextRetry = "RETRY";

  /// <summary>
  /// ボタンテキスト タイトルへ戻る
  /// </summary>
  const char* kButtonTextReturn = "RETURN TITLE";

  /// <summary>
  /// 半分
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 待機時間
  /// </summary>
  const float kWaitTime = 0.7f;

  /// <summary>
  /// リセット時間
  /// </summary>
  const float kResetTime = 0.0f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ResultInfoUi::ResultInfoUi()
  : Task(TaskId::kResultInfoUi)
  , current_phase_(PhaseType::kNone)
  , button_type_(ButtonType::kRetry)
  , button_retry_()
  , button_return_()
  , text_button_retry_()
  , text_button_return_()
  , cursor_()
  , cursor_adjust_()
  , button_color_()
  , alpha_(0)
  , accumulate_time_(0.0f)
  , font_handle_(0) {

  // コンソールに出力
  std::cout << "ResultInfoUi コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ResultInfoUi::~ResultInfoUi() {

  // コンソールに出力
  std::cout << "~ResultInfoUi デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void ResultInfoUi::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kNone: {
    //-------------------------------
    //何もしないフェーズ
    //-------------------------------
    break;
  }
  case PhaseType::kInitialize: {
    //-------------------------------
    //初期化処理フェーズ
    //-------------------------------
    bool is_finish = Initialize();
    if (is_finish) {
      // 現在のフェーズを「起動待機」に変更
      ChangeCurrentPhase(PhaseType::kStartWait);
    }
    break;
  }
  case PhaseType::kStartWait: {
    //-------------------------------
    //起動待機フェーズ
    //-------------------------------
    break;
  }
  case PhaseType::kStartUp: {
    //-------------------------------
    //起動フェーズ
    //-------------------------------
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //-------------------------------
    //処理中フェーズ
    //-------------------------------
    Process(process_time);
    break;
  }
  case PhaseType::kBeforeFinalize: {
    //-------------------------------
    //終了処理前フェーズ
    //-------------------------------
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //-------------------------------
    //終了処理フェーズ
    //-------------------------------
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ResultInfoUi::Render() {

  switch (current_phase_) {
  case PhaseType::kStartUp:
  case PhaseType::kProcess:
  case PhaseType::kBeforeFinalize: {

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

    //-------------------------------------------
    // ボタン
    //-------------------------------------------
    DrawBox(button_retry_.left.x, button_retry_.left.y,
            button_retry_.right.x, button_retry_.right.y,
            button_color_[static_cast<int>(ButtonType::kRetry)], true);
    DrawBox(button_return_.left.x, button_return_.left.y,
            button_return_.right.x, button_return_.right.y,
            button_color_[static_cast<int>(ButtonType::kReturnTitle)], true);

    //-------------------------------------------
    // ボタンテキスト
    //-------------------------------------------
    DrawStringToHandle(text_button_retry_.x, text_button_retry_.y, kButtonTextRetry, kButtonTextColor, font_handle_);
    DrawStringToHandle(text_button_return_.x, text_button_return_.y, kButtonTextReturn, kButtonTextColor, font_handle_);

    //-------------------------------------------
    // カーソル
    //-------------------------------------------
    // 左
    DrawLineAA(cursor_.left.x, cursor_.left.y, cursor_.left.x,
      cursor_.right.y, kCursorColor, kCursorThick);
    // 右
    DrawLineAA(cursor_.right.x - kCursorAdjust, cursor_.left.y,
      cursor_.right.x - kCursorAdjust, cursor_.right.y, kCursorColor, kCursorThick);
    // 上
    DrawLineAA(cursor_.left.x - kCursorAdjust, cursor_.left.y,
      cursor_.right.x + kCursorAdjust, cursor_.left.y, kCursorColor, kCursorThick);
    // 下
    DrawLineAA(cursor_.left.x - kCursorAdjust, cursor_.right.y,
      cursor_.right.x + kCursorAdjust, cursor_.right.y, kCursorColor, kCursorThick);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    break;
  }
  }
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param names=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool ResultInfoUi::Initialize() {

  // 透過率の設定
  alpha_ = kAlphaMin;

  // 累積時間をリセット
  accumulate_time_ = kResetTime;

  // ゲーム情報から画面の中心位置を取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return false;
  }
  int x_pos_center = game_info->GetCenterPointX();
  int y_pos_center = game_info->GetCenterPointY();

  // フォント情報生成
  FontInfo* font_info = FontInfo::GetFontInfoInstance();
  if (font_info == nullptr) {
    return false;
  }
  font_handle_ = font_info->GetFontInfo(kFontName, kFontSize, kFontWidth, DX_FONTTYPE_NORMAL);

  //---------------------------------------------
  // ボタン
  //---------------------------------------------
  button_retry_.size.width = kButtonWidth;
  button_retry_.size.height = kButtonHeight;
  button_retry_.left.x = x_pos_center - kButtonPosX;
  button_retry_.left.y = y_pos_center + kButtonPosY;
  button_retry_.right.x = button_retry_.left.x + button_retry_.size.width;
  button_retry_.right.y = button_retry_.left.y + button_retry_.size.height;

  button_return_.size.width = kButtonWidth;
  button_return_.size.height = kButtonHeight;
  button_return_.right.x = x_pos_center + kButtonPosX;
  button_return_.left.y = y_pos_center + kButtonPosY;
  button_return_.left.x = button_return_.right.x - button_retry_.size.width;
  button_return_.right.y = button_retry_.left.y + button_retry_.size.height;

  //---------------------------------------------
  // ボタン テキスト
  //---------------------------------------------
  int text_width = GetDrawStringWidthToHandle(kButtonTextRetry, static_cast<int>(strlen(kButtonTextRetry)), font_handle_);
  text_button_retry_.x = (button_retry_.left.x + button_retry_.right.x) / kHalfValue - text_width / kHalfValue;
  text_button_retry_.y = button_retry_.left.y + kButtonTextPosY;
  text_width = GetDrawStringWidthToHandle(kButtonTextReturn, static_cast<int>(strlen(kButtonTextReturn)), font_handle_);
  text_button_return_.x = (button_return_.left.x + button_return_.right.x) / kHalfValue - text_width / kHalfValue;;
  text_button_return_.y = button_return_.left.y + kButtonTextPosY;


  //---------------------------------------------
  // カーソル
  //---------------------------------------------
  cursor_.left.x = static_cast<float>(button_retry_.left.x);
  cursor_.left.y = static_cast<float>(button_retry_.left.y);
  cursor_.right.x = static_cast<float>(cursor_.left.x + kButtonWidth);
  cursor_.right.y = static_cast<float>(cursor_.left.y + kButtonHeight);

  cursor_adjust_.x = kCursorMinusAdjustX;
  cursor_adjust_.y = kCursorMinusAdjustY;


  //---------------------------------------------
  // ボタンの種類
  //---------------------------------------------
  button_color_[static_cast<int>(ButtonType::kRetry)] = kButtonSelectColor;
  button_color_[static_cast<int>(ButtonType::kReturnTitle)] = kButtonNoneColor;

  button_type_ = ButtonType::kRetry;

  return true;
}

/// <summary>
/// 処理中
/// </summary>
/// <param names="process_time"></param>
/// <returns></returns>
void ResultInfoUi::Process(float process_time) {

  // 累積時間に加算
  accumulate_time_ += process_time;
  if (accumulate_time_ >= kWaitTime) {
    // 累積時間をリセット
    accumulate_time_ = kResetTime;

    cursor_.left.x -= cursor_adjust_.x;
    cursor_.left.y -= cursor_adjust_.y;
    cursor_.right.x += cursor_adjust_.x;
    cursor_.right.y += cursor_adjust_.y;

    if (cursor_adjust_.x == kCursorPlusAdjustX) {
      cursor_adjust_.x = kCursorMinusAdjustX;
      cursor_adjust_.y = kCursorMinusAdjustY;
    }
    else {
      cursor_adjust_.x = kCursorPlusAdjustX;
      cursor_adjust_.y = kCursorPlusAdjustY;
    }
  }
}

/// <summary>
/// カーソル操作 左右移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ResultInfoUi::OperationCursorLeftRight() {

  switch (button_type_) {
  case ButtonType::kReturnTitle: {
    button_type_ = ButtonType::kRetry;
    break;
  }
  case ButtonType::kRetry: {
    button_type_ = ButtonType::kReturnTitle;
    break;
  }
  }

  // カーソルを移動する
  OperationCursor();
}

/// <summary>
/// カーソル操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ResultInfoUi::OperationCursor() {

  float x_pos_left = 0.0f;
  float y_pos_left = 0.0f;
  float x_pos_right = 0.0f;
  float y_pos_right = 0.0f;

  switch (button_type_) {
  case ButtonType::kRetry: {
    x_pos_left = static_cast<float>(button_retry_.left.x);
    y_pos_left = static_cast<float>(button_retry_.left.y);
    x_pos_right = static_cast<float>(button_retry_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_retry_.left.y + kButtonHeight);
    break;
  }
  case ButtonType::kReturnTitle: {
    x_pos_left = static_cast<float>(button_return_.left.x);
    y_pos_left = static_cast<float>(button_return_.left.y);
    x_pos_right = static_cast<float>(button_return_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_return_.left.y + kButtonHeight);
    break;
  }
  }

  cursor_.left.x = x_pos_left;
  cursor_.left.y = y_pos_left;
  cursor_.right.x = x_pos_right;
  cursor_.right.y = y_pos_right;

  for (int i = 0; i < static_cast<int>(ButtonType::kTypeMaxIndex); ++i) {
    if (i == static_cast<int>(button_type_)) {
      button_color_[i] = kButtonSelectColor;
    }
    else {
      button_color_[i] = kButtonNoneColor;
    }
  }
}

/// <summary>
/// 透過率の調整
/// </summary>
/// <param names=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool ResultInfoUi::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率に調整値を加算
  switch (current_phase_) {
  case PhaseType::kStartUp: {
    // 起動フェーズ
    alpha_ += kAlphaAdjust;
    break;
  }
  case PhaseType::kBeforeFinalize: {
    // 終了処理前フェーズ
    alpha_ -= kAlphaAdjust;
    break;
  }
  }

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= kAlphaMax) {
    alpha_ = kAlphaMax;
    is_finish = true;
  }
  else if (alpha_ <= kAlphaMin) {
    alpha_ = kAlphaMin;
    is_finish = true;
  }

  return is_finish;
}