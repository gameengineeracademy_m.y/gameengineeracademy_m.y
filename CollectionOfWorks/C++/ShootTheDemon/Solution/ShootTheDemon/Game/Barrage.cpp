﻿#include "Game/Barrage.h"

namespace {

  /// <summary>
  /// 360°弧度法
  /// </summary>
  const float kRoundRadian = static_cast <float>(2 * M_PI);

  /// <summary>
  /// 累積時間のリセット値
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// べき乗
  /// </summary>
  const float kSquared = 2.0f;

  /// <summary>
  /// 角度リセット値
  /// </summary>
  const float kResetAngle = 0.0f;

  /// <summary>
  /// 実行した回数リセット値
  /// </summary>
  const int kResetCount = 0;

  /// <summary>
  /// 目標までの距離が「0」
  /// </summary>
  const float kDistanceToGoal = 0.0f;

  /// <summary>
  /// 余りが0
  /// </summary>
  const int kRemainderZero = 0;

  /// <summary>
  /// 余りが1
  /// </summary>
  const int kRemainderOne = 1;

  /// <summary>
  /// 余りが2
  /// </summary>
  const int kRemainderTwo = 2;

  /// <summary>
  /// 3倍
  /// </summary>
  const float kTriple = 3.0f;

  /// <summary>
  /// 100
  /// </summary>
  const float kHundred = 100.0f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name="barrage_id"> バレッジID </param>
/// <returns></returns>
Barrage::Barrage(BarrageId barrage_id)
  : barrage_id_(barrage_id)
  , barrage_event_interface_(nullptr)
  , character_(nullptr)
  , barrage_data_()
  , executed_count_(0)
  , accumulate_time_(0.0f)
  , is_finish_(false)
  , is_exec_finish_(false) {

  // コンソールに出力
  std::cout << "Barrage コンストラクタ" << std::endl;
}

/// <summary>
/// コピーコンストラクタ
/// </summary>
/// <param name="sound"> コピー元クラス </param>
/// <returns></returns>
Barrage::Barrage(const Barrage& barrage) {

  // コンソールに出力
  std::cout << "Barrage コピーコンストラクタ" << std::endl;

  barrage_id_ = barrage.barrage_id_;
  barrage_event_interface_ = barrage.barrage_event_interface_;
  character_ = barrage.character_;
  barrage_data_ = barrage.barrage_data_;
  executed_count_ = barrage.executed_count_;
  accumulate_time_ = barrage.accumulate_time_;
  is_finish_ = barrage.is_finish_;
  is_exec_finish_ = barrage.is_exec_finish_;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Barrage::~Barrage() {

  // コンソールに出力
  std::cout << "~Barrage デストラクタ" << std::endl;
}

/// <summary>
/// バレッジクローン生成
/// </summary>
/// <param name=""></param>
/// <returns> バレッジ </returns>
Barrage* Barrage::GenerateClone() {

  // コピーコンストラクタを使用して生成
  Barrage* barrage = nullptr;
  barrage = new Barrage(*this);
  if (barrage == nullptr) {
    return nullptr;
  }

  return barrage;
}

/// <summary>
/// 弾幕 全方向
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
bool Barrage::AllDirection(float process_time) {

  if (IsFinish()) {
    return true;
  }

  // 累積時間を加算
  accumulate_time_ += process_time;

  // 初めての処理と継続中の処理と実行時間を分けて管理
  if (executed_count_ == kResetCount) {
    // 生成後、初めての実行
    if (accumulate_time_ < barrage_data_.interval.exec) {
      return false;
    }
    switch (barrage_data_.detail.aim_type) {
    case AimType::kPlayer: {
      float x_pos = 0.0f;
      float y_pos = 0.0f;
      barrage_event_interface_->OnGetPlayerPosition(x_pos, y_pos);
      barrage_data_.pos.x_goal = x_pos;
      barrage_data_.pos.y_goal = y_pos;
      break;
    }
    }
  }
  else {
    // 生成後、2回目以降の実行
    if (accumulate_time_ < barrage_data_.interval.wait) {
      return false;
    }
  }

  // 累積時間をリセット
  accumulate_time_ = kResetTime;

  // 角度初期化
  barrage_data_.angle.value = kResetAngle;
  // 角度の調整値の加算
  barrage_data_.angle.value += barrage_data_.angle.adjust;

  // 角度と分割数
  float angle_adjust = static_cast<float>(barrage_data_.count.deploy);

  for (int i = 0; i < barrage_data_.count.generate; ++i) {
    // 弾の生成と各種設定
    CreateBullet();
    // 角度の設定
    barrage_data_.angle.value += kRoundRadian / angle_adjust;
  }

  // 実行した回数
  ++executed_count_;
  if (executed_count_ >= barrage_data_.count.execute) {
    executed_count_ = kResetCount;
  }

  if (IsExecFinish()) {
    return true;
  }

  return false;
}

/// <summary>
/// 弾幕 回転
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool Barrage::Rotation(float process_time) {

  if (IsFinish()) {
    return true;
  }

  // 累積時間を加算
  accumulate_time_ += process_time;

  // 初めての処理と継続中の処理と実行時間を分けて管理
  if (executed_count_ == kResetCount) {
    // 生成後、初めての実行
    if (accumulate_time_ < barrage_data_.interval.exec) {
      return false;
    }

    // 角度の調整値の加算
    barrage_data_.angle.value += barrage_data_.angle.adjust;

    switch (barrage_data_.detail.aim_type) {
    case AimType::kPlayer: {
      float x_pos = 0.0f;
      float y_pos = 0.0f;
      barrage_event_interface_->OnGetPlayerPosition(x_pos, y_pos);
      barrage_data_.pos.x_goal = x_pos;
      barrage_data_.pos.y_goal = y_pos;
      break;
    }
    }
  }
  else {
    // 生成後、2回目以降の実行
    if (accumulate_time_ < barrage_data_.interval.wait) {
      return false;
    }
  }

  // 累積時間をリセット
  accumulate_time_ = kResetTime;
  // 分割数
  float angle_adjust = static_cast<float>(barrage_data_.count.deploy);

  for (int i = 0; i < barrage_data_.count.generate; ++i) {
    // 弾の生成と各種設定
    CreateBullet();
    // 角度の設定
    barrage_data_.angle.value += kRoundRadian / angle_adjust;
  }

  // 実行した回数
  ++executed_count_;
  if (executed_count_ >= barrage_data_.count.execute) {
    executed_count_ = kResetCount;
  }

  if (IsExecFinish()) {
    return true;
  }

  return false;
}

/// <summary>
/// 弾幕 連射
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool Barrage::ContinuousShoot(float process_time) {

  if (IsFinish()) {
    return true;
  }

  // 累積時間を加算
  accumulate_time_ += process_time;

  // 初めての処理と継続中の処理と実行時間を分けて管理
  if (executed_count_ == kResetCount) {
    // 生成後、初めての実行
    if (accumulate_time_ < barrage_data_.interval.exec) {
      return false;
    }

    // 角度の調整値の加算
    barrage_data_.angle.value += barrage_data_.angle.adjust;

    switch (barrage_data_.detail.aim_type) {
    case AimType::kPlayer: {
      float x_pos = 0.0f;
      float y_pos = 0.0f;
      barrage_event_interface_->OnGetPlayerPosition(x_pos, y_pos);
      barrage_data_.pos.x_goal = x_pos;
      barrage_data_.pos.y_goal = y_pos;
      break;
    }
    }
  }
  else {
    // 生成後、2回目以降の実行
    if (accumulate_time_ < barrage_data_.interval.wait) {
      return false;
    }
  }

  // 累積時間をリセット
  accumulate_time_ = kResetTime;

  // 角度の設定
  barrage_data_.angle.value = kResetAngle;

  // 弾の生成と各種設定
  CreateBullet();

  // 実行した回数
  ++executed_count_;
  if (executed_count_ >= barrage_data_.count.execute) {
    executed_count_ = kResetCount;
  }

  if (IsExecFinish()) {
    return true;
  }

  return false;
}

/// <summary>
/// 弾の生成と移動量の計算
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Barrage::CreateBullet() {

  // 目標座標
  float x_pos_goal = 0.0f;
  float y_pos_goal = 0.0f;
  // 配置座標
  float x_pos_disp = 0.0f;
  float y_pos_disp = 0.0f;
  // 始点と終点の距離
  float distance_x = barrage_data_.pos.x_goal - barrage_data_.pos.x;
  float distance_y = barrage_data_.pos.y_goal - barrage_data_.pos.y;
  float distance = std::sqrtf(std::powf(distance_x, kSquared) + std::powf(distance_y, kSquared));
  float deploy_distance = std::sqrtf(std::powf(barrage_data_.distance.x, kSquared) + std::powf(barrage_data_.distance.y, kSquared));
  // 移動量
  float move_x = 0.0f;
  float move_y = 0.0f;

  // 弾を生成
  Bullet* bullet = barrage_event_interface_->OnCreateBullet(barrage_data_.detail.bullet_id);
  if (bullet == nullptr) {
    return;
  }

  // 角度の取得
  float angle = barrage_data_.angle.value;

  if (distance == kDistanceToGoal) {
    // 中心から表示座標までを半径とした円
    x_pos_disp = barrage_data_.distance.x * cosf(angle) - barrage_data_.distance.y * sinf(angle);
    y_pos_disp = barrage_data_.distance.x * sinf(angle) + barrage_data_.distance.y * cosf(angle);

    bullet->SetPositionX(barrage_data_.pos.x + x_pos_disp);
    bullet->SetPositionY(barrage_data_.pos.y + y_pos_disp);

    x_pos_disp = barrage_data_.pos.x + x_pos_disp;
    y_pos_disp = barrage_data_.pos.y + y_pos_disp;
    x_pos_goal = barrage_data_.pos.x_goal;
    y_pos_goal = barrage_data_.pos.y_goal;
    // 移動量
    move_x = barrage_data_.rate.move * (x_pos_goal - x_pos_disp) / deploy_distance;
    move_y = barrage_data_.rate.move * (y_pos_goal - y_pos_disp) / deploy_distance;
  }
  else {
    // 目標座標までを半径とした円
    x_pos_goal = distance_x * cosf(angle) - distance_y * sinf(angle);
    y_pos_goal = distance_x * sinf(angle) + distance_y * cosf(angle);
    // 初期配置
    x_pos_disp = deploy_distance / distance * x_pos_goal;
    y_pos_disp = deploy_distance / distance * y_pos_goal;

    bullet->SetPositionX(barrage_data_.pos.x + x_pos_disp);
    bullet->SetPositionY(barrage_data_.pos.y + y_pos_disp);
    // 移動量
    move_x = barrage_data_.rate.move * x_pos_goal / distance;
    move_y = barrage_data_.rate.move * y_pos_goal / distance;
  }

  // 加速度
  float accele_x = barrage_data_.rate.acce_x * move_x / kHundred;
  float accele_y = barrage_data_.rate.acce_y * move_y / kHundred;
  // 速度の最大値、最小値
  float speed_x_max = move_x + barrage_data_.speed.x_max * accele_x;
  float speed_y_max = move_y + barrage_data_.speed.y_max * accele_y;
  float speed_x_min = move_x + barrage_data_.speed.x_min * accele_x;
  float speed_y_min = move_y + barrage_data_.speed.y_min * accele_y;

  bullet->SetSpeed(move_x, move_y);
  bullet->SetSpeedMax(speed_x_max, speed_y_max);
  bullet->SetSpeedMin(speed_x_min, speed_y_min);
  bullet->SetCharacter(character_);
  bullet->SetExtRate(barrage_data_.expa.value, barrage_data_.expa.adjust,
                     barrage_data_.expa.max, barrage_data_.expa.min);
  bullet->SetAlpha(barrage_data_.alpha.value, barrage_data_.alpha.adjust,
                   barrage_data_.alpha.max, barrage_data_.alpha.min);
  bullet->SetAcceleration(accele_x, accele_y);
  bullet->SetWaitTime(barrage_data_.time.wait_start, barrage_data_.time.wait_accel,
                      barrage_data_.time.wait_expan);
  if (barrage_data_.time.wait_start == kResetTime) {
    bullet->SetMoveStart(true);
  }
}

/// <summary>
/// 弾幕 単発射撃
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool Barrage::SingleShoot(float process_time) {

  // 調整座標
  float x_pos_disp = 0.0f;
  float y_pos_disp = 0.0f;

  // 弾を生成
  Bullet* bullet = barrage_event_interface_->OnCreateBullet(barrage_data_.detail.bullet_id);
  if (bullet == nullptr) {
    return false;
  }

  // 初期配置
  x_pos_disp = barrage_data_.distance.x;
  y_pos_disp = barrage_data_.distance.y;

  bullet->SetPositionX(barrage_data_.pos.x + x_pos_disp);
  bullet->SetPositionY(barrage_data_.pos.y + y_pos_disp);
  bullet->SetCharacter(character_);
  bullet->SetMoveStart(true);

  return true;
}

/// <summary>
/// 弾幕 2倍射撃
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool Barrage::DoubleShoot(float process_time) {

  for (int i = 0; i < barrage_data_.count.generate; ++i) {
    // 弾を生成
    Bullet* bullet = barrage_event_interface_->OnCreateBullet(barrage_data_.detail.bullet_id);
    if (bullet == nullptr) {
      return false;
    }

    // 調整座標
    float x_pos_disp = 0.0f;
    float y_pos_disp = 0.0f;

    // 現在のインデックスを生成個数で割った余りの数から初期配置の調整
    if (i % barrage_data_.count.generate == kRemainderZero) {
      x_pos_disp -= barrage_data_.distance.x;
      y_pos_disp = barrage_data_.distance.y;
    }
    else {
      x_pos_disp += barrage_data_.distance.x;
      y_pos_disp = barrage_data_.distance.y;
    }

    bullet->SetPositionX(barrage_data_.pos.x + x_pos_disp);
    bullet->SetPositionY(barrage_data_.pos.y + y_pos_disp);
    bullet->SetCharacter(character_);
    bullet->SetMoveStart(true);
  }

  return true;
}

/// <summary>
/// 弾幕 3倍射撃
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool Barrage::TripleShoot(float process_time) {

  for (int i = 0; i < barrage_data_.count.generate; ++i) {
    // 弾を生成
    Bullet* bullet = barrage_event_interface_->OnCreateBullet(barrage_data_.detail.bullet_id);
    if (bullet == nullptr) {
      return false;
    }

    // 調整座標
    float x_pos_disp = 0.0f;
    float y_pos_disp = 0.0f;

    // 現在のインデックスを生成個数で割った余りの数から初期配置の調整
    if (i % barrage_data_.count.generate == kRemainderZero) {
      x_pos_disp -= barrage_data_.distance.x;
      y_pos_disp = barrage_data_.distance.y;
    }
    else if (i % barrage_data_.count.generate == kRemainderOne) {
      y_pos_disp = barrage_data_.distance.y;
    }
    else {
      x_pos_disp += barrage_data_.distance.x;
      y_pos_disp = barrage_data_.distance.y;
    }

    bullet->SetPositionX(barrage_data_.pos.x + x_pos_disp);
    bullet->SetPositionY(barrage_data_.pos.y + y_pos_disp);
    bullet->SetCharacter(character_);
    bullet->SetMoveStart(true);
  }

  return true;
}

/// <summary>
/// 弾幕 4倍射撃
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool Barrage::QuardShoot(float process_time) {

  for (int i = 0; i < barrage_data_.count.generate; ++i) {
    // 弾を生成
    Bullet* bullet = barrage_event_interface_->OnCreateBullet(barrage_data_.detail.bullet_id);
    if (bullet == nullptr) {
      return false;
    }

    // 調整座標
    float x_pos_disp = 0.0f;
    float y_pos_disp = 0.0f;

    // 現在のインデックスを生成個数で割った余りの数から初期配置の調整
    if (i % barrage_data_.count.generate == kRemainderZero) {
      x_pos_disp -= kTriple * barrage_data_.distance.x;
      y_pos_disp = barrage_data_.distance.y;
    }
    else if (i % barrage_data_.count.generate == kRemainderOne) {
      x_pos_disp -= barrage_data_.distance.x;
      y_pos_disp = barrage_data_.distance.y;
    }
    else if (i % barrage_data_.count.generate == kRemainderTwo) {
      x_pos_disp += barrage_data_.distance.x;
      y_pos_disp = barrage_data_.distance.y;
    }
    else {
      x_pos_disp += kTriple * barrage_data_.distance.x;
      y_pos_disp = barrage_data_.distance.y;
    }

    bullet->SetPositionX(barrage_data_.pos.x + x_pos_disp);
    bullet->SetPositionY(barrage_data_.pos.y + y_pos_disp);
    bullet->SetCharacter(character_);
    bullet->SetMoveStart(true);
  }

  return true;
}