﻿#include "Game/HitPointBar.h"

namespace {

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjustInit = -20;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 符号反転
  /// </summary>
  const int kMinusSign = -1;

  /// <summary>
  /// 半分
  /// </summary>
  const float kHalfValue = 2.0f;

  /// <summary>
  /// ボスのフレームサイズ 幅
  /// </summary>
  const float kBossFrameSizeX = 476.0f;

  /// <summary>
  /// ボスのフレームサイズ 高さ
  /// </summary>
  const float kBossFrameSizeY = 20.0f;

  /// <summary>
  /// ボスのフレーム位置 画面中心からの高さ
  /// </summary>
  const float kBossFrameHeight = 364.0f;

  /// <summary>
  /// 敵のフレームサイズ 幅
  /// </summary>
  const float kEnemyFrameSizeX = 50.0f;

  /// <summary>
  /// 敵のフレームサイズ 高さ
  /// </summary>
  const float kEnemyFrameSizeY = 3.0f;

  /// <summary>
  /// HPバー 幅・高さ調整値
  /// </summary>
  const float kHpBarAdjust = 1.0f;

  /// <summary>
  /// HPバー上昇調整値
  /// </summary>
  const float kHpBarAdjustRate = 47.5f;

  /// <summary>
  /// 25%
  /// </summary>
  const float kOneEighth = 0.125f;

  /// <summary>
  /// 8分割
  /// </summary>
  const float kEightDivision = 8;

  /// <summary>
  /// 4倍
  /// </summary>
  const float kFourTimes = 4.0f;

  /// <summary>
  /// 2倍
  /// </summary>
  const float kTwoTimes = 2.0f;

  /// <summary>
  /// フレームの背景の色
  /// </summary>
  const int kFrameBackColor = GetColor(50, 50, 50);

  /// <summary>
  /// フレームの色 水色
  /// </summary>
  const int kFrameColor = GetColor(0, 120, 255);

  /// <summary>
  /// バーの色 緑
  /// </summary>
  const int kBarColorGreen = GetColor(0, 255, 0);

  /// <summary>
  /// バーの色 黄
  /// </summary>
  const int kBarColorYellow = GetColor(255, 255, 0);

  /// <summary>
  /// バーの色 赤
  /// </summary>
  const int kBarColorRed = GetColor(255, 0, 0);

  /// <summary>
  /// フレーム調整値
  /// </summary>
  const float kBarFrame = 3.0f;

  /// <summary>
  /// 揺らし量 初期値
  /// </summary>
  const int kShakeInitValue = 0;

  /// <summary>
  /// 揺らしインデックス 初期値
  /// </summary>
  const int kShakeInitIndex = 0;

  /// <summary>
  /// 待機時間
  /// </summary>
  const float kWaitTime = 2.0f;

  /// <summary>
  /// リセット時間
  /// </summary>
  const float kResetTime = 0.0f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name="hit_point_bar_id"> ヒットポイントバーID </param>
/// <returns></returns>
HitPointBar::HitPointBar(HitPointBarId hit_point_bar_id)
  : current_phase_(PhaseType::kStartWait)
  , shake_phase_(ShakePhase::kNone)
  , hit_point_bar_id_(hit_point_bar_id)
  , character_(nullptr)
  , frame_()
  , hp_bar_()
  , list_id_(0)
  , hp_bar_adjust_(0.0f)
  , hp_bar_color_(kBarColorGreen)
  , width_one_eighth_(0.0f)
  , hit_point_(0)
  , shake_value_()
  , shake_index_(0)
  , alpha_(kAlphaMax)
  , alpha_adjust_(kAlphaAdjustInit)
  , accumulate_time_(0.0f)
  , is_finish_(false) {

  // コンソールに出力
  std::cout << "HitPointBar コンストラクタ" << std::endl;
}

/// <summary>
/// コピーコンストラクタ
/// </summary>
/// <param name=""> コピー元クラス </param>
/// <returns></returns>
HitPointBar::HitPointBar(const HitPointBar& hp_bar) {

  // コンソールに出力
  std::cout << "HitPointBar コピーコンストラクタ" << std::endl;

  current_phase_ = hp_bar.current_phase_;
  shake_phase_ = hp_bar.shake_phase_;
  hit_point_bar_id_ = hp_bar.hit_point_bar_id_;
  character_ = hp_bar.character_;
  frame_ = hp_bar.frame_;
  hp_bar_ = hp_bar.hp_bar_;
  list_id_ = hp_bar.list_id_;
  hp_bar_adjust_ = hp_bar.hp_bar_adjust_;
  hp_bar_color_ = hp_bar.hp_bar_color_;
  width_one_eighth_ = hp_bar.width_one_eighth_;
  hit_point_ = hp_bar.hit_point_;
  shake_value_ = hp_bar.shake_value_;
  shake_index_ = hp_bar.shake_index_;
  alpha_ = hp_bar.alpha_;
  alpha_adjust_ = hp_bar.alpha_adjust_;
  is_finish_ = hp_bar.is_finish_;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
HitPointBar::~HitPointBar() {

  // コンソールに出力
  std::cout << "~HitPointBar デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool HitPointBar::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kStartWait: {
    //------------------------------
    //起動待機フェーズ
    //------------------------------
    // 現在のフェーズを「準備」に変更
    ChangeCurrentPhase(PhaseType::kPrepare);
    break;
  }
  case PhaseType::kPrepare: {
    //------------------------------
    //準備フェーズ
    //------------------------------
    bool is_finish = Prepare();
    if (is_finish) {
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //------------------------------
    //処理フェーズ
    //------------------------------
    bool is_finish = Process(process_time);
    if (is_finish) {
      // 現在のフェーズを「終了処理前」に変更
      ChangeCurrentPhase(PhaseType::kFinalizeBefore);
    }
    break;
  }
  case PhaseType::kFinalizeBefore: {
    //------------------------------
    //終了処理前フェーズ
    //------------------------------
    // フェードアウト処理完了後、現在のフェーズを「終了処理」に変更
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //------------------------------
    //終了処理フェーズ
    //------------------------------
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }

  return false;
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void HitPointBar::Render() {

  // 起動待機フェーズ中は処理終了
  if (current_phase_ == PhaseType::kStartWait) {
    return;
  }

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

  // フレームの枠
  DrawBoxAA(frame_.left.x + frame_.shake.x - kBarFrame, frame_.left.y + frame_.shake.y - kBarFrame,
            frame_.right.x + frame_.shake.x + kBarFrame, frame_.right.y + frame_.shake.y + kBarFrame, kFrameColor, true);
  // フレームの背景
  DrawBoxAA(frame_.left.x + frame_.shake.x, frame_.left.y + frame_.shake.y,
            frame_.right.x + frame_.shake.x, frame_.right.y + frame_.shake.y, kFrameBackColor, true);
  // HPバー
  DrawBoxAA(hp_bar_.left.x, hp_bar_.left.y, hp_bar_.right.x, hp_bar_.right.y, hp_bar_color_, true);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
}

/// <summary>
/// 開始準備処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool HitPointBar::Prepare() {

  hp_bar_.right.x += hp_bar_adjust_;
  if (hp_bar_.right.x >= frame_.right.x) {
    hp_bar_.right.x = frame_.right.x;
    return true;
  }

  return false;
}

/// <summary>
/// 処理中
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool HitPointBar::Process(float process_time) {

  // 終了フラグが立っているなら処理終了
  if (IsFinish()) {
    return true;
  }

  // 画面揺らし量を設定
  switch (shake_phase_) {
  case ShakePhase::kExecute: {

    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kWaitTime) {
      // 累積時間をリセットする
      accumulate_time_ = kResetTime;
      // 画面揺らしを止める
      StopScreenShake();
      break;
    }

    frame_.shake.x = shake_value_.at(shake_index_).x;
    frame_.shake.y = shake_value_.at(shake_index_).y;
    ++shake_index_;
    if (shake_index_ >= static_cast<int>(shake_value_.size())) {
      shake_index_ = kShakeInitIndex;
    }
    break;
  }
  case ShakePhase::kNone: {
    frame_.shake.x = kShakeInitValue;
    frame_.shake.y = kShakeInitValue;
    break;
  }
  }

  switch (hit_point_bar_id_) {
  case HitPointBarId::kEnemy: {
    //-------------------------------------------
    //バーの表示の調整
    //-------------------------------------------
    // キャラクターの表示座標と画像の半分のサイズを取得
    // 取得した値から座標をセットする
    float x_pos = character_->GetFloatPositionX();
    float y_pos = character_->GetFloatPositionY();
    float half_height = static_cast<float>(character_->GetImageHalfHeight());
    SetPosition(x_pos, y_pos, half_height);
    break;
  }
  }

  //-------------------------------------------
  //ヒットポイントバーの調整
  //-------------------------------------------
  // キャラクターのHPを取得
  // 1フレームの前のHPと比較し、異なっていたら処理実行
  int hit_point = character_->GetHitPoint();
  if (hit_point == hit_point_) {
    return false;
  }
  // 現在のHPを1フレーム前のHPにセットする
  hit_point_ = hit_point;

  // キャラクターの最大HPを取得し、HPの残量を割合で求める
  int hit_point_max = character_->GetInitialHitPoint();
  float hit_point_rate = static_cast<float>(hit_point) / static_cast<float>(hit_point_max);
  // 敵キャラのHPの割合からバーの長さを変更する
  float hp_bar = frame_.size.width * hit_point_rate;
  hp_bar_.right.x = hp_bar_.left.x + hp_bar;

  // 残HPが最大値の25%以下の場合
  if (hp_bar <= kTwoTimes * width_one_eighth_) {
    hp_bar_color_ = kBarColorRed;
  }
  // 残HPが最大値の50%以下の場合
  else if (hp_bar <= kFourTimes * width_one_eighth_) {
    hp_bar_color_ = kBarColorYellow;
  }
  // それ以外
  else {
    hp_bar_color_ = kBarColorGreen;
  }

  return false;
}

/// <summary>
/// HPバークローン生成
/// </summary>
/// <param name=""></param>
/// <returns> HPバー </returns>
HitPointBar* HitPointBar::GenerateClone() {

  // コピーコンストラクタを使用して生成
  HitPointBar* hp_bar = nullptr;
  hp_bar = new HitPointBar(*this);
  if (hp_bar == nullptr) {
    return nullptr;
  }

  return hp_bar;
}

/// <summary>
/// サイズのセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void HitPointBar::SetSize() {

  switch (hit_point_bar_id_) {
  case HitPointBarId::kBoss: {
    //--------------------------------
    //ボス
    //--------------------------------
    frame_.size.width = kBossFrameSizeX;
    frame_.size.height = kBossFrameSizeY;
    break;
  }
  case HitPointBarId::kEnemy: {
    //--------------------------------
    //敵(雑魚)
    //--------------------------------
    frame_.size.width = kEnemyFrameSizeX;
    frame_.size.height = kEnemyFrameSizeY;
    break;
  }
  }

  // 調整割合をセット
  hp_bar_adjust_ = frame_.size.width / kHpBarAdjustRate;
}

/// <summary>
/// 画面を揺らす量のセット
/// </summary>
/// <param name="shake_x"> X方向 </param>
/// <param name="shake_y"> Y方向 </param>
/// <returns></returns>
void HitPointBar::SetShakeValue(float shake_x, float shake_y) {

  Pos shake_value;
  shake_value.x = shake_x;
  shake_value.y = shake_y;

  shake_value_.push_back(shake_value);
}

/// <summary>
/// ボスキャラの初期表示座標のセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void HitPointBar::SetInitialBossPosition() {

  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // 画面の中央座標を取得
  float x_pos_center = static_cast<float>(game_info->GetCenterPointX());
  float y_pos_center = static_cast<float>(game_info->GetCenterPointY());

  // フレームの初期設定
  frame_.left.x = x_pos_center - (kBossFrameSizeX / kHalfValue);
  frame_.left.y = y_pos_center - kBossFrameHeight;
  frame_.right.x = x_pos_center + (kBossFrameSizeX / kHalfValue);
  frame_.right.y = frame_.left.y + kBossFrameSizeY;
  // HPバーの初期設定
  hp_bar_.left.x = x_pos_center - (kBossFrameSizeX / kHalfValue);
  hp_bar_.left.y = y_pos_center - kBossFrameHeight ;
  hp_bar_.right.x = hp_bar_.left.x;
  hp_bar_.right.y = hp_bar_.left.y + kBossFrameSizeY;
  // フレームの幅の1/8をセット
  width_one_eighth_ = kBossFrameSizeX * kOneEighth;
}

/// <summary>
/// 表示座標のセット
/// </summary>
/// <param name="x_pos"> X座標 </param>
/// <param name="y_pos"> Y座標 </param>
/// <param name="half_height"> 画像の半分の高さ </param>
/// <returns></returns>
void HitPointBar::SetPosition(float x_pos, float y_pos, float half_height) {

  float size_x = 0.0f;
  float size_y = 0.0f;
  float adjust = 0.0f;

  // フレームの初期設定
  frame_.left.x = x_pos - (kEnemyFrameSizeX / kHalfValue);
  frame_.left.y = y_pos - (half_height + kEnemyFrameSizeY);
  frame_.right.x = x_pos + (kEnemyFrameSizeX / kHalfValue);
  frame_.right.y = y_pos - half_height;
  // HPバーの初期設定
  hp_bar_.left.x = x_pos - (kEnemyFrameSizeX / kHalfValue);
  hp_bar_.left.y = y_pos - (half_height + kEnemyFrameSizeY);
  hp_bar_.right.y = y_pos - half_height;
}

/// <summary>
/// HPバーの初期値セット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void HitPointBar::SetInitialHpBar() {

  hp_bar_.right.x = hp_bar_.left.x;
}

/// <summary>
/// 透過率変更処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:継続処理中 </returns>
bool HitPointBar::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率に調整値を加算
  alpha_ += alpha_adjust_;
  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= kAlphaMax) {
    alpha_ = kAlphaMax;
    is_finish = true;
  }
  else if (alpha_ <= kAlphaMin) {
    alpha_ = kAlphaMin;
    is_finish = true;
  }

  return is_finish;
}

/// <summary>
/// 画面揺らしを開始する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void HitPointBar::StartScreenShake() {

  // 画面揺らしフェーズを「実行」に開始
  shake_phase_ = ShakePhase::kExecute;
  // インデックスを初期化する
  shake_index_ = kShakeInitIndex;
}

/// <summary>
/// 画面揺らしを停止する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void HitPointBar::StopScreenShake() {

  // 画面揺らしフェーズを「何もしない」に開始
  shake_phase_ = ShakePhase::kNone;
  // インデックスを初期化する
  shake_index_ = kShakeInitIndex;
}