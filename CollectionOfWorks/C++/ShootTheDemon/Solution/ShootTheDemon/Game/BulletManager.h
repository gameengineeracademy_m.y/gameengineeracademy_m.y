﻿#pragma once

#include "System/Task.h"
#include "Game/Bullet.h"
#include <unordered_map>
#include <vector>

/// <summary>
/// バレットマネージャ
/// </summary>
class BulletManager : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kProcess,          // 処理中フェーズ
    kStop,             // 停止中
    kBeforeFinalize,   // 終了処理前フェーズ
    kFinalize,         // 終了処理フェーズ
    kPhaseMaxIndex     // フェーズ数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  BulletManager();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~BulletManager();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// 弾の生成
  /// </summary>
  /// <param name=""> バレットID </param>
  /// <returns> バレット </returns>
  Bullet* CreateBullet(BulletId);

  /// <summary>
  /// 弾の描画を終了する
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  void FinishBullet(Character*);

  /// <summary>
  /// 弾の破棄
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DisposeBullet();

  /// <summary>
  /// 透過率をセットする
  /// </summary>
  /// <param name=""> 透過率 </param>
  /// <returns></returns>
  void SetAlpha(int);

  /// <summary>
  /// 透過率の調整値をセットする
  /// </summary>
  /// <param name=""> 透過率 調整値 </param>
  /// <returns></returns>
  void SetAlphaAdjust(int);

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }


private:

  /// <summary>
  /// 透過率変更処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AdjustAlphaValue();


private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 弾のリスト
  /// </summary>
  std::unordered_map<BulletId, Bullet*> bullet_list_;

  /// <summary>
  /// 生成した弾のリスト
  /// </summary>
  std::unordered_map<int, Bullet*> generate_bullet_list_;

  /// <summary>
  /// 破棄する弾のIDリスト
  /// </summary>
  std::vector<int> release_id_list_;

  /// <summary>
  /// リストID 最大値
  /// </summary>
  int list_id_max_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  int alpha_adjust_;
};