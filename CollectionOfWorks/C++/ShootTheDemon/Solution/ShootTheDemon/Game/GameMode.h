﻿#pragma once

#include "Dxlib.h"
#include "System/Task.h"
#include "System/FontInfo.h"
#include <string>
#include <vector>

namespace {

  /// <summary>
  /// 残機がゼロ
  /// </summary>
  const int kLeftLifeZero = 0;

  /// <summary>
  /// 残機最大値
  /// </summary>
  const int kLeftLifeMax = 99;

  /// <summary>
  /// 射撃レベル最大値
  /// </summary>
  const int kShootLevelMax = 4;
}

/// <summary>
/// ゲームモード
/// </summary>
/// <remarks>
/// 主にゲームの管理を行うクラス
/// </remarks>
class GameMode : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,          // 初期化処理フェーズ
    kStartWait,           // 起動待機フェーズ
    kStartUp,             // 起動フェーズ
    kPlay,                // プレイ中フェーズ
    kBeforeFinalize,      // 終了処理前フェーズ
    kFinalize,            // 終了処理フェーズ
    kFinalized,           // 終了処理済みフェーズ
    kPhaseMaxIndex        // フェーズ数
  };

  /// <summary>
  /// ゲームデータ
  /// </summary>
  struct GameData {
    int left_life;     // 残機
    int level_shoot;   // 射撃レベル
    bool is_use_item;  // アイテム使用可否
  };

  /// <summary>
  /// フォントサイズの種類
  /// </summary>
  enum class FontSize {
    k25,               // サイズ25
    k30,               // サイズ30
    kSizeMaxIndex      // 種類数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  GameMode();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~GameMode();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// 残機を1つ増やす
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void IncreaseLeftLife() { ++game_data_.left_life; }

  /// <summary>
  /// 残機を1つ減らす
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DecreaseLeftLife() { --game_data_.left_life; }

  /// <summary>
  /// 残機を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 残機 </returns>
  int GetLeftLife() { return game_data_.left_life; }

  /// <summary>
  /// 残機がゼロかどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:残機がゼロ, false:ゼロ以外 </returns>
  bool IsLeftLifeZero() { return game_data_.left_life == kLeftLifeZero; }

  /// <summary>
  /// 残機が最大値かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:残機が最大値, false:最大値以外 </returns>
  bool IsLeftLifeMax() { return game_data_.left_life == kLeftLifeMax; }

  /// <summary>
  /// 射撃レベルを1つ上げる
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void RaiseShootLevel();

  /// <summary>
  /// 射撃レベルを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 射撃レベル </returns>
  int GetShootLevel() { return game_data_.level_shoot; }

  /// <summary>
  /// 射撃レベルが最大値がどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:射撃レベルが最大値, false:最大値以外 </returns>
  bool IsShootLevelMax() { return game_data_.level_shoot == kShootLevelMax; }

  /// <summary>
  /// アイテムの使用有無
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:アイテムを使用, false:アイテムを不使用 </returns>
  bool IsUseItem() { return game_data_.is_use_item; }

  /// <summary>
  /// ゲームデータをセットする
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:射撃レベルが最大値, false:最大値以外 </returns>
  void SetGameData(GameData game_data) { game_data_ = game_data; }

  /// <summary>
  /// グラフィックハンドルをセット
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandleLife(int handle) { handle_life_ = handle; }

  /// <summary>
  /// グラフィックハンドルを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  int GetGraphicHandleLife() { return handle_life_; };

  /// <summary>
  /// グラフィックハンドルをセット
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandlePowerUp(int handle) { handle_power_up_ = handle; }

  /// <summary>
  /// グラフィックハンドルを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  int GetGraphicHandlePowerUp() { return handle_power_up_; };

  /// <summary>
  /// 現在のフェーズを変更
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Initialize();

  /// <summary>
  /// 処理中
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Process();

  /// <summary>
  /// 透過率変更処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AdjustAlphaValue();

  /// <summary>
  /// 透過率の符号を変更
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeAlphaAdjustSign();

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// ゲームデータ
  /// </summary>
  GameData game_data_;

  /// <summary>
  /// グラフィックハンドル 残機
  /// </summary>
  int handle_life_;

  /// <summary>
  /// グラフィックハンドル パワーUP
  /// </summary>
  int handle_power_up_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  int alpha_adjust_;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  int alpha_max_;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  int alpha_min_;

  /// <summary>
  /// 透過率 背景
  /// </summary>
  int alpha_screen_;

  /// <summary>
  /// フォントハンドル
  /// </summary>
  std::vector<int> font_handle_;
};