﻿#pragma once

#include "Game/ItemBase.h"

/// <summary>
/// 残機UPアイテム
/// </summary>
class ItemExtend : public ItemBase {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,       // 初期化フェーズ
    kStartUp,          // 起動フェーズ
    kProcess,          // 処理中フェーズ
    kStop,             // 停止フェーズ
    kBeforeFinalize,   // 終了処理前フェーズ
    kFinalize,         // 終了処理フェーズ
    kFinalized,        // 終了処理済みフェーズ
    kPhaseMaxIndex     // フェーズ数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> キャラクターコントローライベントインターフェース </param>
  /// <param name=""> アイテムイベントインターフェース </param>
  /// <param name=""> キャラクターイベントインターフェース </param>
  /// <returns></returns>
  ItemExtend(CharacterControllerEventInterface&, ItemEventInterface&,
             CharacterEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~ItemExtend();

  /// <summary>
  /// 毎フレームの更新処理
  /// </summary>
  /// <param names=""> 処理時間 </param>
  /// <returns>  </returns>
  void Update(float) override;

  /// <summary>
  /// 毎フレームの描画処理
  /// </summary>
  /// <param names=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// フェーズの種類を変更する
  /// </summary>
  /// <param names="phase_type"> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; };

  /// <summary>
  /// 現在のフェーズが終了処理済みか確認
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理済み, false:終了処理済み以外 </returns>
  bool IsFinalized() { return current_phase_ == PhaseType::kFinalized; }


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param names=""></param>
  /// <returns></returns>
  void Initialize();

  /// <summary>
  /// 処理中
  /// </summary>
  /// <param names=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Process();


private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;
};