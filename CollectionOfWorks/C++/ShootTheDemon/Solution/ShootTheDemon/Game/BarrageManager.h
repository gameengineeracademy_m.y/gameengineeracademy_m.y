﻿#pragma once

#include "System/Task.h"
#include "System/Character.h"
#include "Game/Barrage.h"
#include <unordered_map>

/// <summary>
/// バレッジマネージャ
/// </summary>
class BarrageManager : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kPlay,           // プレイ中
    kStop,           // 停止中
    kPhaseMaxIndex   // フェーズ数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  BarrageManager();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~BarrageManager();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 弾幕(バレッジ)のリスト登録
  /// </summary>
  /// <param name=""> バレッジID </param>
  /// <returns> バレッジ </returns>
  Barrage* RegisterBarrage(BarrageId);

  /// <summary>
  /// 弾幕の初期配置
  /// </summary>
  /// <param name=""> バレッジID </param>
  /// <returns> バレッジ </returns>
  Barrage* DeployBarrageBullet(BarrageId);

  /// <summary>
  /// 生成した弾幕をすべて終了させる
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void FinishGenerateBarrage();

  /// <summary>
  /// 生成した弾幕の破棄
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DisposeGenerateBarrage();

  /// <summary>
  /// 弾幕リストの破棄
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DisposeBarrageList();

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }


private:

  /// <summary>
  /// 弾幕のリスト
  /// </summary>
  std::unordered_map<BarrageId, Barrage*> barrage_list_;

  /// <summary>
  /// 生成した弾幕のリスト
  /// </summary>
  std::unordered_map<int, Barrage*> generate_barrage_list_;

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 生成リストID 現在の最大値
  /// </summary>
  int list_id_max_;
};