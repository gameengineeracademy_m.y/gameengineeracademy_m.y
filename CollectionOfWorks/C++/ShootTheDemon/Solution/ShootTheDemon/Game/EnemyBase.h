﻿#pragma once

#include "System/Character.h"
#include "Game/EnemyEventInterface.h"
#include "Game/EnemyController.h"
#include "Game/BarrageId.h"
#include <vector>

/// <summary>
/// エネミーベース
/// </summary>
/// <remarks>
/// 敵キャラの共通処理を扱うクラス <br/>
/// キャラクタークラスを継承
/// </remarks>
class EnemyBase : public Character {
public:

  /// <summary>
  /// フェーズごとのヒットポイント
  /// </summary>
  struct HitPoint {
    int phase_first;
    int phase_second;
    int phase_third;
    int phase_fourth;
    int phase_fifth;
    int phase_sixth;
    int phase_seventh;
    int phase_eighth;
  };

public:

  /// <summary>
  /// コンストラクタ  初期化処理
  /// </summary>
  /// <param names=""> タスクID </param>
  /// <param names=""> キャラクターの種類 </param>
  /// <param names=""> エネミーイベントインターフェイス </param>
  /// <param names=""> キャラクターコントローライベントインターフェイス </param>
  /// <param names=""> キャラクターイベントインターフェイス </param>
  /// <returns></returns>
  EnemyBase(TaskId, CharacterType, EnemyEventInterface&,
            CharacterControllerEventInterface&,
            CharacterEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param names=""></param>
  /// <returns></returns>
  virtual ~EnemyBase();

  /// <summary>
  /// グラフィックハンドルをセットする
  /// </summary>
  /// <param names=""> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandle(int graphic_handle) { graphic_handle_ = graphic_handle; }

  /// <summary>
  /// グラフィックハンドルを取得する
  /// </summary>
  /// <param names=""><param>
  /// <returns></returns>
  int GetGraphicHandle() { return graphic_handle_; }

  /// <summary>
  /// 毎フレームの描画処理
  /// </summary>
  /// <param names=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// 移動できる向きリストをセット
  /// </summary>
  /// <param names=""> 移動できる向きの配列 </param>
  /// <returns></returns>
  void SetMoveDirection(std::vector<DirectionType>);

  /// <summary>
  /// 始点のX座標の設定
  /// </summary>
  /// <param names="x_pos"> X座標 </param>
  /// <returns></returns>
  void SetStartPositionX(int x_pos) { x_pos_start_ = x_pos; }

  /// <summary>
  /// 始点のY座標の設定
  /// </summary>
  /// <param names="y_pos"> Y座標 </param>
  /// <returns></returns>
  void SetStartPositionY(int y_pos) { y_pos_start_ = y_pos; }

  /// <summary>
  /// 始点のX座標を取得
  /// </summary>
  /// <param names=""></param>
  /// <returns> X座標 </returns>
  int GetStartPositionX() { return x_pos_goal_; }

  /// <summary>
  /// 始点のY座標を取得
  /// </summary>
  /// <param names=""></param>
  /// <returns> Y座標 </returns>
  int GetStartPositionY() { return y_pos_goal_; }

  /// <summary>
  /// 終点のX座標の設定
  /// </summary>
  /// <param names="x_pos"> X座標 </param>
  /// <returns></returns>
  void SetGoalPositionX(int x_pos) { x_pos_goal_ = x_pos; }

  /// <summary>
  /// 終点のY座標の設定
  /// </summary>
  /// <param names="y_pos"> Y座標 </param>
  /// <returns></returns>
  void SetGoalPositionY(int y_pos) { y_pos_goal_ = y_pos; }

  /// <summary>
  /// 終点のX座標を取得
  /// </summary>
  /// <param names=""></param>
  /// <returns> X座標 </returns>
  int GetGoalPositionX() { return x_pos_goal_; }

  /// <summary>
  /// 終点のY座標を取得
  /// </summary>
  /// <param names=""></param>
  /// <returns> Y座標 </returns>
  int GetGoalPositionY() { return y_pos_goal_; }

  /// <summary>
  /// 透過率の設定
  /// </summary>
  /// <param names=""> 透過率 </param>
  /// <param names=""> 調整値 </param>
  /// <param names=""> 最大値 </param>
  /// <param names=""> 最小値 </param>
  /// <returns></returns>
  void SetAlpha(int, int, int, int);

  /// <summary>
  /// フェーズごとのHPの設定
  /// </summary>
  /// <param names=""> フェーズごとのHP </param>
  /// <returns></returns>
  void SetPhaseHitPoint(HitPoint hit_point) { hit_point_ = hit_point; };

  /// <summary>
  /// フェーズごとのHPを取得
  /// </summary>
  /// <param names=""></param>
  /// <returns> フェーズごとのHP </returns>
  HitPoint GetPhaseHitPoint() { return hit_point_; };

  /// <summary>
  /// 射撃処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ShootBullet();

  /// <summary>
  /// 透過率変更処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AdjustAlphaValue();

  /// <summary>
  /// 透過率の符号を変更
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeAlphaAdjustSign();


protected:

  /// <summary>
  /// 移動できる向きのリスト
  /// </summary>
  std::vector<DirectionType> move_direction_;

  /// <summary>
  /// エネミーイベントインターフェイス
  /// </summary>
  EnemyEventInterface& enemy_event_interface_;

  /// <summary>
  /// エネミーコントローラ
  /// </summary>
  EnemyController enemy_controller_;

  /// <summary>
  /// フェーズごとのHP
  /// </summary>
  HitPoint hit_point_;

  /// <summary>
  /// 移動始点座標 X座標
  /// </summary>
  int x_pos_start_;

  /// <summary>
  /// 移動始点座標 Y座標
  /// </summary>
  int y_pos_start_;

  /// <summary>
  /// 目標座標 X座標
  /// </summary>
  int x_pos_goal_;

  /// <summary>
  /// 目標座標 Y座標
  /// </summary>
  int y_pos_goal_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  int alpha_adjust_;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  int alpha_max_;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  int alpha_min_;

  /// <summary>
  /// 角度
  /// </summary>
  float angle_;

private:

  /// <summary>
  /// グラフィックハンドル
  /// </summary>
  int graphic_handle_;
};
