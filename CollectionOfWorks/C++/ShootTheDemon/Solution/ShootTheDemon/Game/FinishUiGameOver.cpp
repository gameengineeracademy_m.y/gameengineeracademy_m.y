﻿#include "Game/FinishUiGameOver.h"

namespace {

  /// <summary>
  /// メニュー画面 テキスト
  /// </summary>
  const char* kGameOverText = "Game Over";

  /// <summary>
  /// メニュー画面 ボタンテキスト
  /// </summary>
  const char* kButtunTextRetry = "RETRY";

  /// <summary>
  /// メニュー画面 ボタンテキスト
  /// </summary>
  const char* kButtunTextTitle = "RETURN TITLE";

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Reggae One";

  /// <summary>
  /// タイトル文字フォントサイズ
  /// </summary>
  const int kTitleFontSize = 55;

  /// <summary>
  /// タイトル文字幅
  /// </summary>
  const int kFontWidth = 1;

  /// <summary>
  /// ボタン文字フォントサイズ
  /// </summary>
  const int kButtonTextFontSize = 20;

  /// <summary>
  /// メニュー画面 枠色
  /// </summary>
  const int kFrameColor = GetColor(255, 0, 0);

  /// <summary>
  /// メニュー画面 背景色
  /// </summary>
  const int kBackColor = GetColor(0, 0, 0);

  /// <summary>
  /// メニュー画面 ボタン色 選択中
  /// </summary>
  const int kButtonSelectColor = GetColor(200, 200, 200);

  /// <summary>
  /// メニュー画面 ボタン色 選択外
  /// </summary>
  const int kButtonNoneColor = GetColor(100, 100, 100);

  /// <summary>
  /// メニュー画面 カーソル色
  /// </summary>
  const int kCursorColor = GetColor(255, 0, 0);

  /// <summary>
  /// メニュー画面 テキスト文字色
  /// </summary>
  const int kTextForeColor = GetColor(255, 0, 0);

  /// <summary>
  /// メニュー画面 ボタンテキスト文字色
  /// </summary>
  const int kButtonTextForeColor = GetColor(0, 0, 0);

  /// <summary>
  /// フレームサイズ 幅
  /// </summary>
  const int kFrameWidth = 400;

  /// <summary>
  /// フレームサイズ 高さ
  /// </summary>
  const int kFrameHeight = 300;

  /// <summary>
  /// スクリーンサイズ 幅
  /// </summary>
  const int kScreenWidth = 370;

  /// <summary>
  /// スクリーンサイズ 高さ
  /// </summary>
  const int kScreenHeight = 270;

  /// <summary>
  /// 文字隠し板 幅
  /// </summary>
  const int kHideTextWidth = 350;

  /// <summary>
  /// 文字隠し板 高さ上限
  /// </summary>
  const int kHideTextMaxHeight = 365;

  /// <summary>
  /// 文字隠し板 高さ下限
  /// </summary>
  const int kHideTextMinHeight = 450;

  /// <summary>
  /// 文字隠し板 配置位置
  /// </summary>
  const int kTextPosX = 300;

  /// <summary>
  /// 文字隠し板 配置位置
  /// </summary>
  const int kTextPosY = 50;

  /// <summary>
  /// 文字 配置位置調整値
  /// </summary>
  const int kTextAdjustPosY = 1;

  /// <summary>
  /// 文字 配置位置上限値
  /// </summary>
  const int kTextMaxPosY = 305;

  /// <summary>
  /// ボタン配置 左上X座標
  /// </summary>
  const int kButtonPosX = 160;

  /// <summary>
  /// ボタン配置 左上Y座標
  /// </summary>
  const int kButtonPosY = 55;

  /// <summary>
  /// ボタンサイズ 幅
  /// </summary>
  const int kButtonWidth = 150;

  /// <summary>
  /// ボタンサイズ 高さ
  /// </summary>
  const int kButtonHeight = 50;

  /// <summary>
  /// ボタンテキスト 高さ
  /// </summary>
  const int kButtonTextHeight = 15;

  /// <summary>
  /// カーソル調整値 固定値
  /// </summary>
  const float kCursorAdjust = 1.0f;

  /// <summary>
  /// カーソル調整値 X方向
  /// </summary>
  const float kCursorPlusAdjustX = 1.5f;

  /// <summary>
  /// カーソル調整値 Y方向
  /// </summary>
  const float kCursorPlusAdjustY = 1.5f;

  /// <summary>
  /// カーソル調整値 X方向
  /// </summary>
  const float kCursorMinusAdjustX = -1.5f;

  /// <summary>
  /// カーソル調整値 Y方向
  /// </summary>
  const float kCursorMinusAdjustY = -1.5f;

  /// <summary>
  /// カーソル 幅
  /// </summary>
  const float kCursorThick = 3.0f;

  /// <summary>
  /// スクリーン透過率 調整値
  /// </summary>
  const int kScreenAlphaAdjust = 5;

  /// <summary>
  /// スクリーン透過率 最大値
  /// </summary>
  const int kScreenAlphaMax = 230;

  /// <summary>
  /// フレーム透過率 最大値
  /// </summary>
  const int kFrameAlphaMax = 150;

  /// <summary>
  /// ボタン透過率 調整値
  /// </summary>
  const int kButtonAlphaAdjust = 30;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjust = 5;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 待機時間
  /// </summary>
  const float kWaitTime = 0.7f;

  /// <summary>
  /// リセット時間
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 半分
  /// </summary>
  const int kHalfValue = 2;
}


/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name="event_interface"> ゲームオーバー時のフィニッシュUI </param>
/// <returns></returns>
FinishUiGameOver::FinishUiGameOver(FinishUiGameOverEventInterface& event_interface)
  : Task(TaskId::kFinishUi)
  , current_phase_(PhaseType::kInitialize)
  , game_over_(GameOverPhase::kScreenView)
  , button_type_(ButtonType::kRetry)
  , finish_ui_event_interface_(event_interface)
  , frame_()
  , screen_()
  , text_hide_()
  , button_retry_()
  , button_title_()
  , cursor_()
  , cursor_adjust_()
  , title_text_()
  , text_button_retry_()
  , text_button_title_()
  , alpha_frame_()
  , alpha_screen_()
  , alpha_hide_()
  , alpha_button_()
  , alpha_text_()
  , button_color_()
  , accumulate_time_(0.0f) {

  // コンソールに出力
  std::cout << "FinishUiGameOver コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
FinishUiGameOver::~FinishUiGameOver() {

  // コンソールに出力
  std::cout << "~FinishUiGameOver デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void FinishUiGameOver::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //--------------------------
    //初期化フェーズ
    //--------------------------
    bool is_finish = Initialize();
    if (is_finish) {
      // 現在のフェーズを「起動」に変更
      ChangeCurrentPhase(PhaseType::kStartUp);
    }
    break;
  }
  case PhaseType::kStartUp: {
    //--------------------------
    //起動フェーズ
    //--------------------------
    // 現在のフェーズを「処理中」に変更
    ChangeCurrentPhase(PhaseType::kProcess);
    break;
  }
  case PhaseType::kProcess: {
    //--------------------------
    //処理中フェーズ
    //--------------------------
    bool is_finish = Process(process_time);
    if (is_finish) {
      // 現在のフェーズを「終了処理前」に変更
      ChangeCurrentPhase(PhaseType::kFinalizeBefore);
    }
    break;
  }
  case PhaseType::kFinalizeBefore: {
    //--------------------------
    //終了処理前フェーズ
    //--------------------------
    // 現在のフェーズを「終了処理」に変更
    ChangeCurrentPhase(PhaseType::kFinalize);
    break;
  }
  case PhaseType::kFinalize: {
    //--------------------------
    //終了処理フェーズ
    //--------------------------

    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void FinishUiGameOver::Render() {

  switch (current_phase_) {
  case PhaseType::kStartUp:
  case PhaseType::kProcess:
  case PhaseType::kFinalizeBefore: {

    // フォント名、フォントの種類のセット
    ChangeFont(kFontName);
    ChangeFontType(DX_FONTTYPE_ANTIALIASING);

    //-------------------------------------------
    // フレーム
    //-------------------------------------------
    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_frame_.value);
    DrawBox(frame_.left.x, frame_.left.y, frame_.right.x, frame_.right.y, kFrameColor, true);
    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    //-------------------------------------------
    // スクリーン
    //-------------------------------------------
    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_screen_.value);
    DrawBox(screen_.left.x, screen_.left.y, screen_.right.x, screen_.right.y, kBackColor, true);
    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    //-------------------------------------------
    // テキスト
    //-------------------------------------------
    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_text_.value);
    DrawStringToHandle(title_text_.x, title_text_.y, kGameOverText, kTextForeColor, font_handle_.at(static_cast<int>(FontSize::k60)));
    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    //-------------------------------------------
    // テキスト隠し
    //-------------------------------------------
    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_hide_.value);
    DrawBox(text_hide_.left.x, text_hide_.left.y, text_hide_.right.x, text_hide_.right.y, kBackColor, true);
    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    //-------------------------------------------
    // ボタン
    //-------------------------------------------
    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_button_.value);
    DrawBox(button_retry_.left.x, button_retry_.left.y,
            button_retry_.right.x, button_retry_.right.y, button_color_.retry, true);
    DrawBox(button_title_.left.x, button_title_.left.y,
            button_title_.right.x, button_title_.right.y, button_color_.title, true);
    DrawStringToHandle(text_button_retry_.x, text_button_retry_.y, kButtunTextRetry, kButtonTextForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
    DrawStringToHandle(text_button_title_.x, text_button_title_.y, kButtunTextTitle, kButtonTextForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    //-------------------------------------------
    // カーソル
    //-------------------------------------------
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_button_.value);
    // 左
    DrawLineAA(cursor_.left.x, cursor_.left.y,
               cursor_.left.x, cursor_.right.y, kCursorColor, kCursorThick);
    // 右
    DrawLineAA(cursor_.right.x - kCursorAdjust, cursor_.left.y,
               cursor_.right.x - kCursorAdjust, cursor_.right.y, kCursorColor, kCursorThick);
    // 上
    DrawLineAA(cursor_.left.x - kCursorAdjust, cursor_.left.y,
               cursor_.right.x + kCursorAdjust, cursor_.left.y, kCursorColor, kCursorThick);
    // 下
    DrawLineAA(cursor_.left.x - kCursorAdjust, cursor_.right.y,
               cursor_.right.x + kCursorAdjust, cursor_.right.y, kCursorColor, kCursorThick);
    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    break;
  }
  }
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool FinishUiGameOver::Initialize() {

  // ゲーム情報取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return false;
  }

  // 画面中央座標を取得
  int x_pos_center = game_info->GetCenterPointX();
  int y_pos_center = game_info->GetCenterPointY();

  // フォント情報生成
  FontInfo* font_info = FontInfo::GetFontInfoInstance();
  if (font_info == nullptr) {
    return false;
  }
  int font_handle = font_info->GetFontInfo(kFontName, kButtonTextFontSize, kFontWidth, DX_FONTTYPE_NORMAL);
  font_handle_.push_back(font_handle);
  font_handle = font_info->GetFontInfo(kFontName, kTitleFontSize, kFontWidth, DX_FONTTYPE_NORMAL);
  font_handle_.push_back(font_handle);


  // メニュー画面 フレーム
  frame_.left.x = x_pos_center - kFrameWidth / kHalfValue;
  frame_.left.y = y_pos_center - kFrameHeight / kHalfValue;
  frame_.right.x = x_pos_center + kFrameWidth / kHalfValue;
  frame_.right.y = y_pos_center + kFrameHeight / kHalfValue;

  // メニュー画面 本体
  screen_.left.x = x_pos_center - kScreenWidth / kHalfValue;
  screen_.left.y = y_pos_center - kScreenHeight / kHalfValue;
  screen_.right.x = x_pos_center + kScreenWidth / kHalfValue;
  screen_.right.y = y_pos_center + kScreenHeight / kHalfValue;

  // メニュー画面 文字隠し
  text_hide_.left.x = x_pos_center - kHideTextWidth / kHalfValue;
  text_hide_.left.y = kHideTextMaxHeight;
  text_hide_.right.x = x_pos_center + kHideTextWidth / kHalfValue;
  text_hide_.right.y = kHideTextMinHeight;

  // メニュー画面 リトライボタン
  button_retry_.left.x = x_pos_center - kButtonPosX;
  button_retry_.left.y = y_pos_center + kButtonPosY;
  button_retry_.right.x = button_retry_.left.x + kButtonWidth;
  button_retry_.right.y = button_retry_.left.y + kButtonHeight;

  // メニュー画面 タイトルボタン
  button_title_.right.x = x_pos_center + kButtonPosX;
  button_title_.left.y = y_pos_center + kButtonPosY;
  button_title_.left.x = button_title_.right.x - kButtonWidth;
  button_title_.right.y = button_title_.left.y + kButtonHeight;

  // メニュー画面 ボタンの色
  button_color_.retry = kButtonSelectColor;
  button_color_.title = kButtonNoneColor;

  // メニュー画面 カーソル
  cursor_.left.x = static_cast<float>(x_pos_center - kButtonPosX);
  cursor_.left.y = static_cast<float>(y_pos_center + kButtonPosY);
  cursor_.right.x = static_cast<float>(button_retry_.left.x + kButtonWidth);
  cursor_.right.y = static_cast<float>(button_retry_.left.y + kButtonHeight);

  // メニュー画面 カーソル調整量
  cursor_adjust_.x = kCursorPlusAdjustX;
  cursor_adjust_.y = kCursorPlusAdjustY;

  // 透過率 メニュー画面 フレーム
  alpha_frame_.value = kAlphaMin;
  alpha_frame_.adjust = kScreenAlphaAdjust;
  alpha_frame_.max = kFrameAlphaMax;
  alpha_frame_.min = kAlphaMin;

  // 透過率 メニュー画面 本体
  alpha_screen_.value = kAlphaMin;
  alpha_screen_.adjust = kScreenAlphaAdjust;
  alpha_screen_.max = kAlphaMax;
  alpha_screen_.min = kAlphaMin;

  // 透過率 メニュー画面 文字隠し
  alpha_hide_.value = kAlphaMin;
  alpha_hide_.adjust = kAlphaAdjust;
  alpha_hide_.max = kAlphaMax;
  alpha_hide_.min = kAlphaMin;

  // 透過率 メニュー画面 ボタン
  alpha_button_.value = kAlphaMin;
  alpha_button_.adjust = kAlphaAdjust;
  alpha_button_.max = kAlphaMax;
  alpha_button_.min = kAlphaMin;

  // 透過率 メニュー画面 文字
  alpha_text_.value = kAlphaMin;
  alpha_text_.adjust = kAlphaAdjust;
  alpha_text_.max = kAlphaMax;
  alpha_text_.min = kAlphaMin;

  // 文字テキスト位置
  int text_width = GetDrawStringWidthToHandle(kGameOverText, static_cast<int>(strlen(kGameOverText)), font_handle_.at(static_cast<int>(FontSize::k60)));
  title_text_.x = x_pos_center - (text_width / kHalfValue);
  title_text_.y = text_hide_.left.y;

  text_width = GetDrawStringWidthToHandle(kButtunTextRetry, static_cast<int>(strlen(kButtunTextRetry)), font_handle_.at(static_cast<int>(FontSize::k20)));
  text_button_retry_.x = (button_retry_.left.x + button_retry_.right.x) / kHalfValue - (text_width / kHalfValue);
  text_button_retry_.y = button_retry_.left.y + kButtonTextHeight;
  text_width = GetDrawStringWidthToHandle(kButtunTextTitle, static_cast<int>(strlen(kButtunTextTitle)), font_handle_.at(static_cast<int>(FontSize::k20)));
  text_button_title_.x = (button_title_.left.x + button_title_.right.x) / kHalfValue - (text_width / kHalfValue);
  text_button_title_.y = button_title_.left.y + kButtonTextHeight;

  return true;
}

/// <summary>
/// 処理中
/// </summary>
/// <param name="process_time"></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool FinishUiGameOver::Process(float process_time) {

  switch (game_over_) {
  case GameOverPhase::kScreenView: {
    //--------------------------
    //スクリーン描画フェーズ
    //--------------------------
    bool is_finish = ViewScreen();
    if (is_finish) {
      // サウンド生成
      finish_ui_event_interface_.OnPlayGameOverSound();
      // フェーズを「テキスト描画」に変更
      ChangeGameOverPhase(GameOverPhase::kTitleTextView);
    }
    break;
  }
  case GameOverPhase::kTitleTextView: {
    //--------------------------
    //テキスト描画フェーズ
    //--------------------------
    bool is_finish = ViewTitleText();
    if (is_finish) {
      // フェーズを「ボタン描画」に変更
      ChangeGameOverPhase(GameOverPhase::kButtonView);
    }
    break;
  }
  case GameOverPhase::kButtonView: {
    //--------------------------
    //ボタン描画フェーズ
    //--------------------------
    bool is_finish = ViewButton();
    if (is_finish) {
      // フェーズを「選択待機」に変更
      ChangeGameOverPhase(GameOverPhase::kSelectWait);
    }
    break;
  }
  case GameOverPhase::kSelectWait: {
    //--------------------------
    //選択待機フェーズ
    //--------------------------
    WaitProcess(process_time);
    break;
  }
  case GameOverPhase::kFinalize: {
    //--------------------------
    //終了処理フェーズ
    //--------------------------
    // フェーズを「終了処理済み」に変更
    ChangeGameOverPhase(GameOverPhase::kFinalized);
    return true;
  }
  }

  return false;
}

/// <summary>
/// スクリーン表示
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool FinishUiGameOver::ViewScreen() {

  // スクリーンとフレームの透過率調整
  alpha_screen_.value += kScreenAlphaAdjust;
  alpha_frame_.value += kScreenAlphaAdjust;


  if (alpha_frame_.value >= alpha_frame_.max) {
    alpha_frame_.value = alpha_frame_.max;
  }
  if (alpha_screen_.value >= alpha_screen_.max) {
    alpha_screen_.value = alpha_screen_.max;
  }

  // 透過率が最大に達した場合、処理終了
  if (alpha_frame_.value == alpha_frame_.max &&
      alpha_screen_.value == alpha_screen_.max) {
    alpha_hide_.value = kAlphaMax;
    alpha_text_.value = kAlphaMax;
    return true;
  }

  return false;
}

/// <summary>
/// テキスト表示
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool FinishUiGameOver::ViewTitleText() {

  // 表示位置を変更
  title_text_.y -= kTextAdjustPosY;

  if (title_text_.y <= kTextMaxPosY) {
    title_text_.y = kTextMaxPosY;
    return true;
  }

  return false;
}

/// <summary>
/// ボタン表示
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool FinishUiGameOver::ViewButton() {

  alpha_button_.value += kButtonAlphaAdjust;
  
  if (alpha_button_.value >= alpha_button_.max) {
    alpha_button_.value = alpha_button_.max;
  }

  // 透過率が最大に達した場合、処理終了
  if (alpha_button_.value == alpha_button_.max) {
    return true;
  }

  return false;
}

/// <summary>
/// 待機中処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void FinishUiGameOver::WaitProcess(float process_time) {

  // 累積時間に加算
  accumulate_time_ += process_time;
  if (accumulate_time_ >= kWaitTime) {
    // 累積時間をリセット
    accumulate_time_ = kResetTime;

    cursor_.left.x -= cursor_adjust_.x;
    cursor_.left.y -= cursor_adjust_.y;
    cursor_.right.x += cursor_adjust_.x;
    cursor_.right.y += cursor_adjust_.y;

    if (cursor_adjust_.x == kCursorPlusAdjustX) {
      cursor_adjust_.x = kCursorMinusAdjustX;
      cursor_adjust_.y = kCursorMinusAdjustY;
    }
    else {
      cursor_adjust_.x = kCursorPlusAdjustX;
      cursor_adjust_.y = kCursorPlusAdjustY;
    }
  }
}

/// <summary>
/// ボタン選択切り替え
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void FinishUiGameOver::ChangeSelectButton() {

  if (game_over_ != GameOverPhase::kSelectWait) {
    return;
  }

  // ボタンの切り替え
  switch (button_type_) {
  case ButtonType::kRetry: {
    //--------------------------
    //リトライ
    //--------------------------
    button_type_ = ButtonType::kTitle;

    // メニュー画面 カーソル
    cursor_.left.x = static_cast<float>(button_title_.left.x);
    cursor_.left.y = static_cast<float>(button_title_.left.y);
    cursor_.right.x = static_cast<float>(button_title_.left.x + kButtonWidth);
    cursor_.right.y = static_cast<float>(button_title_.left.y + kButtonHeight);

    // ボタンの色変更
    button_color_.retry = kButtonNoneColor;
    button_color_.title = kButtonSelectColor;

    // カーソル調整値 初期化
    cursor_adjust_.x = kCursorPlusAdjustX;
    cursor_adjust_.y = kCursorPlusAdjustY;

    break;
  }
  case ButtonType::kTitle: {
    //--------------------------
    //タイトルへ
    //--------------------------
    button_type_ = ButtonType::kRetry;

    // メニュー画面 カーソル
    cursor_.left.x = static_cast<float>(button_retry_.left.x);
    cursor_.left.y = static_cast<float>(button_retry_.left.y);
    cursor_.right.x = static_cast<float>(button_retry_.left.x + kButtonWidth);
    cursor_.right.y = static_cast<float>(button_retry_.left.y + kButtonHeight);

    // ボタンの色変更
    button_color_.retry = kButtonSelectColor;
    button_color_.title = kButtonNoneColor;

    // カーソル調整値 初期化
    cursor_adjust_.x = kCursorPlusAdjustX;
    cursor_adjust_.y = kCursorPlusAdjustY;

    break;
  }
  }
}

/// <summary>
/// ボタン選択決定処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void FinishUiGameOver::DecideSelectButton() {

  switch (button_type_) {
  case ButtonType::kRetry: {
    // バトルレベルへ遷移
    finish_ui_event_interface_.OnTryAgain();
    break;
  }
  case ButtonType::kTitle: {
    // タイトルレベルへ遷移
    finish_ui_event_interface_.OnGoToTitle();
    break;
  }
  }

  // ゲームオーバーフェーズを「終了処理」に変更
  ChangeGameOverPhase(GameOverPhase::kFinalize);
}