﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "System/FontInfo.h"
#include "Game/GameInfo.h"
#include <vector>

namespace {

  /// <summary>
  /// ボタンの種類
  /// </summary>
  const int kPauseButtonType = 4;
}

/// <summary>
/// ポーズUI処理
/// </summary>
class PauseUi : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,      // 初期化処理フェーズ
    kProcess,         // 処理中フェーズ
    kFinalize,        // 終了処理フェーズ
    kFinalized,       // 終了処理済みフェーズ
    kPhaseMaxIndex    // フェーズ数
  };

  /// <summary>
  /// ボタンの種類
  /// </summary>
  enum class ButtonType {
    kResume,          // 再開
    kSetup,           // 各種設定
    kRetry,           // リトライ
    kGoToTitle,       // タイトルへ戻る
    kTypeMaxIndex     // 種類数
  };

  /// <summary>
  /// フォントサイズの種類
  /// </summary>
  enum class FontSize {
    k22,               // サイズ22
    k35,               // サイズ35
    kSizeMaxIndex      // 種類数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  PauseUi();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~PauseUi();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// カーソル操作 上移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OperationCursorUp();

  /// <summary>
  /// カーソル操作 下移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OperationCursorDown();

  /// <summary>
  /// ボタンの種類
  /// </summary>
  /// <param name=""></param>
  /// <returns> ボタンの種類 </returns>
  ButtonType GetButtonType() { return button_type_; }

  /// <summary>
  /// 現在のフェーズを変更
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }

  /// <summary>
  /// ボタンの種類を変更
  /// </summary>
  /// <param name=""> ボタンの種類 </param>
  /// <returns></returns>
  void ChangeButtonType(ButtonType button_type) { button_type_ = button_type; }


private:

  /// <summary>
  /// 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理継続 </returns>
  bool Initialize();

  /// <summary>
  /// 処理中処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Process(float);

  /// <summary>
  /// カーソル操作
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OperationCursor();


private:

  /// <summary>
  /// 表示座標
  /// </summary>
  struct Pos {
    int x;
    int y;
  };

  /// <summary>
  /// 表示座標 浮動小数
  /// </summary>
  struct Posf {
    float x;
    float y;
  };

  /// <summary>
  /// サイズ
  /// </summary>
  struct Size {
    int width;
    int height;
  };

  /// <summary>
  /// 図形
  /// </summary>
  struct Figure {
    Pos left;
    Pos right;
    Size size;
  };

  /// <summary>
  /// 図形 浮動小数
  /// </summary>
  struct Figuref {
    Posf left;
    Posf right;
  };

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// ボタンの種類
  /// </summary>
  ButtonType button_type_;

  /// <summary>
  /// メニュー画面 本体
  /// </summary>
  Figure screen_;

  /// <summary>
  /// メニュー画面 ボタン
  /// </summary>
  Figure button_resume_;

  /// <summary>
  /// メニュー画面 ボタン
  /// </summary>
  Figure button_setup_;

  /// <summary>
  /// メニュー画面 ボタン
  /// </summary>
  Figure button_retry_;

  /// <summary>
  /// メニュー画面 ボタン
  /// </summary>
  Figure button_title_;

  /// <summary>
  /// 背景
  /// </summary>
  Figure back_board_;

  /// <summary>
  /// メニュー画面 カーソル
  /// </summary>
  Figuref cursor_;

  /// <summary>
  /// メニュー画面 カーソル調整値
  /// </summary>
  Posf cursor_adjust_;

  /// <summary>
  /// メニュー画面 スクリーンテキスト
  /// </summary>
  Pos text_screen_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_button_resume_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_button_setup_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_button_retry_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_button_title_;

  /// <summary>
  /// ボタンの色
  /// </summary>
  int button_color_[kPauseButtonType];

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// フォントハンドル
  /// </summary>
  std::vector<int> font_handle_;
};