﻿#pragma once

#include "System/CharacterControllerEventInterface.h"
#include "System/MenuControllerEventInterface.h"
#include "System/CutInManagerEventInterface.h"
#include "System/CharacterEventInterface.h"
#include "System/VolumeControllerEventInterface.h"
#include "System/SoundSetupEventInterface.h"
#include "Game/EnemyEventInterface.h"
#include "Game/BulletEventInterface.h"
#include "Game/BarrageEventInterface.h"
#include "Game/ItemEventInterface.h"
#include "Game/FinishUiGameOverEventInterface.h"
#include "Game/FinishUiGameClearEventInterface.h"

/// <summary>
/// バトルレベルイベントインターフェース
/// </summary>
class BattleLevelEventInterface : public CharacterControllerEventInterface,
                                  public MenuControllerEventInterface,
                                  public CutInManagerEventInterface,
                                  public CharacterEventInterface,
                                  public VolumeControllerEventInterface,
                                  public SoundSetupEventInterface,
                                  public EnemyEventInterface,
                                  public BulletEventInterface,
                                  public BarrageEventInterface,
                                  public ItemEventInterface,
                                  public FinishUiGameOverEventInterface,
                                  public FinishUiGameClearEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  BattleLevelEventInterface() {

    // コンソールに出力
    std::cout << "BattleLevelEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~BattleLevelEventInterface() {

    // コンソールに出力
    std::cout << "~BattleLevelEventInterface デストラクタ" << std::endl;
  }


private:


};