﻿#pragma once

#include <iostream>

/// <summary>
/// ゲームクリア処理イベントインターフェース
/// </summary>
class FinishUiGameClearEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  FinishUiGameClearEventInterface() {

    // コンソールに出力
    std::cout << "FinishUiGameClearEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~FinishUiGameClearEventInterface() {

    // コンソールに出力
    std::cout << "~FinishUiGameClearEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// 花火の打ち上げ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnShootFirework1() = 0;

  /// <summary>
  /// 花火の打ち上げ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnShootFirework2() = 0;

  /// <summary>
  /// 花火の打ち上げ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnShootFirework3() = 0;

  /// <summary>
  /// テキスト非表示終了通知
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnFinishHideGameClearText() = 0;


private:

};