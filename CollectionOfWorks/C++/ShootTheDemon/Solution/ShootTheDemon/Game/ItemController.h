﻿#pragma once

#include "System/Character.h"
#include "System/CharacterControllerEventInterface.h"
#include <iostream>

/// <summary>
/// アイテムコントローラ
/// </summary>
class ItemController {
public:

  /// <summary>
  /// コンストラクタ
  /// </summary>
  /// <param name=""> キャラクターコントローライベントインターフェース </param>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  ItemController(CharacterControllerEventInterface&, Character*);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~ItemController();

  /// <summary>
  /// 下に移動する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void MoveDown();


private:

  /// <summary>
  /// キャラクターコントローライベントインターフェイス
  /// </summary>
  CharacterControllerEventInterface& character_controller_event_interface_;

  /// <summary>
  /// キャラクター
  /// </summary>
  Character* character_;

};