﻿#pragma once

#include "System/MenuControllerEventInterface.h"
#include <iostream>

/// <summary>
/// リザルトレベルイベントインターフェース
/// </summary>
class ResultLevelEventInterface : public MenuControllerEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ResultLevelEventInterface() {

    // コンソールに出力
    std::cout << "ResultLevelEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~ResultLevelEventInterface() {

    // コンソールに出力
    std::cout << "~ResultLevelEventInterface デストラクタ" << std::endl;
  }

private:

};