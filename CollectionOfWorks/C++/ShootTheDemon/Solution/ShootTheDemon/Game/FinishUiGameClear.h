﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "System/FontInfo.h"
#include "Game/GameInfo.h"
#include "Game/FinishUiGameClearEventInterface.h"

/// <summary>
/// ゲームクリア時のフィニッシュUI
/// </summary>
class FinishUiGameClear : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,       // 初期化フェーズ
    kStartUp,          // 起動フェーズ
    kProcess,          // 処理中フェーズ
    kFinalizeBefore,   // 終了処理前フェーズ
    kFinalize,         // 終了処理フェーズ
    kFinalized,        // 終了処理済みフェーズ
    kPhaseMaxIndex     // フェーズ数
  };

  /// <summary>
  /// ゲームクリア時のフェーズ
  /// </summary>
  enum class GameClearPhase {
    kClearTextView,    // テキスト表示フェーズ
    kProductionWait,   // 演出待機フェーズ
    kClearTextFinish,  // テキスト表示終了フェーズ
    kWait,             // 待機フェーズ
    kBeforeFinalize,   // 終了処理前フェーズ
    kFinalize,         // 終了処理フェーズ
    kFinalized,        // 終了処理済みフェーズ
    kPhaseMaxIndex     // フェーズ数
  };

  /// <summary>
  /// 花火フェーズ
  /// </summary>
  enum class FireworkType {
    kType1,           // 花火演出1
    kType2,           // 花火演出2
    kType3,           // 花火演出3
    kStop,            // 停止
    kTypeMaxIndex     // 種類数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> ゲームクリア処理イベントインターフェース </param>
  /// <returns></returns>
  FinishUiGameClear(FinishUiGameClearEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~FinishUiGameClear();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// 現在のフェーズを変更
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }

  /// <summary>
  /// ゲームクリアのフェーズを変更
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeGameClearPhase(GameClearPhase phase_type) { game_clear_ = phase_type; }

  /// <summary>
  /// 花火の種類を変更
  /// </summary>
  /// <param name=""> 花火の種類 </param>
  /// <returns></returns>
  void ChangeFireworkType(FireworkType type) { firework_type_ = type; }


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Initialize();

  /// <summary>
  /// 処理中
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Process(float);

  /// <summary>
  /// テキスト表示
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool ViewClearText();

  /// <summary>
  /// テキスト非表示
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool HideClearText();

  /// <summary>
  /// 花火打ち上げ処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void ShootFirework(float);


private:

  /// <summary>
  /// 表示座標
  /// </summary>
  struct Pos {
    int x;
    int y;
  };

  /// <summary>
  /// 透過度
  /// </summary>
  struct Alpha {
    int value;
    int adjust;
    int max;
    int min;
  };

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// ゲームクリアフェーズ
  /// </summary>
  GameClearPhase game_clear_;

  /// <summary>
  /// 花火の種類
  /// </summary>
  FireworkType firework_type_;

  /// <summary>
  /// ゲームクリア処理イベントインターフェース
  /// </summary>
  FinishUiGameClearEventInterface& game_clear_event_interface_;

  /// <summary>
  /// テキスト 表示位置
  /// </summary>
  Pos text_;

  /// <summary>
  /// テキスト 透過率
  /// </summary>
  Alpha alpha_text_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// 花火打ち上げ管理時間
  /// </summary>
  float firework_time_;

  /// <summary>
  /// フォントハンドル
  /// </summary>
  int font_handle_;
};