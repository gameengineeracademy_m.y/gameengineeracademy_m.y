﻿#pragma once

#include <iostream>

/// <summary>
/// バレットイベントインターフェース
/// </summary>
class BulletEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  BulletEventInterface() {

    // コンソールに出力
    std::cout << "BattleLevelEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~BulletEventInterface() {

    // コンソールに出力
    std::cout << "~BattleLevelEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// 当たり判定チェック
  /// </summary>
  /// <param name=""> バレット </param>
  /// <returns> true:当たった, false:当たっていない </returns>
  virtual bool OnCheckCollisionBullet(class Bullet*) = 0;

  /// <summary>
  /// 弾の描画終了を通知
  /// </summary>
  /// <param name=""></param>
  /// <returns> バレット </returns>
  virtual void OnFinishBullet(class Bullet*) = 0;


private:

};