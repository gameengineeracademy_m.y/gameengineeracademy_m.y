﻿#include "Game/ResultBackGround.h"

namespace {

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjust = 5;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 原点 X座標
  /// </summary>
  const int kOriginPosX = 0;

  /// <summary>
  /// 原点 Y座標
  /// </summary>
  const int kOriginPosY = 0;
}


/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ResultBackGround::ResultBackGround()
  : Task(TaskId::kResultBackGround)
  , current_phase_(PhaseType::kNone)
  , graphic_handle_(0)
  , x_pos_(0)
  , y_pos_(0)
  , alpha_(0) {

  // コンソールに出力
  std::cout << "ResultBackGround コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ResultBackGround::~ResultBackGround() {

  // コンソールに出力
  std::cout << "~ResultBackGround デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void ResultBackGround::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kNone: {
    //-------------------------------
    //何もしないフェーズ
    //-------------------------------
    break;
  }
  case PhaseType::kInitialize: {
    //-------------------------------
    //初期化処理フェーズ
    //-------------------------------
    bool is_finish = Initialize();
    if (is_finish) {
      // 現在のフェーズを「起動」に変更
      ChangeCurrentPhase(PhaseType::kStartUp);
    }
    break;
  }
  case PhaseType::kStartWait: {
    //-------------------------------
    //起動待機フェーズ
    //-------------------------------
    break;
  }
  case PhaseType::kStartUp: {
    //-------------------------------
    //起動フェーズ
    //-------------------------------
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //-------------------------------
    //処理中フェーズ
    //-------------------------------
    break;
  }
  case PhaseType::kBeforeFinalize: {
    //-------------------------------
    //終了処理前フェーズ
    //-------------------------------
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //-------------------------------
    //終了処理フェーズ
    //-------------------------------
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ResultBackGround::Render() {

  switch (current_phase_) {
  case PhaseType::kStartUp:
  case PhaseType::kProcess:
  case PhaseType::kBeforeFinalize: {

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

    DrawGraph(x_pos_, y_pos_, graphic_handle_, true);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
    break;
  }
  }
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param names=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool ResultBackGround::Initialize() {

  // 透過率の設定
  alpha_ = kAlphaMin;

  //背景描画位置 設定
  x_pos_ = kOriginPosX;
  y_pos_ = kOriginPosY;

  return true;
}

/// <summary>
/// 透過率の調整
/// </summary>
/// <param names=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool ResultBackGround::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率に調整値を加算
  switch (current_phase_) {
  case PhaseType::kStartUp: {
    // 起動フェーズ
    alpha_ += kAlphaAdjust;
    break;
  }
  case PhaseType::kBeforeFinalize: {
    // 終了処理前フェーズ
    alpha_ -= kAlphaAdjust;
    break;
  }
  }

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= kAlphaMax) {
    alpha_ = kAlphaMax;
    is_finish = true;
  }
  else if (alpha_ <= kAlphaMin) {
    alpha_ = kAlphaMin;
    is_finish = true;
  }

  return is_finish;
}