﻿#pragma once

#include <iostream>

/// <summary>
/// ゲームモードイベントインターフェース
/// </summary>
class GameModeEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  GameModeEventInterface() {

    // コンソールに出力
    std::cout << "GameModeEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~GameModeEventInterface() {

    // コンソールに出力
    std::cout << "~GameModeEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// ゲームの描画を停止する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnGameStop() = 0;

  /// <summary>
  /// ゲームの描画を再開する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnGameResume() = 0;


private:


};