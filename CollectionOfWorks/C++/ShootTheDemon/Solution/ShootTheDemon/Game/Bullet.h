﻿#pragma once

#include "DxLib.h"
#include "System/Character.h"
#include "Game/BulletId.h"
#include "Game/GameInfo.h"
#include "Game/BulletEventInterface.h"
#include <iostream>

/// <summary>
/// バレット
/// </summary>
class Bullet {
public:

  /// <summary>
  /// 弾の移動量(フレーム当たり)
  /// </summary>
  struct Speed {
    float x;        // X方向
    float y;        // Y方向
    float x_max;    // X方向最大値
    float x_min;    // X方向最小値
    float y_max;    // Y方向最大値
    float y_min;    // Y方向最小値
  };

  /// <summary>
  /// 弾のサイズ
  /// </summary>
  struct Size {
    int width;      // 幅
    int height;     // 高さ
  };

  /// <summary>
  /// 弾の性能
  /// </summary>
  struct Spec {
    int atttack;    // 攻撃力
  };

  /// <summary>
  /// 弾の加速度
  /// </summary>
  struct Accele {
    float x;        // X方向
    float y;        // Y方向
  };

  /// <summary>
  /// 弾の中間点
  /// </summary>
  struct MidPos {
    float x;        // X方向
    float y;        // Y方向
  };

  /// <summary>
  /// 時間に関する量
  /// </summary>
  struct Time {
    float wait_start;     // 動作待機時間
    float wait_accel;     // 加速待機時間
    float wait_expan;     // 拡大待機時間
  };

  /// <summary>
  /// 拡大率
  /// </summary>
  struct ExtRate {
    float value;    // 拡大率
    float adjust;   // 拡大率調整値
    float max;      // 拡大率最大値
    float min;      // 拡大率最小値
  };

  /// <summary>
  /// 角度
  /// </summary>
  struct Angle {
    float value;    // 角度
    float adjust;   // 角度調整値
    float max;      // 角度最大値
    float min;      // 角度最小値
  };

  /// <summary>
  /// 透過度
  /// </summary>
  struct Alpha {
    int value;      // 透過度
    int adjust;     // 透過度調整値
    int max;        // 透過度最大値
    int min;        // 透過度最小値
  };

  /// <summary>
  /// 弾情報 設定値
  /// </summary>
  struct BulletData {
    Speed speed;
    Size size;
    Spec spec;
    Accele accele;
    Time time;
    ExtRate ext_rate;
    Angle angle;
    Alpha alpha;
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> バレットID </param>
  /// <returns></returns>
  Bullet(BulletId);

  /// <summary>
  /// コピーコンストラクタ
  /// </summary>
  /// <param name=""> コピー元クラス </param>
  /// <returns></returns>
  Bullet(const Bullet& bullet);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~Bullet();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:画面外到達, false:描画中 </returns>
  bool Update(float);

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render();

  /// <summary>
  /// 弾のクローン生成
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  Bullet* GenerateClone();

  /// <summary>
  /// バレットイベントインターフェース
  /// </summary>
  /// <param name="graphic_handle"> バレットイベントインターフェース </param>
  /// <returns></returns>
  void SetBulletEventInterface(BulletEventInterface* bullet_event_interface) { bullet_event_interface_ = bullet_event_interface; }

  /// <summary>
  /// グラフィックハンドルをセット
  /// </summary>
  /// <param name="graphic_handle"> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandle(int graphic_handle) { graphic_handle_ = graphic_handle; }

  /// <summary>
  /// グラフィックハンドルを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandle() { return graphic_handle_; }

  /// <summary>
  /// X座標をセット
  /// </summary>
  /// <param name="x_pos"> X座標 </param>
  /// <returns></returns>
  void SetPositionX(float x_pos) { x_pos_ = x_pos; }

  /// <summary>
  /// X座標を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> X座標 </returns>
  float GetPositionX() { return x_pos_; }

  /// <summary>
  /// Y座標をセット
  /// </summary>
  /// <param name="y_pos"> Y座標 </param>
  /// <returns></returns>
  void SetPositionY(float y_pos) { y_pos_ = y_pos; }

  /// <summary>
  /// Y座標を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> Y座標 </returns>
  float GetPositionY() { return y_pos_; }

  /// <summary>
  /// 移動量のセット
  /// </summary>
  /// <param name=""> X方向 </param>
  /// <param name=""> Y方向 </param>
  /// <returns></returns>
  void SetSpeed(float, float);

  /// <summary>
  /// 移動量の最大値のセット
  /// </summary>
  /// <param name=""> X方向 </param>
  /// <param name=""> Y方向 </param>
  /// <returns></returns>
  void SetSpeedMax(float, float);

  /// <summary>
  /// 移動量の最小値のセット
  /// </summary>
  /// <param name=""> X方向 </param>
  /// <param name=""> Y方向 </param>
  /// <returns></returns>
  void SetSpeedMin(float, float);

  /// <summary>
  /// 弾サイズのセット
  /// </summary>
  /// <param name=""> 横幅 </param>
  /// <param name=""> 高さ </param>
  /// <returns></returns>
  void SetSize(int, int);

  /// <summary>
  /// 弾スペックのセット
  /// </summary>
  /// <param name=""> 攻撃力 </param>
  /// <returns></returns>
  void SetSpec(int);

  /// <summary>
  /// 弾の加速度のセット
  /// </summary>
  /// <param name=""> X方向 </param>
  /// <param name=""> Y方向 </param>
  /// <returns></returns>
  void SetAcceleration(float, float);

  /// <summary>
  /// 弾の待機時間のセット
  /// </summary>
  /// <param name=""> 動作待機時間 </param>
  /// <param name=""> 加速待機時間 </param>
  /// <param name=""> 拡大待機時間 </param>
  /// <returns></returns>
  void SetWaitTime(float, float, float);

  /// <summary>
  /// 拡大率の関連値セット
  /// </summary>
  /// <param name=""> 初期値 </param>
  /// <param name=""> 調整値 </param>
  /// <param name=""> 最大値 </param>
  /// <param name=""> 最小値 </param>
  /// <returns></returns>
  void SetExtRate(float, float, float, float);

  /// <summary>
  /// 角度の関連値セット
  /// </summary>
  /// <param name=""> 初期値 </param>
  /// <param name=""> 調整値 </param>
  /// <param name=""> 最大値 </param>
  /// <param name=""> 最小値 </param>
  /// <returns></returns>
  void SetAngle(float, float, float, float);

  /// <summary>
  /// 透過度の関連値セット
  /// </summary>
  /// <param name=""> 初期値 </param>
  /// <param name=""> 調整値 </param>
  /// <param name=""> 最大値 </param>
  /// <param name=""> 最小値 </param>
  /// <returns></returns>
  void SetAlpha(int, int, int, int);

  /// <summary>
  /// 弾の情報を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> 弾の情報 </returns>
  BulletData GetBulletData() { return bullet_data_; }

  /// <summary>
  /// バレットIDを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> バレットID </returns>
  BulletId GetBulletId() { return bullet_id_; }

  /// <summary>
  /// 画像のサイズをセットする
  /// </summary>
  /// <param> グラフィックハンドル </param>
  /// <returns></returns>
  void SetImageSize(int);

  /// <summary>
  /// 画像の幅を取得する
  /// </summary>
  /// <param></param>
  /// <returns> 画像の幅 </returns>
  int GetImageWidth() { return image_width_; }

  /// <summary>
  /// 画像の高さを取得する
  /// </summary>
  /// <param></param>
  /// <returns> 画像の高さ </returns>
  int GetImageHeight() { return image_height_; }

  /// <summary>
  /// 画像の幅の半分を取得する
  /// </summary>
  /// <param></param>
  /// <returns> 画像の半分の幅 </returns>
  int GetImageHalfWidth() { return image_width_half_; }

  /// <summary>
  /// 画像の高さの半分を取得する
  /// </summary>
  /// <param></param>
  /// <returns> 画像の半分の高さ </returns>
  int GetImageHalfHeight() { return image_height_half_; }

  /// <summary>
  /// 半径をセット
  /// </summary>
  /// <param name="radius"> 半径 </param>
  /// <returns></returns>
  void SetRadius(float radius) { radius_ = radius; }

  /// <summary>
  /// 半径を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> 半径 </returns>
  float GetRadius() { return radius_; }

  /// <summary>
  /// キャラクターをセット
  /// </summary>
  /// <param name="character"> キャラクター </param>
  /// <returns></returns>
  void SetCharacter(Character* character) { character_ = character; }

  /// <summary>
  /// キャラクターを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> キャラクター </returns>
  Character* GetCharacter() { return character_; }

  /// <summary>
  /// 動作開始フラグをセットする
  /// </summary>
  /// <param name="flag"> true:移動開始, false:停止 </param>
  /// <returns></returns>
  void SetMoveStart(bool flag) { is_start_ = flag; }

  /// <summary>
  /// 動作開始フラグが立っているか確認
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:動作開始, false:動作停止 </returns>
  bool IsStart() { return is_start_ == true; }

  /// <summary>
  /// 描画終了フラグを立てる
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void FinishDraw();

  /// <summary>
  /// 描画終了フラグが立っているか確認
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:描画終了, false:処理中 </returns>
  bool IsFinish() { return is_finish_ == true; }

  /// <summary>
  /// 透過率変更処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AdjustAlphaValue();

  /// <summary>
  /// 透過率のセット
  /// </summary>
  /// <param name=""> 透過率 </param>
  /// <returns></returns>
  void SetAlphaValue(int value) { bullet_data_.alpha.value = value; }


private:

  /// <summary>
  /// バレットID
  /// </summary>
  BulletId bullet_id_;

  /// <summary>
  /// 発射キャラクター
  /// </summary>
  Character* character_;

  /// <summary>
  /// バレットイベントインターフェース
  /// </summary>
  BulletEventInterface* bullet_event_interface_;

  /// <summary>
  /// グラフィックハンドル
  /// </summary>
  int graphic_handle_;

  /// <summary>
  /// X座標
  /// </summary>
  float x_pos_;

  /// <summary>
  /// Y座標
  /// </summary>
  float y_pos_;

  /// <summary>
  /// 弾の情報
  /// </summary>
  BulletData bullet_data_;

  /// <summary>
  /// 画像のサイズ 幅
  /// </summary>
  int image_width_;

  /// <summary>
  /// 画像のサイズ 高さ
  /// </summary>
  int image_height_;

  /// <summary>
  /// 画像の幅の半分
  /// </summary>
  int image_width_half_;

  /// <summary>
  /// 画像の高さの半分
  /// </summary>
  int image_height_half_;

  /// <summary>
  /// 弾の半径
  /// </summary>
  float radius_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// 動作開始フラグ
  /// </summary>
  bool is_start_;

  /// <summary>
  /// 描画終了フラグ
  /// </summary>
  bool is_finish_;
};