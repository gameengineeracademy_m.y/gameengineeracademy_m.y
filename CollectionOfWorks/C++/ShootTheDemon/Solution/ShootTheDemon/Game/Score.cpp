﻿#include "Game/Score.h"

namespace {

  /// <summary>
  /// スコアデータファイル
  /// </summary>
  const char* kScoreDataFilePath = "File/ScoreData.txt";

  /// <summary>
  /// 10000
  /// </summary>
  const int kTenThousand = 10000;

  /// <summary>
  /// 1000
  /// </summary>
  const float kThousandf = 1000.0f;

  /// <summary>
  /// 1000
  /// </summary>
  const int kThousand = 1000;

  /// <summary>
  /// 100
  /// </summary>
  const int kHundred = 100;

  /// <summary>
  /// 10
  /// </summary>
  const int kTen = 10;

  /// <summary>
  /// 1
  /// </summary>
  const int kOne = 1;

  /// <summary>
  /// 60秒 = 1分 変換
  /// </summary>
  const int kChangeMinutes = 60;

  /// <summary>
  /// 表示時刻最大値
  /// </summary>
  const char* kMaxTimeText = "99:59.999";

  /// <summary>
  /// 表示時刻最小値
  /// </summary>
  const int kMinTime = 0;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjust = 5;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 ボード最大値
  /// </summary>
  const int kAlphaBoardMax = 210;

  /// <summary>
  /// 透過率 背景最大値
  /// </summary>
  const int kAlphaBackMax = 210;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaTextMax = 255;

  /// <summary>
  /// ランキングボード表示位置 X座標
  /// </summary>
  const int kRankingBoardPosX = 505;

  /// <summary>
  /// ランキングボード表示位置 Y座標
  /// </summary>
  const int kRankingBoardPosY = 200;

  /// <summary>
  /// ランキングボード幅
  /// </summary>
  const int kRankingBoardWidth = 400;

  /// <summary>
  /// ランキングボード高さ
  /// </summary>
  const int kRankingBoardHeight = 350;

  /// <summary>
  /// テキスト表示位置 Y座標
  /// </summary>
  const int kTextPosY = 17;

  /// <summary>
  /// 背景 表示位置 X座標
  /// </summary>
  const int kBackPosX = 224;

  /// <summary>
  /// 背景 表示位置 Y座標
  /// </summary>
  const int kBackPosY = 0;

  /// <summary>
  /// 背景 幅
  /// </summary>
  const int kBackWidth = 576;

  /// <summary>
  /// 背景 高さ
  /// </summary>
  const int kBackHeight = 768;

  /// <summary>
  /// ボード 色
  /// </summary>
  const int kBoardColor = GetColor(200, 0, 255);

  /// <summary>
  /// 文字 色
  /// </summary>
  const int kTitleTextColor = GetColor(255, 255, 0);

  /// <summary>
  /// 文字 色
  /// </summary>
  const int kTimeTextColor = GetColor(255, 255, 255);

  /// <summary>
  /// 背景 色
  /// </summary>
  const int kBackColor = GetColor(0, 0, 0);

  /// <summary>
  /// 案内文 文字色
  /// </summary>
  const int kGuideForeColor = GetColor(255, 255, 255);

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Reggae One";

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kGuideTextFontSize = 20;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kRankingTitleTextFontSize = 35;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kTextFontSize = 40;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 1;

  /// <summary>
  /// タイトル Ranking 表示位置
  /// </summary>
  const int kRankingTextPosY = 40;

  /// <summary>
  /// タイトル ランキング1位タイム 表示位置
  /// </summary>
  const int kRankingFirstTextPosY = 315;

  /// <summary>
  /// タイトル ランキング2位タイム 表示位置
  /// </summary>
  const int kRankingSecondTextPosY = 385;

  /// <summary>
  /// タイトル ランキング3位タイム 表示位置
  /// </summary>
  const int kRankingThirdTextPosY = 455;

  /// <summary>
  /// 案内文
  /// </summary>
  const int kGuideTextPosY = 515;

  /// <summary>
  /// タイトル RANKING
  /// </summary>
  const char* kRankingText = "RANKING BOARD";

  /// <summary>
  /// タイトル RANKING 1st
  /// </summary>
  const char* kRankingFirstText = "1st : ";

  /// <summary>
  /// タイトル RANKING 2nd
  /// </summary>
  const char* kRankingSecondText = "2nd : ";

  /// <summary>
  /// タイトル RANKING 3rd
  /// </summary>
  const char* kRankingThirdText = "3rd : ";

  /// <summary>
  /// 案内文
  /// </summary>
  const char* kGuideText = "Return : Esc or X Key";

  /// <summary>
  /// 案内文 点滅
  /// </summary>
  const float kGuideDispInterval = 1.0f;

  /// <summary>
  /// 累積時間 リセット値
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Score::Score()
  : Task(TaskId::kScoreBoard)
  , current_phase_(PhaseType::kInitialize)
  , prepare_phase_(PreparePhase::kSetRanking)
  , time_()
  , time_pos_()
  , time_forecolor_()
  , ranking_board_()
  , back_board_()
  , text_ranking_()
  , text_ranking_first_()
  , text_ranking_second_()
  , text_ranking_third_()
  , text_guide_()
  , alpha_guide_(kAlphaTextMax)
  , accumulate_time_(0.0f)
  , font_handle_() {

  // コンソールに出力
  std::cout << "ScoreBoard コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Score::~Score() {

  // コンソールに出力
  std::cout << "~ScoreBoard デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void Score::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //-------------------------------
    //初期化処理フェーズ
    //-------------------------------
    bool is_finish = Initialize();
    if (is_finish) {
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //-------------------------------
    //処理中フェーズ
    //-------------------------------
    Process(process_time);
    break;
  }
  case PhaseType::kFinalize: {
    //-------------------------------
    //終了処理フェーズ
    //-------------------------------
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Score::Render() {

  switch (current_phase_) {
  case PhaseType::kProcess: {

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, kAlphaBackMax);

    DrawBox(back_board_.left.x, back_board_.left.y,
            back_board_.right.x, back_board_.right.y, kBackColor, true);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);


    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, kAlphaBoardMax);

    DrawBox(ranking_board_.left.x, ranking_board_.left.y,
            ranking_board_.right.x, ranking_board_.right.y, kBoardColor, true);

    DrawStringToHandle(text_ranking_.x, text_ranking_.y, kRankingText,
                       kTitleTextColor, font_handle_[static_cast<int>(FontSize::k35)]);

    DrawStringToHandle(time_pos_[TimeType::kRankingFirst].x, time_pos_[TimeType::kRankingFirst].y,
                       time_text_[TimeType::kRankingFirst].c_str(), time_forecolor_[TimeType::kRankingFirst],
                       font_handle_[static_cast<int>(FontSize::k40)]);

    DrawStringToHandle(time_pos_[TimeType::kRankingSecond].x, time_pos_[TimeType::kRankingSecond].y,
                       time_text_[TimeType::kRankingSecond].c_str(), time_forecolor_[TimeType::kRankingSecond],
                       font_handle_[static_cast<int>(FontSize::k40)]);

    DrawStringToHandle(time_pos_[TimeType::kRankingThird].x, time_pos_[TimeType::kRankingThird].y,
                       time_text_[TimeType::kRankingThird].c_str(), time_forecolor_[TimeType::kRankingThird],
                       font_handle_[static_cast<int>(FontSize::k40)]);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_guide_);

    DrawStringToHandle(text_guide_.x, text_guide_.y, kGuideText,
                       kGuideForeColor, font_handle_[static_cast<int>(FontSize::k20)]);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    break;
  }
  }
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param names=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool Score::Initialize() {

  switch (prepare_phase_) {
  case PreparePhase::kSetRanking: {
    //-------------------------------
    //ランキング準備フェーズ
    //-------------------------------
    // ファイルからタイムを取得する
    LoadSetupFile();

    // 文字色を全て白色に設定
    time_forecolor_[TimeType::kRankingFirst] = kTimeTextColor;
    time_forecolor_[TimeType::kRankingSecond] = kTimeTextColor;
    time_forecolor_[TimeType::kRankingThird] = kTimeTextColor;

    // クリアタイムとランキングのタイムを比較して順位を確定
    int first_time = time_[TimeType::kRankingFirst];
    int second_time = time_[TimeType::kRankingSecond];
    int third_time = time_[TimeType::kRankingThird];

    // ランキングの再作成
    time_[TimeType::kRankingFirst] = first_time;
    time_[TimeType::kRankingSecond] = second_time;
    time_[TimeType::kRankingThird] = third_time;

    // 各タイムを描画用に変換する
    for (int i = 0; i < static_cast<int>(TimeType::kTypeMaxIndex); ++i) {

      TimeType time_type = static_cast<TimeType>(i);
      
      // タイマーの時間を「分・秒・ミリ秒」に変換
      int minutes_value = static_cast<int>(time_[time_type] / (kChangeMinutes * kThousand));
      if (minutes_value >= kHundred) {
        time_text_[time_type] = kMaxTimeText;
        continue;
      }
      int minutes_tens = minutes_value / kTen % kTen;
      int minutes_ones = minutes_value / kOne % kTen;

      int seconds_value = time_[time_type] - minutes_value * kChangeMinutes * kThousand;

      int seconds_tens = seconds_value / kTenThousand % kTen;
      int seconds_ones = seconds_value / kThousand % kTen;
      int milliseconds_hundreds = seconds_value / kHundred % kTen;
      int milliseconds_tens = seconds_value / kTen % kTen;
      int milliseconds_ones = seconds_value / kOne % kTen;

      // タイムをテキストでセットする
      time_text_[time_type] = std::to_string(minutes_tens) + std::to_string(minutes_ones) + ":" +
                              std::to_string(seconds_tens) + std::to_string(seconds_ones) + "." +
                              std::to_string(milliseconds_hundreds) +
                              std::to_string(milliseconds_tens) + std::to_string(milliseconds_ones);
    }

    // 準備フェーズを「初期化処理」に変更
    ChangePreparePhase(PreparePhase::kInitialize);
    break;
  }
  case PreparePhase::kInitialize: {
    //-------------------------------
    //初期化処理フェーズ
    //-------------------------------
    // ゲーム情報から画面の中心位置を取得
    GameInfo* game_info = GameInfo::GetGameInfoInstance();
    if (game_info == nullptr) {
      return false;
    }
    int x_pos_center = game_info->GetCenterPointX();
    int y_pos_center = game_info->GetCenterPointY();

    // フォント情報生成
    FontInfo* font_info = FontInfo::GetFontInfoInstance();
    if (font_info == nullptr) {
      return false;
    }
    font_handle_[static_cast<int>(FontSize::k20)] = font_info->GetFontInfo(kFontName, kGuideTextFontSize, kFontWidth, DX_FONTTYPE_NORMAL);
    font_handle_[static_cast<int>(FontSize::k35)] = font_info->GetFontInfo(kFontName, kTextFontSize, kFontWidth, DX_FONTTYPE_NORMAL);
    font_handle_[static_cast<int>(FontSize::k40)] = font_info->GetFontInfo(kFontName, kRankingTitleTextFontSize, kFontWidth, DX_FONTTYPE_NORMAL);


    //-------------------------------------------
    // ボード
    //-------------------------------------------
    ranking_board_.size.width = kRankingBoardWidth;
    ranking_board_.size.height = kRankingBoardHeight;
    ranking_board_.left.x = x_pos_center - ranking_board_.size.width / kHalfValue;
    ranking_board_.left.y = y_pos_center - ranking_board_.size.height / kHalfValue;
    ranking_board_.right.x = ranking_board_.left.x + ranking_board_.size.width;
    ranking_board_.right.y = ranking_board_.left.y + ranking_board_.size.height;


    //-------------------------------------------
    // 表示テキスト
    //-------------------------------------------
    int text_width = GetDrawStringWidthToHandle(kRankingText, static_cast<int>(strlen(kRankingText)), font_handle_[static_cast<int>(FontSize::k35)]);
    text_ranking_.x = (ranking_board_.left.x + ranking_board_.right.x) / kHalfValue - text_width / kHalfValue;;
    text_ranking_.y = ranking_board_.left.y + kRankingTextPosY;


    //-------------------------------------------
    // タイム
    //-------------------------------------------
    time_text_[TimeType::kRankingFirst] = kRankingFirstText + time_text_[TimeType::kRankingFirst];
    text_width = GetDrawStringWidthToHandle(time_text_[TimeType::kRankingFirst].c_str(),
                                            static_cast<int>(strlen(time_text_[TimeType::kRankingFirst].c_str())),
                                            font_handle_[static_cast<int>(FontSize::k40)]);
    time_pos_[TimeType::kRankingFirst].x = (ranking_board_.left.x + ranking_board_.right.x) / kHalfValue - text_width / kHalfValue;
    time_pos_[TimeType::kRankingFirst].y = kRankingFirstTextPosY;

    time_text_[TimeType::kRankingSecond] = kRankingSecondText + time_text_[TimeType::kRankingSecond];
    text_width = GetDrawStringWidthToHandle(time_text_[TimeType::kRankingSecond].c_str(),
                                            static_cast<int>(strlen(time_text_[TimeType::kRankingSecond].c_str())),
                                            font_handle_[static_cast<int>(FontSize::k40)]);
    time_pos_[TimeType::kRankingSecond].x = (ranking_board_.left.x + ranking_board_.right.x) / kHalfValue - text_width / kHalfValue;
    time_pos_[TimeType::kRankingSecond].y = kRankingSecondTextPosY;

    time_text_[TimeType::kRankingThird] = kRankingThirdText + time_text_[TimeType::kRankingThird];
    text_width = GetDrawStringWidthToHandle(time_text_[TimeType::kRankingThird].c_str(),
                                            static_cast<int>(strlen(time_text_[TimeType::kRankingThird].c_str())),
                                            font_handle_[static_cast<int>(FontSize::k40)]);
    time_pos_[TimeType::kRankingThird].x = (ranking_board_.left.x + ranking_board_.right.x) / kHalfValue - text_width / kHalfValue;
    time_pos_[TimeType::kRankingThird].y = kRankingThirdTextPosY;


    //-------------------------------------------
    // 背景
    //-------------------------------------------
    back_board_.size.width = kBackWidth;
    back_board_.size.height = kBackHeight;
    back_board_.left.x = kBackPosX;
    back_board_.left.y = kBackPosY;
    back_board_.right.x = back_board_.left.x + back_board_.size.width;
    back_board_.right.y = back_board_.left.y + back_board_.size.height;


    //-------------------------------------------
    // 案内文
    //-------------------------------------------
    text_width = GetDrawStringWidthToHandle(kGuideText, static_cast<int>(strlen(kGuideText)),
                                            font_handle_[static_cast<int>(FontSize::k20)]);
    text_guide_.x = x_pos_center - (text_width / kHalfValue);
    text_guide_.y = kGuideTextPosY;

    // 準備フェーズを「初期化処理済み」に変更
    ChangePreparePhase(PreparePhase::kInitialized);

    return true;
  }
  }

  return false;
}

/// <summary>
/// 処理中
/// </summary>
/// <param names="process_time"> 処理時間 </param>
/// <returns></returns>
void Score::Process(float process_time) {

  // 累積時間を加算
  accumulate_time_ += process_time;
  if (accumulate_time_ >= kGuideDispInterval) {

    // 累積時間をリセット
    accumulate_time_ = kResetTime;

    if (alpha_guide_ > kAlphaMin) {
      alpha_guide_ = kAlphaMin;
    }
    else {
      alpha_guide_ = kAlphaTextMax;
    }
  }
}

/// <summary>
/// 設定ファイル読み込み
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool Score::LoadSetupFile() {

  //-----------------------------------------
  //スコアファイル
  //-----------------------------------------
  std::string file_path = kScoreDataFilePath;
  // ファイルをロードし、データを取得する
  bool is_finish = LoadFileScoreData(file_path);
  if (!is_finish) {
    return false;
  }

  return true;
}

/// <summary>
/// ロードファイル スコアデータ
/// </summary>
/// <param name="file_path"> ファイルパス </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool Score::LoadFileScoreData(std::string file_path) {

  // ファイルストリームを宣言と同時にファイルを開く
  std::ifstream stream(file_path, std::ios::in);
  //ファイルが開けないならエラー
  if (!stream) {
    time_[TimeType::kRankingFirst] = kMinTime;
    time_[TimeType::kRankingSecond] = kMinTime;
    time_[TimeType::kRankingThird] = kMinTime;
    return true;
  }

  //ファイルから読み込む
  std::string line;
  //１行ずつ読み出す
  while (std::getline(stream, line)) {
    std::cout << line << std::endl;
    int equal_pos = 0;
    if (line.find("First=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      time_[TimeType::kRankingFirst] = std::stoi(str_value);
    }
    else if (line.find("Second=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      time_[TimeType::kRankingSecond] = std::stoi(str_value);
    }
    else if (line.find("Third=") != std::string::npos) {
      equal_pos = static_cast<int>(line.find("="));
      std::string str_value = line.substr(++equal_pos);
      // 設定値セット
      time_[TimeType::kRankingThird] = std::stoi(str_value);
    }
  }

  //ファイルを閉じる
  stream.close();
  line.clear();
  stream.clear();

  return true;
}