﻿#include "Game/EnemyBase.h"

namespace {

  /// <summary>
  /// 符号反転
  /// </summary>
  const int kMinusSign = -1;
}

/// <summary>
/// コンストラクタ  初期化処理
/// </summary>
/// <param name="task_id"> タスクID </param>
/// <param name="character_type"> キャラクターの種類 </param>
/// <param name="enemy_eventinterface"> エネミーイベントインターフェイス </param>
/// <param name="character_controller_event_interface"> キャラクターコントローライベントインターフェイス </param>
/// <returns></returns>
EnemyBase::EnemyBase(TaskId task_id, CharacterType character_type,
                     EnemyEventInterface& enemy_eventinterface,
                     CharacterControllerEventInterface& character_controller_event_interface,
                     CharacterEventInterface& character_event_interface)
  : Character(task_id, character_type, character_event_interface)
  , move_direction_()
  , enemy_event_interface_(enemy_eventinterface)
  , enemy_controller_(character_controller_event_interface, this)
  , hit_point_()
  , x_pos_start_(0)
  , y_pos_start_(0)
  , x_pos_goal_(0)
  , y_pos_goal_(0)
  , alpha_(0)
  , alpha_adjust_(0)
  , alpha_max_(0)
  , alpha_min_(0)
  , angle_(0.0f)
  , graphic_handle_(0) {

  // コンソールに出力
  std::cout << "EnemyBase コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
EnemyBase::~EnemyBase() {

  // コンソールに出力
  std::cout << "~EnemyBase デストラクタ" << std::endl;
}

/// <summary>
/// 毎フレームの描画処理
/// </summary>
/// <param></param>
/// <returns></returns>
void EnemyBase::Render() {

  int x_pos = GetPositionX();
  int y_pos = GetPositionY();
  float disp_rate = GetImageDispRate();

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

  DrawRotaGraph(x_pos, y_pos, disp_rate, angle_, graphic_handle_, true);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, alpha_min_);
}

/// <summary>
/// 移動できる向きリストをセット
/// </summary>
/// <param name="move_direction"> 移動できる向きの配列 </param>
/// <returns></returns>
void EnemyBase::SetMoveDirection(std::vector<DirectionType> move_direction) {

  move_direction_ = move_direction;
}

/// <summary>
/// 透過率の設定
/// </summary>
/// <param names="value"> 透過率 </param>
/// <param names="adjust"> 調整値 </param>
/// <param names="max"> 最大値 </param>
/// <param names="min"> 最小値 </param>
/// <returns></returns>
void EnemyBase::SetAlpha(int value, int adjust, int max, int min) {

  alpha_ = value;
  alpha_adjust_ = adjust;
  alpha_max_ = max;
  alpha_min_ = min;
}

/// <summary>
/// 弾幕射撃処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void EnemyBase::ShootBullet() {

  // 弾の発射処理
  enemy_controller_.ShootBullet();
}

/// <summary>
/// 透過率変更処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool EnemyBase::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率に調整値を加算
  alpha_ += alpha_adjust_;

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= alpha_max_) {
    alpha_ = alpha_max_;
    is_finish = true;
  }
  else if (alpha_ <= alpha_min_) {
    alpha_ = alpha_min_;
    is_finish = true;
  }

  return is_finish;
}

/// <summary>
/// 透過率の符号を変更
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void EnemyBase::ChangeAlphaAdjustSign() {

  alpha_adjust_ = kMinusSign * alpha_adjust_;
}