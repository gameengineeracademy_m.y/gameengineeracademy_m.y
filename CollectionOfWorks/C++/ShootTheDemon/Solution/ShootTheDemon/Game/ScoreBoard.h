﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "System/FontInfo.h"
#include "Game/GameInfo.h"
#include <unordered_map>
#include <array>
#include <fstream>
#include <string>

namespace {

  /// <summary>
  /// フォントサイズ 種類の数
  /// </summary>
  const int kFontSizeScoreBoard = 5;
}

/// <summary>
/// スコアボード
/// </summary>
class ScoreBoard : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kNone,            // 何もしないフェーズ
    kInitialize,      // 初期化処理フェーズ
    kStartWait,       // 起動待機フェーズ
    kStartUp,         // 起動フェーズ
    kProcess,         // 処理中フェーズ
    kBeforeFinalize,  // 終了処理前フェーズ
    kFinalize,        // 終了処理フェーズ
    kFinalized,       // 終了処理済みフェーズ
    kPhaseMaxIndex    // フェーズ数
  };

  /// <summary>
  /// 準備フェーズ
  /// </summary>
  enum class PreparePhase {
    kSetRanking,      // 順位付けフェーズ
    kInitialize,      // 初期化処理フェーズ
    kInitialized,     // 初期化処理済みフェーズ
    kPhaseMaxIndex    // フェーズ数
  };

  /// <summary>
  /// 処理中フェーズ
  /// </summary>
  enum class ProcessPhase {
    kWait,            // 待機フェーズ
    kClearTime,       // クリアタイム表示フェーズ
    kRanking,         // ランキング表示フェーズ
    kResult,          // リザルト演出フェーズ
    kPhaseMaxIndex    // フェーズ数
  };

  /// <summary>
  /// タイムの表示位置
  /// </summary>
  enum class TimeType {
    kResultTime,      // リザルトタイム
    kRankingFirst,    // ランク1位
    kRankingSecond,   // ランク2位
    kRankingThird,    // ランク3位
    kTypeMaxIndex     // 種類数
  };

  /// <summary>
  /// フォントサイズの種類
  /// </summary>
  enum class FontSize {
    k20,            // 文字サイズ20
    k35,            // 文字サイズ35
    k40,            // 文字サイズ40
    k50,            // 文字サイズ50
    k60,            // 文字サイズ60
    kTypeMaxIndex   // 種類数
  };


public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ScoreBoard();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~ScoreBoard();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// タイムのセット
  /// </summary>
  /// <param name=""> タイムの種類 </param>
  /// <param name=""> タイム </param>
  /// <returns></returns>
  void SetTime(TimeType, int);

  /// <summary>
  /// フェーズの種類を変更する
  /// </summary>
  /// <param names="phase_type"> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }

  /// <summary>
  /// 準備フェーズの種類を変更する
  /// </summary>
  /// <param names="phase_type"> フェーズの種類 </param>
  /// <returns></returns>
  void ChangePreparePhase(PreparePhase phase_type) { prepare_phase_ = phase_type; }

  /// <summary>
  /// 処理中フェーズの種類を変更する
  /// </summary>
  /// <param names="phase_type"> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeProcessPhase(ProcessPhase phase_type) { process_phase_ = phase_type; }

  /// <summary>
  /// 現在のフェーズが終了処理済みか確認
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理済み, false:終了処理済み以外 </returns>
  bool IsFinalized() { return current_phase_ == PhaseType::kFinalized; }


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param names=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Initialize();

  /// <summary>
  /// 処理中
  /// </summary>
  /// <param names=""> 処理時間 </param>
  /// <returns></returns>
  void Process(float);

  /// <summary>
  /// スコアデータファイルの更新処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void UpdateScoreDataFile();

  /// <summary>
  /// 透過率の調整
  /// </summary>
  /// <param names=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AdjustAlphaValue();

  /// <summary>
  /// 透過率の調整
  /// </summary>
  /// <param names=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AdjustAlphaValueClearTime();

  /// <summary>
  /// 透過率の調整
  /// </summary>
  /// <param names=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AdjustAlphaValueRanking();


private:

  /// <summary>
  /// 表示座標
  /// </summary>
  struct Pos {
    int x;
    int y;
  };

  /// <summary>
  /// サイズ
  /// </summary>
  struct Size {
    int width;
    int height;
  };

  /// <summary>
  /// 図形
  /// </summary>
  struct Figure {
    Pos left;
    Pos right;
    Size size;
  };

  /// <summary>
  /// インデックス
  /// </summary>
  struct Time {
    int min_tens;
    int min_ones;
    int sec_tens;
    int sec_ones;
    int msec_huns;
    int msec_tens;
    int msec_ones;
    int colon;
    int period;
  };

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 準備フェーズ
  /// </summary>
  PreparePhase prepare_phase_;

  /// <summary>
  /// 処理中フェーズ
  /// </summary>
  ProcessPhase process_phase_;

  /// <summary>
  /// タイム
  /// </summary>
  std::unordered_map<TimeType, int> time_;

  /// <summary>
  /// タイム
  /// </summary>
  std::unordered_map<TimeType, std::string> time_text_;

  /// <summary>
  /// タイムの種類ごとの表示位置
  /// </summary>
  std::unordered_map<TimeType, Pos> time_pos_;

  /// <summary>
  /// タイムの種類ごとの文字色
  /// </summary>
  std::unordered_map<TimeType, int> time_forecolor_;

  /// <summary>
  /// タイトルボード
  /// </summary>
  Figure title_board_;

  /// <summary>
  /// リザルトボード
  /// </summary>
  Figure result_board_;

  /// <summary>
  /// ランキングボード
  /// </summary>
  Figure ranking_board_;

  /// <summary>
  /// 案内文章表示ボード
  /// </summary>
  Figure guide_board_;

  /// <summary>
  /// テキスト タイトル
  /// </summary>
  Pos text_title_;

  /// <summary>
  /// テキスト Clear Time
  /// </summary>
  Pos text_clear_;

  /// <summary>
  /// テキスト ハイスコア
  /// </summary>
  Pos text_high_score_;

  /// <summary>
  /// テキスト Ranking
  /// </summary>
  Pos text_ranking_;

  /// <summary>
  /// テキスト 1st
  /// </summary>
  Pos text_ranking_first_;

  /// <summary>
  /// テキスト 2nd
  /// </summary>
  Pos text_ranking_second_;

  /// <summary>
  /// テキスト 3rd
  /// </summary>
  Pos text_ranking_third_;

  /// <summary>
  /// 案内文1
  /// </summary>
  Pos text_guide_01_;

  /// <summary>
  /// 案内文2
  /// </summary>
  Pos text_guide_02_;

  /// <summary>
  /// タイトル
  /// </summary>
  std::string title_;

  /// <summary>
  /// 透過率 ボード背景
  /// </summary>
  int alpha_board_;

  /// <summary>
  /// 透過率 タイトル
  /// </summary>
  int alpha_text_title_;

  /// <summary>
  /// 透過率 テキスト
  /// </summary>
  int alpha_text_result_;

  /// <summary>
  /// 透過率 ランキング
  /// </summary>
  int alpha_text_ranking_;

  /// <summary>
  /// 透過率 ハイスコア
  /// </summary>
  int alpha_high_score_;

  /// <summary>
  /// 透過率 案内ボード
  /// </summary>
  int alpha_guide_;

  /// <summary>
  /// 透過率 案内ボード文字
  /// </summary>
  int alpha_text_guide_;

  /// <summary>
  /// フォントハンドル
  /// </summary>
  int font_handle_[kFontSizeScoreBoard];

  /// <summary>
  /// ハイスコアフラグ
  /// </summary>
  bool is_high_score_;

  /// <summary>
  /// 初回実行フラグ
  /// </summary>
  bool is_first_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;
};