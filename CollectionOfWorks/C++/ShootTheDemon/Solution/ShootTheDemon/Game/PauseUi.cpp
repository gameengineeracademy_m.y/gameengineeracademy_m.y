﻿#include "Game/PauseUi.h"

namespace {

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Reggae One";

  /// <summary>
  /// タイトル文字フォントサイズ
  /// </summary>
  const int kTitleFontSize = 35;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kTextFontSize = 22;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 1;

  /// <summary>
  /// スクリーンサイズ 幅
  /// </summary>
  const int kScreenWidth = 300;

  /// <summary>
  /// スクリーンサイズ 高さ
  /// </summary>
  const int kScreenHeight = 350;

  /// <summary>
  /// ボタンサイズ 幅
  /// </summary>
  const int kButtonWidth = 180;

  /// <summary>
  /// ボタンサイズ 高さ
  /// </summary>
  const int kButtonHeight = 50;

  /// <summary>
  /// ボタン表示位置 Y座標
  /// </summary>
  const int kButtonResumePosY = 100;

  /// <summary>
  /// ボタン表示位置 Y座標 調整間隔
  /// </summary>
  const int kButtonPosYAdjust = 65;

  /// <summary>
  /// メニューテキスト表示位置
  /// </summary>
  const int kScreenTextPosY = 150;

  /// <summary>
  /// ボタンテキスト表示位置 調整値
  /// </summary>
  const int kButtonTextPosY = 14;

  /// <summary>
  /// 半分
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// メニュー画面 背景透過率
  /// </summary>
  const int kAlphaScreen = 190;

  /// <summary>
  /// 透過率 背景 最大値
  /// </summary>
  const int kAlphaBackMax = 210;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// メニュー画面 背景色
  /// </summary>
  const int kScreenColor = GetColor(200, 0, 255);

  /// <summary>
  /// メニュー画面 メニューテキスト文字色
  /// </summary>
  const int kScreenTextForeColor = GetColor(255, 255, 0);

  /// <summary>
  /// メニュー画面 ボタンテキスト文字色
  /// </summary>
  const int kButtonTextForeColor = GetColor(255, 255, 255);

  /// <summary>
  /// メニュー画面 ボタン色 選択中
  /// </summary>
  const int kButtonSelectColor = GetColor(190, 90, 255);

  /// <summary>
  /// メニュー画面 ボタン色 選択外
  /// </summary>
  const int kButtonNoneColor = GetColor(100, 0, 200);

  /// <summary>
  /// メニュー画面 カーソル色
  /// </summary>
  const int kCursorColor = GetColor(0, 255, 0);

  /// <summary>
  /// 背景
  /// </summary>
  const int kBackColor = GetColor(0, 0, 0);

  /// <summary>
  /// カーソル調整値 固定値
  /// </summary>
  const float kCursorAdjust = 1.0f;

  /// <summary>
  /// カーソル調整値 X方向
  /// </summary>
  const float kCursorPlusAdjustX = 1.5f;

  /// <summary>
  /// カーソル調整値 Y方向
  /// </summary>
  const float kCursorPlusAdjustY = 1.5f;

  /// <summary>
  /// カーソル調整値 X方向
  /// </summary>
  const float kCursorMinusAdjustX = -1.5f;

  /// <summary>
  /// カーソル調整値 Y方向
  /// </summary>
  const float kCursorMinusAdjustY = -1.5f;

  /// <summary>
  /// カーソル 幅
  /// </summary>
  const float kCursorThick = 3.0f;

  /// <summary>
  /// メニューテキスト
  /// </summary>
  const char* kScreenText = "PAUSE MENU";

  /// <summary>
  /// ボタンテキスト
  /// </summary>
  const char* kButtonTextResume = "RESUME";

  /// <summary>
  /// ボタンテキスト
  /// </summary>
  const char* kButtonTextSetup = "OPTION";

  /// <summary>
  /// ボタンテキスト
  /// </summary>
  const char* kButtonTextRetry = "RETRY";

  /// <summary>
  /// ボタンテキスト
  /// </summary>
  const char* kButtonTextTitle = "RETURN TITLE";

  /// <summary>
  /// 待機時間
  /// </summary>
  const float kWaitTime = 0.7f;

  /// <summary>
  /// リセット時間
  /// </summary>
  const float kResetTime = 0.0f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
PauseUi::PauseUi()
  : Task(TaskId::kPauseUi)
  , current_phase_(PhaseType::kInitialize)
  , button_type_(ButtonType::kResume)
  , screen_()
  , button_resume_()
  , button_setup_()
  , button_retry_()
  , button_title_()
  , back_board_()
  , cursor_()
  , cursor_adjust_()
  , text_screen_()
  , text_button_resume_()
  , text_button_setup_()
  , text_button_retry_()
  , text_button_title_()
  , button_color_()
  , accumulate_time_(0.0f)
  , font_handle_() {

  // コンソールに出力
  std::cout << "PauseUi コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
PauseUi::~PauseUi() {

  // コンソールに出力
  std::cout << "~PauseUi デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void PauseUi::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //------------------------------
    //初期化処理フェーズ
    //------------------------------
    bool is_finish = Initialize();
    if (is_finish) {
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //------------------------------
    //処理中フェーズ
    //------------------------------
    Process(process_time);
    break;
  }
  case PhaseType::kFinalize: {
    //------------------------------
    //終了処理フェーズ
    //------------------------------
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }

}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void PauseUi::Render() {

  switch (current_phase_) {
  case PhaseType::kProcess: {

    //-------------------------------------------
    // 背景
    //-------------------------------------------
    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, kAlphaBackMax);
    DrawBox(back_board_.left.x, back_board_.left.y,
            back_board_.right.x, back_board_.right.y, kBackColor, true);
    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    //-------------------------------------------
    // スクリーン
    //-------------------------------------------
    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, kAlphaScreen);
    DrawBox(screen_.left.x, screen_.left.y, screen_.right.x, screen_.right.y, kScreenColor, true);
    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    //-------------------------------------------
    // ボタン
    //-------------------------------------------
    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, kAlphaMax);

    // 再開
    DrawBox(button_resume_.left.x, button_resume_.left.y,
            button_resume_.right.x, button_resume_.right.y,
            button_color_[static_cast<int>(ButtonType::kResume)], true);
    // 設定
    DrawBox(button_setup_.left.x, button_setup_.left.y,
            button_setup_.right.x, button_setup_.right.y,
            button_color_[static_cast<int>(ButtonType::kSetup)], true);
    // リトライ
    DrawBox(button_retry_.left.x, button_retry_.left.y,
            button_retry_.right.x, button_retry_.right.y,
            button_color_[static_cast<int>(ButtonType::kRetry)], true);
    // タイトルへ戻る
    DrawBox(button_title_.left.x, button_title_.left.y,
            button_title_.right.x, button_title_.right.y,
            button_color_[static_cast<int>(ButtonType::kGoToTitle)], true);

    //-------------------------------------------
    // テキスト
    //-------------------------------------------
    DrawStringToHandle(text_screen_.x, text_screen_.y, kScreenText, kScreenTextForeColor, font_handle_.at(static_cast<int>(FontSize::k35)));

    DrawStringToHandle(text_button_resume_.x, text_button_resume_.y, kButtonTextResume, kButtonTextForeColor, font_handle_.at(static_cast<int>(FontSize::k22)));
    DrawStringToHandle(text_button_setup_.x, text_button_setup_.y, kButtonTextSetup, kButtonTextForeColor, font_handle_.at(static_cast<int>(FontSize::k22)));
    DrawStringToHandle(text_button_retry_.x, text_button_retry_.y, kButtonTextRetry, kButtonTextForeColor, font_handle_.at(static_cast<int>(FontSize::k22)));
    DrawStringToHandle(text_button_title_.x, text_button_title_.y, kButtonTextTitle, kButtonTextForeColor, font_handle_.at(static_cast<int>(FontSize::k22)));

    //-------------------------------------------
    // カーソル
    //-------------------------------------------
    // 左
    DrawLineAA(cursor_.left.x, cursor_.left.y, cursor_.left.x,
               cursor_.right.y, kCursorColor, kCursorThick);
    // 右
    DrawLineAA(cursor_.right.x - kCursorAdjust, cursor_.left.y,
               cursor_.right.x - kCursorAdjust, cursor_.right.y, kCursorColor, kCursorThick);
    // 上
    DrawLineAA(cursor_.left.x - kCursorAdjust, cursor_.left.y,
               cursor_.right.x + kCursorAdjust, cursor_.left.y, kCursorColor, kCursorThick);
    // 下
    DrawLineAA(cursor_.left.x - kCursorAdjust, cursor_.right.y,
               cursor_.right.x + kCursorAdjust, cursor_.right.y, kCursorColor, kCursorThick);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    break;
  }
  }
}

/// <summary>
/// 初期設定
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理継続 </returns>
bool PauseUi::Initialize() {

  // ゲーム情報から画面の中心位置を取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return false;
  }
  int x_pos_center = game_info->GetCenterPointX();
  int y_pos_center = game_info->GetCenterPointY();
  int x_pos_map = game_info->GetMapPositionX();
  int y_pos_map = game_info->GetMapPositionY();
  int map_width = game_info->GetMapWidth();
  int map_height = game_info->GetMapHeight();

  // フォント情報生成
  FontInfo* font_info = FontInfo::GetFontInfoInstance();
  if (font_info == nullptr) {
    return false;
  }
  int font_handle = font_info->GetFontInfo(kFontName, kTextFontSize, kFontWidth, DX_FONTTYPE_NORMAL);
  font_handle_.push_back(font_handle);
  font_handle = font_info->GetFontInfo(kFontName, kTitleFontSize, kFontWidth, DX_FONTTYPE_NORMAL);
  font_handle_.push_back(font_handle);

  // ポーズメニュー画面 本体
  screen_.left.x = x_pos_center - (kScreenWidth / kHalfValue);
  screen_.left.y = y_pos_center - (kScreenHeight / kHalfValue);
  screen_.right.x = screen_.left.x + kScreenWidth;
  screen_.right.y = screen_.left.y + kScreenHeight;

  //  ポーズメニュー画面 ボタン
  button_resume_.left.x = x_pos_center - (kButtonWidth / kHalfValue);
  button_resume_.left.y = y_pos_center - kButtonResumePosY;
  button_resume_.right.x = button_resume_.left.x + kButtonWidth;
  button_resume_.right.y = button_resume_.left.y + kButtonHeight;

  button_setup_.left.x = x_pos_center - (kButtonWidth / kHalfValue);
  button_setup_.left.y = button_resume_.left.y + kButtonPosYAdjust;
  button_setup_.right.x = button_setup_.left.x + kButtonWidth;
  button_setup_.right.y = button_setup_.left.y + kButtonHeight;

  button_retry_.left.x = x_pos_center - (kButtonWidth / kHalfValue);
  button_retry_.left.y = button_setup_.left.y + kButtonPosYAdjust;
  button_retry_.right.x = button_retry_.left.x + kButtonWidth;
  button_retry_.right.y = button_retry_.left.y + kButtonHeight;
  
  button_title_.left.x = x_pos_center - (kButtonWidth / kHalfValue);
  button_title_.left.y = button_retry_.left.y + kButtonPosYAdjust;
  button_title_.right.x = button_title_.left.x + kButtonWidth;
  button_title_.right.y = button_title_.left.y + kButtonHeight;

  //  ポーズメニュー画面 メニュー・ボタンテキスト
  // メニューテキスト
  int text_width = GetDrawStringWidthToHandle(kScreenText, static_cast<int>(strlen(kScreenText)), font_handle_.at(static_cast<int>(FontSize::k35)));
  text_screen_.x = x_pos_center - (text_width / kHalfValue);
  text_screen_.y = y_pos_center - kScreenTextPosY;

  // ボタンテキスト
  text_width = GetDrawStringWidthToHandle(kButtonTextResume, static_cast<int>(strlen(kButtonTextResume)), font_handle_.at(static_cast<int>(FontSize::k22)));
  text_button_resume_.x = x_pos_center - (text_width / kHalfValue);
  text_button_resume_.y = button_resume_.left.y + kButtonTextPosY;
  text_width = GetDrawStringWidthToHandle(kButtonTextSetup, static_cast<int>(strlen(kButtonTextSetup)), font_handle_.at(static_cast<int>(FontSize::k22)));
  text_button_setup_.x = x_pos_center - (text_width / kHalfValue);
  text_button_setup_.y = button_setup_.left.y + kButtonTextPosY;
  text_width = GetDrawStringWidthToHandle(kButtonTextRetry, static_cast<int>(strlen(kButtonTextRetry)), font_handle_.at(static_cast<int>(FontSize::k22)));
  text_button_retry_.x = x_pos_center - (text_width / kHalfValue);
  text_button_retry_.y = button_retry_.left.y + kButtonTextPosY;
  text_width = GetDrawStringWidthToHandle(kButtonTextTitle, static_cast<int>(strlen(kButtonTextTitle)), font_handle_.at(static_cast<int>(FontSize::k22)));
  text_button_title_.x = x_pos_center - (text_width / kHalfValue);
  text_button_title_.y = button_title_.left.y + kButtonTextPosY;

  // ボタンの色
  button_color_[static_cast<int>(ButtonType::kResume)] = kButtonSelectColor;
  button_color_[static_cast<int>(ButtonType::kSetup)] = kButtonNoneColor;
  button_color_[static_cast<int>(ButtonType::kRetry)] = kButtonNoneColor;
  button_color_[static_cast<int>(ButtonType::kGoToTitle)] = kButtonNoneColor;

  // メニュー画面 カーソル
  cursor_.left.x = static_cast<float>(button_resume_.left.x);
  cursor_.left.y = static_cast<float>(button_resume_.left.y);
  cursor_.right.x = static_cast<float>(button_resume_.left.x + kButtonWidth);
  cursor_.right.y = static_cast<float>(button_resume_.left.y + kButtonHeight);

  // メニュー画面 カーソル調整量
  cursor_adjust_.x = kCursorPlusAdjustX;
  cursor_adjust_.y = kCursorPlusAdjustY;

  // 背景
  back_board_.size.width = map_width;
  back_board_.size.height = map_height;
  back_board_.left.x = x_pos_map;
  back_board_.left.y = y_pos_map;
  back_board_.right.x = back_board_.left.x + back_board_.size.width;
  back_board_.right.y = back_board_.left.y + back_board_.size.height;

  // ボタンの種類をセット
  button_type_ = ButtonType::kResume;

  return true;
}

/// <summary>
/// 処理中処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void PauseUi::Process(float process_time) {

  // 累積時間に加算
  accumulate_time_ += process_time;
  if (accumulate_time_ >= kWaitTime) {
    // 累積時間をリセット
    accumulate_time_ = kResetTime;

    cursor_.left.x -= cursor_adjust_.x;
    cursor_.left.y -= cursor_adjust_.y;
    cursor_.right.x += cursor_adjust_.x;
    cursor_.right.y += cursor_adjust_.y;

    if (cursor_adjust_.x == kCursorPlusAdjustX) {
      cursor_adjust_.x = kCursorMinusAdjustX;
      cursor_adjust_.y = kCursorMinusAdjustY;
    }
    else {
      cursor_adjust_.x = kCursorPlusAdjustX;
      cursor_adjust_.y = kCursorPlusAdjustY;
    }
  }
}

/// <summary>
/// カーソル操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void PauseUi::OperationCursor() {

  float x_pos_left = 0.0f;
  float y_pos_left = 0.0f;
  float x_pos_right = 0.0f;
  float y_pos_right = 0.0f;

  switch (button_type_) {
  case ButtonType::kResume: {
    x_pos_left = static_cast<float>(button_resume_.left.x);
    y_pos_left = static_cast<float>(button_resume_.left.y);
    x_pos_right = static_cast<float>(button_resume_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_resume_.left.y + kButtonHeight);
    break;
  }
  case ButtonType::kSetup: {
    x_pos_left = static_cast<float>(button_setup_.left.x);
    y_pos_left = static_cast<float>(button_setup_.left.y);
    x_pos_right = static_cast<float>(button_setup_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_setup_.left.y + kButtonHeight);
    break;
  }
  case ButtonType::kRetry: {
    x_pos_left = static_cast<float>(button_retry_.left.x);
    y_pos_left = static_cast<float>(button_retry_.left.y);
    x_pos_right = static_cast<float>(button_retry_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_retry_.left.y + kButtonHeight);
    break;
  }
  case ButtonType::kGoToTitle: {
    x_pos_left = static_cast<float>(button_title_.left.x);
    y_pos_left = static_cast<float>(button_title_.left.y);
    x_pos_right = static_cast<float>(button_title_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_title_.left.y + kButtonHeight);
    break;
  }
  }

  cursor_.left.x = x_pos_left;
  cursor_.left.y = y_pos_left;
  cursor_.right.x = x_pos_right;
  cursor_.right.y = y_pos_right;

  for (int i = 0; i < static_cast<int>(ButtonType::kTypeMaxIndex); ++i) {

    if (i == static_cast<int>(button_type_)) {
      button_color_[i] = kButtonSelectColor;
    }
    else {
      button_color_[i] = kButtonNoneColor;
    }
  }
}

/// <summary>
/// カーソル操作 上移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void PauseUi::OperationCursorUp() {

  switch (button_type_) {
  case ButtonType::kResume: {
    button_type_ = ButtonType::kGoToTitle;
    break;
  }
  default: {
    int button_type = static_cast<int>(button_type_);
    --button_type;
    button_type_ = static_cast<ButtonType>(button_type);
    break;
  }
  }

  // カーソルを移動する
  OperationCursor();
}

/// <summary>
/// カーソル操作 下移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void PauseUi::OperationCursorDown() {

  switch (button_type_) {
  case ButtonType::kGoToTitle: {
    button_type_ = ButtonType::kResume;
    break;
  }
  default: {
    int button_type = static_cast<int>(button_type_);
    ++button_type;
    button_type_ = static_cast<ButtonType>(button_type);
    break;
  }
  }

  // カーソルを移動する
  OperationCursor();
}