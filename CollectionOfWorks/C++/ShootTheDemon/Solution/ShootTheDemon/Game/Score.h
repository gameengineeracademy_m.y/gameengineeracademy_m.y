﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "System/FontInfo.h"
#include "Game/GameInfo.h"
#include <unordered_map>
#include <array>
#include <fstream>
#include <string>

namespace {

  /// <summary>
  /// フォントサイズ 種類の数
  /// </summary>
  const int kFontSizeScore = 3;
}

/// <summary>
/// スコア
/// </summary>
class Score : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,      // 初期化処理フェーズ
    kProcess,         // 処理中フェーズ
    kFinalize,        // 終了処理フェーズ
    kFinalized,       // 終了処理済みフェーズ
    kPhaseMaxIndex    // フェーズ数
  };

  /// <summary>
  /// 準備フェーズ
  /// </summary>
  enum class PreparePhase {
    kSetRanking,      // ファイル読み取り
    kInitialize,      // 初期化処理フェーズ
    kInitialized,     // 初期化処理済みフェーズ
    kPhaseMaxIndex    // フェーズ数
  };

  /// <summary>
  /// タイムの表示位置
  /// </summary>
  enum class TimeType {
    kRankingFirst,    // ランク1位
    kRankingSecond,   // ランク2位
    kRankingThird,    // ランク3位
    kTypeMaxIndex     // 種類数
  };

  /// <summary>
  /// フォントサイズの種類
  /// </summary>
  enum class FontSize {
    k20,            // 文字サイズ20
    k35,            // 文字サイズ35
    k40,            // 文字サイズ40
    kTypeMaxIndex   // 種類数
  };


public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  Score();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~Score();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// フェーズの種類を変更する
  /// </summary>
  /// <param names="phase_type"> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }

  /// <summary>
  /// フェーズの種類を変更する
  /// </summary>
  /// <param names="phase_type"> フェーズの種類 </param>
  /// <returns></returns>
  void ChangePreparePhase(PreparePhase phase_type) { prepare_phase_ = phase_type; }

  /// <summary>
  /// 現在のフェーズが終了処理済みか確認
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理済み, false:終了処理済み以外 </returns>
  bool IsFinalized() { return current_phase_ == PhaseType::kFinalized; }


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param names=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Initialize();

  /// <summary>
  /// 処理中
  /// </summary>
  /// <param names=""> 処理時間 </param>
  /// <returns></returns>
  void Process(float);

  /// <summary>
  /// 設定ファイル読み込み
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool LoadSetupFile();

  /// <summary>
  /// ロードファイル スコアデータ
  /// </summary>
  /// <param name=""> ファイルパス </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadFileScoreData(std::string);


private:

  /// <summary>
  /// 表示座標
  /// </summary>
  struct Pos {
    int x;
    int y;
  };

  /// <summary>
  /// サイズ
  /// </summary>
  struct Size {
    int width;
    int height;
  };

  /// <summary>
  /// 図形
  /// </summary>
  struct Figure {
    Pos left;
    Pos right;
    Size size;
  };

  /// <summary>
  /// インデックス
  /// </summary>
  struct Time {
    int min_tens;
    int min_ones;
    int sec_tens;
    int sec_ones;
    int msec_huns;
    int msec_tens;
    int msec_ones;
    int colon;
    int period;
  };

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 準備フェーズ
  /// </summary>
  PreparePhase prepare_phase_;

  /// <summary>
  /// タイム
  /// </summary>
  std::unordered_map<TimeType, int> time_;

  /// <summary>
  /// タイム
  /// </summary>
  std::unordered_map<TimeType, std::string> time_text_;

  /// <summary>
  /// タイムの種類ごとの表示位置
  /// </summary>
  std::unordered_map<TimeType, Pos> time_pos_;

  /// <summary>
  /// タイムの種類ごとの文字色
  /// </summary>
  std::unordered_map<TimeType, int> time_forecolor_;

  /// <summary>
  /// ランキングボード
  /// </summary>
  Figure ranking_board_;

  /// <summary>
  /// 背景
  /// </summary>
  Figure back_board_;

  /// <summary>
  /// テキスト Ranking
  /// </summary>
  Pos text_ranking_;

  /// <summary>
  /// テキスト 1st
  /// </summary>
  Pos text_ranking_first_;

  /// <summary>
  /// テキスト 2nd
  /// </summary>
  Pos text_ranking_second_;

  /// <summary>
  /// テキスト 3rd
  /// </summary>
  Pos text_ranking_third_;

  /// <summary>
  /// テキスト 案内
  /// </summary>
  Pos text_guide_;

  /// <summary>
  /// 透過率 案内文
  /// </summary>
  int alpha_guide_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// フォントハンドル
  /// </summary>
  int font_handle_[kFontSizeScore];
};