﻿#include "Game/ItemBase.h"

namespace {

  /// <summary>
  /// 角度
  /// </summary>
  const float kAngle = 0.0f;

  /// <summary>
  /// 符号反転
  /// </summary>
  const int kMinusSign = -1;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name="task_id"> タスクID </param>
/// <param name="character_type"> キャラクターの種類 </param>
/// <param name="character_controller_event_interface"> キャラクターコントローライベントインターフェース </param>
/// <param name="item_event_interface"> アイテムイベントインターフェース </param>
/// <param name="character_event_interface"> キャラクターイベントインターフェース </param>
/// <returns></returns>
ItemBase::ItemBase(TaskId task_id, CharacterType character_type,
                   CharacterControllerEventInterface& character_controller_event_interface,
                   ItemEventInterface& item_event_interface,
                   CharacterEventInterface& character_event_interface)
  : Character(task_id, character_type, character_event_interface)
  , item_event_interfase_(item_event_interface)
  , item_controller_(character_controller_event_interface, this)
  , graphic_handle_(0)
  , alpha_(0)
  , alpha_adjust_(0)
  , alpha_max_(0)
  , alpha_min_(0)
  , is_finish_(false) {

  // コンソールに出力
  std::cout << "ItemBase コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ItemBase::~ItemBase() {

  // コンソールに出力
  std::cout << "~ItemBase デストラクタ" << std::endl;
}

/// <summary>
/// 毎フレームの描画処理
/// </summary>
/// <param names=""></param>
/// <returns></returns>
void ItemBase::Render() {

  int x_pos = GetPositionX();
  int y_pos = GetPositionY();
  float disp_rate = GetImageDispRate();

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

  DrawRotaGraph(x_pos, y_pos, disp_rate, kAngle, graphic_handle_, true);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, alpha_min_);
}

/// <summary>
/// 透過率の設定
/// </summary>
/// <param names="value"> 透過率 </param>
/// <param names="adjust"> 調整値 </param>
/// <param names="max"> 最大値 </param>
/// <param names="min"> 最小値 </param>
/// <returns></returns>
void ItemBase::SetAlpha(int value, int adjust, int max, int min) {

  alpha_ = value;
  alpha_adjust_ = adjust;
  alpha_max_ = max;
  alpha_min_ = min;
}

/// <summary>
/// 描画フラグを立てる
/// </summary>
/// <param names=""></param>
/// <returns></returns>
void ItemBase::SetFinish() {

  is_finish_ = true;
  // アイテム処理が終了したことを通知する
  item_event_interfase_.OnFinishItem(this);
}

/// <summary>
/// 透過率変更処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool ItemBase::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率に調整値を加算
  alpha_ += alpha_adjust_;

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= alpha_max_) {
    alpha_ = alpha_max_;
    is_finish = true;
  }
  else if (alpha_ <= alpha_min_) {
    alpha_ = alpha_min_;
    is_finish = true;
  }

  return is_finish;
}

/// <summary>
/// 透過率の符号を変更
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ItemBase::ChangeAlphaAdjustSign() {

  alpha_adjust_ = kMinusSign * alpha_adjust_;
}