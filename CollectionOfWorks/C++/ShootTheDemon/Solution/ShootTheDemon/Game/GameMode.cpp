﻿#include "Game/GameMode.h"

namespace {

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSizeTitle = 25;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSizeText = 30;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 1;

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Reggae One";

  /// <summary>
  /// 文字の色
  /// </summary>
  const int kTextForeColorAqua = GetColor(0, 200, 255);      // 水色

  /// <summary>
  /// 文字の色
  /// </summary>
  const int kTextForeColorWhite = GetColor(255, 255, 255);   // 白色

  /// <summary>
  /// テキスト
  /// </summary>
  const char* kDispPlayerDataText = "PLAYER DATA";

  /// <summary>
  /// ゲームデータ 文字描画位置
  /// </summary>
  const int kTitleTextPosX = 810;

  /// <summary>
  /// ゲームデータ 文字描画位置
  /// </summary>
  const int kTitleTextPosY = 570;

  /// <summary>
  /// テキスト
  /// </summary>
  const char* kDispText = "× ";

  /// <summary>
  /// テキスト
  /// </summary>
  const char* kDispStringText = "Lv:";

  /// <summary>
  /// ゲームデータ画像描画位置
  /// </summary>
  const int kDispPosX = 865;

  /// <summary>
  /// ゲームデータ画像描画位置
  /// </summary>
  const int kDisp01PosY = 640;

  /// <summary>
  /// ゲームデータ画像描画位置
  /// </summary>
  const int kDisp02PosY = 705;

  /// <summary>
  /// ゲームデータ 文字描画位置
  /// </summary>
  const int kStringPosX = 910;

  /// <summary>
  /// ゲームデータ 文字描画位置
  /// </summary>
  const int kString01PosY = 625;

  /// <summary>
  /// ゲームデータ 文字描画位置
  /// </summary>
  const int kString02PosY = 695;

  /// <summary>
  /// データ背景 表示座標
  /// </summary>
  const int kScreenPosX = 815;

  /// <summary>
  /// データ背景 表示座標
  /// </summary>
  const int kScreenPosY = 600;

  /// <summary>
  /// データ背景 幅
  /// </summary>
  const int kScreenWidth = 200;

  /// <summary>
  /// データ背景 高さ
  /// </summary>
  const int kScreenHeight = 150;

  /// <summary>
  /// データ背景 背景色
  /// </summary>
  const int kBackColor = GetColor(200, 0, 255);

  /// <summary>
  /// 拡大率 残機
  /// </summary>
  const float kExRateLife = 0.8f;

  /// <summary>
  /// 拡大率 射撃レベル
  /// </summary>
  const float kExRateLevel = 0.4f;

  /// <summary>
  /// 角度
  /// </summary>
  const float kAngle = 0.0f;

  /// <summary>
  /// 画像読み込み時のエラー値
  /// </summary>
  const int kLoadError = -1;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjustInit = 10;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 透過率 背景 最大値
  /// </summary>
  const int kAlphaScreenMax = 150;

  /// <summary>
  /// 符号反転
  /// </summary>
  const int kMinusSign = -1;

  /// <summary>
  /// 射撃レベル 最小値
  /// </summary>
  const int kShootLevelMin = 1;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
GameMode::GameMode()
  : Task(TaskId::kGameMode)
  , current_phase_(PhaseType::kInitialize)
  , game_data_()
  , handle_life_(0)
  , handle_power_up_(0)
  , alpha_(0)
  , alpha_adjust_(0)
  , alpha_max_(0)
  , alpha_min_(0)
  , alpha_screen_(0)
  , font_handle_() {

  // コンソールに出力
  std::cout << "GameMode コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
GameMode::~GameMode() {

  // コンソールに出力
  std::cout << "~GameMode デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void GameMode::Update(float process_time) {
 
  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //-------------------------------
    //初期化処理フェーズ
    //-------------------------------
    Initialize();
    // 現在のフェーズを「起動」に変更
    ChangeCurrentPhase(PhaseType::kStartWait);
    break;
  }
  case PhaseType::kStartWait: {
    //-------------------------------
    //起動待機フェーズ
    //-------------------------------
    break;
  }
  case PhaseType::kStartUp: {
    //-------------------------------
    //起動フェーズ
    //-------------------------------
    // 透過率の変更処理
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 透過率調整値の符号変更
      ChangeAlphaAdjustSign();
      // 現在のフェーズを「プレイ中」に変更
      ChangeCurrentPhase(PhaseType::kPlay);
    }
    break;
  }
  case PhaseType::kPlay: {
    //-------------------------------
    //プレイ中フェーズ
    //-------------------------------
    break;
  }
  case PhaseType::kBeforeFinalize: {
    //-------------------------------
    //終了処理前フェーズ
    //-------------------------------
        // 透過率の変更処理
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //-------------------------------
    //終了処理フェーズ
    //-------------------------------
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void GameMode::Render() {

  switch (current_phase_) {
  case PhaseType::kStartUp:
  case PhaseType::kPlay:
  case PhaseType::kBeforeFinalize: {

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_screen_);
    // 図の描画処理
    DrawBox(kScreenPosX, kScreenPosY, kScreenPosX + kScreenWidth, kScreenPosY + kScreenHeight,
      kBackColor, true);
    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

    // 画像の描画処理
    DrawRotaGraph(kDispPosX, kDisp01PosY, kExRateLife, kAngle, handle_life_, true);
    DrawRotaGraph(kDispPosX, kDisp02PosY, kExRateLevel, kAngle, handle_power_up_, true);

    // 文字列の描画処理
    DrawStringToHandle(kTitleTextPosX, kTitleTextPosY, kDispPlayerDataText, kTextForeColorAqua, font_handle_.at(static_cast<int>(FontSize::k25)));

    std::string left_life = std::to_string(GetLeftLife());
    std::string display_text = kDispText + left_life;
    DrawStringToHandle(kStringPosX, kString01PosY, display_text.c_str(), kTextForeColorWhite, font_handle_.at(static_cast<int>(FontSize::k30)));
    std::string shoot_level = std::to_string(GetShootLevel());
    display_text = kDispStringText + shoot_level;
    DrawStringToHandle(kStringPosX, kString02PosY, display_text.c_str(), kTextForeColorWhite, font_handle_.at(static_cast<int>(FontSize::k30)));

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    break;
  }
  }
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void GameMode::Initialize() {

  // 透過率のセット
  alpha_ = kAlphaMin;
  alpha_adjust_ = kAlphaAdjustInit;
  alpha_max_ = kAlphaMax;
  alpha_min_ = kAlphaMin;
  alpha_screen_ = kAlphaMin;

  // フォント情報生成
  FontInfo* font_info = FontInfo::GetFontInfoInstance();
  if (font_info == nullptr) {
    return;
  }
  int font_handle = font_info->GetFontInfo(kFontName, kFontSizeTitle, kFontWidth, DX_FONTTYPE_NORMAL);
  font_handle_.push_back(font_handle);
  font_handle = font_info->GetFontInfo(kFontName, kFontSizeText, kFontWidth, DX_FONTTYPE_NORMAL);
  font_handle_.push_back(font_handle);
}

/// <summary>
/// 処理中
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void GameMode::Process() {

}

/// <summary>
/// 透過率変更処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool GameMode::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率に調整値を加算
  alpha_ += alpha_adjust_;
  alpha_screen_ += alpha_adjust_;

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_screen_ >= kAlphaScreenMax) {
    alpha_screen_ = kAlphaScreenMax;
  }
  else if (alpha_screen_ <= alpha_min_) {
    alpha_screen_ = alpha_min_;
  }

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= alpha_max_) {
    alpha_ = alpha_max_;
    is_finish = true;
  }
  else if (alpha_ <= alpha_min_) {
    alpha_ = alpha_min_;
    is_finish = true;
  }

  return is_finish;
}

/// <summary>
/// 透過率の符号を変更
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void GameMode::ChangeAlphaAdjustSign() {

  alpha_adjust_ = kMinusSign * alpha_adjust_;
}

/// <summary>
/// 射撃レベルを1つ上げる
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void GameMode::RaiseShootLevel() {

  if (game_data_.level_shoot >= kShootLevelMax) {
    return;
  }

  ++game_data_.level_shoot;
}