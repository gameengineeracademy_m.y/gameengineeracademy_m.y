﻿#pragma once

#include "System/CharacterControllerEventInterface.h"
#include "Game/BarrageId.h"

/// <summary>
/// エネミーコントローラ
/// </summary>
class EnemyController {
public:

  /// <summary>
  /// コンストラクタ
  /// </summary>
  /// <param name=""> キャラクターコントローライベントインターフェース </param>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  EnemyController(CharacterControllerEventInterface&, Character*);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~EnemyController();

  /// <summary>
  /// 上に移動する
  /// </summary>
  /// <param name=""></param>
  /// <returns> true：移動成功, false：移動失敗 </returns>
  bool MoveUp();

  /// <summary>
  /// 下に移動する
  /// </summary>
  /// <param name=""></param>
  /// <returns> true：移動成功, false：移動失敗 </returns>
  bool MoveDown();

  /// <summary>
  /// 左に移動する
  /// </summary>
  /// <param name=""></param>
  /// <returns> true：移動成功, false：移動失敗 </returns>
  bool MoveLeft();

  /// <summary>
  /// 右に移動する
  /// </summary>
  /// <param name=""></param>
  /// <returns> true：移動成功, false：移動失敗 </returns>
  bool MoveRight();

  /// <summary>
  /// 弾を撃つ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ShootBullet();


private:

  /// <summary>
  /// キャラクターコントローライベントインターフェイス
  /// </summary>
  CharacterControllerEventInterface& character_controller_event_interface_;

  /// <summary>
  /// キャラクター
  /// </summary>
  Character* character_;
};