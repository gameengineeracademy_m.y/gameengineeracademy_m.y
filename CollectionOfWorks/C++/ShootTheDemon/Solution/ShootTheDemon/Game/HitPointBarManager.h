﻿#pragma once

#include "System/Task.h"
#include "Game/HitPointBar.h"
#include "Game/HitPointBarId.h"
#include <unordered_map>

/// <summary>
/// ヒットポイントバーマネージャ
/// </summary>
class HitPointBarManager : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kProcess,          // 処理中フェーズ
    kStop,             // 停止中フェーズ
    kFinalize,         // 終了処理フェーズ
    kFinalized,        // 終了処理済みフェーズ
    kPhaseMaxIndex     // フェーズ数
  };


public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  HitPointBarManager();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~HitPointBarManager();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// ヒットポイントバーのリスト登録
  /// </summary>
  /// <param name=""> ヒットポイントバーID </param>
  /// <returns> バレッジ </returns>
  HitPointBar* RegisterHitPointBar(HitPointBarId);

  /// <summary>
  /// ヒットポイントバーの生成
  /// </summary>
  /// <param name=""> </param>
  /// <returns></returns>
  HitPointBar* CreateHitPointBar(HitPointBarId);

  /// <summary>
  /// ヒットポイントバーの取得
  /// </summary>
  /// <param name="list_id"> リストID </param>
  /// <returns> ヒットポイントバー </returns>
  HitPointBar* GetHitPointBar(int list_id) { return generate_hit_point_bar_list_[list_id]; }

  /// <summary>
  /// 指定のヒットポイントバーの終了
  /// </summary>
  /// <param name=""> リストID </param>
  /// <returns></returns>
  void FinishHitPointBar(int);

  /// <summary>
  /// 全てのヒットポイントバーの終了処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void FinishAllHitPointBar();

  /// <summary>
  /// ヒットポイントバーの破棄
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DisposeHitPointBar();

  /// <summary>
  /// 現在のフェーズを変更
  /// </summary>
  /// <param name=""> 現在のフェーズ </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }


private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// ヒットポイントバーのリスト
  /// </summary>
  std::unordered_map<HitPointBarId, HitPointBar*> hit_point_bar_list_;

  /// <summary>
  /// 生成したヒットポイントバーのリスト
  /// </summary>
  std::unordered_map<int, HitPointBar*> generate_hit_point_bar_list_;
};