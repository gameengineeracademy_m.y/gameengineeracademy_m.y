﻿#pragma once

#include "DxLib.h"
#include "System/Level.h"
#include "System/MenuController.h"
#include "System/SoundManager.h"
#include "System/FontInfo.h"
#include "Game/ResultLevelEventInterface.h"
#include "Game/GameInfo.h"
#include "Game/ScoreBoard.h"
#include "Game/ResultBackGround.h"
#include "Game/ResultInfoUi.h"
#include <fstream>
#include <string>

/// <summary>
/// リザルトレベル
/// </summary>
/// <remarks>
/// ゲームの結果を扱う画面のタスク
/// </remarks>
class ResultLevel : public Level, public ResultLevelEventInterface {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kLoadFile,            // 設定ファイル読み込みフェーズ
    kPrepare,             // 準備フェーズ
    kStartWait,           // 起動待機フェーズ
    kStartUp,             // 起動フェーズ
    kProcess,             // 処理中フェーズ
    kBeforeTransition,    // レベル遷移前フェーズ
    kLevelTransition,     // レベル遷移フェーズ
    kFinalize,            // 終了処理フェーズ
    kFinalized,           // 終了処理済みフェーズ
    kPhaseMaxIndex        // フェーズ数
  };

  /// <summary>
  /// 音楽登録フェーズ
  /// </summary>
  enum class RegisterSoundPhase {
    kButtonSelect,        // ボタン選択音の登録フェーズ
    kButtonPush,          // ボタン押下音の登録フェーズ
    kBgm,                 // BGMの登録フェーズ
    kFinish,              // 処理終了フェーズ
    kPhaseMaxIndex        // フェーズ数
  };

  /// <summary>
  /// 終了フェーズの種類
  /// </summary>
  enum class FinalizePhaseType {
    kUnloadTask,         // タスクを降ろすフェーズ
    kDisposeTask,        // タスクを破棄するフェーズ
    kPhaseMaxIndex       // フェーズ項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> タスクID </param>
  /// <param name=""> タスクマネージャ </param>
  /// <returns></returns>
  ResultLevel(TaskId, TaskManager&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~ResultLevel();

  /// <summary>
  /// Enterキーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushMenuControllerEnterKey() override;

  /// <summary>
  /// Zキーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushMenuControllerZ() override;

  /// <summary>
  /// 右矢印キーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushMenuControllerRight() override;

  /// <summary>
  /// 左矢印キーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OnPushMenuControllerLeft() override;


protected:

  /// <summary>
  /// 初期化処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool InitializePhase(float) override;

  /// <summary>
  /// 毎フレームの処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool UpdatePhase(float) override;

  /// <summary>
  /// 毎フレームの描画処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void RenderPhase() override;

  /// <summary>
  /// 終了処理フェーズの処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  bool FinishPhase(float) override;


private:

  /// <summary>
  /// 背景 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeBackGround();

  /// <summary>
  /// スコアボード 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeScoreBoard();

  /// <summary>
  /// UI情報 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeResultInfoUi();

  /// <summary>
  /// メニューコントローラ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeMenuController();

  /// <summary>
  /// サウンドマネージャ 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool InitializeSoundManager();

  /// <summary>
  /// 設定ファイル読み込み
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool LoadSetupFile();

  /// <summary>
  /// ロードファイル サウンドデータ
  /// </summary>
  /// <param name=""> ファイルパス </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadFileSoundData(std::string);

  /// <summary>
  /// ロードファイル スコアデータ
  /// </summary>
  /// <param name=""> ファイルパス </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool LoadFileScoreData(std::string);

  /// <summary>
  /// 準備処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Prepare();

  /// <summary>
  /// 決定操作
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DecideOperation();

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; };

  /// <summary>
  /// 音楽登録フェーズを変更する
  /// </summary>
  /// <param name=""> 音楽登録フェーズの種類 </param>
  /// <returns></returns>
  void ChangeSoundPhase(RegisterSoundPhase phase_type) { sound_phase_ = phase_type; }

  /// <summary>
  /// 終了処理フェーズの変更
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeFinalizePhase(FinalizePhaseType finalize_phase) { finalize_phase_ = finalize_phase; };

  /// <summary>
  /// 透過率変更処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AdjustAlphaValue();


private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// サウンド登録のフェーズ
  /// </summary>
  RegisterSoundPhase sound_phase_;

  /// <summary>
  /// 終了フェーズの種類
  /// </summary>
  FinalizePhaseType finalize_phase_;

  /// <summary>
  /// 背景
  /// </summary>
  ResultBackGround* back_ground_;

  /// <summary>
  /// スコアボード
  /// </summary>
  ScoreBoard* score_board_;

  /// <summary>
  /// UI情報
  /// </summary>
  ResultInfoUi* info_ui_;

  /// <summary>
  /// メニューコントローラ
  /// </summary>
  MenuController* menu_controller_;

  /// <summary>
  /// サウンドマネージャ
  /// </summary>
  SoundManager* sound_manager_;

  /// <summary>
  /// 設定値
  /// </summary>
  std::unordered_map<Sound::SoundType, float> sound_volume_list_;

  /// <summary>
  /// 累積処理時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  int alpha_adjust_;

  /// <summary>
  /// 遷移するレベル
  /// </summary>
  TaskId transition_level_;
};