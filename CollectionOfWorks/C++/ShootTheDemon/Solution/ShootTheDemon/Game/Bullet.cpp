﻿#include "Game/Bullet.h"

namespace {

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 累積時間 リセット値
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 加速度初期値
  /// </summary>
  const float kAcceleInit = 0.0f;

  /// <summary>
  /// 拡大率調整値初期値
  /// </summary>
  const float kExpantionInit = 0.0f;

  /// <summary>
  /// 画面外判定 調整値
  /// </summary>
  const int kOutOfScreen = 100;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name="bullet_id"> バレットID </param>
/// <returns></returns>
Bullet::Bullet(BulletId bullet_id)
  : bullet_id_(bullet_id)
  , character_(nullptr)
  , bullet_event_interface_(nullptr)
  , graphic_handle_(0)
  , x_pos_(0)
  , y_pos_(0)
  , bullet_data_()
  , image_width_(0)
  , image_height_(0)
  , image_width_half_(0)
  , image_height_half_(0)
  , radius_(0.0f)
  , accumulate_time_(0.0f)
  , is_start_(false)
  , is_finish_(false) {

  // コンソールに出力
  std::cout << "Bullet コンストラクタ" << std::endl;
}

/// <summary>
/// コピーコンストラクタ
/// </summary>
/// <param name=""> コピー元クラス </param>
/// <returns></returns>
Bullet::Bullet(const Bullet& bullet) {

  // コンソールに出力
  std::cout << "Bullet コピーコンストラクタ" << std::endl;

  bullet_id_ = bullet.bullet_id_;
  character_ = bullet.character_;
  bullet_event_interface_ = bullet.bullet_event_interface_;
  graphic_handle_ = bullet.graphic_handle_;
  x_pos_ = bullet.x_pos_;
  y_pos_ = bullet.y_pos_;
  bullet_data_ = bullet.bullet_data_;
  image_width_ = bullet.image_width_;
  image_height_ = bullet.image_height_;
  image_width_half_ = bullet.image_width_half_;
  image_height_half_ = bullet.image_height_half_;
  radius_ = bullet.radius_;
  accumulate_time_ = bullet.accumulate_time_;
  is_start_ = bullet.is_start_;
  is_finish_ = bullet.is_finish_;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Bullet::~Bullet() {

  // コンソールに出力
  std::cout << "~Bullet デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true:画面外到達, false:描画中 </returns>
bool Bullet::Update(float process_time) {

  // 終了フラグが立っているなら処理終了
  if (IsFinish()) {
    return true;
  }

  // 透過率の調整
  AdjustAlphaValue();

  // 動作開始フラグが立っていない場合は累積時間を加算
  if (!IsStart()) {
    accumulate_time_ += process_time;
    if (accumulate_time_ < bullet_data_.time.wait_start) {
      // キャラクターとの当たり判定確認、当たった場合は描画終了フラグを立てる
      bool is_check = bullet_event_interface_->OnCheckCollisionBullet(this);
      if (is_check) {
        is_finish_ = true;
        return true;
      }
      return false;
    }
    // 累積時間をリセット
    accumulate_time_ = kResetTime;
    // 動作開始フラグを立てる
    SetMoveStart(true);
  }

  Character* character = GetCharacter();
  Character::CharacterType chara_type = character->GetCharacterType();
  switch (chara_type) {
  case Character::CharacterType::kPlayer: {
    x_pos_ -= bullet_data_.speed.x;
    y_pos_ -= bullet_data_.speed.y;
    break;
  }
  case Character::CharacterType::kEnemyA:
  case Character::CharacterType::kEnemyB: {
    x_pos_ += bullet_data_.speed.x;
    y_pos_ += bullet_data_.speed.y;
    break;
  }
  }

  //累積時間を加算
  accumulate_time_ += process_time;

  // 加速度設定されている場合のみ実行
  if (bullet_data_.accele.x != kAcceleInit ||
      bullet_data_.accele.y != kAcceleInit) {
    if (accumulate_time_ >= bullet_data_.time.wait_accel) {
      // 加速度を加算
      bullet_data_.speed.x += bullet_data_.accele.x;
      bullet_data_.speed.y += bullet_data_.accele.y;

      if (bullet_data_.speed.x < bullet_data_.speed.x_min) {
        bullet_data_.speed.x = bullet_data_.speed.x_min;
      }
      else if (bullet_data_.speed.x > bullet_data_.speed.x_max) {
        bullet_data_.speed.x = bullet_data_.speed.x_max;
      }

      if (bullet_data_.speed.y < bullet_data_.speed.y_min) {
        bullet_data_.speed.y = bullet_data_.speed.y_min;
      }
      else if (bullet_data_.speed.y > bullet_data_.speed.y_max) {
        bullet_data_.speed.y = bullet_data_.speed.y_max;
      }
    }
  }

  // 拡大率の調整値が設定されている場合
  if (bullet_data_.ext_rate.adjust != kExpantionInit) {
    if (accumulate_time_ >= bullet_data_.time.wait_expan) {
      // 拡大率に調整値を加算
      bullet_data_.ext_rate.value += bullet_data_.ext_rate.adjust;
      if (bullet_data_.ext_rate.value > bullet_data_.ext_rate.max) {
        bullet_data_.ext_rate.value = bullet_data_.ext_rate.max;
      }
      if (bullet_data_.ext_rate.value < bullet_data_.ext_rate.min) {
        bullet_data_.ext_rate.value = bullet_data_.ext_rate.min;
      }
    }
  }

  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return false;
  }
  // 描画座標を取得
  int screen_top = game_info->GetMapPositionY();
  int screen_bottom = game_info->GetMapHeight();
  int screen_left = game_info->GetMapPositionX();
  int screen_right = screen_left + game_info->GetMapWidth();

  // 画面外に出た弾は描画終了フラグを立てる
  if ((x_pos_ + kOutOfScreen < screen_left || x_pos_ - kOutOfScreen > screen_right) ||
      (y_pos_ + kOutOfScreen< screen_top || x_pos_ - kOutOfScreen > screen_bottom)) {
    is_finish_ = true;
    return true;
  }

  // キャラクターとの当たり判定確認、当たった場合は描画終了フラグを立てる
  bool is_check = bullet_event_interface_->OnCheckCollisionBullet(this);
  if (is_check) {
    is_finish_ = true;
    return true;
  }

  return false;
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Bullet::Render() {

  Character* character = GetCharacter();
  Character::CharacterType chara_type = character->GetCharacterType();
  switch (chara_type) {
  case Character::CharacterType::kPlayer:
  case Character::CharacterType::kEnemyA:
  case Character::CharacterType::kEnemyB: {

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, bullet_data_.alpha.value);

    DrawRotaGraphF(x_pos_, y_pos_, bullet_data_.ext_rate.value, bullet_data_.angle.value, graphic_handle_, true);

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, bullet_data_.alpha.min);
    break;
  }
  }
}

/// <summary>
/// 弾のクローン生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Bullet* Bullet::GenerateClone() {

  // コピーコンストラクタを使用して生成
  Bullet* bullet = nullptr;
  bullet = new Bullet(*this);
  if (bullet == nullptr) {
    return nullptr;
  }

  return bullet;
}

/// <summary>
/// 移動量のセット
/// </summary>
/// <param name="value_x"> X方向 </param>
/// <param name="value_y"> Y方向 </param>
/// <returns></returns>
void Bullet::SetSpeed(float value_x, float value_y) {

  bullet_data_.speed.x = value_x;
  bullet_data_.speed.y = value_y;
}

/// <summary>
/// 移動量の最大値のセット
/// </summary>
/// <param name="x_max"> X方向 </param>
/// <param name="y_max"> Y方向 </param>
/// <returns></returns>
void Bullet::SetSpeedMax(float x_max, float y_max) {

  bullet_data_.speed.x_max = x_max;
  bullet_data_.speed.y_max = y_max;
}

/// <summary>
/// 移動量の最小値のセット
/// </summary>
/// <param name="x_min"> X方向 </param>
/// <param name="y_min"> Y方向 </param>
/// <returns></returns>
void Bullet::SetSpeedMin(float x_min, float y_min) {

  bullet_data_.speed.x_min = x_min;
  bullet_data_.speed.y_min = y_min;
}

/// <summary>
/// 弾サイズのセット
/// </summary>
/// <param name="width"> 横幅 </param>
/// <param name="height"> 高さ </param>
/// <returns></returns>
void Bullet::SetSize(int width, int height) {

  bullet_data_.size.width = width;
  bullet_data_.size.height = height;
}

/// <summary>
/// 弾スペックのセット
/// </summary>
/// <param name="attack"> 攻撃力 </param>
/// <returns></returns>
void Bullet::SetSpec(int attack) {

  bullet_data_.spec.atttack = attack;
}

/// <summary>
/// 弾の加速度のセット
/// </summary>
/// <param name=""> X方向 </param>
/// <param name=""> Y方向 </param>
/// <returns></returns>
void Bullet::SetAcceleration(float acce_x, float acce_y) {

  bullet_data_.accele.x = acce_x;
  bullet_data_.accele.y = acce_y;
}

/// <summary>
/// 弾の待機時間のセット
/// </summary>
/// <param name="wait_start"> 動作待機時間 </param>
/// <param name="wait_accel"> 加速待機時間 </param>
/// <param name="wait_expan"> 拡大待機時間 </param>
/// <returns></returns>
void Bullet::SetWaitTime(float wait_start, float wait_accel, float wait_expan) {

  bullet_data_.time.wait_start = wait_start;
  bullet_data_.time.wait_accel = wait_accel;
  bullet_data_.time.wait_expan = wait_expan;
}

/// <summary>
/// 拡大率の関連値セット
/// </summary>
/// <param name="value"> 初期値 </param>
/// <param name="adjust"> 調整値 </param>
/// <param name="max"> 最大値 </param>
/// <param name="min"> 最小値 </param>
/// <returns></returns>
void Bullet::SetExtRate(float value, float adjust, float max, float min) {

  bullet_data_.ext_rate.value = value;
  bullet_data_.ext_rate.adjust = adjust;
  bullet_data_.ext_rate.max = max;
  bullet_data_.ext_rate.min = min;
}

/// <summary>
/// 角度の関連値セット
/// </summary>
/// <param name="value"> 初期値 </param>
/// <param name="adjust"> 調整値 </param>
/// <param name="max"> 最大値 </param>
/// <param name="min"> 最小値 </param>
/// <returns></returns>
void Bullet::SetAngle(float value, float adjust, float max, float min) {

  bullet_data_.angle.value = value;
  bullet_data_.angle.adjust = adjust;
  bullet_data_.angle.max = max;
  bullet_data_.angle.min = min;
}

/// <summary>
/// 透過度の関連値セット
/// </summary>
/// <param name="value"> 初期値 </param>
/// <param name="adjust"> 調整値 </param>
/// <param name="max"> 最大値 </param>
/// <param name="min"> 最小値 </param>
/// <returns></returns>
void Bullet::SetAlpha(int value, int adjust, int max, int min) {

  bullet_data_.alpha.value = value;
  bullet_data_.alpha.adjust = adjust;
  bullet_data_.alpha.max = max;
  bullet_data_.alpha.min = min;
}

/// <summary>
/// 画像のサイズをセットする
/// </summary>
/// <param> グラフィックハンドル </param>
/// <returns></returns>
void Bullet::SetImageSize(int graphic_handle) {

  // グラフィックハンドルから画像のサイズを取得
  GetGraphSize(graphic_handle, &image_width_, &image_height_);
  // 画像の半分のサイズを取得
  image_width_half_ = image_width_ / kHalfValue;
  image_height_half_ = image_height_ / kHalfValue;
}

/// <summary>
/// 透過率変更処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool Bullet::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率に調整値を加算
  bullet_data_.alpha.value += bullet_data_.alpha.adjust;

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (bullet_data_.alpha.value >= bullet_data_.alpha.max) {
    bullet_data_.alpha.value = bullet_data_.alpha.max;
    is_finish = true;
  }
  else if (bullet_data_.alpha.value <= bullet_data_.alpha.min) {
    bullet_data_.alpha.value = bullet_data_.alpha.min;
    is_finish = true;
  }

  return is_finish;
}

/// <summary>
/// 描画終了フラグを立てる
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Bullet::FinishDraw() {

  // 弾の終了を通知
  bullet_event_interface_->OnFinishBullet(this);
  is_finish_ = true;
}