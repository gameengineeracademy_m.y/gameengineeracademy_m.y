﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "System/FontInfo.h"
#include "Game/GameInfo.h"
#include <vector>

namespace {

  /// <summary>
  /// ボタンの種類
  /// </summary>
  const int kButtonTypeNum = 4;

  /// <summary>
  /// フォントサイズの種類
  /// </summary>
  const int kFontSizeNum = 2;
}

/// <summary>
/// タイトルUI情報
/// </summary>
class TitleInfoUi : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kNone,            // 何もしないフェーズ
    kInitialize,      // 初期化処理フェーズ
    kStartWait,       // 起動待機フェーズ
    kStartUp,         // 起動フェーズ
    kProcess,         // 処理中フェーズ
    kSoundSetting,    // サウンド設定フェーズ
    kScore,           // スコアフェーズ
    kBeforeFinalize,  // 終了処理前フェーズ
    kFinalize,        // 終了処理フェーズ
    kFinalized,       // 終了処理済みフェーズ
    kPhaseMaxIndex    // フェーズ数
  };

  /// <summary>
  /// ボタンの種類
  /// </summary>
  enum class ButtonType {
    kGameStart,       // ゲームスタート
    kOption,          // 設定
    kScore,           // スコア
    kGameEnd,         // ゲーム終了
    kTypeMaxIndex     // 種類数
  };

  /// <summary>
  /// フォントサイズの種類
  /// </summary>
  enum class FontSize {
    k25,              // 文字サイズ25
    k54,              // 文字サイズ54
    kTypeMaxIndex     // 種類数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  TitleInfoUi();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~TitleInfoUi();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// カーソル表示
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ShowCursor();

  /// <summary>
  /// カーソル操作 上移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OperationCursorUp();

  /// <summary>
  /// カーソル操作 下移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OperationCursorDown();

  /// <summary>
  /// ボタンの種類
  /// </summary>
  /// <param name=""></param>
  /// <returns> ボタンの種類 </returns>
  ButtonType GetButtonType() { return button_type_; }

  /// <summary>
  /// フェーズの種類を変更する
  /// </summary>
  /// <param names="phase_type"> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; };

  /// <summary>
  /// ボタンの種類を変更
  /// </summary>
  /// <param name=""> ボタンの種類 </param>
  /// <returns></returns>
  void ChangeButtonType(ButtonType button_type) { button_type_ = button_type; }

  /// <summary>
  /// 現在のフェーズが終了処理済みか確認
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理済み, false:終了処理済み以外 </returns>
  bool IsFinalized() { return current_phase_ == PhaseType::kFinalized; }


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param names=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Initialize();

  /// <summary>
  /// 処理中
  /// </summary>
  /// <param names=""> 処理時間 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  void Process(float);

  /// <summary>
  /// カーソル操作
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OperationCursor();

  /// <summary>
  /// 透過率の調整
  /// </summary>
  /// <param names=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool AdjustAlphaValue();


private:

  /// <summary>
  /// 表示座標
  /// </summary>
  struct Pos {
    int x;
    int y;
  };

  /// <summary>
  /// 表示座標 浮動小数
  /// </summary>
  struct Posf {
    float x;
    float y;
  };

  /// <summary>
  /// サイズ
  /// </summary>
  struct Size {
    int width;
    int height;
  };

  /// <summary>
  /// 図形
  /// </summary>
  struct Figure {
    Pos left;
    Pos right;
    Size size;
  };

  /// <summary>
  /// 図形 浮動小数
  /// </summary>
  struct Figuref {
    Posf left;
    Posf right;
  };

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// ボタンの種類
  /// </summary>
  ButtonType button_type_;

  /// <summary>
  /// ボタン ゲームスタート
  /// </summary>
  Figure button_start_;

  /// <summary>
  /// ボタン 設定
  /// </summary>
  Figure button_option_;

  /// <summary>
  /// ボタン スコア
  /// </summary>
  Figure button_score_;

  /// <summary>
  /// ボタン ゲーム終了
  /// </summary>
  Figure button_end_;

  /// <summary>
  /// ゲームタイトル
  /// </summary>
  std::string title_;

  /// <summary>
  /// テキスト ゲームスタート
  /// </summary>
  Pos text_button_start_;

  /// <summary>
  /// テキスト 設定
  /// </summary>
  Pos text_button_option_;

  /// <summary>
  /// テキスト 設定
  /// </summary>
  Pos text_button_score_;

  /// <summary>
  /// テキスト ゲーム終了
  /// </summary>
  Pos text_button_end_;

  /// <summary>
  /// テキスト ゲームタイトル
  /// </summary>
  Pos text_title_;

  /// <summary>
  /// メニュー画面 カーソル
  /// </summary>
  Figuref cursor_;

  /// <summary>
  /// メニュー画面 カーソル調整値
  /// </summary>
  Posf cursor_adjust_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 透過率 カーソル
  /// </summary>
  int alpha_cursor_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// ボタンの色
  /// </summary>
  int button_color_[kButtonTypeNum];

  /// <summary>
  /// フォントハンドル
  /// </summary>
  int font_handle_[kFontSizeNum];
};