﻿#include "Game/ItemPowerUp.h"

namespace {

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjustInit = 10;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name="character_controller_event_interface"> キャラクターコントローライベントインターフェース </param>
/// <param name="item_event_interface"> アイテムイベントインターフェース </param>
/// <param name="character_event_interface"> キャラクターコントローライベントインターフェース </param>
/// <returns></returns>
ItemPowerUp::ItemPowerUp(CharacterControllerEventInterface& character_controller_event_interface,
                         ItemEventInterface& item_event_interface,
                         CharacterEventInterface& character_event_interface)
  : ItemBase(TaskId::kPowerUpItem, Character::CharacterType::kItemB,
    character_controller_event_interface, item_event_interface,
    character_event_interface)
  , current_phase_(PhaseType::kInitialize) {

  // コンソールに出力
  std::cout << "ItemPowerUp コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ItemPowerUp::~ItemPowerUp() {

  // コンソールに出力
  std::cout << "~ItemPowerUp デストラクタ" << std::endl;
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param names="process_time"> 処理時間 </param>
/// <returns></returns>
void ItemPowerUp::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //----------------------------------
    //初期化フェーズ
    //----------------------------------
    Initialize();
    // 現在のフェーズを「起動」に変更
    ChangeCurrentPhase(PhaseType::kStartUp);
    break;
  }
  case PhaseType::kStartUp: {
    //----------------------------------
    //起動フェーズ
    //----------------------------------
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 透過率の符号を変更しておく
      ChangeAlphaAdjustSign();
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //----------------------------------
    //処理中フェーズ
    //----------------------------------
    bool is_finish = Process();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kStop: {
    //----------------------------------
    //停止フェーズ
    //----------------------------------
    break;
  }
  case PhaseType::kBeforeFinalize: {
    //----------------------------------
    //終了処理前フェーズ
    //----------------------------------
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //----------------------------------
    //終了処理フェーズ
    //----------------------------------
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
};

/// <summary>
/// 毎フレームの描画処理
/// </summary>
/// <param names=""></param>
/// <returns></returns>
void ItemPowerUp::Render() {

  // 描画終了状態なら処理終了
  if (IsFinish()) {
    return;
  }

  ItemBase::Render();
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param names=""></param>
/// <returns></returns>
void ItemPowerUp::Initialize() {

  // 透過率 初期設定
  alpha_ = kAlphaMin;
  alpha_adjust_ = kAlphaAdjustInit;
  alpha_max_ = kAlphaMax;
  alpha_min_ = kAlphaMin;

  // 座標をセットする
  SetPositionX(GetInitialPositionX());
  SetPositionY(GetInitialPositionY());
  // 画像のサイズをセットする
  SetImageSize(graphic_handle_);
  // 描画フラグをリセットする
  ResetFinish();
  // 生きていることにする
  SetAlive();
}

/// <summary>
/// 処理中
/// </summary>
/// <param names=""></param>
/// <returns></returns>
bool ItemPowerUp::Process() {

  // 描画終了状態なら処理終了
  if (IsFinish()) {
    return true;
  }

  // 処理中フェーズ以外は処理終了
  if (current_phase_ != PhaseType::kProcess) {
    return false;
  }

  // 下へ移動させる
  item_controller_.MoveDown();

  return false;
}