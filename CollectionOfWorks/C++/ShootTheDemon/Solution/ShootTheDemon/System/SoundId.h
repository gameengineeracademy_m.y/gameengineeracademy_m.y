﻿#pragma once

/// <summary>
/// サウンドID一覧
/// </summary>
enum class SoundId {
  kButtonSelect,
  kButtonPush,
  kGameStart,
  kShoot,
  kPlayerDeath,
  kPlayerExplosion,
  kPlayerReborn,
  kEnemyExplosion,
  kCutIn,
  kGetItem,
  kGameOver,
  kGameClear,
  kTitleBgm,
  kTitleBgmTest,
  kBattleBgm,
  kBattleBgmTest,
  kResultBgm,
  kSoundMaxIndex
};