﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "System/FontInfo.h"
#include "System/SoundSetupEventInterface.h"
#include "Game/GameInfo.h"
#include <string>
#include <vector>

/// <summary>
/// サウンド設定
/// </summary>
class SoundSetup : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,       // 初期化処理フェーズ
    kProcess,          // 処理中フェーズ
    kStop,             // 停止中フェーズ
    kFinalize,         // 終了処理フェーズ
    kFinalized,        // 終了処理済みフェーズ
    kPhaseMaxIndex     // フェーズ数
  };

  /// <summary>
  /// ボタンの種類
  /// </summary>
  enum class ButtonType {
    kBackGroundMusic,  // BGM設定
    kSoundEffect,      // SE設定
    kDone,             // 決定
    kReturn,           // 戻る
    kTypeMaxIndex      // 種類数
  };

  /// <summary>
  /// フォントサイズの種類
  /// </summary>
  enum class FontSize {
    k22,               // サイズ22
    k40,               // サイズ40
    kSizeMaxIndex      // 種類数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> サウンドセットアップイベントインターフェース </param>
  /// <returns></returns>
  SoundSetup(SoundSetupEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~SoundSetup();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// 背景の位置とサイズ
  /// </summary>
  /// <param name=""> 幅 </param>
  /// <param name=""> 高さ </param>
  /// <param name=""> X座標(左上) </param>
  /// <param name=""> Y座標(左上) </param>
  /// <returns></returns>
  void SetBackPosAndSize(int, int, int, int);

  /// <summary>
  /// 選択中のボタンを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> ボタンの種類 </returns>
  ButtonType GetButtonType() { return button_type_; }

  /// <summary>
  /// サウンドボリュームのセット
  /// </summary>
  /// <param name=""> ボリューム </param>
  /// <returns></returns>
  void SetSoundVolumeBgm(float volume) { volume_bgm_ = volume; }

  /// <summary>
  /// サウンドボリュームのセット
  /// </summary>
  /// <param name=""> ボリューム </param>
  /// <returns></returns>
  void SetSoundVolumeSe(float volume) { volume_se_ = volume; }

  /// <summary>
  /// サウンドボリュームを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> BGMのボリューム </returns>
  float GetSoundVolumeBgm() { return volume_bgm_; }

  /// <summary>
  /// サウンドボリュームを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> SEのボリューム </returns>
  float GetSoundVolumeSe() { return volume_se_; }

  /// <summary>
  /// カーソル操作 上移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OperationCursorUp();

  /// <summary>
  /// カーソル操作 下移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OperationCursorDown();

  /// <summary>
  /// カーソル操作 上移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OperationCursorRight();

  /// <summary>
  /// カーソル操作 下移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OperationCursorLeft();

  /// <summary>
  /// BGMリストIDをセット
  /// </summary>
  /// <param name="list_id"> リストID </param>
  /// <returns></returns>
  void SetBgmListId(int list_id) { list_id_bgm_ = list_id; }

  /// <summary>
  /// BGMリストIDを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> リストID </returns>
  int GetBgmListId() { return list_id_bgm_; }

  /// <summary>
  /// 現在のフェーズを変更
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }

  /// <summary>
  /// ボタンの種類を変更
  /// </summary>
  /// <param name=""> ボタンの種類 </param>
  /// <returns></returns>
  void ChangeButtonType(ButtonType button_type) { button_type_ = button_type; }


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理, false:処理中 </returns>
  bool Initialize();

  /// <summary>
  /// 処理中
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Process(float);

  /// <summary>
  /// カーソル操作
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OperationCursor();

  /// <summary>
  /// ボリューム値 表示位置調整処理
  /// </summary>
  /// <param name=""> ボリューム </param>
  /// <param name=""> X座標 </param>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  void CalcDispVolumeText(int, int&, int&);


private:

  /// <summary>
  /// 表示座標
  /// </summary>
  struct Pos {
    int x;
    int y;
  };

  /// <summary>
  /// 表示座標 浮動小数
  /// </summary>
  struct Posf {
    float x;
    float y;
  };

  /// <summary>
  /// サイズ
  /// </summary>
  struct Size {
    int width;
    int height;
  };

  /// <summary>
  /// 図形 四角
  /// </summary>
  struct Figure {
    Pos left;
    Pos right;
  };

  /// <summary>
  /// 図形 丸
  /// </summary>
  struct Circle {
    Posf center;
    float radius;
  };

  /// <summary>
  /// 図形 浮動小数
  /// </summary>
  struct Figuref {
    Posf left;
    Posf right;
  };

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// ボタンの種類
  /// </summary>
  ButtonType button_type_;

  /// <summary>
  /// サウンドセットアップイベントインターフェース
  /// </summary>
  SoundSetupEventInterface& sound_setup_event_interface_;

  /// <summary>
  /// メニュー画面 本体
  /// </summary>
  Figure screen_;

  /// <summary>
  /// メニュー画面 ボタン
  /// </summary>
  Circle button_bgm_;

  /// <summary>
  /// メニュー画面 ボタン
  /// </summary>
  Circle button_se_;

  /// <summary>
  /// メニュー画面 スクリーンテキスト
  /// </summary>
  Pos text_screen_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_button_bgm_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_button_se_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_volume_bgm_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_volume_se_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_button_done_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_button_return_;

  /// <summary>
  /// メニュー画面 BGMボリューム線
  /// </summary>
  Figuref line_bgm_;

  /// <summary>
  /// メニュー画面 SEボリューム線
  /// </summary>
  Figuref line_se_;

  /// <summary>
  /// メニュー画面 BGMボリューム線 始点
  /// </summary>
  Figuref line_bgm_start_;

  /// <summary>
  /// メニュー画面 SEボリューム線 始点
  /// </summary>
  Figuref line_se_start_;

  /// <summary>
  /// メニュー画面 BGMボリューム線 終点
  /// </summary>
  Figuref line_bgm_end_;

  /// <summary>
  /// メニュー画面 SEボリューム線 終点
  /// </summary>
  Figuref line_se_end_;

  /// <summary>
  /// メニュー画面 BGMボリューム線 中間点
  /// </summary>
  Figuref line_bgm_mid_;

  /// <summary>
  /// メニュー画面 SEボリューム線 中間点
  /// </summary>
  Figuref line_se_mid_;

  /// <summary>
  /// メニュー画面 決定
  /// </summary>
  Figure button_done_;

  /// <summary>
  /// メニュー画面 戻る
  /// </summary>
  Figure button_return_;

  /// <summary>
  /// メニュー画面 カーソル
  /// </summary>
  Figuref cursor_;

  /// <summary>
  /// 背景
  /// </summary>
  Figure back_board_;

  /// <summary>
  /// メニュー画面 カーソル調整値
  /// </summary>
  Posf cursor_adjust_;

  /// <summary>
  /// メニュー画面 カーソル透過率
  /// </summary>
  int alpha_cursor_;

  /// <summary>
  /// メニュー画面 カーソル透過率 調整値
  /// </summary>
  int alpha_cursor_adjust_;

  /// <summary>
  /// メニュー画面 BGMボリューム(%)
  /// </summary>
  int volume_bgm_rate_;

  /// <summary>
  /// メニュー画面 SEボリューム(%)
  /// </summary>
  int volume_se_rate_;

  /// <summary>
  /// BGMボリューム
  /// </summary>
  float volume_bgm_;

  /// <summary>
  /// SEボリューム
  /// </summary>
  float volume_se_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// BGM再生用累積時間
  /// </summary>
  float accumulate_time_bgm_;

  /// <summary>
  /// BGMリストID
  /// </summary>
  int list_id_bgm_;

  /// <summary>
  /// ボタンの色
  /// </summary>
  std::unordered_map<ButtonType, int> button_color_;

  /// <summary>
  /// フォントハンドル
  /// </summary>
  std::vector<int> font_handle_;
};