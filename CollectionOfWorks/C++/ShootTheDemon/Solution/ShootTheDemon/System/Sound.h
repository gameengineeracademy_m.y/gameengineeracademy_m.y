﻿#pragma once

#include "DxLib.h"
#include "System/SoundId.h"
#include <iostream>

/// <summary>
/// サウンド
/// </summary>
class Sound {
public:

  /// <summary>
  /// サウンドのフェーズ
  /// </summary>
  enum class PhaseType {
    kPlayStart,          // プレイ開始
    kPlay,               // プレイ中
    kPause,              // 一時中断中
    kFinishBefore,       // 終了前処理
    kFinish,             // 終了
    kFinished,           // 終了済み
    kPhaseMaxIndex       // 種類の数
  };

  /// <summary>
  /// サウンドの種類
  /// </summary>
  enum class SoundType {
    kNone,               // 何もしない
    kSoundEffect,        // SE
    kBackGroundMusic,    // BGM
    kTypeMaxIndex        // 種類の数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> サウンドID </param>
  /// <returns></returns>
  Sound(SoundId);

  /// <summary>
  /// コピーコンストラクタ
  /// </summary>
  /// <param name=""> コピー元クラス </param>
  /// <returns></returns>
  Sound(const Sound&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~Sound();

  /// <summary>
  /// SEサウンド処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:再生終了, false:再生処理中 </returns>
  bool PlaySoundSe(float);

  /// <summary>
  /// BGMサウンド処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:再生終了, false:再生処理中 </returns>
  bool PlaySoundBgm(float);

  /// <summary>
  /// サウンドを一時停止する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Pause();

  /// <summary>
  /// サウンドを一時停止から再開する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Resume();

  /// <summary>
  /// サウンドボリューム調整値の符号変換
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeSoundVolumeAdjustSign();

  /// <summary>
  /// サウンドの総再生時間を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void GetTotalTime();

  /// <summary>
  /// サウンドクローン生成
  /// </summary>
  /// <param name=""></param>
  /// <returns> サウンド </returns>
  Sound* GenerateClone();

  /// <summary>
  /// サウンドの種類をセット
  /// </summary>
  /// <param name="sound_handle"> サウンドの種類 </param>
  /// <returns></returns>
  void SetSoundType(SoundType sound_type) { sound_type_ = sound_type; }

  /// <summary>
  /// サウンドの種類を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> サウンドの種類 </returns>
  SoundType GetSoundType() { return sound_type_; }

  /// <summary>
  /// リストIDをセット
  /// </summary>
  /// <param name="list_id"> リストID </param>
  /// <returns></returns>
  void SetSoundListId(int list_id) { list_id_ = list_id; }

  /// <summary>
  /// リストIDを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> リストID </returns>
  int GetSoundListId() { return list_id_; }

  /// <summary>
  /// サウンドハンドルをセット
  /// </summary>
  /// <param name="sound_handle"> サウンドハンドル </param>
  /// <returns></returns>
  void SetSoundHandle(int sound_handle) { sound_handle_ = sound_handle; }

  /// <summary>
  /// サウンドハンドルを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> サウンドハンドル </returns>
  int GetSoundHandle() { return sound_handle_; }

  /// <summary>
  /// サウンドボリューム値をセット
  /// </summary>
  /// <param name="sound_volume"> サウンドボリューム </param>
  /// <returns></returns>
  void SetSoundVolume(int sound_volume) { sound_volume_ = sound_volume; }

  /// <summary>
  /// サウンドボリューム調整値をセット
  /// </summary>
  /// <param name="volume_adjust"> 調整値 </param>
  /// <returns></returns>
  void SetSoundVolumeAdjust(int volume_adjust) { sound_volume_adjust_ = volume_adjust; }

  /// <summary>
  /// サウンドのボリュームを変更する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeSoundVolume();

  /// <summary>
  /// サウンドのボリュームを徐々に変更する
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool ChangeFadeSoundVolume();

  /// <summary>
  /// 一時停止フラグをセット
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetPause() { is_pause_ = true; }

  /// <summary>
  /// 一時停止フラグをリセットする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ReSetPause() { is_pause_ = false; }

  /// <summary>
  /// 一時停止フラグが立っているか確認
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:一時停止中, false:それ以外 </returns>
  bool IsPause() { return is_pause_ == true; }

  /// <summary>
  /// 再生終了フラグを立てる
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetPlayFinish() { is_finish_ = true; }

  /// <summary>
  /// 再生終了フラグが立っているか確認
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:再生終了, false:処理中 </returns>
  bool IsFinish() { return is_finish_ == true; }

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }


private:

  /// <summary>
  /// サウンドID
  /// </summary>
  SoundId sound_id_;

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// サウンドの種類
  /// </summary>
  SoundType sound_type_;

  /// <summary>
  /// サウンドリストID
  /// </summary>
  int list_id_;

  /// <summary>
  /// サウンドハンドル
  /// </summary>
  int sound_handle_;

  /// <summary>
  /// サウンドボリューム
  /// </summary>
  int sound_volume_;

  /// <summary>
  /// サウンドボリューム 調整値
  /// </summary>
  int sound_volume_adjust_;

  /// <summary>
  /// サウンドの総再生時間
  /// </summary>
  float sound_total_time_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// 再生一時停止フラグ
  /// </summary>
  bool is_pause_;

  /// <summary>
  /// 再生終了フラグ
  /// </summary>
  bool is_finish_;
};