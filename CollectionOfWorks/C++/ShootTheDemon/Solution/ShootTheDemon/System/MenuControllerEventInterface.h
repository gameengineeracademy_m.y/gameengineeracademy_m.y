﻿#pragma once

#include <iostream>

/// <summary>
/// メニューコントローライベントインターフェース
/// </summary>
class MenuControllerEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  MenuControllerEventInterface() {

    // コンソールに出力
    std::cout << "MenuControllerEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~MenuControllerEventInterface() {

    // コンソールに出力
    std::cout << "~MenuControllerEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// Enterキーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushMenuControllerEnterKey() {};

  /// <summary>
  /// Escapeキーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushMenuControllerEscapeKey() {};

  /// <summary>
  /// Zキーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushMenuControllerZ() {}

  /// <summary>
  /// Xキーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushMenuControllerX() {}

  /// <summary>
  /// Qキーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushMenuControllerQ() {}

  /// <summary>
  /// Aキーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushMenuControllerA() {}

  /// <summary>
  /// 上矢印キーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushMenuControllerUp() {}

  /// <summary>
  /// 下矢印キーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushMenuControllerDown() {}

  /// <summary>
  /// 右矢印キーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushMenuControllerRight() {}

  /// <summary>
  /// 左矢印キーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushMenuControllerLeft() {}


private:

};