﻿#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif

#pragma once

namespace {
 
  /// <summary>
  /// プレイヤー死亡 総分割数
  /// </summary>
  const int kPlayerDeathDivCount = 1;

  /// <summary>
  /// プレイヤー爆発 総分割数
  /// </summary>
  const int kPlayerExplosionDivCount = 20;

  /// <summary>
  /// プレイヤー復活 総分割数
  /// </summary>
  const int kPlayerRebornDivCount = 55;

  /// <summary>
  /// 通常弾消滅 総分割数
  /// </summary>
  const int kDisappearNormalDivCount = 12;

  /// <summary>
  /// 弾消滅 総分割数
  /// </summary>
  const int kDisappearBallDivCount = 10;

  /// <summary>
  /// 敵の爆発 総分割数
  /// </summary>
  const int kEnemyExplosionDivCount = 10;

  /// <summary>
  /// 敵の破裂 総分割数
  /// </summary>
  const int kEnemyRuptureDivCount = 1;

  /// <summary>
  /// アイテム生成 総分割数
  /// </summary>
  const int kItemSummonDivCount = 10;
}

#include "DxLib.h"
#include "System/EffectId.h"
#include <iostream>
#include <vector>
#include <math.h>

/// <summary>
/// エフェクト
/// </summary>
class Effect {
public:

  /// <summary>
  /// エフェクトの種類
  /// </summary>
  enum class EffectType {
    kTemporary,     // 一時的なエフェクト
    kLasting,       // 永続的なエフェクト
    kTypeMaxIndex   // 種類の数
  };

  /// <summary>
  /// 座標
  /// </summary>
  struct Pos {
    int x;        // X座標 表示座標
    int y;        // Y座標 表示座標
  };

  /// <summary>
  /// 画像分割情報
  /// </summary>
  struct Div {
    int total;   // 総数
    int x;       // 分割数 横方向
    int y;       // 分割数 縦方向
    int width;   // サイズ 幅
    int height;  // サイズ 高さ
  };

  /// <summary>
  /// 詳細
  /// </summary>
  struct Detail {
    EffectType type;   // エフェクトの種類
    float wait_time;   // 待機時間
  };

  /// <summary>
  /// 透過率
  /// </summary>
  struct Alpha {
    int value;      // 透過率
    int adjust;     // 透過率調整値
    int max;        // 透過率最大値
    int min;        // 透過率最小値
  };

  /// <summary>
  /// 拡大率 X方向
  /// </summary>
  struct ExtRateX {
    float value;    // 拡大率 X方向
    float adjust;   // 拡大率調整値 X方向
    float max;      // 拡大率最大値 X方向
    float min;      // 拡大率最小値 X方向
  };

  /// <summary>
  /// 拡大率 Y方向
  /// </summary>
  struct ExtRateY {
    float value;    // 拡大率 Y方向
    float adjust;   // 拡大率調整値 Y方向
    float max;      // 拡大率最大値 Y方向
    float min;      // 拡大率最小値 Y方向
  };

  /// <summary>
  /// 角度
  /// </summary>
  struct Angle {
    float value;    // 角度
    float adjust;   // 角度調整値
    float max;      // 角度最大値
    float min;      // 角度最小値
  };

  /// <summary>
  /// エフェクトデータ
  /// </summary>
  struct EffectData {
    Pos pos;
    Detail detail;
    Div div;
    Alpha alpha;
    ExtRateX ex_rate_x;
    ExtRateY ex_rate_y;
    Angle angle;
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> エフェクトID </param>
  /// <returns></returns>
  Effect(EffectId);

  /// <summary>
  /// コピーコンストラクタ
  /// </summary>
  /// <param name=""> コピー元クラス </param>
  /// <returns></returns>
  Effect(const Effect& eefect);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~Effect();

  /// <summary>
  /// クローン生成
  /// </summary>
  /// <param name=""></param>
  /// <returns> エフェクト </returns>
  Effect* GenerateClone();

  /// <summary>
  /// エフェクトの種類をセット
  /// </summary>
  /// <param name=""> エフェクトの種類 </param>
  /// <returns></returns>
  void SetEffectType(EffectType effect_type) { effect_data_.detail.type = effect_type; };

  /// <summary>
  /// エフェクト終了
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetEffectFinish() { is_finish_ = true; };

  /// <summary>
  /// エフェクト終了設定をリセットする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ResetEffectFinish() { is_finish_ = false; };

  /// <summary>
  /// エフェクト終了しているか取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了, false:継続中 </returns>
  bool IsFinish() { return is_finish_; };

  /// <summary>
  /// エフェクトハンドルのセット
  /// </summary>
  /// <param name="handle"> エフェクトハンドル </param>
  /// <returns></returns>
  void SetEffectHandle(int handle) { effect_handle_.push_back(handle); };

  /// <summary>
  /// エフェクトハンドル格納配列のサイズを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> 配列のサイズ </returns>
  int GetEffectHandleArraySize() { return static_cast<int>(effect_handle_.size()); };

  /// <summary>
  /// エフェクトハンドルの取得
  /// </summary>
  /// <param name="index"> インデックス </param>
  /// <returns> エフェクトハンドル </returns>
  int GetEffectHandle(int index) { return effect_handle_.at(index); };

  /// <summary>
  /// エフェクトハンドルのサイズを取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> エフェクトハンドルのサイズ </returns>
  int GetEffectHandleSize() { return static_cast<int>(effect_handle_.size()); };

  /// <summary>
  /// エフェクトの種類を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> エフェクトの種類 </returns>
  EffectType GetEffectType() { return effect_data_.detail.type; };

  /// <summary>
  /// エフェクトIDを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> エフェクトID </returns>
  EffectId GetEffectId() { return effect_id_; }

  /// <summary>
  /// エフェクトリストから降ろしたことにする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetReleaseEffect() { is_release_ = true; }

  /// <summary>
  /// エフェクトを降ろす設定をリセットする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetResetReleaseEffect() { is_release_ = false; }

  /// <summary>
  /// エフェクトを降ろしたかの有無を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> エフェクトを降ろしたかの有無 </returns>
  bool IsReleaseEffect() { return is_release_ == true; }

  /// <summary>
  /// X座標をセットする
  /// </summary>
  /// <param name="x_pos"> X座標 </param>
  /// <returns></returns>
  void SetPosX(int x_pos) { effect_data_.pos.x = x_pos; }

  /// <summary>
  /// X座標を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  int GetPosX() { return effect_data_.pos.x; }

  /// <summary>
  /// Y座標をセットする
  /// </summary>
  /// <param name="y_pos"> Y座標 </param>
  /// <returns></returns>
  void SetPosY(int y_pos) { effect_data_.pos.y = y_pos; }

  /// <summary>
  /// Y座標を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  int GetPosY() { return effect_data_.pos.y; }

  /// <summary>
  /// アルファ値強制セット
  /// </summary>
  /// <param name=""> 透過率 </param>
  /// <returns></returns>
  void CheckAlphaValue(int);

  /// <summary>
  /// グラフィックインデックス切り替え待機時間
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetWaitTime(float wait_time) { effect_data_.detail.wait_time = wait_time; }

  /// <summary>
  /// 画像取得情報のセット
  /// </summary>
  /// <param name=""> 総分割数 </param>
  /// <param name=""> 分割 横方向 </param>
  /// <param name=""> 分割 縦方向 </param>
  /// <param name=""> サイズ 幅 </param>
  /// <param name=""> サイズ 高さ </param>
  /// <returns></returns>
  void SetLoadDivData(int, int, int, int, int);

  /// <summary>
  /// アルファ値の関連値セット
  /// </summary>
  /// <param name=""> 初期値 </param>
  /// <param name=""> 調整値 </param>
  /// <param name=""> 最大値 </param>
  /// <param name=""> 最小値 </param>
  /// <returns></returns>
  void SetAlpha(int, int, int, int);

  /// <summary>
  /// 拡大率の関連値セット
  /// </summary>
  /// <param name=""> 初期値 </param>
  /// <param name=""> 調整値 </param>
  /// <param name=""> 最大値 </param>
  /// <param name=""> 最小値 </param>
  /// <returns></returns>
  void SetExtRateX(float, float, float, float);

  /// <summary>
  /// 拡大率の関連値セット
  /// </summary>
  /// <param name=""> 初期値 </param>
  /// <param name=""> 調整値 </param>
  /// <param name=""> 最大値 </param>
  /// <param name=""> 最小値 </param>
  /// <returns></returns>
  void SetExtRateY(float, float, float, float);

  /// <summary>
  /// 角度の関連値セット
  /// </summary>
  /// <param name=""> 初期値 </param>
  /// <param name=""> 調整値 </param>
  /// <param name=""> 最大値 </param>
  /// <param name=""> 最小値 </param>
  /// <returns></returns>
  void SetAngle(float, float, float, float);

  /// <summary>
  /// 更新処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:終了処理, false:処理中 </returns>
  bool Update(float);

  /// <summary>
  /// 描画処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render();


private:

  /// <summary>
  /// プレイヤー死亡エフェクトの更新処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理, false:処理中 </returns>
  bool UpdatePlayerDeath();

  /// <summary>
  /// プレイヤー爆発エフェクトの更新処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理, false:処理中 </returns>
  bool UpdatePlayerExplosion();

  /// <summary>
  /// プレイヤー復活エフェクトの更新処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理, false:処理中 </returns>
  bool UpdatePlayerReborn();

  /// <summary>
  /// 通常弾の消滅の更新処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理, false:処理中 </returns>
  bool UpdateDisappearNormal();

  /// <summary>
  /// 青弾の消滅の更新処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理, false:処理中 </returns>
  bool UpdateDisappearBlueBall();

  /// <summary>
  /// 紫弾の消滅の更新処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理, false:処理中 </returns>
  bool UpdateDisappearPurpleBall();

  /// <summary>
  /// 赤弾の消滅の更新処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理, false:処理中 </returns>
  bool UpdateDisappearRedBall();

  /// <summary>
  /// 黄弾の消滅の更新処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理, false:処理中 </returns>
  bool UpdateDisappearYellowBall();

  /// <summary>
  /// 緑弾の消滅の更新処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理, false:処理中 </returns>
  bool UpdateDisappearGreenBall();

  /// <summary>
  /// 敵の爆発の更新処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理, false:処理中 </returns>
  bool UpdateEnemyExplosion();

  /// <summary>
  /// 敵の破裂の更新処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理, false:処理中 </returns>
  bool UpdateEnemyRupture();

  /// <summary>
  /// アイテム生成の更新処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理, false:処理中 </returns>
  bool UpdateItemSummon();

  /// <summary>
  /// エフェクトの実行 拡大・縮小
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void RenderExRate();


private:

  /// <summary>
  /// エフェクトID
  /// </summary>
  EffectId effect_id_;

  /// <summary>
  /// エフェクトのハンドル
  /// </summary>
  std::vector<int> effect_handle_;

  /// <summary>
  /// エフェクトハンドルのインデックス
  /// </summary>
  int effect_index_;

  /// <summary>
  /// エフェクト実行終了フラグ
  /// </summary>
  bool is_finish_;

  /// <summary>
  /// エフェクトリストから降ろしたかの有無
  /// </summary>
  bool is_release_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// エフェクトデータ
  /// </summary>
  EffectData effect_data_;
};