﻿#pragma once

/// <summary>
/// エフェクトID
/// </summary>
enum class EffectId {
  kPlayerDeath,          // プレイヤー死亡演出
  kExplosion,            // 爆発
  kReborn,               // 復活
  kDisappearNormal,      // 通常弾消滅
  kDisappearBlueBall,    // 青弾消滅
  kDisappearPurpleBall,  // 紫弾消滅
  kDisappearRedBall,     // 赤弾消滅
  kDisappearYellowBall,  // 黄弾消滅
  kDisappearGreenBall,   // 緑弾消滅
  kEnemyExplosion,       // 敵の爆発
  kEnemyRupture,         // 敵の破裂表示
  kItemSummon,           // アイテムの生成
  kEffectMaxIndex        // エフェクトの数
};