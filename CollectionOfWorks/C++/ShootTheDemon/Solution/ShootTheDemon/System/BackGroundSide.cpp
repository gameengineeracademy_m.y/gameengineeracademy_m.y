﻿#include "System/BackGroundSide.h"

namespace {

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjustInit = 5;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 符号反転
  /// </summary>
  const int kMinusSign = -1;

  /// <summary>
  /// 原点 X座標
  /// </summary>
  const int kOriginX = 0;

  /// <summary>
  /// 原点 Y座標
  /// </summary>
  const int kOriginY = 0;

  /// <summary>
  /// 使用画像 その1
  /// </summary>
  const int kImage01Index = 0;

  /// <summary>
  /// 使用画像 その2
  /// </summary>
  const int kImage02Index = 1;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSizeTitle = 25;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSizeText = 18;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 1;

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Reggae One";

  /// <summary>
  /// 表示文字列 テキストタイトル
  /// </summary>
  const char* kDisplayStringTitle = "<< Manual >>";

  /// <summary>
  /// 表示文字列 テキスト
  /// </summary>
  const char* kDisplayStringText1 = "Cross      : Move";

  /// <summary>
  /// 表示文字列 テキスト
  /// </summary>
  const char* kDisplayStringText2 = "LeftShift : SlowMove";

  /// <summary>
  /// 表示文字列 テキスト
  /// </summary>
  const char* kDisplayStringText3 = "Z     : Shoot";

  /// <summary>
  /// 表示文字列 テキスト
  /// </summary>
  const char* kDisplayStringText4 = "Esc : Pause";

  /// <summary>
  /// 表示文字列 テキスト
  /// </summary>
  const char* kDisplayStringText5 = "X     : Return";

  /// <summary>
  /// 表示文字列 テキストタイトル
  /// </summary>
  const char* kDisplayDebugStringTitle = "<< Debug >>";

  /// <summary>
  /// 表示文字列 テキスト
  /// </summary>
  const char* kDisplayDebugStringText1 = "P : Phase End";

  /// <summary>
  /// 表示文字列 テキスト
  /// </summary>
  const char* kDisplayDebugStringText2 = "O : Retry";

  /// <summary>
  /// 表示文字列 テキスト
  /// </summary>
  const char* kDisplayDebugStringText3 = "I : Enemy End";

  /// <summary>
  /// 表示文字列 テキスト
  /// </summary>
  const char* kDisplayDebugStringText4 = "L : Shoot Lv.Up";

  /// <summary>
  /// 表示文字列 テキスト
  /// </summary>
  const char* kDisplayDebugStringText5 = "Q : Phase Select";

  /// <summary>
  /// 文字表示位置 X座標
  /// </summary>
  const int kTitleTextPosX = 15;

  /// <summary>
  /// 文字表示位置 Y座標
  /// </summary>
  const int kTitleTextPosY = 15;

  /// <summary>
  /// 文字表示位置 Y座標
  /// </summary>
  const int kText1PosY = 55;

  /// <summary>
  /// 文字表示位置 Y座標
  /// </summary>
  const int kText2PosY = 85;

  /// <summary>
  /// 文字表示位置 Y座標
  /// </summary>
  const int kText3PosY = 115;

  /// <summary>
  /// 文字表示位置 Y座標
  /// </summary>
  const int kText4PosY = 145;

  /// <summary>
  /// 文字表示位置 Y座標
  /// </summary>
  const int kText5PosY = 175;

  /// <summary>
  /// 文字表示位置 Y座標
  /// </summary>
  const int kTitleDebugTextPosY = 210;

  /// <summary>
  /// 文字表示位置 Y座標
  /// </summary>
  const int kDebugText1PosY = 250;

  /// <summary>
  /// 文字表示位置 Y座標
  /// </summary>
  const int kDebugText2PosY = 280;

  /// <summary>
  /// 文字表示位置 Y座標
  /// </summary>
  const int kDebugText3PosY = 310;

  /// <summary>
  /// 文字表示位置 Y座標
  /// </summary>
  const int kDebugText4PosY = 340;

  /// <summary>
  /// 文字表示位置 Y座標
  /// </summary>
  const int kDebugText5PosY = 370;

  /// <summary>
  /// 文字表示位置 X座標
  /// </summary>
  const int kGameInfoTextPosX = 795;

  /// <summary>
  /// 文字の色
  /// </summary>
  const int kStringForeColor = GetColor(255, 255, 255);   // 白色

  /// <summary>
  /// 背景の色
  /// </summary>
  const int kBackScreenColor = GetColor(0, 0, 0);         // 黒色
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
BackGroundSide::BackGroundSide()
  : Task(TaskId::kBackGroundSide)
  , current_phase_(PhaseType::kInitialize)
  , graphic_handle_()
  , alpha_(0)
  , alpha_adjust_(0)
  , x_pos_left_(0)
  , y_pos_left_(0)
  , x_pos_right_(0)
  , y_pos_right_(0)
  , image_size_width_(0)
  , image_size_height_(0)
  , back_left_side_()
  , back_right_side_()
  , font_handle_(0) {

  // コンソールに出力
  std::cout << "BackGroundSide コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
BackGroundSide::~BackGroundSide() {

  // コンソールに出力
  std::cout << "~BackGroundSide デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns></returns>
void BackGroundSide::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //-------------------------
    //初期化処理フェーズ
    //-------------------------
    Initialize();
    // 現在フェーズを「起動待機」に変更する
    ChangeCurrentPhase(PhaseType::kStartWait);
    break;
  }
  case PhaseType::kStartWait: {
    //-------------------------
    //起動待機フェーズ
    //-------------------------
    break;
  }
  case PhaseType::kStartUp: {
    //-------------------------
    //起動フェーズ
    //-------------------------
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 透過率調整値の符号を反転しておく
      ChangeAlphaAdjustSign();
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //-------------------------
    //処理中フェーズ
    //-------------------------
    break;
  }
  case PhaseType::kFinalizeBefore: {
    //-------------------------
    //終了処理前フェーズ
    //-------------------------
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //-------------------------
    //終了処理フェーズ
    //-------------------------
    // 現在フェーズを「終了済み」に変更する
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BackGroundSide::Render() {

  switch (current_phase_) {
  case PhaseType::kStartUp:
  case PhaseType::kProcess:
  case PhaseType::kFinalizeBefore: {

    DrawBox(back_left_side_.left.x, back_left_side_.left.y, back_left_side_.right.x, back_left_side_.right.y, kBackScreenColor, true);
    DrawBox(back_right_side_.left.x, back_right_side_.left.y, back_right_side_.right.x, back_right_side_.right.y, kBackScreenColor, true);

    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

    // 背景画像の描画
    DrawRectGraph(x_pos_left_, y_pos_left_, x_pos_left_, y_pos_left_,
      image_size_width_, image_size_height_, graphic_handle_, true, false);
    DrawRectGraph(x_pos_right_, y_pos_right_, x_pos_right_, y_pos_right_,
      image_size_width_, image_size_height_, graphic_handle_, true, false);

    // 文字列の描画処理
    DrawStringToHandle(kTitleTextPosX, kTitleTextPosY, kDisplayStringTitle, kStringForeColor, font_handle_.at(static_cast<int>(FontSize::k25)));
    DrawStringToHandle(kTitleTextPosX, kText1PosY, kDisplayStringText1, kStringForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
    DrawStringToHandle(kTitleTextPosX, kText2PosY, kDisplayStringText2, kStringForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
    DrawStringToHandle(kTitleTextPosX, kText3PosY, kDisplayStringText3, kStringForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
    DrawStringToHandle(kTitleTextPosX, kText4PosY, kDisplayStringText4, kStringForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
    DrawStringToHandle(kTitleTextPosX, kText5PosY, kDisplayStringText5, kStringForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));

    GameInfo* game_info = GameInfo::GetGameInfoInstance();
    if (game_info == nullptr) {
      return;
    }
    bool is_debug = game_info->IsDebug();
    if (is_debug) {
      DrawStringToHandle(kTitleTextPosX, kTitleDebugTextPosY, kDisplayDebugStringTitle, kStringForeColor, font_handle_.at(static_cast<int>(FontSize::k25)));
      DrawStringToHandle(kTitleTextPosX, kDebugText1PosY, kDisplayDebugStringText1, kStringForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
      DrawStringToHandle(kTitleTextPosX, kDebugText2PosY, kDisplayDebugStringText2, kStringForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
      DrawStringToHandle(kTitleTextPosX, kDebugText3PosY, kDisplayDebugStringText3, kStringForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
      DrawStringToHandle(kTitleTextPosX, kDebugText4PosY, kDisplayDebugStringText4, kStringForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
      DrawStringToHandle(kTitleTextPosX, kDebugText5PosY, kDisplayDebugStringText5, kStringForeColor, font_handle_.at(static_cast<int>(FontSize::k20)));
    }

    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

    break;
  }
  }
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BackGroundSide::Initialize() {

  //-------------------------------------
  //透過率の設定
  //-------------------------------------
  alpha_ = kAlphaMin;
  alpha_adjust_ = kAlphaAdjustInit;

  //-------------------------------------
  //描画する画像の位置とサイズの設定
  //-------------------------------------
  // ゲーム情報のインスタンスを取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // マップ中央の位置を取得(マップ表示時の調整値)
  int map_left = game_info->GetMapPositionX();
  int map_width = game_info->GetMapWidth();
  int map_height = game_info->GetMapHeight();
  int window_width = game_info->GetResolutionWidth();

  // フォント情報生成
  FontInfo* font_info = FontInfo::GetFontInfoInstance();
  if (font_info == nullptr) {
    return;
  }
  int font_handle = font_info->GetFontInfo(kFontName, kFontSizeText, kFontWidth, DX_FONTTYPE_NORMAL);
  font_handle_.push_back(font_handle);
  font_handle = font_info->GetFontInfo(kFontName, kFontSizeTitle, kFontWidth, DX_FONTTYPE_NORMAL);
  font_handle_.push_back(font_handle);

  // 描画座標の設定
  x_pos_left_ = kOriginX;
  y_pos_left_ = kOriginY;
  // 描画座標の設定
  x_pos_right_ = map_left + map_width;
  y_pos_right_ = kOriginY;
  // 描画する画像サイズ
  image_size_width_ = map_left;
  image_size_height_ = map_height;

  // 黒背景 描画座標
  back_left_side_.left.x = kOriginX;
  back_left_side_.left.y = kOriginY;
  back_left_side_.right.x = map_left;
  back_left_side_.right.y = map_height;

  back_right_side_.left.x = map_left + map_width;
  back_right_side_.left.y = kOriginY;
  back_right_side_.right.x = window_width;
  back_right_side_.right.y = map_height;
}

/// <summary>
/// 透過率変更処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool BackGroundSide::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率に調整値を加算
  alpha_ += alpha_adjust_;

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= kAlphaMax) {
    alpha_ = kAlphaMax;
    is_finish = true;
  }
  else if (alpha_ <= kAlphaMin) {
    alpha_ = kAlphaMin;
    is_finish = true;
  }

  return is_finish;
}

/// <summary>
/// 透過率の符号を変更
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void BackGroundSide::ChangeAlphaAdjustSign() {

  alpha_adjust_ = kMinusSign * alpha_adjust_;
}