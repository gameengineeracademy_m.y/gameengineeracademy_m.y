﻿#pragma once

#include <iostream>

/// <summary>
/// カットインマネージャイベントインターフェース
/// </summary>
class CutInManagerEventInterface {
public:

  /// <summary>
  /// コンストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  CutInManagerEventInterface() {

    // コンソールに出力
    std::cout << "CutInManagerEventInterface コンストラクタ " << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~CutInManagerEventInterface() {

    // コンソールに出力
    std::cout << "~CutInManagerEventInterface デストラクタ " << std::endl;
  }

  /// <summary>
  /// カットイン処理終了通知
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnFinishCutIn() = 0;
};