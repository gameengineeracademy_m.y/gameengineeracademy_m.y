﻿#pragma once

/// <summary>
/// タスクID一覧
/// </summary>
enum class TaskId {
  kDummyTask,            // 初期設定用ダミータスク
  kLevelChanger,         // レベルチェンジャータスク
  kBootLevel,            // ブートレベルタスク
  kTitleLevel,           // タイトルレベルタスク
  kBattleLevel,          // バトルレベルタスク
  kResultLevel,          // リザルトレベルタスク
  kTitleInfoUi,          // タイトルUI情報タスク
  kResultBackGround,     // リザルト背景タスク
  kResultInfoUi,         // リザルトUI情報タスク
  kFieldManager,         // フィールドマネージャタスク
  kPlayer,               // プレイヤータスク
  kEnemyA,               // 敵キャラA
  kEnemyB,               // 敵キャラB
  kExtendItem,           // 残機UPアイテム
  kPowerUpItem,          // パワーUPアイテム
  kBulletManager,        // バレットマネージャタスク
  kBarrageManager,       // バレッジマネージャタスク
  kEffectManager,        // エフェクトマネージャタスク
  kCutInManager,         // カットインマネージャタスク
  kHitPointBarManager,   // ヒットポイントバータスク
  kBackGroundSide,       // バックグラウンドサイドタスク
  kGameTimer,            // タイマータスク
  kDebugMenu,            // デバッグメニュータスク
  kFinishUi,             // フィニッシュUIタスク
  kPauseUi,              // ポーズUIタスク
  kSoundSetup,           // サウンド設定タスク
  kGameMode,             // ゲームモードタスク
  kScoreBoard,           // スコアボードタスク
  kMenuController,       // メニューコントローラタスク
  kVolumeController,     // ボリュームコントローラタスク
  kPlayerController,     // プレイヤーコントローラタスク
  kSoundManager,         // サウンドマネージャタスク
  kTaskIdMaxIndex        // タスクID最大値
};