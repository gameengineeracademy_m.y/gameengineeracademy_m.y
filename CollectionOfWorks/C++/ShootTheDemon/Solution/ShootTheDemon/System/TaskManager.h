﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include <map>
#include <vector>
#include <iostream>

/// <summary>
/// タスクマネージャ
/// </summary>
/// <remarks>
/// タスクの毎フレームの更新処理、描画処理を実行管理
/// </remarks>
class TaskManager {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  TaskManager();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~TaskManager();

  /// <summary>
  /// タスクを積む
  /// </summary>
  /// <param name=""> タスク </param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool AddTask(Task*);

  /// <summary>
  /// タスクを降ろす
  /// </summary>
  /// <param name=""> タスクID </param>
  /// <returns> タスク </returns>
  Task* ReleaseTask(TaskId);

  /// <summary>
  /// タスクの1フレームの処理を実行する
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void UpdateTask(float);

  /// タスクの1フレームの描画処理を実行する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void RenderTask();


private:

  /// <summary>
  /// タスクリスト
  /// </summary>
  std::map<TaskId, Task*> task_list_;

  /// <summary>
  /// 追加するタスクのタスクリスト
  /// </summary>
  std::vector<Task*> add_task_list_;

  /// <summary>
  /// 降ろすタスクのタスクIDリスト
  /// </summary>
  std::vector<TaskId> release_task_list_;

  /// <summary>
  /// 毎フレーム更新処理中かの有無
  /// </summary>
  bool is_updating_;
};