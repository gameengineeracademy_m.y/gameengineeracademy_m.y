﻿#pragma once

#include "DxLib.h"
#include "Game/GameInfo.h"
#include <iostream>

/// <summary>
/// カットイン
/// </summary>
class CutIn {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,      // 初期化フェーズ
    kStartUp,         // 起動フェーズ
    kProcess,         // 処理中フェーズ
    kFinalizeBefore,  // 終了処理前フェーズ
    kFinalize,        // 終了処理フェーズ
    kFinalized,       // 終了処理済みフェーズ
    kPhaseMaxIndex    // フェーズ数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  CutIn();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~CutIn();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Update();

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render();

  /// <summary>
  /// グラフィックハンドルのセット
  /// </summary>
  /// <param name="graphic_handle"> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandle(int graphic_handle) { graphic_handle_ = graphic_handle; }

  /// <summary>
  /// グラフィックハンドルの取得
  /// </summary>
  /// <param name="graphic_handle"> グラフィックハンドル </param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandle() { return graphic_handle_; }

  /// <summary>
  /// X座標のセット
  /// </summary>
  /// <param name="x_pos"> X座標 </param>
  /// <returns></returns>
  void SetPositionX(int x_pos) { x_pos_ = x_pos; }

  /// <summary>
  /// X座標を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> X座標 </returns>
  int GetPositionX() { return x_pos_; }

  /// <summary>
  /// Y座標のセット
  /// </summary>
  /// <param name="y_pos"> Y座標 </param>
  /// <returns></returns>
  void SetPositionY(int y_pos) { y_pos_ = y_pos; };

  /// <summary>
  /// Y座標を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> Y座標 </returns>
  int GetPositionY() { return y_pos_; }

  /// <summary>
  /// X座標の移動量のセット
  /// </summary>
  /// <param name="move_x_value"> X座標 移動量 </param>
  /// <returns></returns>
  void SetMoveValueX(int move_x_value) { move_x_value_ = move_x_value; }

  /// <summary>
  /// 透過率の符号を変更
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeAlphaAdjustSign();

  /// <summary>
  /// 現在のフェーズを変更
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }

  /// <summary>
  /// 現在のフェーズが処理中かどうか
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理中, false:処理中以外 </returns>
  bool IsProcess() { return current_phase_ == PhaseType::kProcess; }

  /// <summary>
  /// Y座標を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> Y座標 </returns>
  int GetAlpha() { return alpha_; }

  /// <summary>
  /// Y座標を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> Y座標 </returns>
  int GetAlphaAdjust() { return alpha_adjust_; }

private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool Initialize();

  /// <summary>
  /// スクロール準備フェーズ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void PrepareScroll();

  /// <summary>
  /// 透過率変更処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:継続処理中 </returns>
  bool AdjustAlphaValue();


private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// グラフィックハンドル
  /// </summary>
  int graphic_handle_;

  /// <summary>
  /// X座標
  /// </summary>
  int x_pos_;

  /// <summary>
  /// Y座標
  /// </summary>
  int y_pos_;

  /// <summary>
  /// X座標 移動量
  /// </summary>
  int move_x_value_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  int alpha_adjust_;
};