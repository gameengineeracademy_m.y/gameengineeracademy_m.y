﻿#include "System/EffectManager.h"

namespace {

  /// <summary>
  /// リストID 最大値
  /// </summary>
  const int kMaxListId = 10000;

  /// <summary>
  /// リストID リセット値
  /// </summary>
  const int kResetListId = 0;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjust = 10;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
EffectManager::EffectManager()
  : Task(TaskId::kEffectManager)
  , effect_list_()
  , generate_effect_list_()
  , current_phase_(PhaseType::kProcess)
  , list_id_max_(0)
  , alpha_(kAlphaMax) {

  // コンソールに出力
  std::cout << "EffectManager コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
EffectManager::~EffectManager() {

  // コンソールに出力
  std::cout << "~EffectManager コンストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void EffectManager::Update(float process_time) {

  std::vector<int> finish_effect_list;
  bool is_finish = false;

  switch (current_phase_) {
  case PhaseType::kStop: {
    //---------------------------
    //停止フェーズ
    //---------------------------
    return;
  }
  case PhaseType::kFinalize: {
    //---------------------------
    //停止フェーズ
    //---------------------------
    if (alpha_ == kAlphaMin) {
      ChangeCurrentPhase(PhaseType::kFinalized);
    }
    // 透過率を調整
    alpha_ -= kAlphaAdjust;
    if (alpha_ <= kAlphaMin) {
      alpha_ = kAlphaMin;
    }
    break;
  }
  }

  // エフェクト更新処理
  for (auto effect : generate_effect_list_) {
    // 終了済みなら終了リストに追加する
    is_finish = effect.second->IsFinish();
    if (is_finish) {
      finish_effect_list.push_back(effect.first);
      continue;
    }
    if (current_phase_ == PhaseType::kFinalize) {
      effect.second->CheckAlphaValue(alpha_);
    }
    is_finish = effect.second->Update(process_time);
    if (is_finish) {
      finish_effect_list.push_back(effect.first);
      continue;
    }
  }

  for (auto list_id : finish_effect_list) {
    // エフェクトをエフェクトマネージャから降ろす
    Effect* release_effect = ReleaseEffect(list_id);
    if (release_effect != nullptr) {
      delete release_effect;
      release_effect = nullptr;
    }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void EffectManager::Render() {

  for (auto effect : generate_effect_list_) {
    // エフェクトを実行する
    effect.second->Render();
  }
}


Effect* EffectManager::RegisterEffect(EffectId effect_id, Effect::EffectType effect_type) {

  // エフェクト
  Effect* effect = nullptr;
  // 指定のエフェクトIDがエフェクトリストに存在するか確認
  // 存在しない場合はエフェクトを生成してリストに登録
  if (effect_list_.find(effect_id) != effect_list_.end()) {
    return nullptr;
  }
  effect = new Effect(effect_id);
  if (effect == nullptr) {
    return nullptr;
  }
  // エフェクト種類
  effect->SetEffectType(effect_type);

  effect_list_[effect_id] = effect;
  return effect;
}



/// <summary>
/// エフェクトの生成
/// </summary>
/// <param name=""> エフェクトID </param>
/// <returns> 生成したエフェクト </returns>
Effect* EffectManager::CreateEffect(EffectId effect_id) {

  // エフェクト
  Effect* effect = nullptr;
  // リストに存在しないエフェクトなら処理終了
  // リストに存在するならエフェクトを生成し、生成リストに追加する
  if (effect_list_.find(effect_id) == effect_list_.end()) {
    return nullptr;
  }

  // 対象のエフェクトのクローンを生成
  effect = effect_list_[effect_id]->GenerateClone();
  if (effect == nullptr) {
    return nullptr;
  }

  // エフェクトを降ろす設定をリセットする
  effect->SetResetReleaseEffect();

  // エフェクトの種類に応じて実行リストに追加する
  int index = list_id_max_;
  while (true) {
    if (generate_effect_list_.find(index) == generate_effect_list_.end()) {
      generate_effect_list_[index] = effect;
      list_id_max_ = ++index;
      break;
    }
    ++index;
  }
  if (list_id_max_ >= kMaxListId) {
    list_id_max_ = kResetListId;
  }

  return effect;
}

/// <summary>
/// エフェクトを降ろす
/// </summary>
/// <param name="index"> インデックス </param>
/// <returns> 降ろしたエフェクト </returns>
Effect* EffectManager::ReleaseEffect(int index) {

  Effect* effect = nullptr;
  // エフェクトリストに存在するエフェクトか確認
  if (generate_effect_list_.find(index) == generate_effect_list_.end()) {
    return nullptr;
  }

  // エフェクトを取得し、エフェクトを降ろす設定
  effect = generate_effect_list_[index];
  effect->SetReleaseEffect();
  // 生成リストからエフェクトを降ろす
  generate_effect_list_.erase(index);

  return effect;
}

/// <summary>
/// 生成したエフェクトをすべて終了させる
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void EffectManager::FinishGenerateEffect() {

  for (auto effect : generate_effect_list_) {
    effect.second->SetEffectFinish();
  }
}

/// <summary>
/// エフェクトの破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void EffectManager::DisposeEffect() {

  std::vector<int> finish_list_id;
  std::vector<EffectId> finish_effect_id;

  // 実行中のエフェクトの破棄
  for (auto effect : generate_effect_list_) {
    finish_list_id.push_back(effect.first);
  }

  // エフェクトを生成リストから降ろす
  for (auto index : finish_list_id) {
    Effect* release_effect = ReleaseEffect(index);
    if (release_effect != nullptr) {
      delete release_effect;
      release_effect = nullptr;
    }
  }
  // 終了リストIDのクリア
  finish_list_id.clear();

  // エフェクトリストの破棄
  for (auto effect : effect_list_) {
    finish_effect_id.push_back(effect.first);
  }

  // エフェクトをエフェクトリストから降ろす
  for (auto effect_id : finish_effect_id) {
    Effect* effect = effect_list_[effect_id];
    for (int i = 0; i < effect->GetEffectHandleSize(); ++i) {
      // 画像リソースの破棄
      DeleteGraph(effect->GetEffectHandle(i));
    }
    // エフェクトリストから降ろし、エフェクトの破棄
    effect_list_.erase(effect_id);
    if (effect != nullptr) {
      delete effect;
      effect = nullptr;
    }
  }
}

/// <summary>
/// エフェクトを取得
/// </summary>
/// <param name="index"> インデックス </param>
/// <returns></returns>
Effect* EffectManager::GetEffect(int index) {

  // エフェクトリストに存在するエフェクトか確認
  if (generate_effect_list_.find(index) == generate_effect_list_.end()) {
    return nullptr;
  }

  Effect* effect = generate_effect_list_[index];

  return effect;
}