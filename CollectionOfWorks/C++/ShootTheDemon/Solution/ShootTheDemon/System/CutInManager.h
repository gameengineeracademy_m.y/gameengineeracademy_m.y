﻿#pragma once

#include "System/Task.h"
#include "System/CutIn.h"
#include "System/CutInManagerEventInterface.h"
#include "System/FontInfo.h"
#include <unordered_map>
#include <vector>
#include <array>
#include <string>

/// <summary>
/// カットインマネージャ
/// </summary>
class CutInManager : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,       // 初期化処理フェーズ
    kPrepare,          // 準備フェーズ
    kProcess,          // 処理中フェーズ
    kStop,             // 停止中フェーズ
    kFinalize,         // 終了処理フェーズ
    kFinalized,        // 終了処理済みフェーズ
    kPhaseMaxIndex     // フェーズ数
  };

  /// <summary>
  /// 文字描画フェーズの種類
  /// </summary>
  enum class DrawPhase {
    kInitialize,       // 初期化フェーズ
    kWait,             // 待機フェーズ
    kStart,            // 開始フェーズ
    kProcess,          // 処理中フェーズ
    kFinalizeBefore,   // 終了処理前フェーズ
    kFinalize,         // 終了処理フェーズ
    kFinalized,        // 終了処理済みフェーズ
    kPhaseMaxIndex     // フェーズ数
  };

  /// <summary>
  /// カットインの種類
  /// </summary>
  enum class ColorType {
    kBlue,          // 青色
    kPurple,        // 紫色
    kRed,           // 赤色
    kWhite,         // 白色
    kYellow,        // 黄色
    kGreen,         // 緑色
    kTypeMaxIndex   // 種類数
  };

  /// <summary>
  /// フォントサイズの種類
  /// </summary>
  enum class FontSize {
    k35,            // 文字サイズ35
    k50,            // 文字サイズ50
    kTypeMaxIndex   // 種類数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> カットインマネージャイベントインターフェース </param>
  /// <returns></returns>
  CutInManager(CutInManagerEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~CutInManager();

  /// <summary>
  /// 毎フレームの更新処理
  /// </summary>
  /// <param names=""> 処理時間 </param>
  /// <returns>  </returns>
  void Update(float) override;

  /// <summary>
  /// 毎フレームの描画処理
  /// </summary>
  /// <param names=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// 敵の行動フェーズのフェーズ数をセット
  /// </summary>
  /// <param name=""> フェーズ数 </param>
  /// <returns></returns>
  void SetActionPhaseNumber(int phase_num) { enemy_pahse_number_ = phase_num; }

  /// <summary>
  /// 実行カットインのセット
  /// </summary>
  /// <param name=""> カットインの種類 </param>
  /// <returns></returns>
  void SetExecCutIn(ColorType color_type) { exec_cut_in_list_ = cut_in_list_[color_type]; }

  /// <summary>
  /// ボスフェーズのセット
  /// </summary>
  /// <param name=""> 現在のボスフェーズ </param>
  /// <returns></returns>
  void SetBossPhaseName(std::string phase_name) { boss_pahse_name_ = phase_name; }

  /// <summary>
  /// カットインのフェーズを変更する
  /// </summary>
  /// <param name=""> カットインのフェーズの種類 </param>
  /// <returns></returns>
  void ChangeCutInPhase(CutIn::PhaseType);

  /// <summary>
  /// 実行カットインの破棄
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DisposeExecCutIn();

  /// <summary>
  /// カットイン全体の破棄
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DisposeCutIn();

  /// <summary>
  /// フェーズの種類を変更する
  /// </summary>
  /// <param names="phase_type"> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; };

  /// <summary>
  /// 色の種類を変更する
  /// </summary>
  /// <param names="phase_type"> フェーズの種類 </param>
  /// <returns></returns>
  void ChangePreparePhase(ColorType color_type) { prepare_phase_ = color_type; };

  /// <summary>
  /// 描画フェーズを変更する
  /// </summary>
  /// <param names="phase_type"> 描画フェーズ </param>
  /// <returns></returns>
  void ChangeDrawPhase(DrawPhase phase_type) { draw_phase_ = phase_type; };

  /// <summary>
  /// 現在のフェーズが終了処理済みか確認
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理済み, false:終了処理済み以外 </returns>
  bool IsFinalized() { return current_phase_ == PhaseType::kFinalized; }

  /// <summary>
  /// 描画フェーズが終了処理済みか確認
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了処理済み, false:終了処理済み以外 </returns>
  bool IsDrawFinalized() { return draw_phase_ == DrawPhase::kFinalized; }

private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Initialize();

  /// <summary>
  /// 準備処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:正常終了, false:異常終了 </returns>
  bool Prepare();

  /// <summary>
  /// カットイン処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void UpdateCutIn(float);

  /// <summary>
  /// 描画初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DrawInitialize();

  /// <summary>
  /// 透過率調整処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:継続処理中 </returns>
  bool AdjustAlphaValue();

  /// <summary>
  /// 透過率の符号を変更
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeAlphaAdjustSign();

  /// <summary>
  /// 文字描画位置調整処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void AdjustMoveValue();


private:

  /// <summary>
  /// カットインマネージャイベントインターフェース
  /// </summary>
  CutInManagerEventInterface& cut_in_manager_event_interface_;

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 登録リスト
  /// </summary>
  std::unordered_map<ColorType, std::vector<std::vector<CutIn*>>> cut_in_list_;

  /// <summary>
  /// 実行カットインリスト
  /// </summary>
  std::vector<std::vector<CutIn*>> exec_cut_in_list_;

  /// <summary>
  /// 準備のフェーズ
  /// </summary>
  ColorType prepare_phase_;

  /// <summary>
  /// ボスフェーズ名
  /// </summary>
  std::string boss_pahse_name_;

  /// <summary>
  /// 敵の行動フェーズの現在のフェーズ数
  /// </summary>
  int enemy_pahse_number_;

  /// <summary>
  /// 描画文字 X座標
  /// </summary>
  int x_pos_;

  /// <summary>
  /// 描画文字 Y座標
  /// </summary>
  int y_pos_;

  /// <summary>
  /// 描画文字 移動量
  /// </summary>
  int move_x_;

  /// <summary>
  /// 描画文字 表示座標
  /// </summary>
  int x_pos_disp_;

  /// <summary>
  /// 描画文字 表示座標
  /// </summary>
  std::string disp_text_;

  /// <summary>
  /// 描画フェーズ
  /// </summary>
  DrawPhase draw_phase_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  int alpha_adjust_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// フォントハンドル
  /// </summary>
  std::vector<int> font_handle_;
};