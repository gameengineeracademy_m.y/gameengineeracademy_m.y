﻿#include "System/Effect.h"

namespace {

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// ハンドル配列サイズ
  /// </summary>
  const int kArraySize = 1;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 表示拡大率 初期値
  /// </summary>
  const float kInitExtRate = 1.0f;

  /// <summary>
  /// 表示拡大率 最小値
  /// </summary>
  const float kExtRateMin = 0.0f;

  /// <summary>
  /// 表示角度 初期値
  /// </summary>
  const float kInitAngle = 0.0f;

  /// <summary>
  /// 角度 最小値
  /// </summary>
  const float kAngleMin = 0.0f;

  /// <summary>
  /// 描画インデックス 初期値
  /// </summary>
  const int kInitIndex = 0;

  /// <summary>
  /// 累積時間 リセット
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 符号反転
  /// </summary>
  const int kMinusSign = -1;

  /// <summary>
  /// インデックス調整値
  /// </summary>
  const int kIndexAdjust = 6;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> エフェクトID </param>
/// <returns></returns>
Effect::Effect(EffectId effect_id)
  : effect_id_(effect_id)
  , effect_handle_()
  , effect_index_(kInitIndex)
  , is_finish_(false)
  , is_release_(false)
  , accumulate_time_(0.0f)
  , effect_data_() {

  // コンソールに出力
  std::cout << "Effect コンストラクタ" << std::endl;
}

/// <summary>
/// コピーコンストラクタ
/// </summary>
/// <param name=""> コピー元クラス </param>
/// <returns></returns>
Effect::Effect(const Effect& effect) {

  // コンソールに出力
  std::cout << "Effect コピーコンストラクタ" << std::endl;

  effect_id_ = effect.effect_id_;
  effect_handle_ = effect.effect_handle_;
  effect_index_ = effect.effect_index_;
  is_finish_ = effect.is_finish_;
  is_release_ = effect.is_release_;
  accumulate_time_ = effect.accumulate_time_;
  effect_data_ = effect.effect_data_;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Effect::~Effect() {

  // コンソールに出力
  std::cout << "~Effect デストラクタ" << std::endl;
}

/// <summary>
/// バレッジクローン生成
/// </summary>
/// <param name=""></param>
/// <returns> バレッジ </returns>
Effect* Effect::GenerateClone() {

  // コピーコンストラクタを使用して生成
  Effect* effect = nullptr;
  effect = new Effect(*this);
  if (effect == nullptr) {
    return nullptr;
  }

  return effect;
}

/// <summary>
/// アルファ値強制セット
/// </summary>
/// <param name=""> 透過率 </param>
/// <returns></returns>
void Effect::CheckAlphaValue(int alpha) {

  if (effect_data_.alpha.value > alpha) {
    effect_data_.alpha.value = alpha;
  }
}

/// <summary>
/// 画像取得情報のセット
/// </summary>
/// <param name="total_num"> 総分割数 </param>
/// <param name="div_x"> 分割 横方向 </param>
/// <param name="div_y"> 分割 縦方向 </param>
/// <param name="width"> サイズ 幅 </param>
/// <param name="height"> サイズ 高さ </param>
/// <returns></returns>
void Effect::SetLoadDivData(int total_num, int div_x, int div_y,
                            int width, int height) {

  effect_data_.div.total = total_num;
  effect_data_.div.x = div_x;
  effect_data_.div.y = div_y;
  effect_data_.div.width = width;
  effect_data_.div.height = height;
}

/// <summary>
/// アルファ値の関連値セット
/// </summary>
/// <param name="value"> 初期値 </param>
/// <param name="adjust"> 調整値 </param>
/// <param name="max"> 最大値 </param>
/// <param name="min"> 最小値 </param>
/// <returns></returns>
void Effect::SetAlpha(int value, int adjust, int max, int min) {

  effect_data_.alpha.value = value;
  effect_data_.alpha.adjust = adjust;
  effect_data_.alpha.max = max;
  effect_data_.alpha.min = min;
}

/// <summary>
/// 拡大率の関連値セット
/// </summary>
/// <param name="value"> 初期値 </param>
/// <param name="adjust"> 調整値 </param>
/// <param name="max"> 最大値 </param>
/// <param name="min"> 最小値 </param>
/// <returns></returns>
void Effect::SetExtRateX(float value, float adjust, float max, float min) {

  effect_data_.ex_rate_x.value = value;
  effect_data_.ex_rate_x.adjust = adjust;
  effect_data_.ex_rate_x.max = max;
  effect_data_.ex_rate_x.min = min;
}

/// <summary>
/// 拡大率の関連値セット
/// </summary>
/// <param name="value"> 初期値 </param>
/// <param name="adjust"> 調整値 </param>
/// <param name="max"> 最大値 </param>
/// <param name="min"> 最小値 </param>
/// <returns></returns>
void Effect::SetExtRateY(float value, float adjust, float max, float min) {

  effect_data_.ex_rate_y.value = value;
  effect_data_.ex_rate_y.adjust = adjust;
  effect_data_.ex_rate_y.max = max;
  effect_data_.ex_rate_y.min = min;
}

/// <summary>
/// 角度の関連値セット
/// </summary>
/// <param name="value"> 初期値 </param>
/// <param name="adjust"> 調整値 </param>
/// <param name="max"> 最大値 </param>
/// <param name="min"> 最小値 </param>
/// <returns></returns>
void Effect::SetAngle(float value, float adjust, float max, float min) {

  effect_data_.angle.value = value;
  effect_data_.angle.adjust = adjust;
  effect_data_.angle.max = max;
  effect_data_.angle.min = min;
}

/// <summary>
/// 更新処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true:終了処理, false:処理中 </returns>
bool Effect::Update(float process_time) {

  bool is_finish = false;

  // 処理が終了かどうか確認
  if (IsFinish()) {
    return true;
  }

  //----------------------------------
  //各更新処理
  //----------------------------------
  switch (effect_id_) {
  case EffectId::kPlayerDeath: {
    // プレイヤー死亡
    is_finish = UpdatePlayerDeath();
    if (is_finish) {
      return true;
    }
    break;
  }
  case EffectId::kExplosion: {
    // プレイヤー爆発
    is_finish = UpdatePlayerExplosion();
    if (is_finish) {
      return true;
    }
    break;
  }
  case EffectId::kReborn: {
    // プレイヤー復活
    is_finish = UpdatePlayerReborn();
    if (is_finish) {
      return true;
    }
    break;
  }
  case EffectId::kDisappearNormal: {
    // 通常弾消滅
    is_finish = UpdateDisappearNormal();
    if (is_finish) {
      return true;
    }
    break;
  }
  case EffectId::kDisappearBlueBall: {
    // 青弾消滅
    is_finish = UpdateDisappearBlueBall();
    if (is_finish) {
      return true;
    }
    break;
  }
  case EffectId::kDisappearPurpleBall: {
    // 紫弾消滅
    is_finish = UpdateDisappearPurpleBall();
    if (is_finish) {
      return true;
    }
    break;
  }
  case EffectId::kDisappearRedBall: {
    // 赤弾消滅
    is_finish = UpdateDisappearRedBall();
    if (is_finish) {
      return true;
    }
    break;
  }
  case EffectId::kDisappearYellowBall: {
    // 黄弾消滅
    is_finish = UpdateDisappearYellowBall();
    if (is_finish) {
      return true;
    }
    break;
  }
  case EffectId::kDisappearGreenBall: {
    // 緑弾消滅
    is_finish = UpdateDisappearGreenBall();
    if (is_finish) {
      return true;
    }
    break;
  }
  case EffectId::kEnemyExplosion: {
    // 敵の爆発
    is_finish = UpdateEnemyExplosion();
    if (is_finish) {
      return true;
    }
    break;
  }
  case EffectId::kEnemyRupture: {
    // 敵の破裂
    is_finish = UpdateEnemyRupture();
    if (is_finish) {
      return true;
    }
    break;
  }
  case EffectId::kItemSummon: {
    // アイテム生成
    is_finish = UpdateItemSummon();
    if (is_finish) {
      return true;
    }
    break;
  }
  }

  // 累積時間を加算する
  accumulate_time_ += process_time;

  //----------------------------------
  //描画インデックス
  //----------------------------------
  int array_size = static_cast<int>(effect_handle_.size());
  if (array_size > kArraySize) {
    // 指定待機時間が経過したらインデックスをインクリメント
    if (accumulate_time_ >= effect_data_.detail.wait_time) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      if (effect_index_ < array_size - kArraySize) {
        ++effect_index_;
      }
    }
  }

  //----------------------------------
  //透過率
  //----------------------------------
  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (effect_data_.alpha.value >= effect_data_.alpha.max) {
    effect_data_.alpha.value = effect_data_.alpha.max;
  }
  else if (effect_data_.alpha.value <= effect_data_.alpha.min) {
    effect_data_.alpha.value = effect_data_.alpha.min;
  }

  //----------------------------------
  //表示倍率
  //----------------------------------
  // 倍率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (effect_data_.ex_rate_x.value >= effect_data_.ex_rate_x.max) {
    effect_data_.ex_rate_x.value = effect_data_.ex_rate_x.max;
  }
  else if (effect_data_.ex_rate_x.value <= effect_data_.ex_rate_x.min) {
    effect_data_.ex_rate_x.value = effect_data_.ex_rate_x.min;
  }

  // 倍率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (effect_data_.ex_rate_y.value >= effect_data_.ex_rate_y.max) {
    effect_data_.ex_rate_y.value = effect_data_.ex_rate_y.max;
  }
  else if (effect_data_.ex_rate_y.value <= effect_data_.ex_rate_y.min) {
    effect_data_.ex_rate_y.value = effect_data_.ex_rate_y.min;
  }

  //----------------------------------
  //角度
  //----------------------------------
  // 角度が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (effect_data_.angle.value >= effect_data_.angle.max) {
    effect_data_.angle.value = effect_data_.angle.max;
  }
  else if (effect_data_.angle.value <= effect_data_.angle.min) {
    effect_data_.angle.value = effect_data_.angle.min;
  }

  return false;
}

/// <summary>
/// 描画処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Effect::Render() {

  RenderExRate();
}


/// <summary>
/// プレイヤー死亡エフェクトの更新処理
/// </summary>
/// <param name=""></param>
/// <returns> true:終了処理, false:処理中 </returns>
bool Effect::UpdatePlayerDeath() {

  if (effect_data_.ex_rate_x.value <= effect_data_.ex_rate_x.min) {
    return true;
  }

  // 透過率に調整値を加算
  effect_data_.alpha.value += effect_data_.alpha.adjust;
  // 表示倍率に調整値を加算
  effect_data_.ex_rate_x.value += effect_data_.ex_rate_x.adjust;
  effect_data_.ex_rate_y.value += effect_data_.ex_rate_y.adjust;
  // 角度に調整値を加算
  effect_data_.angle.value += effect_data_.angle.adjust;

  return false;
}

/// <summary>
/// プレイヤー爆発エフェクトの更新処理
/// </summary>
/// <param name=""></param>
/// <returns> true:終了処理, false:処理中 </returns>
bool Effect::UpdatePlayerExplosion() {

  // 表示倍率に調整値を加算
  effect_data_.ex_rate_x.value += effect_data_.ex_rate_x.adjust;
  effect_data_.ex_rate_y.value += effect_data_.ex_rate_y.adjust;

  int array_size = static_cast<int>(effect_handle_.size());
  if (effect_index_ < array_size - kIndexAdjust) {
    return false;
  }

  // 累積時間をリセット
  accumulate_time_ = kResetTime;
  // 透過率に調整値を加算
  effect_data_.alpha.value += effect_data_.alpha.adjust;

  if (effect_data_.alpha.value <= effect_data_.alpha.min) {
    return true;
  }

  // 角度に調整値を加算
  effect_data_.angle.value += effect_data_.angle.adjust;

  return false;
}

/// <summary>
/// プレイヤー復活エフェクトの更新処理
/// </summary>
/// <param name=""></param>
/// <returns> true:終了処理, false:処理中 </returns>
bool Effect::UpdatePlayerReborn() {

  int array_size = static_cast<int>(effect_handle_.size());
  if (effect_index_ >= array_size) {
    return true;
  }

  // 透過率に調整値を加算
  effect_data_.alpha.value += effect_data_.alpha.adjust;
  // 表示倍率に調整値を加算
  effect_data_.ex_rate_x.value += effect_data_.ex_rate_x.adjust;
  effect_data_.ex_rate_y.value += effect_data_.ex_rate_y.adjust;
  // 角度に調整値を加算
  effect_data_.angle.value += effect_data_.angle.adjust;

  return false;
}

/// <summary>
/// 通常弾の消滅の更新処理
/// </summary>
/// <param name=""></param>
/// <returns> true:終了処理, false:処理中 </returns>
bool Effect::UpdateDisappearNormal() {

  // 表示倍率に調整値を加算
  effect_data_.ex_rate_x.value += effect_data_.ex_rate_x.adjust;
  effect_data_.ex_rate_y.value += effect_data_.ex_rate_y.adjust;

  int array_size = static_cast<int>(effect_handle_.size());
  if (effect_index_ < array_size - kArraySize) {
    return false;
  }

  // 透過率に調整値を加算
  effect_data_.alpha.value += effect_data_.alpha.adjust;
  // 角度に調整値を加算
  effect_data_.angle.value += effect_data_.angle.adjust;

  if (effect_data_.alpha.value <= effect_data_.alpha.min) {
    return true;
  }

  return false;
}

/// <summary>
/// 青弾の消滅の更新処理
/// </summary>
/// <param name=""></param>
/// <returns> true:終了処理, false:処理中 </returns>
bool Effect::UpdateDisappearBlueBall() {

  // 表示倍率に調整値を加算
  effect_data_.ex_rate_x.value += effect_data_.ex_rate_x.adjust;
  effect_data_.ex_rate_y.value += effect_data_.ex_rate_y.adjust;

  int array_size = static_cast<int>(effect_handle_.size());
  if (effect_index_ < array_size - kArraySize) {
    return false;
  }

  // 透過率に調整値を加算
  effect_data_.alpha.value += effect_data_.alpha.adjust;
  // 角度に調整値を加算
  effect_data_.angle.value += effect_data_.angle.adjust;

  if (effect_data_.alpha.value <= effect_data_.alpha.min) {
    return true;
  }

  return false;
}

/// <summary>
/// 紫弾の消滅の更新処理
/// </summary>
/// <param name=""></param>
/// <returns> true:終了処理, false:処理中 </returns>
bool Effect::UpdateDisappearPurpleBall() {

  // 表示倍率に調整値を加算
  effect_data_.ex_rate_x.value += effect_data_.ex_rate_x.adjust;
  effect_data_.ex_rate_y.value += effect_data_.ex_rate_y.adjust;

  int array_size = static_cast<int>(effect_handle_.size());
  if (effect_index_ < array_size - kArraySize) {
    return false;
  }

  // 透過率に調整値を加算
  effect_data_.alpha.value += effect_data_.alpha.adjust;
  // 角度に調整値を加算
  effect_data_.angle.value += effect_data_.angle.adjust;

  if (effect_data_.alpha.value <= effect_data_.alpha.min) {
    return true;
  }

  return false;
}

/// <summary>
/// 赤弾の消滅の更新処理
/// </summary>
/// <param name=""></param>
/// <returns> true:終了処理, false:処理中 </returns>
bool Effect::UpdateDisappearRedBall() {

  // 表示倍率に調整値を加算
  effect_data_.ex_rate_x.value += effect_data_.ex_rate_x.adjust;
  effect_data_.ex_rate_y.value += effect_data_.ex_rate_y.adjust;

  int array_size = static_cast<int>(effect_handle_.size());
  if (effect_index_ < array_size - kArraySize) {
    return false;
  }

  // 透過率に調整値を加算
  effect_data_.alpha.value += effect_data_.alpha.adjust;
  // 角度に調整値を加算
  effect_data_.angle.value += effect_data_.angle.adjust;

  if (effect_data_.alpha.value <= effect_data_.alpha.min) {
    return true;
  }

  return false;
}

/// <summary>
/// 黄弾の消滅の更新処理
/// </summary>
/// <param name=""></param>
/// <returns> true:終了処理, false:処理中 </returns>
bool Effect::UpdateDisappearYellowBall() {

  // 表示倍率に調整値を加算
  effect_data_.ex_rate_x.value += effect_data_.ex_rate_x.adjust;
  effect_data_.ex_rate_y.value += effect_data_.ex_rate_y.adjust;

  int array_size = static_cast<int>(effect_handle_.size());
  if (effect_index_ < array_size - kArraySize) {
    return false;
  }

  // 透過率に調整値を加算
  effect_data_.alpha.value += effect_data_.alpha.adjust;
  // 角度に調整値を加算
  effect_data_.angle.value += effect_data_.angle.adjust;

  if (effect_data_.alpha.value <= effect_data_.alpha.min) {
    return true;
  }

  return false;
}

/// <summary>
/// 緑弾の消滅の更新処理
/// </summary>
/// <param name=""></param>
/// <returns> true:終了処理, false:処理中 </returns>
bool Effect::UpdateDisappearGreenBall() {

  // 表示倍率に調整値を加算
  effect_data_.ex_rate_x.value += effect_data_.ex_rate_x.adjust;
  effect_data_.ex_rate_y.value += effect_data_.ex_rate_y.adjust;

  int array_size = static_cast<int>(effect_handle_.size());
  if (effect_index_ < array_size - kArraySize) {
    return false;
  }

  // 透過率に調整値を加算
  effect_data_.alpha.value += effect_data_.alpha.adjust;
  // 角度に調整値を加算
  effect_data_.angle.value += effect_data_.angle.adjust;

  if (effect_data_.alpha.value <= effect_data_.alpha.min) {
    return true;
  }

  return false;
}

/// <summary>
/// 敵の爆発の更新処理
/// </summary>
/// <param name=""></param>
/// <returns> true:終了処理, false:処理中 </returns>
bool Effect::UpdateEnemyExplosion() {

  // 表示倍率に調整値を加算
  effect_data_.ex_rate_x.value += effect_data_.ex_rate_x.adjust;
  effect_data_.ex_rate_y.value += effect_data_.ex_rate_y.adjust;

  int array_size = static_cast<int>(effect_handle_.size());
  if (effect_index_ < array_size - kArraySize) {
    return false;
  }

  // 透過率に調整値を加算
  effect_data_.alpha.value += effect_data_.alpha.adjust;
  // 角度に調整値を加算
  effect_data_.angle.value += effect_data_.angle.adjust;

  if (effect_data_.alpha.value <= effect_data_.alpha.min) {
    return true;
  }

  return false;
}

/// <summary>
/// 敵の破裂の更新処理
/// </summary>
/// <param name=""></param>
/// <returns> true:終了処理, false:処理中 </returns>
bool Effect::UpdateEnemyRupture() {

  // 表示倍率に調整値を加算
  effect_data_.ex_rate_x.value += effect_data_.ex_rate_x.adjust;
  effect_data_.ex_rate_y.value += effect_data_.ex_rate_y.adjust;

  int array_size = static_cast<int>(effect_handle_.size());
  if (effect_index_ < array_size - kArraySize) {
    return false;
  }

  // 透過率に調整値を加算
  effect_data_.alpha.value += effect_data_.alpha.adjust;
  // 角度に調整値を加算
  effect_data_.angle.value += effect_data_.angle.adjust;

  if (effect_data_.alpha.value <= effect_data_.alpha.min) {
    return true;
  }

  return false;
}

/// <summary>
/// アイテム生成の更新処理
/// </summary>
/// <param name=""></param>
/// <returns> true:終了処理, false:処理中 </returns>
bool Effect::UpdateItemSummon() {

  // 表示倍率に調整値を加算
  effect_data_.ex_rate_x.value += effect_data_.ex_rate_x.adjust;
  effect_data_.ex_rate_y.value += effect_data_.ex_rate_y.adjust;

  int array_size = static_cast<int>(effect_handle_.size());
  if (effect_index_ < array_size - kArraySize) {
    return false;
  }

  // 透過率に調整値を加算
  effect_data_.alpha.value += effect_data_.alpha.adjust;
  // 角度に調整値を加算
  effect_data_.angle.value += effect_data_.angle.adjust;

  if (effect_data_.alpha.value <= effect_data_.alpha.min) {
    return true;
  }

  return false;
}

/// <summary>
/// エフェクトの実行 拡大・縮小
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Effect::RenderExRate() {

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, effect_data_.alpha.value);

  // エフェクトの描画
  DrawRotaGraph(effect_data_.pos.x, effect_data_.pos.y, effect_data_.ex_rate_x.value, effect_data_.angle.value, effect_handle_.at(effect_index_), true);

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
}