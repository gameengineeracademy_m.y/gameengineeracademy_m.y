﻿#include "System/CutInManager.h"

namespace {

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSizeText01 = 35;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kFontSizeText02 = 50;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 1;

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Reggae One";

  /// <summary>
  /// フェーズテキスト
  /// </summary>
  const char* kPhaseText = " / 6 フェーズ";

  /// <summary>
  /// X座標初期位置
  /// </summary>
  const int kInitText01PosX = 330;

  /// <summary>
  /// Y座標初期位置
  /// </summary>
  const int kInitText01PosY = 290;

  /// <summary>
  /// X座標初期位置
  /// </summary>
  const int kInitText02PosX = 345;

  /// <summary>
  /// Y座標初期位置
  /// </summary>
  const int kInitText02PosY = 335;

  /// <summary>
  /// 文字の色
  /// </summary>
  const int kStringForeColor = GetColor(0, 0, 0);   // 黒色

  /// <summary>
  /// 青背景画像保存パス
  /// </summary>
  const char* kImageBluePath = "Asset/Image/cut_in_blue.png";

  /// <summary>
  /// 紫背景画像保存パス
  /// </summary>
  const char* kImagePurplePath = "Asset/Image/cut_in_purple.png";

  /// <summary>
  /// 赤背景画像保存パス
  /// </summary>
  const char* kImageRedPath = "Asset/Image/cut_in_red.png";

  /// <summary>
  /// 白背景画像保存パス
  /// </summary>
  const char* kImageWhitePath = "Asset/Image/cut_in_white.png";

  /// <summary>
  /// 黄背景画像保存パス
  /// </summary>
  const char* kImageYellowPath = "Asset/Image/cut_in_yellow.png";

  /// <summary>
  /// 緑背景画像保存パス
  /// </summary>
  const char* kImageGreenPath = "Asset/Image/cut_in_green.png";

  /// <summary>
  /// 総分割数
  /// </summary>
  const int kTotalDivNum = 152;

  /// <summary>
  /// 分割数 横方向
  /// </summary>
  const int kDivNumX = 19;

  /// <summary>
  /// 分割数 縦方向
  /// </summary>
  const int kDivNumY = 8;

  /// <summary>
  /// 分割後 画像サイズ
  /// </summary>
  const int kImageSize = 32;

  /// <summary>
  /// カットインY座標初期位置
  /// </summary>
  const int kInitPosY = 256;

  /// <summary>
  /// エラー
  /// </summary>
  const int kLoadError = -1;

  /// <summary>
  /// 2倍
  /// </summary>
  const int kDouble = 2;

  /// <summary>
  /// 余りゼロ
  /// </summary>
  const int kZero = 0;

  /// <summary>
  /// スクロール X方向移動量
  /// </summary>
  const int kMoveValueX = 4;

  /// <summary>
  /// 先頭インデックス
  /// </summary>
  const int kStartIndexX = 0;

  /// <summary>
  /// 先頭インデックス
  /// </summary>
  const int kStartIndexY = 0;

  /// <summary>
  /// 透過率 調整値
  /// </summary>
  const int kAlphaAdjustInit = 20;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 符号反転
  /// </summary>
  const int kMinusSign = -1;

  /// <summary>
  /// リセット時間
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 待機時間
  /// </summary>
  const float kWaitTime = 1.5f;

  /// <summary>
  /// 半分にする
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// X座標 移動量
  /// </summary>
  const int kTextMoveValueX = 5;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name="cut_in_manager_event_interface"> カットインマネージャイベントインターフェース </param>
/// <returns></returns>
CutInManager::CutInManager(CutInManagerEventInterface& cut_in_manager_event_interface)
  : Task(TaskId::kCutInManager)
  , cut_in_manager_event_interface_(cut_in_manager_event_interface)
  , current_phase_(PhaseType::kInitialize)
  , cut_in_list_()
  , exec_cut_in_list_()
  , prepare_phase_(ColorType::kBlue)
  , boss_pahse_name_()
  , enemy_pahse_number_(0)
  , x_pos_(0)
  , y_pos_(0)
  , move_x_(0)
  , x_pos_disp_(0)
  , disp_text_()
  , draw_phase_(DrawPhase::kInitialize)
  , alpha_(0)
  , alpha_adjust_(0)
  , accumulate_time_(0.0f)
  , font_handle_() {

  // コンソールに出力
  std::cout << "CutInManager コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
CutInManager::~CutInManager() {

  // コンソールに出力
  std::cout << "~CutInManager デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void CutInManager::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //------------------------------
    //初期化フェーズ
    //------------------------------
    Initialize();
    // 現在のフェーズを「処理中」に変更
    ChangeCurrentPhase(PhaseType::kPrepare);
    break;
  }
  case PhaseType::kPrepare: {
    bool is_finish = Prepare();
    if (is_finish) {
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //------------------------------
    //処理中フェーズ
    //------------------------------
    // 背景処理(毎フレーム)
    UpdateCutIn(process_time);
    break;
  }
  case PhaseType::kStop: {
    //------------------------------
    //停止中フェーズ
    //------------------------------
    break;
  }
  case PhaseType::kFinalize: {
    //------------------------------
    //終了処理フェーズ
    //------------------------------
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
void CutInManager::Initialize() {

  // フォント情報生成
  FontInfo* font_info = FontInfo::GetFontInfoInstance();
  if (font_info == nullptr) {
    return;
  }
  int font_handle = font_info->GetFontInfo(kFontName, kFontSizeText01, kFontWidth, DX_FONTTYPE_NORMAL);
  font_handle_.push_back(font_handle);
  font_handle = font_info->GetFontInfo(kFontName, kFontSizeText02, kFontWidth, DX_FONTTYPE_NORMAL);
  font_handle_.push_back(font_handle);

  //-------------------------------------------
  //配列の初期化
  //-------------------------------------------
  int max_index = static_cast<int>(ColorType::kTypeMaxIndex);
  std::vector<CutIn*> initial_set_array;
  // 配列の空情報を作成
  for (int x = 0; x < kDivNumX; ++x) {
    initial_set_array.push_back(nullptr);
  }

  for (int i = 0; i < max_index; ++i) {
    // インデックスを取得する
    ColorType color_type = static_cast<ColorType>(i);
    // カットインリストを空情報で初期化する
    cut_in_list_[color_type].resize(kDivNumY);
    for (int y = 0; y < kDivNumY; ++y) {
      cut_in_list_[color_type][y].resize(kDivNumX);
      cut_in_list_[color_type][y] = initial_set_array;
    }
  }

  // 実行カットインリストを空情報で初期化する
  exec_cut_in_list_.resize(kDivNumY);
  for (int y = 0; y < kDivNumY; ++y) {
    exec_cut_in_list_[y].resize(kDivNumX);
    exec_cut_in_list_[y] = initial_set_array;
  }
}

/// <summary>
/// 準備処理
/// </summary>
/// <param name=""></param>
/// <returns> true:正常終了, false:異常終了 </returns>
bool CutInManager::Prepare() {

  std::string image_path;
  ColorType next_type = ColorType::kTypeMaxIndex;

  switch (prepare_phase_) {
  case ColorType::kBlue: {
    image_path = kImageBluePath;
    next_type = ColorType::kPurple;
    break;
  }
  case ColorType::kPurple: {
    image_path = kImagePurplePath;
    next_type = ColorType::kRed;
    break;
  }
  case ColorType::kRed: {
    image_path = kImageRedPath;
    next_type = ColorType::kWhite;
    break;
  }
  case ColorType::kWhite: {
    image_path = kImageWhitePath;
    next_type = ColorType::kYellow;
    break;
  }
  case ColorType::kYellow: {
    image_path = kImageYellowPath;
    next_type = ColorType::kGreen;
    break;
  }
  case ColorType::kGreen: {
    image_path = kImageGreenPath;
    next_type = ColorType::kTypeMaxIndex;
    break;
  }
  case ColorType::kTypeMaxIndex: {
    return true;
  }
  }

  int graphic_handle[kTotalDivNum];
  // 画像のロード
  int result = LoadDivGraph(image_path.c_str(), kTotalDivNum, kDivNumX, kDivNumY, kImageSize, kImageSize, graphic_handle);
  if (result == kLoadError) {
    return false;
  }

  // ゲーム情報からマップ表示位置を取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return false;
  }
  int disp_pos_x = game_info->GetMapPositionX();
  int disp_pos_y = game_info->GetMapPositionY();

  int index = 0;
  // 背景の生成
  for (int y = 0; y < kDivNumY; ++y) {
    for (int x = 0; x < kDivNumX; ++x) {
      // 背景の生成
      CutIn* cut_in = new CutIn();
      // カットインに表示座標、移動量のセット
      cut_in->SetPositionX(disp_pos_x + x * kImageSize - kImageSize);
      cut_in->SetPositionY(disp_pos_y + y * kImageSize + kInitPosY);
      cut_in->SetMoveValueX(kMoveValueX);
      // グラフィックハンドルを渡す
      cut_in->SetGraphicHandle(graphic_handle[index]);
      // インデックスをインクリメント
      ++index;
      // リストに格納
      cut_in_list_[prepare_phase_].at(y).at(x) = cut_in;
    }
  }

  // 準備フェーズを変更
  ChangePreparePhase(next_type);

  return false;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void CutInManager::UpdateCutIn(float process_time) {

  // 描画位置の設定
  for (int y = 0; y < static_cast<int>(exec_cut_in_list_.size()); ++y) {
    for (int x = 0; x < static_cast<int>(exec_cut_in_list_.at(0).size()); ++x) {
      if (exec_cut_in_list_.at(y).at(x) != nullptr) {
        exec_cut_in_list_.at(y).at(x)->Update();
      }
    }
  }

  switch (draw_phase_) {
  case DrawPhase::kInitialize: {
    //--------------------------
    //初期化フェーズ
    //--------------------------
    DrawInitialize();
    // 描画フェーズを「待機」に変更する
    ChangeDrawPhase(DrawPhase::kWait);
    break;
  }
  case DrawPhase::kWait: {
    //--------------------------
    //待機フェーズ
    //--------------------------
    if (exec_cut_in_list_.at(kStartIndexY).at(kStartIndexX) == nullptr) {
      return;
    }
    bool is_check = exec_cut_in_list_.at(kStartIndexY).at(kStartIndexX)->IsProcess();
    if (!is_check) {
      return;
    }
    // 描画フェーズを「開始」に変更する
    ChangeDrawPhase(DrawPhase::kStart);
    break;
  }
  case DrawPhase::kStart: {
    //--------------------------
    //開始フェーズ
    //--------------------------
    // 文字の描画位置を調整
    AdjustMoveValue();
    // 透過率の調整
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 累積時間をリセットする
      accumulate_time_ = kResetTime;
      // 描画フェーズを「処理中」に変更する
      ChangeDrawPhase(DrawPhase::kProcess);
    }
    break;
  }
  case DrawPhase::kProcess: {
    //--------------------------
    //処理フェーズ
    //--------------------------
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kWaitTime) {
      // 累積時間をリセットする
      accumulate_time_ = kResetTime;
      // 描画フェーズを「終了処理前」に変更する
      ChangeDrawPhase(DrawPhase::kFinalizeBefore);
      // カットイン側のフェーズ「終了処理前」に変更する
      ChangeCutInPhase(CutIn::PhaseType::kFinalizeBefore);
    }
    break;
  }
  case DrawPhase::kFinalizeBefore: {
    //--------------------------
    //終了処理前フェーズ
    //--------------------------
    // 文字の描画位置を調整
    AdjustMoveValue();
    bool is_finish = AdjustAlphaValue();
    if (is_finish) {
      // 描画フェーズを「終了処理」に変更する
      ChangeDrawPhase(DrawPhase::kFinalize);
    }
    break;
  }
  case DrawPhase::kFinalize: {
    //--------------------------
    //終了フェーズ
    //--------------------------
    // カットインの破棄
    DisposeExecCutIn();
    // カットイン処理終了通知
    cut_in_manager_event_interface_.OnFinishCutIn();
    // 描画フェーズを「終了処理済み」に変更する
    ChangeDrawPhase(DrawPhase::kFinalized);
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CutInManager::Render() {

  // カットイン描画処理
  for (int y = 0; y < static_cast<int>(exec_cut_in_list_.size()); ++y) {
    for (int x = 0; x < static_cast<int>(exec_cut_in_list_.at(0).size()); ++x) {
      if (exec_cut_in_list_.at(y).at(x) != nullptr) {
        exec_cut_in_list_.at(y).at(x)->Render();
      }
    }
  }

  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_);

  // 文字の描画
  switch (draw_phase_) {
  case DrawPhase::kStart:
  case DrawPhase::kProcess:
  case DrawPhase::kFinalizeBefore: {

    // ボスフェーズ名
    DrawStringToHandle(x_pos_, kInitText01PosY, boss_pahse_name_.c_str(), kStringForeColor, font_handle_.at(static_cast<int>(FontSize::k35)));
    // フェーズ数
    DrawStringToHandle(x_pos_, y_pos_, disp_text_.c_str(), kStringForeColor, font_handle_.at(static_cast<int>(FontSize::k50)));

    break;
  }
  }

  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
}

/// <summary>
/// 描画初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CutInManager::DrawInitialize() {

  //-------------------------------------------
  //透過率の初期設定
  //-------------------------------------------
  alpha_ = kAlphaMin;
  alpha_adjust_ = kAlphaAdjustInit;

  //-------------------------------------------
  //文字描画位置の初期設定
  //-------------------------------------------
  x_pos_ = kInitText02PosX;
  y_pos_ = kInitText02PosY;
  move_x_ = kTextMoveValueX;

  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  // 画面の中心座標を取得
  int x_pos_center = game_info->GetCenterPointX();

  // 文字列の描画処理
  SetFontSize(kFontSizeText02);
  SetFontThickness(kFontWidth);
  ChangeFont(kFontName);
  ChangeFontType(DX_FONTTYPE_ANTIALIASING);

  // 描画文字列の幅を取得
  std::string number = std::to_string(enemy_pahse_number_);
  std::string display_text = number + kPhaseText;

  // テキストのセット
  disp_text_ = display_text;
  // 文字サイズ(幅)を取得
  int string_width = GetDrawStringWidth(display_text.c_str(), static_cast<int>(strlen(display_text.c_str())));
  // 表示位置を算出
  x_pos_disp_ = x_pos_center - (string_width / kHalfValue);
}

/// <summary>
/// カットイン処理のフェーズを変更する
/// </summary>
/// <param name=""> フィールドのフェーズの種類 </param>
/// <returns></returns>
void CutInManager::ChangeCutInPhase(CutIn::PhaseType phase_type) {

  for (int y = 0; y < static_cast<int>(exec_cut_in_list_.size()); ++y) {
    for (int x = 0; x < static_cast<int>(exec_cut_in_list_.at(0).size()); ++x) {
      if (exec_cut_in_list_.at(y).at(x) != nullptr) {
        exec_cut_in_list_.at(y).at(x)->ChangeCurrentPhase(phase_type);
        exec_cut_in_list_.at(y).at(x)->ChangeAlphaAdjustSign();
      }
    }
  }
}

/// <summary>
/// 実行カットインの破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CutInManager::DisposeExecCutIn() {

  for (int y = 0; y < static_cast<int>(exec_cut_in_list_.size()); ++y) {
    for (int x = 0; x < static_cast<int>(exec_cut_in_list_.at(0).size()); ++x) {
      if (exec_cut_in_list_.at(y).at(x) != nullptr) {
        // 背景の破棄
        exec_cut_in_list_.at(y).at(x) = nullptr;
      }
    }
  }

}

/// <summary>
/// カットイン全体の破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CutInManager::DisposeCutIn() {

  for (int y = 0; y < static_cast<int>(exec_cut_in_list_.size()); ++y) {
    for (int x = 0; x < static_cast<int>(exec_cut_in_list_.at(0).size()); ++x) {
      if (exec_cut_in_list_.at(y).at(x) != nullptr) {
        // 背景の破棄
        exec_cut_in_list_.at(y).at(x) = nullptr;
      }
    }
  }

  int max_index = static_cast<int>(ColorType::kTypeMaxIndex);

  for (int i = 0; i < max_index; ++i) {

    ColorType color_type = static_cast<ColorType>(i);

    for (int y = 0; y < static_cast<int>(cut_in_list_[color_type].size()); ++y) {
      for (int x = 0; x < static_cast<int>(cut_in_list_[color_type].at(0).size()); ++x) {

        if (cut_in_list_[color_type].at(y).at(x) != nullptr) {
          if (x < kDivNumX && y < kDivNumY) {
            // 画像リソースを取得し、破棄する
            int graphic_handle = cut_in_list_[color_type].at(y).at(x)->GetGraphicHandle();
            DeleteGraph(graphic_handle);
          }
          // 背景の破棄
          CutIn* cutin = cut_in_list_[color_type].at(y).at(x);

          delete cutin;
          cutin = nullptr;
        }
      }
    }
  }
}

/// <summary>
/// 透過率変更処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool CutInManager::AdjustAlphaValue() {

  bool is_finish = false;

  // 透過率に調整値を加算
  switch (draw_phase_) {
  case DrawPhase::kStart: {
    alpha_ += alpha_adjust_;
    break;
  }
  case DrawPhase::kFinalizeBefore: {
    alpha_ -= alpha_adjust_;
    break;
  }
  }

  // 透過率が最大値よりも大きく、
  // または最小値よりも小さくならないよう調整
  if (alpha_ >= kAlphaMax) {
    alpha_ = kAlphaMax;
    is_finish = true;
  }
  else if (alpha_ <= kAlphaMin) {
    alpha_ = kAlphaMin;
    is_finish = true;
  }

  return is_finish;
}

/// <summary>
/// 透過率の符号を変更
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void CutInManager::ChangeAlphaAdjustSign() {

  alpha_adjust_ = kMinusSign * alpha_adjust_;
}

/// <summary>
/// 文字描画位置調整処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:継続処理中 </returns>
void CutInManager::AdjustMoveValue() {

  // X座標に調整値を減算
  x_pos_ -= move_x_;

  if (draw_phase_ != DrawPhase::kStart) {
    return;
  }

  // 指定位置で止まるよう処理
  if (x_pos_ <= x_pos_disp_) {
    x_pos_ = x_pos_disp_;
  }

}