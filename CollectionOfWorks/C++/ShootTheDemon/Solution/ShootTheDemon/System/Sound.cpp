﻿#include "System/Sound.h"

namespace {

  /// <summary>
  /// 再生中
  /// </summary>
  const int kSoundPlay = 1;

  /// <summary>
  /// 音量 BGM初期値
  /// </summary>
  const int kInitialVolumeBgm = 100;

  /// <summary>
  /// 音量 SE初期値
  /// </summary>
  const int kInitialVolumeSe = 100;

  /// <summary>
  /// サウンド実行エラー
  /// </summary>
  const int kSoundError = -1;

  /// <summary>
  /// ボリューム最大値
  /// </summary>
  const int kSoundVolumeMax = 255;

  /// <summary>
  /// ボリューム最小値
  /// </summary>
  const int kSoundVolumeMin = 0;

  /// <summary>
  /// 符号変換
  /// </summary>
  const int kSignChange = -1;

  /// <summary>
  /// 1000倍を表す ※単位変換で使用
  /// </summary>
  const float kThousandTimes = 1000.0f;

  /// <summary>
  /// 累積時間のリセット値
  /// </summary>
  const float kResetTime = 0.0f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name="sound_id"> サウンドID </param>
/// <returns></returns>
Sound::Sound(SoundId sound_id)
  : sound_id_(sound_id)
  , current_phase_(PhaseType::kPlayStart)
  , sound_type_(SoundType::kNone)
  , list_id_(0)
  , sound_handle_(0)
  , sound_volume_(0)
  , sound_volume_adjust_(0)
  , sound_total_time_(0.0f)
  , accumulate_time_(0.0f)
  , is_pause_(false)
  , is_finish_(false) {

  // コンソールに出力
  std::cout << "Sound コンストラクタ" << std::endl;
}

/// <summary>
/// コピーコンストラクタ
/// </summary>
/// <param name="sound"> コピー元クラス </param>
/// <returns></returns>
Sound::Sound(const Sound& sound) {

  // コンソールに出力
  std::cout << "Sound コピーコンストラクタ" << std::endl;

  sound_id_ = sound.sound_id_;
  current_phase_ = sound.current_phase_;
  sound_type_ = sound.sound_type_;
  list_id_ = sound.list_id_;
  sound_handle_ = sound.sound_handle_;
  sound_volume_ = sound.sound_volume_;
  sound_volume_adjust_ = sound.sound_volume_adjust_;
  sound_total_time_ = sound.sound_total_time_;
  accumulate_time_ = sound.accumulate_time_;
  is_pause_ = sound.is_pause_;
  is_finish_ = sound.is_finish_;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Sound::~Sound() {

  // コンソールに出力
  std::cout << "~Sound デストラクタ" << std::endl;
}

/// <summary>
/// SEサウンド処理(毎フレーム)
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true:再生終了, false:再生処理中 </returns>
bool Sound::PlaySoundSe(float process_time) {

  bool is_top = true;

  switch (current_phase_) {
  case PhaseType::kPlayStart: {
    //----------------------------------
    //再生開始フェーズ
    //----------------------------------
    // 一時停止から再開したなら、停止後から再生を再開する
    // それ以外はサウンドを先頭から実行する
    if (IsPause()) {
      // 一時停止フラグを降ろす
      ReSetPause();
      is_top = false;
    }

    int result = PlaySoundMem(sound_handle_, DX_PLAYTYPE_BACK, is_top);
    if (result == kSoundError) {
      std::cout << "Sound PlaySoundSe：再生エラー" << std::endl;
      return true;
    }
    // 累積時間をリセット
    accumulate_time_ = kResetTime;
    // 現在のフェーズを「再生中」に変更
    ChangeCurrentPhase(PhaseType::kPlay);
    break;
  }
  case PhaseType::kPlay: {
    //----------------------------------
    //再生中フェーズ
    //----------------------------------
    // 累積時間を加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= sound_total_time_) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;
      return true;
    }
    break;
  }
  default: {
    //----------------------------------
    //それ以外
    //----------------------------------
    break;
  }
  }

  return false;
}

/// <summary>
/// BGMサウンド処理(毎フレーム)
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true:再生終了, false:再生処理中 </returns>
bool Sound::PlaySoundBgm(float process_time) {

  bool is_top = true;

  switch (current_phase_) {
  case PhaseType::kPlayStart: {
    //----------------------------------
    //再生開始フェーズ
    //----------------------------------
    // 再生中か確認し、再生中なら処理終了
    if (CheckSoundMem(sound_handle_) == kSoundPlay) {
      return true;
    }
    // 一時停止から再開したなら、停止後から再生を再開する
    // それ以外はサウンドを先頭から実行する
    if (IsPause()) {
      // 一時停止フラグを降ろす
      ReSetPause();
      is_top = false;
    }

    int result = PlaySoundMem(sound_handle_, DX_PLAYTYPE_LOOP, is_top);
    if (result == kSoundError) {
      std::cout << "Sound PlaySoundSe：再生エラー" << std::endl;
      return true;
    }
    // 現在のフェーズを「再生中」に変更
    ChangeCurrentPhase(PhaseType::kPlay);
    break;
  }
  case PhaseType::kPlay: {
    //----------------------------------
    //再生中フェーズ
    //----------------------------------
    break;
  }
  case PhaseType::kFinishBefore: {
    //----------------------------------
    //終了処理前フェーズ
    //----------------------------------
    bool is_finish = ChangeFadeSoundVolume();
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinish);
    }
    break;
  }
  case PhaseType::kFinish: {
    //----------------------------------
    //終了処理フェーズ
    //----------------------------------
    // 終了フラグを立てる
    SetPlayFinish();
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinished);
    return true;
  }
  default: {
    //----------------------------------
    //それ以外
    //----------------------------------
    break;
  }
  }

  return false;
}

/// <summary>
/// サウンドを一時停止する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Sound::Pause() {

  // 再生中でないなら処理終了
  if (CheckSoundMem(sound_handle_) != kSoundPlay) {
    return;
  }

  // サウンドを停止して、現在のフェーズを「一時停止」に変更
  StopSoundMem(sound_handle_);
  ChangeCurrentPhase(PhaseType::kPause);
  SetPause();
}

/// <summary>
/// サウンドを一時停止から再開する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Sound::Resume() {

  // 停止中でないなら処理終了
  if (CheckSoundMem(sound_handle_) == kSoundPlay) {
    return;
  }

  // 現在のフェーズを「再生開始」に変更
  ChangeCurrentPhase(PhaseType::kPlayStart);
}

/// <summary>
/// サウンドボリューム調整値の符号変換
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Sound::ChangeSoundVolumeAdjustSign() {

  switch (current_phase_) {
  case PhaseType::kFinishBefore: {

    if (sound_volume_adjust_ > kSoundVolumeMin) {
      sound_volume_adjust_ = kSignChange * sound_volume_adjust_;
    }
    break;
  }
  }
}

/// <summary>
/// サウンドクローン生成
/// </summary>
/// <param name=""></param>
/// <returns> サウンド </returns>
Sound* Sound::GenerateClone() {

  // コピーコンストラクタを使用して生成
  Sound* sound = nullptr;
  sound = new Sound(*this);
  if (sound == nullptr) {
    return nullptr;
  }

  return sound;
}

/// <summary>
/// サウンドのボリュームを変更する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Sound::ChangeSoundVolume() {

  // 音量を設定する
  ChangeVolumeSoundMem(sound_volume_, sound_handle_);
}

/// <summary>
/// サウンドのボリュームを徐々に変更する
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool Sound::ChangeFadeSoundVolume() {

  if (sound_volume_ >= kSoundVolumeMax &&
      sound_volume_ <= kSoundVolumeMin) {
    return true;
  }

  // サウンドボリュームを調整する
  sound_volume_ += sound_volume_adjust_;
  if (sound_volume_ <= kSoundVolumeMin) {
    sound_volume_ = kSoundVolumeMin;
  }
  if (sound_volume_ >= kSoundVolumeMax) {
    sound_volume_ = kSoundVolumeMax;
  }
  // サウンドのボリュームを変更する
  ChangeSoundVolume();

  return false;
}

/// <summary>
/// サウンドの総再生時間を取得する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void Sound::GetTotalTime() {

  int total_time = static_cast<int>(GetSoundTotalTime(sound_handle_));
  sound_total_time_ = total_time / kThousandTimes;
}