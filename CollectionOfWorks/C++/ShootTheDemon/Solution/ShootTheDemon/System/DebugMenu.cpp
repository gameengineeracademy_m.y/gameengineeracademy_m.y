﻿#include "System/DebugMenu.h"

namespace {

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "ＭＳ 明朝";

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kTextFontSize = 20;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kTextFontWidth = 8;

  /// <summary>
  /// メニュー画面 背景色
  /// </summary>
  const int kBackColor = GetColor(0, 60, 255);

  /// <summary>
  /// メニュー画面 背景透過率
  /// </summary>
  const int kAlphaScreen = 150;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// メニュー画面 メニューテキスト文字色
  /// </summary>
  const int kMenuTextForeColor = GetColor(255, 255, 0);

  /// <summary>
  /// メニュー画面 ボタンテキスト文字色
  /// </summary>
  const int kButtonTextForeColor = GetColor(255, 255, 255);

  /// <summary>
  /// メニュー画面 ボタン色 選択中
  /// </summary>
  const int kButtonSelectColor = GetColor(0, 140, 255);

  /// <summary>
  /// メニュー画面 ボタン色 選択外
  /// </summary>
  const int kButtonNoneColor = GetColor(0, 0, 255);

  /// <summary>
  /// メニュー画面 カーソル色
  /// </summary>
  const int kCursorColor = GetColor(255, 0, 0);

  /// <summary>
  /// スクリーン表示位置 X座標
  /// </summary>
  const int kScreenPosX = 15;

  /// <summary>
  /// スクリーン表示位置 Y座標
  /// </summary>
  const int kScreenPosY = 420;

  /// <summary>
  /// スクリーンサイズ 幅
  /// </summary>
  const int kScreenWidth = 200;

  /// <summary>
  /// スクリーンサイズ 高さ
  /// </summary>
  const int kScreenHeight = 340;

  /// <summary>
  /// ボタンサイズ 幅
  /// </summary>
  const int kButtonWidth = 85;

  /// <summary>
  /// ボタンサイズ 高さ
  /// </summary>
  const int kButtonHeight = 35;

  /// <summary>
  /// ボタン表示位置 左側 X座標
  /// </summary>
  const int kButtonLeftPosX = 25;

  /// <summary>
  /// ボタン表示位置 右側 X座標
  /// </summary>
  const int kButtonRightPosX = 120;

  /// <summary>
  /// ボタン表示位置 1行目 Y座標
  /// </summary>
  const int kButtonRow1PosY = 460;

  /// <summary>
  /// ボタン表示位置 2行目 Y座標
  /// </summary>
  const int kButtonRow2PosY = 505;

  /// <summary>
  /// ボタン表示位置 3行目 Y座標
  /// </summary>
  const int kButtonRow3PosY = 550;

  /// <summary>
  /// ボタン表示位置 5行目 Y座標
  /// </summary>
  const int kButtonRow5PosY = 625;

  /// <summary>
  /// ボタン表示位置 6行目 Y座標
  /// </summary>
  const int kButtonRow6PosY = 670;

  /// <summary>
  /// ボタン表示位置 7行目 Y座標
  /// </summary>
  const int kButtonRow7PosY = 715;

  /// <summary>
  /// ボタンテキスト表示位置 調整値
  /// </summary>
  const int kButtonTextPosY = 7;

  /// <summary>
  /// メニューテキスト位置 EnemyA
  /// </summary>
  const int kMenuTextEnemyAPosY = 430;

  /// <summary>
  /// メニューテキスト位置 EnemyB
  /// </summary>
  const int kMenuTextEnemyBPosY = 595;

  /// <summary>
  /// カーソル調整値 固定値
  /// </summary>
  const float kCursorAdjust = 1.0f;

  /// <summary>
  /// カーソル調整値 X方向
  /// </summary>
  const float kCursorPlusAdjustX = 1.5f;

  /// <summary>
  /// カーソル調整値 Y方向
  /// </summary>
  const float kCursorPlusAdjustY = 1.5f;

  /// <summary>
  /// カーソル調整値 X方向
  /// </summary>
  const float kCursorMinusAdjustX = -1.5f;

  /// <summary>
  /// カーソル調整値 Y方向
  /// </summary>
  const float kCursorMinusAdjustY = -1.5f;

  /// <summary>
  /// カーソル 幅
  /// </summary>
  const float kCursorThick = 3.0f;

  /// <summary>
  /// メニューテキスト
  /// </summary>
  const char* kMenuTextEnemyA = "[ EnemyA ]";

  /// <summary>
  /// メニューテキスト
  /// </summary>
  const char* kMenuTextEnemyB = "[ EnemyB ]";

  /// <summary>
  /// ボタンテキスト
  /// </summary>
  const char* kButtonTextPhase01 = "Phase1";

  /// <summary>
  /// ボタンテキスト
  /// </summary>
  const char* kButtonTextPhase02 = "Phase2";

  /// <summary>
  /// ボタンテキスト
  /// </summary>
  const char* kButtonTextPhase03 = "Phase3";

  /// <summary>
  /// ボタンテキスト
  /// </summary>
  const char* kButtonTextPhase04 = "Phase4";

  /// <summary>
  /// ボタンテキスト
  /// </summary>
  const char* kButtonTextPhase05 = "Phase5";

  /// <summary>
  /// ボタンテキスト
  /// </summary>
  const char* kButtonTextPhase06 = "Phase6";

  /// <summary>
  /// ボタンテキスト
  /// </summary>
  const char* kButtonTextPhase07 = "Phase7";

  /// <summary>
  /// ボタンテキスト
  /// </summary>
  const char* kButtonTextPhase08 = "Phase8";

  /// <summary>
  /// 待機時間
  /// </summary>
  const float kWaitTime = 0.7f;

  /// <summary>
  /// リセット時間
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// 半分
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 2
  /// </summary>
  const int kTwo = 2;

  /// <summary>
  /// 偶数
  /// </summary>
  const int kEvenNumber = 0;

  /// <summary>
  /// 奇数
  /// </summary>
  const int kOddNumber = 1;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
DebugMenu::DebugMenu()
  : Task(TaskId::kDebugMenu)
  , current_phase_(PhaseType::kInitialize)
  , button_type_(ButtonType::kEnemyAPhase01)
  , screen_()
  , button_enemy_a_phase01_()
  , button_enemy_a_phase02_()
  , button_enemy_a_phase03_()
  , button_enemy_a_phase04_()
  , button_enemy_a_phase05_()
  , button_enemy_a_phase06_()
  , button_enemy_b_phase01_()
  , button_enemy_b_phase02_()
  , button_enemy_b_phase03_()
  , button_enemy_b_phase04_()
  , button_enemy_b_phase05_()
  , button_enemy_b_phase06_()
  , cursor_()
  , cursor_adjust_()
  , text_enemy_a_phase01_()
  , text_enemy_a_phase02_()
  , text_enemy_a_phase03_()
  , text_enemy_a_phase04_()
  , text_enemy_a_phase05_()
  , text_enemy_a_phase06_()
  , text_enemy_b_phase01_()
  , text_enemy_b_phase02_()
  , text_enemy_b_phase03_()
  , text_enemy_b_phase04_()
  , text_enemy_b_phase05_()
  , text_enemy_b_phase06_()
  , button_color_()
  , accumulate_time_(0.0f) {

  // コンソールに出力
  std::cout << "DebugMenu コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
DebugMenu::~DebugMenu() {

  // コンソールに出力
  std::cout << "~DebugMenu デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void DebugMenu::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //--------------------------
    //初期化フェーズ
    //--------------------------
    bool is_finish = Initialize();
    if (is_finish) {
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //--------------------------
    //処理中フェーズ
    //--------------------------
    bool is_finish = Process(process_time);
    if (is_finish) {
      // 現在のフェーズを「終了処理」に変更
      ChangeCurrentPhase(PhaseType::kFinalize);
    }
    break;
  }
  case PhaseType::kFinalize: {
    //--------------------------
    //終了処理フェーズ
    //--------------------------

    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void DebugMenu::Render() {

  if (current_phase_ != PhaseType::kProcess) {
    return;
  }

  //-------------------------------------------
  // スクリーン
  //-------------------------------------------
  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, kAlphaScreen);
  DrawBox(screen_.left.x, screen_.left.y, screen_.right.x, screen_.right.y, kBackColor, true);
  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);

  //-------------------------------------------
  // ボタン
  //-------------------------------------------
  // フォント名、フォントの種類のセット
  ChangeFont(kFontName);
  ChangeFontType(DX_FONTTYPE_ANTIALIASING);
  SetFontSize(kTextFontSize);
  SetFontThickness(kTextFontWidth);

  // 図の描画処理
  DrawBox(button_enemy_a_phase01_.left.x, button_enemy_a_phase01_.left.y,
          button_enemy_a_phase01_.right.x, button_enemy_a_phase01_.right.y,
          button_color_[static_cast<int>(ButtonType::kEnemyAPhase01)], true);
  DrawBox(button_enemy_a_phase02_.left.x, button_enemy_a_phase02_.left.y,
          button_enemy_a_phase02_.right.x, button_enemy_a_phase02_.right.y,
          button_color_[static_cast<int>(ButtonType::kEnemyAPhase02)], true);
  DrawBox(button_enemy_a_phase03_.left.x, button_enemy_a_phase03_.left.y,
          button_enemy_a_phase03_.right.x, button_enemy_a_phase03_.right.y,
          button_color_[static_cast<int>(ButtonType::kEnemyAPhase03)], true);
  DrawBox(button_enemy_a_phase04_.left.x, button_enemy_a_phase04_.left.y,
          button_enemy_a_phase04_.right.x, button_enemy_a_phase04_.right.y,
          button_color_[static_cast<int>(ButtonType::kEnemyAPhase04)], true);
  DrawBox(button_enemy_a_phase05_.left.x, button_enemy_a_phase05_.left.y,
          button_enemy_a_phase05_.right.x, button_enemy_a_phase05_.right.y,
          button_color_[static_cast<int>(ButtonType::kEnemyAPhase05)], true);
  DrawBox(button_enemy_a_phase06_.left.x, button_enemy_a_phase06_.left.y,
          button_enemy_a_phase06_.right.x, button_enemy_a_phase06_.right.y,
          button_color_[static_cast<int>(ButtonType::kEnemyAPhase06)], true);
  DrawBox(button_enemy_b_phase01_.left.x, button_enemy_b_phase01_.left.y,
          button_enemy_b_phase01_.right.x, button_enemy_b_phase01_.right.y,
          button_color_[static_cast<int>(ButtonType::kEnemyBPhase01)], true);
  DrawBox(button_enemy_b_phase02_.left.x, button_enemy_b_phase02_.left.y,
          button_enemy_b_phase02_.right.x, button_enemy_b_phase02_.right.y,
          button_color_[static_cast<int>(ButtonType::kEnemyBPhase02)], true);
  DrawBox(button_enemy_b_phase03_.left.x, button_enemy_b_phase03_.left.y,
          button_enemy_b_phase03_.right.x, button_enemy_b_phase03_.right.y,
          button_color_[static_cast<int>(ButtonType::kEnemyBPhase03)], true);
  DrawBox(button_enemy_b_phase04_.left.x, button_enemy_b_phase04_.left.y,
          button_enemy_b_phase04_.right.x, button_enemy_b_phase04_.right.y,
          button_color_[static_cast<int>(ButtonType::kEnemyBPhase04)], true);
  DrawBox(button_enemy_b_phase05_.left.x, button_enemy_b_phase05_.left.y,
          button_enemy_b_phase05_.right.x, button_enemy_b_phase05_.right.y,
          button_color_[static_cast<int>(ButtonType::kEnemyBPhase05)], true);
  DrawBox(button_enemy_b_phase06_.left.x, button_enemy_b_phase06_.left.y,
          button_enemy_b_phase06_.right.x, button_enemy_b_phase06_.right.y,
          button_color_[static_cast<int>(ButtonType::kEnemyBPhase06)], true);

  // 文字の描画処理
  DrawString(text_enemy_a_phase01_.x, text_enemy_a_phase01_.y, kButtonTextPhase01, kButtonTextForeColor);
  DrawString(text_enemy_a_phase02_.x, text_enemy_a_phase02_.y, kButtonTextPhase02, kButtonTextForeColor);
  DrawString(text_enemy_a_phase03_.x, text_enemy_a_phase03_.y, kButtonTextPhase03, kButtonTextForeColor);
  DrawString(text_enemy_a_phase04_.x, text_enemy_a_phase04_.y, kButtonTextPhase04, kButtonTextForeColor);
  DrawString(text_enemy_a_phase05_.x, text_enemy_a_phase05_.y, kButtonTextPhase05, kButtonTextForeColor);
  DrawString(text_enemy_a_phase06_.x, text_enemy_a_phase06_.y, kButtonTextPhase06, kButtonTextForeColor);
  DrawString(text_enemy_b_phase01_.x, text_enemy_b_phase01_.y, kButtonTextPhase01, kButtonTextForeColor);
  DrawString(text_enemy_b_phase02_.x, text_enemy_b_phase02_.y, kButtonTextPhase02, kButtonTextForeColor);
  DrawString(text_enemy_b_phase03_.x, text_enemy_b_phase03_.y, kButtonTextPhase03, kButtonTextForeColor);
  DrawString(text_enemy_b_phase04_.x, text_enemy_b_phase04_.y, kButtonTextPhase04, kButtonTextForeColor);
  DrawString(text_enemy_b_phase05_.x, text_enemy_b_phase05_.y, kButtonTextPhase05, kButtonTextForeColor);
  DrawString(text_enemy_b_phase06_.x, text_enemy_b_phase06_.y, kButtonTextPhase06, kButtonTextForeColor);

  DrawString(kButtonLeftPosX, kMenuTextEnemyAPosY, kMenuTextEnemyA, kMenuTextForeColor);
  DrawString(kButtonLeftPosX, kMenuTextEnemyBPosY, kMenuTextEnemyB, kMenuTextForeColor);


  //-------------------------------------------
  // カーソル
  //-------------------------------------------
  // 左
  DrawLineAA(cursor_.left.x, cursor_.left.y,cursor_.left.x,
             cursor_.right.y, kCursorColor, kCursorThick);
  // 右
  DrawLineAA(cursor_.right.x - kCursorAdjust, cursor_.left.y,
             cursor_.right.x - kCursorAdjust, cursor_.right.y, kCursorColor, kCursorThick);
  // 上
  DrawLineAA(cursor_.left.x - kCursorAdjust, cursor_.left.y,
             cursor_.right.x + kCursorAdjust, cursor_.left.y, kCursorColor, kCursorThick);
  // 下
  DrawLineAA(cursor_.left.x - kCursorAdjust, cursor_.right.y,
             cursor_.right.x + kCursorAdjust, cursor_.right.y, kCursorColor, kCursorThick);
}


/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了, false:処理中 </returns>
bool DebugMenu::Initialize() {

  // メニュー画面 本体
  screen_.left.x = kScreenPosX;
  screen_.left.y = kScreenPosY;
  screen_.right.x = kScreenPosX + kScreenWidth;
  screen_.right.y = kScreenPosY + kScreenHeight;

  // メニュー画面 ボタン 敵キャラA
  button_enemy_a_phase01_.left.x = kButtonLeftPosX;
  button_enemy_a_phase01_.left.y = kButtonRow1PosY;
  button_enemy_a_phase01_.right.x = kButtonLeftPosX + kButtonWidth;
  button_enemy_a_phase01_.right.y = kButtonRow1PosY + kButtonHeight;

  button_enemy_a_phase02_.left.x = kButtonRightPosX;
  button_enemy_a_phase02_.left.y = kButtonRow1PosY;
  button_enemy_a_phase02_.right.x = kButtonRightPosX + kButtonWidth;
  button_enemy_a_phase02_.right.y = kButtonRow1PosY + kButtonHeight;

  button_enemy_a_phase03_.left.x = kButtonLeftPosX;
  button_enemy_a_phase03_.left.y = kButtonRow2PosY;
  button_enemy_a_phase03_.right.x = kButtonLeftPosX + kButtonWidth;
  button_enemy_a_phase03_.right.y = kButtonRow2PosY + kButtonHeight;

  button_enemy_a_phase04_.left.x = kButtonRightPosX;
  button_enemy_a_phase04_.left.y = kButtonRow2PosY;
  button_enemy_a_phase04_.right.x = kButtonRightPosX + kButtonWidth;
  button_enemy_a_phase04_.right.y = kButtonRow2PosY + kButtonHeight;

  button_enemy_a_phase05_.left.x = kButtonLeftPosX;
  button_enemy_a_phase05_.left.y = kButtonRow3PosY;
  button_enemy_a_phase05_.right.x = kButtonLeftPosX + kButtonWidth;
  button_enemy_a_phase05_.right.y = kButtonRow3PosY + kButtonHeight;

  button_enemy_a_phase06_.left.x = kButtonRightPosX;
  button_enemy_a_phase06_.left.y = kButtonRow3PosY;
  button_enemy_a_phase06_.right.x = kButtonRightPosX + kButtonWidth;
  button_enemy_a_phase06_.right.y = kButtonRow3PosY + kButtonHeight;

  // メニュー画面 ボタン 敵キャラB
  button_enemy_b_phase01_.left.x = kButtonLeftPosX;
  button_enemy_b_phase01_.left.y = kButtonRow5PosY;
  button_enemy_b_phase01_.right.x = kButtonLeftPosX + kButtonWidth;
  button_enemy_b_phase01_.right.y = kButtonRow5PosY + kButtonHeight;

  button_enemy_b_phase02_.left.x = kButtonRightPosX;
  button_enemy_b_phase02_.left.y = kButtonRow5PosY;
  button_enemy_b_phase02_.right.x = kButtonRightPosX + kButtonWidth;
  button_enemy_b_phase02_.right.y = kButtonRow5PosY + kButtonHeight;

  button_enemy_b_phase03_.left.x = kButtonLeftPosX;
  button_enemy_b_phase03_.left.y = kButtonRow6PosY;
  button_enemy_b_phase03_.right.x = kButtonLeftPosX + kButtonWidth;
  button_enemy_b_phase03_.right.y = kButtonRow6PosY + kButtonHeight;

  button_enemy_b_phase04_.left.x = kButtonRightPosX;
  button_enemy_b_phase04_.left.y = kButtonRow6PosY;
  button_enemy_b_phase04_.right.x = kButtonRightPosX + kButtonWidth;
  button_enemy_b_phase04_.right.y = kButtonRow6PosY + kButtonHeight;

  button_enemy_b_phase05_.left.x = kButtonLeftPosX;
  button_enemy_b_phase05_.left.y = kButtonRow7PosY;
  button_enemy_b_phase05_.right.x = kButtonLeftPosX + kButtonWidth;
  button_enemy_b_phase05_.right.y = kButtonRow7PosY + kButtonHeight;

  button_enemy_b_phase06_.left.x = kButtonRightPosX;
  button_enemy_b_phase06_.left.y = kButtonRow7PosY;
  button_enemy_b_phase06_.right.x = kButtonRightPosX + kButtonWidth;
  button_enemy_b_phase06_.right.y = kButtonRow7PosY + kButtonHeight;

  // ボタンの色
  button_color_[static_cast<int>(ButtonType::kEnemyAPhase01)] = kButtonSelectColor;
  button_color_[static_cast<int>(ButtonType::kEnemyAPhase02)] = kButtonNoneColor;
  button_color_[static_cast<int>(ButtonType::kEnemyAPhase03)] = kButtonNoneColor;
  button_color_[static_cast<int>(ButtonType::kEnemyAPhase04)] = kButtonNoneColor;
  button_color_[static_cast<int>(ButtonType::kEnemyAPhase05)] = kButtonNoneColor;
  button_color_[static_cast<int>(ButtonType::kEnemyAPhase06)] = kButtonNoneColor;
  button_color_[static_cast<int>(ButtonType::kEnemyBPhase01)] = kButtonNoneColor;
  button_color_[static_cast<int>(ButtonType::kEnemyBPhase02)] = kButtonNoneColor;
  button_color_[static_cast<int>(ButtonType::kEnemyBPhase03)] = kButtonNoneColor;
  button_color_[static_cast<int>(ButtonType::kEnemyBPhase04)] = kButtonNoneColor;
  button_color_[static_cast<int>(ButtonType::kEnemyBPhase05)] = kButtonNoneColor;
  button_color_[static_cast<int>(ButtonType::kEnemyBPhase06)] = kButtonNoneColor;

  // ボタンテキスト 位置
  ChangeFont(kFontName);
  ChangeFontType(DX_FONTTYPE_ANTIALIASING);
  SetFontSize(kTextFontSize);
  SetFontThickness(kTextFontWidth);

  int text_width = GetDrawStringWidth(kButtonTextPhase01, static_cast<int>(strlen(kButtonTextPhase01)));
  int button_left_center = (button_enemy_a_phase01_.left.x + button_enemy_a_phase01_.right.x) / kHalfValue;
  int button_right_center = (button_enemy_a_phase02_.left.x + button_enemy_a_phase02_.right.x) / kHalfValue;

  text_enemy_a_phase01_.x = button_left_center - (text_width / kHalfValue);
  text_enemy_a_phase01_.y = kButtonRow1PosY + kButtonTextPosY;
  text_enemy_a_phase02_.x = button_right_center - (text_width / kHalfValue);
  text_enemy_a_phase02_.y = kButtonRow1PosY + kButtonTextPosY;
  text_enemy_a_phase03_.x = button_left_center - (text_width / kHalfValue);
  text_enemy_a_phase03_.y = kButtonRow2PosY + kButtonTextPosY;
  text_enemy_a_phase04_.x = button_right_center - (text_width / kHalfValue);
  text_enemy_a_phase04_.y = kButtonRow2PosY + kButtonTextPosY;
  text_enemy_a_phase05_.x = button_left_center - (text_width / kHalfValue);
  text_enemy_a_phase05_.y = kButtonRow3PosY + kButtonTextPosY;
  text_enemy_a_phase06_.x = button_right_center - (text_width / kHalfValue);
  text_enemy_a_phase06_.y = kButtonRow3PosY + kButtonTextPosY;

  text_enemy_b_phase01_.x = button_left_center - (text_width / kHalfValue);
  text_enemy_b_phase01_.y = kButtonRow5PosY + kButtonTextPosY;
  text_enemy_b_phase02_.x = button_right_center - (text_width / kHalfValue);
  text_enemy_b_phase02_.y = kButtonRow5PosY + kButtonTextPosY;
  text_enemy_b_phase03_.x = button_left_center - (text_width / kHalfValue);
  text_enemy_b_phase03_.y = kButtonRow6PosY + kButtonTextPosY;
  text_enemy_b_phase04_.x = button_right_center - (text_width / kHalfValue);
  text_enemy_b_phase04_.y = kButtonRow6PosY + kButtonTextPosY;
  text_enemy_b_phase05_.x = button_left_center - (text_width / kHalfValue);
  text_enemy_b_phase05_.y = kButtonRow7PosY + kButtonTextPosY;
  text_enemy_b_phase06_.x = button_right_center - (text_width / kHalfValue);
  text_enemy_b_phase06_.y = kButtonRow7PosY + kButtonTextPosY;

  // メニュー画面 カーソル
  cursor_.left.x = static_cast<float>(button_enemy_a_phase01_.left.x);
  cursor_.left.y = static_cast<float>(button_enemy_a_phase01_.left.y);
  cursor_.right.x = static_cast<float>(button_enemy_a_phase01_.left.x + kButtonWidth);
  cursor_.right.y = static_cast<float>(button_enemy_a_phase01_.left.y + kButtonHeight);

  // メニュー画面 カーソル調整量
  cursor_adjust_.x = kCursorPlusAdjustX;
  cursor_adjust_.y = kCursorPlusAdjustY;

  return true;
}

/// <summary>
/// 処理中
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool DebugMenu::Process(float process_time) {

  // 累積時間に加算
  accumulate_time_ += process_time;
  if (accumulate_time_ >= kWaitTime) {
    // 累積時間をリセット
    accumulate_time_ = kResetTime;

    cursor_.left.x -= cursor_adjust_.x;
    cursor_.left.y -= cursor_adjust_.y;
    cursor_.right.x += cursor_adjust_.x;
    cursor_.right.y += cursor_adjust_.y;

    if (cursor_adjust_.x == kCursorPlusAdjustX) {
      cursor_adjust_.x = kCursorMinusAdjustX;
      cursor_adjust_.y = kCursorMinusAdjustY;
    }
    else {
      cursor_adjust_.x = kCursorPlusAdjustX;
      cursor_adjust_.y = kCursorPlusAdjustY;
    }
  }

  return false;
}

/// <summary>
/// カーソル操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void DebugMenu::OperationCursor() {

  float x_pos_left = 0.0f;
  float y_pos_left = 0.0f;
  float x_pos_right = 0.0f;
  float y_pos_right = 0.0f;

  switch (button_type_) {
  case ButtonType::kEnemyAPhase01: {
    x_pos_left = static_cast<float>(button_enemy_a_phase01_.left.x);
    y_pos_left = static_cast<float>(button_enemy_a_phase01_.left.y);
    x_pos_right = static_cast<float>(button_enemy_a_phase01_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_enemy_a_phase01_.left.y + kButtonHeight);
    break;
  }
  case ButtonType::kEnemyAPhase02: {
    x_pos_left = static_cast<float>(button_enemy_a_phase02_.left.x);
    y_pos_left = static_cast<float>(button_enemy_a_phase02_.left.y);
    x_pos_right = static_cast<float>(button_enemy_a_phase02_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_enemy_a_phase02_.left.y + kButtonHeight);
    break;
  }
  case ButtonType::kEnemyAPhase03: {
    x_pos_left = static_cast<float>(button_enemy_a_phase03_.left.x);
    y_pos_left = static_cast<float>(button_enemy_a_phase03_.left.y);
    x_pos_right = static_cast<float>(button_enemy_a_phase03_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_enemy_a_phase03_.left.y + kButtonHeight);
    break;
  }
  case ButtonType::kEnemyAPhase04: {
    x_pos_left = static_cast<float>(button_enemy_a_phase04_.left.x);
    y_pos_left = static_cast<float>(button_enemy_a_phase04_.left.y);
    x_pos_right = static_cast<float>(button_enemy_a_phase04_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_enemy_a_phase04_.left.y + kButtonHeight);
    break;
  }
  case ButtonType::kEnemyAPhase05: {
    x_pos_left = static_cast<float>(button_enemy_a_phase05_.left.x);
    y_pos_left = static_cast<float>(button_enemy_a_phase05_.left.y);
    x_pos_right = static_cast<float>(button_enemy_a_phase05_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_enemy_a_phase05_.left.y + kButtonHeight);
    break;
  }
  case ButtonType::kEnemyAPhase06: {
    x_pos_left = static_cast<float>(button_enemy_a_phase06_.left.x);
    y_pos_left = static_cast<float>(button_enemy_a_phase06_.left.y);
    x_pos_right = static_cast<float>(button_enemy_a_phase06_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_enemy_a_phase06_.left.y + kButtonHeight);
    break;
  }
  case ButtonType::kEnemyBPhase01: {
    x_pos_left = static_cast<float>(button_enemy_b_phase01_.left.x);
    y_pos_left = static_cast<float>(button_enemy_b_phase01_.left.y);
    x_pos_right = static_cast<float>(button_enemy_b_phase01_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_enemy_b_phase01_.left.y + kButtonHeight);
    break;
  }
  case ButtonType::kEnemyBPhase02: {
    x_pos_left = static_cast<float>(button_enemy_b_phase02_.left.x);
    y_pos_left = static_cast<float>(button_enemy_b_phase02_.left.y);
    x_pos_right = static_cast<float>(button_enemy_b_phase02_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_enemy_b_phase02_.left.y + kButtonHeight);
    break;
  }
  case ButtonType::kEnemyBPhase03: {
    x_pos_left = static_cast<float>(button_enemy_b_phase03_.left.x);
    y_pos_left = static_cast<float>(button_enemy_b_phase03_.left.y);
    x_pos_right = static_cast<float>(button_enemy_b_phase03_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_enemy_b_phase03_.left.y + kButtonHeight);
    break;
  }
  case ButtonType::kEnemyBPhase04: {
    x_pos_left = static_cast<float>(button_enemy_b_phase04_.left.x);
    y_pos_left = static_cast<float>(button_enemy_b_phase04_.left.y);
    x_pos_right = static_cast<float>(button_enemy_b_phase04_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_enemy_b_phase04_.left.y + kButtonHeight);
    break;
  }
  case ButtonType::kEnemyBPhase05: {
    x_pos_left = static_cast<float>(button_enemy_b_phase05_.left.x);
    y_pos_left = static_cast<float>(button_enemy_b_phase05_.left.y);
    x_pos_right = static_cast<float>(button_enemy_b_phase05_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_enemy_b_phase05_.left.y + kButtonHeight);
    break;
  }
  case ButtonType::kEnemyBPhase06: {
    x_pos_left = static_cast<float>(button_enemy_b_phase06_.left.x);
    y_pos_left = static_cast<float>(button_enemy_b_phase06_.left.y);
    x_pos_right = static_cast<float>(button_enemy_b_phase06_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_enemy_b_phase06_.left.y + kButtonHeight);
    break;
  }
  }

  cursor_.left.x = x_pos_left;
  cursor_.left.y = y_pos_left;
  cursor_.right.x = x_pos_right;
  cursor_.right.y = y_pos_right;

  for (int i = 0; i < static_cast<int>(ButtonType::kTypeMaxIndex); ++i) {

    if (i == static_cast<int>(button_type_)) {
      button_color_[i] = kButtonSelectColor;
    }
    else {
      button_color_[i] = kButtonNoneColor;
    }

  }
}

/// <summary>
/// カーソル操作 上移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void DebugMenu::OperationCursorUp() {

  switch (button_type_) {
  case ButtonType::kEnemyAPhase01: {
    button_type_ = ButtonType::kEnemyBPhase05;
    break;
  }
  case ButtonType::kEnemyAPhase02: {
    button_type_ = ButtonType::kEnemyBPhase06;
    break;
  }
  default: {
    int button_type = static_cast<int>(button_type_);
    button_type = button_type - kTwo;
    button_type_ = static_cast<ButtonType>(button_type);
    break;
  }
  }

  // カーソルを移動する
  OperationCursor();
}

/// <summary>
/// カーソル操作 下移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void DebugMenu::OperationCursorDown() {

  switch (button_type_) {
  case ButtonType::kEnemyBPhase05: {
    button_type_ = ButtonType::kEnemyAPhase01;
    break;
  }
  case ButtonType::kEnemyBPhase06: {
    button_type_ = ButtonType::kEnemyAPhase02;
    break;
  }
  default: {
    int button_type = static_cast<int>(button_type_);
    button_type = button_type + kTwo;
    button_type_ = static_cast<ButtonType>(button_type);
    break;
  }
  }

  // カーソルを移動する
  OperationCursor();
}

/// <summary>
/// カーソル操作 右移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void DebugMenu::OperationCursorRight() {

  int button_type = static_cast<int>(button_type_);
  if (button_type % kTwo == kEvenNumber) {
    ++button_type;
  }
  else {
    --button_type;
  }

  // ボタンの種類をセット
  button_type_ = static_cast<ButtonType>(button_type);

  // カーソルを移動する
  OperationCursor();
}

/// <summary>
/// カーソル操作 左移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void DebugMenu::OperationCursorLeft() {

  int button_type = static_cast<int>(button_type_);
  if (button_type % kTwo == kEvenNumber) {
    ++button_type;
  }
  else {
    --button_type;
  }

  // ボタンの種類をセット
  button_type_ = static_cast<ButtonType>(button_type);

  // カーソルを移動する
  OperationCursor();
}