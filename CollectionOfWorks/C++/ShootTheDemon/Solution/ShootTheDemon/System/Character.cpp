﻿#include "System/Character.h"

namespace {

  /// <summary>
  /// 半分にする値
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 累積時間のリセット値
  /// </summary>
  const float kResetTime = 0.0f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name="task_id"> タスクID </param>
/// <param name="character_type"> キャラクターの種類 </param>
/// <param name="character_event_interface"> キャラクターイベントインターフェース </param>
/// <returns></returns>
Character::Character(TaskId task_id, CharacterType character_type,
                     CharacterEventInterface& character_event_interface)
  : Task(task_id)
  , character_type_(character_type)
  , character_event_interface_(character_event_interface)
  , direction_type_(DirectionType::kRight)
  , chara_data_()
  , image_width_(0)
  , image_height_(0)
  , image_width_half_(0)
  , image_height_half_(0)
  , radius_(0.0f)
  , accumulate_time_(0.0f)
  , bullet_id_()
  , barrage_id_()
  , reload_time_(0.0f)
  , is_shoot_(false)
  , hp_bar_list_id_(0)
  , is_dead_(true) {

  // コンソールに出力
  std::cout << "Character コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
Character::~Character() {

  // コンソールに出力
  std::cout << "~Character デストラクタ" << std::endl;
}

/// <summary>
/// 画像のサイズをセットする
/// </summary>
/// <param> グラフィックハンドル </param>
/// <returns></returns>
void Character::SetImageSize(int graphic_handle) {

  // グラフィックハンドルから画像のサイズを取得
  GetGraphSize(graphic_handle, &image_width_, &image_height_);
  // 画像の半分のサイズを取得
  image_width_half_ = image_width_ / kHalfValue;
  image_height_half_ = image_height_ / kHalfValue;
}

/// <summary>
/// 弾発射判定を降ろす処理
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void Character::ReleaseIsShoot(float process_time) {

  if (IsShoot()) {
    return;
  }

  // 累積時間に処理時間を加算
  accumulate_time_ += process_time;

  // 累積時間がリロード時間を超えた場合、フラグを降ろす
  if (accumulate_time_ >= reload_time_) {
    // 累積時間をリセット
    accumulate_time_ = kResetTime;
    SetIsShoot(true);
  }
}