﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "System/CharacterEventInterface.h"
#include "Game/BulletId.h"
#include "Game/BarrageId.h"
#include <unordered_map>

/// <summary>
/// キャラクター処理
/// </summary>
/// <remarks>
/// プレイヤー、敵などの共通部分の処理を実装
/// </remarks>
class Character : public Task {
public:

  /// <summary>
  /// キャラクターの種類
  /// </summary>
  enum class CharacterType {
    kPlayer,              // プレイヤー
    kEnemyA,              // 敵A 中ボス
    kEnemyB,              // 敵B 大ボス
    kItemA,               // アイテムA 残機UP
    kItemB,               // アイテムB 攻撃力UP
    kCharacterMaxIndex    // キャラクターの数
  };

  /// <summary>
  /// 移動方向
  /// </summary>
  enum class DirectionType {
    kLeft,
    kRight,
    kDirectionMaxIndex
  };

  /// <summary>
  /// ステータス
  /// </summary>
  struct Status {
    int attack;          // 攻撃力
    int defense;         // 防御力
    int hit_point_init;  // HP 最大値
    int hit_point;       // 現在のHP
  };

  /// <summary>
  /// 配置場所
  /// </summary>
  struct Pos {
    int x;               // 表示座標 X座標
    int y;               // 表示座標 Y座標
    int x_init;          // 初期配置 X座標
    int y_init;          // 初期配置 Y座標
  };

  /// <summary>
  /// 移動量
  /// </summary>
  struct Move {
    int x;             // X方向
    int y;             // Y方向
  };

  /// <summary>
  /// 倍率
  /// </summary>
  struct Rate {
    float display;       // 表示倍率
    float move;          // 移動倍率
  };

  /// <summary>
  /// キャラクター情報
  /// </summary>
  struct CharaData {
    Status status;
    Pos pos;
    Move move;
    Rate rate;
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> タスクID </param>
  /// <param name=""> キャラクターの種類 </param>
  /// <param name=""> キャラクターイベントインターフェース </param>
  /// <returns></returns>
  Character(TaskId, CharacterType, CharacterEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~Character();

  /// <summary>
  /// キャラクター情報を設定する
  /// </summary>
  /// <param name=""> キャラクター情報 </param>
  /// <returns></returns>
  void SetCharacterData(CharaData chara_data) { chara_data_ = chara_data; }

  /// <summary>
  /// キャラクター情報を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> キャラクター情報 </returns>
  CharaData GetCharacterData() { return chara_data_; }

  /// <summary>
  /// X座標を設定する
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <returns></returns>
  void SetPositionX(int x_pos) { chara_data_.pos.x = x_pos; }

  /// <summary>
  /// X座標を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> X座標 </returns>
  int GetPositionX() { return chara_data_.pos.x; }

  /// <summary>
  /// X座標を取得する 小数
  /// </summary>
  /// <param name=""></param>
  /// <returns> X座標 </returns>
  float GetFloatPositionX() { return static_cast<float>(chara_data_.pos.x); }

  /// <summary>
  /// Y座標を設定する
  /// </summary>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  void SetPositionY(int y_pos) { chara_data_.pos.y = y_pos; }

  /// <summary>
  /// Y座標を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> Y座標 </returns>
  int GetPositionY() { return chara_data_.pos.y; }

  /// <summary>
  /// Y座標を取得する 小数
  /// </summary>
  /// <param name=""></param>
  /// <returns> Y座標 </returns>
  float GetFloatPositionY() { return static_cast<float>(chara_data_.pos.y); }

  /// <summary>
  /// X方向移動量を設定する
  /// </summary>
  /// <param name=""> X方向移動量 </param>
  /// <returns></returns>
  void SetMoveValueX(int move_value) { chara_data_.move.x = move_value; }

  /// <summary>
  /// X方向移動量を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> X方向移動量 </returns>
  int GetMoveValueX() { return chara_data_.move.x; }

  /// <summary>
  /// Y方向移動量を設定する
  /// </summary>
  /// <param name=""> Y方向移動量 </param>
  /// <returns></returns>
  void SetMoveValueY(int move_value) { chara_data_.move.y = move_value; }

  /// <summary>
  /// Y方向移動量を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> Y方向移動量 </returns>
  int GetMoveValueY() { return chara_data_.move.y; }

  /// <summary>
  /// 画像のサイズをセットする
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns></returns>
  void SetImageSize(int);

  /// <summary>
  /// 画像の幅を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 画像の幅 </returns>
  int GetImageWidth() { return image_width_; }

  /// <summary>
  /// 画像の高さを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 画像の高さ </returns>
  int GetImageHeight() { return image_height_; }

  /// <summary>
  /// 画像の幅の半分を取得する
  /// </summary>
  /// <param></param>
  /// <returns> 画像の半分の幅 </returns>
  int GetImageHalfWidth() { return image_width_half_; }

  /// <summary>
  /// 画像の高さの半分を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 画像の半分の高さ </returns>
  int GetImageHalfHeight() { return image_height_half_; }

  /// <summary>
  /// 半径をセットする
  /// </summary>
  /// <param name=""> 半径 </param>
  /// <returns></returns>
  void SetRadius(float radius) { radius_ = radius; };

  /// <summary>
  /// 半径を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 半径 </returns>
  float GetRadius() { return radius_; }

  /// <summary>
  /// 攻撃力を設定する
  /// </summary>
  /// <param name=""> 攻撃力 </param>
  /// <returns></returns>
  void SetAttack(int attack) { chara_data_.status.attack = attack; }

  /// <summary>
  /// 攻撃力を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 攻撃力 </returns>
  int GetAttack() { return chara_data_.status.attack; }

  /// <summary>
  /// 防御力を設定する
  /// </summary>
  /// <param name=""> 防御力 </param>
  /// <returns></returns>
  void SetDefense(int defense) { chara_data_.status.defense = defense; }

  /// <summary>
  /// 防御力を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 防御力 </returns>
  int GetDefense() { return chara_data_.status.defense; }

  /// <summary>
  /// ヒットポイントを設定する
  /// </summary>
  /// <param name=""> ヒットポイント </param>
  /// <returns></returns>
  void SetHitPoint(int hit_point) { chara_data_.status.hit_point = hit_point; }

  /// <summary>
  /// ヒットポイントを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> ヒットポイント </returns>
  int GetHitPoint() { return chara_data_.status.hit_point; }

  /// <summary>
  /// 画像の表示倍率をセットする
  /// </summary>
  /// <param name=""> 表示倍率 </param>
  /// <returns></returns>
  void SetImageDispRate(float disp_rate) { chara_data_.rate.display = disp_rate; }

  /// <summary>
  /// 画像の表示倍率を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 表示倍率 </returns>
  float GetImageDispRate() { return chara_data_.rate.display; }

  /// <summary>
  /// リロード時間をセットする
  /// </summary>
  /// <param name=""> リロード時間 </param>
  /// <returns></returns>
  void SetReloadTime(float reload_time) { reload_time_ = reload_time; }

  /// <summary>
  /// リロード時間を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> リロード時間 </returns>
  float GetReloadTime() { return reload_time_; }

  /// <summary>
  /// バレットIDをセットする
  /// </summary>
  /// <param name=""> バレットID </param>
  /// <returns></returns>
  void SetBulletId(BulletId bullet_id) { bullet_id_ = bullet_id; }

  /// <summary>
  /// バレットIDを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> バレットID </returns>
  BulletId GetBulletId() { return bullet_id_; }

  /// <summary>
  /// バレッジ(弾幕)IDをセットする
  /// </summary>
  /// <param name=""> バレッジID </param>
  /// <returns></returns>
  void SetBarrageId(BarrageId barrage_id) { barrage_id_ = barrage_id; }

  /// <summary>
  /// バレッジIDを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> バレッジID </returns>
  BarrageId GetBarrageId() { return barrage_id_; }

  /// <summary>
  /// 弾発射判定を降ろす処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void ReleaseIsShoot(float);

  /// <summary>
  /// 弾発射判定フラグをセットする
  /// </summary>
  /// <param name=""> 弾発射判定フラグ </param>
  /// <returns></returns>
  void SetIsShoot(bool flag) { is_shoot_ = flag; }

  /// <summary>
  /// 弾発射判定フラグを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 弾発射判定フラグ </returns>
  bool IsShoot() { return is_shoot_; }

  /// <summary>
  /// HPバーリストIDをセットする
  /// </summary>
  /// <param name=""> HPバーリストID </param>
  /// <returns></returns>
  void SetHpBarListId(int hp_bar_list_id) { hp_bar_list_id_ = hp_bar_list_id; }

  /// <summary>
  /// HPバーリストIDを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> HPバーリストID </returns>
  int GetHpBarListId() { return hp_bar_list_id_; }

  /// <summary>
  /// 初期位置 X座標をセットする
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <returns></returns>
  void SetInitialPositionX(int x_pos) { chara_data_.pos.x_init = x_pos; }

  /// <summary>
  /// 初期位置 Y座標をセットする
  /// </summary>
  /// <param name=""> Y座標 </param>
  /// <returns></returns>
  void SetInitialPositionY(int y_pos) { chara_data_.pos.y_init = y_pos; }

  /// <summary>
  /// 初期位置 X座標を取得する
  /// </summary>
  /// <param name=""> X座標 </param>
  /// <returns></returns>
  int GetInitialPositionX() { return chara_data_.pos.x_init; }

  /// <summary>
  /// 初期位置 Y座標を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> Y座標 </returns>
  int GetInitialPositionY() { return chara_data_.pos.y_init; }

  /// <summary>
  /// 初期ヒットポイントをセットする
  /// </summary>
  /// <param name="hit_point"> ヒットポイント </param>
  /// <returns></returns>
  void SetInitialHitPoint(int hit_point) { chara_data_.status.hit_point_init = hit_point; }

  /// <summary>
  /// 初期ヒットポイントを取得する
  /// </summary>
  /// <param name=""> 初期ヒットポイント </param>
  /// <returns></returns>
  int GetInitialHitPoint() { return  chara_data_.status.hit_point_init; }

  /// <summary>
  /// キャラクターの種類を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> キャラクターの種類 </returns>
  CharacterType GetCharacterType() { return character_type_; }

  /// <summary>
  /// キャラクターを取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> キャラクター </returns>
  Character* GetCharacter() { return this; }

  /// <summary>
  /// 移動方向を変更する
  /// </summary>
  /// <param name=""> 移動方向の種類 </param>
  /// <returns></returns>
  void ChangeDirectionType(DirectionType direction_type) { direction_type_ = direction_type; }

  /// <summary>
  /// 移動方向を取得する
  /// </summary>
  /// <param name=""></param>
  /// <returns> 移動方向 </returns>
  DirectionType GetDirectionType() { return direction_type_; }

  /// <summary>
  /// 死んだことにする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetDead() { is_dead_ = true; }

  /// <summary>
  /// 生きていることにする
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetAlive() { is_dead_ = false; }

  /// <summary>
  /// 死んでいるかどうか確認する
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:死んでいる, false:生きている </returns>
  bool IsDead() { return is_dead_; }


protected:

  /// <summary>
  /// キャラクターイベントインターフェース
  /// </summary>
  CharacterEventInterface& character_event_interface_;

private:

  /// <summary>
  /// キャラクターの種類
  /// </summary>
  CharacterType character_type_;

  /// <summary>
  /// 移動方向
  /// </summary>
  DirectionType direction_type_;

  /// <summary>
  /// キャラクター情報
  /// </summary>
  CharaData chara_data_;

  /// <summary>
  /// 画像のサイズ 幅
  /// </summary>
  int image_width_;

  /// <summary>
  /// 画像のサイズ 高さ
  /// </summary>
  int image_height_;

  /// <summary>
  /// 画像の幅の半分
  /// </summary>
  int image_width_half_;

  /// <summary>
  /// 画像の高さの半分
  /// </summary>
  int image_height_half_;

  /// <summary>
  /// 中心座標からの半径
  /// </summary>
  float radius_;

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;

  /// <summary>
  /// 弾の種類
  /// </summary>
  BulletId bullet_id_;

  /// <summary>
  /// 弾幕の種類
  /// </summary>
  BarrageId barrage_id_;

  /// <summary>
  /// リロード時間
  /// </summary>
  float reload_time_;

  /// <summary>
  /// 弾発射可能判定フラグ
  /// </summary>
  bool is_shoot_;

  /// <summary>
  /// HPバーのリストID
  /// </summary>
  int hp_bar_list_id_;

  /// <summary>
  /// 生死フラグ
  /// </summary>
  bool is_dead_;
};