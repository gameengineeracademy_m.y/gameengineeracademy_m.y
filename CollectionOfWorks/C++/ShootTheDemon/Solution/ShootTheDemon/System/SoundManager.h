﻿#pragma once

#include "System/Task.h"
#include "System/Sound.h"
#include <unordered_map>
#include <vector>

/// <summary>
/// サウンドマネージャ
/// </summary>
class SoundManager : public Task {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  SoundManager();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~SoundManager();

  /// <summary>
  /// サウンド再生処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// サウンド一時停止処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void PausePlaySound();

  /// <summary>
  /// サウンドを一時停止から再開する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ResumePlaySound();

  /// <summary>
  /// 指定サウンドの取得
  /// </summary>
  /// <param name=""> リストID </param>
  /// <returns> サウンド </returns>
  Sound* GetSoundBgm(int);

  /// <summary>
  /// サウンドの終了
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void FinishSoundBgm();

  /// <summary>
  /// BGMのボリューム変更
  /// </summary>
  /// <param name=""> ボリューム </param>
  /// <returns></returns>
  void ChangeSoundVolumeBgm(int);

  /// <summary>
  /// SEのボリューム変更
  /// </summary>
  /// <param name=""> ボリューム </param>
  /// <returns></returns>
  void ChangeSoundVolumeSe(int);

  /// <summary>
  /// サウンドのリスト登録
  /// </summary>
  /// <param name=""> サウンドID </param>
  /// <param name=""> サウンドの種類 </param>
  /// <returns> サウンド </returns>
  Sound* RegisterSound(SoundId, Sound::SoundType);

  /// <summary>
  /// サウンドの生成
  /// </summary>
  /// <param name=""> サウンドID </param>
  /// <returns> サウンド </returns>
  Sound* CreateSound(SoundId);

  /// <summary>
  /// サウンドの破棄
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DisposeSound();


private:

  /// <summary>
  /// サウンドリスト
  /// </summary>
  std::unordered_map<SoundId, Sound*> sound_list_;

  /// <summary>
  /// 生成したSEサウンドリスト
  /// </summary>
  std::unordered_map<int, Sound*> generate_se_sound_list_;

  /// <summary>
  /// 生成したBGMサウンドリスト
  /// </summary>
  std::unordered_map<int, Sound*> generate_bgm_sound_list_;
};