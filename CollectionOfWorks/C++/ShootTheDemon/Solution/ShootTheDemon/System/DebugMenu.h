﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "Game/GameInfo.h"

namespace {

  /// <summary>
  /// ボタンの種類
  /// </summary>
  const int kButtonType = 12;
}

/// <summary>
/// デバッグメニュー
/// </summary>
class DebugMenu : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,      // 初期化処理フェーズ
    kProcess,         // 処理中フェーズ
    kFinalize,        // 終了処理フェーズ
    kFinalized,       // 終了処理済みフェーズ
    kPhaseMaxIndex    // フェーズ数
  };

  /// <summary>
  /// ボタンの種類
  /// </summary>
  enum class ButtonType {
    kEnemyAPhase01,   // 敵キャラA フェーズ1
    kEnemyAPhase02,   // 敵キャラA フェーズ2
    kEnemyAPhase03,   // 敵キャラA フェーズ3
    kEnemyAPhase04,   // 敵キャラA フェーズ4
    kEnemyAPhase05,   // 敵キャラA フェーズ5
    kEnemyAPhase06,   // 敵キャラA フェーズ6
    kEnemyBPhase01,   // 敵キャラB フェーズ1
    kEnemyBPhase02,   // 敵キャラB フェーズ2
    kEnemyBPhase03,   // 敵キャラB フェーズ3
    kEnemyBPhase04,   // 敵キャラB フェーズ4
    kEnemyBPhase05,   // 敵キャラB フェーズ5
    kEnemyBPhase06,   // 敵キャラB フェーズ6
    kTypeMaxIndex     // 種類数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  DebugMenu();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~DebugMenu();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// ボタンの種類を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ButtonType GetButtonType() { return button_type_; }

  /// <summary>
  /// カーソル操作
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OperationCursor();

  /// <summary>
  /// カーソル操作 上移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OperationCursorUp();

  /// <summary>
  /// カーソル操作 下移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OperationCursorDown();

  /// <summary>
  /// カーソル操作 右移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OperationCursorRight();

  /// <summary>
  /// カーソル操作 左移動
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void OperationCursorLeft();

  /// <summary>
  /// 現在のフェーズを変更
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }

  /// <summary>
  /// ボタンの種類を変更
  /// </summary>
  /// <param name=""> ボタンの種類 </param>
  /// <returns></returns>
  void ChangeButtonType(ButtonType button_type) { button_type_ = button_type; }


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Initialize();

  /// <summary>
  /// 処理中
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true:処理終了, false:処理中 </returns>
  bool Process(float);


private:

  /// <summary>
  /// 表示座標
  /// </summary>
  struct Pos {
    int x;
    int y;
  };

  /// <summary>
  /// 表示座標 浮動小数
  /// </summary>
  struct Posf {
    float x;
    float y;
  };

  /// <summary>
  /// サイズ
  /// </summary>
  struct Size {
    int width;
    int height;
  };

  /// <summary>
  /// 図形
  /// </summary>
  struct Figure {
    Pos left;
    Pos right;
    Size size;
  };

  /// <summary>
  /// 図形 浮動小数
  /// </summary>
  struct Figuref {
    Posf left;
    Posf right;
  };

  /// <summary>
  /// 透過度
  /// </summary>
  struct Alpha {
    int value;
    int adjust;
    int max;
    int min;
  };

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// ボタンの種類
  /// </summary>
  ButtonType button_type_;

  /// <summary>
  /// メニュー画面 本体
  /// </summary>
  Figure screen_;

  /// <summary>
  /// メニュー画面 ボタン
  /// </summary>
  Figure button_enemy_a_phase01_;

  /// <summary>
  /// メニュー画面 ボタン
  /// </summary>
  Figure button_enemy_a_phase02_;

  /// <summary>
  /// メニュー画面 ボタン
  /// </summary>
  Figure button_enemy_a_phase03_;

  /// <summary>
  /// メニュー画面 ボタン
  /// </summary>
  Figure button_enemy_a_phase04_;

  /// <summary>
  /// メニュー画面 ボタン
  /// </summary>
  Figure button_enemy_a_phase05_;

  /// <summary>
  /// メニュー画面 ボタン
  /// </summary>
  Figure button_enemy_a_phase06_;

  /// <summary>
  /// メニュー画面 ボタン
  /// </summary>
  Figure button_enemy_b_phase01_;

  /// <summary>
  /// メニュー画面 ボタン
  /// </summary>
  Figure button_enemy_b_phase02_;

  /// <summary>
  /// メニュー画面 ボタン
  /// </summary>
  Figure button_enemy_b_phase03_;

  /// <summary>
  /// メニュー画面 ボタン
  /// </summary>
  Figure button_enemy_b_phase04_;

  /// <summary>
  /// メニュー画面 ボタン
  /// </summary>
  Figure button_enemy_b_phase05_;

  /// <summary>
  /// メニュー画面 ボタン
  /// </summary>
  Figure button_enemy_b_phase06_;

  /// <summary>
  /// メニュー画面 カーソル
  /// </summary>
  Figuref cursor_;

  /// <summary>
  /// メニュー画面 カーソル調整値
  /// </summary>
  Posf cursor_adjust_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_enemy_a_phase01_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_enemy_a_phase02_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_enemy_a_phase03_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_enemy_a_phase04_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_enemy_a_phase05_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_enemy_a_phase06_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_enemy_b_phase01_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_enemy_b_phase02_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_enemy_b_phase03_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_enemy_b_phase04_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_enemy_b_phase05_;

  /// <summary>
  /// メニュー画面 ボタン文字表示位置
  /// </summary>
  Pos text_enemy_b_phase06_;

  /// <summary>
  /// ボタンの色
  /// </summary>
  int button_color_[kButtonType];

  /// <summary>
  /// 累積時間
  /// </summary>
  float accumulate_time_;
};