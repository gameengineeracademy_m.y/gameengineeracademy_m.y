﻿#include "System/MenuController.h"

namespace {

  /// <summary>
  /// 押されている
  /// </summary>
  const int kPushing = 1;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> メニューコントローライベントインターフェース </param>
/// <returns></returns>
MenuController::MenuController(MenuControllerEventInterface& menu_controller_event_interface)
  : Task(TaskId::kMenuController)
  , menu_controller_event_interface_(menu_controller_event_interface)
  , push_key_{ false, false, false, false, false, false, false, false, false } {

  // コンソールに出力
  std::cout << "MenuController コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
MenuController::~MenuController() {

  // コンソールに出力
  std::cout << "~MenuController デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void MenuController::Update(float process_time) {

  bool is_enter = GetPushEnterKey();
  bool is_escape = GetPushEscapeKey();
  bool is_z = GetPushZ();
  bool is_x = GetPushX();
  bool is_q = GetPushQ();
  bool is_up = GetPushUp();
  bool is_down = GetPushDown();
  bool is_right = GetPushRight();
  bool is_left = GetPushLeft();

  if (is_enter) {
    // Enterキー押下処理イベント実行
    menu_controller_event_interface_.OnPushMenuControllerEnterKey();
  }
  else if (is_escape) {
    // Escapeキー押下処理イベント実行
    menu_controller_event_interface_.OnPushMenuControllerEscapeKey();
  }

  if (is_z) {
    // Zキー押下処理イベント実行
    menu_controller_event_interface_.OnPushMenuControllerZ();
  }
  else if (is_x) {
    // Xキー押下処理イベント実行
    menu_controller_event_interface_.OnPushMenuControllerX();
  }
  else if (is_q) {
    // Qキー押下処理イベント実行
    menu_controller_event_interface_.OnPushMenuControllerQ();
  }

  if (is_up) {
    // 上キー押下処理イベント実行
    menu_controller_event_interface_.OnPushMenuControllerUp();
  }
  else if (is_down) {
    // 下キー押下処理イベント実行
    menu_controller_event_interface_.OnPushMenuControllerDown();
  }

  if (is_right) {
    // 右キー押下処理イベント実行
    menu_controller_event_interface_.OnPushMenuControllerRight();
  }
  else if (is_left) {
    // 左キー押下処理イベント実行
    menu_controller_event_interface_.OnPushMenuControllerLeft();
  }
}

/// <summary>
/// Enterキー押下処理
/// </summary>
/// <param name=""></param>
/// <returns> true：押された, false：押されていない </returns>
bool MenuController::GetPushEnterKey() {

  return CheckPushKey(KEY_INPUT_RETURN, push_key_[static_cast<int>(KeyType::kEnter)]);
}

/// <summary>
/// Esacapeキー押下処理
/// </summary>
/// <param name=""></param>
/// <returns> true：押された, false：押されていない </returns>
bool MenuController::GetPushEscapeKey() {

  return CheckPushKey(KEY_INPUT_ESCAPE, push_key_[static_cast<int>(KeyType::kEscape)]);
}

/// <summary>
/// Zキー押下処理
/// </summary>
/// <param name=""></param>
/// <returns> true：押された, false：押されていない </returns>
bool MenuController::GetPushZ() {

  return CheckPushKey(KEY_INPUT_Z, push_key_[static_cast<int>(KeyType::kZ)]);
}

/// <summary>
/// Xキー押下処理
/// </summary>
/// <param name=""></param>
/// <returns> true：押された, false：押されていない </returns>
bool MenuController::GetPushX() {

  return CheckPushKey(KEY_INPUT_X, push_key_[static_cast<int>(KeyType::kX)]);
}

/// <summary>
/// Qキー押下処理
/// </summary>
/// <param name=""></param>
/// <returns> true：押された, false：押されていない </returns>
bool MenuController::GetPushQ() {

  return CheckPushKey(KEY_INPUT_Q, push_key_[static_cast<int>(KeyType::kQ)]);
}

/// <summary>
/// 上矢印キー押下処理
/// </summary>
/// <param name=""></param>
/// <returns> true：押された, false：押されていない </returns>
bool MenuController::GetPushUp() {

  return CheckPushKey(KEY_INPUT_UP, push_key_[static_cast<int>(KeyType::kUp)]);
}

/// <summary>
/// 下矢印キー押下処理
/// </summary>
/// <param name=""></param>
/// <returns> true：押された, false：押されていない </returns>
bool MenuController::GetPushDown() {

  return CheckPushKey(KEY_INPUT_DOWN, push_key_[static_cast<int>(KeyType::kDown)]);
}

/// <summary>
/// 右矢印キー押下処理
/// </summary>
/// <param name=""></param>
/// <returns> true：押された, false：押されていない </returns>
bool MenuController::GetPushRight() {

  return CheckPushKey(KEY_INPUT_RIGHT, push_key_[static_cast<int>(KeyType::kRight)]);
}

/// <summary>
/// 左矢印キー押下処理
/// </summary>
/// <param name=""></param>
/// <returns> true：押された, false：押されていない </returns>
bool MenuController::GetPushLeft() {

  return CheckPushKey(KEY_INPUT_LEFT, push_key_[static_cast<int>(KeyType::kLeft)]);
}

/// <summary>
/// 指定のキー押下有無を取得する　※離したら押されたことにする
/// </summary>
/// <param name="key_code"> キーコード </param>
/// <param name="push_key"> キーの押下の有無 </param>
/// <returns>キーが押された：false、キーが押されていない：false</returns>
bool MenuController::CheckPushKey(int key_code, bool& push_key) {

  // 指定のボタンが押されている
  if (CheckHitKey(key_code) == kPushing) {

    // 直前まで押されていない場合は押されていることにする
    if (push_key == false) {
      push_key = true;
      // 判定を押されたとして返す
      return true;
    }
  }
  else {
    // 前回処理時にキーが押されていた場合は押されていないことにする
    if (push_key) {
      push_key = false;
    }
  }

  return false;
}

/// <summary>
/// 初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void MenuController::InitializePushKeyFlag() {

  push_key_[static_cast<int>(KeyType::kEnter)] = false;
  push_key_[static_cast<int>(KeyType::kEscape)] = true;
  push_key_[static_cast<int>(KeyType::kZ)] = false;
  push_key_[static_cast<int>(KeyType::kX)] = false;
  push_key_[static_cast<int>(KeyType::kQ)] = true;
  push_key_[static_cast<int>(KeyType::kUp)] = false;
  push_key_[static_cast<int>(KeyType::kDown)] = false;
  push_key_[static_cast<int>(KeyType::kRight)] = false;
  push_key_[static_cast<int>(KeyType::kLeft)] = false;
}