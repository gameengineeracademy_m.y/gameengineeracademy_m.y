﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "System/FontInfo.h"
#include "Game/GameInfo.h"
#include <vector>

/// <summary>
/// バックグラウンドサイド
/// </summary>
class BackGroundSide : public Task {
public:

  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,     // 初期化処理
    kStartWait,      // 起動待機
    kStartUp,        // 起動
    kProcess,        // 処理中
    kFinalizeBefore, // 終了処理前
    kFinalize,       // 終了処理
    kFinalized,      // 終了処理済み
    kPhaseMaxIndex   // フェーズ数
  };

  /// <summary>
  /// フォントサイズの種類
  /// </summary>
  enum class FontSize {
    k20,               // サイズ20
    k25,               // サイズ25
    kSizeMaxIndex      // 種類数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  BackGroundSide();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~BackGroundSide();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// グラフィックハンドルをセットする
  /// </summary>
  /// <param name=""> グラフィックハンドル </param>
  /// <returns></returns>
  void SetGraphicHandle(int handle) { graphic_handle_ = handle; }

  /// <summary>
  /// グラフィックハンドルを取得する
  /// </summary>
  /// <param name="index"> インデックス </param>
  /// <returns> グラフィックハンドル </returns>
  int GetGraphicHandle() { return graphic_handle_; }

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; };


private:

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Initialize();

  /// <summary>
  /// 透過率調整処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:継続処理中 </returns>
  bool AdjustAlphaValue();

  /// <summary>
  /// 透過率の符号を変更
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void ChangeAlphaAdjustSign();


private:

  /// <summary>
  /// 表示座標
  /// </summary>
  struct Pos {
    int x;
    int y;
  };

  /// <summary>
  /// 図形
  /// </summary>
  struct Figure {
    Pos left;
    Pos right;
  };

private:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 背景グラフィックハンドル
  /// </summary>
  int graphic_handle_;

  /// <summary>
  /// 描画 透過率
  /// </summary>
  int alpha_;

  /// <summary>
  /// 描画 透過率 開始変化値
  /// </summary>
  int alpha_adjust_;

  /// <summary>
  /// 左側画像の描画位置 X座標
  /// </summary>
  int x_pos_left_;

  /// <summary>
  /// 左側画像の描画位置 Y座標
  /// </summary>
  int y_pos_left_;

  /// <summary>
  /// 右側画像の描画位置 X座標
  /// </summary>
  int x_pos_right_;

  /// <summary>
  /// 右側画像の描画位置 Y座標
  /// </summary>
  int y_pos_right_;

  /// <summary>
  /// 画像の幅
  /// </summary>
  int image_size_width_;

  /// <summary>
  /// 画像の高さ
  /// </summary>
  int image_size_height_;

  /// <summary>
  /// 背景 左サイド
  /// </summary>
  Figure back_left_side_;

  /// <summary>
  /// 背景 右サイド
  /// </summary>
  Figure back_right_side_;

  /// <summary>
  /// フォントハンドル
  /// </summary>
  std::vector<int> font_handle_;
};