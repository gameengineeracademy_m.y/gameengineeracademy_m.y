﻿#pragma once

#include <iostream>

/// <summary>
/// サウンドセットアップイベントインターフェース
/// </summary>
class SoundSetupEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  SoundSetupEventInterface() {

    // コンソールに出力
    std::cout << "SoundSetupEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~SoundSetupEventInterface() {

    // コンソールに出力
    std::cout << "~SoundSetupEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// BGMの再生
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPlaySoundBGM() {};

  /// <summary>
  /// BGMの破棄
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnDisposeSoundBGM() {};


private:

};