﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "System/VolumeControllerEventInterface.h"

/// <summary>
/// ボリュームコントローラ
/// </summary>
class VolumeController : public Task {
public:

  /// <summary>
  /// キーの種類
  /// </summary>
  enum class KeyType {
    kRight,               // 右矢印キー
    kLeft,                // 左矢印キー
    kKeyTypeMaxIndex      // キー項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> ボリュームコントローライベントインターフェース </param>
  /// <returns></returns>
  VolumeController(VolumeControllerEventInterface&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~VolumeController();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void InitializePushKeyFlag();

  /// <summary>
  /// 左矢印キー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushLeftKey(float);

  /// <summary>
  /// 右矢印キー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushRightKey(float);


private:

  /// <summary>
  /// ボタン押下の有無を取得する
  /// </summary>
  /// <param name="key_push"> キーが押されているかどうか </param>
  /// <param name="key_push_before"> キーが押されていたかどうか </param>
  /// <param name="time_sec"> 毎フレームの処理時間 </param>
  /// <param name="push_time_sec"> キー押下継続時間 </param> 
  /// <returns> true：押された, false：押されていない </returns>
  bool CheckPushKey(bool, bool& , float, float&);


private:

  /// <summary>
  /// ボリュームコントローライベントインターフェース
  /// </summary>
  VolumeControllerEventInterface& volume_controller_event_interface_;

  /// <summary>
  /// ボタン長押し時間
  /// </summary>
  float key_press_keep_time_[static_cast<int>(KeyType::kKeyTypeMaxIndex)];

  /// <summary>
  /// キーの押下の有無
  /// </summary>
  bool push_key_[static_cast<int>(KeyType::kKeyTypeMaxIndex)];
};