﻿#pragma once

#include "System/Task.h"
#include "System/TaskManager.h"
#include "System/Level.h"
#include "Game/BootLevel.h"
#include "Game/TitleLevel.h"
#include "Game/BattleLevel.h"
#include "Game/ResultLevel.h"


/// <summary>
/// レベルチェンジャー
/// </summary>
/// <remarks>
/// レベルの状態を管理し、切り替え指示を行う
/// </remarks>
class LevelChanger : public Task {
public:

  /// <summary>
  /// フェーズの一覧
  /// </summary>
  enum class PhaseType {
    kInitialize,         // 初期化フェーズ
    kProcess,            // 処理フェーズ
    kFinalize,           // 終了フェーズ
    kFinalized,          // 終了済みフェーズ
    kPhaseTypeMaxIndex   // フェーズ数
  };

  /// <summary>
  /// レベル遷移フェーズの一覧
  /// </summary>
  enum class LevelTransitionPhase {
    kNone,                  // 何もしないフェーズ
    kCurrentLevelFinalize,  // 現在のレベル終了フェーズ
    kCurrentLevelFinalized, // 現在のレベル終了済みフェーズ
    kNextLevelCreate,       // 次のレベル生成フェーズ
    kNextLevelInitialize,   // 次のレベル初期化フェーズ
    kNextLevelInitialized,  // 次のレベル初期化終了済みフェーズ
    kSwitchFinished,        // 切り替え終了済みフェーズ
    kProcessFinish,         // レベルチェンジャー終了フェーズ
    kPhaseMaxIndex          // 遷移フェーズ数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> タスクマネージャ </param>
  /// <returns></returns>
  LevelChanger(TaskManager&);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~LevelChanger();

  /// <summary>
  /// タスクマネージャーの設定
  /// </summary>
  /// <param name="task_manager"> タスクマネージャー </param>
  /// <returns></returns>
  void SetTaskManager(TaskManager& task_manager) { task_manager_ = task_manager; }

  /// <summary>
  /// 1フレームの処理を実行する
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 1フレームの描画処理を実行する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// 終了フェーズを設定する
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void SetPhaseFinalize() { current_phase_ = PhaseType::kFinalize; }

  /// <summary>
  /// レベルチェンジャーが終了しているかの有無を取得
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:終了済み, false:終了済み以外 </returns>
  bool IsLevelChangerFinalized() { return current_phase_ == PhaseType::kFinalized; }


private:

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangePhaseType(PhaseType);

  /// <summary>
  /// 遷移フェーズを変更する
  /// </summary>
  /// <param name=""> 遷移フェーズの種類 </param>
  /// <returns></returns>
  void ChangeTransitionPhase(LevelTransitionPhase);

  /// <summary>
  /// レベル処理を生成する
  /// </summary>
  /// <param name=""> タスクID </param>
  /// <returns> レベル処理 </returns>
  Level* CreateNextLevel(TaskId);

  /// <summary>
  /// 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理継続 </returns>
  bool Initialize();

  /// <summary>
  /// 処理を行う
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理継続 </returns>
  bool Process();

  /// <summary>
  /// 終了処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:処理終了, false:処理継続 </returns>
  bool Finish();

private:

  /// <summary>
  /// タスクマネージャ
  /// </summary>
  TaskManager& task_manager_;

  /// <summary>
  /// フェーズの種類
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// レベル遷移フェーズの種類
  /// </summary>
  LevelTransitionPhase transition_phase_;

  /// <summary>
  /// 処理中のレベルのポインタ
  /// </summary>
  Level* current_level_;

  /// <summary>
  /// 次に遷移するレベルのタスクID
  /// </summary>
  TaskId next_level_task_id_;
};