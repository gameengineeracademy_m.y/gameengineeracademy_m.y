﻿#pragma once

#include "System/Character.h"
#include <iostream>

/// <summary>
/// キャラクターコントローライベントインターフェース
/// </summary>
class CharacterControllerEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  CharacterControllerEventInterface() {

    // コンソールに出力
    std::cout << "CharacterControllerEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~CharacterControllerEventInterface() {

    // コンソールに出力
    std::cout << "~CharacterControllerEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// 上矢印キー押下イベント
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <param name=""> 左Shiftキー押下有無 </param>
  /// <returns></returns>
  virtual void OnPushUpKey(Character*, bool) = 0;

  /// <summary>
  /// 下矢印キー押下イベント
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <param name=""> 左Shiftキー押下有無 </param>
  /// <returns></returns>
  virtual void OnPushDownKey(Character*, bool) = 0;

  /// <summary>
  /// 左矢印キー押下イベント
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <param name=""> 左Shiftキー押下有無 </param>
  /// <returns></returns>
  virtual void OnPushLeftKey(Character*, bool) = 0;

  /// <summary>
  /// 右矢印キー押下イベント
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <param name=""> 左Shiftキー押下有無 </param>
  /// <returns></returns>
  virtual void OnPushRightKey(Character*, bool) = 0;

  /// <summary>
  /// Zキー押下イベント
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  virtual void OnPushZKey(Character*) {};

  /// <summary>
  /// Escキー押下イベント
  /// </summary>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  virtual void OnPushEscapeKey() {};

  /// <summary>
  /// Oキー押下イベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushOKey() {};

  /// <summary>
  /// Pキー押下イベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushPKey() {};

  /// <summary>
  /// Lキー押下イベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushLKey() {};

  /// <summary>
  /// Iキー押下イベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushIKey() {};

  /// <summary>
  /// Qキー押下イベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushQKey() {};


private:

};