﻿#include "System/SoundSetup.h"

namespace {

  /// <summary>
  /// フォント名
  /// </summary>
  const char* kFontName = "Reggae One";

  /// <summary>
  /// タイトル文字フォントサイズ
  /// </summary>
  const int kTitleFontSize = 40;

  /// <summary>
  /// 文字フォントサイズ
  /// </summary>
  const int kTextFontSize = 22;

  /// <summary>
  /// 文字幅
  /// </summary>
  const int kFontWidth = 1;

  /// <summary>
  /// スクリーンサイズ 幅
  /// </summary>
  const int kScreenWidth = 400;

  /// <summary>
  /// スクリーンサイズ 高さ
  /// </summary>
  const int kScreenHeight = 300;

  /// <summary>
  /// メニューテキスト表示位置
  /// </summary>
  const int kScreenTextPosY = 115;

  /// <summary>
  /// ボタン表示位置 X座標
  /// </summary>
  const int kButtonBgmPosX = 175;

  /// <summary>
  /// ボタン表示位置 Y座標
  /// </summary>
  const int kButtonBgmPosY = 35;

  /// <summary>
  /// ボタン表示位置 Y座標 調整間隔
  /// </summary>
  const int kButtonPosYAdjust = 60;

  /// <summary>
  /// ボタンテキスト表示位置 調整値
  /// </summary>
  const int kButtonTextPosY = 8;

  /// <summary>
  /// ボリュームボタン設定範囲 始点
  /// </summary>
  const float kButtonLineMinPosX = 537.0f;

  /// <summary>
  /// ボリュームボタン設定範囲 終点
  /// </summary>
  const float kButtonLineMaxPosX = 687.0f;

  /// <summary>
  /// ボリュームボタン設定範囲 高さ
  /// </summary>
  const float kButtonLineBgmPosY = 362.0f;

  /// <summary>
  /// ボリュームボタン設定範囲 高さ
  /// </summary>
  const float kButtonLineSePosY = 422.0f;

  /// <summary>
  /// ボリュームボタン 中間線 高さ
  /// </summary>
  const float kButtonLineMidPosY = 10.0f;

  /// <summary>
  /// ボリュームボタン設定範囲 線幅
  /// </summary>
  const float kButtonLineThick = 2.0f;

  /// <summary>
  /// ボリュームボタン 半径
  /// </summary>
  const float kButtonRadiusOut = 7.0f;

  /// <summary>
  /// ボリュームボタン 半径
  /// </summary>
  const float kButtonRadiusIn = 3.0f;

  /// <summary>
  /// ボリュームボタン 頂点数
  /// </summary>
  const int kButtonPosNum = 10;

  /// <summary>
  /// ボリューム割合 表示位置 調整値
  /// </summary>
  const int kVolumePosX = 14;

  /// <summary>
  /// カーソル座標 BGM側
  /// </summary>
  const int kCursorPosX = 5;

  /// <summary>
  /// カーソル座標 BGM側
  /// </summary>
  const int kCursorPosY = 5;

  /// <summary>
  /// カーソル 幅
  /// </summary>
  const int kCursorWidth = 135;

  /// <summary>
  /// カーソル 高さ
  /// </summary>
  const int kCursorHeight = 35;

  /// <summary>
  /// 半分
  /// </summary>
  const int kHalfValue = 2;

  /// <summary>
  /// 半分
  /// </summary>
  const float kHalfValuef = 2.0f;

  /// <summary>
  /// メニュー画面 背景透過率
  /// </summary>
  const int kAlphaScreen = 210;

  /// <summary>
  /// メニュー画面 カーソル透過率
  /// </summary>
  const int kAlphaCursorMax = 150;

  /// <summary>
  /// メニュー画面 カーソル透過率
  /// </summary>
  const int kAlphaCursorMin = 80;

  /// <summary>
  /// メニュー画面 カーソル透過率 調整値
  /// </summary>
  const int kAlphaCursorPlusAdjust = 5;

  /// <summary>
  /// メニュー画面 カーソル透過率 調整値
  /// </summary>
  const int kAlphaCursorMinusAdjust = -5;

  /// <summary>
  /// 透過率 最大値
  /// </summary>
  const int kAlphaMax = 255;

  /// <summary>
  /// 透過率 背景最大値
  /// </summary>
  const int kAlphaBackMax = 210;

  /// <summary>
  /// 透過率 最小値
  /// </summary>
  const int kAlphaMin = 0;

  /// <summary>
  /// メニュー画面 背景色
  /// </summary>
  const int kScreenColor = GetColor(200, 0, 255);

  /// <summary>
  /// メニュー画面 メニューテキスト文字色
  /// </summary>
  const int kScreenTextForeColor = GetColor(255, 255, 0);

  /// <summary>
  /// メニュー画面 ボタンテキスト文字色
  /// </summary>
  const int kButtonTextForeColor = GetColor(255, 255, 255);

  /// <summary>
  /// メニュー画面 ボタンライン色
  /// </summary>
  const int kButtonLineColor = GetColor(255, 255, 255);

  /// <summary>
  /// メニュー画面 ボタン色 選択中
  /// </summary>
  const int kButtonSelectColor = GetColor(190, 90, 255);

  /// <summary>
  /// メニュー画面 ボタン色 選択外
  /// </summary>
  const int kButtonNoneColor = GetColor(100, 0, 200);

  /// <summary>
  /// メニュー画面 ボタン色 外側
  /// </summary>
  const int kButtonColorOut = GetColor(0, 210, 0);

  /// <summary>
  /// メニュー画面 ボタン色 内側
  /// </summary>
  const int kButtonColorIn = GetColor(255, 255, 255);

  /// <summary>
  /// メニュー画面 カーソル
  /// </summary>
  const int kCursorColorWhite = GetColor(255, 255, 255);

  /// <summary>
  /// メニュー画面 カーソル
  /// </summary>
  const int kCursorColorGreen = GetColor(0, 255, 0);

  /// <summary>
  /// 背景 色
  /// </summary>
  const int kBackColor = GetColor(0, 0, 0);

  /// <summary>
  /// 決定・戻るボタン配置 左上X座標
  /// </summary>
  const int kButtonPosX = 140;

  /// <summary>
  /// 決定・戻るボタン配置 左上Y座標
  /// </summary>
  const int kButtonPosY = 85;

  /// <summary>
  /// ボタンサイズ 幅
  /// </summary>
  const int kButtonWidth = 120;

  /// <summary>
  /// ボタンサイズ 高さ
  /// </summary>
  const int kButtonHeight = 40;

  /// <summary>
  /// メニューテキスト
  /// </summary>
  const char* kScreenText = "SOUND SETTING";

  /// <summary>
  /// テキスト
  /// </summary>
  const char* kButtonTextBgm = "BGM VOL.";

  /// <summary>
  /// テキスト
  /// </summary>
  const char* kButtonTextSe = "SE VOL.";

  /// <summary>
  /// テキスト
  /// </summary>
  const char* kVolumeTextRate = "%";

  /// <summary>
  /// テキスト
  /// </summary>
  const char* kButtonTextDone = "DONE";

  /// <summary>
  /// テキスト
  /// </summary>
  const char* kButtonTextReturn = "RETURN";

  /// <summary>
  /// ボリューム 最大値
  /// </summary>
  const float kVolumeMax = 255.0f;

  /// <summary>
  /// ボリューム 最小値
  /// </summary>
  const float kVolumeMin = 0.0f;

  /// <summary>
  /// ボリューム 1回当たりの調整値
  /// </summary>
  const float kVolumeAdjust = 2.55f;

  /// <summary>
  /// 百分率
  /// </summary>
  const float kHundred = 100.0f;

  /// <summary>
  /// ボリューム ライン上の1メモリ
  /// </summary>
  const float kLineAdjust = 1.5f;

  /// <summary>
  /// カーソル調整値 X方向
  /// </summary>
  const float kCursorPlusAdjustX = 1.0f;

  /// <summary>
  /// カーソル調整値 Y方向
  /// </summary>
  const float kCursorPlusAdjustY = 1.0f;

  /// <summary>
  /// カーソル調整値 X方向
  /// </summary>
  const float kCursorMinusAdjustX = -1.0f;

  /// <summary>
  /// カーソル調整値 Y方向
  /// </summary>
  const float kCursorMinusAdjustY = -1.0f;

  /// <summary>
  /// カーソル調整値 固定値
  /// </summary>
  const float kCursorAdjust = 1.0f;

  /// <summary>
  /// カーソル 幅
  /// </summary>
  const float kCursorThick = 3.0f;

  /// <summary>
  /// カーソル透過率調整 待機時間
  /// </summary>
  const float kAlphaAdjustWaitTime = 0.05f;

  /// <summary>
  /// カーソルサイズ調整 待機時間
  /// </summary>
  const float kSizeAdjustWaitTime = 0.7f;

  /// <summary>
  /// BGMリストIDの初期値
  /// </summary>
  const int kBgmListIdInit = -1;

  /// <summary>
  /// BGM再生開始 待機時間
  /// </summary>
  const float kBgmPlayWaitTime = 1.0f;

  /// <summary>
  /// リセット時間
  /// </summary>
  const float kResetTime = 0.0f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name="event_interface"> サウンドセットアップイベントインターフェース </param>
/// <returns></returns>
SoundSetup::SoundSetup(SoundSetupEventInterface& event_interface)
  : Task(TaskId::kSoundSetup)
  , current_phase_(PhaseType::kInitialize)
  , button_type_(ButtonType::kBackGroundMusic)
  , sound_setup_event_interface_(event_interface)
  , screen_()
  , button_bgm_()
  , button_se_()
  , text_screen_()
  , text_button_bgm_()
  , text_button_se_()
  , text_volume_bgm_()
  , text_volume_se_()
  , text_button_done_()
  , text_button_return_()
  , line_bgm_()
  , line_se_()
  , line_bgm_start_()
  , line_se_start_()
  , line_bgm_end_()
  , line_se_end_()
  , line_bgm_mid_()
  , line_se_mid_()
  , button_done_()
  , button_return_()
  , cursor_()
  , back_board_()
  , cursor_adjust_()
  , alpha_cursor_(0)
  , alpha_cursor_adjust_(0)
  , volume_bgm_rate_(0)
  , volume_se_rate_(0)
  , volume_bgm_(0.0f)
  , volume_se_(0.0f)
  , accumulate_time_(0.0f)
  , accumulate_time_bgm_(0.0f)
  , list_id_bgm_(kBgmListIdInit)
  , button_color_()
  , font_handle_() {

  // コンソールに出力
  std::cout << "SoundSetup コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
SoundSetup::~SoundSetup() {

  // コンソールに出力
  std::cout << "~SoundSetup デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void SoundSetup::Update(float process_time) {

  switch (current_phase_) {
  case PhaseType::kInitialize: {
    //------------------------------
    //初期化処理フェーズ
    //------------------------------
    bool is_finish = Initialize();
    if (is_finish) {
      // 現在のフェーズを「処理中」に変更
      ChangeCurrentPhase(PhaseType::kProcess);
    }
    break;
  }
  case PhaseType::kProcess: {
    //------------------------------
    //処理中フェーズ
    //------------------------------
    Process(process_time);
    break;
  }
  case PhaseType::kFinalize: {
    //------------------------------
    //終了処理フェーズ
    //------------------------------
    // 現在のフェーズを「終了処理済み」に変更
    ChangeCurrentPhase(PhaseType::kFinalized);
    break;
  }
  }
}

/// <summary>
/// 描画処理(毎フレーム)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void SoundSetup::Render() {

  //-------------------------------------------
  // 背景
  //-------------------------------------------
  // 描画ブレンドモードをアルファブレンドに設定
  SetDrawBlendMode(DX_BLENDMODE_ALPHA, kAlphaBackMax);
  DrawBox(back_board_.left.x, back_board_.left.y,
          back_board_.right.x, back_board_.right.y, kBackColor, true);
  // 描画ブレンドモードをノーブレンドにする
  SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);


  switch (current_phase_) {
  case PhaseType::kProcess: {

    //-------------------------------------------
    // スクリーン
    //-------------------------------------------
    // 描画ブレンドモードをアルファブレンドに設定
    SetDrawBlendMode(DX_BLENDMODE_ALPHA, kAlphaScreen);
    DrawBox(screen_.left.x, screen_.left.y, screen_.right.x, screen_.right.y, kScreenColor, true);
    // 描画ブレンドモードをノーブレンドにする
    SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);


    //-------------------------------------------
    // テキスト
    //-------------------------------------------
    DrawStringToHandle(text_screen_.x, text_screen_.y, kScreenText, kScreenTextForeColor, font_handle_.at(static_cast<int>(FontSize::k40)));

    DrawStringToHandle(text_button_bgm_.x, text_button_bgm_.y, kButtonTextBgm, kButtonTextForeColor, font_handle_.at(static_cast<int>(FontSize::k22)));
    DrawStringToHandle(text_button_se_.x, text_button_se_.y, kButtonTextSe, kButtonTextForeColor, font_handle_.at(static_cast<int>(FontSize::k22)));

    std::string volume = std::to_string(volume_bgm_rate_) + kVolumeTextRate;
    DrawStringToHandle(text_volume_bgm_.x, text_volume_bgm_.y, volume.c_str(), kButtonTextForeColor, font_handle_.at(static_cast<int>(FontSize::k22)));
    volume = std::to_string(volume_se_rate_) + kVolumeTextRate;
    DrawStringToHandle(text_volume_se_.x, text_volume_se_.y, volume.c_str(), kButtonTextForeColor, font_handle_.at(static_cast<int>(FontSize::k22)));


    //-------------------------------------------
    // ボリューム設定ボタン
    //-------------------------------------------
    // ボリュームライン
    DrawLineAA(line_bgm_.left.x, line_bgm_.left.y, line_bgm_.right.x, line_bgm_.right.y,
               kButtonLineColor, kButtonLineThick);
    DrawLineAA(line_se_.left.x, line_se_.left.y, line_se_.right.x, line_se_.right.y,
               kButtonLineColor, kButtonLineThick);

    DrawLineAA(line_bgm_start_.left.x, line_bgm_start_.left.y, line_bgm_start_.right.x, line_bgm_start_.right.y,
               kButtonLineColor, kButtonLineThick);
    DrawLineAA(line_se_start_.left.x, line_se_start_.left.y, line_se_start_.right.x, line_se_start_.right.y,
               kButtonLineColor, kButtonLineThick);

    DrawLineAA(line_bgm_mid_.left.x, line_bgm_mid_.left.y, line_bgm_mid_.right.x, line_bgm_mid_.right.y,
               kButtonLineColor, kButtonLineThick);
    DrawLineAA(line_se_mid_.left.x, line_se_mid_.left.y, line_se_mid_.right.x, line_se_mid_.right.y,
               kButtonLineColor, kButtonLineThick);

    DrawLineAA(line_bgm_end_.left.x, line_bgm_end_.left.y, line_bgm_end_.right.x, line_bgm_end_.right.y,
               kButtonLineColor, kButtonLineThick);
    DrawLineAA(line_se_end_.left.x, line_se_end_.left.y, line_se_end_.right.x, line_se_end_.right.y,
               kButtonLineColor, kButtonLineThick);

    // ボリュームボタン
    DrawCircleAA(button_bgm_.center.x, button_bgm_.center.y, button_bgm_.radius, kButtonPosNum,
                 kButtonColorOut, true);
    DrawCircleAA(button_se_.center.x, button_se_.center.y, button_se_.radius, kButtonPosNum,
                 kButtonColorOut, true);
    DrawCircleAA(button_bgm_.center.x, button_bgm_.center.y, kButtonRadiusIn, kButtonPosNum,
                 kButtonColorIn, true);
    DrawCircleAA(button_se_.center.x, button_se_.center.y, kButtonRadiusIn, kButtonPosNum,
                 kButtonColorIn, true);


    //-------------------------------------------
    // 保存・キャンセルボタン
    //-------------------------------------------
    DrawBox(button_done_.left.x, button_done_.left.y, button_done_.right.x, button_done_.right.y,
            button_color_[ButtonType::kDone], true);
    DrawBox(button_return_.left.x, button_return_.left.y, button_return_.right.x, button_return_.right.y,
            button_color_[ButtonType::kReturn], true);

    DrawStringToHandle(text_button_done_.x, text_button_done_.y, kButtonTextDone, kButtonTextForeColor, font_handle_.at(static_cast<int>(FontSize::k22)));
    DrawStringToHandle(text_button_return_.x, text_button_return_.y, kButtonTextReturn, kButtonTextForeColor, font_handle_.at(static_cast<int>(FontSize::k22)));


    //-------------------------------------------
    // カーソル
    //-------------------------------------------
    switch (button_type_) {
    case ButtonType::kBackGroundMusic:
    case ButtonType::kSoundEffect: {
      // 描画ブレンドモードをアルファブレンドに設定
      SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha_cursor_);
      DrawBoxAA(cursor_.left.x, cursor_.left.y, cursor_.right.x, cursor_.right.y, kCursorColorWhite, true);
      // 描画ブレンドモードをノーブレンドにする
      SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
      break;
    }
    case ButtonType::kDone:
    case ButtonType::kReturn: {
      // 描画ブレンドモードをアルファブレンドに設定
      SetDrawBlendMode(DX_BLENDMODE_ALPHA, kAlphaMax);

      // 左
      DrawLineAA(cursor_.left.x, cursor_.left.y, cursor_.left.x,
                 cursor_.right.y, kCursorColorGreen, kCursorThick);
      // 右
      DrawLineAA(cursor_.right.x - kCursorAdjust, cursor_.left.y,
                 cursor_.right.x - kCursorAdjust, cursor_.right.y, kCursorColorGreen, kCursorThick);
      // 上
      DrawLineAA(cursor_.left.x - kCursorAdjust, cursor_.left.y,
                 cursor_.right.x + kCursorAdjust, cursor_.left.y, kCursorColorGreen, kCursorThick);
      // 下
      DrawLineAA(cursor_.left.x - kCursorAdjust, cursor_.right.y,
                 cursor_.right.x + kCursorAdjust, cursor_.right.y, kCursorColorGreen, kCursorThick);

      // 描画ブレンドモードをノーブレンドにする
      SetDrawBlendMode(DX_BLENDMODE_NOBLEND, kAlphaMin);
      break;
    }
    }
    break;
  }
  }
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns> true:終了処理, false:処理中 </returns>
bool SoundSetup::Initialize() {

  // ゲーム情報から画面の中心位置を取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return false;
  }
  int x_pos_center = game_info->GetCenterPointX();
  int y_pos_center = game_info->GetCenterPointY();

  // フォント情報生成
  FontInfo* font_info = FontInfo::GetFontInfoInstance();
  if (font_info == nullptr) {
    return false;
  }
  int font_handle = font_info->GetFontInfo(kFontName, kTextFontSize, kFontWidth, DX_FONTTYPE_NORMAL);
  font_handle_.push_back(font_handle);
  font_handle = font_info->GetFontInfo(kFontName, kTitleFontSize, kFontWidth, DX_FONTTYPE_NORMAL);
  font_handle_.push_back(font_handle);

  //---------------------------------------------------
  // サウンドメニュー画面 本体
  //---------------------------------------------------
  screen_.left.x = x_pos_center - (kScreenWidth / kHalfValue);
  screen_.left.y = y_pos_center - (kScreenHeight / kHalfValue);
  screen_.right.x = screen_.left.x + kScreenWidth;
  screen_.right.y = screen_.left.y + kScreenHeight;

  //---------------------------------------------------
  // ボリューム関連
  //---------------------------------------------------
  line_bgm_.left.x = kButtonLineMinPosX;
  line_bgm_.left.y = kButtonLineBgmPosY;
  line_bgm_.right.x = kButtonLineMaxPosX;
  line_bgm_.right.y = kButtonLineBgmPosY;

  line_se_.left.x = kButtonLineMinPosX;
  line_se_.left.y = kButtonLineSePosY;
  line_se_.right.x = kButtonLineMaxPosX;
  line_se_.right.y = kButtonLineSePosY;

  line_bgm_start_.left.x = kButtonLineMinPosX;
  line_bgm_start_.left.y = line_bgm_.left.y - kButtonLineMidPosY;
  line_bgm_start_.right.x = kButtonLineMinPosX;
  line_bgm_start_.right.y = line_bgm_.left.y + kButtonLineMidPosY;

  line_se_start_.left.x = kButtonLineMinPosX;
  line_se_start_.left.y = line_se_.left.y - kButtonLineMidPosY;
  line_se_start_.right.x = kButtonLineMinPosX;
  line_se_start_.right.y = line_se_.left.y + kButtonLineMidPosY;

  line_bgm_mid_.left.x = (kButtonLineMinPosX + kButtonLineMaxPosX) / kHalfValuef;
  line_bgm_mid_.left.y = line_bgm_.left.y - kButtonLineMidPosY;
  line_bgm_mid_.right.x = (kButtonLineMinPosX + kButtonLineMaxPosX) / kHalfValuef;
  line_bgm_mid_.right.y = line_bgm_.left.y + kButtonLineMidPosY;

  line_se_mid_.left.x = (kButtonLineMinPosX + kButtonLineMaxPosX) / kHalfValuef;
  line_se_mid_.left.y = line_se_.left.y - kButtonLineMidPosY;
  line_se_mid_.right.x = (kButtonLineMinPosX + kButtonLineMaxPosX) / kHalfValuef;
  line_se_mid_.right.y = line_se_.left.y + kButtonLineMidPosY;

  line_bgm_end_.left.x = kButtonLineMaxPosX;
  line_bgm_end_.left.y = line_bgm_.left.y - kButtonLineMidPosY;
  line_bgm_end_.right.x = kButtonLineMaxPosX;
  line_bgm_end_.right.y = line_bgm_.left.y + kButtonLineMidPosY;

  line_se_end_.left.x = kButtonLineMaxPosX;
  line_se_end_.left.y = line_se_.left.y - kButtonLineMidPosY;
  line_se_end_.right.x = kButtonLineMaxPosX;
  line_se_end_.right.y = line_se_.left.y + kButtonLineMidPosY;

  button_bgm_.center.x = kButtonLineMaxPosX;
  button_bgm_.center.y = kButtonLineBgmPosY;
  button_bgm_.radius = kButtonRadiusOut;

  button_se_.center.x = kButtonLineMaxPosX;
  button_se_.center.y = kButtonLineSePosY;
  button_se_.radius = kButtonRadiusOut;

  //---------------------------------------------------
  // ボタン位置
  //---------------------------------------------------
  // 百分率に変換
  volume_bgm_rate_ = static_cast<int>(volume_bgm_ / kVolumeMax * kHundred);
  volume_se_rate_ = static_cast<int>(volume_se_ / kVolumeMax * kHundred);
  // ボタンの座標を求める
  button_bgm_.center.x = kButtonLineMinPosX + static_cast<float>(volume_bgm_rate_) * kLineAdjust;
  button_se_.center.x = kButtonLineMinPosX + static_cast<float>(volume_se_rate_) * kLineAdjust;

  //---------------------------------------------------
  // サウンドメニュー画面 メニュー・ボタンテキスト
  //---------------------------------------------------
  // テキスト
  int text_width = GetDrawStringWidthToHandle(kScreenText, static_cast<int>(strlen(kScreenText)),font_handle_.at(static_cast<int>(FontSize::k40)));
  text_screen_.x = x_pos_center - (text_width / kHalfValue);
  text_screen_.y = y_pos_center - kScreenTextPosY;

  // ボタンテキスト
  text_width = GetDrawStringWidthToHandle(kButtonTextBgm, static_cast<int>(strlen(kButtonTextBgm)), font_handle_.at(static_cast<int>(FontSize::k22)));
  text_button_bgm_.x = x_pos_center - kButtonBgmPosX;
  text_button_bgm_.y = y_pos_center - kButtonBgmPosY;
  text_width = GetDrawStringWidthToHandle(kButtonTextSe, static_cast<int>(strlen(kButtonTextSe)), font_handle_.at(static_cast<int>(FontSize::k22)));
  text_button_se_.x = x_pos_center - kButtonBgmPosX;
  text_button_se_.y = text_button_bgm_.y + kButtonPosYAdjust;
  // ボリューム ％表示
  std::string volume = std::to_string(volume_bgm_rate_) + kVolumeTextRate;
  text_width = GetDrawStringWidthToHandle(volume.c_str(), static_cast<int>(strlen(volume.c_str())), font_handle_.at(static_cast<int>(FontSize::k22)));
  text_volume_bgm_.x = x_pos_center - text_width + kVolumePosX;
  text_volume_bgm_.y = text_button_bgm_.y;
  volume = std::to_string(volume_se_rate_) + kVolumeTextRate;
  text_width = GetDrawStringWidthToHandle(volume.c_str(), static_cast<int>(strlen(volume.c_str())), font_handle_.at(static_cast<int>(FontSize::k22)));
  text_volume_se_.x = x_pos_center - text_width + kVolumePosX;
  text_volume_se_.y = text_button_se_.y;

  //---------------------------------------------------
  // 決定・戻るボタン
  //---------------------------------------------------
  button_done_.left.x = x_pos_center - kButtonPosX;
  button_done_.left.y = y_pos_center + kButtonPosY;
  button_done_.right.x = button_done_.left.x + kButtonWidth;
  button_done_.right.y = button_done_.left.y + kButtonHeight;

  button_return_.right.x = x_pos_center + kButtonPosX;
  button_return_.left.y = y_pos_center + kButtonPosY;
  button_return_.left.x = button_return_.right.x - kButtonWidth;
  button_return_.right.y = button_return_.left.y + kButtonHeight;

  text_width = GetDrawStringWidthToHandle(kButtonTextDone, static_cast<int>(strlen(kButtonTextDone)), font_handle_.at(static_cast<int>(FontSize::k22)));
  text_button_done_.x = (button_done_.left.x + button_done_.right.x) / kHalfValue - text_width / kHalfValue;
  text_button_done_.y = button_done_.left.y + kButtonTextPosY;
  text_width = GetDrawStringWidthToHandle(kButtonTextReturn, static_cast<int>(strlen(kButtonTextReturn)), font_handle_.at(static_cast<int>(FontSize::k22)));
  text_button_return_.x = (button_return_.left.x + button_return_.right.x) / kHalfValue - text_width / kHalfValue;;
  text_button_return_.y = button_return_.left.y + kButtonTextPosY;

  //---------------------------------------------------
  // カーソル
  //---------------------------------------------------
  cursor_.left.x = static_cast<float>(text_button_bgm_.x - kCursorPosX);
  cursor_.left.y = static_cast<float>(text_button_bgm_.y - kCursorPosY);
  cursor_.right.x = static_cast<float>(cursor_.left.x + kCursorWidth);
  cursor_.right.y = static_cast<float>(cursor_.left.y + kCursorHeight);

  cursor_adjust_.x = kCursorMinusAdjustX;
  cursor_adjust_.y = kCursorMinusAdjustY;


  alpha_cursor_ = kAlphaCursorMin;
  alpha_cursor_adjust_ = kAlphaCursorPlusAdjust;

  // ボタンの種類
  button_type_ = ButtonType::kBackGroundMusic;

  button_color_[ButtonType::kDone] = kButtonNoneColor;
  button_color_[ButtonType::kReturn] = kButtonNoneColor;

  // リストID
  list_id_bgm_ = kBgmListIdInit;

  return true;
}

/// <summary>
/// 処理中
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void SoundSetup::Process(float process_time) {

  switch (button_type_) {
  case ButtonType::kBackGroundMusic: {

    if (list_id_bgm_ != kBgmListIdInit) {
      break;
    }

    accumulate_time_bgm_ += process_time;
    if (accumulate_time_bgm_ >= kBgmPlayWaitTime) {
      // 累積時間をリセット
      accumulate_time_bgm_ = kResetTime;
      // BGMを自動再生
      sound_setup_event_interface_.OnPlaySoundBGM();
    }
    break;
  }
  default: {

    if (list_id_bgm_ == kBgmListIdInit) {
      break;
    }

    // 累積時間をリセット
    accumulate_time_bgm_ = kResetTime;
    // サウンドリストから指定のBGMを降ろす
    sound_setup_event_interface_.OnDisposeSoundBGM();

    // リストIDの初期化
    list_id_bgm_ = kBgmListIdInit;
    break;
  }
  }

  switch (button_type_) {
  case ButtonType::kBackGroundMusic:
  case ButtonType::kSoundEffect: {

    // 累積時間に加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kAlphaAdjustWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;

      alpha_cursor_ += alpha_cursor_adjust_;

      if (alpha_cursor_ >= kAlphaCursorMax) {
        alpha_cursor_ = kAlphaCursorMax;
        alpha_cursor_adjust_ = kAlphaCursorMinusAdjust;
      }
      else if (alpha_cursor_ <= kAlphaCursorMin) {
        alpha_cursor_ = kAlphaCursorMin;
        alpha_cursor_adjust_ = kAlphaCursorPlusAdjust;
      }
    }
    break;
  }
  case ButtonType::kDone:
  case ButtonType::kReturn: {

    // 累積時間に加算
    accumulate_time_ += process_time;
    if (accumulate_time_ >= kSizeAdjustWaitTime) {
      // 累積時間をリセット
      accumulate_time_ = kResetTime;

      cursor_.left.x -= cursor_adjust_.x;
      cursor_.left.y -= cursor_adjust_.y;
      cursor_.right.x += cursor_adjust_.x;
      cursor_.right.y += cursor_adjust_.y;

      if (cursor_adjust_.x == kCursorPlusAdjustX) {
        cursor_adjust_.x = kCursorMinusAdjustX;
        cursor_adjust_.y = kCursorMinusAdjustY;
      }
      else {
        cursor_adjust_.x = kCursorPlusAdjustX;
        cursor_adjust_.y = kCursorPlusAdjustY;
      }
    }
    break;
  }
  }
}

/// <summary>
/// カーソル操作 上移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void SoundSetup::OperationCursorUp() {

  switch (button_type_) {
  case ButtonType::kBackGroundMusic: {
    button_type_ = ButtonType::kReturn;
    break;
  }
  case ButtonType::kSoundEffect: {
    button_type_ = ButtonType::kBackGroundMusic;
    break;
  }
  case ButtonType::kDone: {
    button_type_ = ButtonType::kSoundEffect;
    break;
  }
  case ButtonType::kReturn: {
    button_type_ = ButtonType::kDone;
    break;
  }
  }

  // カーソルを移動する
  OperationCursor();
}

/// <summary>
/// カーソル操作 下移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void SoundSetup::OperationCursorDown() {

  switch (button_type_) {
  case ButtonType::kBackGroundMusic: {
    button_type_ = ButtonType::kSoundEffect;
    break;
  }
  case ButtonType::kSoundEffect: {
    button_type_ = ButtonType::kDone;
    break;
  }
  case ButtonType::kDone: {
    button_type_ = ButtonType::kReturn;
    break;
  }
  case ButtonType::kReturn: {
    button_type_ = ButtonType::kBackGroundMusic;
    break;
  }
  }

  // カーソルを移動する
  OperationCursor();
}

/// <summary>
/// カーソル操作 右移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void SoundSetup::OperationCursorRight() {

  switch (button_type_) {
  case ButtonType::kBackGroundMusic: {
    //--------------------------------
    //BGM設定ボタン
    //--------------------------------
    // ボリュームを増加
    volume_bgm_ += kVolumeAdjust;
    if (volume_bgm_ >= kVolumeMax) {
      volume_bgm_ = kVolumeMax;
    }

    // 百分率に変換
    volume_bgm_rate_ = static_cast<int>(volume_bgm_ / kVolumeMax * kHundred);
    // ボタンの座標を求める
    button_bgm_.center.x = kButtonLineMinPosX + static_cast<float>(volume_bgm_rate_) * kLineAdjust;

    // ボリューム位置を設定
    CalcDispVolumeText(volume_bgm_rate_, text_volume_bgm_.x, text_volume_bgm_.y);

    break;
  }
  case ButtonType::kSoundEffect: {
    //--------------------------------
    //SE設定ボタン
    //--------------------------------
    // ボリュームを増加
    volume_se_ += kVolumeAdjust;
    if (volume_se_ >= kVolumeMax) {
      volume_se_ = kVolumeMax;
    }

    // 百分率に変換
    volume_se_rate_ = static_cast<int>(volume_se_ / kVolumeMax * kHundred);
    // ボタンの座標を求める
    button_se_.center.x = kButtonLineMinPosX + static_cast<float>(volume_se_rate_) * kLineAdjust;

    // ボリューム位置を設定
    CalcDispVolumeText(volume_se_rate_, text_volume_se_.x, text_volume_se_.y);

    break;
  }
  case ButtonType::kDone: {
    //--------------------------------
    //決定ボタン
    //--------------------------------
    // 戻るボタンに設定
    button_type_ = ButtonType::kReturn;
    // カーソルの移動
    OperationCursor();
    break;
  }
  case ButtonType::kReturn: {
    //--------------------------------
    //戻るボタン
    //--------------------------------
    // 決定ボタンに設定
    button_type_ = ButtonType::kDone;
    // カーソルの移動
    OperationCursor();
    break;
  }
  }
}

/// <summary>
/// カーソル操作 左移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void SoundSetup::OperationCursorLeft() {

  switch (button_type_) {
  case ButtonType::kBackGroundMusic: {
    //--------------------------------
    //BGM設定ボタン
    //--------------------------------
    // ボリュームを減少
    volume_bgm_ -= kVolumeAdjust;
    if (volume_bgm_ <= kVolumeMin) {
      volume_bgm_ = kVolumeMin;
    }

    // 百分率に変換
    volume_bgm_rate_ = static_cast<int>(volume_bgm_ / kVolumeMax * kHundred);
    // ボタンの座標を求める
    button_bgm_.center.x = kButtonLineMinPosX + static_cast<float>(volume_bgm_rate_) * kLineAdjust;

    // ボリューム位置を設定
    CalcDispVolumeText(volume_bgm_rate_, text_volume_bgm_.x, text_volume_bgm_.y);

    break;
  }
  case ButtonType::kSoundEffect: {
    //--------------------------------
    //SE設定ボタン
    //--------------------------------
    // ボリュームを減少
    volume_se_ -= kVolumeAdjust;
    if (volume_se_ <= kVolumeMin) {
      volume_se_ = kVolumeMin;
    }

    // 百分率に変換
    volume_se_rate_ = static_cast<int>(volume_se_ / kVolumeMax * kHundred);
    // ボタンの座標を求める
    button_se_.center.x = kButtonLineMinPosX + static_cast<float>(volume_se_rate_) * kLineAdjust;

    // ボリューム位置を設定
    CalcDispVolumeText(volume_se_rate_, text_volume_se_.x, text_volume_se_.y);

    break;
  }
  case ButtonType::kDone: {
    //--------------------------------
    //決定ボタン
    //--------------------------------
    // 戻るボタンに設定
    button_type_ = ButtonType::kReturn;
    // カーソルの移動
    OperationCursor();
    break;
  }
  case ButtonType::kReturn: {
    //--------------------------------
    //戻るボタン
    //--------------------------------
    // 決定ボタンに設定
    button_type_ = ButtonType::kDone;
    // カーソルの移動
    OperationCursor();
    break;
  }
  }
}

/// <summary>
/// カーソル操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void SoundSetup::OperationCursor() {

  float x_pos_left = 0.0f;
  float y_pos_left = 0.0f;
  float x_pos_right = 0.0f;
  float y_pos_right = 0.0f;

  switch (button_type_) {
  case ButtonType::kBackGroundMusic: {
    x_pos_left = static_cast<float>(text_button_bgm_.x - kCursorPosX);
    y_pos_left = static_cast<float>(text_button_bgm_.y - kCursorPosY);
    x_pos_right = static_cast<float>(x_pos_left + kCursorWidth);
    y_pos_right = static_cast<float>(y_pos_left + kCursorHeight);

    button_color_[ButtonType::kDone] = kButtonNoneColor;
    button_color_[ButtonType::kReturn] = kButtonNoneColor;
    break;
  }
  case ButtonType::kSoundEffect: {
    x_pos_left = static_cast<float>(text_button_se_.x - kCursorPosX);
    y_pos_left = static_cast<float>(text_button_se_.y - kCursorPosY);
    x_pos_right = static_cast<float>(x_pos_left + kCursorWidth);
    y_pos_right = static_cast<float>(y_pos_left + kCursorHeight);

    button_color_[ButtonType::kDone] = kButtonNoneColor;
    button_color_[ButtonType::kReturn] = kButtonNoneColor;
    break;
  }
  case ButtonType::kDone: {
    x_pos_left = static_cast<float>(button_done_.left.x);
    y_pos_left = static_cast<float>(button_done_.left.y);
    x_pos_right = static_cast<float>(button_done_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_done_.left.y + kButtonHeight);

    cursor_adjust_.x = kCursorMinusAdjustX;
    cursor_adjust_.y = kCursorMinusAdjustY;

    button_color_[ButtonType::kDone] = kButtonSelectColor;
    button_color_[ButtonType::kReturn] = kButtonNoneColor;
    break;
  }
  case ButtonType::kReturn: {
    x_pos_left = static_cast<float>(button_return_.left.x);
    y_pos_left = static_cast<float>(button_return_.left.y);
    x_pos_right = static_cast<float>(button_return_.left.x + kButtonWidth);
    y_pos_right = static_cast<float>(button_return_.left.y + kButtonHeight);

    cursor_adjust_.x = kCursorMinusAdjustX;
    cursor_adjust_.y = kCursorMinusAdjustY;

    button_color_[ButtonType::kDone] = kButtonNoneColor;
    button_color_[ButtonType::kReturn] = kButtonSelectColor;
    break;
  }
  }

  cursor_.left.x = x_pos_left;
  cursor_.left.y = y_pos_left;
  cursor_.right.x = x_pos_right;
  cursor_.right.y = y_pos_right;
}

/// <summary>
/// ボリューム値 表示位置調整処理
/// </summary>
/// <param name=""> ボリューム </param>
/// <param name=""> X座標 </param>
/// <param name=""> Y座標 </param>
/// <returns></returns>
void SoundSetup::CalcDispVolumeText(int volume, int& x_pos, int& y_pos) {

  // ゲーム情報から画面の中心位置を取得
  GameInfo* game_info = GameInfo::GetGameInfoInstance();
  if (game_info == nullptr) {
    return;
  }
  int x_pos_center = game_info->GetCenterPointX();
  int y_pos_center = game_info->GetCenterPointY();

  // フォント設定
  switch (button_type_) {
  case ButtonType::kBackGroundMusic: {
    // BGMボリューム表示位置
    std::string volume = std::to_string(volume_bgm_rate_) + kVolumeTextRate;
    int text_width = GetDrawStringWidthToHandle(volume.c_str(), static_cast<int>(strlen(volume.c_str())), font_handle_.at(static_cast<int>(FontSize::k22)));
    x_pos = x_pos_center - text_width + kVolumePosX;
    y_pos = text_button_bgm_.y;
    break;
  }
  case ButtonType::kSoundEffect: {
    // SEボリューム表示位置
    std::string volume = std::to_string(volume_se_rate_) + kVolumeTextRate;
    int text_width = GetDrawStringWidthToHandle(volume.c_str(), static_cast<int>(strlen(volume.c_str())), font_handle_.at(static_cast<int>(FontSize::k22)));
    x_pos = x_pos_center - text_width + kVolumePosX;
    y_pos = text_button_se_.y;
    break;
  }
  }
}

/// <summary>
/// 背景の位置とサイズ
/// </summary>
/// <param name=""> 幅 </param>
/// <param name=""> 高さ </param>
/// <param name=""> X座標(左上) </param>
/// <param name=""> Y座標(左上) </param>
/// <returns></returns>
void SoundSetup::SetBackPosAndSize(int width, int height, int x_pos, int y_pos) {

  back_board_.left.x = x_pos;
  back_board_.left.y = y_pos;
  back_board_.right.x = back_board_.left.x + width;
  back_board_.right.y = back_board_.left.y + height;
}