﻿#pragma once

#include "System/EffectId.h"
#include "System/Character.h"
#include <iostream>

/// <summary>
/// キャラクターイベントインターフェース
/// </summary>
class CharacterEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  CharacterEventInterface() {

    // コンソールに出力
    std::cout << "CharacterEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~CharacterEventInterface() {

    // コンソールに出力
    std::cout << "~CharacterEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// バトル再開
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnResumeBattle() = 0;

  /// <summary>
  /// エフェクト処理
  /// </summary>
  /// <param name=""> エフェクトID </param>
  /// <param name=""> キャラクター </param>
  /// <returns></returns>
  virtual void OnExecuteEffect(EffectId, class Character*) = 0;

  /// <summary>
  /// 弾の表示終了
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnFinishDisplayBullet(class Character*){};

  /// <summary>
  /// 残機のチェック処理
  /// </summary>
  /// <param name=""></param>
  /// <returns> true:ゲームオーバー, false:再開 </returns>
  virtual bool OnCheckLeftLife() = 0;


private:

};