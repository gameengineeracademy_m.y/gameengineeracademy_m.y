﻿#pragma once

#include <iostream>

/// <summary>
/// ボリュームコントローライベントインターフェース
/// </summary>
class VolumeControllerEventInterface {
public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  VolumeControllerEventInterface() {

    // コンソールに出力
    std::cout << "VolumeControllerEventInterface コンストラクタ" << std::endl;
  }

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual ~VolumeControllerEventInterface() {

    // コンソールに出力
    std::cout << "~VolumeControllerEventInterface デストラクタ" << std::endl;
  }

  /// <summary>
  /// 右矢印キーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushVolumeControllerRight() {}

  /// <summary>
  /// 左矢印キーが押された時のイベント
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  virtual void OnPushVolumeControllerLeft() {}


private:

};