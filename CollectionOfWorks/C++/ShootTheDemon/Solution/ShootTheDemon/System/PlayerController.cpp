﻿#include "System/PlayerController.h"

namespace {

  /// <summary>
  /// ボタン押下継続時間 リセット
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// ボタン押下有無判定時間
  /// </summary>
  const float kExecuteCheckTime = 0.04f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> キャラクターコントローライベントインターフェース </param>
/// <param name=""> 操作キャラクター </param>
/// <returns></returns>
PlayerController::PlayerController(CharacterControllerEventInterface& character_controller_event_interface,
                                   Player* player)
  : Task(TaskId::kPlayerController)
  , character_controller_event_interface_(character_controller_event_interface)
  , player_(player)
  , key_press_keep_time_{ 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f }
  , push_key_{ false, false, false, false, false } {

  // コンソールに出力
  std::cout << "PlayerController コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
PlayerController::~PlayerController() {

  // コンソールに出力
  std::cout << "~PlayerController デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void PlayerController::Update(float process_time) {

  bool is_up = GetPushUpKey(process_time);
  bool is_down = GetPushDownKey(process_time);
  bool is_left = GetPushLeftKey(process_time);
  bool is_right = GetPushRightKey(process_time);
  bool is_leftshift = GetPushLShiftKey(process_time);
  bool is_esc = GetPushEscapeKey(process_time);
  bool is_z = GetPushZKey(process_time);
  bool is_o = GetPushOKey(process_time);
  bool is_p = GetPushPKey(process_time);
  bool is_l = GetPushLKey(process_time);
  bool is_i = GetPushIKey(process_time);
  bool is_q = GetPushQKey(process_time);

  // 上下移動処理
  if (is_up) {
    //---------------------------------------
    //対象キャラクターを上方向へ移動
    //---------------------------------------
    character_controller_event_interface_.OnPushUpKey(player_, is_leftshift);
  }
  else if (is_down) {
    //---------------------------------------
    //対象キャラクターを下方向へ移動
    //---------------------------------------
    character_controller_event_interface_.OnPushDownKey(player_, is_leftshift);
  }

  // 左右移動処理
  if (is_left) {
    //---------------------------------------
    //対象キャラクターを左方向へ移動
    //---------------------------------------
    character_controller_event_interface_.OnPushLeftKey(player_, is_leftshift);
  }
  else if (is_right) {
    //---------------------------------------
    //対象キャラクターを右方向へ移動
    //---------------------------------------
    character_controller_event_interface_.OnPushRightKey(player_, is_leftshift);
  }

  // 弾発射
  if (is_z) {
    //---------------------------------------
    //弾の発射
    //---------------------------------------
    character_controller_event_interface_.OnPushZKey(player_);
  }

  if (is_esc) {
    //---------------------------------------
    //ポーズメニュー
    //---------------------------------------
    character_controller_event_interface_.OnPushEscapeKey();
  }

  if (is_o) {
    //---------------------------------------
    //バトルレベルを初めからやり直す
    //---------------------------------------
    character_controller_event_interface_.OnPushOKey();
  }

  if (is_p) {
    //---------------------------------------
    //敵HPの操作
    //---------------------------------------
    character_controller_event_interface_.OnPushPKey();
  }

  if (is_l) {
    //---------------------------------------
    //プレイヤーの弾切り替え
    //---------------------------------------
    character_controller_event_interface_.OnPushLKey();
  }

  if (is_i) {
    //---------------------------------------
    //最終ボス 最終フェーズへの切り替え
    //---------------------------------------
    character_controller_event_interface_.OnPushIKey();
  }

  if (is_q) {
    //---------------------------------------
    //最終ボス 最終フェーズへの切り替え
    //---------------------------------------
    character_controller_event_interface_.OnPushQKey();
  }
}

/// <summary>
/// 上矢印キー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushUpKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_UP);
  return CheckPushKey(flag_key_push, process_time, key_press_keep_time_[static_cast<int>(KeyType::kUp)]);
}

/// <summary>
/// 下矢印キー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushDownKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_DOWN);
  return CheckPushKey(flag_key_push, process_time, key_press_keep_time_[static_cast<int>(KeyType::kDown)]);
}

/// <summary>
/// 左矢印キー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushLeftKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_LEFT);
  return CheckPushKey(flag_key_push, process_time, key_press_keep_time_[static_cast<int>(KeyType::kLeft)]);
}

/// <summary>
/// 右矢印キー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushRightKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_RIGHT);
  return CheckPushKey(flag_key_push, process_time, key_press_keep_time_[static_cast<int>(KeyType::kRight)]);
}

/// <summary>
/// Shiftキー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushLShiftKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_LSHIFT);
  return CheckPushKey(flag_key_push, process_time, key_press_keep_time_[static_cast<int>(KeyType::kLShift)]);
}

/// <summary>
/// Zキー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushZKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_Z);
  return CheckPushKey(flag_key_push, process_time, key_press_keep_time_[static_cast<int>(KeyType::kZ)]);
}

/// <summary>
/// ボタン押下の有無を取得する
/// </summary>
/// <param name="key_push"> キーが押されているかどうか </param>
/// <param name="time_sec"> 毎フレームの処理時間 </param>
/// <param name="push_time_sec"> キー押下継続時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::CheckPushKey(bool key_push, float time_sec, float& push_time_sec) {

  if (key_push) {
    //------------------------------
    //現在押されている場合
    //------------------------------
    push_time_sec += time_sec;
    if (push_time_sec > kExecuteCheckTime) {
      return true;
    }
  }
  else {
    //------------------------------
    //現在押されていない場合
    //------------------------------
    // 長押し時間初期化
    push_time_sec = kResetTime;
  }

  return false;
}

/// <summary>
/// Escキー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushEscapeKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_ESCAPE);
  return CheckPushKey2(KEY_INPUT_ESCAPE, push_key_[static_cast<int>(KeyType2::kEsc)]);
}

/// <summary>
/// Oキー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushOKey(float process_time) {

  return CheckPushKey2(KEY_INPUT_O, push_key_[static_cast<int>(KeyType2::kO)]);
}

/// <summary>
/// Pキー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushPKey(float process_time) {

  return CheckPushKey2(KEY_INPUT_P, push_key_[static_cast<int>(KeyType2::kP)]);
}

/// <summary>
/// Lキー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushLKey(float process_time) {

  return CheckPushKey2(KEY_INPUT_L, push_key_[static_cast<int>(KeyType2::kL)]);
}

/// <summary>
/// Iキー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushIKey(float process_time) {

  return CheckPushKey2(KEY_INPUT_I, push_key_[static_cast<int>(KeyType2::kI)]);
}

/// <summary>
/// Qキー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool PlayerController::GetPushQKey(float process_time) {

  return CheckPushKey2(KEY_INPUT_Q, push_key_[static_cast<int>(KeyType2::kQ)]);
}

/// <summary>
/// 指定のキーが押されたかの有無を取得する
/// </summary>
/// <param name="key_code"> キーコード </param>
/// <param name="push_key"> キーの押下の有無 </param>
/// <returns>キーが押された：false、キーが押されていない：false</returns>
bool PlayerController::CheckPushKey2(int key_code, bool& push_key) {

  // 指定のボタンが押されている
  if (CheckHitKey(key_code)) {

    // 直前まで押されていない場合は押されていることにする
    if (push_key == false) {
      push_key = true;
      // 判定を押されたとして返す
      return true;
    }
  }
  else {
    // 前回処理時にキーが押されていた場合は押されていないことにする
    if (push_key) {
      push_key = false;
    }
  }

  return false;
}

/// <summary>
/// 初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void PlayerController::InitializePushKeyFlag() {

  push_key_[static_cast<int>(KeyType2::kEsc)] = true;
  push_key_[static_cast<int>(KeyType2::kO)] = false;
  push_key_[static_cast<int>(KeyType2::kP)] = false;
  push_key_[static_cast<int>(KeyType2::kL)] = false;
  push_key_[static_cast<int>(KeyType2::kI)] = false;
  push_key_[static_cast<int>(KeyType2::kQ)] = true;
}