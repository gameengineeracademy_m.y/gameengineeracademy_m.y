﻿#include "System/LogConsole.h"

namespace {

  /// <summary>
  /// スクリーンバッファ 横
  /// </summary>
  const int kScreenBufferX = 300;

  /// <summary>
  /// スクリーンバッファ 縦
  /// </summary>
  const int kScreenBufferY = 9999;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
LogConsole::LogConsole()
  : std_output_()
  , std_error_output_() {

}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
LogConsole::~LogConsole() {

}

/// <summary>
/// コンソールの生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void LogConsole::CreateConsole() {

  // コンソールの生成
  AllocConsole();

  // 標準出力ハンドルの取得
  HANDLE stdout_handle = GetStdHandle(STD_OUTPUT_HANDLE);
  HANDLE stdout_err_handle = GetStdHandle(STD_ERROR_HANDLE);

  // コンソールのスクリーンバッファを設定する
  COORD coord;

  coord.X = kScreenBufferX;
  coord.Y = kScreenBufferY;

  SetConsoleScreenBufferSize(stdout_handle, coord);
  SetConsoleScreenBufferSize(stdout_err_handle, coord);

  // 標準出力をアクティブなコンソールに書き込み設定で向ける
  freopen_s(&std_output_, "CONOUT$", "w", stdout);
  // 標準エラー出力をアクティブなコンソールに書き込み設定で向ける
  freopen_s(&std_error_output_, "CONOUT$", "w", stderr);
}

/// <summary>
/// コンソールの破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void LogConsole::ReleaseConsole() {

  // 使用したファイルハンドルを閉じる
  fclose(std_output_);
  fclose(std_error_output_);

  // コンソールを破棄する
  FreeConsole();
}