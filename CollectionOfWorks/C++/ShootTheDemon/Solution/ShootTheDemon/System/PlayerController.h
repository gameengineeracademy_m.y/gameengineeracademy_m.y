﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "System/CharacterControllerEventInterface.h"
#include "Game/Player.h"

/// <summary>
/// プレイヤーコントローラ
/// </summary>
class PlayerController : public Task {
public:

  /// <summary>
  /// キーの種類 長押し
  /// </summary>
  enum class KeyType {
    kUp,              // 上矢印キー
    kDown,            // 下矢印キー
    kLeft,            // 左矢印キー
    kRight,           // 右矢印キー
    kLShift,          // 左Shiftキー
    kZ,               // Zキー
    kKeyTypeMaxIndex  // キー項目数
  };

  /// <summary>
  /// キーの種類 1回押し
  /// </summary>
  enum class KeyType2 {
    kEsc,             // Escキー
    kO,               // Oキー
    kP,               // Pキー
    kL,               // Lキー
    kI,               // Iキー
    kQ,               // Qキー
    kKeyTypeMaxIndex  // キー項目数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""> キャラクターコントローライベントインターフェース </param>
  /// <param name=""> 操作キャラクター </param>
  /// <returns></returns>
  PlayerController(CharacterControllerEventInterface&, Player*);

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~PlayerController();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 初期設定
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void InitializePushKeyFlag();

  /// <summary>
  /// 上矢印キー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushUpKey(float);

  /// <summary>
  /// 下矢印キー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushDownKey(float);

  /// <summary>
  /// 左矢印キー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushLeftKey(float);

  /// <summary>
  /// 右矢印キー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushRightKey(float);

  /// <summary>
  /// 左Shiftキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushLShiftKey(float);

  /// <summary>
  /// Escキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushEscapeKey(float);

  /// <summary>
  /// Zキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushZKey(float);

  /// <summary>
  /// Oキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushOKey(float);

  /// <summary>
  /// Pキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushPKey(float);

  /// <summary>
  /// Lキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushLKey(float);

  /// <summary>
  /// Iキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushIKey(float);

  /// <summary>
  /// Qキー押下処理
  /// </summary>
  /// <param name=""> 処理時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool GetPushQKey(float);


private:

  /// <summary>
  /// ボタン押下の有無を取得する
  /// </summary>
  /// <param name="key_push"> キーが押されているかどうか </param>
  /// <param name="time_sec"> 毎フレームの処理時間 </param>
  /// <param name="push_time_sec"> キー押下継続時間 </param>
  /// <returns> true：押された, false：押されていない </returns>
  bool CheckPushKey(bool, float, float&);

  /// <summary>
  /// 指定のキーが押されたかの有無を取得する
  /// </summary>
  /// <param name="key_code"> キーコード </param>
  /// <param name="push_key"> キーの押下の有無 </param>
  /// <returns>キーが押された：false、キーが押されていない：false</returns>
  bool CheckPushKey2(int, bool&);


private:

  /// <summary>
  /// キャラクターコントローラのイベントインターフェース
  /// </summary>
  CharacterControllerEventInterface& character_controller_event_interface_;

  /// <summary>
  /// 操作キャラクター
  /// </summary>
  Player* player_;

  /// <summary>
  /// ボタン長押し時間
  /// </summary>
  float key_press_keep_time_[static_cast<int>(KeyType::kKeyTypeMaxIndex)];

  /// <summary>
  /// キーの押下の有無
  /// </summary>
  bool push_key_[static_cast<int>(KeyType2::kKeyTypeMaxIndex)];
};