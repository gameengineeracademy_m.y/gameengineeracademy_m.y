﻿#include "System/VolumeController.h"

namespace {

  /// <summary>
  /// ボタン押下継続時間 リセット
  /// </summary>
  const float kResetTime = 0.0f;

  /// <summary>
  /// ボタン押下有無判定時間
  /// </summary>
  const float kExecuteCheckTime = 0.08f;
}

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""> ボリュームコントローライベントインターフェース </param>
/// <returns></returns>
VolumeController::VolumeController(VolumeControllerEventInterface& event_interface)
  : Task(TaskId::kVolumeController)
  , volume_controller_event_interface_(event_interface)
  , key_press_keep_time_{ 0.0f, 0.0f }
  , push_key_{ false, false } {

  // コンソールに出力
  std::cout << "VolumeController コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
VolumeController::~VolumeController() {

  // コンソールに出力
  std::cout << "~VolumeController デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void VolumeController::Update(float process_time) {

  bool is_left = GetPushLeftKey(process_time);
  bool is_right = GetPushRightKey(process_time);

  if (is_right) {
    // 右キー押下処理イベント実行
    volume_controller_event_interface_.OnPushVolumeControllerRight();
  }
  else if (is_left) {
    // 左キー押下処理イベント実行
    volume_controller_event_interface_.OnPushVolumeControllerLeft();
  }
}

/// <summary>
/// 左矢印キー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool VolumeController::GetPushLeftKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_LEFT);
  return CheckPushKey(flag_key_push, push_key_[static_cast<int>(KeyType::kLeft)],
                      process_time, key_press_keep_time_[static_cast<int>(KeyType::kLeft)]);
}

/// <summary>
/// 右矢印キー押下処理
/// </summary>
/// <param name=""> 処理時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool VolumeController::GetPushRightKey(float process_time) {

  bool flag_key_push = CheckHitKey(KEY_INPUT_RIGHT);
  return CheckPushKey(flag_key_push, push_key_[static_cast<int>(KeyType::kRight)],
                      process_time, key_press_keep_time_[static_cast<int>(KeyType::kRight)]);
}

/// <summary>
/// ボタン押下の有無を取得する
/// </summary>
/// <param name="key_push"> キーが押されているかどうか </param>
/// <param name="key_push_before"> キーが押されていたかどうか </param>
/// <param name="time_sec"> 毎フレームの処理時間 </param>
/// <param name="push_time_sec"> キー押下継続時間 </param>
/// <returns> true：押された, false：押されていない </returns>
bool VolumeController::CheckPushKey(bool key_push, bool& key_push_before, float time_sec, float& push_time_sec) {

  if (key_push) {
    //------------------------------
    //現在押されている場合
    //------------------------------
    // 1つ前のフレーム処理で押されていたかチェック
    if (key_push_before) {
      push_time_sec += time_sec;
      if (push_time_sec > kExecuteCheckTime) {
        return true;
      }
    }
    else {
      key_push_before = true;
      return true;
    }
  }
  else {
    //------------------------------
    //現在押されていない場合
    //------------------------------
    // 長押し時間、押されたフラグ初期化
    push_time_sec = kResetTime;
    key_push_before = false;
  }

  return false;
}

/// <summary>
/// 初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void VolumeController::InitializePushKeyFlag() {

  key_press_keep_time_[static_cast<int>(KeyType::kRight)] = kResetTime;
  key_press_keep_time_[static_cast<int>(KeyType::kLeft)] = kResetTime;
}