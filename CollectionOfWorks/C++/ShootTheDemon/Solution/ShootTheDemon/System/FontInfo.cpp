﻿#include "System/FontInfo.h"

//-------------------------------
// データメンバの定義 
//-------------------------------
/// <summary>
/// ゲーム情報のインスタンス
/// </summary>
FontInfo* FontInfo::font_info_instance_ = nullptr;

/// <summary>
/// コンストラクタ
/// </summary>
/// <param></param>
/// <returns></returns>
FontInfo::FontInfo() {

}

/// <summary>
/// デストラクタ
/// </summary>
/// <param></param>
/// <returns></returns>
FontInfo::~FontInfo() {

}

/// <summary>
/// インスタンスを作成する
/// </summary>
/// <param></param>
/// <returns></returns>
void FontInfo::CreateFontInfoInstance() {

  if (font_info_instance_ == nullptr) {
    font_info_instance_ = new FontInfo;
  }
}

/// <summary>
/// インスタンスを解放する
/// </summary>
/// <param></param>
/// <returns></returns>
void FontInfo::ReleaseFontInfoInstance() {

  if (font_info_instance_ != nullptr) {
    delete font_info_instance_;
    font_info_instance_ = nullptr;
  }
}

/// <summary>
///  フォントの生成
/// </summary>
/// <param name="file_path"> ファイルパス </param>
/// <returns> 0:生成失敗, それ以外:生成成功 </returns>
int FontInfo::CreateFontInfo(const char* file_path) {

  int result = AddFontResourceEx(file_path, FR_PRIVATE, NULL);

  return result;
}

/// <summary>
///  フォントの破棄
/// </summary>
/// <param name="file_path"> ファイルパス </param>
/// <returns> 0:生成失敗, それ以外:生成成功 </returns>
int FontInfo::ReleaseFontInfo(const char* file_path) {

  int result = RemoveFontResourceEx(file_path, FR_PRIVATE, NULL);

  return result;
}

/// <summary>
/// フォント情報を取得する
/// </summary>
/// <param name=""> フォント名 </param>
/// <param name=""> フォントサイズ </param>
/// <param name=""> フォント幅 </param>
/// <param name=""> フォントの種類 </param>
/// <returns> フォントハンドル </returns>
int FontInfo::GetFontInfo(const char* font_name, int size, int thick, int type) {

  int font_handle = CreateFontToHandle(font_name, size, thick, type, DX_CHARSET_DEFAULT);

  return font_handle;
}