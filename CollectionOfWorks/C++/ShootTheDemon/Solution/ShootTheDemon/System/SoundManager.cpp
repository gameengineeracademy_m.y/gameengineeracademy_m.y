﻿#include "System/SoundManager.h"

/// <summary>
/// コンストラクタ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
SoundManager::SoundManager()
  : Task(TaskId::kSoundManager)
  , sound_list_()
  , generate_se_sound_list_()
  , generate_bgm_sound_list_() {

  // コンソールに出力
  std::cout << "SoundManager コンストラクタ" << std::endl;
}

/// <summary>
/// デストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
SoundManager::~SoundManager() {

  // コンソールに出力
  std::cout << "~SoundManager デストラクタ" << std::endl;
}

/// <summary>
/// 更新処理(毎フレーム)
/// </summary>
/// <param name="process_time"> 処理時間 </param>
/// <returns></returns>
void SoundManager::Update(float process_time) {

  std::vector<int> release_index;

  //----------------------------------------
  // サウンドエフェクト 再生処理
  //----------------------------------------
  for (auto sound : generate_se_sound_list_) {
    bool is_finish = sound.second->PlaySoundSe(process_time);
    if (is_finish) {
      // 再生終了状態にする
      sound.second->SetPlayFinish();
      // 再生後、リリースリストにインデックスを追加
      release_index.push_back(sound.first);
    }
  }
  for (auto index : release_index) {
    if (generate_se_sound_list_.find(index) == generate_se_sound_list_.end()) {
      continue;
    }
    Sound* sound = generate_se_sound_list_[index];
    bool is_finish = sound->IsFinish();
    if (!is_finish) {
      continue;
    }
    generate_se_sound_list_.erase(index);
    if (sound != nullptr) {
      delete sound;
      sound = nullptr;
    }
  }
  // リリースリストをクリア
  release_index.clear();

  //----------------------------------------
  // バックグラウンドミュージック 再生処理
  //----------------------------------------
  for (auto sound : generate_bgm_sound_list_) {
    bool is_finish = sound.second->PlaySoundBgm(process_time);
    if (is_finish) {
      // 再生終了状態にする
      sound.second->SetPlayFinish();
      // 再生後、リリースリストにインデックスを追加
      release_index.push_back(sound.first);
    }
  }
  for (auto index : release_index) {
    if (generate_bgm_sound_list_.find(index) == generate_bgm_sound_list_.end()) {
      continue;
    }
    Sound* sound = generate_bgm_sound_list_[index];
    bool is_finish = sound->IsFinish();
    if (!is_finish) {
      continue;
    }
    generate_bgm_sound_list_.erase(index);
    if (sound != nullptr) {
      int handle = sound->GetSoundHandle();
      StopSoundMem(handle);
      delete sound;
      sound = nullptr;
    }
  }
  // リリースリストをクリア
  release_index.clear();
};

/// <summary>
/// サウンドのリスト登録
/// </summary>
/// <param name="sound_id"> サウンドID </param>
/// <param name="sound_type"> サウンドの種類 </param>
/// <returns> サウンド </returns>
Sound* SoundManager::RegisterSound(SoundId sound_id, Sound::SoundType sound_type) {

  Sound* sound = nullptr;
  // 指定のサウンドIDがサウンドリストに存在するか確認
  // 存在しない場合はサウンドを生成してリストに登録
  if (sound_list_.find(sound_id) != sound_list_.end()) {
    return nullptr;
  }

  sound = new Sound(sound_id);
  if (sound == nullptr) {
    return nullptr;
  }
  // サウンドの種類の設定
  sound->SetSoundType(sound_type);
  sound_list_[sound_id] = sound;
  return sound;
}

/// <summary>
/// サウンドの生成
/// </summary>
/// <param name="sound_id"> サウンドID </param>
/// <returns> サウンド </returns>
Sound* SoundManager::CreateSound(SoundId sound_id) {

  Sound* sound = nullptr;

  // 指定のサウンドIDがサウンドリストに存在するか確認
  // 存在しない場合は処理終了
  if (sound_list_.find(sound_id) == sound_list_.end()) {
    return nullptr;
  }

  // 対象のサウンドを生成
  sound = sound_list_[sound_id]->GenerateClone();
  if (sound == nullptr) {
    return nullptr;
  }

  // サウンドハンドルの音量を設定
  sound->ChangeSoundVolume();

  int index = 0;
  // 生成リストの空きインデックスを調べ、生成リストに登録
  Sound::SoundType sound_type = sound->GetSoundType();
  switch (sound_type) {
  case Sound::SoundType::kSoundEffect: {

    while (true) {
      if (generate_se_sound_list_.find(index) == generate_se_sound_list_.end()) {
        generate_se_sound_list_[index] = sound;
        sound->SetSoundListId(index);
        break;
      }
      ++index;
    }
    break;
  }
  case Sound::SoundType::kBackGroundMusic: {

    while (true) {
      if (generate_bgm_sound_list_.find(index) == generate_bgm_sound_list_.end()) {
        generate_bgm_sound_list_[index] = sound;
        sound->SetSoundListId(index);
        break;
      }
      ++index;
    }
    break;
  }
  }
  return sound;
}

/// <summary>
/// サウンドの破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void SoundManager::DisposeSound() {

  std::vector<int> release_index;
  std::vector<SoundId> release_id;

  // 生成リストにSEサウンドが残っている場合はすべてリストから降ろし、破棄する
  if (!generate_se_sound_list_.empty()) {
    // 生成リスト内のインデックスをすべて取得
    for (auto sound : generate_se_sound_list_) {
      release_index.push_back(sound.first);
    }
    // 生成リストから降ろし、サウンドを破棄する
    for (auto index : release_index) {
      Sound* sound = generate_se_sound_list_[index];
      generate_se_sound_list_.erase(index);
      if (sound != nullptr) {
        delete sound;
        sound = nullptr;
      }
    }
  }
  // インデックスリストクリア
  release_index.clear();

  // 生成リストにBGMサウンドが残っている場合はすべてリストから降ろし、破棄する
  if (!generate_bgm_sound_list_.empty()) {
    // 生成リスト内のインデックスをすべて取得
    for (auto sound : generate_bgm_sound_list_) {
      release_index.push_back(sound.first);
    }
    // 生成リストから降ろし、サウンドを破棄する
    for (auto index : release_index) {
      Sound* sound = generate_bgm_sound_list_[index];
      generate_bgm_sound_list_.erase(index);
      if (sound != nullptr) {
        delete sound;
        sound = nullptr;
      }
    }
  }
  // インデックスリストクリア
  release_index.clear();

  // サウンドリストの破棄
  if (!sound_list_.empty()) {
    // リスト内のインデックスをすべて取得
    for (auto sound : sound_list_) {
      release_id.push_back(sound.first);
    }
    // 画像リソースの破棄後、バレットリストから降ろし、弾を破棄する
    for (auto index : release_id) {
      Sound* sound = sound_list_[index];
      // サウンドリソースの破棄
      DeleteSoundMem(sound->GetSoundHandle());
      // バレットリストから降ろし、弾の破棄
      sound_list_.erase(index);
      if (sound != nullptr) {
        delete sound;
        sound = nullptr;
      }
    }
  }
  // IDリストクリア
  release_id.clear();
}

/// <summary>
/// 指定サウンドの取得
/// </summary>
/// <param name=""> リストID </param>
/// <returns> サウンド </returns>
Sound* SoundManager::GetSoundBgm(int list_id) {

  // 生成リストが空なら処理終了
  if (generate_bgm_sound_list_.empty()) {
    return nullptr;
  }
  // 生成リストに指定のリストIDが存在しないなら処理終了
  if (generate_bgm_sound_list_.find(list_id) == generate_bgm_sound_list_.end()) {
    return nullptr;
  }

  // 指定リストIDのサウンドを取得
  Sound* sound = generate_bgm_sound_list_[list_id];

  return sound;
}

/// <summary>
/// サウンドの終了
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void SoundManager::FinishSoundBgm() {

  if (!generate_bgm_sound_list_.empty()) {
    // 生成リスト内のインデックスをすべて取得
    for (auto sound : generate_bgm_sound_list_) {
      // サウンドのフェーズを「終了処理前」に変更
      sound.second->ChangeCurrentPhase(Sound::PhaseType::kFinishBefore);
      // サウンドボリューム調整値の符号変換
      sound.second->ChangeSoundVolumeAdjustSign();
    }
  }
}

/// <summary>
/// サウンド一時停止処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void SoundManager::PausePlaySound() {

  // 再生中のSEサウンドを一時停止状態にする
  for (auto sound : generate_se_sound_list_) {
    if (sound.second == nullptr) {
      continue;
    }
    sound.second->Pause();
  }
  // 再生中のBGMサウンドを一時停止状態にする
  for (auto sound : generate_bgm_sound_list_) {
    if (sound.second == nullptr) {
      continue;
    }
    sound.second->Pause();
  }
}

/// <summary>
/// サウンドを一時停止から再開する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void SoundManager::ResumePlaySound() {

  // 一時停止中のSEサウンドを再生中にする
  for (auto sound : generate_se_sound_list_) {
    if (sound.second == nullptr) {
      continue;
    }
    sound.second->Resume();
  }
  // 一時停止中のBGMサウンドを再生中にする
  for (auto sound : generate_bgm_sound_list_) {
    if (sound.second == nullptr) {
      continue;
    }
    sound.second->Resume();
  }
}

/// <summary>
/// BGMのボリューム変更
/// </summary>
/// <param name="volume"> ボリューム </param>
/// <returns></returns>
void SoundManager::ChangeSoundVolumeBgm(int volume) {

  for (auto sound : generate_bgm_sound_list_) {
    // サウンドボリューム値を設定し、実際のボリュームを変更
    sound.second->SetSoundVolume(volume);
    sound.second->ChangeSoundVolume();
  }

  for (auto sound : sound_list_) {

    Sound::SoundType sound_type = sound.second->GetSoundType();
    if (sound_type != Sound::SoundType::kBackGroundMusic) {
      continue;
    }
    // サウンドボリューム値を設定し、実際のボリュームを変更
    sound.second->SetSoundVolume(volume);
    sound.second->ChangeSoundVolume();
  }
}

/// <summary>
/// SEのボリューム変更
/// </summary>
/// <param name="volume"> ボリューム </param>
/// <returns></returns>
void SoundManager::ChangeSoundVolumeSe(int volume) {

  for (auto sound : generate_se_sound_list_) {
    // サウンドボリューム値を設定し、実際のボリュームを変更
    sound.second->SetSoundVolume(volume);
    sound.second->ChangeSoundVolume();
  }

  for (auto sound : sound_list_) {

    Sound::SoundType sound_type = sound.second->GetSoundType();
    if (sound_type != Sound::SoundType::kSoundEffect) {
      continue;
    }
    // サウンドボリューム値を設定し、実際のボリュームを変更
    sound.second->SetSoundVolume(volume);
    sound.second->ChangeSoundVolume();
  }
}