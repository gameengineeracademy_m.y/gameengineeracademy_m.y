﻿#pragma once

#include "DxLib.h"
#include "System/Task.h"
#include "System/Effect.h"
#include "System/EffectId.h"
#include <unordered_map>

/// <summary>
/// エフェクトマネージャ
/// </summary>
class EffectManager : public Task {
public:

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  enum class PhaseType {
    kProcess,          // 処理中フェーズ
    kStop,             // 停止フェーズ
    kFinalize,         // 終了処理フェーズ
    kFinalized,        // 終了処理済みフェーズ
    kPhaseMaxIndex     // フェーズ数
  };

public:

  /// <summary>
  /// コンストラクタ 初期化処理
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  EffectManager();

  /// <summary>
  /// デストラクタ
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  ~EffectManager();

  /// <summary>
  /// エフェクトの登録
  /// </summary>
  /// <param name=""> エフェクトID </param>
  /// <param name=""> エフェクトの種類 </param>
  /// <returns> 生成したエフェクト </returns>
  Effect* RegisterEffect(EffectId, Effect::EffectType);

  /// <summary>
  /// エフェクトの生成
  /// </summary>
  /// <param name=""> エフェクトID </param>
  /// <returns> 生成したエフェクト </returns>
  Effect* CreateEffect(EffectId);

  /// <summary>
  /// 生成したエフェクトをすべて終了させる
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void FinishGenerateEffect();

  /// <summary>
  /// エフェクトを降ろす
  /// </summary>
  /// <param name=""> インデックス </param>
  /// <returns> 降ろしたエフェクト </returns>
  Effect* ReleaseEffect(int);

  /// <summary>
  /// エフェクトの破棄
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void DisposeEffect();

  /// <summary>
  /// 更新処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Update(float) override;

  /// <summary>
  /// 描画処理(毎フレーム)
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  void Render() override;

  /// <summary>
  /// 現在のフェーズを変更する
  /// </summary>
  /// <param name=""> フェーズの種類 </param>
  /// <returns></returns>
  void ChangeCurrentPhase(PhaseType phase_type) { current_phase_ = phase_type; }


private:

  /// <summary>
  /// エフェクトを取得
  /// </summary>
  /// <param name=""> インデックス </param>
  /// <returns></returns>
  Effect* GetEffect(int);


private:

  /// <summary>
  /// エフェクトリスト
  /// </summary>
  std::unordered_map<EffectId, Effect*> effect_list_;

  /// <summary>
  /// 生成エフェクトリスト
  /// </summary>
  std::unordered_map<int, Effect*> generate_effect_list_;

  /// <summary>
  /// 現在のフェーズ
  /// </summary>
  PhaseType current_phase_;

  /// <summary>
  /// 生成リストID最大値
  /// </summary>
  int list_id_max_;

  /// <summary>
  /// 透過率
  /// </summary>
  int alpha_;
};