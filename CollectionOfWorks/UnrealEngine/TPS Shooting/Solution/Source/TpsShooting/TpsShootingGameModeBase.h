// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TpsShootingGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TPSSHOOTING_API ATpsShootingGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
