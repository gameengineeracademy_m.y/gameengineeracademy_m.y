﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Title/UiManager/TitleUiManager.h"
#include "Audio/Manager/AudioManager.h"
#include "TitleController.generated.h"


/// <summary>
/// 入力の種類
/// </summary>
UENUM()
enum class ETitleInputType {
	None,          // なし
	Title,         // タイトル操作
	Audio,         // オーディオ設定操作
	Credit,        // クレジット操作
	MaxTypeIndex
};

/// <summary>
/// タイトルレベル用コントローラ
/// </summary>
UCLASS()
class ATitleController : public APlayerController
{
	GENERATED_BODY()

public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ATitleController();

	/// <summary>
	/// 各ウィジェットの表示準備処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PrepareOpenWidget();

	/// <summary>
	/// 各ウィジェットの表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OpenWidget();

	/// <summary>
	/// 各ウィジェットの非表示準備処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PrepareCloseWidget();

	/// <summary>
	/// 各ウィジェットの非表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CloseWidget();

	/// <summary>
	/// タイトルウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> タイトルウィジェット </returns>
	UTitleWidget* GetTitleWidget();

	/// <summary>
	/// フェードウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> フェードウィジェット </returns>
	UFadeWidget* GetFadeWidget();

	/// <summary>
	/// クレジットウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> クレジットウィジェット </returns>
	UCreditWidget* GetCreditWidget();

	/// <summary>
	/// オーディオメニューウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オーディオメニューウィジェット </returns>
	UAudioMenuWidget* GetAudioMenuWidget();

	/// <summary>
	/// タイトルガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> タイトルガイドウィジェット </returns>
	UTitleGuideWidget* GetTitleGuideWidget();

	/// <summary>
	/// オーディオガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オーディオガイドウィジェット </returns>
	UAudioGuideWidget* GetAudioGuideWidget();

	/// <summary>
	/// クレジットガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> クレジットガイドウィジェット </returns>
	UCreditGuideWidget* GetCreditGuideWidget();

	/// <summary>
	/// オーディオマネージャの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オーディオマネージャ </returns>
	AAudioManager* GetAudioManager();

	/// <summary>
	/// インプットの種類を追加
	/// </summary>
	/// <param name="InputType"> 入力の種類 </param>
	/// <returns></returns>
	void AddInputType(ETitleInputType InputType);

	/// <summary>
	/// インプットの種類を削除
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void DeleteInputType();

	/// <summary>
	/// マウスカーソルが移動したかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:移動した </returns>
	bool IsMoveMouseCursor();

protected:

	/// <summary>
	/// インプットコンポーネントの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void SetupInputComponent() override;

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

private:

	/// <summary>
	/// オーディオマネージャの生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreateAudioManager();

	/// <summary>
	/// キーバインドアクションを切り替える
	/// </summary>
	/// <param name="InputType"> 入力の種類 </param>
	/// <returns></returns>
	void SwitchKeyBindAction(ETitleInputType InputType);

	/// <summary>
	/// タイトル画面用のキーバインドアクション設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupKeyBindActionTitle();

	/// <summary>
	/// オプション画面用のキーバインドアクション設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupKeyBindActionOption();

	/// <summary>
	/// クレジット画面用のキーバインドアクション設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupKeyBindActionCredit();

	/// <summary>
	/// 設定されたインプットが存在するかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:存在する </returns>
	bool IsExistsInput();

	/// <summary>
	/// インプットの種類を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> インプットの種類 </returns>
	ETitleInputType GetInputType();

	/// <summary>
	/// 決定ボタン押下処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressDecide();

	/// <summary>
	/// 戻るボタン押下処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressReturn();

	/// <summary>
	/// 上方向ボタン押下処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressUp();

	/// <summary>
	/// 下方向ボタン押下処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressDown();

	/// <summary>
	/// 左方向ボタン押下処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressLeft();

	/// <summary>
	/// 右方向ボタン押下処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressRight();

private:

	/// <summary>
	/// オーディオマネージャクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<AAudioManager> AudioManagerClass;

	/// <summary>
	/// オーディオマネージャ
	/// </summary>
	UPROPERTY()
	AAudioManager* AudioManager;

	/// <summary>
	/// インプットの種類
	/// </summary>
	TArray<ETitleInputType> ArrayInputType;

	/// <summary>
	/// 上下方向のマウス移動量
	/// </summary>
	float OldCurPosX;

	/// <summary>
	/// 水平方向のマウス移動量
	/// </summary>
	float OldCurPosY;

private:
	//--------------------------------
	//以下、定数宣言
	//--------------------------------
	/// <summary>
	/// 0
	/// </summary>
	const int32 Zero = 0;

	/// <summary>
	/// 1
	/// </summary>
	const int32 One = 1;

	/// <summary>
	/// 最小インデックス
	/// </summary>
	const int32 MinIndex = 0;

	/// <summary>
	/// 移動していない量
	/// </summary>
	const float NotMoveValue = 0.0f;
};