﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Title/Maps/TitleLevel.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"
#include "../Public/TpsShootingGameInstance.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ATitleLevel::ATitleLevel()
	: TitleController(nullptr)
	, CurrentPhase(ETitlePhaseType::Initialize)
	, bCompleted(false)
	, AccumulateTime(0.0f) {

	// Tickイベントの有効化
	PrimaryActorTick.bCanEverTick = true;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleLevel::BeginPlay() {

	Super::BeginPlay();

	//---------------------------------------
	//変数の初期化処理
	//---------------------------------------
	// 現在のフェーズ
	CurrentPhase = ETitlePhaseType::Initialize;
	bCompleted = false;
	AccumulateTime = ResetTime;

	//---------------------------------------
	// バトルコントローラのインプット設定
	//---------------------------------------
	// タイトルコントローラを取得
	TitleController = Cast<ATitleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(TitleController)) {
		return;
	}

	//---------------------------------------
	// 各ウィジェットの表示前準備処理
	// 各ウィジェットの表示処理
	//---------------------------------------
	TitleController->PrepareOpenWidget();
	TitleController->OpenWidget();

	// マウスカーソルを表示しておく
	TitleController->bShowMouseCursor = true;

	// 現在のレベルをタイトル画面に設定しておく
	UTpsShootingGameInstance* GameInstance = Cast<UTpsShootingGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (!IsValid(GameInstance)) {
		return;
	}
	GameInstance->SetCurrentLevel(ELevelType::Title);
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ATitleLevel::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

	switch (CurrentPhase) {
	case ETitlePhaseType::Initialize: {
		//--------------------------
		//初期化処理フェーズ
		//--------------------------
		AAudioManager* AudioManager = GetAudioManager();
		if (IsValid(AudioManager)) {
			AudioManager->InitializeBGM();
		}

		// 現在のフェーズを「背景表示待機」フェーズに変更
		ChangeCurrentPhase(ETitlePhaseType::DisplayBackGround);
		break;
	}
	case ETitlePhaseType::DisplayBackGround: {
		//--------------------------
		//背景表示フェーズ
		//--------------------------
		AccumulateTime += DeltaTime;
		if (AccumulateTime < DisplayBackGroundWaitTime) {
			break;
		}
		AccumulateTime = ResetTime;

		// 現在のフェーズを「フェードイン」フェーズに変更
		ChangeCurrentPhase(ETitlePhaseType::FadeIn);
		break;
	}
	case ETitlePhaseType::FadeIn: {
		//--------------------------
		//フェードインフェーズ
		//--------------------------
		// タイトルコントローラ、フェードウィジェットが
		// 見つからない場合は強制的に次のフェーズに進む
		UFadeWidget* FadeWidget = GetFadeWidget();
		if (IsValid(FadeWidget)) {
			// フェードイン実行
			FadeWidget->PlayFadeInAnimation();
		}

		// 現在のフェーズを「フェードイン待機」フェーズに変更
		ChangeCurrentPhase(ETitlePhaseType::WaitFadeIn);
		break;
	}
	case ETitlePhaseType::WaitFadeIn: {
		//--------------------------
		//フェードイン待機フェーズ
		//--------------------------
		// タイトルコントローラ、各ウィジェットが
		// 見つからない場合は強制的に次のフェーズに進む
		bool IsFinish = true;
		UFadeWidget* FadeWidget = GetFadeWidget();
		if (IsValid(FadeWidget)) {
			IsFinish = FadeWidget->IsAnimFinish();
			// フェードウィジェットのフラグリセット
			FadeWidget->ResetAnimFlag();
		}
		if (!IsFinish) {
			break;
		}
		FadeWidget->Closed();

		AAudioManager* AudioManager = GetAudioManager();
		if (IsValid(AudioManager)) {
			// BGMの設定と再生
			AudioManager->SetBGMSound(EBGMType::Title);
			AudioManager->FadeInBGM();
		}

		// 現在のフェーズを「タイトルアニメーション」フェーズに変更
		ChangeCurrentPhase(ETitlePhaseType::TitleAnimation);
		break;
	}
	case ETitlePhaseType::TitleAnimation: {
		//--------------------------
		//タイトルアニメーションフェーズ
		//--------------------------
		UTitleWidget* TitleWidget = GetTitleWidget();
		if (!IsValid(TitleWidget)) {
			return;
		}
		TitleWidget->ResetAnimFlag();
		TitleWidget->PlayTitleAnimation();

		// 現在のフェーズを「アニメショーン待機」フェーズに変更
		ChangeCurrentPhase(ETitlePhaseType::WaitAnimation);
		break;
	}
	case ETitlePhaseType::WaitAnimation: {
		//--------------------------
		//アニメーション待機フェーズ
		//--------------------------
		UTitleWidget* TitleWidget = GetTitleWidget();
		if (IsValid(TitleWidget)) {

			// スキップチェック
			bool IsSkip = TitleWidget->IsSkip();
			if (IsSkip) {
				// タイトルウィジェットのアニメーションを終了させる
				TitleWidget->FinishTitleAnimation();
				TitleWidget->FinishedAnimation();
				// 現在のフェーズを「操作準備」フェーズに変更
				ChangeCurrentPhase(ETitlePhaseType::PrepareOperate);
				// ガイドウィジェットを表示しておく
				UTitleGuideWidget* TitleGuideWidget = GetTitleGuideWidget();
				if (IsValid(TitleGuideWidget)) {
					TitleGuideWidget->Display();
				}
				return;
			}

			// アニメーション終了チェック
			bool IsFinish = true;
			IsFinish = TitleWidget->IsAnimFinish();
			if (!IsFinish) {
				break;
			}
			// タイトルウィジェットのフラグリセット
			// ボタン操作を可能にしておく
			TitleWidget->ResetAnimFlag();
			TitleWidget->FinishedAnimation();
		}

		UTitleGuideWidget* TitleGuideWidget = GetTitleGuideWidget();
		if (IsValid(TitleGuideWidget)) {
			TitleGuideWidget->Display();
		}

		// 現在のフェーズを「操作準備」フェーズに変更
		ChangeCurrentPhase(ETitlePhaseType::PrepareOperate);
		break;
	}
	case ETitlePhaseType::PrepareOperate: {
		//--------------------------
		//操作準備フェーズ
		//--------------------------
		if (!IsValid(TitleController)) {
			break;
		}
		TitleController->AddInputType(ETitleInputType::Title);

		// 現在のフェーズを「タイトル画面操作」に変更
		ChangeCurrentPhase(ETitlePhaseType::OperateTitle);
		break;
	}
	case ETitlePhaseType::OperateTitle: {
		//--------------------------
		//タイトル画面操作フェーズ
		//--------------------------
		OperateTitle();
		break;
	}
	case ETitlePhaseType::OperateOption: {
		//--------------------------
		//オプション操作フェーズ
		//--------------------------
		bool IsFinish = OperateOption();
		if (!IsFinish) {
			break;
		}

		// 現在のフェーズを「ボタン操作」に変更
		ChangeCurrentPhase(ETitlePhaseType::OperateTitle);
		break;
	}
	case ETitlePhaseType::OperateCredit: {
		//--------------------------
		//クレジット操作フェーズ
		//--------------------------
		bool IsFinish = OperateCredit();
		if (!IsFinish) {
			break;
		}

		// 現在のフェーズを「ボタン操作」に変更
		ChangeCurrentPhase(ETitlePhaseType::OperateTitle);
		break;
	}
	case ETitlePhaseType::FadeOut: {
		//--------------------------
		//フェードアウトフェーズ
		//--------------------------
		// タイトルコントローラ、フェードウィジェットが
		// 見つからない場合は強制的に次のフェーズに進む
		UFadeWidget* FadeWidget = GetFadeWidget();
		if (IsValid(FadeWidget)) {
			// フェードアウト実行
			FadeWidget->Open();
			FadeWidget->PlayFadeOutAnimation();
		}

		// 現在のフェーズを「レベル切替」フェーズに変更
		ChangeCurrentPhase(ETitlePhaseType::ChangeLevel);
		break;
	}
	case ETitlePhaseType::ChangeLevel: {
		//--------------------------
		//レベル切替フェーズ
		//※フェードアウトが終了するまで待機
		//--------------------------
		// タイトルコントローラ、フェードウィジェットが
		// 見つからない場合は強制的に次のフェーズに進む
		bool IsFinish = true;
		UFadeWidget* FadeWidget = GetFadeWidget();
		if (IsValid(FadeWidget)) {
			IsFinish = FadeWidget->IsAnimFinish();
			// フェードウィジェットのフラグリセット
			FadeWidget->ResetAnimFlag();
		}

		if (!IsFinish) {
			break;
		}

		// 背景レベルのアンロード処理
		UnloadStreamLevel(TitleBackLevelName, CompletedFunctionName);

		//ウィジェットを非表示にする
		TitleController->PrepareCloseWidget();
		TitleController->CloseWidget();

		//--------------------------
		// レベルを変更する
		//--------------------------
		UGameplayStatics::OpenLevel(GetWorld(), TEXT("BattleLevel"), true);
		// 現在のフェーズを「終了処理」に変更
		ChangeCurrentPhase(ETitlePhaseType::Finish);
		break;
	}
	case ETitlePhaseType::Finish: {
		//--------------------------
		//終了処理フェーズ
		//--------------------------
		break;
	}
	}
}

/// <summary>
/// レベルのロード処理
/// </summary>
/// <param name="LevelName"> ロードするレベル名 </param>
/// <param name="FunctionName"> ロード後の実行関数名 </param>
/// <returns></returns>
void ATitleLevel::LoadStreamLevel(FName LevelName, FName FunctionName) {

	//----------------------------------------------
	//レベルロード完了フラグを降ろした後、
	//指定レベルを非同期処理にてロードする
	//----------------------------------------------
	ResetCompleted();

	FLatentActionInfo LatentAction;
	LatentAction.CallbackTarget = this;
	LatentAction.ExecutionFunction = FunctionName;
	LatentAction.UUID += UUIDIndex;
	LatentAction.Linkage = LinkageValue;
	UGameplayStatics::LoadStreamLevel(this, LevelName, true, false, LatentAction);
}

/// <summary>
/// レベルのアンロード処理
/// </summary>
/// <param name="LevelName"> ロードするレベル名 < / param>
/// <param name="FunctionName"> ロード後の実行関数名 </param>
/// <returns></returns>
void ATitleLevel::UnloadStreamLevel(FName LevelName, FName FunctionName) {

	//----------------------------------------------
	//レベルロード完了フラグを降ろした後、
	//指定レベルを非同期処理にてアンロードする
	//----------------------------------------------
	ResetCompleted();

	FLatentActionInfo LatentAction;
	LatentAction.CallbackTarget = this;
	LatentAction.ExecutionFunction = FunctionName;
	LatentAction.UUID += UUIDIndex;
	LatentAction.Linkage = LinkageValue;
	UGameplayStatics::UnloadStreamLevel(this, LevelName, LatentAction, false);
}

/// <summary>
/// 処理完了通知処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleLevel::OnCompleted() {

	SetCompleted();
}

/// <summary>
/// ロード完了フラグのセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleLevel::SetCompleted() {

	bCompleted = true;
}

/// <summary>
/// ロード完了フラグのリセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleLevel::ResetCompleted() {

	bCompleted = false;
}

/// <summary>
/// ロード完了したかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:ロード完了 </returns>
bool ATitleLevel::IsCompleted() {

	return bCompleted;
}

/// <summary>
/// タイトル画面処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleLevel::OperateTitle() {

	//---------------------------------------------
	// 決定操作が行われるまで待機
	// 決定操作が行われたボタンに応じてフェーズを
	// 遷移したり、別ウィジェットを表示する
	//---------------------------------------------
	UTitleWidget* TitleWidget = GetTitleWidget();
	if (!IsValid(TitleWidget) || !IsValid(TitleController)) {
		return;
	}

	bool bCheckMove = TitleController->IsMoveMouseCursor();
	// マウス操作処理
	TitleWidget->OperateMouse(bCheckMove);

	if (!TitleWidget->IsDecide()) {
		return;
	}
	EButtonType ButtonType = TitleWidget->GetDecideButtonType();
	switch (ButtonType) {
	case EButtonType::GameStart: {
		//-------------------------------
		// ゲーム開始処理
		//-------------------------------
		UTpsShootingGameInstance* GameInstance = Cast<UTpsShootingGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
		if (!IsValid(GameInstance)) {
			return;
		}
		GameInstance->SetCurrentLevel(ELevelType::None);
		GameInstance->SetNextTransLevel(ELevelType::MainField);
		
		//非同期で遷移するレベルを読み込んでおく
		FLatentActionInfo LatentActionInfo;
		UGameplayStatics::LoadStreamLevel(GetWorld(), TEXT("BattleLevel"), false, false, LatentActionInfo);
		
		AAudioManager* AudioManager = GetAudioManager();
		if (IsValid(AudioManager)) {
			// BGMのフェードアウト
			AudioManager->FadeOutBGM();
		}
		
		// 現在のフェーズを「フェードアウト」に変更
		ChangeCurrentPhase(ETitlePhaseType::FadeOut);
		break;
	}
	case EButtonType::Tutorial: {
		//-------------------------------
		// チュートリアル処理
		//-------------------------------
		UTpsShootingGameInstance* GameInstance = Cast<UTpsShootingGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
		if (!IsValid(GameInstance)) {
			return;
		}
		GameInstance->SetCurrentLevel(ELevelType::None);
		GameInstance->SetNextTransLevel(ELevelType::Tutorial);

		//非同期で遷移するレベルを読み込んでおく
		FLatentActionInfo LatentActionInfo;
		UGameplayStatics::LoadStreamLevel(GetWorld(), TEXT("BattleLevel"), false, false, LatentActionInfo);
		
		AAudioManager* AudioManager = GetAudioManager();
		if (IsValid(AudioManager)) {
			// BGMのフェードアウト
			AudioManager->FadeOutBGM();
		}

		// 現在のフェーズを「フェードアウト」に変更
		ChangeCurrentPhase(ETitlePhaseType::FadeOut);
		break;
	}
	case EButtonType::Option: {
		//-------------------------------
		// オプション表示処理
		//-------------------------------
		UAudioMenuWidget* AudioMenuWidget = GetAudioMenuWidget();
		AAudioManager* AudioManager = GetAudioManager();
		if (!IsValid(AudioMenuWidget) || !IsValid(AudioManager)) {
			break;
		}

		// オーディオメニュー画面の設定を行い、オプション画面を開く
		AudioMenuWidget->SetupAudioVolume(AudioManager->GetAudioVolume());
		AudioMenuWidget->Open();
		TitleController->AddInputType(ETitleInputType::Audio);

		// オーディオガイドを開く
		UAudioGuideWidget* AudioGuideWidget = GetAudioGuideWidget();
		if (IsValid(AudioGuideWidget)) {
			AudioGuideWidget->Open();
		}

		// 現在のフェーズを「オプション操作」に変更
		ChangeCurrentPhase(ETitlePhaseType::OperateOption);
		break;
	}
	case EButtonType::Credit: {
		//-------------------------------
		// クレジット表示処理
		//-------------------------------
		UCreditWidget* CreditWidget = GetCreditWidget();
		if (!IsValid(CreditWidget)) {
			break;
		}

		// クレジット画面を開く
		CreditWidget->Open();
		// クレジットガイドを開く
		UCreditGuideWidget* CreditGuideWidget = GetCreditGuideWidget();
		if (IsValid(CreditGuideWidget)) {
			CreditGuideWidget->Open();
		}

		TitleController->AddInputType(ETitleInputType::Credit);

		// 現在のフェーズを「クレジット操作」に変更
		ChangeCurrentPhase(ETitlePhaseType::OperateCredit);
		break;
	}
	case EButtonType::GameEnd: {
		//-------------------------------
		// ゲーム終了処理
		//-------------------------------
		AAudioManager* AudioManager = GetAudioManager();
		if (IsValid(AudioManager)) {
			// BGMの終了
			AudioManager->FinishSoundBGM();
		}

		// ゲームを終了する
		UKismetSystemLibrary::QuitGame(GetWorld(), nullptr, EQuitPreference::Quit, false);
		// 現在のフェーズを「終了処理」に変更
		ChangeCurrentPhase(ETitlePhaseType::Finish);
		break;
	}
	default: {

		break;
	}
	}

	// 決定した処理をリセットしておく
	TitleWidget->ResetOperation();
}

/// <summary>
/// オプション画面処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了 </returns>
bool ATitleLevel::OperateOption() {

	//---------------------------------------------
	// 決定または戻る操作が行われるまで待機
	// 決定操作が行われたボタンに応じてフェーズを
	// 遷移したり、別ウィジェットを表示する
	//---------------------------------------------

	UAudioMenuWidget* AudioMenuWidget = GetAudioMenuWidget();
	if (!IsValid(AudioMenuWidget) || !IsValid(TitleController)) {
		return false;
	}

	bool bCheckMove = TitleController->IsMoveMouseCursor();
	// マウス処理
	AudioMenuWidget->OperateMouse(bCheckMove);

	if (!AudioMenuWidget->IsDecide() && !AudioMenuWidget->IsReturn()) {
		return false;
	}

	AAudioManager* AudioManager = GetAudioManager();
	if (IsValid(AudioManager)) {
		if (AudioMenuWidget->IsDecide()) {
			// 新しい音量を設定する
			AudioManager->SetAudioVolume(AudioMenuWidget->GetAfterSettingAudioVolume());
		}
		// 音量を更新する
		// ※決定操作も戻る操作も更新しておく
		AudioManager->UpdateVolume();
	}

	// オーディオガイドを閉じる
	UAudioGuideWidget* AudioGuideWidget = GetAudioGuideWidget();
	if (IsValid(AudioGuideWidget)) {
		if (AudioGuideWidget->IsDisplay()) {
			AudioGuideWidget->Closed();
		}
	}

	// オプション画面を閉じる
	AudioMenuWidget->Closed();
	TitleController->DeleteInputType();

	return true;
}

/// <summary>
/// クレジット画面処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了 </returns>
bool ATitleLevel::OperateCredit() {

	UCreditWidget* CreditWidget = GetCreditWidget();
	if (!IsValid(CreditWidget)) {
		return false;
	}

	if (!CreditWidget->IsReturn()) {
		return false;
	}

	// クレジットガイドを閉じる
	UCreditGuideWidget* CreditGuideWidget = GetCreditGuideWidget();
	if (IsValid(CreditGuideWidget)) {
		if (CreditGuideWidget->IsDisplay()) {
			CreditGuideWidget->Closed();
		}
	}

	// オプション画面を閉じる
	CreditWidget->Closed();
	TitleController->DeleteInputType();

	return true;
}

/// <summary>
/// 現在のフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ATitleLevel::ChangeCurrentPhase(ETitlePhaseType PhaseType) {

	CurrentPhase = PhaseType;
}

/// <summary>
/// フェードウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> フェードウィジェット </returns>
UFadeWidget* ATitleLevel::GetFadeWidget() {

	if (!IsValid(TitleController)) {
		return nullptr;
	}

	return TitleController->GetFadeWidget();
}

/// <summary>
/// タイトルウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> タイトルウィジェット </returns>
UTitleWidget* ATitleLevel::GetTitleWidget() {

	if (!IsValid(TitleController)) {
		return nullptr;
	}
	
	return TitleController->GetTitleWidget();
}

/// <summary>
/// クレジットウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> クレジットウィジェット </returns>
UCreditWidget* ATitleLevel::GetCreditWidget() {

	if (!IsValid(TitleController)) {
		return nullptr;
	}

	return TitleController->GetCreditWidget();
}

/// <summary>
/// オーディオメニューウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> オーディオメニューウィジェット </returns>
UAudioMenuWidget* ATitleLevel::GetAudioMenuWidget() {

	if (!IsValid(TitleController)) {
		return nullptr;
	}

	return TitleController->GetAudioMenuWidget();
}

/// <summary>
/// タイトルガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> タイトルガイドウィジェット </returns>
UTitleGuideWidget* ATitleLevel::GetTitleGuideWidget() {

	if (!IsValid(TitleController)) {
		return nullptr;
	}

	return TitleController->GetTitleGuideWidget();
}

/// <summary>
/// オーディオガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> オーディオガイドウィジェット </returns>
UAudioGuideWidget* ATitleLevel::GetAudioGuideWidget() {

	if (!IsValid(TitleController)) {
		return nullptr;
	}

	return TitleController->GetAudioGuideWidget();
}

/// <summary>
/// クレジットガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> クレジットガイドウィジェット </returns>
UCreditGuideWidget* ATitleLevel::GetCreditGuideWidget() {

	if (!IsValid(TitleController)) {
		return nullptr;
	}

	return TitleController->GetCreditGuideWidget();
}

/// <summary>
/// オーディオマネージャの取得
/// </summary>
/// <param name=""></param>
/// <returns> オーディオマネージャ </returns>
AAudioManager* ATitleLevel::GetAudioManager() {

	if (!IsValid(TitleController)) {
		return nullptr;
	}

	return TitleController->GetAudioManager();
}