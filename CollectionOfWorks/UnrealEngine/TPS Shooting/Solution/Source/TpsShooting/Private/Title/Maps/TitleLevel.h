﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "Title/Controller/TitleController.h"
#include "TitleLevel.generated.h"

/// <summary>
/// レベル実行フェーズの種類
/// </summary>
UENUM()
enum class ETitlePhaseType {
	Initialize,        // 初期化処理フェーズ
	DisplayBackGround, // 背景表示待機フェーズ
	FadeIn,            // フェードインフェーズ
	WaitFadeIn,        // フェードイン待機フェーズ
	TitleAnimation,    // タイトルアニメーションフェーズ
	WaitAnimation,     // アニメーション待機フェーズ
	PrepareOperate,    // 操作準備フェーズ
	OperateTitle,      // ボタン操作フェーズ
	OperateOption,     // オプション操作フェーズ
	OperateCredit,     // クレジット操作フェーズ
	FadeOut,           // フェードアウトフェーズ
	ChangeLevel,       // レベル切替フェーズ
	Finish,            // 終了処理フェーズ
	MaxTypeIndex
};

/// <summary>
/// タイトルレベル
/// </summary>
UCLASS()
class ATitleLevel : public ALevelScriptActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ATitleLevel();

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

	/// <summary>
	/// 完了通知処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION()
	void OnCompleted();

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

private:

	/// <summary>
	/// レベルのロード処理
	/// </summary>
	/// <param name="LevelName"> ロードするレベル名 < / param>
	/// <param name="FunctionName"> ロード後の実行関数名 </param>
	/// <returns></returns>
	void LoadStreamLevel(FName LevelName, FName FunctionName);

	/// <summary>
	/// レベルのアンロード処理
	/// </summary>
	/// <param name="LevelName"> ロードするレベル名 < / param>
	/// <param name="FunctionName"> ロード後の実行関数名 </param>
	/// <returns></returns>
	void UnloadStreamLevel(FName LevelName, FName FunctionName);

	/// <summary>
	/// 処理完了フラグのセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetCompleted();

	/// <summary>
	/// 処理完了フラグのリセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetCompleted();

	/// <summary>
	/// 処理が完了したかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:ロード完了 </returns>
	bool IsCompleted();

	/// <summary>
	/// 現在のフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentPhase(ETitlePhaseType PhaseType);

	/// <summary>
	/// タイトル画面処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OperateTitle();

	/// <summary>
	/// オプション画面処理
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:処理終了 </returns>
	bool OperateOption();

	/// <summary>
	/// クレジット画面処理
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:処理終了 </returns>
	bool OperateCredit();

	/// <summary>
	/// フェードウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> フェードウィジェット </returns>
	UFadeWidget* GetFadeWidget();

	/// <summary>
	/// タイトルウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> タイトルウィジェット </returns>
	UTitleWidget* GetTitleWidget();

	/// <summary>
	/// クレジットウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> クレジットウィジェット </returns>
	UCreditWidget* GetCreditWidget();

	/// <summary>
	/// オーディオメニューウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オーディオメニューウィジェット </returns>
	UAudioMenuWidget* GetAudioMenuWidget();

	/// <summary>
	/// タイトルガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> タイトルガイドウィジェット </returns>
	UTitleGuideWidget* GetTitleGuideWidget();

	/// <summary>
	/// オーディオガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オーディオガイドウィジェット </returns>
	UAudioGuideWidget* GetAudioGuideWidget();

	/// <summary>
	/// クレジットガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> クレジットガイドウィジェット </returns>
	UCreditGuideWidget* GetCreditGuideWidget();

	/// <summary>
	/// オーディオマネージャの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オーディオマネージャ </returns>
	AAudioManager* GetAudioManager();

private:

	/// <summary>
	/// タイトルコントローラ
	/// </summary>
	UPROPERTY()
	ATitleController* TitleController;

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	ETitlePhaseType CurrentPhase;

	/// <summary>
	/// 処理完了フラグ
	/// </summary>
	bool bCompleted;

	/// <summary>
	/// 累積時間
	/// </summary>
	float AccumulateTime;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// リセット時間
	/// </summary>
	const float ResetTime = 0.0f;

	/// <summary>
	/// 背景表示待機時間
	/// </summary>
	const float DisplayBackGroundWaitTime = 0.5f;

	/// <summary>
	/// UUID加算値
	/// </summary>
	const int32 UUIDIndex = 1;

	/// <summary>
	/// リンケージ
	/// </summary>
	const int32 LinkageValue = 1;

	/// <summary>
	/// タイトル背景レベル名
	/// </summary>
	const FName TitleBackLevelName = TEXT("TitleBack");

	/// <summary>
	/// 完了通知処理名
	/// </summary>
	const FName CompletedFunctionName = TEXT("OnCompleted");

	/// <summary>
	/// バトルレベル
	/// </summary>
	const FName BattleLevelName = TEXT("BattleLevel");
};