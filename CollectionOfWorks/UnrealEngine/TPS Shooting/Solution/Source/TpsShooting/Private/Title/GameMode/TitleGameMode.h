﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TitleGameMode.generated.h"

/// <summary>
/// タイトルレベルのゲームモード処理
/// </summary>
UCLASS()
class ATitleGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ATitleGameMode();

protected:

	/// <summary>
	/// 生成後実行イベント
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;
};
