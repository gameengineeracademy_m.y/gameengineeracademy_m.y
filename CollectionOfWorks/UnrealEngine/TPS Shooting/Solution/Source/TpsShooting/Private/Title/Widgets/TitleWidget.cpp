// Fill out your copyright notice in the Description page of Project Settings.


#include "Title/Widgets/TitleWidget.h"
#include "Title/Controller/TitleController.h"
#include "UiAssignmentWidget/OperateBaseWidget/Button/LevelUIButton/LevelUIButton.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "Components/Image.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UTitleWidget::UTitleWidget() {

}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTitleWidget::NativeConstruct() {

	Super::NativeConstruct();

	CurrentAnimPhase = ETitleAnimPhaseType::None;
	// アニメーション準備
	InitializeAnimation();
}

/// <summary>
/// イベントの設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTitleWidget::SetupEvent() {

	UButton* Button = Cast<UButton>(GetWidgetFromName(SkipButtonName));
	if (!IsValid(Button)) {
		return;
	}
	//-------------------------------------
	//ボタンのクリック処理のデリゲート追加
	//-------------------------------------
	if (!Button->OnPressed.IsBound()) {
		Button->OnPressed.AddDynamic(this, &UTitleWidget::OnPressed);
	}
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTitleWidget::InitializeAnimation() {

	//----------------------------------
	//アニメーション準備のため、
	//各項目を事前に透過しておく
	//----------------------------------

	// タイトルテキスト
	UTextBlock* TextBlock = Cast<UTextBlock>(GetWidgetFromName(TitleTextName));
	if (!IsValid(TextBlock)) {
		return;
	}
	TextBlock->SetRenderOpacity(Transparent);

	// ボタン背景イメージ
	UImage* Image = Cast<UImage>(GetWidgetFromName(LeftBackImageName));
	if (!IsValid(Image)) {
		return;
	}
	Image->SetRenderOpacity(Transparent);

	// ボタン配置パネル
	UVerticalBox* VerticalBox = Cast<UVerticalBox>(GetWidgetFromName(VerticalBoxName));
	if (!IsValid(VerticalBox)) {
		return;
	}
	VerticalBox->SetVisibility(ESlateVisibility::HitTestInvisible);
	VerticalBox->SetRenderOpacity(Transparent);
}

/// <summary>
/// アニメーション終了後処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTitleWidget::FinishedAnimation() {

	// ボタン配置パネル
	UVerticalBox* VerticalBox = Cast<UVerticalBox>(GetWidgetFromName(VerticalBoxName));
	if (!IsValid(VerticalBox)) {
		return;
	}
	VerticalBox->SetVisibility(ESlateVisibility::Visible);
}

/// <summary>
/// プレイ中かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:プレイ中 </returns>
bool UTitleWidget::IsPlay() {

	return CurrentAnimPhase == ETitleAnimPhaseType::Play;
}

/// <summary>
/// スキップされたかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:スキップされた </returns>
bool UTitleWidget::IsSkip() {

	return CurrentAnimPhase == ETitleAnimPhaseType::Skip;
}

/// <summary>
/// 現在のアニメーションフェーズの種類を変更
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void UTitleWidget::ChangeCurrentAnimPhaseType(ETitleAnimPhaseType PhaseType) {

	CurrentAnimPhase = PhaseType;
}

/// <summary>
/// プレス処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTitleWidget::OnPressed() {

	if (!IsPlay()) {
		return;
	}
	ChangeCurrentAnimPhaseType(ETitleAnimPhaseType::Skip);
}

/// <summary>
/// アニメーション実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTitleWidget::PlayTitleAnimation_Implementation() {

}

/// <summary>
/// アニメーション強制終了処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTitleWidget::FinishTitleAnimation_Implementation() {

}