// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/OperateBaseWidget/LevelUIBaseWidget/LevelUIBaseWidget.h"
#include "TitleWidget.generated.h"


/// <summary>
/// タイトルアニメーションフェーズの種類
/// </summary>
UENUM(BlueprintType)
enum class ETitleAnimPhaseType : uint8 {
	None,      // 何もなし
	Play,      // プレイ中
	Skip,      // スキップ
	Finish,    // 終了
	MaxPhaseIndex
};

/// <summary>
/// タイトルレベルウィジェット
/// </summary>
UCLASS()
class UTitleWidget : public ULevelUIBaseWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UTitleWidget();

	/// <summary>
	/// イベントの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupEvent();

	/// <summary>
	/// アニメーション初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeAnimation();

	/// <summary>
	/// アニメーション終了後処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void FinishedAnimation();

	/// <summary>
	/// プレイ中かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:プレイ中 </returns>
	UFUNCTION(BlueprintCallable, Category = "TitleWidget")
	bool IsPlay();

	/// <summary>
	/// スキップされたかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:スキップされた </returns>
	bool IsSkip();

	/// <summary>
	/// プレス処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION()
	void OnPressed();

	/// <summary>
	/// 現在のアニメーションフェーズの種類を変更
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "TitleWidget")
	void ChangeCurrentAnimPhaseType(ETitleAnimPhaseType PhaseType);

	/// <summary>
	/// アニメーション実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="TitleWidget")
	void PlayTitleAnimation();

	/// <summary>
	/// アニメーション強制終了処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "TitleWidget")
	void FinishTitleAnimation();

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

private:

	/// <summary>
	/// アニメーション実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayTitleAnimation_Implementation();

	/// <summary>
	/// アニメーション強制終了処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void FinishTitleAnimation_Implementation();

private:

	/// <summary>
	/// 現在のアニメーションフェーズの種類
	/// </summary>
	ETitleAnimPhaseType CurrentAnimPhase;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// スキップボタン
	/// </summary>
	const FName SkipButtonName = "Button_Skip";

	/// <summary>
	/// ボタン背景イメージ名
	/// </summary>
	const FName LeftBackImageName = "Image_LeftBack";

	/// <summary>
	/// タイトルテキスト名
	/// </summary>
	const FName TitleTextName = "TextBlock_Title";

	/// <summary>
	/// 透過
	/// </summary>
	const float Transparent = 0.0f;
};
