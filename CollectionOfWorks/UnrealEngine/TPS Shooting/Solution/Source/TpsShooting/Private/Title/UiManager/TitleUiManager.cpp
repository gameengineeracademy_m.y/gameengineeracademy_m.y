﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Title/UiManager/TitleUiManager.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ATitleUiManager::ATitleUiManager()
	: TitleWidgetClass()
	, TitleWidget(nullptr)
	, FadeWidgetClass()
	, FadeWidget(nullptr)
	, CreditWidgetClass()
	, CreditWidget(nullptr)
	, AudioMenuWidgetClass()
	, AudioMenuWidget(nullptr)
	, TitleGuideWidgetClass()
	, TitleGuideWidget(nullptr)
	, AudioGuideWidgetClass()
	, AudioGuideWidget(nullptr)
	, CreditGuideWidgetClass()
	, CreditGuideWidget(nullptr) {

}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleUiManager::PreInitializeComponents() {

	//-----------------------------------------------------
	// 各ウィジェットパスを「DefaultGame.ini」から取得し、
	// 各ウィジェットを生成する
	//-----------------------------------------------------
	// タイトルウィジェット
	FString TitleWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("TitleWidgetPath"), TitleWidgetPath, GGameIni);
	TitleWidgetClass = TSoftClassPtr<UTitleWidget>(FSoftObjectPath(*TitleWidgetPath)).LoadSynchronous();
	if (TitleWidgetClass) {
		TitleWidget = CreateWidget<UTitleWidget>(GetWorld(), TitleWidgetClass);
	}
	
	// フェードウィジェット
	FString FadeWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("FadeWidgetPath"), FadeWidgetPath, GGameIni);
	FadeWidgetClass = TSoftClassPtr<UFadeWidget>(FSoftObjectPath(*FadeWidgetPath)).LoadSynchronous();
	if (FadeWidgetClass) {
		FadeWidget = CreateWidget<UFadeWidget>(GetWorld(), FadeWidgetClass);
	}
	
	// クレジットウィジェット
	FString CreditWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("CreditWidgetPath"), CreditWidgetPath, GGameIni);
	CreditWidgetClass = TSoftClassPtr<UCreditWidget>(FSoftObjectPath(*CreditWidgetPath)).LoadSynchronous();
	if (CreditWidgetClass) {
		CreditWidget = CreateWidget<UCreditWidget>(GetWorld(), CreditWidgetClass);
	}
	
	// オーディオメニューウィジェット
	FString AudioMenuWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("AudioMenuWidgetPath"), AudioMenuWidgetPath, GGameIni);
	AudioMenuWidgetClass = TSoftClassPtr<UAudioMenuWidget>(FSoftObjectPath(*AudioMenuWidgetPath)).LoadSynchronous();
	if (AudioMenuWidgetClass) {
		AudioMenuWidget = CreateWidget<UAudioMenuWidget>(GetWorld(), AudioMenuWidgetClass);
	}

	// タイトルガイドウィジェット
	FString TitleGuideWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("TitleGuideWidgetPath"), TitleGuideWidgetPath, GGameIni);
	TitleGuideWidgetClass = TSoftClassPtr<UTitleGuideWidget>(FSoftObjectPath(*TitleGuideWidgetPath)).LoadSynchronous();
	if (TitleGuideWidgetClass) {
		TitleGuideWidget = CreateWidget<UTitleGuideWidget>(GetWorld(), TitleGuideWidgetClass);
	}

	// オーディオガイドウィジェット
	FString AudioGuideWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("AudioGuideWidgetPath"), AudioGuideWidgetPath, GGameIni);
	AudioGuideWidgetClass = TSoftClassPtr<UAudioGuideWidget>(FSoftObjectPath(*AudioGuideWidgetPath)).LoadSynchronous();
	if (AudioGuideWidgetClass) {
		AudioGuideWidget = CreateWidget<UAudioGuideWidget>(GetWorld(), AudioGuideWidgetClass);
	}
	
	// クレジットガイドウィジェット
	FString CreditGuideWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("CreditGuideWidgetPath"), CreditGuideWidgetPath, GGameIni);
	CreditGuideWidgetClass = TSoftClassPtr<UCreditGuideWidget>(FSoftObjectPath(*CreditGuideWidgetPath)).LoadSynchronous();
	if (CreditGuideWidgetClass) {
		CreditGuideWidget = CreateWidget<UCreditGuideWidget>(GetWorld(), CreditGuideWidgetClass);
	}
}

/// <summary>
/// 各ウィジェット表示準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleUiManager::PrepareOpenWidget() {

	//-----------------------------------------------------
	// 各ウィジェットのアニメーションフラグを初期化
	// 各ウィジェットごとの初期化処理
	//-----------------------------------------------------
	// タイトルウィジェット
	if (IsValid(TitleWidget)) {
		TitleWidget->ResetNowDisplay();
		TitleWidget->SetupWidget();
		TitleWidget->SetupNavigationOFF();
		TitleWidget->SetupEvent();
		TitleWidget->ResetAnimFlag();
	}

	// フェードウィジェット
	if (IsValid(FadeWidget)) {
		FadeWidget->ResetNowDisplay();
		FadeWidget->ResetAnimFlag();
		FadeWidget->SetImageRenderOpacity(Opacity);
	}

	// オーディオメニューウィジェット
	if (IsValid(AudioMenuWidget)) {
		AudioMenuWidget->ResetNowDisplay();
		AudioMenuWidget->SetupWidget();
	}

	// クレジットウィジェット
	if (IsValid(CreditWidget)) {
		CreditWidget->ResetNowDisplay();
	}

	// タイトルガイドウィジェット
	if (IsValid(TitleGuideWidget)) {
		TitleGuideWidget->ResetNowDisplay();
	}
}

/// <summary>
/// 各ウィジェット表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleUiManager::OpenWidget() {

	//-----------------------------------------------------
	// 各ウィジェットを表示
	// ※クレジットとオプションはここでは表示しない
	//-----------------------------------------------------
	// タイトルガイドウィジェット
	if (!IsValid(TitleGuideWidget)) {
		return;
	}
	TitleGuideWidget->Open();
	TitleGuideWidget->Hide();

	// タイトルウィジェット
	if (!IsValid(TitleWidget)) {
		return;
	}
	TitleWidget->Open();

	// フェードウィジェット
	if (!IsValid(FadeWidget)) {
		return;
	}
	FadeWidget->Open();
}

/// <summary>
/// 各ウィジェット非表示準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleUiManager::PrepareCloseWidget() {

	//-----------------------------------------------------
	// 各ウィジェットを非表示前準備処理
	//-----------------------------------------------------
	// タイトルガイドウィジェット
	if (!IsValid(TitleGuideWidget)) {
		return;
	}
	TitleGuideWidget->Close();

	// タイトルウィジェット
	if (!IsValid(TitleWidget)) {
		return;
	}
	TitleWidget->Close();

	// フェードウィジェット
	if (!IsValid(FadeWidget)) {
		return;
	}
	FadeWidget->Close();

	// フェードウィジェットを透明にしておく
	FadeWidget->SetImageRenderOpacity(Transparent);
}

/// <summary>
/// 各ウィジェット非表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleUiManager::CloseWidget() {

	//-----------------------------------------------------
	// 各ウィジェットを非表示
	// ※フェードウィジェットは非表示にしない
	//-----------------------------------------------------
	// タイトルウィジェット
	if (!IsValid(TitleWidget)) {
		return;
	}
	TitleWidget->Closed();

	// タイトルガイドウィジェット
	if (!IsValid(TitleGuideWidget)) {
		return;
	}
	TitleGuideWidget->Closed();
}

/// <summary>
/// タイトルウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> タイトルウィジェット </returns>
UTitleWidget* ATitleUiManager::GetTitleWidget() {

	return TitleWidget;
}

/// <summary>
/// フェードウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> フェードウィジェット </returns>
UFadeWidget* ATitleUiManager::GetFadeWidget() {

	return FadeWidget;
}

/// <summary>
/// クレジットウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> クレジットウィジェット </returns>
UCreditWidget* ATitleUiManager::GetCreditWidget() {

	return CreditWidget;
}

/// <summary>
/// オーディオメニューウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> オーディオメニューウィジェット </returns>
UAudioMenuWidget* ATitleUiManager::GetAudioMenuWidget() {

	return AudioMenuWidget;
}

/// <summary>
/// タイトルガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> タイトルガイドウィジェット </returns>
UTitleGuideWidget* ATitleUiManager::GetTitleGuideWidget() {

	return TitleGuideWidget;
}

/// <summary>
/// オーディオガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> オーディオガイドウィジェット </returns>
UAudioGuideWidget* ATitleUiManager::GetAudioGuideWidget() {

	return AudioGuideWidget;
}

/// <summary>
/// クレジットガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> クレジットガイドウィジェット </returns>
UCreditGuideWidget* ATitleUiManager::GetCreditGuideWidget() {

	return CreditGuideWidget;
}