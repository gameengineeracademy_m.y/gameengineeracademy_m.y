﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Title/GameMode/TitleGameMode.h"
#include "Title/Controller/TitleController.h"
#include "Title/UiManager/TitleUiManager.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ATitleGameMode::ATitleGameMode() {

	// Tickイベントの無効化
	PrimaryActorTick.bCanEverTick = false;

	HUDClass = ATitleUiManager::StaticClass();
	PlayerControllerClass = ATitleController::StaticClass();
	DefaultPawnClass = nullptr;
}

/// <summary>
/// 生成後実行イベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleGameMode::BeginPlay() {

}