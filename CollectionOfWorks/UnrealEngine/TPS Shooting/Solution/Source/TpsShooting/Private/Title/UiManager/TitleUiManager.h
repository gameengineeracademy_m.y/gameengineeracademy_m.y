﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Title/Widgets/TitleWidget.h"
#include "UiAssignmentWidget/CreditWidget/CreditWidget.h"
#include "UiAssignmentWidget/FadeWidget/FadeWidget.h"
#include "UiAssignmentWidget/OperateBaseWidget/OptionBaseWidget/AudioMenuWidget/AudioMenuWidget.h"
#include "UiAssignmentWidget/GuideWidget/TitleGuideWidget/TitleGuideWidget.h"
#include "UiAssignmentWidget/GuideWidget/AudioGuideWidget/AudioGuideWidget.h"
#include "UiAssignmentWidget/GuideWidget/CreditGuideWidget/CreditGuideWidget.h"
#include "TitleUiManager.generated.h"

/// <summary>
/// タイトルレベルのUIマネージャー処理
/// </summary>
UCLASS()
class ATitleUiManager : public AHUD
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ATitleUiManager();

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PreInitializeComponents() override;

	/// <summary>
	/// 各ウィジェット表示準備処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PrepareOpenWidget();

	/// <summary>
	/// 各ウィジェット表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OpenWidget();

	/// <summary>
	/// 各ウィジェット非表示準備処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PrepareCloseWidget();

	/// <summary>
	/// 各ウィジェット非表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CloseWidget();

	/// <summary>
	/// タイトルウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> タイトルウィジェット </returns>
	UTitleWidget* GetTitleWidget();

	/// <summary>
	/// フェードウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> フェードウィジェット </returns>
	UFadeWidget* GetFadeWidget();

	/// <summary>
	/// クレジットウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> クレジットウィジェット </returns>
	UCreditWidget* GetCreditWidget();

	/// <summary>
	/// オーディオメニューウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オーディオメニューウィジェット </returns>
	UAudioMenuWidget* GetAudioMenuWidget();

	/// <summary>
	/// タイトルガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> タイトルガイドウィジェット </returns>
	UTitleGuideWidget* GetTitleGuideWidget();

	/// <summary>
	/// オーディオガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オーディオガイドウィジェット </returns>
	UAudioGuideWidget* GetAudioGuideWidget();

	/// <summary>
	/// クレジットガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> クレジットガイドウィジェット </returns>
	UCreditGuideWidget* GetCreditGuideWidget();

private:

	/// <summary>
	/// タイトルウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UTitleWidget> TitleWidgetClass;

	/// <summary>
	/// タイトルウィジェット
	/// </summary>
	UPROPERTY()
	UTitleWidget* TitleWidget;

	/// <summary>
	/// フェードウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UFadeWidget> FadeWidgetClass;

	/// <summary>
	/// フェードウィジェット
	/// </summary>
	UPROPERTY()
	UFadeWidget* FadeWidget;

	/// <summary>
	/// クレジットウィジェットクラス
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	TSubclassOf<UCreditWidget> CreditWidgetClass;

	/// <summary>
	/// クレジットウィジェット
	/// </summary>
	UPROPERTY()
	UCreditWidget* CreditWidget;

	/// <summary>
	/// オーディオメニューウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UAudioMenuWidget> AudioMenuWidgetClass;

	/// <summary>
	/// オーディオメニューウィジェット
	/// </summary>
	UPROPERTY()
	UAudioMenuWidget* AudioMenuWidget;

	/// <summary>
	/// タイトルガイドウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UTitleGuideWidget> TitleGuideWidgetClass;

	/// <summary>
	/// タイトルガイドウィジェット
	/// </summary>
	UPROPERTY()
	UTitleGuideWidget* TitleGuideWidget;

	/// <summary>
	/// オーディオガイドウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UAudioGuideWidget> AudioGuideWidgetClass;

	/// <summary>
	/// オーディオガイドウィジェット
	/// </summary>
	UPROPERTY()
	UAudioGuideWidget* AudioGuideWidget;

	/// <summary>
	/// クレジットガイドウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UCreditGuideWidget> CreditGuideWidgetClass;

	/// <summary>
	/// クレジットガイドウィジェット
	/// </summary>
	UPROPERTY()
	UCreditGuideWidget* CreditGuideWidget;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// 不透明
	/// </summary>
	const float Opacity = 1.0f;

	/// <summary>
	/// 透明
	/// </summary>
	const float Transparent = 0.0f;
};
