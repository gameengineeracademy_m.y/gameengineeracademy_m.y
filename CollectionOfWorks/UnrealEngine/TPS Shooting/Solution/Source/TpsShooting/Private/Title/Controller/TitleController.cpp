﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Title/Controller/TitleController.h"
#include "GameFramework/PlayerInput.h"
#include "Kismet/GameplayStatics.h"
#include "../Public/TpsShootingGameInstance.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ATitleController::ATitleController()
	: AudioManagerClass()
	, AudioManager(nullptr)
	, ArrayInputType()
	, OldCurPosX(0.0f)
	, OldCurPosY(0.0f) {

}

/// <summary>
/// SetupinputComponent
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::SetupInputComponent() {

	Super::SetupInputComponent();

	//UE4のアサートマクロ
	check(InputComponent);

	if (!IsValid(InputComponent)) {
		return;
	}

	//------------------------------------------
	//キーバインドとアクションを設定
	//マウスカーソル表示設定
	//------------------------------------------
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Up", EKeys::Up));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Up", EKeys::W));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Down", EKeys::Down));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Down", EKeys::S));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Right", EKeys::Right));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Right", EKeys::D));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Left", EKeys::Left));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Left", EKeys::A));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Decide", EKeys::F));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Return", EKeys::RightMouseButton));
	
	InputComponent->BindAction("Up", IE_Pressed, this, &ATitleController::PressUp);
	InputComponent->BindAction("Down", IE_Pressed, this, &ATitleController::PressDown);
	InputComponent->BindAction("Left", IE_Pressed, this, &ATitleController::PressLeft);
	InputComponent->BindAction("Right", IE_Pressed, this, &ATitleController::PressRight);
	InputComponent->BindAction("Decide", IE_Pressed, this, &ATitleController::PressDecide);
	InputComponent->BindAction("Return", IE_Pressed, this, &ATitleController::PressReturn);
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::BeginPlay() {

	Super::BeginPlay();

	// 現在のマウスカーソルの位置を取得しておく
	GetMousePosition(OldCurPosX, OldCurPosY);

	bShowMouseCursor = true;

	// オーディオマネージャの生成
	CreateAudioManager();
}

/// <summary>
/// オーディオマネージャの生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::CreateAudioManager() {

	FString AudioManagerPath;
	GConfig->GetString(TEXT("ActorSettings"), TEXT("AudioManagerPath"), AudioManagerPath, GGameIni);

	AudioManagerClass = TSoftClassPtr<AAudioManager>(FSoftObjectPath(*AudioManagerPath)).LoadSynchronous();
	if (!IsValid(AudioManagerClass)) {
		return;
	}
	AudioManager = GetWorld()->SpawnActor<AAudioManager>(AudioManagerClass);
}

/// <summary>
/// 各ウィジェットの表示準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::PrepareOpenWidget() {

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ATitleUiManager* UiManager = Cast<ATitleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 各ウィジェットの表示準備処理
	UiManager->PrepareOpenWidget();
}

/// <summary>
/// 各ウィジェットの表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::OpenWidget() {

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ATitleUiManager* UiManager = Cast<ATitleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 各ウィジェットの表示処理
	UiManager->OpenWidget();
}

/// <summary>
/// 各ウィジェットの非表示準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::PrepareCloseWidget() {

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ATitleUiManager* UiManager = Cast<ATitleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 各ウィジェットの非表示準備処理
	UiManager->PrepareCloseWidget();
}

/// <summary>
/// 各ウィジェットの非表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::CloseWidget() {

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ATitleUiManager* UiManager = Cast<ATitleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 非表示処理実行
	UiManager->CloseWidget();
}

/// <summary>
/// タイトルウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> タイトルウィジェット </returns>
UTitleWidget* ATitleController::GetTitleWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ATitleUiManager* UiManager = Cast<ATitleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}
	// タイトルウィジェット取得
	return UiManager->GetTitleWidget();
}

/// <summary>
/// フェードウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> フェードウィジェット </returns>
UFadeWidget* ATitleController::GetFadeWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ATitleUiManager* UiManager = Cast<ATitleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}
	// フェードウィジェット取得
	return UiManager->GetFadeWidget();
}

/// <summary>
/// クレジットウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> クレジットウィジェット </returns>
UCreditWidget* ATitleController::GetCreditWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ATitleUiManager* UiManager = Cast<ATitleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}
	// クレジットウィジェット取得
	return UiManager->GetCreditWidget();
}

/// <summary>
/// オーディオメニューウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> オーディオメニューウィジェット </returns>
UAudioMenuWidget* ATitleController::GetAudioMenuWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ATitleUiManager* UiManager = Cast<ATitleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}
	// オーディオメニューウィジェット取得
	return UiManager->GetAudioMenuWidget();
}

/// <summary>
/// タイトルガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> タイトルガイドウィジェット </returns>
UTitleGuideWidget* ATitleController::GetTitleGuideWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ATitleUiManager* UiManager = Cast<ATitleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}
	// タイトルガイドウィジェット取得
	return UiManager->GetTitleGuideWidget();
}

/// <summary>
/// オーディオガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> オーディオガイドウィジェット </returns>
UAudioGuideWidget* ATitleController::GetAudioGuideWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ATitleUiManager* UiManager = Cast<ATitleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}
	// オーディオガイドウィジェット取得
	return UiManager->GetAudioGuideWidget();
}

/// <summary>
/// クレジットガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> クレジットガイドウィジェット </returns>
UCreditGuideWidget* ATitleController::GetCreditGuideWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ATitleUiManager* UiManager = Cast<ATitleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}
	// クレジットガイドウィジェット取得
	return UiManager->GetCreditGuideWidget();
}

/// <summary>
/// オーディオマネージャの取得
/// </summary>
/// <param name=""></param>
/// <returns> オーディオマネージャ </returns>
AAudioManager* ATitleController::GetAudioManager() {

	return AudioManager;
}

/// <summary>
/// インプットの種類を追加
/// </summary>
/// <param name="InputType"> 入力の種類 </param>
/// <returns></returns>
void ATitleController::AddInputType(ETitleInputType InputType) {

	ArrayInputType.Push(InputType);
	SwitchKeyBindAction(InputType);
}

/// <summary>
/// インプットの種類を削除
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::DeleteInputType() {

	ArrayInputType.Pop();

	ETitleInputType InputType = GetInputType();
	if (InputType == ETitleInputType::None) {
		InputType = ETitleInputType::Title;
		AddInputType(InputType);
	}

	SwitchKeyBindAction(InputType);
}

/// <summary>
/// キーバインドアクションを切り替える
/// </summary>
/// <param name="InputType"> 選択したボタンの種類 </param>
/// <returns></returns>
void ATitleController::SwitchKeyBindAction(ETitleInputType InputType) {

	switch (InputType) {
	case ETitleInputType::Title: {
		//-------------------------------------
		//タイトル画面
		//-------------------------------------
		SetupKeyBindActionTitle();
		break;
	}
	case ETitleInputType::Audio: {
		//-------------------------------------
		//オプション画面
		//-------------------------------------
		SetupKeyBindActionOption();
		break;
	}
	case ETitleInputType::Credit: {
		//-------------------------------------
		//クレジット画面
		//-------------------------------------
		SetupKeyBindActionCredit();
		break;
	}
	}
}

/// <summary>
/// タイトル画面用のキーバインドアクション設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::SetupKeyBindActionTitle() {

	// 一度全てのキーバインドを外す
	InputComponent->ClearActionBindings();
	// キーのバインド設定
	InputComponent->BindAction("Up", IE_Pressed, this, &ATitleController::PressUp);
	InputComponent->BindAction("Down", IE_Pressed, this, &ATitleController::PressDown);
	InputComponent->BindAction("Left", IE_Pressed, this, &ATitleController::PressLeft);
	InputComponent->BindAction("Right", IE_Pressed, this, &ATitleController::PressRight);
	InputComponent->BindAction("Decide", IE_Pressed, this, &ATitleController::PressDecide);
	InputComponent->BindAction("Return", IE_Pressed, this, &ATitleController::PressReturn);
}

/// <summary>
/// オプション画面用のキーバインドアクション設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::SetupKeyBindActionOption() {

	// 一度全てのキーバインドを外す
	InputComponent->ClearActionBindings();
	// キーのバインド設定
	InputComponent->BindAction("Up", IE_Pressed, this, &ATitleController::PressUp);
	InputComponent->BindAction("Down", IE_Pressed, this, &ATitleController::PressDown);
	InputComponent->BindAction("Left", IE_Repeat, this, &ATitleController::PressLeft);
	InputComponent->BindAction("Left", IE_Pressed, this, &ATitleController::PressLeft);
	InputComponent->BindAction("Right", IE_Repeat, this, &ATitleController::PressRight);
	InputComponent->BindAction("Right", IE_Pressed, this, &ATitleController::PressRight);
	InputComponent->BindAction("Decide", IE_Pressed, this, &ATitleController::PressDecide);
	InputComponent->BindAction("Return", IE_Pressed, this, &ATitleController::PressReturn);
}

/// <summary>
/// クレジット画面用のキーバインドアクション設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::SetupKeyBindActionCredit() {

	// 一度全てのキーバインドを外す
	InputComponent->ClearActionBindings();
	// キーのバインド設定
	InputComponent->BindAction("Up", IE_Repeat, this, &ATitleController::PressUp);
	InputComponent->BindAction("Up", IE_Pressed, this, &ATitleController::PressUp);
	InputComponent->BindAction("Down", IE_Repeat, this, &ATitleController::PressDown);
	InputComponent->BindAction("Down", IE_Pressed, this, &ATitleController::PressDown);
	InputComponent->BindAction("Left", IE_Pressed, this, &ATitleController::PressLeft);
	InputComponent->BindAction("Right", IE_Pressed, this, &ATitleController::PressRight);
	InputComponent->BindAction("Decide", IE_Pressed, this, &ATitleController::PressDecide);
	InputComponent->BindAction("Return", IE_Pressed, this, &ATitleController::PressReturn);
}

/// <summary>
/// 設定されたインプットが存在するかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:存在する </returns>
bool ATitleController::IsExistsInput() {

	return ArrayInputType.Num() > Zero;
}

/// <summary>
/// インプットの種類を取得
/// </summary>
/// <param name=""></param>
/// <returns> インプットの種類 </returns>
ETitleInputType ATitleController::GetInputType() {

	if (!IsExistsInput()) {
		return ETitleInputType::None;
	}

	return ArrayInputType[ArrayInputType.Num() - One];
}

/// <summary>
/// 決定ボタン押下処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::PressDecide() {

	//-----------------------------------------------
	//選択中のボタンを決定したことにする
	//-----------------------------------------------
	
	if (!IsExistsInput()) {
		return;
	}

	ETitleInputType InputType = GetInputType();
	switch (InputType) {
	case ETitleInputType::Title: {

		//タイトルウィジェットを取得
		UTitleWidget* TitleWidget = GetTitleWidget();
		if (!IsValid(TitleWidget)) {
			return;
		}
		TitleWidget->Decide();
		break;
	}
	case ETitleInputType::Audio: {

		//オーディオメニューウィジェットを取得
		UAudioMenuWidget* AudioMenuWidget = GetAudioMenuWidget();
		if (!IsValid(AudioMenuWidget)) {
			return;
		}
		AudioMenuWidget->PressDecide();
		break;
	}
	}
}

/// <summary>
/// 戻るボタン押下処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::PressReturn() {

	//-----------------------------------------------
	//選択中のボタンをなしに変更、タイトル画面に戻る
	//タイトル画面用のキーバインドアクションに切り替える
	//-----------------------------------------------

	if (!IsExistsInput()) {
		return;
	}

	ETitleInputType InputType = GetInputType();
	switch (InputType) {
	case ETitleInputType::Audio: {

		//オーディオメニューウィジェットを取得
		UAudioMenuWidget* AudioMenuWidget = GetAudioMenuWidget();
		if (!IsValid(AudioMenuWidget)) {
			return;
		}
		AudioMenuWidget->PressReturn();
		break;
	}
	case ETitleInputType::Credit: {

		//クレジットウィジェットを取得
		UCreditWidget* CreditWidget = GetCreditWidget();
		if (!IsValid(CreditWidget)) {
			return;
		}
		CreditWidget->PressReturn();
		break;
	}
	}
}

/// <summary>
/// 上方向ボタン押下処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::PressUp() {

	//-------------------------------------
	//Wキーまたは上矢印キーで処理実行
	//-------------------------------------
	if (!IsExistsInput()) {
		return;
	}

	ETitleInputType InputType = GetInputType();
	switch (InputType) {
	case ETitleInputType::Title: {

		//タイトルウィジェットを取得
		UTitleWidget* TitleWidget = GetTitleWidget();
		if (!IsValid(TitleWidget)) {
			return;
		}
		TitleWidget->PressUp();
		break;
	}
	case ETitleInputType::Audio: {

		//オーディオメニューウィジェットを取得
		UAudioMenuWidget* AudioMenuWidget = GetAudioMenuWidget();
		if (!IsValid(AudioMenuWidget)) {
			return;
		}
		AudioMenuWidget->PressUp();
		break;
	}
	case ETitleInputType::Credit: {

		//クレジットウィジェットを取得
		UCreditWidget* CreditWidget = GetCreditWidget();
		if (!IsValid(CreditWidget)) {
			return;
		}
		CreditWidget->ScrollUp();
		break;
	}
	}
}

/// <summary>
/// 下方向ボタン押下処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::PressDown() {

	//-------------------------------------
	//Sキーまたは下矢印キーで処理実行
	//-------------------------------------
	if (!IsExistsInput()) {
		return;
	}

	ETitleInputType InputType = GetInputType();
	switch (InputType) {
	case ETitleInputType::Title: {

		//タイトルウィジェットを取得
		UTitleWidget* TitleWidget = GetTitleWidget();
		if (!IsValid(TitleWidget)) {
			return;
		}
		TitleWidget->PressDown();
		break;
	}
	case ETitleInputType::Audio: {

		//オーディオメニューウィジェットを取得
		UAudioMenuWidget* AudioMenuWidget = GetAudioMenuWidget();
		if (!IsValid(AudioMenuWidget)) {
			return;
		}
		AudioMenuWidget->PressDown();
		break;
	}
	case ETitleInputType::Credit: {

		//クレジットウィジェットを取得
		UCreditWidget* CreditWidget = GetCreditWidget();
		if (!IsValid(CreditWidget)) {
			return;
		}
		CreditWidget->ScrollDown();
		break;
	}
	}
}

/// <summary>
/// 左方向ボタン押下処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::PressLeft() {

	//-------------------------------------
	//Aキーまたは左矢印キーで処理実行
	//-------------------------------------
	if (!IsExistsInput()) {
		return;
	}
	
	ETitleInputType InputType = GetInputType();
	switch (InputType) {
	case ETitleInputType::Audio: {

		//オーディオメニューウィジェットを取得
		UAudioMenuWidget* AudioMenuWidget = GetAudioMenuWidget();
		if (!IsValid(AudioMenuWidget)) {
			return;
		}
		AudioMenuWidget->AdjustVolumeDown();
		break;
	}
	}
}

/// <summary>
/// 右方向ボタン押下処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::PressRight() {

	//-------------------------------------
	//Dキーまたは右矢印キーで処理実行
	//-------------------------------------
	if (!IsExistsInput()) {
		return;
	}

	ETitleInputType InputType = GetInputType();
	switch (InputType) {
	case ETitleInputType::Audio: {

		//オーディオメニューウィジェットを取得
		UAudioMenuWidget* AudioMenuWidget = GetAudioMenuWidget();
		if (!IsValid(AudioMenuWidget)) {
			return;
		}
		AudioMenuWidget->AdjustVolumeUp();
		break;
	}
	}
}

/// <summary>
/// マウスカーソルが移動したかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:移動した </returns>
bool ATitleController::IsMoveMouseCursor() {

	float CurPosX = NotMoveValue;
	float CurPosY = NotMoveValue;

	// 現在のマウスカーソルの位置を取得
	GetMousePosition(CurPosX, CurPosY);
	// 前回のマウスカーソルの位置との差分を算出
	float DifferX = OldCurPosX - CurPosX;
	float DifferY = OldCurPosY - CurPosY;

	OldCurPosX = CurPosX;
	OldCurPosY = CurPosY;

	if (FMath::Abs(DifferX) > NotMoveValue ||
		FMath::Abs(DifferX) > NotMoveValue) {

		return true;
	}

	return false;
}