﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "Audio/DataAssets/AudioVolume/AudioVolumeData.h"
#include "AudioSaveGame.generated.h"


/// <summary>
/// オーディオセーブゲームクラス
/// </summary>
UCLASS()
class UAudioSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name="ObjectInitializer"> イニシャライザー </param>
	/// <returns></returns>
	UAudioSaveGame(const FObjectInitializer& ObjectInitializer);

	/// <summary>
	/// 音量データのセーブデータをセット
	/// </summary>
	/// <param name="VolumeData"> 音量データ </param>
	/// <returns></returns>
	void SetSaveAudioVolumeData(FAudioVolumeData VolumeData);

	/// <summary>
	/// 音量データのセーブデータを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 音量データ </returns>
	FAudioVolumeData GetSaveAudioVolumeData();

private:
	
	/// <summary>
	/// 音量データ
	/// </summary>
	UPROPERTY()
	FAudioVolumeData SaveAudioVolumeData;
};
