﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/DataTable.h"
#include "Audio/DataAssets/AudioVolume/AudioVolumeData.h"
#include "Audio/DataTables/Structs/SoundData.h"
#include "Components/AudioComponent.h"
#include "Sound/SoundCue.h"
#include "../Public/TpsShootingGameInstance.h"
#include "Audio/SaveGame/AudioSaveGame.h"
#include "AudioManager.generated.h"


/// <summary>
/// BGM実行フェーズ
/// </summary>
UENUM()
enum class EBGMPhaseType {
	None,     // 何もしない
	Play,     // 実行中
	Stop,     // 停止中
	Finish,   // 終了
	MaxTypeIndex
};

/// <summary>
/// BGMの種類
/// </summary>
UENUM(BlueprintType)
enum class EBGMType : uint8 {
	None,     // なし
	Title,    // タイトル
	Game,     // ゲーム中
	Battle,   // バトル中
	Result,   // リザルト
	MaxTypeIndex
};

/// <summary>
/// BGMのデータ
/// </summary>
USTRUCT(BlueprintType)
struct FBGMDataTable : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// 使用するレベルの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AudioManager")
	ELevelType LevelType;

	/// <summary>
	/// BGMの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AudioManager")
	EBGMType BGMType;

	/// <summary>
	/// BGM
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AudioManager")
	USoundCue* BGMSound;
};

/// <summary>
/// オーディオマネージャ
/// </summary>
UCLASS()
class AAudioManager : public AActor
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AAudioManager();

	/// <summary>
	/// BGMの初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeBGM();

	/// <summary>
	/// オーディオボリュームをセット
	/// </summary>
	/// <param name="Volume"> オーディオボリューム </param>
	/// <returns></returns>
	void SetAudioVolume(FAudioVolumeData Volume);

	/// <summary>
	/// オーディオボリュームの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オーディオボリューム </returns>
	FAudioVolumeData GetAudioVolume();

	/// <summary>
	/// BGMの設定
	/// </summary>
	/// <param name="BGMType"> BGMの種類 </param>
	/// <returns></returns>
	void SetBGMSound(EBGMType BGMType);

	/// <summary>
	/// 次に再生するBGMをセット
	/// </summary>
	/// <param name="BGMType"> BGMの種類 </param>
	/// <returns></returns>
	void SetNextBGMSound(EBGMType BGMType);

	/// <summary>
	/// BGMの再生
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PlaySoundBGM();

	/// <summary>
	/// BGMの停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StopSoundBGM();

	/// <summary>
	/// BGMの終了
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void FinishSoundBGM();

	/// <summary>
	/// BGMのフェードイン
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void FadeInBGM();

	/// <summary>
	/// BGMのフェードアウト
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void FadeOutBGM();

	/// <summary>
	/// 再生中のBGMが戦闘曲かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:戦闘曲 </returns>
	bool IsPlayingBattleBGM();

	/// <summary>
	/// 音量ボリュームの保存
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "AudioManager")
	void SaveAudioVolume();

	/// <summary>
	/// ボリューム設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, Category = "AudioManager")
	void SetupVolume();

	/// <summary>
	/// ボリュームを更新する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, Category = "AudioManager")
	void UpdateVolume();

	/// <summary>
	/// BGM再生
	/// </summary>
	/// <param name="BGMType"> BGMの種類 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "AudioManager")
	void PlayBGM(EBGMType BGMType);

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

private:

	/// <summary>
	/// 現在のBGMフェーズが終了かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:終了 </returns>
	bool IsFinish();

	/// <summary>
	/// 現在のBGMフェーズを変更
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentBGMPhaseType(EBGMPhaseType PhaseType);

	/// <summary>
	/// ボリューム設定
	/// </summary>
	/// <param name="_AudioComponent"> オーディオコンポーネント </param>
	/// <returns></returns>
	UFUNCTION()
	void FinishedPlayBGM(UAudioComponent* _AudioComponent);

	/// <summary>
	/// 音量セーブクラスのロード
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void LoadAudioSaveGameClass();

	/// <summary>
	/// 音量ボリュームのロード
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void LoadAudioVolume();

	/// <summary>
	/// 音量ボリュームデータの存在確認
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CheckAudioVolumeData();

	/// <summary>
	/// ボリューム設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void SetupVolume_Implementation();

	/// <summary>
	/// ボリュームを保存する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void UpdateVolume_Implementation();

	/// <summary>
	/// BGM再生
	/// </summary>
	/// <param name="BGMType"> BGMの種類 </param>
	/// <returns></returns>
	virtual void PlayBGM_Implementation(EBGMType BGMType);

protected:

	/// <summary>
	/// BGMデータテーブル
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "AudioManager")
	UDataTable* BGMDataTable;

	/// <summary>
	/// 音量データ
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "AudioManager")
	FAudioVolumeData AudioVolume;

	/// <summary>
	/// フェードイン時間
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "AudioManager")
	float FadeInTime;

	/// <summary>
	/// フェードアウト時間
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "AudioManager")
	float FadeOutTime;

	/// <summary>
	/// フェードボリュームレベル
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "AudioManager")
	float FadeInVolumeLevel;

	/// <summary>
	/// フェードアウトボリュームレベル
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "AudioManager")
	float FadeOutVolumeLevel;

private:

	/// <summary>
	/// 現在のBGMフェーズの種類
	/// </summary>
	EBGMPhaseType CurrentBGMPhaseType;

	/// <summary>
	/// BGM情報
	/// </summary>
	UPROPERTY()
	TMap<EBGMType, USoundCue*> BGMList;

	/// <summary>
	/// オーディオコンポーネント
	/// </summary>
	UPROPERTY()
	UAudioComponent* AudioComp;

	/// <summary>
	/// 現在再生しているBGMの種類
	/// </summary>
	EBGMType CurrentBGMType;

	/// <summary>
	/// 次に再生するBGMの種類
	/// </summary>
	EBGMType NextBGMType;

	/// <summary>
	/// セーブゲームクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UAudioSaveGame> AudioSaveGameClass;

	/// <summary>
	/// セーブゲーム
	/// </summary>
	UPROPERTY()
	UAudioSaveGame* AudioSaveGame;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// サウンドデータ セーブスロット名
	/// </summary>
	const FString SoundVolumeDataSlotName = "SoundVolumeDataSlot";

	/// <summary>
	/// サウンドデータ スロットインデックス
	/// </summary>
	const int32 SoundVolumeDataSlotIndex = 0;

	/// <summary>
	/// SEの初期音量
	/// </summary>
	const float InitSEVolume = 0.1f;

	/// <summary>
	/// BGMの初期音量
	/// </summary>
	const float InitBGMVolume = 0.3f;
};