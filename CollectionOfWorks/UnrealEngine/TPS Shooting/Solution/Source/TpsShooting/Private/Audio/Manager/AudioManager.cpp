﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Audio/Manager/AudioManager.h"
#include "Kismet/GameplayStatics.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AAudioManager::AAudioManager()
	: BGMDataTable(nullptr)
	, AudioVolume()
	, FadeInTime(0.0f)
	, FadeOutTime(0.0f)
	, FadeInVolumeLevel(0.0f)
	, FadeOutVolumeLevel(0.0f)
	, CurrentBGMPhaseType(EBGMPhaseType::None)
	, BGMList()
	, AudioComp(nullptr)
	, CurrentBGMType(EBGMType::None)
	, NextBGMType(EBGMType::None)
	, AudioSaveGameClass()
	, AudioSaveGame(nullptr) {

	// Tick処理 無効化
 	PrimaryActorTick.bCanEverTick = false;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AAudioManager::BeginPlay() {

	Super::BeginPlay();
	
	CurrentBGMPhaseType = EBGMPhaseType::None;
	CurrentBGMType = EBGMType::None;
	NextBGMType = EBGMType::None;

	// セーブデータから音量データを取得
	// 音量設定を行う　※BP側で実行
	LoadAudioSaveGameClass();
	CheckAudioVolumeData();
	SetupVolume();

	// BGMの初期化処理
	InitializeBGM();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AAudioManager::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}

/// <summary>
/// BGMの初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AAudioManager::InitializeBGM() {

	//---------------------------------------
	// 現在のレベルを取得
	//---------------------------------------
	UTpsShootingGameInstance* GameInstance = Cast<UTpsShootingGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (!IsValid(GameInstance)) {
		return;
	}
	ELevelType CurrentLevelType = GameInstance->GetCurrentLevel();

	if (!IsValid(BGMDataTable)) {
		return;
	}
	TArray<FName> RowNames = BGMDataTable->GetRowNames();
	for (FName RowName : RowNames) {

		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		FBGMDataTable* BGMData = BGMDataTable->FindRow<FBGMDataTable>(RowName, "");
		if (BGMData->LevelType != CurrentLevelType) {
			continue;
		}

		// BGMをリストに格納
		BGMList.Add(BGMData->BGMType, BGMData->BGMSound);
	}
}

/// <summary>
/// オーディオボリュームをセット
/// </summary>
/// <param name="Volume"> オーディオボリューム </param>
/// <returns></returns>
void AAudioManager::SetAudioVolume(FAudioVolumeData Volume) {

	AudioVolume = Volume;
}

/// <summary>
/// オーディオボリュームの取得
/// </summary>
/// <param name=""></param>
/// <returns> オーディオボリューム </returns>
FAudioVolumeData AAudioManager::GetAudioVolume() {

	return AudioVolume;
}

/// <summary>
/// BGMの設定
/// </summary>
/// <param name="BGMType"> BGMの種類 </param>
/// <returns></returns>
void AAudioManager::SetBGMSound(EBGMType BGMType) {

	if (!BGMList.Contains(BGMType)) {
		return;
	}

	if (BGMType == EBGMType::None) {
		CurrentBGMType = BGMType;
		return;
	}

	CurrentBGMType = BGMType;
	AudioComp = UGameplayStatics::SpawnSound2D(GetWorld(), BGMList[BGMType]);
	ChangeCurrentBGMPhaseType(EBGMPhaseType::Play);

	if (AudioComp->OnAudioFinishedNative.IsBound()) {
		return;
	}
	AudioComp->OnAudioFinishedNative.AddUFunction(this, "FinishedPlayBGM");
}

/// <summary>
/// 次に再生するBGMをセット
/// </summary>
/// <param name="BGMType"> BGMの種類 </param>
/// <returns></returns>
void AAudioManager::SetNextBGMSound(EBGMType BGMType) {

	if (!BGMList.Contains(BGMType)) {
		return;
	}

	// 次の指定楽曲がリザルトBGMならリザルトBGMを優先する
	switch (NextBGMType) {
	case EBGMType::Result: {

		break;
	}
	default: {

		NextBGMType = BGMType;
		break;
	}
	}
}

/// <summary>
/// BGMの再生
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AAudioManager::PlaySoundBGM() {

	if (!IsValid(AudioComp)) {
		return;
	}
	AudioComp->Play();

	ChangeCurrentBGMPhaseType(EBGMPhaseType::Play);
}

/// <summary>
/// BGMの停止
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AAudioManager::StopSoundBGM() {

	if (!IsValid(AudioComp)) {
		return;
	}
	AudioComp->Stop();

	ChangeCurrentBGMPhaseType(EBGMPhaseType::Stop);
}

/// <summary>
/// BGMの終了
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AAudioManager::FinishSoundBGM() {

	ChangeCurrentBGMPhaseType(EBGMPhaseType::Finish);

	if (!IsValid(AudioComp)) {
		return;
	}
	AudioComp->Stop();
}

/// <summary>
/// BGMのフェードイン
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AAudioManager::FadeInBGM() {

	if (!IsValid(AudioComp)) {
		return;
	}
	AudioComp->FadeIn(FadeInTime, FadeInVolumeLevel);

	ChangeCurrentBGMPhaseType(EBGMPhaseType::Play);
}

/// <summary>
/// BGMのフェードアウト
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AAudioManager::FadeOutBGM() {

	if (!IsValid(AudioComp)) {
		return;
	}
	AudioComp->FadeOut(FadeOutTime, FadeOutVolumeLevel);

	ChangeCurrentBGMPhaseType(EBGMPhaseType::Finish);
}

/// <summary>
/// 再生中のBGMが戦闘曲かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:戦闘曲 </returns>
bool AAudioManager::IsPlayingBattleBGM() {

	return CurrentBGMType == EBGMType::Battle;
}

/// <summary>
/// 現在のBGMフェーズが終了かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:終了 </returns>
bool AAudioManager::IsFinish() {

	return CurrentBGMPhaseType == EBGMPhaseType::Finish;
}

/// <summary>
/// 現在のBGMフェーズを変更
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void AAudioManager::ChangeCurrentBGMPhaseType(EBGMPhaseType PhaseType) {

	CurrentBGMPhaseType = PhaseType;
}

/// <summary>
/// ボリューム設定
/// </summary>
/// <param name="_AudioComponent"> オーディオコンポーネント </param>
/// <returns></returns>
void AAudioManager::FinishedPlayBGM(UAudioComponent* _AudioComponent) {

	if (!IsFinish()) {
		return;
	}

	_AudioComponent->DestroyComponent(true);
	SetBGMSound(NextBGMType);

	NextBGMType = EBGMType::None;
}

/// <summary>
/// 音量セーブクラスのロード
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AAudioManager::LoadAudioSaveGameClass() {

	FString Path;
	GConfig->GetString(TEXT("SaveGameSetting"), TEXT("AudioSaveGamePath"), Path, GGameIni);
	AudioSaveGameClass = TSoftClassPtr<UAudioSaveGame>(FSoftObjectPath(*Path)).LoadSynchronous();
}

/// <summary>
/// 音量ボリュームデータの存在確認
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AAudioManager::CheckAudioVolumeData() {

	bool bCheck = UGameplayStatics::DoesSaveGameExist(SoundVolumeDataSlotName, SoundVolumeDataSlotIndex);
	if (bCheck) {

		LoadAudioVolume();
	}
	else {

		AudioVolume.Se = InitSEVolume;
		AudioVolume.Bgm = InitBGMVolume;
		SaveAudioVolume();
	}
}

/// <summary>
/// 音量ボリュームのロード
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AAudioManager::LoadAudioVolume() {

	AudioSaveGame = Cast<UAudioSaveGame>(UGameplayStatics::LoadGameFromSlot(SoundVolumeDataSlotName, SoundVolumeDataSlotIndex));
	if (!IsValid(AudioSaveGame)) {
		UE_LOG(LogTemp, Warning, TEXT("LoadAudioVolume LoadError!"))
		return;
	}
	AudioVolume = AudioSaveGame->GetSaveAudioVolumeData();

	UE_LOG(LogTemp, Warning, TEXT("LoadAudioVolume SEVolume : %f"), AudioVolume.Se)
	UE_LOG(LogTemp, Warning, TEXT("LoadAudioVolume BGMVolume : %f"), AudioVolume.Bgm)
}

/// <summary>
/// 音量ボリュームの保存
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AAudioManager::SaveAudioVolume() {

	if (!IsValid(AudioSaveGame)) {

		if (!AudioSaveGameClass) {
			return;
		}
		AudioSaveGame = Cast<UAudioSaveGame>(UGameplayStatics::CreateSaveGameObject(AudioSaveGameClass));
		if (!IsValid(AudioSaveGame)) {
			UE_LOG(LogTemp, Warning, TEXT("CreateSaveGameObject CreateError!"))
			return;
		}
	}

	AudioSaveGame->SetSaveAudioVolumeData(AudioVolume);

	bool bCheck = UGameplayStatics::SaveGameToSlot(AudioSaveGame, SoundVolumeDataSlotName, SoundVolumeDataSlotIndex);
	if (!bCheck) {
		UE_LOG(LogTemp, Warning, TEXT("SaveAudioVolume SaveError!"))
	}
	UE_LOG(LogTemp, Warning, TEXT("SaveAudioVolume SEVolume : %f"), AudioVolume.Se)
	UE_LOG(LogTemp, Warning, TEXT("SaveAudioVolume BGMVolume : %f"), AudioVolume.Bgm)
	UE_LOG(LogTemp, Warning, TEXT("SaveAudioVolume SaveSuccess!"))
}

/// <summary>
/// ボリューム設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AAudioManager::SetupVolume_Implementation() {

}

/// <summary>
/// ボリュームを保存する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AAudioManager::UpdateVolume_Implementation() {

}

/// <summary>
/// BGM再生
/// </summary>
/// <param name="BGMType"> BGMの種類 </param>
/// <returns></returns>
void AAudioManager::PlayBGM_Implementation(EBGMType BGMType) {

}