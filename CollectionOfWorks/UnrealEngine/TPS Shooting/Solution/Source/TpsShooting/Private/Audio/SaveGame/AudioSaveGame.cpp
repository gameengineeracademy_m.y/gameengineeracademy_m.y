﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Audio/SaveGame/AudioSaveGame.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name="ObjectInitializer"> イニシャライザー </param>
/// <returns></returns>
UAudioSaveGame::UAudioSaveGame(const FObjectInitializer& ObjectInitializer) {

}

/// <summary>
/// 音量データの設定
/// </summary>
/// <param name="VolumeData"> 音量データ </param>
/// <returns></returns>
void UAudioSaveGame::SetSaveAudioVolumeData(FAudioVolumeData VolumeData) {

	SaveAudioVolumeData = VolumeData;

	UE_LOG(LogTemp, Warning, TEXT("SaveAudioVolume SEVolume : %f"), SaveAudioVolumeData.Se)
	UE_LOG(LogTemp, Warning, TEXT("SaveAudioVolume BGMVolume : %f"), SaveAudioVolumeData.Bgm)
}

/// <summary>
/// 音量データのセーブデータを取得
/// </summary>
/// <param name=""></param>
/// <returns> 音量データ </returns>
FAudioVolumeData UAudioSaveGame::GetSaveAudioVolumeData() {

	return SaveAudioVolumeData;
}