﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Engine/DataTable.h"
#include "AudioVolumeData.generated.h"

/// <summary>
/// オーディオボリューム用構造体
/// </summary>
USTRUCT(BlueprintType)
struct FAudioVolumeData {

	GENERATED_BODY()

public:

	/// <summary>
	/// 効果音の大きさ
	/// </summary>
	UPROPERTY(BlueprintReadWrite, meta = (ClampMin = 0.0f, ClampMax = 1.0f))
	float Se;

	/// <summary>
	/// BGMの大きさ
	/// </summary>
	UPROPERTY(BlueprintReadWrite, meta = (ClampMin = 0.0f, ClampMax = 1.0f))
	float Bgm;
};
