﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Battle/Widgets/BattleHUDItem/BattleHUDItemBase.h"
#include "BattleHUDWeaponIcon.generated.h"

/// <summary>
/// バトルHUDアイテム武器アイコンクラス
/// </summary>
UCLASS()
class UBattleHUDWeaponIcon : public UBattleHUDItemBase
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UBattleHUDWeaponIcon();

	/// <summary>
	/// 武器切り替え開始アニメーション
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleHUDWeaponIcon")
	void PlaySwitchStartWeaponAnimation();

	/// <summary>
	/// 武器切り替え終了アニメーション
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleHUDWeaponIcon")
	void PlaySwitchEndWeaponAnimation();

private:

	/// <summary>
	/// 武器切り替え開始アニメーション
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PlaySwitchStartWeaponAnimation_Implementation();

	/// <summary>
	/// 武器切り替え終了アニメーション
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PlaySwitchEndWeaponAnimation_Implementation();
};
