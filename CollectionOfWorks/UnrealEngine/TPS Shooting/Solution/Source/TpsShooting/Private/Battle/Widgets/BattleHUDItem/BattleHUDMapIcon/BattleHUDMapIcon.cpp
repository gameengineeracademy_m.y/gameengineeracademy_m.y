﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Widgets/BattleHUDItem/BattleHUDMapIcon/BattleHUDMapIcon.h"


/// <summary>
/// マップアイコンの種類のセット
/// </summary>
/// <param name="IconType"> マップアイコンの種類 </param>
/// <returns></returns>
void UBattleHUDMapIcon::SetMapIconType(EMapIconType IconType) {

	MapIconType = IconType;
}

/// <summary>
/// 表示エリアのセット
/// </summary>
/// <param name="Type"> エリアの種類 </param>
/// <returns></returns>
void UBattleHUDMapIcon::SetStageType(EStageType Type) {

	StageType = Type;
}

/// <summary>
/// 表示エリアの取得
/// </summary>
/// <param name=""></param>
/// <returns> エリアの種類 </returns>
EStageType UBattleHUDMapIcon::GetStageType() {

	return StageType;
}

/// <summary>
/// 紐づけ番号のセット
/// </summary>
/// <param name="Index"> 紐づけ番号 </param>
/// <returns></returns>
void UBattleHUDMapIcon::SetObjectIndex(int32 Index) {

	ObjectIndex = Index;
}

/// <summary>
/// 紐づけ番号の取得
/// </summary>
/// <param name=""></param>
/// <returns> 紐づけ番号 </returns>
int32 UBattleHUDMapIcon::GetObjectIndex() {

	return ObjectIndex;
}

/// <summary>
/// 背景イメージの表示設定
/// </summary>
/// <param name="VisibleType"> 表示設定 </param>
/// <returns></returns>
void UBattleHUDMapIcon::SetupDisplayBackImage(ESlateVisibility VisibleType) {

	UImage* Image = GetBackImage();
	if (!IsValid(Image)) {
		return;
	}
	Image->SetVisibility(VisibleType);
}

/// <summary>
/// 背景イメージの取得
/// </summary>
/// <param name=""></param>
/// <returns> 背景イメージ </returns>
UImage* UBattleHUDMapIcon::GetBackImage() {

	return Cast<UImage>(GetWidgetFromName(ImageBackName));
}

/// <summary>
/// 背景アニメーションフラグのリセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleHUDMapIcon::ResetPlayAnimFlag() {

	bIsPlayAnimFlag = false;
}

/// <summary>
/// 背景アニメーションが実行されているかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:実行中 </returns>
bool UBattleHUDMapIcon::IsPlayBackAnim() {

	return bIsPlayAnimFlag;
}

/// <summary>
/// 背景エフェクトアニメーション実行
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleHUDMapIcon::PlayBackAnimation_Implementation() {

}

/// <summary>
/// 背景エフェクトアニメーション停止
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleHUDMapIcon::StopBackAnimation_Implementation() {

}