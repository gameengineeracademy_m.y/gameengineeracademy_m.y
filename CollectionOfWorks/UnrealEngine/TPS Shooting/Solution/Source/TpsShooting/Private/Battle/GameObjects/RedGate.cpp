﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/GameObjects/RedGate.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ARedGate::ARedGate() {

 	// Tick処理を無効化
	PrimaryActorTick.bCanEverTick = false;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ARedGate::BeginPlay() {

	Super::BeginPlay();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ARedGate::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}

