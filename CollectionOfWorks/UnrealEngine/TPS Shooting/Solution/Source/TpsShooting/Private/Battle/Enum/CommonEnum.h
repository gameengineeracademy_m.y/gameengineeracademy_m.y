﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/// <summary>
/// 共通の列挙型宣言ファイル
/// </summary>

/// <summary>
/// キャラクターの種類
/// </summary>
UENUM(BlueprintType)
enum class ECharacterType : uint8 {
	None,           // 設定なし
	Player,         // プレイヤー
	Enemy,          // 敵
	MaxTypeIndex    // 種類数
};

/// <summary>
/// エリアの種類
/// </summary>
UENUM(BlueprintType)
enum class EStageType : uint8 {
	None,           // 設定なし
	First,          // 第1ステージ
	Second,         // 第2ステージ
	Third,          // 第3ステージ
	MaxTypeIndex    // 種類数
};

/// <summary>
/// 方向の種類
/// </summary>
UENUM(BlueprintType)
enum class EDirectionType : uint8 {
	Forward,       // 前方
	Back,          // 背後
	Left,          // 左側
	Right,         // 右側
	MaxTypeIndex
};

