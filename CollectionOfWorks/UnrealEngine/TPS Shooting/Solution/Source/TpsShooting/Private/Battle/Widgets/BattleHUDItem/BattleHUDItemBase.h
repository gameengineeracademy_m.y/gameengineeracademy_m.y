﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Battle/Enum/CommonEnum.h"
#include "Materials/Material.h"
#include "Components/Image.h"
#include "BattleHUDItemBase.generated.h"

/// <summary>
/// バトルHUDアイテムの種類
/// </summary>
UENUM()
enum class EBattleHUDItemType {
	None,         // 設定なし
	WeaponIcon,   // 武器アイコン
	MapIcon,      // マップアイコン
	MaxTypeIndex
};

/// <summary>
/// バトルHUDアイテムベースクラス
/// </summary>
UCLASS()
class UBattleHUDItemBase : public UUserWidget
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// HUDアイテムの種類のセット
	/// </summary>
	/// <param name="ItemType"> HUDアイテムの種類 </param>
	/// <returns></returns>
	void SetBattleHUDItemType(EBattleHUDItemType ItemType);

	/// <summary>
	/// HUDアイテムの種類を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> HUDアイテムの種類 </returns>
	EBattleHUDItemType GetBattleHUDItemType();

	/// <summary>
	/// 表示設定
	/// </summary>
	/// <param name="bDisplay"> 表示有無 </param>
	/// <returns></returns>
	void SetDisplay(bool bDisplay);

	/// <summary>
	/// 表示するかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:表示する </returns>
	bool IsDisplay();

	/// <summary>
	/// アイコンのセット
	/// </summary>
	/// <param name="Material"> マテリアル </param>
	/// <param name="ImageName"> セットするイメージ名 </param>
	/// <returns></returns>
	void SetBattleHUDIcon(UMaterial* Material, FName ImageName);

	/// <summary>
	/// HUDアイテムのサイズ変更
	/// </summary>
	/// <param name="Size"> 変更後のサイズ </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleHUDItemBase")
	void ChangeItemSize(FVector2D Size);


protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

private:

	/// <summary>
	/// HUDアイテムのサイズ変更
	/// </summary>
	/// <param name="Size"> 変更後のサイズ </param>
	/// <returns></returns>
	virtual void ChangeItemSize_Implementation(FVector2D Size);

private:
	
	/// <summary>
	/// HUDアイテムの種類
	/// </summary>
	EBattleHUDItemType HUDItemType;

	/// <summary>
	/// 表示するかどうか
	/// </summary>
	bool bIsDisplay;
};
