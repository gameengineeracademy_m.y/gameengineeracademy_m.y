﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/GameObjects/TargetObjects/ObjectBase.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AObjectBase::AObjectBase() {

	// Tick処理の無効化
	PrimaryActorTick.bCanEverTick = false;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AObjectBase::BeginPlay() {

	Super::BeginPlay();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AObjectBase::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}

/// <summary>
/// エリアの種類をセット
/// </summary>
/// <param name="Type"> エリアの種類 </param>
/// <returns></returns>
void AObjectBase::SetStageType(EStageType Type) {

	StageType = Type;
}

/// <summary>
/// エリアの種類を取得
/// </summary>
/// <param name=""></param>
/// <returns> エリアの種類 </returns>
EStageType AObjectBase::GetStageType() {

	return StageType;
}