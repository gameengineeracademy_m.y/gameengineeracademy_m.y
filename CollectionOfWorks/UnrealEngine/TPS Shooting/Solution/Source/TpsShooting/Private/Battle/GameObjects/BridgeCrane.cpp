﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/GameObjects/BridgeCrane.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ABridgeCrane::ABridgeCrane()
	: MoveStartPosition()
	, MoveEndPosition()
	, bSetupFinish(false)
	, CurrentPhase(EBridgeCranePhase::BeforeSetup)
	, CurrentMovePhase(EBridgeCraneMovePhase::None)
	, MoveVector()
	, MoveDistance()
	, WaitTime(0.0f)
	, AccumulateTime(0.0f) {

	PrimaryActorTick.bCanEverTick = true;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABridgeCrane::BeginPlay() {

	Super::BeginPlay();
	
	CurrentPhase = EBridgeCranePhase::BeforeSetup;
	CurrentMovePhase = EBridgeCraneMovePhase::Forward;

	WaitTime = ResetTime;
	AccumulateTime = ResetTime;

	bSetupFinish = false;

	MoveStartPosition = FVector::ZeroVector;
	MoveEndPosition = FVector::ZeroVector;
	MoveVector = FVector::ZeroVector;
	MoveDistance = FVector::ZeroVector;
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ABridgeCrane::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

	switch (CurrentPhase) {
	case EBridgeCranePhase::None: {
		//---------------------------------
		//なにもしない
		//---------------------------------
		break;
	}
	case EBridgeCranePhase::BeforeSetup: {
		//---------------------------------
		//移動設定前処理
		// トリガーボックスイベントにて初期設定を行う
		// Overlapイベント実行のため動かしておく
		//---------------------------------

		if (bSetupFinish) {
			ChangeCurrentPhase(EBridgeCranePhase::Initialize);
			break;
		}

		AccumulateTime += DeltaTime;

		FVector CurrentPos = GetActorLocation();
		CurrentPos.Z -= InitMoveValue;
		SetActorLocation(CurrentPos);

		//---------------------------------
		//指定時間移動処理を実行し、
		//設定が行われなかった場合は
		//動かないオブジェクトとする
		//---------------------------------
		if (AccumulateTime < BeforeSetupTime) {
			break;
		}
		AccumulateTime = ResetTime;

		ChangeCurrentPhase(EBridgeCranePhase::None);
		break;
	}
	case EBridgeCranePhase::Initialize: {
		//---------------------------------
		//初期化処理
		//---------------------------------

		AccumulateTime = ResetTime;
		WaitTime = GetRandamWaitTime();

		// 初期位置に配置
		SetActorLocation(MoveStartPosition);

		// 1フレームあたりの移動量を算出
		MoveVector = (MoveEndPosition - MoveStartPosition) / ArrivalMoveTime * DeltaTime;
		MoveDistance = FVector::ZeroVector;

		ChangeCurrentPhase(EBridgeCranePhase::Stop);
		break;
	}
	case EBridgeCranePhase::Stop: {
		//---------------------------------
		//停止処理
		//---------------------------------

		AccumulateTime += DeltaTime;
		if (AccumulateTime < WaitTime) {
			break;
		}
		AccumulateTime = ResetTime;

		ChangeCurrentPhase(EBridgeCranePhase::Move);
		break;
	}
	case EBridgeCranePhase::Move: {
		//---------------------------------
		//移動処理
		//---------------------------------
		bool IsFinish = Move();
		if (!IsFinish) {
			break;
		}
		
		//----------------------------------
		//移動処理完了後、待機時間を設定
		//フェーズを停止に変更する
		//----------------------------------
		MoveDistance = FVector::ZeroVector;
		WaitTime = GetRandamWaitTime();
		ChangeCurrentPhase(EBridgeCranePhase::Stop);
		break;
	}
	}
}

/// <summary>
/// 移動処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理完了 </returns>
bool ABridgeCrane::Move() {

	// 総移動量に移動量を加算
	MoveDistance += MoveVector;
	bool IsFinish = IsArrivedGoal();

	switch (CurrentMovePhase) {
	case EBridgeCraneMovePhase::Forward: {
		//---------------------------------
		//前進処理
		//---------------------------------
		FVector CurrentPos = MoveStartPosition + MoveDistance;
		SetActorLocation(CurrentPos);
		if (!IsFinish) {
			break;
		}
		ChangeCurrentMovePhase(EBridgeCraneMovePhase::Back);
		return true;
	}
	case EBridgeCraneMovePhase::Back: {
		//---------------------------------
		//後退処理
		//---------------------------------
		FVector CurrentPos = MoveEndPosition - MoveDistance;
		SetActorLocation(CurrentPos);
		if (!IsFinish) {
			break;
		}
		ChangeCurrentMovePhase(EBridgeCraneMovePhase::Forward);
		return true;
	}
	}

	return false;
}

/// <summary>
/// 目標地点に到達したかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:到達 </returns>
bool ABridgeCrane::IsArrivedGoal() {

	bool IsArriveX = false;
	bool IsArriveY = false;
	bool IsArriveZ = false;

	//-------------------------------------------
	//目標地点に到達しているかを総移動距離で判定
	//-------------------------------------------

	FVector DistanceToGoal = MoveEndPosition - MoveStartPosition;
	if (MoveDistance.X >= DistanceToGoal.X) {
		IsArriveX = true;
		MoveDistance.X = DistanceToGoal.X;
	}

	if (MoveDistance.Y >= DistanceToGoal.Y) {
		IsArriveY = true;
		MoveDistance.Y = DistanceToGoal.Y;
	}

	if (MoveDistance.Z >= DistanceToGoal.Z) {
		IsArriveZ = true;
		MoveDistance.Z = DistanceToGoal.Z;
	}

	if (!IsArriveX || !IsArriveY || !IsArriveZ) {
		return false;
	}

	return true;
}

/// <summary>
/// 待機時間をランダムに設定し取得する
/// </summary>
/// <param name=""></param>
/// <returns> 待機時間 </returns>
float ABridgeCrane::GetRandamWaitTime() {

	return FMath::RandRange(MinWaitTime, MaxWaitTime);
}

/// <summary>
/// 現在のフェーズを変更する
/// </summary>
/// <param name="PhaseType"> 処理時間 </param>
/// <returns></returns>
void ABridgeCrane::ChangeCurrentPhase(EBridgeCranePhase PhaseType) {

	CurrentPhase = PhaseType;
}

/// <summary>
/// 現在の移動フェーズを変更する
/// </summary>
/// <param name="PhaseType"> 処理時間 </param>
/// <returns></returns>
void ABridgeCrane::ChangeCurrentMovePhase(EBridgeCraneMovePhase PhaseType) {

	CurrentMovePhase = PhaseType;
}
