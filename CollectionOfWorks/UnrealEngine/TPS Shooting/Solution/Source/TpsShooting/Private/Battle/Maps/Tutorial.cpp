﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Maps/Tutorial.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ATutorial::ATutorial()
	: ExecCamraraDirectionTrigger(nullptr)
	, BattleController(nullptr)
	, BattleGameState(nullptr)
	, PlayerManagerClass()
	, PlayerManager(nullptr)
	, EnemyManagerClass()
	, EnemyManager(nullptr)
	, WeaponFactoryClass()
	, WeaponFactory(nullptr)
	, PickupItemManagerClass()
	, PickupItemManager(nullptr)
	, WarningLampManagerClass()
	, WarningLampManager(nullptr)
	, TargetObjectManagerClass()
	, TargetObjectManager(nullptr)
	, CurrentPhase(ETutorialPhaseType::Initialize)
	, CurrentGamePhase(EGamePhaseType::None)
	, CurrentResultPhase(ETutorialResultPhaseType::Wait)
	, SwitchPhase(ETutorialSwitchPhaseType::None)
	, CurrentPauseType(ETutorialPauseType::None)
	, AccumulateTime(0.0f)
	, KeepPressValue(0.0f)
	, bIsCameraDirection(false)
	, bGameClear(false) {

	// Tickイベントの有効化
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bTickEvenWhenPaused = true;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::BeginPlay() {

	Super::BeginPlay();

	//---------------------------------------
	//変数の初期化処理
	//---------------------------------------
	CurrentPhase = ETutorialPhaseType::Initialize;
	CurrentGamePhase = EGamePhaseType::None;
	CurrentResultPhase = ETutorialResultPhaseType::Wait;
	SwitchPhase = ETutorialSwitchPhaseType::None;
	CurrentPauseType = ETutorialPauseType::None;
	AccumulateTime = ResetTime;
	bIsCameraDirection = true;
	bGameClear = false;   // リザルトウィジェットの非表示処理に使用

	//---------------------------------------
	// バトルゲームステートの進行の初期化
	//---------------------------------------
	BattleGameState = Cast<ABattleGameState>(GetWorld()->GetGameState());
	if (!IsValid(BattleGameState)) {
		return;
	}
	BattleGameState->ResetGameState();

	//---------------------------------------
	// バトルコントローラのインプットアクションの設定
	//---------------------------------------
	// バトルコントローラを取得
	BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(BattleController)) {
		return;
	}
	BattleController->SetEventInterface(this);
	BattleController->SetupInput();
	BattleController->AddInputType(EBattleInputType::Game);

	//---------------------------------------
	// 各ウィジェットの表示前準備処理
	// 各ウィジェットの表示処理
	//---------------------------------------
	BattleController->CreateUi();
	BattleController->PrepareOpenUi();
	BattleController->OpenUi();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ATutorial::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

	switch (CurrentPhase) {
	case ETutorialPhaseType::Initialize: {
		//--------------------------
		//初期化処理フェーズ
		//--------------------------
		// 警戒ランプマネージャ
		InitializeWarningLampManager();
		// ウェポンファクトリー
		InitializeWeaponFactory();
		// プレイヤーマネージャ
		InitializePlayerManager();
		// エネミーマネージャ
		InitializeEnemyManager();
		// アイテムマネージャ
		InitializePickupItemManager();
		// ターゲットオブジェクトマネージャ
		InitializeTargetObjectManager();

		// サウンド生成
		CreateSound();
		// プレイヤー生成
		CreatePlayer();
		// 敵の生成
		CreateEnemies();
		// アイテム生成
		CreatePickupItem();
		// ターゲットオブジェクト生成
		CreateTargetObject();
		// バトルHUD情報の設定
		SetupBattleHUDWeapon();
		// ミニマップ情報の設定
		SetupMiniMap();
		// エリアマップの設定
		SetupAreaMap();

		// 現在のフェーズを「フェードイン」フェーズに変更
		ChangeCurrentPhase(ETutorialPhaseType::FadeIn);
		break;
	}
	case ETutorialPhaseType::FadeIn: {
		//--------------------------
		//フェードインフェーズ
		//--------------------------
		// バトルコントローラ、フェードウィジェットが
		// 見つからない場合は強制的に次のフェーズに進む
		UFadeWidget* FadeWidget = GetFadeWidget();
		if (IsValid(FadeWidget)) {
			// フェードイン実行
			FadeWidget->PlayFadeInAnimation();
		}

		AAudioManager* AudioManager = GetAudioManager();
		if (IsValid(AudioManager)) {
			AudioManager->SetBGMSound(EBGMType::Game);
			AudioManager->FadeInBGM();
		}

		// 現在のフェーズを「フェードイン待機」フェーズに変更
		ChangeCurrentPhase(ETutorialPhaseType::WaitFadeIn);
		break;
	}
	case ETutorialPhaseType::WaitFadeIn: {
		//--------------------------
		//フェードイン待機フェーズ
		//--------------------------
		// バトルコントローラ、各ウィジェットが
		// 見つからない場合は強制的に次のフェーズに進む
		bool IsFinish = true;
		UFadeWidget* FadeWidget = GetFadeWidget();
		if (IsValid(FadeWidget)) {
			IsFinish = FadeWidget->IsAnimFinish();
			// フェードウィジェットのフラグリセット
			FadeWidget->ResetAnimFlag();
		}
		if (!IsFinish) {
			break;
		}
		FadeWidget->Closed();

		// 現在のフェーズを「プレイ前準備」フェーズに変更
		ChangeCurrentPhase(ETutorialPhaseType::PreparePlay);
		break;
	}
	case ETutorialPhaseType::PreparePlay: {
		//--------------------------
		//プレイ前準備フェーズ
		//--------------------------
		bool IsFinish = PossessPlayer();
		if (!IsFinish) {
			break;
		}

		// 現在のフェーズを「プレイ中」フェーズに変更
		ChangeCurrentPhase(ETutorialPhaseType::Play);
		break;
	}
	case ETutorialPhaseType::Play: {
		//--------------------------
		//プレイ中フェーズ
		//--------------------------
		CheckGameState();
		break;
	}
	case ETutorialPhaseType::CameraDirection: {
		//--------------------------
		//カメラ演出フェーズ
		//--------------------------
		break;
	}
	case ETutorialPhaseType::Pause: {
		//--------------------------
		//ポーズフェーズ
		//--------------------------
		switch (CurrentPauseType) {
		case ETutorialPauseType::Play: {

			OperatePauseMenu();
			break;
		}
		}
		break;
	}
	case ETutorialPhaseType::OptionOperation: {
		//--------------------------
		//オプション操作フェーズ
		//--------------------------
		bool IsFinish = OperateOptionMenu();
		if (!IsFinish) {
			break;
		}
		// 現在のフェーズを「ポーズ操作」に変更
		ChangeCurrentPhase(ETutorialPhaseType::Pause);
		break;
	}
	case ETutorialPhaseType::OperateList: {
		//--------------------------
		//操作一覧表示フェーズ
		//--------------------------
		bool IsFinish = OperateOperateList();
		if (!IsFinish) {
			break;
		}
		// 現在のフェーズを「ポーズ操作」に変更
		ChangeCurrentPhase(ETutorialPhaseType::Pause);
		break;
	}
	case ETutorialPhaseType::GameOver: {
		//--------------------------
		//ゲームオーバーフェーズ
		//--------------------------
		bool IsFinish = PerformGameOver(DeltaTime);
		if (!IsFinish) {
			break;
		}
		// 現在のフェーズを「フェードアウト」フェーズに変更
		ChangeCurrentPhase(ETutorialPhaseType::FadeOut);
		break;
	}
	case ETutorialPhaseType::GameClear: {
		//--------------------------
		//ゲームクリアフェーズ
		//--------------------------
		bool IsFinish = PerformGameClear(DeltaTime);
		if (!IsFinish) {
			break;
		}
		// 現在のフェーズを「フェードアウト」フェーズに変更
		ChangeCurrentPhase(ETutorialPhaseType::FadeOut);
		break;
	}
	case ETutorialPhaseType::Retry: {

		if (!IsValid(BattleGameState)) {
			return;
		}
		// ゲーム状況をリセットする
		BattleGameState->ResetGameState();
		BattleGameState->LoadStageType();
		// プレイヤーのリスポーン
		RespawnPlayer();
		// エネミーのリスポーン
		RespawnEnemies();
		// リザルトウィジェットの初期化
		InitializeResult();
		// バトルHUDウィジェットの初期化
		InitializeBattleHUD();
		// フィールドアイテムのリセット
		ResetPickupItem();

		// バトルHUDウィジェットを表示しておく
		SetupDisplayBattleHUD(true);
		ResetFoundMarker();

		// 現在のフェーズを「フェードイン」フェーズに変更
		ChangeCurrentPhase(ETutorialPhaseType::FadeIn);
		break;
	}
	case ETutorialPhaseType::FadeOut: {
		//--------------------------
		//フェードアウトフェーズ
		//--------------------------
		// バトルコントローラ、フェードウィジェットが
		// 見つからない場合は強制的に次のフェーズに進む
		UFadeWidget* FadeWidget = GetFadeWidget();
		if (IsValid(FadeWidget)) {
			// フェードアウト実行
			FadeWidget->Open();
			FadeWidget->PlayFadeOutAnimation();
		}

		// 現在のフェーズを「レベル切替」フェーズに変更
		ChangeCurrentPhase(ETutorialPhaseType::ChangeLevel);
		break;
	}
	case ETutorialPhaseType::ChangeLevel: {
		//--------------------------
		//レベル切替フェーズ
		//※フェードアウトが終了するまで待機
		//--------------------------
		// バトルコントローラ、フェードウィジェットが
		// 見つからない場合は強制的に次のフェーズに進む
		bool IsFinish = true;
		UFadeWidget* FadeWidget = GetFadeWidget();
		if (IsValid(FadeWidget)) {
			IsFinish = FadeWidget->IsAnimFinish();
			// フェードウィジェットのフラグリセット
			FadeWidget->ResetAnimFlag();
		}

		if (!IsFinish) {
			break;
		}

		switch (SwitchPhase) {
		case ETutorialSwitchPhaseType::Retry: {

			if (bGameClear) {
				UResultWidget* ResultWidget = GetResultWidget(EWidgetType::TutorialClear);
				if (IsValid(ResultWidget)) {
					// 操作処理をリセットしておく
					ResultWidget->ResetOperation();
					// リザルト画面を閉じる
					ResultWidget->Closed();
				}
			}
			bGameClear = false;

			// 現在のフェーズを「リトライ」に変更
			ChangeCurrentPhase(ETutorialPhaseType::Retry);
			break;
		}
		case ETutorialSwitchPhaseType::ChangeLevel: {

			// 次に遷移するレベルを指定する
			UTpsShootingGameInstance* GameInstance = Cast<UTpsShootingGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
			if (!IsValid(GameInstance)) {
				return;
			}
			GameInstance->SetNextTransLevel(ELevelType::Title);
			// 現在のフェーズを「終了処理」に変更
			ChangeCurrentPhase(ETutorialPhaseType::Finish);
			break;
		}
		default: {
			// 現在のフェーズを「終了処理」に変更
			ChangeCurrentPhase(ETutorialPhaseType::Finish);
			break;
		}
		}
		break;
	}
	case ETutorialPhaseType::Finish: {
		//--------------------------
		//終了処理フェーズ
		//--------------------------
		break;
	}
	}
}

/// <summary>
/// 警戒ランプマネージャの初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::InitializeWarningLampManager() {

	// 警戒ランプマネージャの生成
	FString WarningLampManagerPath;
	GConfig->GetString(TEXT("ActorSettings"), TEXT("WarningLampManagerPath"), WarningLampManagerPath, GGameIni);

	WarningLampManagerClass = TSoftClassPtr<AWarningLampManager>(FSoftObjectPath(*WarningLampManagerPath)).LoadSynchronous();
	if (!IsValid(WarningLampManagerClass)) {
		return;
	}
	WarningLampManager = GetWorld()->SpawnActor<AWarningLampManager>(WarningLampManagerClass);
	if (!IsValid(WarningLampManager)) {
		return;
	}
	// 初期化処理を実行
	WarningLampManager->Initialize();
}

/// <summary>
/// ウェポンファクトリーの初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::InitializeWeaponFactory() {

	// プレイヤーマネージャの生成
	FString WeaponFactoryPath;
	GConfig->GetString(TEXT("ActorSettings"), TEXT("WeaponFactoryPath"), WeaponFactoryPath, GGameIni);

	WeaponFactoryClass = TSoftClassPtr<AWeaponFactory>(FSoftObjectPath(*WeaponFactoryPath)).LoadSynchronous();
	if (!IsValid(WeaponFactoryClass)) {
		return;
	}
	WeaponFactory = GetWorld()->SpawnActor<AWeaponFactory>(WeaponFactoryClass);
}

/// <summary>
/// プレイヤーマネージャの初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::InitializePlayerManager() {

	// プレイヤーマネージャの生成
	FString PlayerManagerPath;
	GConfig->GetString(TEXT("ActorSettings"), TEXT("PlayerManagerPath"), PlayerManagerPath, GGameIni);

	PlayerManagerClass = TSoftClassPtr<APlayerManager>(FSoftObjectPath(*PlayerManagerPath)).LoadSynchronous();
	if (!IsValid(PlayerManagerClass)) {
		return;
	}
	PlayerManager = GetWorld()->SpawnActor<APlayerManager>(PlayerManagerClass);
	if (!IsValid(PlayerManager)) {
		return;
	}
	if (!IsValid(PlayerManager) || !IsValid(WeaponFactory) ||
		!IsValid(BattleGameState)) {
		return;
	}
	// ウェポンファクトリーをプレイヤーマネージャに渡す
	// ステート情報にプレイヤーマネージャを渡しておく
	PlayerManager->SetWeaponFactory(WeaponFactory);
	BattleGameState->SetPlayerManager(PlayerManager);
}

/// <summary>
/// エネミーマネージャの初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::InitializeEnemyManager() {

	// プレイヤーマネージャの生成
	FString EnemyManagerPath;
	GConfig->GetString(TEXT("ActorSettings"), TEXT("EnemyManagerPath"), EnemyManagerPath, GGameIni);

	EnemyManagerClass = TSoftClassPtr<AEnemyManager>(FSoftObjectPath(*EnemyManagerPath)).LoadSynchronous();
	if (!IsValid(EnemyManagerClass)) {
		return;
	}
	EnemyManager = GetWorld()->SpawnActor<AEnemyManager>(EnemyManagerClass);

	if (!IsValid(EnemyManager) || !IsValid(WeaponFactory) ||
		!IsValid(BattleGameState)) {
		return;
	}
	// ウェポンファクトリーと警告ランプマネージャーをエネミーマネージャに渡す
	// ステート情報にエネミーマネージャを渡しておく
	EnemyManager->SetWeaponFactory(WeaponFactory);
	EnemyManager->SetWarningLampManager(WarningLampManager);
	BattleGameState->SetEnemyManager(EnemyManager);
}

/// <summary>
/// アイテムマネージャの初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::InitializePickupItemManager() {

	// アイテムマネージャの生成
	FString PickupItemManagerPath;
	GConfig->GetString(TEXT("ActorSettings"), TEXT("PickupItemManagerPath"), PickupItemManagerPath, GGameIni);

	PickupItemManagerClass = TSoftClassPtr<APickupItemManager>(FSoftObjectPath(*PickupItemManagerPath)).LoadSynchronous();
	if (!IsValid(PickupItemManagerClass)) {
		return;
	}
	PickupItemManager = GetWorld()->SpawnActor<APickupItemManager>(PickupItemManagerClass);
	if (!IsValid(PickupItemManager)) {
		return;
	}
	PickupItemManager->SetEventInterface(this);
}

/// <summary>
/// ターゲットオブジェクトマネージャの初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::InitializeTargetObjectManager() {

	// ターゲットオブジェクトマネージャの生成
	FString TargetObjectManagerPath;
	GConfig->GetString(TEXT("ActorSettings"), TEXT("TargetObjectManagerPath"), TargetObjectManagerPath, GGameIni);

	TargetObjectManagerClass = TSoftClassPtr<ATargetObjectManager>(FSoftObjectPath(*TargetObjectManagerPath)).LoadSynchronous();
	if (!IsValid(TargetObjectManagerClass)) {
		return;
	}
	TargetObjectManager = GetWorld()->SpawnActor<ATargetObjectManager>(TargetObjectManagerClass);
}

/// <summary>
/// サウンドの生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::CreateSound() {

	//------------------------------------
	//データテーブルからBGMとSEのパスを
	//取得し、サウンドを生成する
	//------------------------------------
	// BGM初期設定
	AAudioManager* AudioManager = GetAudioManager();
	if (!IsValid(AudioManager)) {
		return;
	}
	AudioManager->InitializeBGM();
}

/// <summary>
/// プレイヤーの生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::CreatePlayer() {

	//--------------------------------------
	//操作プレイヤーを生成し、
	//コントローラに紐づける
	//--------------------------------------
	if (!IsValid(PlayerManager)) {
		return;
	}

	AOperatePlayer* Player = PlayerManager->CreatePlayer();
	if (!IsValid(Player)) {
		return;
	}
	// イベントインターフェース設定
	Player->SetEventInterface(this);

	// 現在の所持している武器を保存しておく
	PlayerManager->SaveEquipWeapon(Player->GetPossessWeaponList());

	if (!IsValid(BattleController)) {
		return;
	}
	// カメラの位置をプレイヤーに合わせる
	BattleController->SetViewTargetWithBlend(Player);
}

/// <summary>
/// エネミーの生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::CreateEnemies() {

	//-----------------------------------------
	//エネミーを生成
	// 本処理内で武器も生成
	//-----------------------------------------
	if (!IsValid(EnemyManager)) {
		return;
	}
	EnemyManager->CreateEnemies();
}

/// <summary>
/// アイテムの生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::CreatePickupItem() {

	if (!IsValid(PickupItemManager)) {
		return;
	}
	PickupItemManager->SpawnPickupItem();
}

/// <summary>
/// ターゲットオブジェクトの生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::CreateTargetObject() {

	if (!IsValid(TargetObjectManager)) {
		return;
	}
	TargetObjectManager->SpawnObjects();
}

/// <summary>
/// バトルHUD情報の設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::SetupBattleHUDWeapon() {

	UBattleWidget* Widget = GetBattleWidget();
	if (!IsValid(Widget) || !IsValid(PlayerManager)) {
		return;
	}

	TMap<EEquipPositionType, EWeaponType> WeaponList = PlayerManager->GetSaveEquipWeapon();
	for (auto Weapon : WeaponList) {
		// 装備している部位に応じて武器アイコンを設定する
		switch (Weapon.Key) {
		case EEquipPositionType::Hand: {
			Widget->SetEquipWeaponIcon(Weapon.Value);
			break;
		}
		case EEquipPositionType::Back: {
			Widget->SetSecondWeaponIcon(Weapon.Value);
			break;
		}
		}
	}
}

/// <summary>
/// ミニマップウィジェットの設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::SetupMiniMap() {

	UMiniMapWidget* Widget = GetMiniMapWidget();
	if (!IsValid(Widget)) {
		return;
	}

	// 各マネージャクラスのアドレスを渡す
	Widget->SetPlayerManager(PlayerManager);
	Widget->SetEnemyManager(EnemyManager);
	Widget->SetPickupItemManager(PickupItemManager);
	Widget->SetTargetObjectManager(TargetObjectManager);

	// マップの初期設定
	Widget->SetupWidget();

	// エリアの種類をウィジェットに渡す
	EStageType StageType = BattleGameState->GetCurrentStageType();
	Widget->SetStageType(StageType);
}

/// <summary>
/// エリアマップウィジェットの設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::SetupAreaMap() {

	UAreaMapWidget* Widget = GetAreaMapWidget();
	if (!IsValid(Widget)) {
		return;
	}

	// 各マネージャクラスのアドレスを渡す
	Widget->SetPlayerManager(PlayerManager);
	Widget->SetEnemyManager(EnemyManager);
	Widget->SetPickupItemManager(PickupItemManager);
	Widget->SetTargetObjectManager(TargetObjectManager);

	// マップの初期設定
	Widget->SetupWidget();
	// エリアの種類をウィジェットに渡し、マスクテクスチャを切り替える
	EStageType StageType = BattleGameState->GetCurrentStageType();
	Widget->SwitchAreaMap(StageType);
}

/// <summary>
/// バトルHUD情報の武器の更新
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::OnUpdateBattleHUDWeapon() {

	UBattleWidget* Widget = GetBattleWidget();
	if (!IsValid(Widget) || !IsValid(PlayerManager)) {
		return;
	}

	AOperatePlayer* Player = PlayerManager->GetOperatePlayer();
	if (!IsValid(Player)) {
		return;
	}
	TMap<EEquipPositionType, EWeaponType> WeaponList = Player->GetPossessWeapon();
	for (auto Weapon : WeaponList) {
		// 装備している部位に応じて武器アイコンを設定する
		switch (Weapon.Key) {
		case EEquipPositionType::Hand: {
			Widget->SetEquipWeaponIcon(Weapon.Value);
			break;
		}
		case EEquipPositionType::Back: {
			Widget->SetSecondWeaponIcon(Weapon.Value);
			break;
		}
		}
	}
}

/// <summary>
/// バトルHUDの弾薬情報の更新
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::OnUpdateBattleHUDWeaponAmmos() {

	if (!IsValid(PlayerManager)) {
		return;
	}

	UBattleWidget* Widget = GetBattleWidget();
	AOperatePlayer* Player = PlayerManager->GetOperatePlayer();
	if (!IsValid(Widget) || !IsValid(Player)) {
		return;
	}
	
	AWeaponBase* Weapon = Player->GetEquipWeapon();
	if (!IsValid(Weapon)) {
		return;
	}
	int32 LoadedAmmos = Weapon->GetLoadedAmmos();
	int32 PossessAmmos = Weapon->GetPossessAmmos();

	Widget->SetLoadedAmmos(LoadedAmmos);
	Widget->SetPossessAmmos(PossessAmmos);
}

/// <summary>
/// 現在のフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ATutorial::ChangeCurrentPhase(ETutorialPhaseType PhaseType) {

	CurrentPhase = PhaseType;
}

/// <summary>
/// 現在のゲームフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ATutorial::ChangeCurrentGamePhase(EGamePhaseType PhaseType) {

	CurrentGamePhase = PhaseType;
}

/// <summary>
/// 現在のポーズの種類を変更する
/// </summary>
/// <param name="PauseType"> ポーズの種類 </param>
/// <returns></returns>
void ATutorial::ChangeCurrentPauseType(ETutorialPauseType PauseType) {

	CurrentPauseType = PauseType;
}

/// <summary>
/// 現在のリザルトフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ATutorial::ChangeCurrentResultPhase(ETutorialResultPhaseType PhaseType) {

	CurrentResultPhase = PhaseType;
}

/// <summary>
/// フェーズ切替の種類を変更する
/// </summary>
/// <param name="Type"> フェーズ切替の種類 </param>
/// <returns></returns>
void ATutorial::ChangeSwitchPhaseType(ETutorialSwitchPhaseType Type) {

	SwitchPhase = Type;
}

/// <summary>
/// 現在のゲームフェーズが開始かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:開始 </returns>
bool ATutorial::IsStart() {

	return CurrentGamePhase == EGamePhaseType::Start;
}

/// <summary>
/// 現在のフェーズがプレイ中かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:プレイ中 </returns>
bool ATutorial::IsPlay() {

	return CurrentPhase == ETutorialPhaseType::Play;
}

/// <summary>
/// 現在のフェーズがポーズ中かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:ポーズ中 </returns>
bool ATutorial::IsPause() {

	return CurrentPhase == ETutorialPhaseType::Pause;
}

/// <summary>
/// 現在のフェーズがマップ表示中かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:マップ表示中 </returns>
bool ATutorial::IsMap() {

	return CurrentPhase == ETutorialPhaseType::Map;
}

/// <summary>
/// 現在のフェーズがカメラ演出中かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:カメラ演出中 </returns>
bool ATutorial::IsCameraDirection() {

	return CurrentPhase == ETutorialPhaseType::CameraDirection;
}

/// <summary>
/// プレイヤーにコントローラを紐づける
/// </summary>
/// <param name=""></param>
/// <returns> true:処理完了 </returns>
bool ATutorial::PossessPlayer() {

	if (!IsValid(BattleController) || !IsValid(PlayerManager)) {
		return false;
	}

	// バトルコントローラにプレイヤーを紐づける
	AOperatePlayer* Player = PlayerManager->GetOperatePlayer();
	if (!IsValid(Player)) {
		return false;
	}
	BattleController->Possess(Player);

	return true;
}

/// <summary>
/// コントローラから所有物を切り離す
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::UnPossessOwner() {

	if (!IsValid(BattleController)) {
		return;
	}

	// バトルコントローラから切り離す
	BattleController->UnPossess();
}

/// <summary>
/// ゲーム中のHUDの表示設定
/// </summary>
/// <param name="IsDisplay"> 表示有無 </param>
/// <returns></returns>
void ATutorial::SetupDisplayBattleHUD(bool IsDisplay) {

	if (!IsValid(BattleController)) {
		return;
	}

	// HUDを取得
	UBattleWidget* BattleWidget = BattleController->GetBattleWidget();
	UMiniMapWidget* MiniMapWidget = BattleController->GetMiniMapWidget();
	UCrossHairWidget* CrossHairWidget = BattleController->GetCrossHairWidget();
	if (!IsValid(BattleWidget) || !IsValid(MiniMapWidget) || 
		!IsValid(CrossHairWidget)) {
		return;
	}

	if (IsDisplay) {
		BattleWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
		CrossHairWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
		MiniMapWidget->Display();
	}
	else {
		BattleWidget->SetVisibility(ESlateVisibility::Hidden);
		CrossHairWidget->SetVisibility(ESlateVisibility::Hidden);
		MiniMapWidget->Hide();
	}
}

/// <summary>
/// 敵に発見されたウィジェットをリセットする
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::ResetFoundMarker() {

	if (!IsValid(BattleController)) {
		return;
	}
	UMiniMapWidget* MiniMapWidget = BattleController->GetMiniMapWidget();
	if (!IsValid(MiniMapWidget)) {
		return;
	}
	MiniMapWidget->CancelFoundMarkerAnimation();
}

/// <summary>
/// プレイヤー操作の設定
/// </summary>
/// <param name="bIsOperate"> 有効化有無 </param>
/// <returns></returns>
void ATutorial::SetupOperatePlayer(bool bIsOperate) {

	if (!IsValid(PlayerManager) || !IsValid(BattleController)) {
		return;
	}
	AOperatePlayer* Player = PlayerManager->GetOperatePlayer();
	if (!IsValid(Player)) {
		return;
	}

	if (bIsOperate) {
		// プレイヤーの入力操作を有効化する
		Player->EnableInput(BattleController);
	}
	else {
		// プレイヤーの入力操作を無効化する
		Player->DisableInput(BattleController);
	}
}

/// <summary>
/// ゲーム状況確認処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::CheckGameState() {

	if (!IsValid(BattleGameState)) {
		return;
	}

	// 「プレイ中」なら処理終了
	bool IsCheck = BattleGameState->IsPlay();
	if (IsCheck) {
		return;
	}

	EGameStateType StateType = BattleGameState->GetCurrentGameState();
	switch (StateType) {
	case EGameStateType::GameOver: {

		AAudioManager* AudioManager = GetAudioManager();
		if (IsValid(AudioManager)) {
			AudioManager->FadeOutBGM();
		}

		// 現在のフェーズを「ゲームオーバー」に変更
		ChangeCurrentPhase(ETutorialPhaseType::GameOver);
		break;
	}
	case EGameStateType::GameClear: {

		UnPossessOwner();
		SetupDisplayBattleHUD(false);

		// プレイヤーの状態を初期化する
		PausePlayer(EDyingDisplayStateType::Pause);

		AAudioManager* AudioManager = GetAudioManager();
		if (IsValid(AudioManager)) {
			AudioManager->FadeOutBGM();
			AudioManager->SetNextBGMSound(EBGMType::Result);
		}

		// 現在のフェーズを「ゲームクリア」に変更
		ChangeCurrentPhase(ETutorialPhaseType::GameClear);
		break;
	}
	}

	// 敵の現在の処理を停止し、巡回行動のみさせる
	EStageType StageType = BattleGameState->GetCurrentStageType();
	if (!IsValid(EnemyManager)) {
		return;
	}
	EnemyManager->StopEnemy(StageType);
}

/// <summary>
/// エネミーの準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::PrepareEnemies() {

	//------------------------------------------
	//レベルブループリント内から呼び出される処理
	//ゲーム開始中ならエネミーをリスポーンし、
	//エネミーを初期化する
	//------------------------------------------
	bool IsCheck = IsStart();
	if (!IsCheck) {
		return;
	}
	RespawnEnemies();

	// サウンドを通常BGMに切り替えておく
	AAudioManager* AudioManager = BattleController->GetAudioManager();
	if (IsValid(AudioManager)) {
		if (AudioManager->IsPlayingBattleBGM()) {

			AudioManager->FadeOutBGM();
			AudioManager->SetNextBGMSound(EBGMType::Game);
		}
	}

	// 警告ランプも消灯しておく
	if (!IsValid(WarningLampManager)) {
		return;
	}
	WarningLampManager->TurnOffLamp();
}

/// <summary>
/// ポーズメニュー操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::OperatePauseMenu() {

	//---------------------------------------------
	// ゲーム開始が押されているか、
	// ゲーム終了が押されているかを確認
	// 状況に応じてフェーズを遷移する
	//---------------------------------------------

	UPauseMenuWidget* PauseMenuWidget = GetPauseMenuWidget();
	if (!IsValid(PauseMenuWidget) || !IsValid(BattleController)) {
		return;
	}

	bool bCheckMove = BattleController->IsMoveMouseCursor();
	// マウス処理
	PauseMenuWidget->OperateMouse(bCheckMove);

	if (!PauseMenuWidget->IsDecide()) {
		return;
	}

	EOptionButtonType OptionButtonType = PauseMenuWidget->GetDecideOptionButtonType();
	switch (OptionButtonType) {
	case EOptionButtonType::Resume: {
		//-------------------------------
		// ゲーム再開
		//-------------------------------
		OnSwitchPause();

		break;
	}
	case EOptionButtonType::OperateList: {
		//-------------------------------
		// 操作一覧
		//-------------------------------
		UOperateListWidget* OperateListWidget = GetOperateListWidget();
		if (!IsValid(OperateListWidget)) {
			break;
		}
		OperateListWidget->Open();

		// 操作一覧用ガイドを開く
		UOperateListGuideWidget* OperateListGuideWidget = GetOperateListGuideWidget();
		if (IsValid(OperateListGuideWidget)) {
			OperateListGuideWidget->Open();
		}
		BattleController->AddInputType(EBattleInputType::OperateList);

		// 現在のフェーズを「操作一覧操作」に変更
		ChangeCurrentPhase(ETutorialPhaseType::OperateList);
		break;
	}
	case EOptionButtonType::AudioMenu: {
		//-------------------------------
		// オーディオメニュー
		//-------------------------------
		UAudioMenuWidget* AudioMenuWidget = GetAudioMenuWidget();
		AAudioManager* AudioManager = GetAudioManager();
		if (!IsValid(AudioMenuWidget) || !IsValid(AudioManager)) {
			break;
		}

		// オーディオメニュー画面の設定を行い、オプション画面を開く
		AudioMenuWidget->SetupAudioVolume(AudioManager->GetAudioVolume());
		AudioMenuWidget->Open();
		// オーディオガイドを開く
		UAudioGuideWidget* AudioGuideWidget = GetAudioGuideWidget();
		if (IsValid(AudioGuideWidget)) {
			AudioGuideWidget->Open();
		}
		BattleController->AddInputType(EBattleInputType::Audio);

		// 現在のフェーズを「オプション操作」に変更
		ChangeCurrentPhase(ETutorialPhaseType::OptionOperation);
		break;
	}
	case EOptionButtonType::ReturnTitle: {
		//-------------------------------
		// タイトルへ戻る
		//-------------------------------
		OnSwitchPause();
		UnPossessOwner();

		AAudioManager* AudioManager = GetAudioManager();
		if (IsValid(AudioManager)) {
			AudioManager->FadeOutBGM();
		}

		// フェーズ切替の種類を「レベル切替」に変更する
		ChangeSwitchPhaseType(ETutorialSwitchPhaseType::ChangeLevel);
		// 現在のフェーズを「フェードアウト」に変更
		ChangeCurrentPhase(ETutorialPhaseType::FadeOut);
		break;
	}
	default: {

		break;
	}
	}

	// 決定した処理をリセットしておく
	PauseMenuWidget->ResetOperation();
}

/// <summary>
/// オプションメニュー操作
/// </summary>
/// <param name=""></param>
/// <returns> true:操作終了 </returns>
bool ATutorial::OperateOptionMenu() {

	//----------------------------------------------------
	//オプション画面で戻る操作が行われた際に
	//ウィジェットを閉じる操作を行う
	//その後、レベルの現在のフェーズを変更する
	//----------------------------------------------------

	UAudioMenuWidget* AudioMenuWidget = GetAudioMenuWidget();
	if (!IsValid(AudioMenuWidget) || !IsValid(BattleController)) {
		return false;
	}

	bool bCheckMove = BattleController->IsMoveMouseCursor();
	// マウス処理
	AudioMenuWidget->OperateMouse(bCheckMove);

	if (!AudioMenuWidget->IsDecide() && !AudioMenuWidget->IsReturn()) {
		return false;
	}

	AAudioManager* AudioManager = GetAudioManager();
	if (IsValid(AudioManager)) {
		if (AudioMenuWidget->IsDecide()) {
			// 新しい音量を設定する
			AudioManager->SetAudioVolume(AudioMenuWidget->GetAfterSettingAudioVolume());
		}
		// 音量を更新する
		// ※決定操作も戻る操作も更新しておく
		AudioManager->UpdateVolume();
	}

	// オーディオガイドを閉じる
	UAudioGuideWidget* AudioGuideWidget = GetAudioGuideWidget();
	if (IsValid(AudioGuideWidget)) {
		if (AudioGuideWidget->IsDisplay()) {
			AudioGuideWidget->Closed();
		}
	}

	// オプション画面を閉じる
	AudioMenuWidget->Closed();
	BattleController->DeleteInputType();

	return true;
}

/// <summary>
/// 操作一覧操作
/// </summary>
/// <param name=""></param>
/// <returns> true:操作終了 </returns>
bool ATutorial::OperateOperateList() {

	//----------------------------------------------------
	//操作一覧画面で戻る操作が行われた際に
	//ウィジェットを閉じる操作を行う
	//その後、レベルの現在のフェーズを変更する
	//----------------------------------------------------

	UOperateListWidget* OperateListWidget = GetOperateListWidget();
	if (!IsValid(OperateListWidget) || !IsValid(BattleController)) {
		return false;
	}

	if (!OperateListWidget->IsReturn()) {
		return false;
	}

	// 操作一覧ガイドを閉じる
	UOperateListGuideWidget* OperateListGuideWidget = GetOperateListGuideWidget();
	if (IsValid(OperateListGuideWidget)) {
		if (OperateListGuideWidget->IsDisplay()) {
			OperateListGuideWidget->Closed();
		}
	}

	// 操作一覧画面を閉じる
	OperateListWidget->Closed();
	BattleController->DeleteInputType();

	return true;
}

/// <summary>
/// ゲームオーバー演出処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns> true:処理終了 </returns>
bool ATutorial::PerformGameOver(float DeltaTime) {

	switch (CurrentResultPhase) {
	case ETutorialResultPhaseType::Wait: {
		//---------------------------
		//待機フェーズ
		//---------------------------
		bool IsFinish = DestroyPlayer();
		if (!IsFinish) {
			break;
		}

		// フェーズ切替の種類を「リトライ」に変更する
		ChangeSwitchPhaseType(ETutorialSwitchPhaseType::Retry);
		// 現在のリザルトフェーズを「リトライ」に変更
		ChangeCurrentResultPhase(ETutorialResultPhaseType::Finish);
		break;
	}
	case ETutorialResultPhaseType::Finish: {
		//---------------------------
		//終了フェーズ
		//---------------------------
		return true;
	}
	}

	return false;
}

/// <summary>
/// ゲームクリア演出処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns> true:処理終了 </returns>
bool ATutorial::PerformGameClear(float DeltaTime) {

	switch (CurrentResultPhase) {
	case ETutorialResultPhaseType::Wait: {
		//---------------------------
		//待機フェーズ
		//---------------------------
		AccumulateTime += DeltaTime;
		if (AccumulateTime < ResultWaitTime) {
			break;
		}
		AccumulateTime = ResetTime;

		// 現在のリザルトフェーズを「リザルト表示」に変更
		ChangeCurrentResultPhase(ETutorialResultPhaseType::DisplayWidget);
		break;
	}
	case ETutorialResultPhaseType::DisplayWidget: {
		//---------------------------
		//リザルト表示フェーズ
		//---------------------------
		UResultWidget* ResultWidget = GetResultWidget(EWidgetType::TutorialClear);
		if (!IsValid(ResultWidget)) {
			break;
		}
		// リザルト画面を開く
		ResultWidget->Open();
		ResultWidget->StartAnimation();
		BattleController->AddInputType(EBattleInputType::TutorialClear);

		// プレイヤーの動きを停止する
		if (IsValid(PlayerManager)) {
			AOperatePlayer* Player = PlayerManager->GetOperatePlayer();
			if (IsValid(Player)) {
				Player->GetCharacterMovement()->StopMovementImmediately();
			}
		}

		bGameClear = true;

		// 現在のリザルトフェーズを「アニメーション待機」に変更
		ChangeCurrentResultPhase(ETutorialResultPhaseType::WaitAnim);
		break;
	}
	case ETutorialResultPhaseType::WaitAnim: {
		//---------------------------
		//アニメーション待機フェーズ
		//---------------------------
		bool IsFinish = true;
		UResultWidget* ResultWidget = GetResultWidget(EWidgetType::TutorialClear);
		if (!IsValid(ResultWidget)) {
			break;
		}

		IsFinish = ResultWidget->IsAnimFinish();
		if (!IsFinish) {
			break;
		}
		// リザルトウィジェットのフラグリセット
		ResultWidget->ResetAnimFlag();

		// リザルトガイドを開く
		UResultGuideWidget* ResultGuideWidget = GetResultGuideWidget();
		if (IsValid(ResultGuideWidget)) {
			ResultGuideWidget->Open();
		}
		// マウスカーソルを表示
		BattleController->bShowMouseCursor = true;

		// 現在のリザルトフェーズを「操作」に変更
		ChangeCurrentResultPhase(ETutorialResultPhaseType::Operate);
		break;
	}
	case ETutorialResultPhaseType::Operate: {
		//---------------------------
		//操作フェーズ
		//---------------------------
		bool IsFinish = OperateGameClear();
		if (!IsFinish) {
			break;
		}

		AAudioManager* AudioManager = GetAudioManager();
		if (IsValid(AudioManager)) {
			AudioManager->FadeOutBGM();
		}

		// リザルトウィジェットを表示した状態のまま
		// フェードアウトさせるため、
		// ここではリザルトウィジェットを非表示にしない

		// マウスカーソルを非表示
		BattleController->bShowMouseCursor = false;
		// 現在のリザルトフェーズを「終了」に変更
		ChangeCurrentResultPhase(ETutorialResultPhaseType::Finish);
		break;
	}
	case ETutorialResultPhaseType::Finish: {
		//---------------------------
		//終了フェーズ
		//---------------------------
		return true;
	}
	}

	return false;
}

/// <summary>
/// ゲームクリア操作
/// </summary>
/// <param name=""></param>
/// <returns> true:操作終了 </returns>
bool ATutorial::OperateGameClear() {

	//----------------------------------------------------
	//ゲームクリア画面の操作
	//決定処理にて次のフェーズに進める
	//----------------------------------------------------

	UResultWidget* ResultWidget = GetResultWidget(EWidgetType::TutorialClear);
	if (!IsValid(ResultWidget) || !IsValid(BattleController)) {
		return false;
	}

	bool bCheckMove = BattleController->IsMoveMouseCursor();
	// マウス操作処理
	ResultWidget->OperateMouse(bCheckMove);

	if (!ResultWidget->IsDecide()) {
		return false;
	}

	// リザルトガイドを閉じる
	UResultGuideWidget* ResultGuideWidget = GetResultGuideWidget();
	if (IsValid(ResultGuideWidget)) {
		if (ResultGuideWidget->IsDisplay()) {
			ResultGuideWidget->Closed();
		}
	}

	EButtonType ButtonType = ResultWidget->GetDecideButtonType();
	switch (ButtonType) {
	case EButtonType::Retry: {

		// フェーズ切替の種類を「リトライ」に変更する
		ChangeSwitchPhaseType(ETutorialSwitchPhaseType::Retry);
		break;
	}
	case EButtonType::ReturnTitle: {

		// フェーズ切替の種類を「レベル切替」に変更する
		ChangeSwitchPhaseType(ETutorialSwitchPhaseType::ChangeLevel);
		break;
	}
	}
	BattleController->DeleteInputType();

	return true;
}

/// <summary>
/// フェードウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UFadeWidget* ATutorial::GetFadeWidget() {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetFadeWidget();
}

/// <summary>
/// 照準ウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UCrossHairWidget* ATutorial::GetCrossHairWidget() {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetCrossHairWidget();
}

/// <summary>
/// 照準の中心点ウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> 照準の中心点ウィジェット </returns>
UCenterPointWidget* ATutorial::GetCenterPointWidget() {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetCenterPointWidget();
}

/// <summary>
/// ポーズメニューウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> ポーズメニューウィジェット </returns>
UPauseMenuWidget* ATutorial::GetPauseMenuWidget() {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetPauseMenuWidget();
}

/// <summary>
/// オーディオメニューウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> オーディオメニューウィジェット </returns>
UAudioMenuWidget* ATutorial::GetAudioMenuWidget() {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetAudioMenuWidget();
}

/// <summary>
/// リザルトウィジェットの取得
/// </summary>
/// <param name="WidgetType"> ウィジェットの種類 </param>
/// <returns></returns>
UResultWidget* ATutorial::GetResultWidget(EWidgetType WidgetType) {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetResultWidget(WidgetType);
}

/// <summary>
/// バトルHUDウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> バトルHUDウィジェット </returns>
UBattleWidget* ATutorial::GetBattleWidget() {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return  BattleController->GetBattleWidget();
}

/// <summary>
/// ミニマップウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> ミニマップウィジェット </returns>
UMiniMapWidget* ATutorial::GetMiniMapWidget() {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetMiniMapWidget();
}

/// <summary>
/// エリアマップウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> ミニマップウィジェット </returns>
UAreaMapWidget* ATutorial::GetAreaMapWidget() {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetAreaMapWidget();
}

/// <summary>
/// ポーズウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> ポーズウィジェット </returns>
UPauseWidget* ATutorial::GetPauseWidget() {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetPauseWidget();
}

/// <summary>
/// 操作一覧ウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> 操作一覧ウィジェット </returns>
UOperateListWidget* ATutorial::GetOperateListWidget() {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetOperateListWidget();
}

/// <summary>
/// カメラ演出ガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> カメラ演出ガイドウィジェット </returns>
UCameraGuideWidget* ATutorial::GetCameraGuideWidget() {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetCameraGuideWidget();
}

/// <summary>
/// カメラ演出時のポーズガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> カメラ演出時のポーズガイドウィジェット </returns>
UPauseDirectionGuideWidget* ATutorial::GetPauseDirectionGuideWidget() {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetPauseDirectionGuideWidget();
}

/// <summary>
/// オーディオガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> オーディオガイドウィジェット </returns>
UAudioGuideWidget* ATutorial::GetAudioGuideWidget() {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetAudioGuideWidget();
}

/// <summary>
/// マップガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> マップガイドウィジェット </returns>
UMapGuideWidget* ATutorial::GetMapGuideWidget() {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetMapGuideWidget();
}

/// <summary>
/// ポーズガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> ポーズガイドウィジェット </returns>
UPauseGuideWidget* ATutorial::GetPauseGuideWidget() {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetPauseGuideWidget();
}

/// <summary>
/// リザルトガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> リザルトガイドウィジェット </returns>
UResultGuideWidget* ATutorial::GetResultGuideWidget() {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetResultGuideWidget();
}

/// <summary>
/// 操作一覧ガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> 操作一覧ガイドウィジェット </returns>
UOperateListGuideWidget* ATutorial::GetOperateListGuideWidget() {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetOperateListGuideWidget();
}

/// <summary>
/// オーディオマネージャの取得
/// </summary>
/// <param name=""></param>
/// <returns> オーディオマネージャ </returns>
AAudioManager* ATutorial::GetAudioManager() {

	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetAudioManager();
}

/// <summary>
/// プレイヤーマネージャの取得
/// </summary>
/// <param name=""></param>
/// <returns> プレイヤーマネージャ </returns>
APlayerManager* ATutorial::GetPlayerManager() {

	return PlayerManager;
}

/// <summary>
/// プレイヤーを消滅させる
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了 </returns>
bool ATutorial::DestroyPlayer() {

	//---------------------------------------
	//プレイヤーが消滅したら
	//プレイヤーと武器を破壊する
	//---------------------------------------

	if (!IsValid(PlayerManager)) {
		return false;
	}

	AOperatePlayer* Player = PlayerManager->GetOperatePlayer();
	if (!IsValid(Player)) {
		return true;
	}

	bool IsDisappearance = Player->IsDisappearance();
	if (IsDisappearance) {
		Player->DestroyWeapon();
		Player->Destroy();
		Player = nullptr;
		return true;
	}

	return false;
}

/// <summary>
/// プレイヤーのリスポーン
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::RespawnPlayer() {

	// プレイヤーを開始位置にリスポーンする
	// プレイヤーが破棄されていなかった場合は
	// 強制的にプレイヤーと武器の破棄を行う
	if (!IsValid(PlayerManager)) {
		return;
	}
	
	AOperatePlayer* Player = PlayerManager->GetOperatePlayer();
	if (IsValid(Player)) {
		PlayerManager->DisposePlayer();
	}

	CreatePlayer();
}

/// <summary>
/// エネミーのリスポーン
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::RespawnEnemies() {

	if (!IsValid(EnemyManager) || !IsValid(BattleController)) {
		return;
	}

	// エネミーを開始位置にリスポーンする
	// エネミーが破棄されていなかった場合は
	// 強制的にエネミーと武器の破棄を行う
	EnemyManager->DisposeEnemies();
	EnemyManager->Initialize();
	CreateEnemies();

	// マップ情報の初期化
	UMiniMapWidget* MiniMapWidget = BattleController->GetMiniMapWidget();
	UAreaMapWidget* AreaMapWidget = BattleController->GetAreaMapWidget();
	if (!IsValid(MiniMapWidget) || !IsValid(AreaMapWidget)) {
		return;
	}
	MiniMapWidget->ResetEnemyData();
	AreaMapWidget->ResetEnemyData();
}

/// <summary>
/// リザルトウィジェットの初期化
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::InitializeResult() {

	// リザルトフェーズを「待機」に戻す
	ChangeCurrentResultPhase(ETutorialResultPhaseType::Wait);

	UResultWidget* ResultWidget = BattleController->GetResultWidget(EWidgetType::TutorialClear);
	if (!IsValid(ResultWidget)) {
		return;
	}

	ResultWidget->InitializeWidget();
	ResultWidget->InitializeResultWidget();
}

/// <summary>
/// バトルHUDウィジェットの初期化
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::InitializeBattleHUD() {

	// 武器HUD情報の初期化
	UBattleWidget* BattleWidget = BattleController->GetBattleWidget();
	if (!IsValid(BattleWidget)) {
		return;
	}
	BattleWidget->ResetWeaponIcon();
	SetupBattleHUDWeapon();

	// ミニマップHUD情報の初期化
	UMiniMapWidget* MiniMapWidget = BattleController->GetMiniMapWidget();
	if (!IsValid(MiniMapWidget)) {
		return;
	}
	MiniMapWidget->ResetMapIcon();
	MiniMapWidget->SetupWidget();

	// エリアマップ情報の初期化
	UAreaMapWidget* AreaMapWidget = BattleController->GetAreaMapWidget();
	if (!IsValid(AreaMapWidget)) {
		return;
	}
	AreaMapWidget->ResetMapIcon();
	AreaMapWidget->SetupWidget();

	if (!IsValid(BattleGameState)) {
		return;
	}
	AreaMapWidget->SwitchAreaMap(BattleGameState->GetCurrentStageType());
}

/// <summary>
/// アイテムのリセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::ResetPickupItem() {

	if (!IsValid(PickupItemManager)) {
		return;
	}

	PickupItemManager->ResetPickupItem();
}

/// <summary>
/// プレイヤーのポーズ処理
/// </summary>
/// <param name="StateType"> 設定の種類 </param>
/// <returns></returns>
void ATutorial::PausePlayer(EDyingDisplayStateType StateType) {

	if (!IsValid(PlayerManager)) {
		return;
	}
	AOperatePlayer* Player = PlayerManager->GetOperatePlayer();
	if (!IsValid(Player)) {
		return;
	}
	Player->PauseDying(StateType);
}

/// <summary>
/// プレイヤーのリセット処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::ResetPlayer() {

	if (!IsValid(PlayerManager)) {
		return;
	}
	AOperatePlayer* Player = PlayerManager->GetOperatePlayer();
	if (!IsValid(Player)) {
		return;
	}
	Player->CancelAim();
}

/// <summary>
/// ポーズ画面の表示・非表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::OnSwitchPause() {

	// 現在のフェーズがプレイ中でもポーズ中でも
	// カメラ演出中でもない場合は処理終了
	if (!IsPlay() && !IsCameraDirection() && !IsPause()) {
		return;
	}

	if (!IsValid(BattleController)) {
		return;
	}

	switch (CurrentPhase) {
	case ETutorialPhaseType::Play: {

		UPauseMenuWidget* PauseMenuWidget = GetPauseMenuWidget();
		if (!IsValid(PauseMenuWidget)) {
			return;
		}
		// ポーズメニュー画面を開く
		PauseMenuWidget->Open();
		
		// ポーズガイドを開く
		UPauseGuideWidget* PauseGuideWidget = GetPauseGuideWidget();
		if (IsValid(PauseGuideWidget)) {
			PauseGuideWidget->Open();
		}
		BattleController->AddInputType(EBattleInputType::PausePlay);
		// マウスカーソルを表示
		BattleController->bShowMouseCursor = true;
		// ゲームをポーズ状態にする
		UGameplayStatics::SetGamePaused(GetWorld(), true);

		// 現在のフェーズを「ポーズ」フェーズに変更
		ChangeCurrentPhase(ETutorialPhaseType::Pause);
		// ポーズの種類を「プレイポーズ」に変更
		ChangeCurrentPauseType(ETutorialPauseType::Play);
		break;
	}
	case ETutorialPhaseType::CameraDirection: {

		UPauseWidget* PauseWidget = GetPauseWidget();
		UCameraGuideWidget* GuideWidget = BattleController->GetCameraGuideWidget();
		UPauseDirectionGuideWidget* PauseGuideWidget = GetPauseDirectionGuideWidget();
		if (IsValid(PauseWidget)) {
			// 演出ポーズウィジェットを開く
			PauseWidget->Open();
		}
		if (IsValid(GuideWidget)) {
			// カメラ演出操作ガイドを非表示にする
			if (GuideWidget->IsDisplay()) {
				GuideWidget->Closed();
			}
		}
		if (IsValid(PauseGuideWidget)) {
			// 演出ポーズ時のガイドウィジェットを開く
			if (!PauseGuideWidget->IsDisplay()) {
				PauseGuideWidget->Open();
			}
		}

		BattleController->AddInputType(EBattleInputType::PauseDirection);
		// ゲームをポーズ状態にする
		UGameplayStatics::SetGamePaused(GetWorld(), true);

		// 現在のフェーズを「ポーズ」フェーズに変更
		ChangeCurrentPhase(ETutorialPhaseType::Pause);
		// ポーズの種類を「演出ポーズ」に変更
		ChangeCurrentPauseType(ETutorialPauseType::CameraDirection);
		break;
	}
	case ETutorialPhaseType::Pause: {

		switch (CurrentPauseType) {
		case ETutorialPauseType::Play: {

			// ポーズメニュー画面を閉じる
			UPauseMenuWidget* PauseMenuWidget = GetPauseMenuWidget();
			if (!IsValid(PauseMenuWidget)) {
				return;
			}

			// ポーズガイドを閉じる
			UPauseGuideWidget* PauseGuideWidget = GetPauseGuideWidget();
			if (IsValid(PauseGuideWidget)) {
				if (PauseGuideWidget->IsDisplay()) {
					PauseGuideWidget->Closed();
				}
			}

			PauseMenuWidget->Closed();
			// マウスカーソルを非表示
			BattleController->bShowMouseCursor = false;

			// 現在のフェーズを「プレイ中」フェーズに変更
			ChangeCurrentPhase(ETutorialPhaseType::Play);
			break;
		}
		case ETutorialPauseType::CameraDirection: {

			// 演出ポーズ画面を閉じる
			UPauseWidget* PauseWidget = GetPauseWidget();
			UCameraGuideWidget* GuideWidget = BattleController->GetCameraGuideWidget();
			UPauseDirectionGuideWidget* PauseGuideWidget = GetPauseDirectionGuideWidget();
			if (IsValid(PauseWidget)) {
				PauseWidget->Closed();
			}

			if (IsValid(GuideWidget)) {
				// カメラ演出操作ガイドを表示
				if (!GuideWidget->IsDisplay()) {
					GuideWidget->Open();
				}
			}
			if (IsValid(PauseGuideWidget)) {
				if (GuideWidget->IsDisplay()) {
					// 演出ポーズ時のガイドウィジェットを非表示
					PauseGuideWidget->Closed();
				}
			}

			// 現在のフェーズを「カメラ演出」フェーズに変更
			ChangeCurrentPhase(ETutorialPhaseType::CameraDirection);
			break;
		}
		}

		BattleController->DeleteInputType();
		UGameplayStatics::SetGamePaused(GetWorld(), false);
		// ポーズの種類を「設定なし」に変更
		ChangeCurrentPauseType(ETutorialPauseType::None);
		break;
	}
	}
}

/// <summary>
/// マップ画面の表示・非表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::OnSwitchMap() {

	// 現在のフェーズがプレイ中でもマップ中でもない場合は処理終了
	if (!IsPlay() && !IsMap()) {
		return;
	}

	UBattleWidget* BattleWidget = GetBattleWidget();
	UMiniMapWidget* MiniMapWidget = GetMiniMapWidget();
	UAreaMapWidget* AreaMapWidget = GetAreaMapWidget();
	if (!IsValid(MiniMapWidget) || !IsValid(AreaMapWidget) ||
		!IsValid(BattleWidget) || !IsValid(PlayerManager)) {
		// マップガイドは無くても進行可能なためここではチェックしない
		return;
	}

	AOperatePlayer* Player = PlayerManager->GetOperatePlayer();
	if (!IsValid(Player)) {
		return;
	}
	
	switch (CurrentPhase) {
	case ETutorialPhaseType::Play: {

		// コントローラからの入力を無効にしておく
		SetupOperatePlayer(false);

		// 画面上のHUDを非表示にしておく
		BattleWidget->Hide();
		MiniMapWidget->Hide();

		// マップ画面を開く
		AreaMapWidget->Display();
		UMapGuideWidget* MapGuideWidget = GetMapGuideWidget();
		if (IsValid(MapGuideWidget)) {
			MapGuideWidget->Display();
		}
		BattleController->AddInputType(EBattleInputType::Map);

		// 現在のフェーズを「マップ表示」フェーズに変更
		ChangeCurrentPhase(ETutorialPhaseType::Map);
		break;
	}
	case ETutorialPhaseType::Map: {

		// コントローラからの入力を有効にしておく
		SetupOperatePlayer(true);

		// 画面上のHUDを表示する
		BattleWidget->Display();
		MiniMapWidget->Display();

		// マップ画面を閉じる
		UMapGuideWidget* MapGuideWidget = GetMapGuideWidget();
		if (IsValid(MapGuideWidget)) {
			MapGuideWidget->Hide();
		}
		AreaMapWidget->Hide();
		BattleController->DeleteInputType();

		// 現在のフェーズを「プレイ中」フェーズに変更
		ChangeCurrentPhase(ETutorialPhaseType::Play);
		break;
	}
	}
}

/// <summary>
/// 敵の視覚情報ON・OFF
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::OnSetupSight() {

	//-----------------------------------------
	// ※デバック時のみ実行可
	//-----------------------------------------
	bool IsPackage = UKismetSystemLibrary::IsPackagedForDistribution();
	if (IsPackage) {
		return;
	}
	if (!IsValid(BattleGameState) || !IsValid(EnemyManager)) {
		return;
	}

	bool IsSight = BattleGameState->IsEnemySight();
	if (IsSight) {
		BattleGameState->SetupDebugEnemySight(false);
		EnemyManager->SetupDebugEnemySight(false);

		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, TEXT("EnemySight : FALSE"));
	}
	else {
		BattleGameState->SetupDebugEnemySight(true);
		EnemyManager->SetupDebugEnemySight(true);

		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, TEXT("EnemySight : TRUE"));
	}
}

/// <summary>
/// 武器の生成処理
/// </summary>
/// <param name="WeaponType"> 武器の種類 </param>
/// <returns> 武器 </returns>
AWeaponBase* ATutorial::OnCreateWeapon(EWeaponType WeaponType) {

	return WeaponFactory->CreateWeapon(WeaponType);
}

/// <summary>
/// セーブデータに指定の武器があるかどうか
/// </summary>
/// <param name="WeaponType"> 武器の種類 </param>
/// <returns> true:ある </returns>
bool ATutorial::OnIsSavePossessWeapon(EWeaponType WeaponType) {

	return false;
}

/// <summary>
/// HUDの武器入れ替えアニメーション開始
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::OnStartSwitchWeaponAnimation() {

	UBattleWidget* Widget = GetBattleWidget();
	if (!IsValid(Widget)) {
		return;
	}
	Widget->PlaySwitchStartWeaponAnimation();
}

/// <summary>
/// HUDの武器入れ替え処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::OnSwitchWeapon() {

	//----------------------------------------
	//武器アイコンの入れ替えを行い、
	//その後アニメーションを実行する
	//----------------------------------------

	UBattleWidget* Widget = GetBattleWidget();
	if (!IsValid(Widget)) {
		return;
	}
	Widget->SwitchWeaponIcon();
	Widget->PlaySwitchEndWeaponAnimation();
}

/// <summary>
/// 太陽と雲の設定を変更する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::OnChangeSettingSunAndCloud() {

	// 何もしない
}

/// <summary>
/// カメラ演出実行開始処理
/// </summary>
/// <param name="TriggerBox"> トリガーボックス </param>
/// <returns></returns>
void ATutorial::StartCameraDirection(ACameraDirectionTriggerBox* TriggerBox) {

	//----------------------------------------------
	//レベルブループリント内から呼び出される処理
	//プレイヤーがゲーム入り口に入った際に実行
	//1回目のみカメラ演出を実行する
	//----------------------------------------------

	if (!IsValid(TriggerBox)) {
		return;
	}

	if (bIsCameraDirection) {

		// カメラ演出を実行するトリガーボックスを取得
		ExecCamraraDirectionTrigger = TriggerBox;

		// トリガーにこのクラスのアドレスを渡し、カメラ演出の設定を行う
		ExecCamraraDirectionTrigger->SetEventInterface(this);
		ExecCamraraDirectionTrigger->SetupCameraDirection();

		// 現在のフェーズを「カメラ演出」フェーズに変更
		ChangeCurrentPhase(ETutorialPhaseType::CameraDirection);
		// ゲーム中のHUDを非表示にする
		SetupDisplayBattleHUD(false);
		// プレイヤー操作を無効化
		SetupOperatePlayer(false);

		bIsCameraDirection = false;

		// 瀕死状態のウィジェット非表示とサウンド一時停止
		PausePlayer(EDyingDisplayStateType::Pause);
		ResetPlayer();
	}
	else {

		if (!IsValid(EnemyManager)) {
			return;
		}
		AppearanceEnemies();
	}

	// ゲーム開始にしておく
	ChangeCurrentGamePhase(EGamePhaseType::Start);
}

/// <summary>
/// エネミーの表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::AppearanceEnemies() {

	TArray<int32> IndexList = EnemyManager->GetEnemyIndexList();
	for (int32 Index : IndexList) {

		AEnemy* Enemy = EnemyManager->GetEnemy(Index);
		if (!IsValid(Enemy)) {
			continue;
		}
		bool IsCheck = Enemy->IsWaitAppearance();
		if (!IsCheck) {
			continue;
		}
		Enemy->ChangeDissolvePhase(EDissolvePhase::Appearance);
	}
}

/// <summary>
/// カメラ演出時のカメラ切り替え
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::OnSwitchDirectionCamera() {

	if (!IsValid(ExecCamraraDirectionTrigger)) {
		return;
	}

	SwitchCameraDirection();
}

/// <summary>
/// カメラ演出実行終了処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::OnFinishCameraDirection() {

	// カメラ演出を実行するトリガーボックスのアドレスを破棄
	ExecCamraraDirectionTrigger = nullptr;
	// 現在のフェーズを「プレイ中」フェーズに変更
	ChangeCurrentPhase(ETutorialPhaseType::Play);
	// ゲーム中のHUDを表示する
	SetupDisplayBattleHUD(true);
	// プレイヤー操作を有効化
	SetupOperatePlayer(true);

	// 瀕死状態のウィジェット表示とサウンド再開
	PausePlayer(EDyingDisplayStateType::Resume);
}

/// <summary>
/// 演出スキップ処理
/// </summary>
/// <param name="Value"> 押したかどうか </param>
/// <returns></returns>
void ATutorial::OnSkipDirection(float Value) {

	// 演出処理中以外なら処理終了
	if (!IsCameraDirection()) {
		return;
	}

	if (!IsValid(ExecCamraraDirectionTrigger)) {
		return;
	}
	bool bFinishPhase = ExecCamraraDirectionTrigger->IsFinishPhase();
	if (bFinishPhase) {
		KeepPressValue = ResetKeepPressValue;
		return;
	}

	//------------------------------------
	// 一定時間指定キーを長押ししたら
	// ムービーをキャンセルする処理を実行
	//------------------------------------
	if (Value == ResetKeepPressValue) {
		KeepPressValue = ResetKeepPressValue;
	}
	else {
		KeepPressValue += Value;
	}

	// スキップゲージの値(割合)を計算し、ウィジェットに渡す
	UCameraGuideWidget* CameraGuideWidget = GetCameraGuideWidget();
	if (IsValid(CameraGuideWidget)) {
		float SkipGaugeValue = KeepPressValue / NeedKeepPressValue;
		CameraGuideWidget->AdjustSkipGaugeValue(SkipGaugeValue);
	}

	if (KeepPressValue < NeedKeepPressValue) {
		return;
	}

	KeepPressValue = ResetKeepPressValue;
	ExecCamraraDirectionTrigger->SetupCancel();

	// 出現し切っていない敵の表示処理
	AppearanceEnemies();
}

/// <summary>
/// リザルト画像をセットする
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::SetupResultTexture(UTexture2DDynamic* ResultTexture) {

	UResultWidget* ResultWidget = GetResultWidget(EWidgetType::TutorialClear);
	if (!IsValid(ResultWidget)) {
		return;
	}
	ResultWidget->SetupTexture(ResultTexture);
}

/// <summary>
/// 演出用のカメラ切替処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATutorial::SwitchCameraDirection_Implementation() {

}