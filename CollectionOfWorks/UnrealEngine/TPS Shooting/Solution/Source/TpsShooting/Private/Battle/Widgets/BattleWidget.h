﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/UiAssignmentWidget.h"
#include "Battle/Widgets/BattleHUDItem/BattleHUDWeaponIcon/BattleHUDWeaponIcon.h"
#include "Battle/Weapons/WeaponBase.h"
#include "Engine/DataTable.h"
#include "BattleWidget.generated.h"


/// <summary>
/// 武器アイコンテーブル
/// </summary>
USTRUCT(BlueprintType)
struct FWeaponIconList : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// 武器の種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EWeaponType WeaponType;

	/// <summary>
	/// 武器アイコン
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterial* WeaponMaterial;
};

/// <summary>
/// バトルレベルのHUD情報クラス
/// </summary>
UCLASS()
class UBattleWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UBattleWidget();

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeWidget();

	/// <summary>
	/// 装備中武器アイコンの設定
	/// </summary>
	/// <param name="WeaponType"> 武器の種類 </param>
	/// <returns></returns>
	void SetEquipWeaponIcon(EWeaponType WeaponType);

	/// <summary>
	/// サブ武器アイコンの設定
	/// </summary>
	/// <param name="WeaponType"> 武器の種類 </param>
	/// <returns></returns>
	void SetSecondWeaponIcon(EWeaponType WeaponType);

	/// <summary>
	/// 武器アイコン設定のリセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetWeaponIcon();

	/// <summary>
	/// 装填弾薬数を設定
	/// </summary>
	/// <param name="Ammos"> 弾薬数 </param>
	/// <returns></returns>
	void SetLoadedAmmos(int32 Ammos);

	/// <summary>
	/// 所持弾薬数を設定
	/// </summary>
	/// <param name="Ammos"> 弾薬数 </param>
	/// <returns></returns>
	void SetPossessAmmos(int32 Ammos);

	/// <summary>
	/// 表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Display();

	/// <summary>
	/// 非表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Hide();

	/// <summary>
	/// 武器アイコンの切替
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "BattleWidget")
	void SwitchWeaponIcon();

	/// <summary>
	/// 武器切り替え開始アニメーション
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="BattleWidget")
	void PlaySwitchStartWeaponAnimation();

	/// <summary>
	/// 武器切り替え終了アニメーション
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void PlaySwitchEndWeaponAnimation();

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

private:

	/// <summary>
	/// 武器アイコン生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UBattleHUDWeaponIcon* CreateWeaponIcon();

	/// <summary>
	/// 武器アイコンの設定
	/// </summary>
	/// <param name="WeaponType"> 武器の種類 </param>
	/// <param name="Material"> マテリアル </param>
	/// <returns></returns>
	void SetupWeaponIcon(EWeaponType WeaponType, UMaterial* Material);

	/// <summary>
	/// サブ武器欄の非表示設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void HideSecondWeapon();

	/// <summary>
	/// サブ武器欄の表示設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void DisplaySecondWeapon();

	/// <summary>
	/// 武器切り替え開始アニメーション
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PlaySwitchStartWeaponAnimation_Implementation();

	/// <summary>
	/// 武器切り替え終了アニメーション
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PlaySwitchEndWeaponAnimation_Implementation();

public:

	/// <summary>
	/// 武器アイコンテーブル
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BattleWidget")
	UDataTable* WeaponIconTable;

private:

	/// <summary>
	/// 武器アイコンリスト
	/// </summary>
	UPROPERTY()
	TMap<EWeaponType, UBattleHUDWeaponIcon*> WeaponIconList;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// ホリゾンタルボックス サブ武器設定
	/// </summary>
	const FName HorizontalBoxSecondWeapon = "HorizontalBox_SecondWeapon";

	/// <summary>
	/// サイズボックス 装備中の武器設定
	/// </summary>
	const FName SizeBoxEquipWeapon = "SizeBox_EquipWeapon";
	
	/// <summary>
	/// サイズボックス サブ武器設定
	/// </summary>
	const FName SizeBoxSecondWeapon = "SizeBox_SecondWeapon";

	/// <summary>
	/// テキストボックス 装填弾薬
	/// </summary>
	const FName TextBlockLoadedAmmos = "TextBlock_LoadedAmmos";
	
	/// <summary>
	/// テキストボックス 装填弾薬
	/// </summary>
	const FName TextBlockPossessAmmos = "TextBlock_PossessAmmos";

	/// <summary>
	/// イメージ 武器アイコン設定先
	/// </summary>
	const FName ImageWeaponName = "Image_Weapon";

	/// <summary>
	/// 最小インデックス
	/// </summary>
	const int32 MinIndex = 0;

	/// <summary>
	/// テクスチャ 装備武器アイコンサイズ
	/// </summary>
	const FVector2D EquipWeaponIconSize = FVector2D(240, 80);

	/// <summary>
	/// テクスチャ サブ武器アイコンサイズ
	/// </summary>
	const FVector2D SecondWeaponIconSize = FVector2D(120, 40);

	/// <summary>
	/// 表示しない
	/// </summary>
	const float Transparent = 0.0f;

	/// <summary>
	/// 表示する
	/// </summary>
	const float Opacity = 1.0f;
};
