﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Widgets/BattleHUDItem/BattleHUDItemBase.h"

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleHUDItemBase::NativeConstruct() {

}

/// <summary>
/// HUDアイテムの種類のセット
/// </summary>
/// <param name="ItemType"> HUDアイテムの種類 </param>
/// <returns></returns>
void UBattleHUDItemBase::SetBattleHUDItemType(EBattleHUDItemType ItemType) {

	HUDItemType = ItemType;
}

/// <summary>
/// HUDアイテムの種類を取得
/// </summary>
/// <param name=""></param>
/// <returns> HUDアイテムの種類 </returns>
EBattleHUDItemType UBattleHUDItemBase::GetBattleHUDItemType() {

	return HUDItemType;
}

/// <summary>
/// 表示設定
/// </summary>
/// <param name="bDisplay"> 表示有無 </param>
/// <returns></returns>
void UBattleHUDItemBase::SetDisplay(bool bDisplay) {

	bIsDisplay = bDisplay;
}

/// <summary>
/// 表示するかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:表示する </returns>
bool UBattleHUDItemBase::IsDisplay() {

	return bIsDisplay;
}

/// <summary>
/// 武器アイコンのセット
/// </summary>
/// <param name="Material"> マテリアル </param>
/// <param name="ImageName"> セットするイメージ名 </param>
/// <returns></returns>
void UBattleHUDItemBase::SetBattleHUDIcon(UMaterial* Material, FName ImageName) {

	if (!IsValid(Material)) {
		return;
	}
	UImage* Image = Cast<UImage>(GetWidgetFromName(ImageName));
	if (!IsValid(Image)) {
		return;
	}
	Image->SetBrushFromMaterial(Material);
}

/// <summary>
/// HUDアイテムのサイズ変更
/// </summary>
/// <param name="Size"> 変更後のサイズ </param>
/// <returns></returns>
void UBattleHUDItemBase::ChangeItemSize_Implementation(FVector2D Size) {

}