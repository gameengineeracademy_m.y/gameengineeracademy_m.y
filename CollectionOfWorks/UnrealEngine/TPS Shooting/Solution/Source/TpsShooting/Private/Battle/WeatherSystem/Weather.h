﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Particles/ParticleSystemComponent.h"
#include "Weather.generated.h"


/// <summary>
/// 天気の種類
/// </summary>
UENUM(BlueprintType)
enum class EWeatherType : uint8 {
	None,         // 設定なし
	Sunny,        // 晴れ
	Rainy,        // 雨
	MaxTypeIndex
};

/// <summary>
/// 天候エフェクトクラス
/// </summary>
UCLASS()
class AWeather : public AActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AWeather();

	/// <summary>
	/// 天候エフェクトを設定する
	/// </summary>
	/// <param name="Effect"> 天候エフェクト </param>
	/// <returns></returns>
	void SetupWeatherEffect(UParticleSystem* Effect);

	/// <summary>
	/// エフェクトの破棄
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void DestroyEffect();

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

protected:

	/// <summary>
	/// パーティクルシステムコンポーネント
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weather")
	UParticleSystemComponent* ParticleSystemComp;

private:

	/// <summary>
	/// 天候エフェクト
	/// </summary>
	UPROPERTY()
	UParticleSystem* WeatherEffect;
};
