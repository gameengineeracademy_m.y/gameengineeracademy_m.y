﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LevelSequenceActor.h"
#include "Engine/TriggerBox.h"
#include "Battle/GameState/BattleGameState.h"
#include "Battle/Controller/BattleController.h"
#include "CameraDirectionTriggerBox.generated.h"

//----------------------------------
//前方宣言
//----------------------------------
class UBattleEvent;

/// <summary>
/// カメラ演出の種類
/// </summary>
UENUM(BlueprintType)
enum class ECameraDirectionType : uint8 {
	None,         // 指定なし
	Tutorial,     // チュートリアル
	FirstArea,    // 第1エリア
	SecondArea,   // 第2エリア
	ThirdArea,    // 第3エリア
	FoundGoal,    // ゴール発見
	MaxTypeIndex
};

/// <summary>
/// チュートリアルのシーンフェーズの種類
/// </summary>
UENUM(BlueprintType)
enum class ETutorialPhase : uint8 {
	None,
	SpawnEnemy01,
	WaitSpawn01,
	SpawnEnemy02,
	WaitSpawn02,
	SpawnEnemy03,
	WaitSpawn03,
	SpawnEnemy04,
	WaitSpawn04,
	WaitFinish,
	Finish,
	MaxPhaseIndex
};

/// <summary>
/// 第1エリアのシーンフェーズの種類
/// </summary>
UENUM(BlueprintType)
enum class EFirstAreaPhase : uint8 {
	None,
	MoveCamera01,
	WaitCamera01,
	MoveCamera02,
	WaitCamera02,
	Finish,
	MaxPhaseIndex
};

/// <summary>
/// 第2エリアのシーンフェーズの種類
/// </summary>
UENUM(BlueprintType)
enum class ESecondAreaPhase : uint8 {
	None,
	MoveCamera01,
	WaitCamera01,
	MoveCamera02,
	WaitCamera02,
	MoveCamera03,
	WaitCamera03,
	Finish,
	MaxPhaseIndex
};

/// <summary>
/// 第3エリアのシーンフェーズの種類
/// </summary>
UENUM(BlueprintType)
enum class EThirdAreaPhase : uint8 {
	None,
	MoveCamera01,
	WaitCamera01,
	MoveCamera02,
	WaitCamera02,
	Finish,
	MaxPhaseIndex
};

/// <summary>
/// ゴール発見時のシーンフェーズの種類
/// </summary>
UENUM(BlueprintType)
enum class EFoundGoalPhase : uint8 {
	None,
	MoveCamera01,
	WaitCamera01,
	Finish,
	MaxPhaseIndex
};

/// <summary>
/// シーン開始処理のフェーズの種類
/// </summary>
UENUM()
enum class EStartScenePhase {
	None,
	StartFadeIn,
	FadeIn,
	FinishFadeIn,
	Setup,
	Wait,
	StartFadeOut,
	FadeOut,
	FinishFadeOut,
	Finish,
	MaxPhaseIndex
};

/// <summary>
/// シーン終了処理のフェーズの種類
/// </summary>
UENUM()
enum class EFinishScenePhase {
	None,
	StartFadeIn,
	FadeIn,
	FinishFadeIn,
	Cancel,
	SetupCamera,
	Wait,
	StartFadeOut,
	FadeOut,
	FinishFadeOut,
	SetupMoveCamera,
	SwitchCamera,
	WaitCamera,
	MoveCamera,
	FinishMoveCamera,
	Finish,
	MaxPhaseIndex
};

/// <summary>
/// カメラ演出用トリガーボックス
/// </summary>
UCLASS()
class ACameraDirectionTriggerBox : public ATriggerBox
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ACameraDirectionTriggerBox();

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	/// <param name=""> イベントインターフェース </param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "CameraDirectionTriggerBox")
	void SetEventInterface(const TScriptInterface<IBattleEvent>& Interface);

	/// <summary>
	/// カメラ演出の設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupCameraDirection();

	/// <summary>
	/// 現在開始処理中かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:開始処理中 </returns>
	bool IsStartPhase();

	/// <summary>
	/// 現在終了処理中かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:終了処理中 </returns>
	bool IsFinishPhase();

	/// <summary>
	/// ムービーをキャンセルする
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupCancel();

	/// <summary>
	/// チュートリアルのシーンフェーズを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> フェーズの種類 </returns>
	UFUNCTION(BlueprintCallable, Category = "CameraDirectionTriggerBox")
	ETutorialPhase GetTutorialPhase();

	/// <summary>
	/// 第1エリアのシーンフェーズを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> フェーズの種類 </returns>
	UFUNCTION(BlueprintCallable, Category = "CameraDirectionTriggerBox")
	EFirstAreaPhase GetFirstAreaPhase();

	/// <summary>
	/// 第2エリアのシーンフェーズを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> フェーズの種類 </returns>
	UFUNCTION(BlueprintCallable, Category = "CameraDirectionTriggerBox")
	ESecondAreaPhase GetSecondAreaPhase();

	/// <summary>
	/// 第3エリアのシーンフェーズを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> フェーズの種類 </returns>
	UFUNCTION(BlueprintCallable, Category = "CameraDirectionTriggerBox")
	EThirdAreaPhase GetThirdAreaPhase();

	/// <summary>
	/// ゴール発見時のシーンフェーズを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> フェーズの種類 </returns>
	UFUNCTION(BlueprintCallable, Category = "CameraDirectionTriggerBox")
	EFoundGoalPhase GetFoundGoalPhase();

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

private:

	/// <summary>
	/// カットシーン処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void ProcessScene(float DeltaTime);

	/// <summary>
	/// チュートリアルのシーン処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void ProcessTutorialScene(float DeltaTime);

	/// <summary>
	/// 第1エリアのシーン処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void ProcessFirstAreaScene(float DeltaTime);

	/// <summary>
	/// 第2エリアのシーン処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void ProcessSecondAreaScene(float DeltaTime);

	/// <summary>
	/// 第3エリアのシーン処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void ProcessThirdAreaScene(float DeltaTime);

	/// <summary>
	/// ゴール発見のシーン処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void ProcessFoundGoalScene(float DeltaTime);

	/// <summary>
	/// シーン開始処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void ProcessStartScene(float DeltaTime);

	/// <summary>
	/// シーン終了処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void ProcessFinishScene(float DeltaTime);

	/// <summary>
	/// プレイヤーの位置と向きの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupPlayerPosAndAngle();

	/// <summary>
	/// カメラ演出時用の操作に切り替える
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SwitchOperate();

	/// <summary>
	/// フェードウィジェットを取得する
	/// </summary>
	/// <param name=""></param>
	/// <returns> フェードウィジェット </returns>
	UFadeWidget* GetFadeWidget();

	/// <summary>
	/// 操作プレイヤーを取得する
	/// </summary>
	/// <param name=""></param>
	/// <returns> 操作プレイヤー </returns>
	AOperatePlayer* GetOperatePlayer();

	/// <summary>
	/// エネミーを取得する
	/// </summary>
	/// <param name="Index"> インデックス </param>
	/// <returns> エネミー </returns>
	AEnemy* GetEnemy(int32 Index);

	/// <summary>
	/// シーケンサが終了フラグのリセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetFinishCameraDirection();

	/// <summary>
	/// シーケンサが終了したかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:終了 </returns>
	bool IsFinishCameraDirection();

	/// <summary>
	/// カメラ演出の停止処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PauseMovie();

	/// <summary>
	/// カメラ演出のフェーズ切り替え停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StopCameraDirectionPhase();

	/// <summary>
	/// 現在のチュートリアルフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentTutorialPhase(ETutorialPhase PhaseType);

	/// <summary>
	/// 現在の第1エリアフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentFirstAreaPhase(EFirstAreaPhase PhaseType);

	/// <summary>
	/// 現在の第2エリアフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentSecondAreaPhase(ESecondAreaPhase PhaseType);

	/// <summary>
	/// 現在の第3エリアフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentThirdAreaPhase(EThirdAreaPhase PhaseType);

	/// <summary>
	/// 現在のゴール発見フェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentFoundGoalPhase(EFoundGoalPhase PhaseType);

	/// <summary>
	/// 現在の開始シーンフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentStartScenePhase(EStartScenePhase PhaseType);

	/// <summary>
	/// 現在の終了シーンフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentFinishScenePhase(EFinishScenePhase PhaseType);

public:

	/// <summary>
	/// カメラ演出の種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CameraDirectionTriggerBox")
	ECameraDirectionType CameraDirectionType;

	/// <summary>
	/// カメラ演出実行時のプレイヤー位置
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CameraDirectionTriggerBox")
	FVector DirectionPlayerPos;

	/// <summary>
	/// カメラ演出実行時のプレイヤー回転量
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CameraDirectionTriggerBox")
	float DirectionPlayerRot;

	/// <summary>
	/// 再生中のレベルシーケンスアクター
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "CameraDirectionTriggerBox")
	ALevelSequenceActor* PlayingLevelSequenceActor;

	/// <summary>
	/// 中止フラグ
	/// </summary>
	UPROPERTY(BlueprintReadOnly, Category = "CameraDirectionTriggerBox")
	bool bCancel;

private:

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IBattleEvent> EventInterface;

	/// <summary>
	/// 現在のチュートリアルのシーンフェーズ
	/// </summary>
	ETutorialPhase CurrentTutorialPhase;

	/// <summary>
	/// 現在の第1エリアのシーンフェーズ
	/// </summary>
	EFirstAreaPhase CurrentFirstAreaPhase;

	/// <summary>
	/// 現在の第2エリアのシーンフェーズ
	/// </summary>
	ESecondAreaPhase CurrentSecondAreaPhase;

	/// <summary>
	/// 現在の第3エリアのシーンフェーズ
	/// </summary>
	EThirdAreaPhase CurrentThirdAreaPhase;

	/// <summary>
	/// 現在のゴール発見のシーンフェーズ
	/// </summary>
	EFoundGoalPhase CurrentFoundGoalPhase;

	/// <summary>
	/// 現在のシーン開始のフェーズ
	/// </summary>
	EStartScenePhase CurrentStartScenePhase;

	/// <summary>
	/// 現在のシーン終了のフェーズ
	/// </summary>
	EFinishScenePhase CurrentFinishScenePhase;

	/// <summary>
	/// 累積時間
	/// </summary>
	float AccumulateTime;

	/// <summary>
	/// カメラ位置初期値
	/// </summary>
	FVector InitCameraPos;

	/// <summary>
	/// エネミーインデックス
	/// </summary>
	int32 EnemyIndex;

private:
	//--------------------------------
	//以下、定数宣言
	//--------------------------------
	/// <summary>
	/// 時間リセット
	/// </summary>
	const float ResetTime = 0.0f;

	/// <summary>
	/// エネミーインデックス初期値
	/// </summary>
	const int32 MinIndex = 0;

	/// <summary>
	/// フェード待機時間
	/// </summary>
	const float FadeWaitTime = 0.15f;

	/// <summary>
	/// カメラ演出時の高さ加算値
	/// </summary>
	const float AddCameraHeight = 500.0f;

	/// <summary>
	/// カメラ移動前の待機時間
	/// </summary>
	const float BeforeMoveCameraWaitTime = 0.5f;

	/// <summary>
	/// カメラ高さを降ろしてくるスピード
	/// </summary>
	const float CameraHeightLowerSpeed = 2.0f;

	/// <summary>
	/// カメラ高さを降ろしてくる時の到達許容値
	/// </summary>
	const float HeightAllowableValue = 0.25f;
};
