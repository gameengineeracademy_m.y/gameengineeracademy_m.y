﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Battle/Widgets/BattleHUDItem/BattleHUDItemBase.h"
#include "Engine/DataTable.h"
#include "BattleHUDMapIcon.generated.h"

/// <summary>
/// マップアイコンの種類
/// </summary>
UENUM(BlueprintType)
enum class EMapIconType : uint8 {
	None,          // なし
	Player,        // プレイヤー
	Enemy,         // エネミー
	ItemAmmo,      // 弾薬アイテム
	ItemHeal,      // 回復アイテム
	ItemWeapon,    // 武器アイテム
	TargetPoint,   // 目標地点
	MaxTypeIndex
};

/// <summary>
/// バトルHUDアイテム マップアイコンクラス
/// </summary>
UCLASS()
class UBattleHUDMapIcon : public UBattleHUDItemBase
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// マップアイコンの種類のセット
	/// </summary>
	/// <param name="IconType"> マップアイコンの種類 </param>
	/// <returns></returns>
	void SetMapIconType(EMapIconType IconType);

	/// <summary>
	/// 表示エリアのセット
	/// </summary>
	/// <param name="Type"> エリアの種類 </param>
	/// <returns></returns>
	void SetStageType(EStageType Type);

	/// <summary>
	/// 表示エリアの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> エリアの種類 </returns>
	EStageType GetStageType();

	/// <summary>
	/// 紐づけ番号のセット
	/// </summary>
	/// <param name="Index"> 紐づけ番号 </param>
	/// <returns></returns>
	void SetObjectIndex(int32 Index);

	/// <summary>
	/// 紐づけ番号の取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 紐づけ番号 </returns>
	int32 GetObjectIndex();

	/// <summary>
	/// 背景イメージの表示設定
	/// </summary>
	/// <param name="VisibleType"> 表示設定 </param>
	/// <returns></returns>
	void SetupDisplayBackImage(ESlateVisibility VisibleType);

	/// <summary>
	/// 背景アニメーションフラグのリセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetPlayAnimFlag();

	/// <summary>
	/// 背景アニメーションが実行されているかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:実行中 </returns>
	bool IsPlayBackAnim();

	/// <summary>
	/// 背景エフェクトアニメーション実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, Category="BattleHUDMapIcon")
	void PlayBackAnimation();

	/// <summary>
	/// 背景エフェクトアニメーション停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, Category = "BattleHUDMapIcon")
	void StopBackAnimation();

private:

	/// <summary>
	/// 背景イメージの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 背景イメージ </returns>
	UImage* GetBackImage();

	/// <summary>
	/// 背景エフェクトアニメーション実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayBackAnimation_Implementation();

	/// <summary>
	/// 背景エフェクトアニメーション停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void StopBackAnimation_Implementation();

protected:

	/// <summary>
	/// アニメーション実行フラグ
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "BattleHUDMapIcon")
	bool bIsPlayAnimFlag;

private:

	/// <summary>
	/// マップアイコンの種類
	/// </summary>
	EMapIconType MapIconType;

	/// <summary>
	/// 表示エリア
	/// </summary>
	EStageType StageType;

	/// <summary>
	/// 各オブジェクトとの紐づけ番号
	/// </summary>
	int32 ObjectIndex;


private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// イメージ マップアイコン設定先
	/// </summary>
	const FName ImageMapIconName = "Image_MapIcon";

	/// <summary>
	/// イメージ 背景イメージ設定先
	/// </summary>
	const FName ImageBackName = "Image_Back";
};
