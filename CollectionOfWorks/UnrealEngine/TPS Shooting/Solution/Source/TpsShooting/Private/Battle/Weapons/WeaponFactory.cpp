﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Weapons/WeaponFactory.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AWeaponFactory::AWeaponFactory()
	: AssaultRifleClass()
	, SniperRifleClass() {

	PrimaryActorTick.bCanEverTick = false;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponFactory::BeginPlay() {

	Super::BeginPlay();

	// アサルトライフル
	FString AssaultRiflePath;
	GConfig->GetString(TEXT("ActorSettings"), TEXT("AssaultRiflePath"), AssaultRiflePath, GGameIni);
	UE_LOG(LogTemp, Display, TEXT("AssaultRiflePath : %s"), *AssaultRiflePath)
	AssaultRifleClass = TSoftClassPtr<AAssaultRifle>(FSoftObjectPath(*AssaultRiflePath)).LoadSynchronous();
	if (!AssaultRifleClass) {
		UE_LOG(LogTemp, Display, TEXT("AssaultRifleClass None"))
	}
	// スナイパーライフル
	FString SniperRiflePath;
	GConfig->GetString(TEXT("ActorSettings"), TEXT("SniperRiflePath"), SniperRiflePath, GGameIni);
	UE_LOG(LogTemp, Display, TEXT("SniperRiflePath : %s"), *SniperRiflePath)
	SniperRifleClass = TSoftClassPtr<ASniperRifle>(FSoftObjectPath(*SniperRiflePath)).LoadSynchronous();
	if (!SniperRifleClass) {
		UE_LOG(LogTemp, Display, TEXT("SniperRifleClass None"))
	}
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AWeaponFactory::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}

/// <summary>
/// ウェポンの生成
/// </summary>
/// <param name="Type"> ウェポンの種類 </param>
/// <returns> 生成したウェポン </returns>
AWeaponBase* AWeaponFactory::CreateWeapon(EWeaponType Type) {

	AWeaponBase* Weapon = nullptr;

	switch (Type) {
	case EWeaponType::AssaultRifle: {

		if (!AssaultRifleClass) {
			return nullptr;
		}
		AAssaultRifle* AssaultRifle = GetWorld()->SpawnActor<AAssaultRifle>(AssaultRifleClass);
		if (!IsValid(AssaultRifle)) {
			return nullptr;
		}
		Weapon = AssaultRifle;
		break;
	}
	case EWeaponType::SniperRifle: {

		if (!SniperRifleClass) {
			return nullptr;
		}

		ASniperRifle* SniperRifle = GetWorld()->SpawnActor<ASniperRifle>(SniperRifleClass);
		if (!IsValid(SniperRifle)) {
			return nullptr;
		}

		Weapon = SniperRifle;
		break;
	}
	}

	return Weapon;
}