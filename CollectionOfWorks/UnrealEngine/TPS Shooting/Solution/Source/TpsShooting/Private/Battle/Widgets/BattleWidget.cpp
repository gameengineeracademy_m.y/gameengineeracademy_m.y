﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Widgets/BattleWidget.h"
#include "Blueprint/WidgetLayoutLibrary.h"
#include "Components/TextBlock.h"
#include "Components/SizeBox.h"
#include "Components/HorizontalBox.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UBattleWidget::UBattleWidget() {

}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::NativeConstruct() {

}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::InitializeWidget() {

	if (!IsValid(WeaponIconTable)) {
		return;
	}
	TArray<FName> RowNames = WeaponIconTable->GetRowNames();
	for (FName RowName : RowNames) {

		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		FWeaponIconList* WeaponIcon = WeaponIconTable->FindRow<FWeaponIconList>(RowName, "");

		// 既に登録済みの武器の種類なら処理終了
		if (WeaponIconList.Contains(WeaponIcon->WeaponType)) {
			continue;
		}
		SetupWeaponIcon(WeaponIcon->WeaponType, WeaponIcon->WeaponMaterial);		
	}

	// サブ武器欄は非表示にしておく
	HideSecondWeapon();
}

/// <summary>
/// 武器アイコンの設定
/// </summary>
/// <param name="WeaponType"> 武器の種類 </param>
/// <param name="Material"> マテリアル </param>
/// <returns></returns>
void UBattleWidget::SetupWeaponIcon(EWeaponType WeaponType, UMaterial* Material) {

	UBattleHUDWeaponIcon* WeaponIcon = CreateWeaponIcon();
	if (!IsValid(WeaponIcon)) {
		return;
	}
	WeaponIcon->SetBattleHUDItemType(EBattleHUDItemType::WeaponIcon);
	WeaponIcon->SetBattleHUDIcon(Material, ImageWeaponName);

	WeaponIconList.Add(WeaponType, WeaponIcon);
}

/// <summary>
/// 武器アイコン生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UBattleHUDWeaponIcon* UBattleWidget::CreateWeaponIcon() {

	UBattleHUDWeaponIcon* WeaponIcon = nullptr;
	FString Path;
	GConfig->GetString(TEXT("UMGBattleHUDSettings"), TEXT("BattleHUDWeaponIcon"), Path, GGameIni);

	TSubclassOf<UBattleHUDWeaponIcon> WeaponIconClass = TSoftClassPtr<UBattleHUDWeaponIcon>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (!WeaponIconClass) {
		return nullptr;
	}
	WeaponIcon = CreateWidget<UBattleHUDWeaponIcon>(GetWorld(), WeaponIconClass);

	return WeaponIcon;
}

/// <summary>
/// 装備中武器アイコンの設定
/// </summary>
/// <param name="WeaponType"> 武器の種類 </param>
/// <returns></returns>
void UBattleWidget::SetEquipWeaponIcon(EWeaponType WeaponType) {

	if (!WeaponIconList.Contains(WeaponType)) {
		return;
	}

	UBattleHUDWeaponIcon* WeaponIcon = WeaponIconList[WeaponType];
	if (!IsValid(WeaponIcon)) {
		return;
	}
	WeaponIcon->ChangeItemSize(EquipWeaponIconSize);

	USizeBox* SizeBox = Cast<USizeBox>(GetWidgetFromName(SizeBoxEquipWeapon));
	if (!IsValid(SizeBox)) {
		return;
	}
	// 子が既に存在するか確認し、存在したら子を切り離して非表示にする
	UBattleHUDWeaponIcon* EquipWeapon = Cast<UBattleHUDWeaponIcon>(SizeBox->GetChildAt(MinIndex));
	if (IsValid(EquipWeapon)) {
		SizeBox->RemoveChild(EquipWeapon);
		EquipWeapon->SetRenderOpacity(Transparent);
	}

	SizeBox->AddChild(WeaponIcon);
	WeaponIcon->SetRenderOpacity(Opacity);
}

/// <summary>
/// サブ武器アイコンの設定
/// </summary>
/// <param name="WeaponType"> 武器の種類 </param>
/// <returns></returns>
void UBattleWidget::SetSecondWeaponIcon(EWeaponType WeaponType) {

	if (!WeaponIconList.Contains(WeaponType)) {
		return;
	}

	UBattleHUDWeaponIcon* WeaponIcon = WeaponIconList[WeaponType];
	if (!IsValid(WeaponIcon)) {
		return;
	}
	WeaponIcon->ChangeItemSize(SecondWeaponIconSize);

	USizeBox* SizeBox = Cast<USizeBox>(GetWidgetFromName(SizeBoxSecondWeapon));
	if (!IsValid(SizeBox)) {
		return;
	}
	// 子が既に存在するか確認し、存在したら子を切り離して非表示にする
	UBattleHUDWeaponIcon* SecondWeapon = Cast<UBattleHUDWeaponIcon>(SizeBox->GetChildAt(MinIndex));
	if (IsValid(SecondWeapon)) {
		SizeBox->RemoveChild(SecondWeapon);
		SecondWeapon->SetRenderOpacity(Transparent);
	}

	SizeBox->AddChild(WeaponIcon);
	WeaponIcon->SetRenderOpacity(Opacity);

	// サブ武器欄を表示しておく
	DisplaySecondWeapon();
}

/// <summary>
/// 武器アイコン設定のリセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::ResetWeaponIcon() {

	USizeBox* EquipWeaponSizeBox = Cast<USizeBox>(GetWidgetFromName(SizeBoxEquipWeapon));
	USizeBox* SecondWeaponSizeBox = Cast<USizeBox>(GetWidgetFromName(SizeBoxEquipWeapon));

	if (!IsValid(EquipWeaponSizeBox) || !IsValid(SecondWeaponSizeBox)) {
		return;
	}
	// 子が既に存在するか確認し、存在したら子を切り離して非表示にする
	UBattleHUDWeaponIcon* EquipWeapon = Cast<UBattleHUDWeaponIcon>(EquipWeaponSizeBox->GetChildAt(MinIndex));
	UBattleHUDWeaponIcon* SecondWeapon = Cast<UBattleHUDWeaponIcon>(SecondWeaponSizeBox->GetChildAt(MinIndex));
	if (IsValid(EquipWeapon)) {
		EquipWeaponSizeBox->RemoveChild(EquipWeapon);
		EquipWeapon->SetRenderOpacity(Transparent);
	}
	if (IsValid(SecondWeapon)) {
		SecondWeaponSizeBox->RemoveChild(SecondWeapon);
		SecondWeapon->SetRenderOpacity(Transparent);
	}

	// サブ武器欄を非表示にしておく
	HideSecondWeapon();
}

/// <summary>
/// 武器アイコンの切替
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::SwitchWeaponIcon() {

	USizeBox* EquipWeaponSizeBox = Cast<USizeBox>(GetWidgetFromName(SizeBoxEquipWeapon));
	USizeBox* SecondWeaponSizeBox = Cast<USizeBox>(GetWidgetFromName(SizeBoxSecondWeapon));
	if (!IsValid(EquipWeaponSizeBox) || !IsValid(SecondWeaponSizeBox)) {
		return;
	}

	UBattleHUDWeaponIcon* EquipWeapon = Cast<UBattleHUDWeaponIcon>(EquipWeaponSizeBox->GetChildAt(MinIndex));
	UBattleHUDWeaponIcon* SecondWeapon = Cast<UBattleHUDWeaponIcon>(SecondWeaponSizeBox->GetChildAt(MinIndex));
	if (!IsValid(EquipWeapon) || !IsValid(SecondWeapon)) {
		return;
	}

	// 一旦、子ウィジェットを取り外す
	bool IsRemoveEquip = EquipWeaponSizeBox->RemoveChild(EquipWeapon);
	bool IsRemoveSecond = SecondWeaponSizeBox->RemoveChild(SecondWeapon);
	if (!IsRemoveEquip || !IsRemoveSecond) {
		return;
	}
	//EquipWeapon->SetRenderOpacity(Transparent);
	//SecondWeapon->SetRenderOpacity(Transparent);

	// 装備武器とサブ武器のサイズ設定を行う
	EquipWeapon->ChangeItemSize(SecondWeaponIconSize);
	SecondWeapon->ChangeItemSize(EquipWeaponIconSize);

	// 子ウィジェットとして再設定する
	EquipWeaponSizeBox->AddChild(SecondWeapon);
	SecondWeaponSizeBox->AddChild(EquipWeapon);
}

/// <summary>
/// 装填弾薬数を設定
/// </summary>
/// <param name="Ammos"> 弾薬数 </param>
/// <returns></returns>
void UBattleWidget::SetLoadedAmmos(int32 Ammos) {

	UTextBlock* TextBlock = Cast<UTextBlock>(GetWidgetFromName(TextBlockLoadedAmmos));
	if (!IsValid(TextBlock)) {
		return;
	}
	TextBlock->SetText(FText::FromString(FString::FromInt(Ammos)));
}

/// <summary>
/// 所持弾薬数を設定
/// </summary>
/// <param name="Ammos"> 弾薬数 </param>
/// <returns></returns>
void UBattleWidget::SetPossessAmmos(int32 Ammos) {

	UTextBlock* TextBlock = Cast<UTextBlock>(GetWidgetFromName(TextBlockPossessAmmos));
	if (!IsValid(TextBlock)) {
		return;
	}
	TextBlock->SetText(FText::FromString(FString::FromInt(Ammos)));
}

/// <summary>
/// 表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::Display() {

	SetVisibility(ESlateVisibility::SelfHitTestInvisible);
}

/// <summary>
/// 非表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::Hide() {

	SetVisibility(ESlateVisibility::Hidden);
}

/// <summary>
/// サブ武器欄の非表示設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::HideSecondWeapon() {

	UHorizontalBox* HorizontalBox = Cast<UHorizontalBox>(GetWidgetFromName(HorizontalBoxSecondWeapon));
	if (!IsValid(HorizontalBox)) {
		return;
	}
	HorizontalBox->SetRenderOpacity(Transparent);
}

/// <summary>
/// サブ武器欄の表示設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::DisplaySecondWeapon() {

	UHorizontalBox* HorizontalBox = Cast<UHorizontalBox>(GetWidgetFromName(HorizontalBoxSecondWeapon));
	if (!IsValid(HorizontalBox)) {
		return;
	}
	HorizontalBox->SetRenderOpacity(Opacity);
}

/// <summary>
/// 武器切り替え開始アニメーション
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::PlaySwitchStartWeaponAnimation_Implementation() {

}

/// <summary>
/// 武器切り替え終了アニメーション
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::PlaySwitchEndWeaponAnimation_Implementation() {

}