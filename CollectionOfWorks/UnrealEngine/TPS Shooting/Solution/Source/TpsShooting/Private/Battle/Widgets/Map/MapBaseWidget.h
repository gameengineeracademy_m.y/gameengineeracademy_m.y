﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/UiAssignmentWidget.h"
#include "Battle/CharacterManager/PlayerManager/PlayerManager.h"
#include "Battle/CharacterManager/EnemyManager/EnemyManager.h"
#include "Battle/GameObjects/PickupItem/PickupItemManager.h"
#include "Battle/GameObjects/TargetObjects/TargetObjectManager.h"
#include "Battle/Widgets/BattleHUDItem/BattleHUDMapIcon/BattleHUDMapIcon.h"
#include "MapBaseWidget.generated.h"


/// <summary>
/// マップの種類
/// </summary>
USTRUCT(BlueprintType)
struct FMapType : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// バトルレベルの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "MapBaseWidget")
	ELevelType BattleLevelType;

	/// <summary>
	/// マップテクスチャ
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "MapBaseWidget")
	UTexture2D* MapTexture;
};

/// <summary>
/// アイコンデータ
/// </summary>
USTRUCT(BlueprintType)
struct FMapIconData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// マップアイコンの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "MapBaseWidget")
	EMapIconType MapIconType;

	/// <summary>
	/// アイコンマテリアル
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "MapBaseWidget")
	UMaterial* MapIcon;
};

/// <summary>
/// マップベースウィジェット
/// </summary>
UCLASS()
class UMapBaseWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UMapBaseWidget();

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="Geometry"> ジオメトリ </param>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void NativeTick(const FGeometry& Geometry, float DeltaTime) override;

	/// <summary>
	/// エリアの種類のセット
	/// </summary>
	/// <param name="Type"> エリアの種類 </param>
	/// <returns></returns>
	void SetStageType(EStageType Type);

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeWidget();

	/// <summary>
	/// マップテクスチャのセット
	/// </summary>
	/// <param name="Texture"> テクスチャ </param>
	/// <returns></returns>
	void SetMapImage(UTexture2D* Texture);

	/// <summary>
	/// プレイヤーマネージャをセット
	/// </summary>
	/// <param name="Manager"> プレイヤーマネージャ </param>
	/// <returns></returns>
	void SetPlayerManager(APlayerManager* Manager);

	/// <summary>
	/// エネミーマネージャをセット
	/// </summary>
	/// <param name="Manager"> エネミーマネージャ </param>
	/// <returns></returns>
	void SetEnemyManager(AEnemyManager* Manager);

	/// <summary>
	/// アイテムマネージャをセット
	/// </summary>
	/// <param name="Manager"> アイテムマネージャ </param>
	/// <returns></returns>
	void SetPickupItemManager(APickupItemManager* Manager);

	/// <summary>
	/// ターゲットオブジェクトマネージャをセット
	/// </summary>
	/// <param name="Manager"> ターゲットオブジェクトマネージャ </param>
	/// <returns></returns>
	void SetTargetObjectManager(ATargetObjectManager* Manager);

	/// <summary>
	/// 初期設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupWidget();

	/// <summary>
	/// マップアイコンのリセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetMapIcon();

	/// <summary>
	/// エネミーデータの再設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetEnemyData();

	/// <summary>
	/// マップ画像のサイズを変更
	/// </summary>
	/// <param name="Size"> マップサイズ </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "MapBaseWidget")
	void ChangeMapImageSize(float Size);

	/// <summary>
	/// マップ画像のサイズを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "MapBaseWidget")
	void SetupMapImageSize();

	/// <summary>
	/// 表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Display();

	/// <summary>
	/// 非表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Hide();

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

	/// <summary>
	/// エネミーアイコンの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupEnemyIcon();

	/// <summary>
	/// アイテムアイコンの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupItemIcon();

	/// <summary>
	/// ゴールアイコンの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupGoalIcon();

	/// <summary>
	/// マップアイコンの作成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UBattleHUDMapIcon* CreateMapIcon();

	/// <summary>
	/// マップアイコン用のオーバーレイの子に設定
	/// </summary>
	/// <param name="MapIcon"> マップアイコン </param>
	/// <returns></returns>
	void AddChildOverlay(UBattleHUDMapIcon* MapIcon);

	/// <summary>
	/// エネミーアイコンのリセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetMapIconEnemy();

	/// <summary>
	/// アイテムアイコンのリセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetMapIconItem();

	/// <summary>
	/// 実際の位置からマップ上の位置を取得
	/// </summary>
	/// <param name="Position"> 実際の位置 </param>
	/// <returns> マップ上の位置 </returns>
	FVector2D GetPositionOnMap(FVector Position);

	/// <summary>
	/// プレイヤー位置更新
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void UpdatePlayerIcon() {};

	/// <summary>
	/// エネミーアイコンの更新
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void UpdateEnemyIcon() {};

	/// <summary>
	/// アイテムアイコンの更新
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void UpdatePickupItemIcon() {};

	/// <summary>
	/// ゴールアイコンの更新
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void UpdateGoalIcon() {};

	/// <summary>
	/// マップ画像のサイズを変更
	/// </summary>
	/// <param name="Size"> マップサイズ </param>
	/// <returns></returns>
	virtual void ChangeMapImageSize_Implementation(float Size);

	/// <summary>
	/// マップ画像のサイズを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void SetupMapImageSize_Implementation();

public:

	/// <summary>
	/// マップテーブル
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "MapBaseWidget")
	UDataTable* MapTable;

	/// <summary>
	/// マップアイコンテーブル
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "MapBaseWidget")
	UDataTable* MapIconTable;

	/// <summary>
	/// 画像サイズ
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "MapBaseWidget")
	FVector2D ImageSize;

protected:

	/// <summary>
	/// エリアの種類
	/// </summary>
	EStageType CurrentStageType;

	/// <summary>
	/// プレイヤーマネージャ
	/// </summary>
	UPROPERTY()
	APlayerManager* PlayerManager;

	/// <summary>
	/// エネミーマネージャ
	/// </summary>
	UPROPERTY()
	AEnemyManager* EnemyManager;

	/// <summary>
	/// ピックアップアイテムマネージャ
	/// </summary>
	UPROPERTY()
	APickupItemManager* PickupItemManager;

	/// <summary>
	/// ターゲットオブジェクトマネージャ
	/// </summary>
	UPROPERTY()
	ATargetObjectManager* TargetObjectManager;

	/// <summary>
	/// マップアイコンリスト
	/// </summary>
	UPROPERTY()
	TMap<EMapIconType, UMaterial*> MapIconList;

	/// <summary>
	/// エネミーアイコンリスト
	/// </summary>
	UPROPERTY()
	TArray<UBattleHUDMapIcon*> EnemyIconList;

	/// <summary>
	/// アイテムアイコンリスト
	/// </summary>
	UPROPERTY()
	TArray<UBattleHUDMapIcon*> PickupItemIconList;

	/// <summary>
	/// ゴールアイコンリスト
	/// </summary>
	UPROPERTY()
	TArray<UBattleHUDMapIcon*> GoalIconList;

	/// <summary>
	/// マップの中心座標
	/// </summary>
	FVector2D MapCenterPos;

protected:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// プレイヤーカーソル名
	/// </summary>
	const FName ImagePlayerName = "Image_Player";

	/// <summary>
	/// メインマップ名
	/// </summary>
	const FName ImageMapName = "Image_Map";

	/// <summary>
	/// マップ設定先
	/// </summary>
	const FName BorderMapName = "Border_Map";

	/// <summary>
	/// マップアイコン設定先
	/// </summary>
	const FName ImageIconName = "Image_Icon";

	/// <summary>
	/// マップアイコン設定先
	/// </summary>
	const FName OverlayMapIconName = "Overlay_MapIcon";

	/// <summary>
	/// キャンバスパネル設定先
	/// </summary>
	const FName CanvasPanelMapName = "CanvasPanel_Map";

	/// <summary>
	/// アイコンポジション
	/// </summary>
	const FVector2D IconPosition = FVector2D(0.0f, 0.0f);

	/// <summary>
	/// アイコンサイズ
	/// </summary>
	const FVector2D IconSize = FVector2D(15.0f, 15.0f);

	/// <summary>
	/// アイコンのアンカー
	/// </summary>
	const FVector2D IconAnchorVector2D = FVector2D(0.5f, 0.5f);

	/// <summary>
	/// アイコンのアラインメント
	/// </summary>
	const FVector2D IconAlignment = FVector2D(0.5f, 0.5f);

	/// <summary>
	/// マップ基準 ブロッキングボリューム 位置
	/// </summary>
	const FVector VolumePosition = FVector(-6000.0f, 6500.0f, 0.0f);

	/// <summary>
	/// ブロッキングボリュームの1辺
	/// </summary>
	const float VolumeSize = 200.0f;

	/// <summary>
	/// ブロッキングボリュームの倍率
	/// </summary>
	const float SizeRate = 120.0f;

	/// <summary>
	/// 負の値
	/// </summary>
	const float MinusValue = -1.0f;

	/// <summary>
	/// 半分
	/// </summary>
	const float HalfValue = 0.5f;

	/// <summary>
	/// 向きが反対
	/// </summary>
	const float OppositeDirection = -1.0f;

	/// <summary>
	/// 0°
	/// </summary>
	const float Angle0 = 0.0f;

	/// <summary>
	/// 90°
	/// </summary>
	const float Angle90 = 90.0f;

	/// <summary>
	/// 180°
	/// </summary>
	const float Angle180 = 180.0f;

	/// <summary>
	/// 透明
	/// </summary>
	const float Transparent = 0.0f;

	/// <summary>
	/// 不透明
	/// </summary>
	const float Opacity = 1.0f;

	/// <summary>
	/// ゼロ
	/// </summary>
	const float Zero = 0.0f;

	/// <summary>
	/// 最小インデックス
	/// </summary>
	const int32 MinIndex = 0;
};
