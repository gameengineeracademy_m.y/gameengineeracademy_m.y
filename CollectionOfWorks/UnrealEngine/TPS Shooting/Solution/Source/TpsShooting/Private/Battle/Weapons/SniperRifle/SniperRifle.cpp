﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Weapons/SniperRifle/SniperRifle.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ASniperRifle::ASniperRifle() {

	// Tick処理を無効化
	PrimaryActorTick.bCanEverTick = true;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ASniperRifle::BeginPlay() {

	Super::BeginPlay();

	//----------------------------------
	//初期設定
	//----------------------------------
	// ウェポンの種類の設定
	SetWeaponType(EWeaponType::SniperRifle);
	// データテーブルからデータを取得
	SetupWeaponData();
	SetupAimCameraData();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ASniperRifle::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}