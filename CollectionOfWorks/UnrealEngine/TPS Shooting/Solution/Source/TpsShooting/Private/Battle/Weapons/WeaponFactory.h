﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Battle/Weapons/AssaultRifle/AssaultRifle.h"
#include "Battle/Weapons/SniperRifle/SniperRifle.h"
#include "WeaponFactory.generated.h"


/// <summary>
/// ウェポンファクトリークラス
/// </summary>
UCLASS()
class AWeaponFactory : public AActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AWeaponFactory();

	/// <summary>
	/// ウェポンの生成
	/// </summary>
	/// <param name="Type"> ウェポンの種類 </param>
	/// <returns> 生成したウェポン </returns>
	AWeaponBase* CreateWeapon(EWeaponType Type);

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

private:

	/// <summary>
	/// アサルトライフルクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<AAssaultRifle> AssaultRifleClass;

	/// <summary>
	/// スナイパーライフルクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<ASniperRifle> SniperRifleClass;
};
