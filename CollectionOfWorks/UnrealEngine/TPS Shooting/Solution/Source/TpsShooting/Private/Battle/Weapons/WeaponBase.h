﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/DataTable.h"
#include "Audio/DataTables/Structs/SoundData.h"
#include "Components/AudioComponent.h"
#include "Sound/SoundCue.h"
#include "Particles/ParticleSystemComponent.h"
#include "Animation/AnimSequence.h"
#include "Battle/Camera/CameraShake/ShootCameraShake.h"
#include "WeaponBase.generated.h"

//-----------------------------
//前方宣言
//-----------------------------
class ACharacterBase;

/// <summary>
/// ウェポンの種類
/// </summary>
UENUM(BlueprintType)
enum class EWeaponType : uint8 {
	None,
	AssaultRifle,
	SniperRifle,
	MaxTypeIndex
};

/// <summary>
/// 射撃の種類
/// </summary>
UENUM(BlueprintType)
enum class EShootType : uint8 {
	None,
	Single,
	Continuous,
	MaxTypeIndex
};

/// <summary>
/// 銃の操作フェーズ
/// </summary>
UENUM(BlueprintType)
enum class EWeaponPhase : uint8 {
	Wait,
	StartShoot,
	Shoot,
	ShootSetting,
	StopShoot,
	Reload,
	MaxTypeIndex
};

/// <summary>
/// カメラディテール 構造体
/// </summary>
USTRUCT(BlueprintType)
struct FAimCameraData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// 武器の種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponBase")
	EWeaponType WeaponType;

	/// <summary>
	/// カメラとプレイヤーの距離
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponBase")
	float PlayerDistance;

	/// <summary>
	/// 水平位置
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponBase")
	float HorizontalPos;

	/// <summary>
	/// 視野角
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponBase")
	float FOV;
};

/// <summary>
/// カメラの情報 構造体
/// </summary>
USTRUCT()
struct FAimCameraDetail {

	GENERATED_BODY()

public:

	/// <summary>
	/// カメラとプレイヤーの距離
	/// </summary>
	float PlayerDistance;

	/// <summary>
	/// 水平位置
	/// </summary>
	float HorizontalPos;

	/// <summary>
	/// 視野角
	/// </summary>
	float FOV;
};

/// <summary>
/// ウェポンの情報
/// </summary>
USTRUCT()
struct FWeaponData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// ウェポンの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	EWeaponType WeaponType;

	/// <summary>
	/// 射撃の種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	EShootType ShootType;

	/// <summary>
	/// 弾の威力
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	int32 Power;

	/// <summary>
	/// 最大装填数
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	int32 MaxLoadedAmmos;

	/// <summary>
	/// 最大所持弾数
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	int32 MaxPossessAmmos;

	/// <summary>
	/// 射撃間隔時間
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	float ShootDuration;

	/// <summary>
	/// 視野角
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	float FOV;
};

/// <summary>
/// ウェポンのソケット情報
/// </summary>
USTRUCT()
struct FWeaponSocketData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// ウェポンの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	EWeaponType WeaponType;

	/// <summary>
	/// マズルソケット名
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	FName MuzzleSocketName;

	/// <summary>
	/// 空薬莢排出ソケット名
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	FName AmmoEjectSocketName;
};

/// <summary>
/// ウェポンのサウンド情報
/// </summary>
USTRUCT()
struct FWeaponSoundData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// ウェポンの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	EWeaponType WeaponType;

	/// <summary>
	/// 射撃中のサウンド
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	USoundCue* ShootSound;

	/// <summary>
	/// 射撃後のサウンド
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	USoundCue* EndShootSound;

	/// <summary>
	/// 空撃ちのサウンド
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	USoundCue* EmptyShootSound;

	/// <summary>
	/// リロード(取り外し)のサウンド
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	USoundCue* ReloadRemoveSound;

	/// <summary>
	/// リロード(取り付け)のサウンド
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	USoundCue* ReloadInsertSound;
};

/// <summary>
/// ウェポンのエフェクト情報
/// </summary>
USTRUCT()
struct FWeaponEffectData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// ウェポンの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	EWeaponType WeaponType;

	/// <summary>
	/// マズルフラッシュ
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	UParticleSystem* MuzzleFlash;

	/// <summary>
	/// 軌跡
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	UParticleSystem* ShootTrail;
};

/// <summary>
/// 着弾地点のサウンド情報
/// </summary>
USTRUCT()
struct FImpactSoundData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// ウェポンの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	EWeaponType WeaponType;

	/// <summary>
	/// 血しぶき
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	USoundCue* BloodSplatter;

	/// <summary>
	/// はじかれる
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	USoundCue* Repelled;

	/// <summary>
	/// クリティカル
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	USoundCue* Critical;
};

/// <summary>
/// 着弾地点のエフェクト情報
/// </summary>
USTRUCT()
struct FImpactEffectData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// ウェポンの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	EWeaponType WeaponType;

	/// <summary>
	/// 血しぶき
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	UParticleSystem* BloodSplatter;

	/// <summary>
	/// はじかれる
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	UParticleSystem* Repelled;
};

/// <summary>
/// ウェポンのアニメーション
/// </summary>
USTRUCT()
struct FWeaponAnimData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// ウェポンの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	EWeaponType WeaponType;

	/// <summary>
	/// 射撃フェーズの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	EWeaponPhase WeaponPhase;

	/// <summary>
	/// アニメーション
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponData")
	UAnimSequence* AnimSequence;
};


/// <summary>
/// ウェポンベース
/// </summary>
UCLASS()
class AWeaponBase : public AActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AWeaponBase();

	/// <summary>
	/// ウェポンの種類のセット
	/// </summary>
	/// <param name="Type"> ウェポンの種類 </param>
	/// <returns></returns>
	void SetWeaponType(EWeaponType Type);

	/// <summary>
	/// ウェポンの種類の取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ウェポンの種類 </returns>
	EWeaponType GetWeaponType();

	/// <summary>
	/// 所有者のセット
	/// </summary>
	/// <param name="Character"> 所有者 </param>
	/// <returns></returns>
	void SetCharaOwner(ACharacterBase* Character);

	/// <summary>
	/// 所有者の取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 所有者 </returns>
	ACharacterBase* GetCharaOwner();

	/// <summary>
	/// 発砲する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Fire();

	/// <summary>
	/// 発砲をやめる
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StopFire();

	/// <summary>
	/// リロードが必要かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:必要 </returns>
	bool IsReload();

	/// <summary>
	/// 装填された弾薬の数を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 装填された弾薬の数 </returns>
	int32 GetLoadedAmmos();

	/// <summary>
	/// 所持弾薬数の取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 所持弾薬数 </returns>
	int32 GetPossessAmmos();

	/// <summary>
	/// 装填された弾薬があるかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:ある </returns>
	bool IsExistsLoadedAmmos();

	/// <summary>
	/// 所持弾数があるかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:ある </returns>
	bool IsExistsPossessAmmos();

	/// <summary>
	/// 装填された弾薬が最大かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:ある </returns>
	bool IsExistsMaxLoadedAmmos();

	/// <summary>
	/// 所持弾薬数が最大かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:ある </returns>
	bool IsExistsMaxPossessAmmos();

	/// <summary>
	/// 強制的に射撃を止める
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StopForciblyFire();

	/// <summary>
	/// リロード開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StartReload();

	/// <summary>
	/// リロード終了
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void FinishReload();

	/// <summary>
	/// アニメーションの実行
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void PlayWeaponAnimation(EWeaponPhase PhaseType);

	/// <summary>
	/// アニメーションのリセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetWeaponAnimation();

	/// <summary>
	/// 所持弾薬を上限までセットする
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetMaxPossessAmmos();

	/// <summary>
	/// 所持弾薬を上限 + 未装填分セットする
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupExceedMaxPossessAmmos();

	/// <summary>
	/// 装填弾薬を上限までセットする
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetMaxLoadedAmmos();

	/// <summary>
	/// 所持弾薬・装填弾薬が上限かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:上限 </returns>
	bool IsExistsMaxAmmos();

	/// <summary>
	/// 射撃処理を中止する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CancelShooting();

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

protected:

	/// <summary>
	/// ウェポン情報の設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupWeaponData();

	/// <summary>
	/// カメラ情報の設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupAimCameraData();

private:

	/// <summary>
	/// 射撃時のカメラ揺らしクラスの生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreateShootCameraShake();

	/// <summary>
	/// 射撃サウンドの再生
	/// </summary>
	/// <param name="SoundCue"> サウンドキュー </param>
	/// <returns> サウンド </returns>
	UAudioComponent* PlayShootSound(USoundCue* SoundCue);

	/// <summary>
	/// 射撃開始サウンドを鳴らす
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StartSoundShoot();

	/// <summary>
	/// 射撃終了サウンドを鳴らす
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void EndSoundShoot();

	/// <summary>
	/// 射撃を止める
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StopSoundShoot();

	/// <summary>
	/// サウンドの再生
	/// </summary>
	/// <param name="SoundCue"> サウンドキュー </param>
	/// <returns></returns>
	void PlaySound(USoundCue* SoundCue);

	/// <summary>
	/// マズルフラッシュ実行
	/// </summary>
	/// <param name=""> エフェクト </param>
	/// <returns></returns>
	void StartMuzzleEffect();

	/// <summary>
	/// マズルフラッシュ停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StopMuzzleEffect();

	/// <summary>
	/// 弾の軌跡表示
	/// </summary>
	/// <param name="StartPos"> 開始地点 </param>
	/// <param name="EndPos"> 終端地点 </param>
	/// <returns></returns>
	void ShowShootTrail(FVector StartPos, FVector EndPos);

	/// <summary>
	/// 着弾処理実行
	/// </summary>
	/// <param name="Hit"> ヒット情報 </param>
	/// <returns></returns>
	void ImpactEffectAndSound(FHitResult Hit);

	/// <summary>
	/// 連続射撃処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ShootContinuous();

	/// <summary>
	/// 単発射撃処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ShootSingle();

	/// <summary>
	/// 射撃処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Shoot();

	/// <summary>
	/// 射撃できるかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:射撃可能 </returns>
	bool IsShootable();

	/// <summary>
	/// 射撃準備時間
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void PrepareShoot(float DeltaTime);

	/// <summary>
	/// 装填されている弾を減らす
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void DecreaseAmmo();

	/// <summary>
	/// リロード処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Reload();

	/// <summary>
	/// シミュレーション
	/// </summary>
	/// <param name="StartPos"> 開始地点 </param>
	/// <param name="EndPos"> 終端地点 </param>
	/// <returns></returns>
	FHitResult SimulateRay(FVector StartPos, FVector EndPos);

	/// <summary>
	/// ダメージ計算処理
	/// </summary>
	/// <param name="Hit"> ヒット情報 </param>
	/// <param name="MuzzlePos"> 銃口の位置 </param>
	/// <returns></returns>
	void CalcDamage(FHitResult Hit, FVector MuzzlePos);

	/// <summary>
	/// 現在のウェポンフェーズを変更する
	/// </summary>
	/// <param name="Phase"> ウェポンフェーズ </param>
	/// <returns></returns>
	void ChangeCurrentWeaponPhase(EWeaponPhase Phase);

public:

	/// <summary>
	/// スケルタルメッシュコンポーネント
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponBase")
	USkeletalMeshComponent* WeaponMesh;

	/// <summary>
	/// ウェポンデータ
	/// </summary>
	UPROPERTY(BlueprintReadOnly, Category = "WeaponBase")
	FWeaponData WeaponData;

	/// <summary>
	/// ウェポンのソケットデータ
	/// </summary>
	UPROPERTY(BlueprintReadOnly, Category = "WeaponBase")
	FWeaponSocketData WeaponSocketData;

	/// <summary>
	/// ウェポンのサウンドデータ
	/// </summary>
	UPROPERTY(BlueprintReadOnly, Category = "WeaponBase")
	FWeaponSoundData WeaponSoundData;

	/// <summary>
	/// ウェポンのエフェクトデータ
	/// </summary>
	UPROPERTY(BlueprintReadOnly, Category = "WeaponBase")
	FWeaponEffectData WeaponEffectData;

	/// <summary>
	/// 着弾地点のサウンドデータ
	/// </summary>
	UPROPERTY(BlueprintReadOnly, Category = "WeaponBase")
	FImpactSoundData ImpactSoundData;

	/// <summary>
	/// 着弾地点のエフェクトデータ
	/// </summary>
	UPROPERTY(BlueprintReadOnly, Category = "WeaponBase")
	FImpactEffectData ImpactEffectData;

	/// <summary>
	/// エイム時のカメラデータ
	/// </summary>
	UPROPERTY(BlueprintReadOnly, Category = "WeaponBase")
	FAimCameraData AimCameraData;

	/// <summary>
	/// ウェポンアニメーションデータ
	/// </summary>
	UPROPERTY(BlueprintReadOnly, Category = "WeaponBase")
	TMap<EWeaponPhase, UAnimSequence*> WeaponAnimList;

	/// <summary>
	/// リロード中フラグ
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "WeaponBase")
	bool bReload;

protected:

	/// <summary>
	/// ウェポンの種類
	/// </summary>
	UPROPERTY(BlueprintReadOnly, Category = "WeaponBase")
	EWeaponType WeaponType;

	/// <summary>
	/// ウェポンデータテーブル
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponBase")
	UDataTable* WeaponDataTable;

	/// <summary>
	/// ウェポンのソケットデータテーブル
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponBase")
	UDataTable* WeaponSocketTable;

	/// <summary>
	/// ウェポンのサウンドデータテーブル
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponBase")
	UDataTable* WeaponSoundTable;

	/// <summary>
	/// ウェポンのエフェクトデータテーブル
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponBase")
	UDataTable* WeaponEffectTable;

	/// <summary>
	/// 着弾サウンドデータテーブル
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponBase")
	UDataTable* ImpactSoundTable;

	/// <summary>
	/// 着弾エフェクトデータテーブル
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponBase")
	UDataTable* ImpactEffectTable;

	/// <summary>
	/// エイム時のカメラデータテーブル
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponBase")
	UDataTable* AimCameraTable;

	/// <summary>
	/// ウェポンのアニメーションデータテーブル
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponBase")
	UDataTable* AnimationTable;

	/// <summary>
	/// 射撃フラグ
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "WeaponBase")
	bool bShootable;

	/// <summary>
	/// 目標距離
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "WeaponBase")
	float AimDistance;

private:

	/// <summary>
	/// 射撃時の画面揺らしクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UShootCameraShake> ShootCameraShakeClass;

	/// <summary>
	/// 所有者
	/// ※前方宣言変数のためUPROPERTYは付けない
	/// </summary>
	ACharacterBase* Owner;

	/// <summary>
	/// 射撃音
	/// </summary>
	UPROPERTY()
	TArray<UAudioComponent*> ShootSound;

	/// <summary>
	/// マズルエフェクト
	/// </summary>
	UPROPERTY()
	TArray<UParticleSystemComponent*> MuzzleEffect;

	/// <summary>
	/// 射撃実行
	/// </summary>
	EWeaponPhase CurrentPhase;

	/// <summary>
	/// 前回の射撃からの累積時間
	/// </summary>
	float AccumulateTime;

	/// <summary>
	/// 装填弾数
	/// </summary>
	int32 LoadedAmmos;

	/// <summary>
	/// 所持弾数
	/// </summary>
	int32 PossessAmmos;

protected:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// サウンドフェードアウト遅延時間
	/// </summary>
	const float FadeOutDuration = 0.1f;

	/// <summary>
	/// サウンドフェードアウト後のボリューム
	/// </summary>
	const float FadeOutVolumeLevel = 0.0f;

	/// <summary>
	/// 累積時間リセット
	/// </summary>
	const float ResetTime = 0.0f;

	/// <summary>
	/// ゼロ
	/// </summary>
	const int32 Zero = 0;

	/// <summary>
	/// エフェクト倍率 中サイズ(デフォルト)
	/// </summary>
	const FVector EffectDefaultScale = FVector(1.2f, 1.2f, 1.2f);

	/// <summary>
	/// エフェクト倍率 小サイズ
	/// </summary>
	const FVector EffectSmallScale = FVector(0.8f, 0.8f, 0.8f);

	/// <summary>
	/// エフェクト倍率 大サイズ
	/// </summary>
	const FVector EffectBigScale = FVector(2.0f, 2.0f, 2.0f);

	/// <summary>
	/// 銃弾の軌跡 終端地点パラメータ名
	/// </summary>
	const FName TrailTargetParam = "ShockBeamEnd";
};