﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Widgets/ResultWidget.h"
#include "Battle/Controller/BattleController.h"
#include "UiAssignmentWidget/OperateBaseWidget/Button/LevelUIButton/LevelUIButton.h"
#include "Components/CanvasPanel.h"
#include "Components/Image.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UResultWidget::UResultWidget() {

}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UResultWidget::NativeConstruct() {

	Super::NativeConstruct();

	InitializeResultWidget();
}

/// <summary>
/// ウィジェットの初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UResultWidget::SetupWidget() {

	//-----------------------------------------------
	//リザルト画面上のボタンを取得
	//ボタンの初期化処理とイベントデリゲート処理
	//-----------------------------------------------

	UHorizontalBox* HorizontalBox = GetHorizontalBox();
	if (!IsValid(HorizontalBox)) {
		return;
	}

	TArray<UWidget*> ChildrenArray = HorizontalBox->GetAllChildren();
	int32 Index = MinIndex;
	for (UWidget* Widget : ChildrenArray) {

		ULevelUIButton* Button = Cast<ULevelUIButton>(Widget);
		if (!IsValid(Button)) {
			continue;
		}
		Button->StopButtonAnimation();
		Button->SetupHoverAndPressEvent();
		EButtonType Type = Button->GetButtonType();

		ButtonList.Add(Index, Button);
		++Index;
	}

	if (ButtonList.Num() <= Zero) {
		return;
	}
	// 先頭メニューを選択状態にする
	CurrentButtonIndex = MinIndex;
	ULevelUIButton* Button = Cast<ULevelUIButton>(GetSelectButton());
	if (!IsValid(Button)) {
		return;
	}
	Button->PlayButtonAnimation();

	// 操作不可にしておく
	ChangeCurrentOperateType(EOperateType::Lock);
}

/// <summary>
/// リザルトウィジェット初期化
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UResultWidget::InitializeResultWidget() {

	// アニメーション初期化処理
	// 現在の操作をロック状態にする
	InitializeAnimation();
	ChangeCurrentOperateType(EOperateType::Lock);
}

/// <summary>
/// アニメーション初期化
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UResultWidget::InitializeAnimation() {

	bAnimFinishFlag = false;

	// 背景の透過率
	UImage* ImageGameOver = Cast<UImage>(GetWidgetFromName(ImageGameOverName));
	if (IsValid(ImageGameOver)) {
		ImageGameOver->SetRenderOpacity(Transparent);
	}
	UImage* ImageGameClear = Cast<UImage>(GetWidgetFromName(ImageGameClearName));
	if (IsValid(ImageGameClear)) {
		ImageGameClear->SetRenderOpacity(Transparent);
	}

	// テキストの透過率
	UCanvasPanel* CanvasPanelTitle = Cast<UCanvasPanel>(GetWidgetFromName(CanvasPanelTitleName));
	if (IsValid(CanvasPanelTitle)) {
		CanvasPanelTitle->SetRenderOpacity(Transparent);
	}
	UCanvasPanel* CanvasPanelGuide = Cast<UCanvasPanel>(GetWidgetFromName(CanvasPanelGuideName));
	if (IsValid(CanvasPanelGuide)) {
		CanvasPanelGuide->SetRenderOpacity(Transparent);
	}

	// ボタンの透過率
	UHorizontalBox* HorizontalBox = Cast<UHorizontalBox>(GetWidgetFromName(HorizontalBoxName));
	if (IsValid(HorizontalBox)) {
		HorizontalBox->SetRenderOpacity(Transparent);
	}
}

/// <summary>
/// テクスチャのセット
/// </summary>
/// <param name="Texture"> テクスチャ </param>
/// <returns></returns>
void UResultWidget::SetupTexture(UTexture2DDynamic* Texture) {

	if (!IsValid(Texture)) {
		return;
	}
	UImage* ImageGameClear = Cast<UImage>(GetWidgetFromName(ImageGameClearName));
	if (!IsValid(ImageGameClear)) {
		return;
	}
	ImageGameClear->SetBrushFromTextureDynamic(Texture);
}

/// <summary>
/// ホリゾンタルボックスの取得
/// </summary>
/// <param name=""></param>
/// <returns> ホリゾンタルボックス </returns>
UHorizontalBox* UResultWidget::GetHorizontalBox() {

	return Cast<UHorizontalBox>(GetWidgetFromName(HorizontalBoxName));
}

/// <summary>
/// アニメーションの開始
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UResultWidget::StartAnimation_Implementation() {

}