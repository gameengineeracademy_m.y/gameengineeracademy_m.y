﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "Battle/Controller/BattleController.h"
#include "Battle/Interface/BattleEvent.h"
#include "../Public/TpsShootingGameInstance.h"
#include "BattleLevel.generated.h"


/// <summary>
/// レベル遷移フェーズ
/// </summary>
UENUM()
enum class ETransitionPhaseType {
	Initialize,           // 初期化処理フェーズ
	ShowLoadingWidget,    // ロード画面表示フェーズ
	LoadNextTransLevel,   // 遷移するレベルのロードフェーズ
	HandOver,             // 引継ぎ処理フェーズ
	WaitLoading,          // ロード待機フェーズ
	UnLoadCurrentLevel,   // 現在レベルのアンロードフェーズ
	PostProcess,          // ロード後処理フェーズ
	HideLoadingWidget,    // ロード画面非表示フェーズ
	Wait,                 // 待機処理フェーズ
	ChangeLevel,          // パーシスタントレベルの切替フェーズ
	Finish,               // 終了フェーズ
	MaxTypeIndex
};

/// <summary>
/// バトルレベル
/// </summary>
UCLASS()
class ABattleLevel : public ALevelScriptActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ABattleLevel();

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

	/// <summary>
	/// 完了通知処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION()
	void OnCompleted();


protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

private:

	/// <summary>
	/// レベルのロード処理
	/// </summary>
	/// <param name="LevelName"> ロードするレベル名 < / param>
	/// <param name="FunctionName"> ロード後の実行関数名 </param>
	/// <returns></returns>
	void LoadStreamLevel(FName LevelName, FName FunctionName);

	/// <summary>
	/// レベルのアンロード処理
	/// </summary>
	/// <param name="LevelName"> ロードするレベル名 < / param>
	/// <param name="FunctionName"> ロード後の実行関数名 </param>
	/// <returns></returns>
	void UnloadStreamLevel(FName LevelName, FName FunctionName);

	/// <summary>
	/// 処理完了フラグのセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetCompleted();

	/// <summary>
	/// 処理完了フラグのリセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetCompleted();

	/// <summary>
	/// 処理が完了したかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:ロード完了 </returns>
	bool IsCompleted();

	/// <summary>
	/// レベルの切替処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SwitchLevel();

	/// <summary>
	/// 現在のフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentPhase(ETransitionPhaseType PhaseType);


private:

	/// <summary>
	/// バトルコントローラ
	/// </summary>
	UPROPERTY()
	ABattleController* BattleController;

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	ETransitionPhaseType CurrentPhase;

	/// <summary>
	/// 破棄するレベルの種類
	/// </summary>
	ELevelType UnLoadSubLevel;

	/// <summary>
	/// 処理完了フラグ
	/// </summary>
	bool bCompleted;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// UUID加算値
	/// </summary>
	const int32 UUIDIndex = 1;

	/// <summary>
	/// リンケージ
	/// </summary>
	const int32 LinkageValue = 1;

	/// <summary>
	/// ロードレベルレベル名
	/// </summary>
	const FName LoadingLevelName = TEXT("Loading");

	/// <summary>
	/// チュートリアルレベル名
	/// </summary>
	const FName TutorialLevelName = TEXT("TutorialField");

	/// <summary>
	/// メインフィールド名
	/// </summary>
	const FName MainFieldLevelName = TEXT("MainField");

	/// <summary>
	/// 完了通知処理名
	/// </summary>
	const FName CompletedFunctionName = TEXT("OnCompleted");

	/// <summary>
	/// タイトルレベル
	/// </summary>
	const FName TitleLevelName = TEXT("TitleLevel");
};