﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/WeatherSystem/WeatherManager.h"
#include "Battle/Interface/BattleEvent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AWeatherManager::AWeatherManager()
	: WeatherEffectTable(nullptr)
	, SpawnPositionTable(nullptr)
	, SunnyIntensityValue(0.0f)
	, SunnySunBrightnessValue(0.0f)
	, SunnyCloudOpacityValue(0.0f)
	, SunnyCloudColor()
	, SunnySkyColor()
	, AdjustIntensityValue(0.0f)
	, AdjustSunBrightnessValue(0.0f)
	, AdjustCloudOpacityValue(0.0f)
	, AdjustCloudColorValue(0.0f)
	, AdjustSkyColorValue(0.0f)
	, SwitchWeatherWaitTime(0.0f)
	, EventInterface(nullptr)
	, CurrentWeatherPhase(EWeatherPhase::None)
	, CurrentWeatherType(EWeatherType::None)
	, WeatherEffectData()
	, ArrayWeather()
	, AccumulateTime(0.0f)
	, IntensityValue(0.0f)
	, SunBrightnessValue(0.0f)
	, CloudOpacityValue(0.0f)
	, CloudColor()
	, SkyColor() {

	// Tick処理を有効化
	PrimaryActorTick.bCanEverTick = true;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeatherManager::BeginPlay() {

	Super::BeginPlay();

	CurrentWeatherPhase = EWeatherPhase::None;
	CurrentWeatherType = EWeatherType::None;

	IntensityValue = SunnyIntensityValue;
	SunBrightnessValue = SunnySunBrightnessValue;
	CloudOpacityValue = SunnyCloudOpacityValue;
	CloudColor = SunnyCloudColor;
	SkyColor = SunnySkyColor;

	LoadWeatherClass();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AWeatherManager::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

	switch (CurrentWeatherPhase) {
	case EWeatherPhase::None: {

		break;
	}
	case EWeatherPhase::Wait: {

		//----------------------------------------
		//一定時間待機し、その後天候を切り替える
		//----------------------------------------
		//AccumulateTime += DeltaTime;
		//if (AccumulateTime < SwitchWeatherWaitTime) {
		//	break;
		//}
		//AccumulateTime = ResetTime;

		//switch (CurrentWeatherType) {
		//case EWeatherType::Sunny: {

		//	ChangeWeather(EWeatherType::Rainy);
		//	break;
		//}
		//case EWeatherType::Rainy: {

		//	ChangeWeather(EWeatherType::Sunny);
		//	break;
		//}
		//}
		break;
	}
	default: {
		//--------------------------------------
		//天候を変化させる処理
		//--------------------------------------
		switch (CurrentWeatherType) {
		case EWeatherType::Sunny: {

			SwitchSunny(DeltaTime);
			break;
		}
		case EWeatherType::Rainy: {

			SwitchRainy(DeltaTime);
			break;
		}
		}
		break;
	}
	}
}

/// <summary>
/// イベントインターフェース
/// </summary>
/// <param name=""> イベントインターフェース </param>
/// <returns></returns>
void AWeatherManager::SetEventInterface(const TScriptInterface<IBattleEvent>& Interface) {

	EventInterface = Interface;
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeatherManager::Initialize() {

	// 天候クラスの配置処理
	SetupWeatherEffect();

	// 晴れの天候に設定する
	//EWeatherType WeatherType = DecideRandomWeather();
	ChangeWeather(EWeatherType::Sunny);
}

/// <summary>
/// 晴れの時の太陽と雲の変化量の計算処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了 </returns>
bool AWeatherManager::CalcSunnyChangeSunAndCloudValues() {

	bool bIsIntensity = false;
	bool bIsSunBrightness = false;
	bool bIsCloudOpacity = false;
	bool bIsCloudColorR = false;
	bool bIsCloudColorG = false;
	bool bIsCloudColorB = false;
	bool bIsSkyColorR = false;
	bool bIsSkyColorG = false;
	bool bIsSkyColorB = false;

	// 明るさに1フレーム当たりの調整値を加算
	IntensityValue += AdjustIntensityValue;
	if (IntensityValue >= SunnyIntensityValue) {
		bIsIntensity = true;
		IntensityValue = SunnyIntensityValue;
	}

	// 太陽の明るさに1フレーム当たりの調整値を加算
	SunBrightnessValue += AdjustSunBrightnessValue;
	if (SunBrightnessValue >= SunnySunBrightnessValue) {
		bIsSunBrightness = true;
		SunBrightnessValue = SunnySunBrightnessValue;
	}

	// 雲の量に1フレーム当たりの調整値を減算
	CloudOpacityValue -= AdjustCloudOpacityValue;
	if (CloudOpacityValue <= SunnyCloudOpacityValue) {
		bIsCloudOpacity = true;
		CloudOpacityValue = SunnyCloudOpacityValue;
	}

	// 雲の色に1フレーム当たりの調整値を加算
	CloudColor.R += AdjustCloudColorValue;
	CloudColor.G += AdjustCloudColorValue;
	CloudColor.B += AdjustCloudColorValue;
	if (CloudColor.R >= SunnyCloudColor.R) {
		bIsCloudColorR = true;
		CloudColor.R = SunnyCloudColor.R;
	}

	if (CloudColor.G >= SunnyCloudColor.G) {
		bIsCloudColorG = true;
		CloudColor.G = SunnyCloudColor.G;
	}

	if (CloudColor.B >= SunnyCloudColor.B) {
		bIsCloudColorB = true;
		CloudColor.B = SunnyCloudColor.B;
	}

	// 空の色に1フレーム当たりの調整値を加算
	SkyColor.R += AdjustSkyColorValue;
	SkyColor.G += AdjustSkyColorValue;
	SkyColor.B += AdjustSkyColorValue;
	if (SkyColor.R >= SunnySkyColor.R) {
		bIsSkyColorR = true;
		SkyColor.R = SunnySkyColor.R;
	}

	if (SkyColor.G >= SunnySkyColor.G) {
		bIsSkyColorG = true;
		SkyColor.G = SunnySkyColor.G;
	}

	if (SkyColor.B >= SunnySkyColor.B) {
		bIsSkyColorB = true;
		SkyColor.B = SunnySkyColor.B;
	}

	//---------------------------------------------
	//全て目標値に到達したら処理終了
	//---------------------------------------------
	if (bIsIntensity && bIsSunBrightness && bIsCloudOpacity &&
		bIsCloudColorR && bIsCloudColorG && bIsCloudColorB &&
		bIsSkyColorR && bIsSkyColorG && bIsSkyColorB) {
		return true;
	}

	return false;
}

/// <summary>
/// 雨の時の太陽と雲の変化量の計算処理
/// </summary>
/// <param name=""></param>
/// <returns> true:処理終了 </returns>
bool AWeatherManager::CalcRainyChangeSunAndCloudValues() {

	bool bIsIntensity = false;
	bool bIsSunBrightness = false;
	bool bIsCloudOpacity = false;
	bool bIsCloudColorR = false;
	bool bIsCloudColorG = false;
	bool bIsCloudColorB = false;
	bool bIsSkyColorR = false;
	bool bIsSkyColorG = false;
	bool bIsSkyColorB = false;

	// 明るさに1フレーム当たりの調整値を加算
	IntensityValue -= AdjustIntensityValue;
	if (IntensityValue <= RainyIntensityValue) {
		bIsIntensity = true;
		IntensityValue = RainyIntensityValue;
	}

	// 太陽の明るさに1フレーム当たりの調整値を減算
	SunBrightnessValue -= AdjustSunBrightnessValue;
	if (SunBrightnessValue <= RainySunBrightnessValue) {
		bIsSunBrightness = true;
		SunBrightnessValue = RainySunBrightnessValue;
	}

	// 雲の量に1フレーム当たりの調整値を加算
	CloudOpacityValue += AdjustCloudOpacityValue;
	if (CloudOpacityValue >= RainyCloudOpacityValue) {
		bIsCloudOpacity = true;
		CloudOpacityValue = RainyCloudOpacityValue;
	}

	// 雲の色に1フレーム当たりの調整値を減算
	CloudColor.R -= AdjustCloudColorValue;
	CloudColor.G -= AdjustCloudColorValue;
	CloudColor.B -= AdjustCloudColorValue;
	if (CloudColor.R <= RainyCloudColor.R) {
		bIsCloudColorR = true;
		CloudColor.R = RainyCloudColor.R;
	}

	if (CloudColor.G <= RainyCloudColor.G) {
		bIsCloudColorG = true;
		CloudColor.G = RainyCloudColor.G;
	}

	if (CloudColor.B <= RainyCloudColor.B) {
		bIsCloudColorB = true;
		CloudColor.B = RainyCloudColor.B;
	}

	// 空の色に1フレーム当たりの調整値を減算
	SkyColor.R -= AdjustSkyColorValue;
	SkyColor.G -= AdjustSkyColorValue;
	SkyColor.B -= AdjustSkyColorValue;
	if (SkyColor.R <= RainySkyColor.R) {
		bIsSkyColorR = true;
		SkyColor.R = RainySkyColor.R;
	}

	if (SkyColor.G <= RainySkyColor.G) {
		bIsSkyColorG = true;
		SkyColor.G = RainySkyColor.G;
	}

	if (SkyColor.B <= RainySkyColor.B) {
		bIsSkyColorB = true;
		SkyColor.B = RainySkyColor.B;
	}

	//---------------------------------------------
	//全て目標値に到達したら処理終了
	//---------------------------------------------
	if (bIsIntensity && bIsSunBrightness && bIsCloudOpacity &&
		bIsCloudColorR && bIsCloudColorG && bIsCloudColorB &&
		bIsSkyColorR && bIsSkyColorG && bIsSkyColorB) {
		return true;
	}

	return false;
}

/// <summary>
/// 明るさを取得
/// </summary>
/// <param name=""></param>
/// <returns> 明るさ </returns>
float AWeatherManager::GetIntensityValue() {

	return IntensityValue;
}

/// <summary>
/// 太陽の明るさを取得
/// </summary>
/// <param name=""></param>
/// <returns> 太陽の明るさ </returns>
float AWeatherManager::GetSunBrightnessValue() {

	return SunBrightnessValue;
}

/// <summary>
/// 雲の量を取得
/// </summary>
/// <param name=""></param>
/// <returns> 雲の量 </returns>
float AWeatherManager::GetCloudOpacityValue() {

	return CloudOpacityValue;
}

/// <summary>
/// 雲の色を取得
/// </summary>
/// <param name=""></param>
/// <returns> 雲の色 </returns>
FLinearColor AWeatherManager::GetCloudColor() {

	return CloudColor;
}

/// <summary>
/// 空の色を取得
/// </summary>
/// <param name=""></param>
/// <returns> 空の色 </returns>
FLinearColor AWeatherManager::GetSkyColor() {

	return SkyColor;
}

/// <summary>
/// 現在の天候の種類を取得
/// </summary>
/// <param name=""></param>
/// <returns> 現在の天候の種類 </returns>
EWeatherType AWeatherManager::GetCurrentWeatherType() {

	return CurrentWeatherType;
}

/// <summary>
/// 天候の変化を停止する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeatherManager::StopChangeWeather() {

	AccumulateTime = ResetTime;
	CurrentWeatherPhase = EWeatherPhase::None;
}

/// <summary>
/// 天候のロード処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeatherManager::LoadWeatherClass() {

	FString Path;
	// 弾薬アイテムクラスのロード ソフト参照
	GConfig->GetString(TEXT("ActorSettings"), TEXT("WeatherPath"), Path, GGameIni);
	WeatherClass = TSoftClassPtr<AWeather>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (!WeatherClass) {
		UE_LOG(LogTemp, Display, TEXT("AWeatherManager WeatherClass Error"));
		return;
	}
}

/// <summary>
/// 天候クラスの設置
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeatherManager::SetupWeatherEffect() {

	//---------------------------------------
	// 現在のレベルを取得
	//---------------------------------------
	UTpsShootingGameInstance* GameInstance = Cast<UTpsShootingGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (!IsValid(GameInstance)) {
		return;
	}
	ELevelType CurrentLevelType = GameInstance->GetCurrentLevel();

	if (!IsValid(WeatherEffectTable) || !IsValid(SpawnPositionTable)) {
		return;
	}

	//---------------------------------------
	// 天候エフェクトの設定
	//---------------------------------------
	TArray<FName> RowNames = WeatherEffectTable->GetRowNames();
	for (FName RowName : RowNames) {

		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		FWeatherEffectData* EffectData = WeatherEffectTable->FindRow<FWeatherEffectData>(RowName, "");
		if (WeatherEffectData.Contains(EffectData->WeatherType)) {
			continue;
		}
		WeatherEffectData.Add(EffectData->WeatherType, EffectData->WeatherEffect);
	}

	//---------------------------------------
	// 天候のスポーン処理
	//---------------------------------------
	RowNames = SpawnPositionTable->GetRowNames();
	for (FName RowName : RowNames) {

		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		// 現在のレベルと異なる場合は処理終了
		FSpawnWeatherData* SpawnData = SpawnPositionTable->FindRow<FSpawnWeatherData>(RowName, "");
		if (SpawnData->LevelType != CurrentLevelType) {
			continue;
		}

		AWeather* Weather = nullptr;
		FActorSpawnParameters Params;
		FVector SpawnPos = FVector::ZeroVector;
		// 天候をスポーンし、リストに追加する
		SpawnPos.X = SpawnData->SpawnPosX;
		SpawnPos.Y = SpawnData->SpawnPosY;
		SpawnPos.Z = SpawnData->SpawnPosZ;
		Weather = GetWorld()->SpawnActor<AWeather>(WeatherClass, SpawnPos, FRotator::ZeroRotator, Params);
		if (!IsValid(Weather)) {
			continue;
		}
		ArrayWeather.Add(Weather);
	}
}

/// <summary>
/// 天候をランダムに決定する
/// </summary>
/// <param name=""></param>
/// <returns> 決定した天候の種類 </returns>
EWeatherType AWeatherManager::DecideRandomWeather() {

	EWeatherType WeatherType = EWeatherType::None;

	// 天候の種類から最大と最小の値を設定
	int32 MinNumber = static_cast<int32>(EWeatherType::Sunny);
	int32 MaxNumber = static_cast<int32>(EWeatherType::Rainy);

	int32 DecideNumber = UKismetMathLibrary::RandomIntegerInRange(MinNumber, MaxNumber);
	WeatherType = static_cast<EWeatherType>(DecideNumber);

	return WeatherType;
}

/// <summary>
/// 天候を変更する
/// </summary>
/// <param name="Type"> 変更する天候の種類 </param>
/// <returns></returns>
void AWeatherManager::ChangeWeather(EWeatherType WeatherType) {

	UE_LOG(LogTemp, Display, TEXT("WeatherManager ChangeWeather"))

	// 天候の種類が設定されていないなら処理終了
	if (WeatherType == EWeatherType::None) {
		return;
	}

	// 現在の天候の種類を変更し、
	// 現在の天候フェーズの種類を変更する
	ChangeCurrentWeatherType(WeatherType);
	ChangeCurrentWeatherPhase(EWeatherPhase::StartFirst);
}

/// <summary>
/// 天候を晴れに変化させる処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AWeatherManager::SwitchSunny(float DeltaTime) {


	switch (CurrentWeatherPhase) {
	case EWeatherPhase::StartFirst: {

		ChangeCurrentWeatherPhase(EWeatherPhase::ChangeFirst);
		break;
	}
	case EWeatherPhase::ChangeFirst: {

		for (AWeather* Weather : ArrayWeather) {

			// 天候が存在しない場合は次へ
			if (!IsValid(Weather)) {
				continue;
			}
			Weather->DestroyEffect();
		}

		AccumulateTime = ResetTime;
		ChangeCurrentWeatherPhase(EWeatherPhase::StartSecond);
		break;
	}
	case EWeatherPhase::StartSecond: {

		AccumulateTime += DeltaTime;
		if (AccumulateTime < ProcessWaitTime) {
			break;
		}
		AccumulateTime = ResetTime;
		ChangeCurrentWeatherPhase(EWeatherPhase::ChangeSecond);
		break;
	}
	case EWeatherPhase::ChangeSecond: {

		// 晴れの時の太陽の明るさと雲の値を算出し、
		// レベルに配置されている太陽と雲の値を変更
		bool IsFinish = CalcSunnyChangeSunAndCloudValues();
		EventInterface->OnChangeSettingSunAndCloud();
		if (!IsFinish) {
			break;
		}

		ChangeCurrentWeatherPhase(EWeatherPhase::Finish);
		break;
	}
	case EWeatherPhase::Finish: {

		UE_LOG(LogTemp, Display, TEXT("WeatherManager Sunny"))

		AccumulateTime = ResetTime;
		// 現在の天候フェーズを「待機」に変更
		ChangeCurrentWeatherPhase(EWeatherPhase::Wait);
		break;
	}
	default: {

		break;
	}
	}
}

/// <summary>
/// 天候を雨に変化させる処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AWeatherManager::SwitchRainy(float DeltaTime) {


	switch (CurrentWeatherPhase) {
	case EWeatherPhase::StartFirst: {

		ChangeCurrentWeatherPhase(EWeatherPhase::ChangeFirst);
		break;
	}
	case EWeatherPhase::ChangeFirst: {

		// 雨の時の太陽の明るさと雲の値を算出し、
		// レベルに配置されている太陽と雲の値を変更
		bool IsFinish = CalcRainyChangeSunAndCloudValues();
		EventInterface->OnChangeSettingSunAndCloud();
		if (!IsFinish) {
			break;
		}
		AccumulateTime = ResetTime;
		ChangeCurrentWeatherPhase(EWeatherPhase::StartSecond);
		break;
	}
	case EWeatherPhase::StartSecond: {

		AccumulateTime += DeltaTime;
		if (AccumulateTime < ProcessWaitTime) {
			break;
		}
		AccumulateTime = ResetTime;
		ChangeCurrentWeatherPhase(EWeatherPhase::ChangeSecond);
		break;
	}
	case EWeatherPhase::ChangeSecond: {

		for (AWeather* Weather : ArrayWeather) {

			// 天候が存在しない場合は次へ
			if (!IsValid(Weather)) {
				continue;
			}
			Weather->SetupWeatherEffect(WeatherEffectData[EWeatherType::Rainy]);
		}

		ChangeCurrentWeatherPhase(EWeatherPhase::Finish);
		break;
	}
	case EWeatherPhase::Finish: {

		UE_LOG(LogTemp, Display, TEXT("WeatherManager Rainy"))

		AccumulateTime = ResetTime;
		// 現在の天候フェーズを「待機」に変更
		ChangeCurrentWeatherPhase(EWeatherPhase::Wait);
		break;
	}
	default: {

		break;
	}
	}
}

/// <summary>
/// 現在のフェーズの種類を変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void AWeatherManager::ChangeCurrentWeatherPhase(EWeatherPhase PhaseType) {

	CurrentWeatherPhase = PhaseType;
}

/// <summary>
/// 現在の天候の種類を変更する
/// </summary>
/// <param name="Type"> 天候の種類 </param>
/// <returns></returns>
void AWeatherManager::ChangeCurrentWeatherType(EWeatherType Type) {

	CurrentWeatherType = Type;
}

