﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MatineeCameraShake.h"
#include "ShootCameraShake.generated.h"

/// <summary>
/// 射撃時のカメラ揺れクラス
/// </summary>
UCLASS()
class UShootCameraShake : public UMatineeCameraShake
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UShootCameraShake();
};
