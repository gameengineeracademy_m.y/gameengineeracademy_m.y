﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Widgets/FoundWidget.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UFoundWidget::UFoundWidget() {

}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UFoundWidget::NativeConstruct() {

}

/// <summary>
/// アニメーション再生
/// </summary>
/// <param name="DirectionType"> 方向 </param>
/// <returns></returns>
void UFoundWidget::PlayFoundAnimation_Implementation(EDirectionType DirectionType) {

}