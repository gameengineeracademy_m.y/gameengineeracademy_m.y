﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/GameMode/BattleGameMode.h"
#include "Battle/Controller/BattleController.h"
#include "Battle/GameState/BattleGameState.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ABattleGameMode::ABattleGameMode() {

	// Tickイベントの無効化
	PrimaryActorTick.bCanEverTick = false;

	HUDClass = ABattleUiManager::StaticClass();
	PlayerControllerClass = ABattleController::StaticClass();
	DefaultPawnClass = nullptr;
	GameStateClass = ABattleGameState::StaticClass();
}

/// <summary>
/// 生成後実行イベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleGameMode::BeginPlay() {

}