﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Engine/DataTable.h"
#include "CharacterDetail.generated.h"

/// <summary>
/// キャラクターディテール 構造体
/// </summary>
USTRUCT(BlueprintType)
struct FCharacterDetailData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// 体力
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterDetail")
	float HP;

	/// <summary>
	/// 歩行スピード
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterDetail")
	float WalkSpeed;

	/// <summary>
	/// 走行スピード
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterDetail")
	float RunSpeed;

	/// <summary>
	/// ジャンプ力
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterDetail")
	float JumpingPower;
};

/// <summary>
/// キャラクターディテールデータ
/// </summary>
UCLASS()
class UCharacterDetail : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:

	/// <summary>
	/// 実データ
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterDetail")
	FCharacterDetailData CharacterDetail;
};