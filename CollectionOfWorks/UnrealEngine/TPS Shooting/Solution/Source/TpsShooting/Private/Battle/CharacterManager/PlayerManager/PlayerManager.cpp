﻿
#include "Battle/CharacterManager/PlayerManager/PlayerManager.h"
#include "Battle/GameState/BattleGameState.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
APlayerManager::APlayerManager()
	: OperatePlayerClass()
	, OperatePlayer(nullptr)
	, PlayerSpawnTable(nullptr)
	, PlayerDetail(nullptr)
	, WeaponFactory(nullptr)
	, SpawnData() {

	// Tick処理有効化
	PrimaryActorTick.bCanEverTick = true;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerManager::BeginPlay() {

	Super::BeginPlay();

	//---------------------------------------
	// データテーブルから
	// スポーン位置とプレイヤーの向き取得
	//---------------------------------------
	InitializeSpawnData();

	// 操作プレイヤークラスのロード ソフト参照
	FString OperatePlayerPath;
	GConfig->GetString(TEXT("CharacterSettings"), TEXT("OperatePlayerPath"), OperatePlayerPath, GGameIni);
	OperatePlayerClass = TSoftClassPtr<AOperatePlayer>(FSoftObjectPath(*OperatePlayerPath)).LoadSynchronous();
	if (!OperatePlayerClass) {
		UE_LOG(LogTemp, Display, TEXT("APlayerManager OperatePlayerClass Error"));
		return;
	}
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void APlayerManager::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

	if (!IsValid(OperatePlayer)) {
		return;
	}
	OperatePlayer->Tick(DeltaTime);
}

/// <summary>
/// スポーン情報の初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerManager::InitializeSpawnData() {

	//---------------------------------------
	// 現在のレベルを取得
	//---------------------------------------
	UTpsShootingGameInstance* GameInstance = Cast<UTpsShootingGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (!IsValid(GameInstance)) {
		return;
	}
	ELevelType CurrentLevelType = GameInstance->GetCurrentLevel();

	if (!IsValid(PlayerSpawnTable)) {
		return;
	}
	TArray<FName> RowNames = PlayerSpawnTable->GetRowNames();
	for (FName RowName : RowNames) {

		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		FCharacterSpawn* CharaSpawn = PlayerSpawnTable->FindRow<FCharacterSpawn>(RowName, "");
		if (CharaSpawn->BattleLevelType != CurrentLevelType) {
			continue;
		}

		// スポーン情報に初期値を設定
		FSpawnData Data;
		Data.Position = FVector::ZeroVector;
		Data.Rotation = FRotator::ZeroRotator;
		SpawnData.Add(CharaSpawn->StageType, Data);
		// スポーン情報に実データを設定
		SpawnData[CharaSpawn->StageType].Position = FVector(CharaSpawn->PositionX, CharaSpawn->PositionY, CharaSpawn->PositionZ);
		SpawnData[CharaSpawn->StageType].Rotation.Yaw = CharaSpawn->RotationYaw;
		
		// 初期装備の武器を設定
		InitializeCharaEquipWeapon(RowName.ToString());
	}
}

/// <summary>
/// 装備する武器の初期設定
/// </summary>
/// <param name="RowNumber"> 行番号 </param>
/// <returns></returns>
void APlayerManager::InitializeCharaEquipWeapon(FString RowNumber) {

	if (EquipWeaponList.Num() > Zero) {
		return;
	}

	// 装備する武器の初期化処理
	EquipWeaponList.Empty();
	EquipWeaponList.Add(EEquipPositionType::Hand, EWeaponType::None);
	EquipWeaponList.Add(EEquipPositionType::Back, EWeaponType::None);

	TArray<FName> RowNames = PlayerInitWeaponTable->GetRowNames();
	for (FName RowName : RowNames) {

		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}

		// スポーン情報の行番号と一致しない場合は処理終了
		FCharaInitWeapon* InitWeapon = PlayerInitWeaponTable->FindRow<FCharaInitWeapon>(RowName, "");
		if (InitWeapon->SpawnNumber != RowNumber) {
			continue;
		}

		// 装備箇所がない場合は次の行へ
		if (!EquipWeaponList.Contains(InitWeapon->EquipPosition)) {
			continue;
		}
		EquipWeaponList[InitWeapon->EquipPosition] = InitWeapon->WeaponType;
	}
}

/// <summary>
/// 操作プレイヤーの生成
/// </summary>
/// <param name=""></param>
/// <returns> 操作プレイヤー </returns>
AOperatePlayer* APlayerManager::CreatePlayer() {

	//---------------------------------------
	// 現在のレベルを取得
	//---------------------------------------
	UTpsShootingGameInstance* GameInstance = Cast<UTpsShootingGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (!IsValid(GameInstance)) {
		return nullptr;
	}
	ELevelType CurrentLevelType = GameInstance->GetCurrentLevel();

	//-----------------------------------
	//プレイヤーをスポーンし、
	//ディテール情報を設定する
	//-----------------------------------
	OperatePlayer = SpawnPlayer();
	if (!IsValid(OperatePlayer)) {
		return nullptr;
	}
	OperatePlayer->SetDetailData(PlayerDetail->CharacterDetail);
	OperatePlayer->ReflectDetail();
	OperatePlayer->SetupHitPointLine();
	OperatePlayer->SetCharaType(ECharacterType::Player);
	OperatePlayer->SetCharaStateType(ECharaStateType::Fine);

	//---------------------------------
	//出現時にディゾルブ処理を実行する
	//※チュートリアルのみ
	//---------------------------------
	if (CurrentLevelType == ELevelType::Tutorial) {
		OperatePlayer->SetDissolveType(EDissolveType::Appearance);
		OperatePlayer->ChangeDissolvePhase(EDissolvePhase::Appearance);
	}
	else {
		OperatePlayer->SetDissolveType(EDissolveType::None);
		OperatePlayer->ChangeDissolvePhase(EDissolvePhase::Appearance);
	}
	OperatePlayer->InitializeDissolve();
	OperatePlayer->SetupDissolve();

	OperatePlayer->SetDissolveType(EDissolveType::None);
	OperatePlayer->ChangeDissolvePhase(EDissolvePhase::Appearance);
	OperatePlayer->InitializeDissolve();
	OperatePlayer->SetupDissolve();

	for (auto WeaponData : EquipWeaponList) {

		if (WeaponData.Value == EWeaponType::None) {
			continue;
		}

		// 武器の生成
		AWeaponBase* Weapon = WeaponFactory->CreateWeapon(WeaponData.Value);
		if (!IsValid(Weapon)) {
			return nullptr;
		}
		//---------------------------------
		//武器に対して所有者の設定
		//武器を指定の装備位置にセットする
		//---------------------------------
		Weapon->SetCharaOwner(OperatePlayer);
		Weapon->SetHidden(true);
		Weapon->SetMaxLoadedAmmos();
		EWeaponType WeaponType = Weapon->GetWeaponType();
		OperatePlayer->SetupPossessWeapon(WeaponType, WeaponData.Key);
		OperatePlayer->SetWeapon(WeaponType, Weapon);
		OperatePlayer->AttachParantSocket(Weapon, WeaponData.Key);
	}

	return OperatePlayer;
}

/// <summary>
/// 操作プレイヤーの生成
/// </summary>
/// <param name=""></param>
/// <returns> 生成した操作プレイヤー </returns>
AOperatePlayer* APlayerManager::SpawnPlayer() {

	if (IsValid(OperatePlayer)) {
		return nullptr;
	}

	// 現在進行中のエリアを取得する
	ABattleGameState* BattleGameState = Cast<ABattleGameState>(GetWorld()->GetGameState());
	if (!IsValid(BattleGameState)) {
		return nullptr;
	}
	EStageType StageType = BattleGameState->GetSavedStageType();
	if (!SpawnData.Contains(StageType)) {
		StageType = EStageType::First;
	}

	// プレイヤーのスポーン
	FActorSpawnParameters Params;
	AOperatePlayer* Player = GetWorld()->SpawnActor<AOperatePlayer>(OperatePlayerClass, SpawnData[StageType].Position, SpawnData[StageType].Rotation, Params);
	if (!IsValid(Player)) {
		UE_LOG(LogTemp, Display, TEXT("APlayerManager OperatePlayerSpawn Error"));
		return nullptr;
	}

	return Player;
}

/// <summary>
/// 操作プレイヤーの取得
/// </summary>
/// <param name=""></param>
/// <returns> 操作プレイヤー </returns>
AOperatePlayer* APlayerManager::GetOperatePlayer() {

	return OperatePlayer;
}

/// <summary>
/// ウェポンファクトリーのセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerManager::SetWeaponFactory(AWeaponFactory* Factory) {

	WeaponFactory = Factory;
}

/// <summary>
/// ウェポンファクトリーの取得
/// </summary>
/// <param name=""></param>
/// <returns> ウェポンファクトリー </returns>
AWeaponFactory* APlayerManager::GetWeaponFactory() {

	return WeaponFactory;
}

/// <summary>
/// 操作プレイヤーの破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerManager::DisposePlayer() {

	if (!IsValid(OperatePlayer)) {
		return;
	}

	// 武器とプレイヤーを破棄する
	OperatePlayer->DestroyWeapon();
	OperatePlayer->Destroy();
	OperatePlayer = nullptr;
}

/// <summary>
/// 装備武器の保存
/// </summary>
/// <param name="PossessWeaponList"> 装備武器リスト </param>
/// <returns></returns>
void APlayerManager::SaveEquipWeapon(TMap<EEquipPositionType, EWeaponType> PossessWeaponList) {

	EquipWeaponList = PossessWeaponList;
}

/// <summary>
/// 保存された武器情報の取得
/// </summary>
/// <param name=""></param>
/// <returns> 装備武器リスト </returns>
TMap<EEquipPositionType, EWeaponType> APlayerManager::GetSaveEquipWeapon() {

	return EquipWeaponList;
}

/// <summary>
/// 指定の武器がセーブされているかどうか
/// </summary>
/// <param name="WeaponType"> 武器の種類 </param>
/// <returns> true:セーブ済み </returns>
bool APlayerManager::IsSavePossessWeapon(EWeaponType WeaponType) {

	for (auto Weapon : EquipWeaponList) {
		if (Weapon.Value != WeaponType) {
			continue;
		}
		return true;
	}

	return false;
}