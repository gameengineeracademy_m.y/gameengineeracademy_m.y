﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Widgets/Map/MiniMapWidget.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/Image.h"
#include "Components/Border.h"
#include "Components/Overlay.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UMiniMapWidget::UMiniMapWidget()
	: MapRotCenterPos()
	, CameraRotAngle(0.0f) {

}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMiniMapWidget::NativeConstruct() {

	Super::NativeConstruct();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="Geometry"> ジオメトリ </param>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void UMiniMapWidget::NativeTick(const FGeometry& Geometry, float DeltaTime) {

	Super::NativeTick(Geometry, DeltaTime);

	// プレイヤーアイコン更新
	UpdatePlayerIcon();
	// エネミーアイコン更新
	UpdateEnemyIcon();
	// アイテムアイコン更新
	UpdatePickupItemIcon();
	// ゴールアイコン更新
	UpdateGoalIcon();
}

/// <summary>
/// プレイヤー位置更新
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMiniMapWidget::UpdatePlayerIcon() {

	if (!IsValid(PlayerManager)) {
		return;
	}

	AOperatePlayer* Player = PlayerManager->GetOperatePlayer();
	if (!IsValid(Player)) {
		return;
	}

	// 実エリア座標からエリア上に配置した基準ボリュームと
	// イメージマップのスケール比を用いてミニマップ上での位置を算出
	FVector CurrentPlayerPos = Player->GetActorLocation();
	MapCenterPos = GetPositionOnMap(CurrentPlayerPos) * OppositeDirection;

	// ミニマップの回転中心位置を算出
	// ※ミニマップの中心を原点として算出
	MapRotCenterPos = (FVector2D(MapHalfSize, MapHalfSize) - MapCenterPos) / MapSize;

	// プレイヤーとカメラの向きを度数で算出
	FVector Direction = Player->GetActorForwardVector();
	float PlayerAngle = UKismetMathLibrary::DegAtan2(Direction.Y, Direction.X);
	CameraRotAngle = Player->GetControlRotation().Yaw;

	// プレイヤーアイコンの方向はカメラの回転量を考慮する
	// ※カメラ回転時にアイコンの向きに影響を与えないようにするため
	PlayerAngle += CameraRotAngle * OppositeDirection;
	CameraRotAngle = (CameraRotAngle + Angle90) * OppositeDirection;

	UImage* PlayerImage = Cast<UImage>(GetWidgetFromName(ImagePlayerName));
	UBorder* MapImage = Cast<UBorder>(GetWidgetFromName(BorderMapName));
	if (!IsValid(MapImage) || !IsValid(PlayerImage)) {
		return;
	}

	// プレイヤーがミニマップ上に存在する位置と
	// 偏心率、回転角度の3点をミニマップに渡す
	FWidgetTransform MapImageTransform;
	MapImageTransform.Translation = MapCenterPos;
	MapImageTransform.Angle = CameraRotAngle;
	MapImage->SetRenderTransform(MapImageTransform);
	MapImage->SetRenderTransformPivot(MapRotCenterPos);

	// プレイヤーアイコンにプレイヤーの角度を渡す
	FWidgetTransform PlayerImageTransform;
	PlayerImageTransform.Angle = PlayerAngle;
	PlayerImage->SetRenderTransform(PlayerImageTransform);
}

/// <summary>
/// エネミーアイコンの更新
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMiniMapWidget::UpdateEnemyIcon() {

	if (!IsValid(EnemyManager)) {
		return;
	}

	for (UBattleHUDMapIcon* MapIcon : EnemyIconList) {

		if (!IsValid(MapIcon)) {
			continue;
		}

		// 紐づけ番号を元にエネミーを取得、
		// 実エリアの座標を取得し、マップ上の座標を算出する
		int32 EnemyIndex = MapIcon->GetObjectIndex();
		AEnemy* Enemy = EnemyManager->GetEnemy(EnemyIndex);
		if (!IsValid(Enemy)) {
			continue;
		}

		if (!Enemy->IsFine()) {
			MapIcon->SetDisplay(false);
			MapIcon->SetRenderOpacity(Transparent);
			continue;
		}

		// 敵が表示待機中ならアイコンを非表示にする
		if (Enemy->IsWaitAppearance()) {
			MapIcon->SetDisplay(false);
			MapIcon->SetRenderOpacity(Transparent);
			continue;
		}

		// 現在いるエリアでないならアイコンを非表示にする
		EStageType StageType = Enemy->GetStageType();
		if (StageType != CurrentStageType) {
			MapIcon->SetDisplay(false);
			MapIcon->SetRenderOpacity(Transparent);
			continue;
		}

		MapIcon->SetDisplay(true);
		MapIcon->SetRenderOpacity(Opacity);

		FVector EnemyPosition = Enemy->GetActorLocation();
		FVector2D MapPosition = GetPositionOnMap(EnemyPosition);

		// アイコンの座標設定
		FWidgetTransform ItemTransform;
		ItemTransform.Translation = FVector2D(MapPosition.X, MapPosition.Y);
		MapIcon->SetRenderTransform(ItemTransform);
	}
}

/// <summary>
/// アイテムアイコンの更新
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMiniMapWidget::UpdatePickupItemIcon() {

	if (!IsValid(PickupItemManager)) {
		return;
	}

	for (UBattleHUDMapIcon* MapIcon : PickupItemIconList) {

		if (!IsValid(MapIcon)) {
			continue;
		}

		// 紐づけ番号を元にアイテムを取得
		int32 ItemIndex = MapIcon->GetObjectIndex();
		APickupItemBase* Item = PickupItemManager->GetPickupItem(ItemIndex);
		if (!IsValid(Item)) {
			continue;
		}

		// アイテムが拾われているならアイコンを非表示にする
		if (Item->IsPickup()) {
			MapIcon->SetDisplay(false);
			MapIcon->SetRenderOpacity(Transparent);
			continue;
		}
		// マップ上のアイテムは常に同じ向きで表示させるため、
		// カメラの回転と反対方向に回転させる
		float Angle = OppositeDirection * CameraRotAngle;
		MapIcon->SetRenderTransformAngle(Angle);
	}
}

/// <summary>
/// ゴールアイコンの更新
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMiniMapWidget::UpdateGoalIcon() {

	if (!IsValid(TargetObjectManager)) {
		return;
	}

	for (UBattleHUDMapIcon* MapIcon : GoalIconList) {

		if (!IsValid(MapIcon)) {
			continue;
		}

		// 紐づけ番号を元にアイテムを取得
		int32 GoalIndex = MapIcon->GetObjectIndex();
		AGoalObject* GoalObject = TargetObjectManager->GetGoalObject(GoalIndex);
		if (!IsValid(GoalObject)) {
			continue;
		}

		// 現在いるエリアでないならアイコンを非表示にする
		EStageType StageType = GoalObject->GetStageType();
		if (StageType != CurrentStageType) {
			MapIcon->SetDisplay(false);
			MapIcon->SetRenderOpacity(Transparent);

			if (MapIcon->IsPlayBackAnim()) {
				MapIcon->SetupDisplayBackImage(ESlateVisibility::Collapsed);
				MapIcon->StopBackAnimation();
			}
			continue;
		}

		MapIcon->SetDisplay(true);
		MapIcon->SetRenderOpacity(Opacity);

		if (!MapIcon->IsPlayBackAnim()) {
			MapIcon->SetupDisplayBackImage(ESlateVisibility::Visible);
			MapIcon->PlayBackAnimation();
		}

		// マップ上のアイテムは常に同じ向きで表示させるため、
		// カメラの回転と反対方向に回転させる
		float Angle = OppositeDirection * CameraRotAngle;
		MapIcon->SetRenderTransformAngle(Angle);
	}
}

/// <summary>
/// 発見マーカー再生
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMiniMapWidget::PlayFoundMarkerAnimation_Implementation() {

}

/// <summary>
/// 発見マーカー停止
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMiniMapWidget::StopFoundMarkerAnimation_Implementation() {

}

/// <summary>
/// 発見マーカーキャンセル
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMiniMapWidget::CancelFoundMarkerAnimation_Implementation() {

}