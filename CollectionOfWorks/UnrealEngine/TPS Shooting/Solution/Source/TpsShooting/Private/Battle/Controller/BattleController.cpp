﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Controller/BattleController.h"
#include "GameFramework/PlayerInput.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ABattleController::ABattleController() {

}

/// <summary>
/// インプットコンポーネントの設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::SetupInputComponent() {

	Super::SetupInputComponent();
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::BeginPlay() {

	Super::BeginPlay();

	// 現在のマウスカーソルの位置を取得しておく
	GetMousePosition(OldCurPosX, OldCurPosY);

	// 初期化処理
	CreateAudioManager();
}

/// <summary>
/// オーディオマネージャの生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::CreateAudioManager() {

	// オーディオマネージャの生成
	FString AudioManagerPath;
	GConfig->GetString(TEXT("ActorSettings"), TEXT("AudioManagerPath"), AudioManagerPath, GGameIni);

	AudioManagerClass = TSoftClassPtr<AAudioManager>(FSoftObjectPath(*AudioManagerPath)).LoadSynchronous();
	if (!IsValid(AudioManagerClass)) {
		return;
	}
	AudioManager = GetWorld()->SpawnActor<AAudioManager>(AudioManagerClass);
}

/// <summary>
/// インプットの設定処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::SetupInput() {

	//UE4のアサートマクロ
	check(InputComponent);

	if (!IsValid(InputComponent)) {
		return;
	}

	//------------------------------------------
	//キーバインドとアクションを設定
	//マウスカーソル表示設定
	//------------------------------------------
	UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("MoveForward", EKeys::W, ForwardMoveValue));
	UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("MoveForward", EKeys::S, BackMoveValue));
	UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("MoveRight", EKeys::D, RightMoveValue));
	UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("MoveRight", EKeys::A, LeftMoveValue));
	UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("Turn", EKeys::MouseX, RightMoveValue));
	UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("LookUp", EKeys::MouseY, LookUpValue));
	UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("SwitchWeapon", EKeys::MouseWheelAxis , MouseWheelValue));

	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Shoot", EKeys::LeftMouseButton));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Run", EKeys::LeftShift));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Crouch", EKeys::Q));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Aim", EKeys::RightMouseButton));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Jump", EKeys::SpaceBar));

	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("AreaMap", EKeys::Tab));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Pause", EKeys::Escape));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Blind", EKeys::B));

	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Up", EKeys::Up));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Down", EKeys::Down));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Left", EKeys::Left));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Right", EKeys::Right));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Up", EKeys::W));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Down", EKeys::S));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Left", EKeys::A));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Right", EKeys::D));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Reload", EKeys::E));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Decide", EKeys::F));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Return", EKeys::RightMouseButton));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Skip", EKeys::S));

	InputComponent->BindAction("Pause", IE_Pressed, this, &ABattleController::PressEscape);
	InputComponent->BindAction("AreaMap", IE_Pressed, this, &ABattleController::PressTab);
	InputComponent->BindAction("Blind", IE_Pressed, this, &ABattleController::PressB);
}

/// <summary>
/// イベントインターフェース
/// </summary>
/// <param name=""> イベントインターフェース </param>
/// <returns></returns>
void ABattleController::SetEventInterface(const TScriptInterface<IBattleEvent>& Interface) {

	EventInterface = Interface;
}

/// <summary>
/// ロード処理時のUI生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::CreateUiForLoading() {

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// ロード処理時のUI表示準備処理
	UiManager->CreateUiForLoading();
}

/// <summary>
/// ロード処理時のUI表示準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::PrepareOpenUiForLoading() {

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// ロード処理時のUI表示準備処理
	UiManager->PrepareOpenUiForLoading();
}

/// <summary>
/// ロード処理時のUI表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::OpenUiForLoading() {

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 各ウィジェットの表示処理
	UiManager->OpenUiForLoading();
}

/// <summary>
/// ロード処理時のUI非表示準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::PrepareCloseUiForLoading() {

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 各ウィジェットの非表示準備処理
	UiManager->PrepareCloseUiForLoading();
}

/// <summary>
/// ロード処理時のUI非表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::CloseUiForLoading() {

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 非表示処理実行
	UiManager->CloseUiForLoading();
}

/// <summary>
/// 各ウィジェットのUI生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::CreateUi() {

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// ロード処理時のUI表示準備処理
	UiManager->CreateUi();
}

/// <summary>
/// UI表示準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::PrepareOpenUi() {

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// ロード処理時のUI表示準備処理
	UiManager->PrepareOpenUi();
}

/// <summary>
/// UI表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::OpenUi() {

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 各ウィジェットの表示処理
	UiManager->OpenUi();
}

/// <summary>
/// UI非表示準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::PrepareCloseUi() {

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 各ウィジェットの非表示準備処理
	UiManager->PrepareCloseUi();
}

/// <summary>
/// UI非表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::CloseUi() {

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 非表示処理実行
	UiManager->CloseUi();
}

/// <summary>
/// フェードウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> フェードウィジェット </returns>
UFadeWidget* ABattleController::GetFadeWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// フェードウィジェット取得
	return UiManager->GetFadeWidget();
}

/// <summary>
/// 照準ウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> 照準ウィジェット </returns>
UCrossHairWidget* ABattleController::GetCrossHairWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// 照準ウィジェット取得
	return UiManager->GetCrossHairWidget();
}

/// <summary>
/// 照準の中心点ウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> 照準の中心点ウィジェット </returns>
UCenterPointWidget* ABattleController::GetCenterPointWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// 照準の中心点ウィジェット取得
	return UiManager->GetCenterPointWidget();
}

/// <summary>
/// ポーズメニューウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> ポーズメニューウィジェット </returns>
UPauseMenuWidget* ABattleController::GetPauseMenuWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// ポーズメニューウィジェット取得
	return UiManager->GetPauseMenuWidget();
}

/// <summary>
/// オーディオメニューウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> オーディオメニューウィジェット </returns>
UAudioMenuWidget* ABattleController::GetAudioMenuWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// オーディオメニューウィジェット取得
	return UiManager->GetAudioMenuWidget();
}

/// <summary>
/// リザルトウィジェットの取得
/// </summary>
/// <param name="OptionType"> ウィジェットの種類 </param>
/// <returns> リザルトウィジェット </returns>
UResultWidget* ABattleController::GetResultWidget(EWidgetType WidgetType) {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// リザルトウィジェット取得
	return UiManager->GetResultWidget(WidgetType);
}

/// <summary>
/// バトルHUDウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> バトルHUDウィジェット </returns>
UBattleWidget* ABattleController::GetBattleWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// バトルHUDウィジェット取得
	return UiManager->GetBattleWidget();
}

/// <summary>
/// ミニマップウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> ミニマップウィジェット </returns>
UMiniMapWidget* ABattleController::GetMiniMapWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// ミニマップウィジェット取得
	return UiManager->GetMiniMapWidget();
}

/// <summary>
/// エリアマップウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> エリアマップウィジェット </returns>
UAreaMapWidget* ABattleController::GetAreaMapWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// エリアマップウィジェット取得
	return UiManager->GetAreaMapWidget();
}

/// <summary>
/// 瀕死ウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> 瀕死ウィジェット </returns>
ULowHealthWidget* ABattleController::GetLowHealthWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// 瀕死ウィジェット取得
	return UiManager->GetLowHealthWidget();
}

/// <summary>
/// 被弾ウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> 被弾ウィジェット </returns>
UShootedWidget* ABattleController::GetShootedWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// 被弾ウィジェット取得
	return UiManager->GetShootedWidget();
}

/// <summary>
/// 発見ウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> 発見ウィジェット </returns>
UFoundWidget* ABattleController::GetFoundWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// 発見ウィジェット取得
	return UiManager->GetFoundWidget();
}

/// <summary>
/// ポーズウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> ポーズウィジェット </returns>
UPauseWidget* ABattleController::GetPauseWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// ポーズウィジェット取得
	return UiManager->GetPauseWidget();
}

/// <summary>
/// 操作一覧ウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> 操作一覧ウィジェット </returns>
UOperateListWidget* ABattleController::GetOperateListWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// 操作一覧ウィジェット取得
	return UiManager->GetOperateListWidget();
}

/// <summary>
/// カメラ演出ガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> カメラ演出ガイドウィジェット </returns>
UCameraGuideWidget* ABattleController::GetCameraGuideWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// カメラ演出ガイドウィジェット取得
	return UiManager->GetCameraGuideWidget();
}

/// <summary>
/// カメラ演出時のポーズガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> カメラ演出時のポーズガイドウィジェット </returns>
UPauseDirectionGuideWidget* ABattleController::GetPauseDirectionGuideWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// カメラ演出時のポーズガイドウィジェット取得
	return UiManager->GetPauseDirectionGuideWidget();
}

/// <summary>
/// オーディオガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> オーディオガイドウィジェット </returns>
UAudioGuideWidget* ABattleController::GetAudioGuideWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// オーディオガイドウィジェット取得
	return UiManager->GetAudioGuideWidget();
}

/// <summary>
/// マップガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> マップガイドウィジェット </returns>
UMapGuideWidget* ABattleController::GetMapGuideWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// マップガイドウィジェット取得
	return UiManager->GetMapGuideWidget();
}

/// <summary>
/// ポーズガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> ポーズガイドウィジェット </returns>
UPauseGuideWidget* ABattleController::GetPauseGuideWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// ポーズガイドウィジェット取得
	return UiManager->GetPauseGuideWidget();
}

/// <summary>
/// リザルトガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> リザルトガイドウィジェット </returns>
UResultGuideWidget* ABattleController::GetResultGuideWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// リザルトガイドウィジェット取得
	return UiManager->GetResultGuideWidget();
}

/// <summary>
/// 操作一覧ガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> 操作一覧ガイドウィジェット </returns>
UOperateListGuideWidget* ABattleController::GetOperateListGuideWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	// 操作一覧ガイドウィジェット取得
	return UiManager->GetOperateListGuideWidget();
}

/// <summary>
/// オーディオマネージャの取得
/// </summary>
/// <param name=""></param>
/// <returns> オーディオマネージャ </returns>
AAudioManager* ABattleController::GetAudioManager() {

	return AudioManager;
}

/// <summary>
/// Escapeキー押下処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::PressEscape() {

	EventInterface->OnSwitchPause();
}

/// <summary>
/// Tabキー押下処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::PressTab() {

	EventInterface->OnSwitchMap();
}

/// <summary>
/// Bキー押下処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::PressB() {

	EventInterface->OnSetupSight();
}

/// <summary>
/// インプットの種類を追加
/// </summary>
/// <param name="InputType"> 入力の種類 </param>
/// <returns></returns>
void ABattleController::AddInputType(EBattleInputType InputType) {

	ArrayInputType.Push(InputType);
	SwitchKeyBindAction(InputType);
}

/// <summary>
/// インプットの種類を削除
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::DeleteInputType() {

	ArrayInputType.Pop();

	EBattleInputType InputType = GetInputType();
	if (InputType == EBattleInputType::None) {
		InputType = EBattleInputType::Game;
		AddInputType(InputType);
	}

	SwitchKeyBindAction(InputType);
}

/// <summary>
/// インプットの種類を取得
/// </summary>
/// <param name=""></param>
/// <returns> インプットの種類 </returns>
EBattleInputType ABattleController::GetInputType() {

	if (!IsExistsInput()) {
		return EBattleInputType::None;
	}

	return ArrayInputType[ArrayInputType.Num() - One];
}

/// <summary>
/// キーバインドアクションを切り替える
/// </summary>
/// <param name="InputType"> 入力の種類 </param>
/// <returns></returns>
void ABattleController::SwitchKeyBindAction(EBattleInputType InputType) {

	switch (InputType) {
	case EBattleInputType::Game: {

		SetupKeyBindActionGamePlay();
		break;
	}
	case EBattleInputType::Map: {

		SetupKeyBindActionMap();
		break;
	}
	case EBattleInputType::CameraDirection: {

		SetupKeyBindActionCameraDirection();
		break;
	}
	case EBattleInputType::PausePlay: {

		SetupKeyBindActionPausePlay();
		break;
	}
	case EBattleInputType::PauseDirection: {

		SetupKeyBindActionPauseDirection();
		break;
	}
	case EBattleInputType::Audio: {

		SetupKeyBindActionAudio();
		break;
	}
	case EBattleInputType::OperateList: {

		SetupKeyBindActionOperateList();
		break;
	}
	case EBattleInputType::TutorialClear:
	case EBattleInputType::GameOver:
	case EBattleInputType::GameClear: {

		SetupKeyBindActionResult();
		break;
	}
	}
}

/// <summary>
/// ゲームプレイ中のキーバインドアクション設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::SetupKeyBindActionGamePlay() {

	// 一度全てのキーバインドを外す
	InputComponent->ClearActionBindings();
	// キーのバインド設定
	InputComponent->BindAction("Pause", IE_Pressed, this, &ABattleController::PressEscape);
	InputComponent->BindAction("AreaMap", IE_Pressed, this, &ABattleController::PressTab);
	InputComponent->BindAction("Blind", IE_Pressed, this, &ABattleController::PressB);
}

/// <summary>
/// マップ用のキーバインドアクション設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::SetupKeyBindActionMap() {

	// 一度全てのキーバインドを外す
	InputComponent->ClearActionBindings();
	// キーのバインド設定
	InputComponent->BindAction("AreaMap", IE_Pressed, this, &ABattleController::PressTab);
}

/// <summary>
/// 演出用のキーバインドアクション設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::SetupKeyBindActionCameraDirection() {

	// 一度全てのキーバインドを外す
	InputComponent->ClearActionBindings();
	// キーのバインド設定
	InputComponent->BindAction("Pause", IE_Pressed, this, &ABattleController::PressEscape).bExecuteWhenPaused = true;
	InputComponent->BindAction("Skip", IE_Pressed, this, &ABattleController::PressS);
	InputComponent->BindAction("Skip", IE_Repeat, this, &ABattleController::PressS);
	InputComponent->BindAction("Skip", IE_Released, this, &ABattleController::ReleaseS);
}

/// <summary>
/// ポーズ用のキーバインドアクション設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::SetupKeyBindActionPausePlay() {

	// 一度全てのキーバインドを外す
	InputComponent->ClearActionBindings();
	// キーのバインド設定
	InputComponent->BindAction("Pause", IE_Pressed, this, &ABattleController::PressEscape).bExecuteWhenPaused = true;
	InputComponent->BindAction("Up", IE_Pressed, this, &ABattleController::PressUp).bExecuteWhenPaused = true;
	InputComponent->BindAction("Down", IE_Pressed, this, &ABattleController::PressDown).bExecuteWhenPaused = true;
	InputComponent->BindAction("Left", IE_Pressed, this, &ABattleController::PressLeft).bExecuteWhenPaused = true;
	InputComponent->BindAction("Right", IE_Pressed, this, &ABattleController::PressRight).bExecuteWhenPaused = true;
	InputComponent->BindAction("Decide", IE_Pressed, this, &ABattleController::PressDecide).bExecuteWhenPaused = true;
}

/// <summary>
/// 演出ポーズ用のキーバインドアクション設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::SetupKeyBindActionPauseDirection() {

	// 一度全てのキーバインドを外す
	InputComponent->ClearActionBindings();
	// キーのバインド設定
	InputComponent->BindAction("Pause", IE_Pressed, this, &ABattleController::PressEscape).bExecuteWhenPaused = true;
}

/// <summary>
/// オーディオ用のキーバインドアクション設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::SetupKeyBindActionAudio() {

	// 一度全てのキーバインドを外す
	InputComponent->ClearActionBindings();
	// キーのバインド設定
	InputComponent->BindAction("Up", IE_Pressed, this, &ABattleController::PressUp).bExecuteWhenPaused = true;
	InputComponent->BindAction("Down", IE_Pressed, this, &ABattleController::PressDown).bExecuteWhenPaused = true;
	InputComponent->BindAction("Left", IE_Pressed, this, &ABattleController::PressLeft).bExecuteWhenPaused = true;
	InputComponent->BindAction("Left", IE_Repeat, this, &ABattleController::PressLeft).bExecuteWhenPaused = true;
	InputComponent->BindAction("Right", IE_Pressed, this, &ABattleController::PressRight).bExecuteWhenPaused = true;
	InputComponent->BindAction("Right", IE_Repeat, this, &ABattleController::PressRight).bExecuteWhenPaused = true;
	InputComponent->BindAction("Decide", IE_Pressed, this, &ABattleController::PressDecide).bExecuteWhenPaused = true;
	InputComponent->BindAction("Return", IE_Pressed, this, &ABattleController::PressReturn).bExecuteWhenPaused = true;
}

/// <summary>
/// 操作一覧用のキーバインドアクション設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::SetupKeyBindActionOperateList() {

	// 一度全てのキーバインドを外す
	InputComponent->ClearActionBindings();
	// キーのバインド設定
	InputComponent->BindAction("Return", IE_Pressed, this, &ABattleController::PressReturn).bExecuteWhenPaused = true;
}

/// <summary>
/// リザルト用のキーバインドアクション設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::SetupKeyBindActionResult() {

	// 一度全てのキーバインドを外す
	InputComponent->ClearActionBindings();
	// キーのバインド設定
	InputComponent->BindAction("Left", IE_Pressed, this, &ABattleController::PressLeft).bExecuteWhenPaused = true;
	InputComponent->BindAction("Right", IE_Pressed, this, &ABattleController::PressRight).bExecuteWhenPaused = true;
	InputComponent->BindAction("Decide", IE_Pressed, this, &ABattleController::PressDecide).bExecuteWhenPaused = true;
}

/// <summary>
/// 設定されたインプットが存在するかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:存在する </returns>
bool ABattleController::IsExistsInput() {

	return ArrayInputType.Num() > Zero;
}

/// <summary>
/// 決定ボタン押下処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::PressDecide() {

	if (!IsExistsInput()) {
		return;
	}

	EBattleInputType InputType = GetInputType();
	switch (InputType) {
	case EBattleInputType::PausePlay: {

		//ポーズメニューウィジェットを取得
		UPauseMenuWidget* PauseMenuWidget = GetPauseMenuWidget();
		if (!IsValid(PauseMenuWidget)) {
			break;
		}
		PauseMenuWidget->PressDecide();
		break;
	}
	case EBattleInputType::Audio: {

		//オーディオメニューウィジェットを取得
		UAudioMenuWidget* AudioMenuWidget = GetAudioMenuWidget();
		if (!IsValid(AudioMenuWidget)) {
			break;
		}
		AudioMenuWidget->PressDecide();
		break;
	}
	case EBattleInputType::TutorialClear:
	case EBattleInputType::GameOver:
	case EBattleInputType::GameClear: {

		//リザルトウィジェットを取得
		UResultWidget* ResultWidget = nullptr;
		
		switch (InputType) {
		case EBattleInputType::TutorialClear: {
			ResultWidget = GetResultWidget(EWidgetType::TutorialClear);
			break;
		}
		case EBattleInputType::GameOver: {
			ResultWidget = GetResultWidget(EWidgetType::GameOver);
			break;
		}
		case EBattleInputType::GameClear: {
			ResultWidget = GetResultWidget(EWidgetType::GameClear);
			break;
		}
		}
		if (!IsValid(ResultWidget)) {
			break;
		}

		bool IsLock = ResultWidget->IsLock();
		if (IsLock) {
			break;
		}
		// 決定処理を行う
		ResultWidget->Decide();
		break;
	}
	default: {

		break;
	}
	}
}

/// <summary>
/// 戻るボタン押下処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::PressReturn() {

	if (!IsExistsInput()) {
		return;
	}

	EBattleInputType InputType = GetInputType();
	switch (InputType) {
	case EBattleInputType::PausePlay: {

		//ポーズウィジェットを取得
		UPauseMenuWidget* PauseMenuWidget = GetPauseMenuWidget();
		if (!IsValid(PauseMenuWidget)) {
			return;
		}
		PauseMenuWidget->PressReturn();
		break;
	}
	case EBattleInputType::Audio: {

		//オーディオメニューウィジェットを取得
		UAudioMenuWidget* AudioMenuWidget = GetAudioMenuWidget();
		if (!IsValid(AudioMenuWidget)) {
			return;
		}
		AudioMenuWidget->PressReturn();
		break;
	}
	case EBattleInputType::OperateList: {

		//操作一覧ウィジェットを取得
		UOperateListWidget* OperateListWidget = GetOperateListWidget();
		if (!IsValid(OperateListWidget)) {
			return;
		}
		OperateListWidget->PressReturn();
		break;
	}
	default: {

		break;
	}
	}
}

/// <summary>
/// 上方向ボタン押下処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::PressUp() {

	if (!IsExistsInput()) {
		return;
	}

	EBattleInputType InputType = GetInputType();
	switch (InputType) {
	case EBattleInputType::PausePlay: {

		//ポーズメニューウィジェットを取得
		UPauseMenuWidget* PauseMenuWidget = GetPauseMenuWidget();
		if (!IsValid(PauseMenuWidget)) {
			return;
		}
		PauseMenuWidget->PressUp();
		break;
	}
	case EBattleInputType::Audio: {

		//オーディオメニューウィジェットを取得
		UAudioMenuWidget* AudioMenuWidget = GetAudioMenuWidget();
		if (!IsValid(AudioMenuWidget)) {
			return;
		}
		AudioMenuWidget->PressUp();
		break;
	}
	default: {

		break;
	}
	}
}

/// <summary>
/// 下方向ボタン押下処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::PressDown() {

	if (!IsExistsInput()) {
		return;
	}

	EBattleInputType InputType = GetInputType();
	switch (InputType) {
	case EBattleInputType::PausePlay: {

		//ポーズメニューウィジェットを取得
		UPauseMenuWidget* PauseMenuWidget = GetPauseMenuWidget();
		if (!IsValid(PauseMenuWidget)) {
			return;
		}
		PauseMenuWidget->PressDown();
		break;
	}
	case EBattleInputType::Audio: {

		//オーディオメニューウィジェットを取得
		UAudioMenuWidget* AudioMenuWidget = GetAudioMenuWidget();
		if (!IsValid(AudioMenuWidget)) {
			return;
		}
		AudioMenuWidget->PressDown();
		break;
	}
	default: {

		break;
	}
	}
}

/// <summary>
/// 左方向ボタン押下処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::PressLeft() {

	if (!IsExistsInput()) {
		return;
	}

	EBattleInputType InputType = GetInputType();
	switch (InputType) {
	case EBattleInputType::Audio: {

		//オーディオメニューウィジェットを取得
		UAudioMenuWidget* AudioMenuWidget = GetAudioMenuWidget();
		if (!IsValid(AudioMenuWidget)) {
			return;
		}
		AudioMenuWidget->AdjustVolumeDown();
		break;
	}
	case EBattleInputType::TutorialClear:
	case EBattleInputType::GameOver:
	case EBattleInputType::GameClear: {

		//リザルトウィジェットを取得
		UResultWidget* ResultWidget = nullptr;

		switch (InputType) {
		case EBattleInputType::TutorialClear: {
			ResultWidget = GetResultWidget(EWidgetType::TutorialClear);
			break;
		}
		case EBattleInputType::GameOver: {
			ResultWidget = GetResultWidget(EWidgetType::GameOver);
			break;
		}
		case EBattleInputType::GameClear: {
			ResultWidget = GetResultWidget(EWidgetType::GameClear);
			break;
		}
		}
		if (!IsValid(ResultWidget)) {
			break;
		}

		bool IsLock = ResultWidget->IsLock();
		if (IsLock) {
			break;
		}
		// 左方向処理を行う
		ResultWidget->PressLeft();
		break;
	}
	default: {

		break;
	}
	}
}

/// <summary>
/// 右方向ボタン押下処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::PressRight() {

	if (!IsExistsInput()) {
		return;
	}

	EBattleInputType InputType = GetInputType();
	switch (InputType) {
	case EBattleInputType::Audio: {

		//オーディオメニューウィジェットを取得
		UAudioMenuWidget* AudioMenuWidget = GetAudioMenuWidget();
		if (!IsValid(AudioMenuWidget)) {
			return;
		}
		AudioMenuWidget->AdjustVolumeUp();
		break;
	}
	case EBattleInputType::TutorialClear:
	case EBattleInputType::GameOver:
	case EBattleInputType::GameClear: {

		//リザルトウィジェットを取得
		UResultWidget* ResultWidget = nullptr;

		switch (InputType) {
		case EBattleInputType::TutorialClear: {
			ResultWidget = GetResultWidget(EWidgetType::TutorialClear);
			break;
		}
		case EBattleInputType::GameOver: {
			ResultWidget = GetResultWidget(EWidgetType::GameOver);
			break;
		}
		case EBattleInputType::GameClear: {
			ResultWidget = GetResultWidget(EWidgetType::GameClear);
			break;
		}
		}
		if (!IsValid(ResultWidget)) {
			break;
		}

		bool IsLock = ResultWidget->IsLock();
		if (IsLock) {
			break;
		}
		// 右方向処理を行う
		ResultWidget->PressRight();
		break;
	}
	default: {

		break;
	}
	}
}

/// <summary>
/// Sボタン押下処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::PressS() {

	EventInterface->OnSkipDirection(PressValue);
}

/// <summary>
/// Sボタンを離した処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::ReleaseS() {

	EventInterface->OnSkipDirection(ReleaseValue);
}

/// <summary>
/// マウスカーソルが移動したかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:移動した </returns>
bool ABattleController::IsMoveMouseCursor() {

	float CurPosX = NotMoveValue;
	float CurPosY = NotMoveValue;

	// 現在のマウスカーソルの位置を取得
	GetMousePosition(CurPosX, CurPosY);
	// 前回のマウスカーソルの位置との差分を算出
	float DifferX = OldCurPosX - CurPosX;
	float DifferY = OldCurPosY - CurPosY;

	OldCurPosX = CurPosX;
	OldCurPosY = CurPosY;

	if (FMath::Abs(DifferX) > NotMoveValue ||
		FMath::Abs(DifferX) > NotMoveValue) {

		return true;
	}

	return false;
}
