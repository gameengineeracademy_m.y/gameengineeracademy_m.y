﻿
#include "Battle/Character/OperatePlayer/OperatePlayer.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Battle/GameObjects/PickupItem/PickupItemAmmo/PickupItemAmmo.h"
#include "Battle/GameObjects/PickupItem/PickupItemHeal/PickupItemHeal.h"
#include "Battle/GameObjects/PickupItem/PickupItemWeapon/PickupItemWeapon.h"
#include "Battle/Controller/BattleController.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AOperatePlayer::AOperatePlayer()	
	: HeartBeatSound(nullptr)
	, CurrentDyingPhase(EPlayerDyingPhase::None)
	, EventInterface()
	, InputWheel(0.0f)
	, DyingHitPoint(0)
	, PlayerInputComp(nullptr)
	, AudioCompHeartBeat(nullptr) {

	// Tick処理の有効化
	PrimaryActorTick.bCanEverTick = false;
}

/// <summary>
/// プレイヤーインプットコンポーネント設定
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AOperatePlayer::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) {

	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComp = PlayerInputComponent;

	// 軸バインド
	PlayerInputComp->BindAxis("MoveForward", this, &ACharacterBase::MoveForward);
	PlayerInputComp->BindAxis("MoveRight", this, &ACharacterBase::MoveRight);
	PlayerInputComp->BindAxis("Turn", this, &ACharacterBase::Turn);
	PlayerInputComp->BindAxis("LookUp", this, &ACharacterBase::LookUp);
	PlayerInputComp->BindAxis("SwitchWeapon", this, &AOperatePlayer::OperateMouseWheel);

	// アクションバインド
	PlayerInputComp->BindAction("Shoot", IE_Pressed, this, &ACharacterBase::Shoot);
	PlayerInputComp->BindAction("Shoot", IE_Released, this, &ACharacterBase::StopShooting);
	PlayerInputComp->BindAction("Aim", IE_Pressed, this, &AOperatePlayer::Aim);
	PlayerInputComp->BindAction("Aim", IE_Released, this, &AOperatePlayer::StopAiming);
	PlayerInputComp->BindAction("Run", IE_Pressed, this, &ACharacterBase::Run);
	PlayerInputComp->BindAction("Run", IE_Released, this, &ACharacterBase::StopRunning);
	PlayerInputComp->BindAction("Jump", IE_Pressed, this, &ACharacterBase::TouchStartedJump);
	PlayerInputComp->BindAction("Jump", IE_Released, this, &ACharacterBase::TouchStoppedJump);
	PlayerInputComp->BindAction("Crouch", IE_Pressed, this, &ACharacterBase::CrouchAndStand);
	PlayerInputComp->BindAction("Reload", IE_Pressed, this, &ACharacterBase::Reload);
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AOperatePlayer::BeginPlay() {

	Super::BeginPlay();

	InputWheel = NoneWheelValue;
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AOperatePlayer::Tick(float DeltaTime) {

	//-----------------------------------------------
	// 処理負荷軽減のためTick処理は自身では実行せず、
	// PlayerManagerクラスから実行する
	// ※EnemyManagerに合わせ、この方法とする
	//-----------------------------------------------

	Super::Tick(DeltaTime);

	// HPの割合に応じて処理を行う
	switch (CharaState) {
	case ECharaStateType::Fine: {
		//---------------------------
		//元気な状態
		//---------------------------

		if (!IsDying()) {
			break;
		}

		SetCharaStateType(ECharaStateType::Dying);
		ChangeCurrentDyingPhase(EPlayerDyingPhase::Start);
		break;
	}
	case ECharaStateType::Dying: {
		//---------------------------
		//瀕死状態
		//---------------------------

		switch (CurrentDyingPhase) {
		case EPlayerDyingPhase::Start: {

			// 瀕死ウィジェットアニメーション開始
			BeDying();

			// 現在の瀕死フェーズを「処理中」に変更
			ChangeCurrentDyingPhase(EPlayerDyingPhase::Play);
			break;
		}
		case EPlayerDyingPhase::Play: {

			if (IsDying()) {
				break;
			}
			// 現在の瀕死フェーズを「終了」に変更
			ChangeCurrentDyingPhase(EPlayerDyingPhase::Finish);
			break;
		}
		case EPlayerDyingPhase::Finish: {

			// 瀕死ウィジェットアニメーション終了
			RecoverFromDying();

			// キャラクターの状態を「元気」に変更
			SetCharaStateType(ECharaStateType::Fine);
			// 現在の瀕死フェーズを「何もしない」に変更
			ChangeCurrentDyingPhase(EPlayerDyingPhase::None);
			break;
		}
		default: {

			break;
		}
		}

		break;
	}
	case ECharaStateType::Dead: {
		//---------------------------
		//死亡状態
		//---------------------------

		// 瀕死状態だった場合は瀕死アニメーションを停止しておく
		if (CurrentDyingPhase != EPlayerDyingPhase::Play &&
			CurrentDyingPhase != EPlayerDyingPhase::Finish) {
			break;
		}

		// 瀕死ウィジェットアニメーション終了
		RecoverFromDying();
		// 現在の瀕死フェーズを「何もしない」に変更
		ChangeCurrentDyingPhase(EPlayerDyingPhase::None);
		break;
	}
	default: {

		break;
	}
	}

	// 被弾時のウィジェット表示処理
	if (bRecieveDamage) {
		bRecieveDamage = false;
		DisplayShootedWidget();
	}

	// 弾薬を取得して、HUDに情報を渡す
	if (!IsValid(EventInterface.GetObject())) {
		return;
	}
	EventInterface->OnUpdateBattleHUDWeaponAmmos();
}

/// <summary>
/// イベントインターフェース
/// </summary>
/// <param name=""> イベントインターフェース </param>
/// <returns></returns>
void AOperatePlayer::SetEventInterface(const TScriptInterface<IBattleEvent>& Interface) {

	EventInterface = Interface;
}

/// <summary>
/// 所持武器リストを取得
/// </summary>
/// <param name=""></param>
/// <returns></returns>
TMap<EEquipPositionType, EWeaponType> AOperatePlayer::GetPossessWeaponList() {

	return PossessWeaponList;
}

/// <summary>
/// アイテム取得イベント
/// </summary>
/// <param name="Actor"> アクター </param>
/// <returns></returns>
void AOperatePlayer::OnObtainItem(AActor* Actor) {

	APickupItemBase* ItemBase = Cast<APickupItemBase>(Actor);
	if (!IsValid(ItemBase)) {
		return;
	}
	EPickupItemType ItemType = ItemBase->GetPickupItemType();
	switch (ItemType) {
	case EPickupItemType::Ammo: {

		// 全ての武器の弾薬を最大量取得する
		bool bGetAmmo = false;
		for (auto Weapon : WeaponList) {
			if (!IsValid(Weapon.Value)) {
				continue;
			}
			if (Weapon.Value->IsExistsMaxAmmos()) {
				continue;
			}
			Weapon.Value->SetupExceedMaxPossessAmmos();
			bGetAmmo = true;
		}

		// 弾薬が上限に達していた場合は処理終了
		if (!bGetAmmo) {
			break;
		}

		USoundCue* Sound = ItemBase->PickupSound;
		if (!IsValid(Sound)) {
			break;
		}
		PlaySound(Sound);

		break;
	}
	case EPickupItemType::Healing: {

		// 全回復し、回復サウンドとエフェクトを実行する
		SetHitPoint(DetailData.HP);

		USoundCue* Sound = ItemBase->PickupSound;
		if (IsValid(Sound)) {
			PlaySound(Sound);
		}

		UParticleSystem* Effect = ItemBase->PickupEffect;
		if (IsValid(Effect)) {
			PlayEffect(Effect);
		}

		break;
	}
	case EPickupItemType::Weapon: {

		// 武器をスポーンし、装備する
		// アイテムのパーティクルシステムを停止する
		APickupItemWeapon* ItemWeapon = Cast<APickupItemWeapon>(Actor);
		if (!IsValid(ItemWeapon)) {
			return;
		}
		// 対象アイテムをフェードアウトさせ、サウンドを鳴らす
		ItemWeapon->StartFadeOut();
		USoundCue* Sound = ItemBase->PickupSound;
		if (IsValid(Sound)) {
			PlaySound(Sound);
		}

		// 武器を生成し、プレイヤーに装備する
		EPickupWeaponType ItemWeaponType = ItemWeapon->GetPickupWeaponType();
		AWeaponBase* Weapon = nullptr;
		EWeaponType WeaponType = EWeaponType::None;
		switch (ItemWeaponType) {
		case EPickupWeaponType::AssaultRifle: {

			WeaponType = EWeaponType::AssaultRifle;
			break;
		}
		case EPickupWeaponType::SniperRifle: {

			WeaponType = EWeaponType::SniperRifle;
			break;
		}
		}

		// 指定武器を所持しているかどうか
		bool IsCheck = IsPossessWeapon(WeaponType);
		if (IsCheck) {
			break;
		}

		Weapon = EventInterface->OnCreateWeapon(WeaponType);
		if (!IsValid(Weapon)) {
			break;
		}

		Weapon->SetCharaOwner(this);
		Weapon->SetMaxLoadedAmmos();
		SetWeapon(WeaponType, Weapon);
		SetupPossessWeapon(WeaponType, EEquipPositionType::Back);
		AttachParantSocket(Weapon, EEquipPositionType::Back);

		// HUD情報の更新
		EventInterface->OnUpdateBattleHUDWeapon();
		break;
	}
	}
}

/// <summary>
/// 狙う
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AOperatePlayer::Aim() {

	bool IsCheck = IsGameStatePlay();
	if (!IsCheck) {
		return;
	}

	ACharacterBase::Aim();

	ABattleController* BattleController = Cast<ABattleController>(UGameplayStatics::GetPlayerController(this, 0));
	if (!IsValid(BattleController)) {
		return;
	}
	UCenterPointWidget* CenterPointWidget = BattleController->GetCenterPointWidget();
	if (!IsValid(CenterPointWidget)) {
		return;
	}
	CenterPointWidget->Open();
}

/// <summary>
/// 狙うのをやめる
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AOperatePlayer::StopAiming() {

	ACharacterBase::StopAiming();

	// クロスヘアの非表示
	ABattleController* BattleController = Cast<ABattleController>(UGameplayStatics::GetPlayerController(this, 0));
	if (!IsValid(BattleController)) {
		return;
	}
	UCenterPointWidget* CenterPointWidget = BattleController->GetCenterPointWidget();
	if (!IsValid(CenterPointWidget)) {
		return;
	}
	CenterPointWidget->Closed();
}

/// <summary>
/// マウスホイール操作
/// </summary>
/// <param name="Value"> 入力方向 </param>
/// <returns></returns>
void AOperatePlayer::OperateMouseWheel(float Value) {

	if (Value == NoneWheelValue) {
		InputWheel = NoneWheelValue;
		return;
	}

	InputWheel += Value;
	if (FMath::Abs(InputWheel) < InputWheelValue) {
		return;
	}
	InputWheel = NoneWheelValue;

	// 武器切り替え中、射撃処理中は実行不可
	if (bPressSwitchButton || bPressShootButton) {
		return;
	}

	// 切り替える武器が存在するかどうか
	bool IsCheck = IsPossessWeaponMultiple();
	if (!IsCheck) {
		return;
	}

	// リロード中なら処理を中止する
	if (bPressReloadButton) {
		CancelReload();
	}

	// 一旦カメラを直立時の状態にセットする
	SetupCamera(EPhisicalPosition::Stand);

	bPressSwitchButton = true;
	ExecuteAnimMontage(ECharaAnimType::SwitchWeapon);

	// HUDの武器の入れ替えアニメーションの実施
	EventInterface->OnStartSwitchWeaponAnimation();
}

/// <summary>
/// 武器を切り替える
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AOperatePlayer::SwitchWeapon() {

	AWeaponBase* EquipWeapon = GetEquipWeapon();
	if (!IsValid(EquipWeapon)) {
		return;
	}
	EWeaponType CurrentWeaponType = EquipWeapon->GetWeaponType();
	EWeaponType NextWeaponType = EWeaponType::None;
	switch (CurrentWeaponType) {
	case EWeaponType::AssaultRifle: {
		NextWeaponType = EWeaponType::SniperRifle;
		break;
	}
	case EWeaponType::SniperRifle: {
		NextWeaponType = EWeaponType::AssaultRifle;
		break;
	}
	}

	AWeaponBase* CurrentEquipWeapon = GetWeapon(CurrentWeaponType);
	AWeaponBase* NextEquipWeapon = GetWeapon(NextWeaponType);
	if (!IsValid(CurrentEquipWeapon) || !IsValid(NextEquipWeapon)) {
		return;
	}

	// 装備武器の再設定
	SetupPossessWeapon(NextWeaponType, EEquipPositionType::Hand);
	SetupPossessWeapon(CurrentWeaponType, EEquipPositionType::Back);
	// 実際の武器の入れ替え処理
	AttachParantSocket(NextEquipWeapon, EEquipPositionType::Hand);
	AttachParantSocket(CurrentEquipWeapon, EEquipPositionType::Back);

	// HUDの武器の入れ替え処理を行う
	EventInterface->OnSwitchWeapon();
}

/// <summary>
/// 武器を切り替え終了処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AOperatePlayer::CompletedSwitchWeapon() {

	bPressSwitchButton = false;

	// エイムしている場合はカメラをセットし直す
	if (!bPressAimButton) {
		return;
	}
	FaceForward();
	SetupCamera(EPhisicalPosition::Aim);
}

/// <summary>
/// 撃たれた時の処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AOperatePlayer::DisplayShootedWidget() {

	// キャラクターに対する角度から方角を取得
	EDirectionType DirectionType = SearchDirection(HitRotYaw);

	// ウィジェットの表示
	ABattleController* BattleController = Cast<ABattleController>(UGameplayStatics::GetPlayerController(this, 0));
	if (!IsValid(BattleController)) {
		return;
	}
	UShootedWidget* ShootedWidget = BattleController->GetShootedWidget();
	if (!IsValid(ShootedWidget)) {
		return;
	}
	ShootedWidget->PlayShootedAnimation(DirectionType);
}

/// <summary>
/// プレイヤーが瀕死かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:瀕死 </returns>
bool AOperatePlayer::IsDying() {

	return HitPoint < DyingHitPoint;
}

/// <summary>
/// 瀕死ウィジェットの表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AOperatePlayer::OpenDyingWidget() {

	ABattleController* BattleController = Cast<ABattleController>(UGameplayStatics::GetPlayerController(this, 0));
	if (!IsValid(BattleController)) {
		return;
	}
	BattleController->GetLowHealthWidget();
	ULowHealthWidget* LowHealthWidget = BattleController->GetLowHealthWidget();
	if (!IsValid(LowHealthWidget)) {
		return;
	}
	LowHealthWidget->PlayLowHealthAnimation();
}

/// <summary>
/// 瀕死ウィジェットの非表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AOperatePlayer::CloseDyingWidget() {

	ABattleController* BattleController = Cast<ABattleController>(UGameplayStatics::GetPlayerController(this, 0));
	if (!IsValid(BattleController)) {
		return;
	}
	ULowHealthWidget* LowHealthWidget = BattleController->GetLowHealthWidget();
	if (!IsValid(LowHealthWidget)) {
		return;
	}
	LowHealthWidget->FinishLowHealthAnimation();
}

/// <summary>
/// 瀕死ウィジェットの表示有無
/// </summary>
/// <param name="StateType"> 設定の種類 </param>
/// <returns></returns>
void AOperatePlayer::DisplayDyingWidget(EDyingDisplayStateType StateType) {

	ABattleController* BattleController = Cast<ABattleController>(UGameplayStatics::GetPlayerController(this, 0));
	if (!IsValid(BattleController)) {
		return;
	}
	ULowHealthWidget* LowHealthWidget = BattleController->GetLowHealthWidget();
	if (!IsValid(LowHealthWidget)) {
		return;
	}

	switch (StateType) {
	case EDyingDisplayStateType::Pause: {

		// 瀕死ウィジェットの非表示
		LowHealthWidget->SetVisibility(ESlateVisibility::Collapsed);
		break;
	}
	case EDyingDisplayStateType::Resume: {

		// 瀕死ウィジェットの表示
		LowHealthWidget->SetVisibility(ESlateVisibility::HitTestInvisible);
		break;
	}
	}
}

/// <summary>
/// 現在の瀕死フェーズの変更
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void AOperatePlayer::ChangeCurrentDyingPhase(EPlayerDyingPhase PhaseType) {

	CurrentDyingPhase = PhaseType;
}

/// <summary>
/// 現在の瀕死ウィジェットの表示状態の種類
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void AOperatePlayer::ChangeCurrentDyingDisplayStateType(EDyingDisplayStateType StateType) {

	CurrentDyingDisplayStateType = StateType;
}

/// <summary>
/// 各種ヒットポイントの境界値をセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AOperatePlayer::SetupHitPointLine() {

	// 状態遷移の境界値を算出
	// ※値は四捨五入する
	DyingHitPoint = FMath::RoundToInt(static_cast<float>(HitPoint) * DyingLineRate);
}

/// <summary>
/// エイムキャンセル
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AOperatePlayer::CancelAim() {

	StopAiming();
}

/// <summary>
/// 瀕死状態になる
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AOperatePlayer::BeDying() {

	// 瀕死ウィジェットの表示
	OpenDyingWidget();

	// 心音を鳴らす
	if (!IsValid(HeartBeatSound)) {
		return;
	}
	AudioCompHeartBeat = UGameplayStatics::SpawnSoundAttached(HeartBeatSound, GetMesh());
}

/// <summary>
/// 瀕死状態から回復する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AOperatePlayer::RecoverFromDying() {

	// 瀕死ウィジェットの非表示
	CloseDyingWidget();

	// 心音を鳴らすのをやめる
	if (!IsValid(AudioCompHeartBeat)) {
		return;
	}
	AudioCompHeartBeat->Stop();
	AudioCompHeartBeat->DestroyComponent();
}

/// <summary>
/// 瀕死状態の一時停止設定
/// </summary>
/// <param name="StateType"> 設定の種類 </param>
/// <returns></returns>
void AOperatePlayer::PauseDying(EDyingDisplayStateType StateType) {

	if (!IsDying()) {
		return;
	}

	// 瀕死ウィジェットの設定
	DisplayDyingWidget(StateType);

	switch (StateType) {
	case EDyingDisplayStateType::Pause: {

		// 心音の停止
		if (!IsValid(AudioCompHeartBeat)) {
			return;
		}
		AudioCompHeartBeat->SetPaused(true);

		break;
	}
	case EDyingDisplayStateType::Resume: {

		// 心音の再開
		if (!IsValid(AudioCompHeartBeat)) {
			return;
		}
		AudioCompHeartBeat->SetPaused(false);

		break;
	}
	default: {

		break;
	}
	}
}