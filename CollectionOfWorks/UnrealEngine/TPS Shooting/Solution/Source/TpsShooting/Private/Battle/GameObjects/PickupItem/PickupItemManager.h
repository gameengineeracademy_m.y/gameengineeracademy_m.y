﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Public/TpsShootingGameInstance.h"
#include "Battle/GameObjects/PickupItem/PickupItemAmmo/PickupItemAmmo.h"
#include "Battle/GameObjects/PickupItem/PickupItemHeal/PickupItemHeal.h"
#include "Battle/GameObjects/PickupItem/PickupItemWeapon/PickupItemWeapon.h"
#include "Engine/DataTable.h"
#include "Battle/Interface/BattleEvent.h"
#include "PickupItemManager.generated.h"


/// <summary>
/// アイテムのスポーン位置
/// </summary>
USTRUCT(BlueprintType)
struct FItemSpawnData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// バトルレベルの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "PickupItemManager")
	ELevelType BattleLevelType;

	/// <summary>
	/// エリアの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "PickupItemManager")
	EStageType StageType;

	/// <summary>
	/// アイテムの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "PickupItemManager")
	EPickupItemType PickupItemType;

	/// <summary>
	/// スポーン位置 X座標
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "PickupItemManager")
	float PositionX;

	/// <summary>
	/// スポーン位置 Y座標
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "PickupItemManager")
	float PositionY;

	/// <summary>
	/// スポーン位置 Z座標
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "PickupItemManager")
	float PositionZ;
};

/// <summary>
/// アイテムマネージャクラス
/// </summary>
UCLASS()
class APickupItemManager : public AActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	APickupItemManager();

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	/// <param name=""> イベントインターフェース </param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "PickupItemManager")
	void SetEventInterface(const TScriptInterface<IBattleEvent>& Interface);

	/// <summary>
	/// アイテムのスポーン
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SpawnPickupItem();

	/// <summary>
	/// アイテムリストのインデックスリスト取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> インデックスリスト </returns>
	TArray<int32> GetListIndexArray();

	/// <summary>
	/// リストからアイテムを取得
	/// </summary>
	/// <param name="Index"> インデックス </param>
	/// <returns> アイテム </returns>
	APickupItemBase* GetPickupItem(int32 Index);

	/// <summary>
	/// アイテムの状態をリセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetPickupItem();

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

private:

	/// <summary>
	/// クラスのロード
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void LoadPickupItemClass();

public:

	/// <summary>
	/// アイテムスポーンテーブル
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="PickupItemManager")
	UDataTable* ItemSpawnTable;

private:

	/// <summary>
	/// 弾薬アイテムクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<APickupItemAmmo> PickupItemAmmoClass;

	/// <summary>
	/// 回復アイテムクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<APickupItemHeal> PickupItemHealClass;

	/// <summary>
	/// 武器アイテムクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<APickupItemWeapon> PickupItemWeaponClass;

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IBattleEvent> EventInterface;

	/// <summary>
	/// アイテムリスト
	/// </summary>
	UPROPERTY()
	TMap<int32, APickupItemBase*> PickupItemList;

	/// <summary>
	/// インデックスリスト
	/// </summary>
	UPROPERTY()
	TArray<int32> ListIndexArray;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// 最小インデックス
	/// </summary>
	const int32 MinIndex = 0;
};
