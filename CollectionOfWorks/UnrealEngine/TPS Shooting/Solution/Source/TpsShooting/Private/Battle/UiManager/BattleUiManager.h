﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Battle/Widgets/LoadWidget.h"
#include "UiAssignmentWidget/GuideWidget/CameraGuideWidget/CameraGuideWidget.h"
#include "UiAssignmentWidget/GuideWidget/PauseDirectionGuideWidget/PauseDirectionGuideWidget.h"
#include "UiAssignmentWidget/GuideWidget/AudioGuideWidget/AudioGuideWidget.h"
#include "UiAssignmentWidget/GuideWidget/MapGuideWidget/MapGuideWidget.h"
#include "UiAssignmentWidget/GuideWidget/PauseGuideWidget/PauseGuideWidget.h"
#include "UiAssignmentWidget/GuideWidget/ResultGuideWidget/ResultGuideWidget.h"
#include "UiAssignmentWidget/GuideWidget/OperateListGuideWidget/OperateListGuideWidget.h"
#include "UiAssignmentWidget/FadeWidget/FadeWidget.h"
#include "Battle/Widgets/ResultWidget.h"
#include "Battle/Widgets/CrossHairWidget.h"
#include "Battle/Widgets/CenterPointWidget.h"
#include "Battle/Widgets/BattleWidget.h"
#include "Battle/Widgets/LowHealthWidget.h"
#include "Battle/Widgets/ShootedWidget.h"
#include "Battle/Widgets/FoundWidget.h"
#include "Battle/Widgets/PauseWidget.h"
#include "Battle/Widgets/Map/MiniMapWidget.h"
#include "Battle/Widgets/Map/AreaMapWidget.h"
#include "UiAssignmentWidget/OperateBaseWidget/OptionBaseWidget/PauseMenuWidget/PauseMenuWidget.h"
#include "UiAssignmentWidget/OperateBaseWidget/OptionBaseWidget/OperateListWidget/OperateListWidget.h"
#include "UiAssignmentWidget/OperateBaseWidget/OptionBaseWidget/AudioMenuWidget/AudioMenuWidget.h"
#include "BattleUiManager.generated.h"

/// <summary>
/// バトルUIマネージャ
/// </summary>
UCLASS()
class ABattleUiManager : public AHUD
{
	GENERATED_BODY()

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ABattleUiManager();

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PreInitializeComponents() override;

	/// <summary>
	/// ロード処理時のUI生成処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreateUiForLoading();

	/// <summary>
	/// ロード処理時のUI表示準備処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PrepareOpenUiForLoading();

	/// <summary>
	/// ロード処理時のUI表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OpenUiForLoading();

	/// <summary>
	/// ロード処理時のUI非表示準備処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PrepareCloseUiForLoading();

	/// <summary>
	/// ロード処理時のUI非表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CloseUiForLoading();

	/// <summary>
	/// UI生成処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreateUi();

	/// <summary>
	/// UI表示準備処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PrepareOpenUi();

	/// <summary>
	/// UI表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OpenUi();

	/// <summary>
	/// UI非表示準備処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PrepareCloseUi();

	/// <summary>
	/// UI非表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CloseUi();

	/// <summary>
	/// フェードウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> フェードウィジェット </returns>
	UFadeWidget* GetFadeWidget();

	/// <summary>
	/// 照準ウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 照準ウィジェット </returns>
	UCrossHairWidget* GetCrossHairWidget();

	/// <summary>
	/// 照準の中心点ウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 照準の中心点ウィジェット </returns>
	UCenterPointWidget* GetCenterPointWidget();

	/// <summary>
	/// ポーズメニューウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ポーズメニューウィジェット </returns>
	UPauseMenuWidget* GetPauseMenuWidget();

	/// <summary>
	/// オーディオメニューウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オーディオメニューウィジェット </returns>
	UAudioMenuWidget* GetAudioMenuWidget();

	/// <summary>
	/// リザルトウィジェットの取得
	/// </summary>
	/// <param name="WidgetType"> オプションの種類 </param>
	/// <returns> リザルトウィジェット </returns>
	UResultWidget* GetResultWidget(EWidgetType WidgetType);

	/// <summary>
	/// バトルウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> バトルウィジェット </returns>
	UBattleWidget* GetBattleWidget();

	/// <summary>
	/// ミニマップウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ミニマップウィジェット </returns>
	UMiniMapWidget* GetMiniMapWidget();

	/// <summary>
	/// エリアマップウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> エリアマップウィジェット </returns>
	UAreaMapWidget* GetAreaMapWidget();

	/// <summary>
	/// 瀕死ウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 瀕死ウィジェット </returns>
	ULowHealthWidget* GetLowHealthWidget();

	/// <summary>
	/// 被弾ウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 被弾ウィジェット </returns>
	UShootedWidget* GetShootedWidget();

	/// <summary>
	/// 発見ウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 発見ウィジェット </returns>
	UFoundWidget* GetFoundWidget();

	/// <summary>
	/// ポーズウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ポーズウィジェット </returns>
	UPauseWidget* GetPauseWidget();

	/// <summary>
	/// 操作一覧ウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 操作一覧ウィジェット </returns>
	UOperateListWidget* GetOperateListWidget();

	/// <summary>
	/// カメラ演出ガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> カメラ演出ガイドウィジェット </returns>
	UCameraGuideWidget* GetCameraGuideWidget();

	/// <summary>
	/// カメラ演出時のポーズガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> カメラ演出時のポーズガイドウィジェット </returns>
	UPauseDirectionGuideWidget* GetPauseDirectionGuideWidget();

	/// <summary>
	/// オーディオガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オーディオガイドウィジェット </returns>
	UAudioGuideWidget* GetAudioGuideWidget();

	/// <summary>
	/// マップガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> マップガイドウィジェット </returns>
	UMapGuideWidget* GetMapGuideWidget();

	/// <summary>
	/// ポーズガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ポーズガイドウィジェット </returns>
	UPauseGuideWidget* GetPauseGuideWidget();

	/// <summary>
	/// リザルトガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> リザルトガイドウィジェット </returns>
	UResultGuideWidget* GetResultGuideWidget();

	/// <summary>
	/// 操作一覧ガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 操作一覧ガイドウィジェット </returns>
	UOperateListGuideWidget* GetOperateListGuideWidget();

private:

	/// <summary>
	/// ロードウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<ULoadWidget> LoadWidgetClass;

	/// <summary>
	/// ロードウィジェット
	/// </summary>
	UPROPERTY()
	ULoadWidget* LoadWidget;

	/// <summary>
	/// フェードウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UFadeWidget> FadeWidgetClass;

	/// <summary>
	/// フェードウィジェット
	/// </summary>
	UPROPERTY()
	UFadeWidget* FadeWidget;

	/// <summary>
	/// 照準ウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UCrossHairWidget> CrossHairWidgetClass;

	/// <summary>
	/// 照準ウィジェット
	/// </summary>
	UPROPERTY()
	UCrossHairWidget* CrossHairWidget;

	/// <summary>
	/// 照準の中心点ウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UCenterPointWidget> CenterPointWidgetClass;

	/// <summary>
	/// 照準の中心点ウィジェット
	/// </summary>
	UPROPERTY()
	UCenterPointWidget* CenterPointWidget;

	/// <summary>
	/// ポーズメニューウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UPauseMenuWidget> PauseMenuWidgetClass;

	/// <summary>
	/// ポーズメニューウィジェット
	/// </summary>
	UPROPERTY()
	UPauseMenuWidget* PauseMenuWidget;

	/// <summary>
	/// オーディオメニューウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UAudioMenuWidget> AudioMenuWidgetClass;

	/// <summary>
	/// オーディオメニューウィジェット
	/// </summary>
	UPROPERTY()
	UAudioMenuWidget* AudioMenuWidget;

	/// <summary>
	/// 操作一覧ウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UOperateListWidget> OperateListWidgetClass;

	/// <summary>
	/// 操作一覧ウィジェット
	/// </summary>
	UPROPERTY()
	UOperateListWidget* OperateListWidget;

	/// <summary>
	/// リザルトウィジェット配列
	/// </summary>
	UPROPERTY()
	TMap<EWidgetType, UResultWidget*> ResultWidgetList;

	/// <summary>
	/// バトルHUDウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UBattleWidget> BattleWidgetClass;

	/// <summary>
	/// バトルHUDウィジェット
	/// </summary>
	UPROPERTY()
	UBattleWidget* BattleWidget;

	/// <summary>
	/// ミニマップウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UMiniMapWidget> MiniMapWidgetClass;

	/// <summary>
	/// ミニマップウィジェット
	/// </summary>
	UPROPERTY()
	UMiniMapWidget* MiniMapWidget;

	/// <summary>
	/// 瀕死ウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<ULowHealthWidget> LowHealthWidgetClass;

	/// <summary>
	/// 瀕死ウィジェット
	/// </summary>
	UPROPERTY()
	ULowHealthWidget* LowHealthWidget;

	/// <summary>
	/// 被弾ウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UShootedWidget> ShootedWidgetClass;

	/// <summary>
	/// 被弾ウィジェット
	/// </summary>
	UPROPERTY()
	UShootedWidget* ShootedWidget;

	/// <summary>
	/// 発見ウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UFoundWidget> FoundWidgetClass;

	/// <summary>
	/// 発見ウィジェット
	/// </summary>
	UPROPERTY()
	UFoundWidget* FoundWidget;

	/// <summary>
	/// エリアマップウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UAreaMapWidget> AreaMapWidgetClass;

	/// <summary>
	/// エリアマップウィジェット
	/// </summary>
	UPROPERTY()
	UAreaMapWidget* AreaMapWidget;

	/// <summary>
	/// ポーズウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UPauseWidget> PauseWidgetClass;

	/// <summary>
	/// ポーズウィジェット
	/// </summary>
	UPROPERTY()
	UPauseWidget* PauseWidget;

	/// <summary>
	/// カメラ演出ガイドウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UCameraGuideWidget> CameraGuideWidgetClass;

	/// <summary>
	/// カメラ演出ガイドウィジェット
	/// </summary>
	UPROPERTY()
	UCameraGuideWidget* CameraGuideWidget;

	/// <summary>
	/// カメラ演出時のポーズガイドウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UPauseDirectionGuideWidget> PauseDirectionGuideWidgetClass;

	/// <summary>
	/// カメラ演出時のポーズガイドウィジェット
	/// </summary>
	UPROPERTY()
	UPauseDirectionGuideWidget* PauseDirectionGuideWidget;

	/// <summary>
	/// オーディオガイドウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UAudioGuideWidget> AudioGuideWidgetClass;

	/// <summary>
	/// オーディオガイドウィジェット
	/// </summary>
	UPROPERTY()
	UAudioGuideWidget* AudioGuideWidget;

	/// <summary>
	/// マップガイドウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UMapGuideWidget> MapGuideWidgetClass;

	/// <summary>
	/// マップガイドウィジェット
	/// </summary>
	UPROPERTY()
	UMapGuideWidget* MapGuideWidget;

	/// <summary>
	/// ポーズガイドウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UPauseGuideWidget> PauseGuideWidgetClass;

	/// <summary>
	/// ポーズガイドウィジェット
	/// </summary>
	UPROPERTY()
	UPauseGuideWidget* PauseGuideWidget;

	/// <summary>
	/// リザルトガイドウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UResultGuideWidget> ResultGuideWidgetClass;

	/// <summary>
	/// リザルトガイドウィジェット
	/// </summary>
	UPROPERTY()
	UResultGuideWidget* ResultGuideWidget;

	/// <summary>
	/// 操作一覧ガイドウィジェットクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<UOperateListGuideWidget> OperateListGuideWidgetClass;

	/// <summary>
	/// 操作一覧ガイドウィジェット
	/// </summary>
	UPROPERTY()
	UOperateListGuideWidget* OperateListGuideWidget;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// 不透明
	/// </summary>
	const float Opacity = 1.0f;

	/// <summary>
	/// 透明
	/// </summary>
	const float Transparent = 0.0f;
};
