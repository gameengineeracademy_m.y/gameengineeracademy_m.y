﻿
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Battle/Character/OperatePlayer/OperatePlayer.h"
#include "Battle/DataAssets/CharacterDetail/CharacterDetail.h"
#include "Battle/Weapons/WeaponFactory.h"
#include "Engine/DataTable.h"
#include "PlayerManager.generated.h"

/// <summary>
/// 操作キャラクターマネージャクラス
/// </summary>
UCLASS()
class APlayerManager : public AActor
{
	GENERATED_BODY()

public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	APlayerManager();

	/// <summary>
	/// 操作プレイヤーの生成
	/// </summary>
	/// <param name=""></param>
	/// <returns> 操作プレイヤー </returns>
	AOperatePlayer* CreatePlayer();

	/// <summary>
	/// 操作プレイヤーの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 操作プレイヤー </returns>
	AOperatePlayer* GetOperatePlayer();

	/// <summary>
	/// ウェポンファクトリーのセット
	/// </summary>
	/// <param name="Factory"> ウェポンファクトリー </param>
	/// <returns></returns>
	void SetWeaponFactory(AWeaponFactory* Factory);

	/// <summary>
	/// ウェポンファクトリーの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ウェポンファクトリー </returns>
	AWeaponFactory* GetWeaponFactory();

	/// <summary>
	/// 操作プレイヤーの破棄
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void DisposePlayer();

	/// <summary>
	/// 装備武器の保存
	/// </summary>
	/// <param name="PossessWeaponList"> 装備武器リスト </param>
	/// <returns></returns>
	void SaveEquipWeapon(TMap<EEquipPositionType, EWeaponType> PossessWeaponList);

	/// <summary>
	/// 保存された武器情報の取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 装備武器リスト </returns>
	TMap<EEquipPositionType, EWeaponType> GetSaveEquipWeapon();

	/// <summary>
	/// 指定の武器がセーブされているかどうか
	/// </summary>
	/// <param name="WeaponType"> 武器の種類 </param>
	/// <returns> true:セーブ済み </returns>
	bool IsSavePossessWeapon(EWeaponType WeaponType);


protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

private:

	/// <summary>
	/// 操作プレイヤーのスポーン
	/// </summary>
	/// <param name=""></param>
	/// <returns> スポーンした操作プレイヤー </returns>
	AOperatePlayer* SpawnPlayer();

	/// <summary>
	/// スポーン情報の初期設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeSpawnData();

	/// <summary>
	/// 装備する武器の初期設定
	/// </summary>
	/// <param name="RowNumber"> 行番号 </param>
	/// <returns></returns>
	void InitializeCharaEquipWeapon(FString RowNumber);

private:

	/// <summary>
	/// 操作プレイヤークラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<AOperatePlayer> OperatePlayerClass;

	/// <summary>
	/// 操作プレイヤー
	/// </summary>
	UPROPERTY()
	AOperatePlayer* OperatePlayer;

	/// <summary>
	/// プレイヤースポーン情報テーブル
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "PlayerManager")
	UDataTable* PlayerSpawnTable;

	/// <summary>
	/// プレイヤー初期装備テーブル
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "PlayerManager")
	UDataTable* PlayerInitWeaponTable;

	/// <summary>
	/// プレイヤーディテール
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "PlayerManager")
	UCharacterDetail* PlayerDetail;

	/// <summary>
	/// ウェポンファクトリー
	/// </summary>
	UPROPERTY()
	AWeaponFactory* WeaponFactory;

	/// <summary>
	/// スポーン情報 受け取り用
	/// </summary>
	TMap<EStageType, FSpawnData> SpawnData;

	/// <summary>
	/// 装備武器
	/// </summary>
	TMap<EEquipPositionType, EWeaponType> EquipWeaponList;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// ゼロ
	/// </summary>
	const int32 Zero = 0;
};