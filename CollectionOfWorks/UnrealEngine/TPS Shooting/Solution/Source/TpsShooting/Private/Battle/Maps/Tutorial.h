﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "Battle/Controller/BattleController.h"
#include "Battle/GameState/BattleGameState.h"
#include "Battle/Trigger/TriggerBox/CameraDirectionTriggerBox.h"
#include "Battle/CharacterManager/PlayerManager/PlayerManager.h"
#include "Battle/CharacterManager/EnemyManager/EnemyManager.h"
#include "Battle/GameObjects/PickupItem/PickupItemManager.h"
#include "Battle/Interface/BattleEvent.h"
#include "Tutorial.generated.h"

/// <summary>
/// レベル実行フェーズの種類
/// </summary>
UENUM()
enum class ETutorialPhaseType {
	Initialize,        // 初期化処理フェーズ
	FadeIn,            // フェードインフェーズ
	WaitFadeIn,        // フェードイン待機フェーズ
	PreparePlay,       // プレイ前準備フェーズ
	Play,              // プレイ中フェーズ
	CameraDirection,   // カメラ演出フェーズ
	Retry,             // リトライフェーズ
	GameOver,          // ゲームオーバーフェーズ
	GameClear,         // ゲームクリアフェーズ
	Map,               // マップ表示フェーズ
	Pause,             // ポーズフェーズ
	OperateList,       // 操作一覧表示フェーズ
	OptionOperation,   // オプション操作フェーズ
	FadeOut,           // フェードアウトフェーズ
	ChangeLevel,       // レベル切替フェーズ
	Finish,            // 終了処理フェーズ
	MaxTypeIndex
};

/// <summary>
/// ポーズの種類
/// </summary>
UENUM()
enum class ETutorialPauseType {
	None,              // 設定なし
	Play,              // プレイポーズフェーズ
	CameraDirection,   // カメラ演出フェーズ
	MaxTypeIndex
};

/// <summary>
/// リザルトフェーズの種類
/// </summary>
UENUM()
enum class ETutorialResultPhaseType {
	Wait,            // 待機フェーズ
	DisplayWidget,   // ウィジェット表示フェーズ
	WaitAnim,        // アニメーション待機フェーズ
	Operate,         // ボタン操作フェーズ
	Finish,          // 終了フェーズ
	MaxTypeIndex
};

/// <summary>
/// フェーズ切替の種類
/// </summary>
UENUM()
enum class ETutorialSwitchPhaseType {
	None,            // なにもしないフェーズ
	Retry,           // リトライフェーズ
	ChangeLevel,     // レベル切替フェーズ
	Finish,          // 終了フェーズ
	MaxTypeIndex
};

/// <summary>
/// ゲームフェーズの種類
/// </summary>
UENUM()
enum class EGamePhaseType {
	None,          // なにもしない
	Start,         // 開始フェーズ
	Reset,         // リセットフェーズ
	Finish,        // 終了フェーズ
	MaxTypeIndex
};

/// <summary>
/// チュートリアルレベル
/// </summary>
UCLASS()
class ATutorial : public ALevelScriptActor, public IBattleEvent
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ATutorial();

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

	/// <summary>
	/// カメラ演出実行開始処理
	/// </summary>
	/// <param name="TriggerBox"> トリガーボックス </param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "Tutorial")
	void StartCameraDirection(ACameraDirectionTriggerBox* TriggerBox);

	/// <summary>
	/// 演出用のカメラ切替処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Tutorial")
	void SwitchCameraDirection();

	/// <summary>
	/// エネミーの準備処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "Tutorial")
	void PrepareEnemies();

	/// <summary>
	/// リザルト画像をセットする
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "Tutorial")
	void SetupResultTexture(UTexture2DDynamic* ResultTexture);

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

private:

	/// <summary>
	/// 警戒ランプマネージャの初期設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeWarningLampManager();

	/// <summary>
	/// ウェポンファクトリーの初期設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeWeaponFactory();

	/// <summary>
	/// プレイヤーマネージャの初期設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializePlayerManager();

	/// <summary>
	/// エネミーマネージャの初期設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeEnemyManager();

	/// <summary>
	/// アイテムマネージャの初期設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializePickupItemManager();

	/// <summary>
	/// ターゲットオブジェクトマネージャの初期設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeTargetObjectManager();

	/// <summary>
	/// サウンドの生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreateSound();

	/// <summary>
	/// プレイヤーの生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreatePlayer();

	/// <summary>
	/// エネミーの生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreateEnemies();

	/// <summary>
	/// アイテムの生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreatePickupItem();

	/// <summary>
	/// ターゲットオブジェクトの生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreateTargetObject();

	/// <summary>
	/// バトルHUD情報の設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupBattleHUDWeapon();

	/// <summary>
	/// ミニマップウィジェットの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupMiniMap();

	/// <summary>
	/// エリアマップウィジェットの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupAreaMap();

	/// <summary>
	/// 現在のポーズの種類を変更する
	/// </summary>
	/// <param name="PauseType"> ポーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentPauseType(ETutorialPauseType PauseType);

	/// <summary>
	/// 現在のフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentPhase(ETutorialPhaseType PhaseType);

	/// <summary>
	/// 現在のゲームフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentGamePhase(EGamePhaseType PhaseType);

	/// <summary>
	/// 現在のリザルトフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentResultPhase(ETutorialResultPhaseType PhaseType);

	/// <summary>
	/// フェーズ切替の種類を変更する
	/// </summary>
	/// <param name="Type"> フェーズ切替の種類 </param>
	/// <returns></returns>
	void ChangeSwitchPhaseType(ETutorialSwitchPhaseType Type);

	/// <summary>
	/// 現在のゲームフェーズが開始かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:開始 </returns>
	bool IsStart();

	/// <summary>
	/// 現在のフェーズがプレイ中かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:プレイ中 </returns>
	bool IsPlay();

	/// <summary>
	/// 現在のフェーズがポーズ中かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:ポーズ中 </returns>
	bool IsPause();

	/// <summary>
	/// 現在のフェーズがマップ表示中かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:マップ表示中 </returns>
	bool IsMap();

	/// <summary>
	/// 現在のフェーズがカメラ演出中かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:カメラ演出中 </returns>
	bool IsCameraDirection();

	/// <summary>
	/// プレイヤーにコントローラを紐づける
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:処理完了 </returns>
	bool PossessPlayer();

	/// <summary>
	/// コントローラから所有物を切り離す
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void UnPossessOwner();

	/// <summary>
	/// ゲーム中のHUDの表示設定
	/// </summary>
	/// <param name="IsDisplay"> 表示有無 </param>
	/// <returns></returns>
	void SetupDisplayBattleHUD(bool IsDisplay);

	/// <summary>
	/// 敵に発見されたウィジェットをリセットする
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetFoundMarker();

	/// <summary>
	/// プレイヤー操作の設定
	/// </summary>
	/// <param name="bIsOperate"> 有効化有無 </param>
	/// <returns></returns>
	void SetupOperatePlayer(bool bIsOperate);

	/// <summary>
	/// ゲーム状況確認処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CheckGameState();

	/// <summary>
	/// ポーズメニュー操作
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OperatePauseMenu();

	/// <summary>
	/// オプションメニュー操作
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:操作終了 </returns>
	bool OperateOptionMenu();

	/// <summary>
	/// 操作一覧操作
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:操作終了 </returns>
	bool OperateOperateList();

	/// <summary>
	/// ゲームオーバー演出処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns> true:処理終了 </returns>
	bool PerformGameOver(float DeltaTime);

	/// <summary>
	/// ゲームクリア演出処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns> true:処理終了 </returns>
	bool PerformGameClear(float DeltaTime);

	/// <summary>
	/// ゲームクリア操作
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:操作終了 </returns>
	bool OperateGameClear();

	/// <summary>
	/// プレイヤーを消滅させる
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:処理終了 </returns>
	bool DestroyPlayer();

	/// <summary>
	/// プレイヤーのリスポーン
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void RespawnPlayer();

	/// <summary>
	/// エネミーのリスポーン
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void RespawnEnemies();

	/// <summary>
	/// エネミーの表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void AppearanceEnemies();

	/// <summary>
	/// リザルトウィジェットの初期化
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeResult();

	/// <summary>
	/// バトルHUDウィジェットの初期化
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeBattleHUD();

	/// <summary>
	/// アイテムのリセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetPickupItem();

	/// <summary>
	/// プレイヤーのポーズ処理
	/// </summary>
	/// <param name="StateType"> 設定の種類 </param>
	/// <returns></returns>
	void PausePlayer(EDyingDisplayStateType StateType);

	/// <summary>
	/// プレイヤーのリセット処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetPlayer();

	/// <summary>
	/// フェードウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> フェードウィジェット </returns>
	UFadeWidget* GetFadeWidget();

	/// <summary>
	/// 照準ウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 照準ウィジェット </returns>
	UCrossHairWidget* GetCrossHairWidget();

	/// <summary>
	/// 照準の中心点ウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 照準の中心点ウィジェット </returns>
	UCenterPointWidget* GetCenterPointWidget();

	/// <summary>
	/// ポーズメニューウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ポーズメニューウィジェット </returns>
	UPauseMenuWidget* GetPauseMenuWidget();

	/// <summary>
	/// オーディオメニューウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オーディオメニューウィジェット </returns>
	UAudioMenuWidget* GetAudioMenuWidget();

	/// <summary>
	/// リザルトウィジェットの取得
	/// </summary>
	/// <param name="WidgetType"> ウィジェットの種類 </param>
	/// <returns></returns>
	UResultWidget* GetResultWidget(EWidgetType WidgetType);

	/// <summary>
	/// バトルHUDウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> バトルHUDウィジェット </returns>
	UBattleWidget* GetBattleWidget();

	/// <summary>
	/// ミニマップウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ミニマップウィジェット </returns>
	UMiniMapWidget* GetMiniMapWidget();

	/// <summary>
	/// エリアマップウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ミニマップウィジェット </returns>
	UAreaMapWidget* GetAreaMapWidget();

	/// <summary>
	/// ポーズウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ポーズウィジェット </returns>
	UPauseWidget* GetPauseWidget();

	/// <summary>
	/// 操作一覧ウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 操作一覧ウィジェット </returns>
	UOperateListWidget* GetOperateListWidget();

	/// <summary>
	/// カメラ演出ガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> カメラ演出ガイドウィジェット </returns>
	UCameraGuideWidget* GetCameraGuideWidget();

	/// <summary>
	/// カメラ演出時のポーズガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> カメラ演出時のポーズガイドウィジェット </returns>
	UPauseDirectionGuideWidget* GetPauseDirectionGuideWidget();

	/// <summary>
	/// オーディオガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オーディオガイドウィジェット </returns>
	UAudioGuideWidget* GetAudioGuideWidget();

	/// <summary>
	/// マップガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> マップガイドウィジェット </returns>
	UMapGuideWidget* GetMapGuideWidget();

	/// <summary>
	/// ポーズガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ポーズガイドウィジェット </returns>
	UPauseGuideWidget* GetPauseGuideWidget();

	/// <summary>
	/// リザルトガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> リザルトガイドウィジェット </returns>
	UResultGuideWidget* GetResultGuideWidget();

	/// <summary>
	/// 操作一覧ガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 操作一覧ガイドウィジェット </returns>
	UOperateListGuideWidget* GetOperateListGuideWidget();

	/// <summary>
	/// オーディオマネージャの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オーディオマネージャ </returns>
	AAudioManager* GetAudioManager();

	/// <summary>
	/// プレイヤーマネージャの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> プレイヤーマネージャ </returns>
	APlayerManager* GetPlayerManager();

	/// <summary>
	/// ポーズ画面の表示・非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OnSwitchPause() override;

	/// <summary>
	/// マップ画面の表示・非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OnSwitchMap() override;

	/// <summary>
	/// 敵の視覚情報ON・OFF
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OnSetupSight() override;

	/// <summary>
	/// 武器の生成処理
	/// </summary>
	/// <param name="WeaponType"> 武器の種類 </param>
	/// <returns> 武器 </returns>
	AWeaponBase* OnCreateWeapon(EWeaponType WeaponType) override;

	/// <summary>
	/// セーブデータに指定の武器があるかどうか
	/// </summary>
	/// <param name="WeaponType"> 武器の種類 </param>
	/// <returns> true:ある </returns>
	bool OnIsSavePossessWeapon(EWeaponType WeaponType) override;

	/// <summary>
	/// HUDの武器入れ替えアニメーション開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OnStartSwitchWeaponAnimation() override;

	/// <summary>
	/// HUDの武器入れ替え処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OnSwitchWeapon() override;

	/// <summary>
	/// バトルHUD情報の武器の更新
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OnUpdateBattleHUDWeapon() override;

	/// <summary>
	/// バトルHUD情報の弾薬数の更新
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OnUpdateBattleHUDWeaponAmmos() override;

	/// <summary>
	/// 太陽と雲の設定を変更する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OnChangeSettingSunAndCloud() override;

	/// <summary>
	/// カメラ演出時のカメラ切り替え
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OnSwitchDirectionCamera() override;

	/// <summary>
	/// カメラ演出実行終了処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OnFinishCameraDirection() override;

	/// <summary>
	/// 演出スキップ処理
	/// </summary>
	/// <param name="Value"> 押したかどうか </param>
	/// <returns></returns>
	void OnSkipDirection(float Value) override;

	/// <summary>
	/// 演出用のカメラ切替処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void SwitchCameraDirection_Implementation();

protected:

	/// <summary>
	/// 実行中のカメラ演出トリガー
	/// </summary>
	UPROPERTY(BlueprintReadOnly, Category = "Tutorial")
	ACameraDirectionTriggerBox* ExecCamraraDirectionTrigger;

private:

	/// <summary>
	/// バトルコントローラ
	/// </summary>
	UPROPERTY()
	ABattleController* BattleController;

	/// <summary>
	/// バトルゲームステート
	/// </summary>
	UPROPERTY()
	ABattleGameState* BattleGameState;

	/// <summary>
	/// プレイヤーマネージャクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<APlayerManager> PlayerManagerClass;

	/// <summary>
	/// プレイヤーマネージャ
	/// </summary>
	UPROPERTY()
	APlayerManager* PlayerManager;

	/// <summary>
	/// エネミーマネージャクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<AEnemyManager> EnemyManagerClass;

	/// <summary>
	/// エネミーマネージャ
	/// </summary>
	UPROPERTY()
	AEnemyManager* EnemyManager;

	/// <summary>
	/// ウェポンファクトリークラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<AWeaponFactory> WeaponFactoryClass;

	/// <summary>
	/// ウェポンファクトリー
	/// </summary>
	UPROPERTY()
	AWeaponFactory* WeaponFactory;

	/// <summary>
	/// アイテムマネージャクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<APickupItemManager> PickupItemManagerClass;

	/// <summary>
	/// アイテムマネージャ
	/// </summary>
	UPROPERTY()
	APickupItemManager* PickupItemManager;

	/// <summary>
	/// 警告ランプマネージャクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<AWarningLampManager> WarningLampManagerClass;

	/// <summary>
	/// 警告ランプマネージャ
	/// </summary>
	UPROPERTY()
	AWarningLampManager* WarningLampManager;

	/// <summary>
	/// ターゲットオブジェクトマネージャクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<ATargetObjectManager> TargetObjectManagerClass;

	/// <summary>
	/// ターゲットオブジェクトマネージャ
	/// </summary>
	UPROPERTY()
	ATargetObjectManager* TargetObjectManager;

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	ETutorialPhaseType CurrentPhase;

	/// <summary>
	/// 現在のゲームフェーズ
	/// </summary>
	EGamePhaseType CurrentGamePhase;

	/// <summary>
	/// 現在のリザルトフェーズ
	/// </summary>
	ETutorialResultPhaseType CurrentResultPhase;

	/// <summary>
	/// フェーズ切替の種類
	/// </summary>
	ETutorialSwitchPhaseType SwitchPhase;

	/// <summary>
	/// 現在のポーズの種類
	/// </summary>
	ETutorialPauseType CurrentPauseType;

	/// <summary>
	/// 累積時間
	/// </summary>
	float AccumulateTime;

	/// <summary>
	/// 押下量
	/// </summary>
	float KeepPressValue;

	/// <summary>
	/// カメラ演出を実行するか
	/// </summary>
	bool bIsCameraDirection;

	/// <summary>
	/// クリア状況
	/// </summary>
	bool bGameClear;

private:
	//--------------------------------
	//以下、定数宣言
	//--------------------------------
	/// <summary>
	/// 時間リセット
	/// </summary>
	const float ResetTime = 0.0f;

	/// <summary>
	/// 待機時間
	/// </summary>
	const float ResultWaitTime = 0.5f;

	/// <summary>
	/// 押下量リセット
	/// </summary>
	const float ResetKeepPressValue = 0.0f;

	/// <summary>
	/// スキップに必要な押下量
	/// </summary>
	const float NeedKeepPressValue = 45.0f;
};