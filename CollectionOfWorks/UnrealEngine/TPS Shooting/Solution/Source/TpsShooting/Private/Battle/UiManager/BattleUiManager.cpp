﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/UiManager/BattleUiManager.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ABattleUiManager::ABattleUiManager()
	: LoadWidgetClass()
	, LoadWidget(nullptr)
	, FadeWidgetClass()
	, FadeWidget(nullptr)
	, CrossHairWidgetClass() 
	, CrossHairWidget(nullptr)
	, CenterPointWidgetClass()
	, CenterPointWidget(nullptr)
	, PauseMenuWidgetClass()
	, PauseMenuWidget(nullptr)
	, AudioMenuWidgetClass()
	, AudioMenuWidget(nullptr)
	, OperateListWidgetClass()
	, OperateListWidget(nullptr)
	, ResultWidgetList()
	, BattleWidgetClass()
	, BattleWidget(nullptr)
	, MiniMapWidgetClass()
	, MiniMapWidget(nullptr)
	, LowHealthWidgetClass() 
	, LowHealthWidget(nullptr)
	, ShootedWidgetClass() 
	, ShootedWidget(nullptr)
	, FoundWidgetClass() 
	, FoundWidget(nullptr)
	, AreaMapWidgetClass()
	, AreaMapWidget(nullptr)
	, PauseWidgetClass()
	, PauseWidget(nullptr)
	, CameraGuideWidgetClass()
	, CameraGuideWidget(nullptr)
	, PauseDirectionGuideWidgetClass()
	, PauseDirectionGuideWidget(nullptr)
	, AudioGuideWidgetClass()
	, AudioGuideWidget(nullptr)
	, MapGuideWidgetClass()
	, MapGuideWidget(nullptr)
	, PauseGuideWidgetClass()
	, PauseGuideWidget(nullptr)
	, ResultGuideWidgetClass()
	, ResultGuideWidget()
	, OperateListGuideWidgetClass()
	, OperateListGuideWidget(nullptr) {

	// Tickイベントの無効化
	PrimaryActorTick.bCanEverTick = false;
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleUiManager::PreInitializeComponents() {

}

/// <summary>
/// ロード処理時のUI生成処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleUiManager::CreateUiForLoading() {

	//-----------------------------------------------------
	// ウィジェットパスを「DefaultGame.ini」から取得し、
	// ウィジェットを生成する
	//-----------------------------------------------------
	// ロードウィジェット
	FString LoadWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("LoadWidgetPath"), LoadWidgetPath, GGameIni);

	LoadWidgetClass = TSoftClassPtr<ULoadWidget>(FSoftObjectPath(*LoadWidgetPath)).LoadSynchronous();
	if (!LoadWidgetClass) {
		return;
	}
	LoadWidget = CreateWidget<ULoadWidget>(GetWorld(), LoadWidgetClass);
}

/// <summary>
/// ロード処理時の表示準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleUiManager::PrepareOpenUiForLoading() {

	// ロードウィジェット
	if (!IsValid(LoadWidget)) {
		return;
	}
	LoadWidget->ResetNowDisplay();
}

/// <summary>
/// ロード処理時の表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleUiManager::OpenUiForLoading() {

	//-----------------------------------------------------
	// 各ウィジェットを表示
	//-----------------------------------------------------
	// ロードウィジェット
	if (!IsValid(LoadWidget)) {
		return;
	}
	LoadWidget->Open();
}

/// <summary>
/// ロード処理時のUI非表示準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleUiManager::PrepareCloseUiForLoading() {

	//-----------------------------------------------------
	// 各ウィジェットを非表示前準備処理
	//-----------------------------------------------------
	// ロードウィジェット
	if (!IsValid(LoadWidget)) {
		return;
	}
	LoadWidget->Close();
}

/// <summary>
/// ロード処理時のUI非表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleUiManager::CloseUiForLoading() {

	//-----------------------------------------------------
	// 各ウィジェットを非表示
	//-----------------------------------------------------
	// ロードウィジェット
	if (!IsValid(LoadWidget)) {
		return;
	}
	// ロードウィジェット
	LoadWidget->Closed();
}

/// <summary>
/// UI生成処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleUiManager::CreateUi() {

	//-----------------------------------------------------
	// ウィジェットパスを「DefaultGame.ini」から取得し、
	// ウィジェットを生成する
	//-----------------------------------------------------
	// フェードウィジェット
	FString FadeWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("FadeWidgetPath"), FadeWidgetPath, GGameIni);
	FadeWidgetClass = TSoftClassPtr<UFadeWidget>(FSoftObjectPath(*FadeWidgetPath)).LoadSynchronous();
	if (FadeWidgetClass) {
		FadeWidget = CreateWidget<UFadeWidget>(GetWorld(), FadeWidgetClass);
	}

	// 瀕死ウィジェット
	FString LowHealthWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("LowHealthWidgetPath"), LowHealthWidgetPath, GGameIni);
	LowHealthWidgetClass = TSoftClassPtr<ULowHealthWidget>(FSoftObjectPath(*LowHealthWidgetPath)).LoadSynchronous();
	if (LowHealthWidgetClass) {
		LowHealthWidget = CreateWidget<ULowHealthWidget>(GetWorld(), LowHealthWidgetClass);
	}

	// バトルHUDウィジェット
	FString BattleWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("BattleWidgetPath"), BattleWidgetPath, GGameIni);
	BattleWidgetClass = TSoftClassPtr<UBattleWidget>(FSoftObjectPath(*BattleWidgetPath)).LoadSynchronous();
	if (BattleWidgetClass) {
		BattleWidget = CreateWidget<UBattleWidget>(GetWorld(), BattleWidgetClass);
	}

	// ミニマップウィジェット
	FString MapWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("MiniMapWidgetPath"), MapWidgetPath, GGameIni);
	MiniMapWidgetClass = TSoftClassPtr<UMiniMapWidget>(FSoftObjectPath(*MapWidgetPath)).LoadSynchronous();
	if (MiniMapWidgetClass) {
		MiniMapWidget = CreateWidget<UMiniMapWidget>(GetWorld(), MiniMapWidgetClass);
	}

	// 照準ウィジェット
	FString CrossHairWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("CrossHairWidgetPath"), CrossHairWidgetPath, GGameIni);
	CrossHairWidgetClass = TSoftClassPtr<UCrossHairWidget>(FSoftObjectPath(*CrossHairWidgetPath)).LoadSynchronous();
	if (CrossHairWidgetClass) {
		CrossHairWidget = CreateWidget<UCrossHairWidget>(GetWorld(), CrossHairWidgetClass);
	}
	
	// 照準の中心点ウィジェット
	FString CenterPointWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("CenterPointWidgetPath"), CenterPointWidgetPath, GGameIni);
	CenterPointWidgetClass = TSoftClassPtr<UCenterPointWidget>(FSoftObjectPath(*CenterPointWidgetPath)).LoadSynchronous();
	if (CenterPointWidgetClass) {
		CenterPointWidget = CreateWidget<UCenterPointWidget>(GetWorld(), CenterPointWidgetClass);
	}

	// 発見ウィジェット
	FString FoundWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("FoundWidgetPath"), FoundWidgetPath, GGameIni);
	FoundWidgetClass = TSoftClassPtr<UFoundWidget>(FSoftObjectPath(*FoundWidgetPath)).LoadSynchronous();
	if (FoundWidgetClass) {
		FoundWidget = CreateWidget<UFoundWidget>(GetWorld(), FoundWidgetClass);
	}

	// 被弾ウィジェット
	FString ShootedWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("ShootedWidgetPath"), ShootedWidgetPath, GGameIni);
	ShootedWidgetClass = TSoftClassPtr<UShootedWidget>(FSoftObjectPath(*ShootedWidgetPath)).LoadSynchronous();
	if (ShootedWidgetClass) {
		ShootedWidget = CreateWidget<UShootedWidget>(GetWorld(), ShootedWidgetClass);
	}

	// 操作一覧ウィジェット
	FString OperateListWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("OperateListWidgetPath"), OperateListWidgetPath, GGameIni);
	OperateListWidgetClass = TSoftClassPtr<UOperateListWidget>(FSoftObjectPath(*OperateListWidgetPath)).LoadSynchronous();
	if (OperateListWidgetClass) {
		OperateListWidget = CreateWidget<UOperateListWidget>(GetWorld(), OperateListWidgetClass);
	}
	
	// ポーズメニューウィジェット
	FString PauseMenuWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("PauseMenuWidgetPath"), PauseMenuWidgetPath, GGameIni);
	PauseMenuWidgetClass = TSoftClassPtr<UPauseMenuWidget>(FSoftObjectPath(*PauseMenuWidgetPath)).LoadSynchronous();
	if (PauseMenuWidgetClass) {
		PauseMenuWidget = CreateWidget<UPauseMenuWidget>(GetWorld(), PauseMenuWidgetClass);
	}

	// オーディオメニューウィジェット
	FString AudioMenuWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("AudioMenuWidgetPath"), AudioMenuWidgetPath, GGameIni);
	AudioMenuWidgetClass = TSoftClassPtr < UAudioMenuWidget>(FSoftObjectPath(*AudioMenuWidgetPath)).LoadSynchronous();
	if (AudioMenuWidgetClass) {
		AudioMenuWidget = CreateWidget<UAudioMenuWidget>(GetWorld(), AudioMenuWidgetClass);
	}

	//ウィジェットの種類の数だけ生成し、配列に格納
	TArray<EWidgetType> ArrrayResultType;
	ArrrayResultType.Add(EWidgetType::TutorialClear);
	ArrrayResultType.Add(EWidgetType::GameOver);
	ArrrayResultType.Add(EWidgetType::GameClear);
	for (EWidgetType Type : ArrrayResultType) {

		// リザルトウィジェット
		FString ResultWidgetPath;
		TSubclassOf<UResultWidget> ResultWidgetClass;

		switch (Type) {
		case EWidgetType::TutorialClear: {

			GConfig->GetString(TEXT("UMGSettings"), TEXT("TutorialClearWidgetPath"), ResultWidgetPath, GGameIni);
			ResultWidgetClass = TSoftClassPtr<UResultWidget>(FSoftObjectPath(*ResultWidgetPath)).LoadSynchronous();
			break;
		}
		case EWidgetType::GameOver: {

			GConfig->GetString(TEXT("UMGSettings"), TEXT("GameOverWidgetPath"), ResultWidgetPath, GGameIni);
			ResultWidgetClass = TSoftClassPtr<UResultWidget>(FSoftObjectPath(*ResultWidgetPath)).LoadSynchronous();
			break;
		}
		case EWidgetType::GameClear: {

			GConfig->GetString(TEXT("UMGSettings"), TEXT("GameClearWidgetPath"), ResultWidgetPath, GGameIni);
			ResultWidgetClass = TSoftClassPtr<UResultWidget>(FSoftObjectPath(*ResultWidgetPath)).LoadSynchronous();
			break;
		}
		}

		UResultWidget* ResultWidget = CreateWidget<UResultWidget>(GetWorld(), ResultWidgetClass);
		if (!IsValid(ResultWidget)) {
			continue;
		}
		ResultWidgetList.Add(Type, ResultWidget);
	}

	// エリアマップウィジェットの生成
	FString AreaMapWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("AreaMapWidgetPath"), AreaMapWidgetPath, GGameIni);
	AreaMapWidgetClass = TSoftClassPtr<UAreaMapWidget>(FSoftObjectPath(*AreaMapWidgetPath)).LoadSynchronous();
	if (AreaMapWidgetClass) {
		AreaMapWidget = CreateWidget<UAreaMapWidget>(GetWorld(), AreaMapWidgetClass);
	}

	// ポーズウィジェットの生成
	FString PauseWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("PauseWidgetPath"), PauseWidgetPath, GGameIni);
	PauseWidgetClass = TSoftClassPtr<UPauseWidget>(FSoftObjectPath(*PauseWidgetPath)).LoadSynchronous();
	if (PauseWidgetClass) {
		PauseWidget = CreateWidget<UPauseWidget>(GetWorld(), PauseWidgetClass);
	}

	// カメラ演出ガイドウィジェットの生成
	FString CameraGuideWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("CameraGuideWidgetPath"), CameraGuideWidgetPath, GGameIni);
	CameraGuideWidgetClass = TSoftClassPtr<UCameraGuideWidget>(FSoftObjectPath(*CameraGuideWidgetPath)).LoadSynchronous();
	if (CameraGuideWidgetClass) {
		CameraGuideWidget = CreateWidget<UCameraGuideWidget>(GetWorld(), CameraGuideWidgetClass);
	}

	// カメラ演出時のポーズガイドウィジェットの生成
	FString PauseDirectionGuideWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("PauseDirectionGuideWidgetPath"), PauseDirectionGuideWidgetPath, GGameIni);
	PauseDirectionGuideWidgetClass = TSoftClassPtr<UPauseDirectionGuideWidget>(FSoftObjectPath(*PauseDirectionGuideWidgetPath)).LoadSynchronous();
	if (PauseDirectionGuideWidgetClass) {
		PauseDirectionGuideWidget = CreateWidget<UPauseDirectionGuideWidget>(GetWorld(), PauseDirectionGuideWidgetClass);
	}

	// オーディオガイドウィジェットの生成
	FString AudioGuideWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("AudioGuideWidgetPath"), AudioGuideWidgetPath, GGameIni);
	AudioGuideWidgetClass = TSoftClassPtr<UAudioGuideWidget>(FSoftObjectPath(*AudioGuideWidgetPath)).LoadSynchronous();
	if (AudioGuideWidgetClass) {
		AudioGuideWidget = CreateWidget<UAudioGuideWidget>(GetWorld(), AudioGuideWidgetClass);
	}

	// マップガイドウィジェットの生成
	FString MapGuideWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("MapGuideWidgetPath"), MapGuideWidgetPath, GGameIni);
	MapGuideWidgetClass = TSoftClassPtr<UMapGuideWidget>(FSoftObjectPath(*MapGuideWidgetPath)).LoadSynchronous();
	if (MapGuideWidgetClass) {
		MapGuideWidget = CreateWidget<UMapGuideWidget>(GetWorld(), MapGuideWidgetClass);
	}

	// ポーズガイドウィジェットの生成
	FString PauseGuideWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("PauseGuideWidgetPath"), PauseGuideWidgetPath, GGameIni);
	PauseGuideWidgetClass = TSoftClassPtr<UPauseGuideWidget>(FSoftObjectPath(*PauseGuideWidgetPath)).LoadSynchronous();
	if (PauseGuideWidgetClass) {
		PauseGuideWidget = CreateWidget<UPauseGuideWidget>(GetWorld(), PauseGuideWidgetClass);
	}

	// リザルトガイドウィジェットの生成
	FString ResultGuideWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("ResultGuideWidgetPath"), ResultGuideWidgetPath, GGameIni);
	ResultGuideWidgetClass = TSoftClassPtr<UResultGuideWidget>(FSoftObjectPath(*ResultGuideWidgetPath)).LoadSynchronous();
	if (ResultGuideWidgetClass) {
		ResultGuideWidget = CreateWidget<UResultGuideWidget>(GetWorld(), ResultGuideWidgetClass);
	}

	// 操作一覧ガイドウィジェット
	FString OperateListGuideWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("OperateListGuideWidgetPath"), OperateListGuideWidgetPath, GGameIni);
	OperateListGuideWidgetClass = TSoftClassPtr<UOperateListGuideWidget>(FSoftObjectPath(*OperateListGuideWidgetPath)).LoadSynchronous();
	if (OperateListGuideWidgetClass) {
		OperateListGuideWidget = CreateWidget<UOperateListGuideWidget>(GetWorld(), OperateListGuideWidgetClass);
	}
}

/// <summary>
/// UI表示準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleUiManager::PrepareOpenUi() {

	// フェードウィジェット
	if (IsValid(FadeWidget)) {
		FadeWidget->ResetNowDisplay();
		FadeWidget->ResetAnimFlag();
		FadeWidget->SetImageRenderOpacity(Opacity);
	}

	// 瀕死ウィジェット
	if (IsValid(LowHealthWidget)) {
		LowHealthWidget->ResetNowDisplay();
	}

	// バトルHUDウィジェット
	if (IsValid(BattleWidget)) {
		BattleWidget->ResetNowDisplay();
		BattleWidget->InitializeWidget();
	}

	// ミニマップウィジェット
	if (IsValid(MiniMapWidget)) {
		MiniMapWidget->ResetNowDisplay();
		MiniMapWidget->InitializeWidget();
	}

	// エリアマップウィジェット
	if (IsValid(AreaMapWidget)) {
		AreaMapWidget->ResetNowDisplay();
		AreaMapWidget->InitializeWidget();
	}

	// 照準ウィジェット
	if (IsValid(CrossHairWidget)) {
		CrossHairWidget->ResetNowDisplay();
	}

	// 照準の中心ウィジェット
	if (IsValid(CenterPointWidget)) {
		CenterPointWidget->ResetNowDisplay();
	}

	// ポーズメニューウィジェット
	if (IsValid(PauseMenuWidget)) {
		PauseMenuWidget->ResetNowDisplay();
		PauseMenuWidget->SetupWidget();
	}

	// オーディオメニューウィジェット
	if (IsValid(AudioMenuWidget)) {
		AudioMenuWidget->ResetNowDisplay();
		AudioMenuWidget->SetupWidget();
	}

	// 操作一覧ウィジェット
	if (IsValid(OperateListWidget)) {
		OperateListWidget->ResetNowDisplay();
	}

	// 発見ウィジェット
	if (IsValid(FoundWidget)) {
		FoundWidget->ResetNowDisplay();
	}

	// 被弾ウィジェット
	if (IsValid(ShootedWidget)) {
		ShootedWidget->ResetNowDisplay();
	}

	// ポーズウィジェット
	if (IsValid(PauseWidget)) {
		PauseWidget->ResetNowDisplay();
	}

	// カメラ演出ガイドウィジェット
	if (IsValid(CameraGuideWidget)) {
		CameraGuideWidget->ResetNowDisplay();
	}

	// カメラ演出時のポーズガイドウィジェット
	if (IsValid(PauseDirectionGuideWidget)) {
		PauseDirectionGuideWidget->ResetNowDisplay();
	}

	// オーディオガイドウィジェット
	if (IsValid(AudioGuideWidget)) {
		AudioGuideWidget->ResetNowDisplay();
	}

	// マップガイドウィジェット
	if (IsValid(MapGuideWidget)) {
		MapGuideWidget->ResetNowDisplay();
	}

	// ポーズガイドウィジェット
	if (IsValid(PauseGuideWidget)) {
		PauseGuideWidget->ResetNowDisplay();
	}

	// リザルトガイドウィジェット
	if (IsValid(ResultGuideWidget)) {
		ResultGuideWidget->ResetNowDisplay();
	}

	// リザルトウィジェット
	for (auto Widget : ResultWidgetList) {
		if (!IsValid(Widget.Value)) {
			continue;
		}
		Widget.Value->ResetNowDisplay();
		Widget.Value->SetupWidget();
		Widget.Value->InitializeAnimation();
		Widget.Value->ResetAnimFlag();
	}
}

/// <summary>
/// UI表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleUiManager::OpenUi() {

	//-----------------------------------------------------
	// 各ウィジェットを表示
	//-----------------------------------------------------
	// 瀕死ウィジェット
	if (IsValid(LowHealthWidget)) {
		LowHealthWidget->Open();
	}
	
	// バトルHUDウィジェット
	if (IsValid(BattleWidget)) {
		BattleWidget->Open();
	}
	
	// ミニマップウィジェット
	if (IsValid(MiniMapWidget)) {
		MiniMapWidget->Open();
	}

	// 照準ウィジェット
	if (IsValid(CrossHairWidget)) {
		CrossHairWidget->Open();
	}

	// エリアマップウィジェット
	if (IsValid(AreaMapWidget)) {
		AreaMapWidget->Open();
	}

	// マップガイドウィジェット
	if (IsValid(MapGuideWidget)) {
		MapGuideWidget->Open();
	}

	// 発見ウィジェット
	if (IsValid(FoundWidget)) {
		FoundWidget->Open();
	}

	// 被弾ウィジェット
	if (IsValid(ShootedWidget)) {
		ShootedWidget->Open();
	}

	// フェードウィジェット
	if (IsValid(FadeWidget)) {
		FadeWidget->Open();
	}
}

/// <summary>
/// UI非表示準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleUiManager::PrepareCloseUi() {

	//-----------------------------------------------------
	// 各ウィジェットを非表示前準備処理
	//-----------------------------------------------------
	// フェードウィジェット
	if (!IsValid(FadeWidget)) {
		return;
	}
	FadeWidget->Close();

	// フェードウィジェットを透明にしておく
	FadeWidget->SetImageRenderOpacity(Transparent);
}

/// <summary>
/// UI非表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleUiManager::CloseUi() {

	//-----------------------------------------------------
	// 各ウィジェットを非表示
	// ※フェードウィジェットは非表示にしない
	//-----------------------------------------------------
}

/// <summary>
/// フェードウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> フェードウィジェット </returns>
UFadeWidget* ABattleUiManager::GetFadeWidget() {

	return FadeWidget;
}

/// <summary>
/// 照準ウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> 照準ウィジェット </returns>
UCrossHairWidget* ABattleUiManager::GetCrossHairWidget() {

	return CrossHairWidget;
}

/// <summary>
/// 照準の中心点ウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> 照準の中心点ウィジェット </returns>
UCenterPointWidget* ABattleUiManager::GetCenterPointWidget() {

	return CenterPointWidget;
}

/// <summary>
/// ポーズメニューウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> ポーズメニューウィジェット </returns>
UPauseMenuWidget* ABattleUiManager::GetPauseMenuWidget() {

	return PauseMenuWidget;
}

/// <summary>
/// オーディオメニューウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> オーディオメニューウィジェット </returns>
UAudioMenuWidget* ABattleUiManager::GetAudioMenuWidget() {

	return AudioMenuWidget;
}

/// <summary>
/// リザルトウィジェットの取得
/// </summary>
/// <param name="WidgetType"> オプションの種類 </param>
/// <returns> リザルトウィジェット </returns>
UResultWidget* ABattleUiManager::GetResultWidget(EWidgetType WidgetType) {

	if (!ResultWidgetList.Contains(WidgetType)) {
		return nullptr;
	}

	return ResultWidgetList[WidgetType];
}

/// <summary>
/// バトルウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> バトルウィジェット </returns>
UBattleWidget* ABattleUiManager::GetBattleWidget() {

	return BattleWidget;
}

/// <summary>
/// ミニマップウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> ミニマップウィジェット </returns>
UMiniMapWidget* ABattleUiManager::GetMiniMapWidget() {

	return MiniMapWidget;
}

/// <summary>
/// エリアマップウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> エリアマップウィジェット </returns>
UAreaMapWidget* ABattleUiManager::GetAreaMapWidget() {

	return AreaMapWidget;
}

/// <summary>
/// 瀕死ウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> 瀕死ウィジェット </returns>
ULowHealthWidget* ABattleUiManager::GetLowHealthWidget() {

	return LowHealthWidget;
}

/// <summary>
/// 被弾ウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> 被弾ウィジェット </returns>
UShootedWidget* ABattleUiManager::GetShootedWidget() {

	return ShootedWidget;
}

/// <summary>
/// 発見ウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> 発見ウィジェット </returns>
UFoundWidget* ABattleUiManager::GetFoundWidget() {

	return FoundWidget;
}

/// <summary>
/// ポーズウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> ポーズウィジェット </returns>
UPauseWidget* ABattleUiManager::GetPauseWidget() {

	return PauseWidget;
}

/// <summary>
/// 操作一覧ウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> 操作一覧ウィジェット </returns>
UOperateListWidget* ABattleUiManager::GetOperateListWidget() {

	return OperateListWidget;
}

/// <summary>
/// カメラ演出ガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> カメラ演出ガイドウィジェット </returns>
UCameraGuideWidget* ABattleUiManager::GetCameraGuideWidget() {

	return CameraGuideWidget;
}

/// <summary>
/// カメラ演出時のポーズガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> カメラ演出時のポーズガイドウィジェット </returns>
UPauseDirectionGuideWidget* ABattleUiManager::GetPauseDirectionGuideWidget() {

	return PauseDirectionGuideWidget;
}

/// <summary>
/// オーディオガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> オーディオガイドウィジェット </returns>
UAudioGuideWidget* ABattleUiManager::GetAudioGuideWidget() {

	return AudioGuideWidget;
}

/// <summary>
/// マップガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> マップガイドウィジェット </returns>
UMapGuideWidget* ABattleUiManager::GetMapGuideWidget() {

	return MapGuideWidget;
}

/// <summary>
/// ポーズガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> ポーズガイドウィジェット </returns>
UPauseGuideWidget* ABattleUiManager::GetPauseGuideWidget() {

	return PauseGuideWidget;
}

/// <summary>
/// リザルトガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> リザルトガイドウィジェット </returns>
UResultGuideWidget* ABattleUiManager::GetResultGuideWidget() {

	return ResultGuideWidget;
}

/// <summary>
/// 操作一覧ガイドウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns> 操作一覧ガイドウィジェット </returns>
UOperateListGuideWidget* ABattleUiManager::GetOperateListGuideWidget() {

	return OperateListGuideWidget;
}