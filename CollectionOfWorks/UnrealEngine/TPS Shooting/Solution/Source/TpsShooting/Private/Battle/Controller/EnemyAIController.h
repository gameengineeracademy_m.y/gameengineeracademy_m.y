﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/PawnSensingComponent.h"
#include "Battle/Character/Enemy/Enemy.h"
#include "EnemyAIController.generated.h"


/// <summary>
/// エネミー用のAIコントローラ
/// </summary>
UCLASS()
class AEnemyAIController : public AAIController
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AEnemyAIController(const class FObjectInitializer& ObjectInit);

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

	/// <summary>
	/// 視覚設定をする
	/// </summary>
	/// <param name="bBlind"> 視覚有無 </param>
	/// <returns></returns>
	void SetupSee(bool bBlind);

	/// <summary>
	/// 視野の設定をする
	/// </summary>
	/// <param name="EnemyType"> 敵の種類 </param>
	/// <returns></returns>
	void SetSightRadius(EEnemyType EnemyType);

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

protected:

	/// <summary>
	/// ノイズを聴いた際に呼ぶ処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION()
	void OnHearNoise(APawn* Object, const FVector& Location, float Volume);

	/// <summary>
	/// オブジェクトを視認した時に呼ぶ処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION()
	void OnSeePawn(APawn* Object);

private:

	/// <summary>
	/// 本当にプレイヤーが見えているかシミュレーションする
	/// </summary>
	/// <param name="StartPos"> 開始地点 </param>
	/// <param name="EndPos"> 終端地点 </param>
	/// <returns></returns>
	FHitResult CheckSeePlayer(FVector StartPos, FVector EndPos);

protected:

	/// <summary>
	/// ポーンセンサー
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "EnemyAICnotroller")
	UPawnSensingComponent* PawnSensor;

private:

	/// <summary>
	/// 所有しているポーンの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 所有しているポーン </returns>
	AEnemy* GetPossesPawn();

private:

	/// <summary>
	/// 視覚機能をOFFにするかどうか
	/// </summary>
	float FoundTime;

protected:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// 頭のソケット名
	/// </summary>
	const FName HeadSocketName = "head";

	/// <summary>
	/// 視力半径 デフォルト値
	/// </summary>
	const float SightRadiusDefaultValue = 2500.0f;

	/// <summary>
	/// 視力半径 通常値
	/// </summary>
	const float SightRadiusNormal = 2500.0f;

	/// <summary>
	/// 視力半径 スナイパー
	/// </summary>
	const float SightRadiusSniper = 8000.0f;
};