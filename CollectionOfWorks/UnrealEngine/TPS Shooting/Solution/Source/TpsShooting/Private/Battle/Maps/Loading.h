﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "Battle/Controller/BattleController.h"
#include "Loading.generated.h"

/// <summary>
/// レベル実行フェーズの種類
/// </summary>
UENUM()
enum class ELoadPhaseType {
	Initialize,        // 初期化処理フェーズ
	Load,              // ロード中フェーズ
	Finish,            // 終了処理フェーズ
	MaxTypeIndex
};

/// <summary>
/// ロードレベル
/// </summary>
UCLASS()
class ALoading : public ALevelScriptActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ALoading();

	/// <summary>
	/// 現在のフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentPhase(ELoadPhaseType PhaseType);

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

private:

	/// <summary>
	/// バトルコントローラ
	/// </summary>
	UPROPERTY()
	ABattleController* BattleController;

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	ELoadPhaseType CurrentPhase;
};
