﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "Runtime/Engine/Classes/Engine/TargetPoint.h"
#include "Battle/Character/Enemy/Enemy.h"
#include "EnemySetupTriggerBox.generated.h"

/// <summary>
/// エネミー設定用トリガーボックス
/// </summary>
UCLASS()
class AEnemySetupTriggerBox : public ATriggerBox
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AEnemySetupTriggerBox();

	/// <summary>
	/// エネミーにターゲットポイントをセットする
	/// </summary>
	/// <param name="Enemy"> エネミー </param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category="EnemySetupTriggerBox")
	void SetupTargetPoint(AEnemy* Enemy);

public:

	/// <summary>
	/// ターゲットポイント
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "EnemySetup")
	TArray<ATargetPoint*> TargetPointList;
};
