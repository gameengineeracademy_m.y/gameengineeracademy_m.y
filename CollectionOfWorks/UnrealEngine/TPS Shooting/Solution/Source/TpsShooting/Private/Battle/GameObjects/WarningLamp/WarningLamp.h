﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Battle/Enum/CommonEnum.h"
#include "Sound/SoundCue.h"
#include "Components/AudioComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "WarningLamp.generated.h"

/// <summary>
/// 警告ランプクラス
/// </summary>
UCLASS()
class AWarningLamp : public AActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AWarningLamp();

	/// <summary>
	/// 設置エリアのセット
	/// </summary>
	/// <param name="Type"> エリアの種類 </param>
	/// <returns></returns>
	void SetStageType(EStageType Type);

	/// <summary>
	/// 設置エリアのセット
	/// </summary>
	/// <param name=""></param>
	/// <returns> エリアの種類 </returns>
	EStageType GetStageType();

	/// <summary>
	/// ランプを点灯する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Light();

	/// <summary>
	/// ランプを消灯する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void TurnOff();

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

private:

	/// <summary>
	/// 初期設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Initialize();

protected:

	/// <summary>
	/// スタティックメッシュコンポーネント
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarningLamp")
	UStaticMeshComponent* LampMeshComp;

	/// <summary>
	/// ランプマテリアルインスタンス
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "WarningLamp")
	UMaterialInstanceDynamic* LampMaterialInstance;

	/// <summary>
	/// 警戒サウンド
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "WarningLamp")
	USoundCue* WarningSound;

	/// <summary>
	/// パラメータ名:点滅
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "WarningLamp")
	FName BlinkingParamName;

	/// <summary>
	/// 点滅 初期値
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "WarningLamp")
	float InitBlinkingValue;

	/// <summary>
	/// パラメータ名:明るさ
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "WarningLamp")
	FName BrightnessParamName;

	/// <summary>
	/// 明るさ
	/// </summary>
	UPROPERTY(EditAnywhere, Category="WarningLamp")
	float BrightnessValue;

	/// <summary>
	/// パラメータ名:点滅速度
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "WarningLamp")
	FName FlashingSpeedParamName;

	/// <summary>
	/// 点滅速度
	/// </summary>
	UPROPERTY(EditAnywhere, Category="WarningLamp")
	float FlashingSpeedValue;

private:

	/// <summary>
	/// 設置エリアの種類
	/// </summary>
	EStageType StageType;

	/// <summary>
	/// 警戒ランプサウンドコンポーネント
	/// </summary>
	UPROPERTY()
	UAudioComponent* WarningSoundComp;

private:
	//--------------------------------
	//以下、定数宣言
	//--------------------------------
	/// <summary>
	/// 点灯値
	/// </summary>
	const float LightValue = 1.0f;

	/// <summary>
	/// 消灯値
	/// </summary>
	const float TurnOffValue = 0.0f;
};