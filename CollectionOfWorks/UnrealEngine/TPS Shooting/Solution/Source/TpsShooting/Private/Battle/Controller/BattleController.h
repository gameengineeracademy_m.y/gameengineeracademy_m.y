﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Battle/UiManager/BattleUiManager.h"
#include "Battle/Interface/BattleEvent.h"
#include "Audio/Manager/AudioManager.h"
#include "BattleController.generated.h"

/// <summary>
/// 入力の種類
/// </summary>
UENUM()
enum class EBattleInputType {
	None,              // なし
	Game,              // ゲーム中操作
	CameraDirection,   // カメラ演出操作
	Map,               // マップ操作
	PausePlay,         // プレイポーズ中操作
	PauseDirection,    // 演出ポーズ中操作
	Audio,             // オーディオ設定操作
	OperateList,       // 操作一覧操作
	TutorialClear,     // チュートリアルクリア操作
	GameOver,          // ゲームオーバー操作
	GameClear,         // ゲームクリア操作
	MaxTypeIndex
};

/// <summary>
/// バトルレベル用コントローラ
/// </summary>
UCLASS()
class ABattleController : public APlayerController
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ABattleController();

	/// <summary>
	/// インプットの設定処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupInput();

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	/// <param name=""> イベントインターフェース </param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "BattleController")
	void SetEventInterface(const TScriptInterface<IBattleEvent>& Interface);

	/// <summary>
	/// ロード処理時のUI生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreateUiForLoading();

	/// <summary>
	/// ロード処理時のUI表示準備処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PrepareOpenUiForLoading();

	/// <summary>
	/// ロード処理時のUI表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OpenUiForLoading();

	/// <summary>
	/// ロード処理時のUI非表示準備処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PrepareCloseUiForLoading();

	/// <summary>
	/// ロード処理時のUI非表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CloseUiForLoading();

	/// <summary>
	/// 各ウィジェットのUI生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreateUi();

	/// <summary>
	/// UI表示準備処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PrepareOpenUi();

	/// <summary>
	/// UI表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OpenUi();

	/// <summary>
	/// UI非表示準備処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PrepareCloseUi();

	/// <summary>
	/// UI非表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CloseUi();

	/// <summary>
	/// フェードウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> フェードウィジェット </returns>
	UFadeWidget* GetFadeWidget();

	/// <summary>
	/// 照準ウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 照準ウィジェット </returns>
	UCrossHairWidget* GetCrossHairWidget();

	/// <summary>
	/// 照準の中心点ウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 照準の中心点ウィジェット </returns>
	UCenterPointWidget* GetCenterPointWidget();

	/// <summary>
	/// ポーズメニューウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ポーズメニューウィジェット </returns>
	UPauseMenuWidget* GetPauseMenuWidget();

	/// <summary>
	/// オーディオメニューウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オーディオメニューウィジェット </returns>
	UAudioMenuWidget* GetAudioMenuWidget();

	/// <summary>
	/// リザルトウィジェットの取得
	/// </summary>
	/// <param name="OptionType"> ウィジェットの種類 </param>
	/// <returns> リザルトウィジェット </returns>
	UResultWidget* GetResultWidget(EWidgetType WidgetType);

	/// <summary>
	/// バトルHUDウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> バトルHUDウィジェット </returns>
	UFUNCTION(BlueprintCallable, Category = "BattleController")
	UBattleWidget* GetBattleWidget();

	/// <summary>
	/// ミニマップウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ミニマップウィジェット </returns>
	UMiniMapWidget* GetMiniMapWidget();

	/// <summary>
	/// エリアマップウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> エリアマップウィジェット </returns>
	UAreaMapWidget* GetAreaMapWidget();

	/// <summary>
	/// 瀕死ウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 瀕死ウィジェット </returns>
	ULowHealthWidget* GetLowHealthWidget();

	/// <summary>
	/// 被弾ウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 被弾ウィジェット </returns>
	UShootedWidget* GetShootedWidget();

	/// <summary>
	/// 発見ウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 発見ウィジェット </returns>
	UFoundWidget* GetFoundWidget();

	/// <summary>
	/// ポーズウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ポーズウィジェット </returns>
	UPauseWidget* GetPauseWidget();

	/// <summary>
	/// 操作一覧ウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 操作一覧ウィジェット </returns>
	UOperateListWidget* GetOperateListWidget();

	/// <summary>
	/// カメラ演出ガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> カメラ演出ガイドウィジェット </returns>
	UCameraGuideWidget* GetCameraGuideWidget();

	/// <summary>
	/// カメラ演出時のポーズガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> カメラ演出時のポーズガイドウィジェット </returns>
	UPauseDirectionGuideWidget* GetPauseDirectionGuideWidget();

	/// <summary>
	/// オーディオガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オーディオガイドウィジェット </returns>
	UAudioGuideWidget* GetAudioGuideWidget();

	/// <summary>
	/// マップガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> マップガイドウィジェット </returns>
	UMapGuideWidget* GetMapGuideWidget();

	/// <summary>
	/// ポーズガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ポーズガイドウィジェット </returns>
	UPauseGuideWidget* GetPauseGuideWidget();

	/// <summary>
	/// リザルトガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> リザルトガイドウィジェット </returns>
	UResultGuideWidget* GetResultGuideWidget();

	/// <summary>
	/// 操作一覧ガイドウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 操作一覧ガイドウィジェット </returns>
	UOperateListGuideWidget* GetOperateListGuideWidget();

	/// <summary>
	/// オーディオマネージャの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オーディオマネージャ </returns>
	AAudioManager* GetAudioManager();

	/// <summary>
	/// インプットの種類を追加
	/// </summary>
	/// <param name="InputType"> 入力の種類 </param>
	/// <returns></returns>
	void AddInputType(EBattleInputType InputType);

	/// <summary>
	/// インプットの種類を削除
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void DeleteInputType();

	/// <summary>
	/// マウスカーソルが移動したかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:移動した </returns>
	bool IsMoveMouseCursor();

protected:

	/// <summary>
	/// インプットコンポーネントの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void SetupInputComponent() override;

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

private:

	/// <summary>
	/// オーディオマネージャの生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreateAudioManager();

	/// <summary>
	/// Escapeキー押下処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressEscape();

	/// <summary>
	/// Tabキー押下処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressTab();

	/// <summary>
	/// Bキー押下処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressB();

	/// <summary>
	/// キーバインドアクションを切り替える
	/// </summary>
	/// <param name="InputType"> 入力の種類 </param>
	/// <returns></returns>
	void SwitchKeyBindAction(EBattleInputType InputType);

	/// <summary>
	/// ゲームプレイ中のキーバインドアクション設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupKeyBindActionGamePlay();

	/// <summary>
	/// マップ用のキーバインドアクション設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupKeyBindActionMap();

	/// <summary>
	/// 演出用のキーバインドアクション設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupKeyBindActionCameraDirection();

	/// <summary>
	/// プレイポーズ用のキーバインドアクション設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupKeyBindActionPausePlay();

	/// <summary>
	/// 演出ポーズ用のキーバインドアクション設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupKeyBindActionPauseDirection();

	/// <summary>
	/// オーディオ用のキーバインドアクション設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupKeyBindActionAudio();

	/// <summary>
	/// 操作一覧用のキーバインドアクション設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupKeyBindActionOperateList();

	/// <summary>
	/// リザルト用のキーバインドアクション設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupKeyBindActionResult();

	/// <summary>
	/// 設定されたインプットが存在するかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:存在する </returns>
	bool IsExistsInput();

	/// <summary>
	/// インプットの種類を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> インプットの種類 </returns>
	EBattleInputType GetInputType();

	/// <summary>
	/// 決定ボタン押下処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressDecide();

	/// <summary>
	/// 戻るボタン押下処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressReturn();

	/// <summary>
	/// 上方向ボタン押下処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressUp();

	/// <summary>
	/// 下方向ボタン押下処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressDown();

	/// <summary>
	/// 左方向ボタン押下処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressLeft();

	/// <summary>
	/// 右方向ボタン押下処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressRight();

	/// <summary>
	/// Sボタン押下処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressS();

	/// <summary>
	/// Sボタンを離した処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ReleaseS();

private:

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IBattleEvent> EventInterface;

	/// <summary>
	/// インプットの種類
	/// </summary>
	UPROPERTY()
	TArray<EBattleInputType> ArrayInputType;

	/// <summary>
	/// オーディオマネージャクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<AAudioManager> AudioManagerClass;

	/// <summary>
	/// オーディオマネージャ
	/// </summary>
	UPROPERTY()
	AAudioManager* AudioManager;

	/// <summary>
	/// 上下方向のマウス移動量
	/// </summary>
	float OldCurPosX;

	/// <summary>
	/// 水平方向のマウス移動量
	/// </summary>
	float OldCurPosY;

private:
	//--------------------------------
	//以下、定数宣言
	//--------------------------------
	/// <summary>
	/// 前進移動量
	/// </summary>
	const float ForwardMoveValue = 1.0f;

	/// <summary>
	/// 後退移動量
	/// </summary>
	const float BackMoveValue = -1.0f;

	/// <summary>
	/// 右移動量
	/// </summary>
	const float RightMoveValue = 1.0f;

	/// <summary>
	/// 左移動量
	/// </summary>
	const float LeftMoveValue = -1.0f;

	/// <summary>
	/// 見上げる量
	/// </summary>
	const float LookUpValue = -1.0f;

	/// <summary>
	/// マウスホイール操作量
	/// </summary>
	const float MouseWheelValue = 1.0f;

	/// <summary>
	/// 0
	/// </summary>
	const int32 Zero = 0;

	/// <summary>
	/// 1
	/// </summary>
	const int32 One = 1;

	/// <summary>
	/// 最小インデックス
	/// </summary>
	const int32 MinIndex = 0;

	/// <summary>
	/// 押している時の値
	/// </summary>
	const float PressValue = 1.0f;

	/// <summary>
	/// 離している時の値
	/// </summary>
	const float ReleaseValue = 0.0f;

	/// <summary>
	/// 移動していない量
	/// </summary>
	const float NotMoveValue = 0.0f;
};
