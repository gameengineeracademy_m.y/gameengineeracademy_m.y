﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Character/Enemy/Enemy.h"
#include "Battle/Controller/EnemyAIController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "NavigationSystem.h"
#include "EnvironmentQuery/EnvQueryManager.h"
#include "Kismet/GameplayStatics.h"
#include "Battle/Controller/BattleController.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AEnemy::AEnemy()
	: SearchSurroundNearPoint(nullptr)
	, SearchSurroundFarPoint(nullptr)
	, SearchHidePoint(nullptr)
	, FoundPlayerSound(nullptr)
	, EnemyType(EEnemyType::None)
	, StageType(EStageType::None)
	, CurrentAlertLevel(EAlertLevel::None)
	, CurrentPatrolPhase(EPatrolPhase::None)
	, CurrentBewarePhase(EBewarePhase::None)
	, CurrentFightPhase(EFightPhase::None)
	, CurrentChasePhase(EChasePhase::None)
	, NextChasePhase(EChasePhase::None)
	, CurrentHidePhase(EHidePhase::None)
	, CurrentApproachPhase(EApproachPhase::None)
	, CurrentShootPhase(EShootPhase::None)
	, CurrentSearchPhase(ESearchPhase::None)
	, EnvQueryType(EEnvQueryType::None)
	, BewareActionType(EBewareActionType::None)
	, PatrolPoint()
	, PatrolIndex(0)
	, NavigationPath(nullptr)
	, PathIndex(0)
	, TargetPosition()
	, MovePosition()
	, bFoundSomething(false)
	, bOldFoundSomething(false)
	, LevelSwitchTimerHandle()
	, NotFoundTime(0.0f)
	, WaitTotalTime(0.0f)
	, HideWaitTime(0.0f)
	, ShootTime(0.0f)
	, StopShootTime(0.0f)
	, FoundGradeResetTime(0.0f)
	, FoundGrade(0.0f)
	, bFinishSearch(false) {

	// Tick処理の無効化
	PrimaryActorTick.bCanEverTick = false;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemy::BeginPlay() {

	Super::BeginPlay();

	EnemyType = EEnemyType::None;
	PatrolIndex = MinIndex;

	CurrentAlertLevel = EAlertLevel::Patrol;
	CurrentPatrolPhase = EPatrolPhase::Wait;
	CurrentBewarePhase = EBewarePhase::None;
	CurrentFightPhase = EFightPhase::None;
	CurrentShootPhase = EShootPhase::None;
	CurrentSearchPhase = ESearchPhase::None;
	CurrentHidePhase = EHidePhase::None;

	WaitTotalTime = ResetTime;
	HideWaitTime = ResetTime;
	ShootTime = ResetTime;
	StopShootTime = ResetTime;
	FoundGradeResetTime = ResetTime;
	FoundGrade = ResetFoundGradeValue;

	bFinishSearch = false;
	bOldFoundSomething = false;
	bFoundSomething = false;

	bUseControllerRotationYaw = true;
	GetCharacterMovement()->bUseControllerDesiredRotation = true;

	SetHitPoint(DefaultHP);
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AEnemy::Tick(float DeltaTime) {

	//-----------------------------------------------
	// 処理負荷軽減のためTick処理は自身では実行せず、
	// EnemyManagerクラスから実行する
	//-----------------------------------------------
	Super::Tick(DeltaTime);

	// 被弾フラグを降ろしておく
	if (bRecieveDamage) {
		bRecieveDamage = false;
		//TurnTowardsShooted();
	}

	if (!IsFine()) {
		return;
	}

	// 所持弾薬のオート取得処理
	SupplyAmmosAuto();

	// ゲームの状況が「プレイ中」の時のみ
	// 状況に応じた処理を実行する
	bool IsCheck = IsGameStatePlay();
	if (IsCheck) {
		AccordSituation(DeltaTime);
	}

	switch (CurrentAlertLevel) {
	case EAlertLevel::Patrol: {

		PatrolAction(DeltaTime);
		break;
	}
	case EAlertLevel::Beware: {

		BewareAction(DeltaTime);
		break;
	}
	case EAlertLevel::Fight: {

		FightAction(DeltaTime);
		break;
	}
	}

	// 視覚処理が毎フレームの処理ではないため、
	// 一度視覚に入ると気づいたままになってしまうので
	// ここで一度気づいていないことにしておく
	bOldFoundSomething = bFoundSomething;
	bFoundSomething = false;
}

/// <summary>
/// エネミーの種類のセット
/// </summary>
/// <param name="Type"> エネミーの種類 </param>
/// <returns></returns>
void AEnemy::SetEnemyType(EEnemyType Type) {

	EnemyType = Type;
}

/// <summary>
/// エネミーの種類の取得
/// </summary>
/// <param name=""></param>
/// <returns> エネミーの種類 </returns>
EEnemyType AEnemy::GetEnemyType() {

	return EnemyType;
}

/// <summary>
/// エリアの種類のセット
/// </summary>
/// <param name="Type"> エリアの種類 </param>
/// <returns></returns>
void AEnemy::SetStageType(EStageType Type) {

	StageType = Type;
}

/// <summary>
/// エリアの種類の取得
/// </summary>
/// <param name=""></param>
/// <returns> エリアの種類 </returns>
EStageType AEnemy::GetStageType() {

	return StageType;
}

/// <summary>
/// パトロールポイント配列が空かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:空 </returns>
bool AEnemy::IsNullPatrolPoint() {
	
	return PatrolPoint.Num() == Zero;
}

/// <summary>
/// パトロールポイントを配列に追加
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemy::AddPatrolPoint(ATargetPoint* Target) {

	if (!IsValid(Target)) {
		return;
	}
	PatrolPoint.Add(Target);
}

/// <summary>
/// ターゲット位置をセット
/// </summary>
/// <param name="Position"> ターゲット位置 </param>
/// <returns></returns>
void AEnemy::SetTargetPosition(FVector Position) {

	TargetPosition = Position;
}

/// <summary>
/// ターゲット位置を取得
/// </summary>
/// <param name=""></param>
/// <returns> ターゲット位置 </returns>
FVector AEnemy::GetTargetPosition() {

	return TargetPosition;
}

/// <summary>
/// 所持弾薬の自動補給
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemy::SupplyAmmosAuto() {

	AWeaponBase* Weapon = GetEquipWeapon();
	if (!IsValid(Weapon)) {
		return;
	}
	if (Weapon->IsExistsMaxPossessAmmos()) {
		return;
	}
	Weapon->SetMaxPossessAmmos();
}

/// <summary>
/// インデックスを加算して取得
/// </summary>
/// <param name=""></param>
/// <returns> インデックス </returns>
int32 AEnemy::AddPatrolIndex() {

	++PatrolIndex;
	if (PatrolIndex >= PatrolPoint.Num()) {
		PatrolIndex = MinIndex;
	}

	return PatrolIndex;
}

/// <summary>
/// パトロールポイントの取得
/// </summary>
/// <param name="Index"> インデックス </param>
/// <returns> パトロールポイント </returns>
ATargetPoint* AEnemy::GetPatrolPoint(int32 Index) {

	if (Index < MinIndex || Index >= PatrolPoint.Num()) {
		return nullptr;
	}

	return PatrolPoint[Index];
}

/// <summary>
/// 何かに気づいたことにする
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemy::FoundSomething() {

	bFoundSomething = true;
}

/// <summary>
/// 警戒レベルを強制的に巡回に変更する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemy::ChangeForciblyAlertLevelPatrol() {

	StopShooting();
	GetCharacterMovement()->StopMovementImmediately();
	OnSwitchPatrolLevel();
}

/// <summary>
/// 何かに気づいたかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:気づいた </returns>
bool AEnemy::IsFoundSomething() {

	return bFoundSomething;
}

/// <summary>
/// コントローラの取得
/// </summary>
/// <param name=""></param>
/// <returns> コントローラ </returns>
AEnemyAIController* AEnemy::GetEnemyController() {

	return Cast<AEnemyAIController>(UAIBlueprintHelperLibrary::GetAIController(this));
}

/// <summary>
/// 状況に応じて行動を行う
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AEnemy::AccordSituation(float DeltaTime) {

	bool IsFound = CheckFoundPlayer(DeltaTime);
	if (IsFound) {
		if (IsPatrol()) {
			// 画面に発見ウィジェットを表示する
			FoundPlayer();
		}
		// 警戒レベルを戦闘レベルに遷移する
		SwitchFightLevel();
	}

	//--------------------------------------------
	//各処理を頻繁に動かさないために
	//プレイヤーを見つけてから見失うまでの時間に
	//遅延補正をかける
	//--------------------------------------------
	bFoundSomething = CorrectNotFoundPlayer(DeltaTime);

	switch (CurrentAlertLevel) {
	case EAlertLevel::Patrol: {

		if (bFoundSomething) {

			ChangeCurrentPatrolPhase(EPatrolPhase::Look);
		}
		else {

			if (CurrentPatrolPhase != EPatrolPhase::Look) {
				break;
			}

			if (FoundGrade >= FoundSomethingAmount) {

				ChangeCurrentPatrolPhase(EPatrolPhase::ApproachSlowly);
				ChangeCurrentApproachPhase(EApproachPhase::SearchRoute);
			}
			else {
				ChangeCurrentPatrolPhase(EPatrolPhase::NonCombat);
			}
		}

		break;
	}
	case EAlertLevel::Beware: {

		//---------------------------------------------
		// 警戒レベル
		// 指定条件時のみ次の移動先探索フェーズを変更する
		//---------------------------------------------

		if (CurrentFightPhase != EFightPhase::Chase ||
			CurrentChasePhase != EChasePhase::SearchPosition) {
			break;
		}

		if (CurrentSearchPhase != ESearchPhase::None) {
			break;
		}

		ChangeCurrentSearchPhase(ESearchPhase::SearchChaseMove);
		break;
	}
	case EAlertLevel::Fight: {

		//---------------------------------------------
		// 戦闘レベル
		// プレイヤーを見つけたら射撃処理を実行
		// 指定条件時のみ次の移動先探索フェーズを変更する
		//---------------------------------------------

		if (bFoundSomething) {

			switch (EnemyType) {
			case EEnemyType::Normal: {

				//----------------------------------------------
				//プレイヤーが見えている場合、直線距離と
				//高さの差が許容範囲内であれば射撃を行う
				//----------------------------------------------
				float DifferDistance = (GetActorLocation() - TargetPosition).Size();
				float DifferHeight = FMath::Abs(GetActorLocation().Z - TargetPosition.Z);
				if (DifferDistance >= DistanceAllowableError ||
					DifferHeight >= HeightAllowableError) {
					break;
				}

				if (CurrentShootPhase != EShootPhase::None) {
					break;
				}

				ACharacterBase::StopRunning();
				ACharacterBase::Aim();
				ChangeCurrentShootPhase(EShootPhase::InitWait);
				ChangeCurrentFightPhase(EFightPhase::Shoot);
				break;
			}
			case EEnemyType::Sniper: {

				//----------------------------------------------
				//プレイヤーが見えている場合、直線距離と
				//高さの差が許容範囲内であれば射撃を行う
				//----------------------------------------------
				if (CurrentShootPhase != EShootPhase::None) {
					break;
				}

				ACharacterBase::StopRunning();
				ACharacterBase::Aim();
				ChangeCurrentShootPhase(EShootPhase::StartShoot);
				ChangeCurrentFightPhase(EFightPhase::Shoot);
				break;
			}
			}
		}
		else {

			switch (EnemyType) {
			case EEnemyType::Normal: {
				if (CurrentFightPhase != EFightPhase::Shoot ||
					CurrentShootPhase != EShootPhase::None) {
					break;
				}

				ChangeCurrentFightPhase(EFightPhase::Chase);
				ChangeCurrentSearchPhase(ESearchPhase::SearchChaseMove);
				break;
			}
			}
		}

		break;
	}
	}
}

/// <summary>
/// プレイヤー発見したかどうか
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns> true:発見した </returns>
bool AEnemy::CheckFoundPlayer(float DeltaTime) {

	if (!bFoundSomething) {
		return false;
	}

	//警戒レベルに応じて処理を実行する
	EAlertLevel AlertLevel = GetCurrentAlertLevel();
	switch (AlertLevel) {
	case EAlertLevel::Fight: {

		//--------------------------------------------
		//警戒レベルが既に戦闘中
		//--------------------------------------------
		break;
	}
	case EAlertLevel::Beware: {

		//--------------------------------------------
		//警戒レベルが警戒中なら戦闘中に移行するように
		//発見度に大きな値を渡す
		//--------------------------------------------
		AddFoundGrade(FoundPlayerValue);
		break;
	}
	case EAlertLevel::Patrol: {

		//--------------------------------------------
		//警戒レベルが巡回中ならプレイヤーとの
		//直線距離に応じた値を発見度に加算する
		//--------------------------------------------
		float Distance = CheckTargetLinearDistance(GetActorLocation(), TargetPosition);
		float FoundGradeValue = FoundPlayerDistance / Distance;

		// 発見度リセットタイマーの初期化
		FoundGradeResetTime = ResetTime;
		AddFoundGrade(FoundGradeValue);
		break;
	}
	}

	if (FoundGrade < FoundPlayerAmount) {
		return false;
	}

	return true;
}

/// <summary>
/// 見失うまでの補正処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns> false:見失う </returns>
bool AEnemy::CorrectNotFoundPlayer(float DeltaTime) {

	//---------------------------------------
	//見失っている時間が一定時間を超えた時、
	//見失っているとする
	//---------------------------------------
	if (bFoundSomething) {
		NotFoundTime = ResetTime;
		return true;
	}

	if (!bOldFoundSomething) {
		return false;
	}

	NotFoundTime += DeltaTime;
	if (NotFoundTime < NotFoundTimeValue) {
		return true;
	}
	NotFoundTime = ResetTime;

	return false;
}

/// <summary>
/// プレイヤーを発見度を加算
/// </summary>
/// <param name="Grade"> 発見度合い </param>
/// <returns></returns>
void AEnemy::AddFoundGrade(float Grade) {

	//-----------------------------------------------
	//プレイヤーの発見度合いを加算する
	//-----------------------------------------------
	FoundGrade += Grade;
	GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, FString::FromInt(FoundGrade));
}

/// <summary>
/// プレイヤー発見度のリセット処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AEnemy::ResetFoundGrade(float DeltaTime) {

	// 未発見の場合の処理
	if (IsFoundSomething() && FoundGrade <= ResetFoundGradeValue) {
		return;
	}

	FoundGradeResetTime += DeltaTime;
	if (FoundGradeResetTime < ResetTimeFoundGrade) {
		return;
	}
	FoundGradeResetTime = ResetTime;
	FoundGrade = ResetFoundGradeValue;
}

/// <summary>
/// プレイヤーを発見
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemy::FoundPlayer() {

	AOperatePlayer* Player = Cast<AOperatePlayer>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (!IsValid(Player)) {
		return;
	}

	// プレイヤーに対するエネミーの角度を算出し、
	// プレイヤーに対する角度から方角を取得
	float EnemyAngle = CalcAngleToAim(GetActorForwardVector(), Player);
	EDirectionType DirectionType = SearchDirection(EnemyAngle);

	// 発見ウィジェットの表示
	ABattleController* BattleController = Cast<ABattleController>(UGameplayStatics::GetPlayerController(this, 0));
	if (!IsValid(BattleController)) {
		return;
	}
	BattleController->GetFoundWidget()->PlayFoundAnimation(DirectionType);

	// 発見サウンドを鳴らす
	if (!IsValid(FoundPlayerSound)) {
		return;
	}
	UGameplayStatics::PlaySound2D(GetWorld(), FoundPlayerSound);
}

/// <summary>
/// 巡回行動
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AEnemy::PatrolAction(float DeltaTime) {

	// 発見度の初期化処理
	ResetFoundGrade(DeltaTime);

	switch (CurrentPatrolPhase) {
	case EPatrolPhase::None: {

		break;
	}
	case EPatrolPhase::DecidePos: {

		// 次に向かうパトロール地点をセット
		int32 Index = AddPatrolIndex();

		ChangeCurrentPatrolPhase(EPatrolPhase::Wait);
		break;
	}
	case EPatrolPhase::Wait: {

		//--------------------------------------
		//指定時間待機する
		//その後、移動先までの経路探索を行う
		//--------------------------------------
		WaitTotalTime += DeltaTime;
		if (WaitTotalTime < PatrolWaitTime) {
			break;
		}
		WaitTotalTime = ResetTime;

		ChangeCurrentPatrolPhase(EPatrolPhase::SearchRoute);
		break;
	}
	case EPatrolPhase::SearchRoute: {

		ATargetPoint* Point = GetPatrolPoint(PatrolIndex);
		if (!IsValid(Point)) {
			break;
		}
		FVector Pos = Point->GetActorLocation();

		bool IsFinish = SearchRoute(Pos);
		if (!IsFinish) {
			break;
		}

		ChangeCurrentPatrolPhase(EPatrolPhase::Turn);
		break;
	}
	case EPatrolPhase::Turn: {

		//--------------------------------------
		//次に向かう地点へ方向転換を行う
		//--------------------------------------
		if (!IsValid(NavigationPath)) {
			break;
		}
		
		// 方向転換
		bool IsFinish = Turn(DeltaTime, StartIndex);
		if (!IsFinish) {
			break;
		}

		ChangeCurrentPatrolPhase(EPatrolPhase::Move);
		break;
	}
	case EPatrolPhase::Move: {

		//--------------------------------------
		//次に向かうパトロール地点へ移動する
		//※移動しながら身体の向きを微調整する
		//--------------------------------------
		Turn(DeltaTime, PathIndex);

		bool IsArrived = Move(DeltaTime);
		if (!IsArrived) {
			break;
		}

		ChangeCurrentPatrolPhase(EPatrolPhase::DecidePos);
		break;
	}
	case EPatrolPhase::Look: {

		TurnPlayer();
		ACharacterBase::Aim();
		GetCharacterMovement()->StopMovementImmediately();
		break;
	}
	case EPatrolPhase::ApproachSlowly: {

		bool IsFinish = Approach(DeltaTime);
		if (!IsFinish) {
			break;
		}

		ChangeCurrentPatrolPhase(EPatrolPhase::NonCombat);
		break;
	}
	case EPatrolPhase::NonCombat: {

		ChangeCurrentShootPhase(EShootPhase::StopShoot);
		ACharacterBase::StopAiming();
		ACharacterBase::StopRunning();

		ChangeCurrentPatrolPhase(EPatrolPhase::Return);
		break;
	}
	case EPatrolPhase::Return: {

		ATargetPoint* Point = GetPatrolPoint(PatrolIndex);
		if (!IsValid(Point)) {
			break;
		}
		FVector Pos = Point->GetActorLocation();

		bool IsFinish = SearchRoute(Pos);
		if (!IsFinish) {
			break;
		}

		ChangeCurrentPatrolPhase(EPatrolPhase::Turn);
		break;
	}
	case EPatrolPhase::NotifyWait: {

		GetCharacterMovement()->StopMovementImmediately();
		break;
	}
	}
}

/// <summary>
/// 警戒行動
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AEnemy::BewareAction(float DeltaTime) {

	switch (BewareActionType) {
	case EBewareActionType::Approach: {
		//--------------------------------
		//目標位置へ接近する
		//--------------------------------
		switch (CurrentBewarePhase) {
		case EBewarePhase::None: {

			break;
		}
		case EBewarePhase::Chase: {

			Chase(DeltaTime);
			break;
		}
		}

		break;
	}
	case EBewareActionType::Hide: {
		//--------------------------------
		//隠れて待機
		//--------------------------------
		switch (CurrentBewarePhase) {
		case EBewarePhase::None: {

			break;
		}
		case EBewarePhase::Hide: {

			Hide(DeltaTime);
			break;
		}
		}

		break;
	}
	}
}

/// <summary>
/// 戦闘行動
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AEnemy::FightAction(float DeltaTime) {

	switch (CurrentFightPhase) {
	case EFightPhase::Chase: {

		Chase(DeltaTime);
		break;
	}
	case EFightPhase::Shoot: {

		Shoot(DeltaTime);
		break;
	}
	default: {

		break;
	}
	}
}

/// <summary>
/// 追跡行動
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AEnemy::Chase(float DeltaTime) {

	switch (CurrentChasePhase) {
	case EChasePhase::None: {

		break;
	}
	case EChasePhase::Wait: {

		// 他のエネミーがプレイヤーを見つけていた場合、
		// 待機せずに次のルートを探索する
		if (bResetSearchRoute) {
			bResetSearchRoute = false;
			WaitTotalTime = ChaseWaitTime;
		}

		WaitTotalTime += DeltaTime;
		if (WaitTotalTime < ChaseWaitTime) {
			break;
		}
		WaitTotalTime = ResetTime;

		ChangeCurrentSearchPhase(ESearchPhase::SearchChaseMove);
		ChangeCurrentChasePhase(EChasePhase::SearchPosition);
		break;
	}
	case EChasePhase::SearchPosition: {

		//---------------------------------
		//次に移動する場所を探索し、
		//戦闘行動フェーズを設定する
		//※この処理はプレイヤーが視界から
		//  消えたら実行される
		//---------------------------------
		bool IsFinish = SearchDestination();
		if (!IsFinish) {
			break;
		}

		// プレイヤーとの移動総距離を算出する
		float TotalDistance = CheckTargetTotalDistance();
		UE_LOG(LogTemp, Display, TEXT("ESearchPhase::DecideAction Distance : %f"), TotalDistance)

		switch (CurrentAlertLevel) {
		case EAlertLevel::Fight: {
			//----------------------------------
			// 警戒レベル：戦闘中
			//----------------------------------

			// プレイヤーとの移動総距離に応じて処理を変更する
			if (TotalDistance >= FarDistance) {
				ChangeCurrentChasePhase(EChasePhase::Move);
			}
			else {
				ChangeCurrentChasePhase(EChasePhase::Patrol);
			}
			break;
		}
		case EAlertLevel::Beware: {
			//----------------------------------
			// 警戒レベル：警戒中
			//----------------------------------

			ChangeCurrentChasePhase(EChasePhase::Patrol);
			break;
		}
		}
		break;
	}
	case EChasePhase::Move: {

		ACharacterBase::StopAiming();
		ACharacterBase::Run();

		Turn(DeltaTime, PathIndex);

		bool IsArrived = Move(DeltaTime);
		if (!IsArrived) {
			break;
		}

		ChangeCurrentSearchPhase(ESearchPhase::SearchChaseMove);
		ChangeCurrentChasePhase(EChasePhase::SearchPosition);
		break;
	}
	case EChasePhase::Patrol: {

		ACharacterBase::StopRunning();
		ACharacterBase::Aim();

		Turn(DeltaTime, PathIndex);

		bool IsArrived = Move(DeltaTime);
		if (!IsArrived) {
			break;
		}

		WaitTotalTime = ResetTime;
		ChangeCurrentChasePhase(EChasePhase::Wait);
		break;
	}
	}
}

/// <summary>
/// 隠れる行動
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AEnemy::Hide(float DeltaTime) {

	switch (CurrentHidePhase) {
	case EHidePhase::Wait: {

		WaitTotalTime += DeltaTime;
		if (WaitTotalTime < HideWaitTime) {
			break;
		}
		WaitTotalTime = ResetTime;

		ChangeCurrentSearchPhase(ESearchPhase::SearchHideMove);
		ChangeCurrentHidePhase(EHidePhase::SearchPosition);
		break;
	}
	case EHidePhase::SearchPosition: {

		//---------------------------------
		//次に移動する場所を探索する
		//---------------------------------
		bool IsFinish = SearchDestination();
		if (!IsFinish) {
			break;
		}

		bPressCrouchButton = false;
		ChangeCurrentHidePhase(EHidePhase::Move);
		break;
	}
	case EHidePhase::Move: {

		ACharacterBase::StopAiming();
		ACharacterBase::Run();

		Turn(DeltaTime, PathIndex);

		bool IsArrived = Move(DeltaTime);
		if (!IsArrived) {
			break;
		}

		bPressCrouchButton = true;
		WaitTotalTime = ResetTime;
		HideWaitTime = FMath::RandRange(HideMinTime, HideMaxTime);
		ChangeCurrentHidePhase(EHidePhase::Wait);
		break;
	}
	}
}

/// <summary>
/// 接近行動
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns> true:接近終了 </returns>
bool AEnemy::Approach(float DeltaTime) {

	switch (CurrentApproachPhase) {
	case EApproachPhase::SearchRoute: {

		//---------------------------------
		//次に移動する場所を探索し、
		//戦闘行動フェーズを設定する
		//※プレイヤーが視界に捉えられて
		//  いない間に実行される
		//---------------------------------
		bool IsFinish = SearchRoute(TargetPosition);
		if (!IsFinish) {
			break;
		}

		// プレイヤーとの移動総距離に応じて処理を切り替える
		float TotalDistance = CheckTargetTotalDistance();
		UE_LOG(LogTemp, Display, TEXT("ESearchPhase::DecideAction Distance : %f"), TotalDistance)

		if (TotalDistance >= ApproachDistance) {

			ChangeCurrentApproachPhase(EApproachPhase::Return);
		}
		else {
			ACharacterBase::StopRunning();
			ACharacterBase::Aim();

			ChangeCurrentApproachPhase(EApproachPhase::Move);
		}
		break;
	}
	case EApproachPhase::Move: {

		Turn(DeltaTime, PathIndex);

		bool IsArrived = Move(DeltaTime);
		if (!IsArrived) {
			break;
		}

		ChangeCurrentApproachPhase(EApproachPhase::Return);
		break;
	}
	case EApproachPhase::Return: {

		return true;
	}
	default: {

		break;
	}
	}

	return false;
}

/// <summary>
/// プレイヤーの向きを変える
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <param name="Index"> パスインデックス </param>
/// <returns> true:処理終了 </returns>
bool AEnemy::Turn(float DeltaTime, int32 Index) {

	AEnemyAIController* EnemyController = GetEnemyController();
	if (!IsValid(EnemyController)) {
		return false;
	}
	FRotator ControllerRot = EnemyController->GetControlRotation();

	if (Index >= NavigationPath->PathPoints.Num()) {
		return false;
	}
	FVector TargetPos = NavigationPath->PathPoints[Index];
	FVector ActorPos = GetActorLocation();

	FRotator TargetRot = UKismetMathLibrary::FindLookAtRotation(ActorPos, TargetPos);
	FRotator NewRot = UKismetMathLibrary::RInterpTo(ControllerRot, TargetRot, DeltaTime, TurnValue);
	ControllerRot.Yaw = NewRot.Yaw;

	// 移動方向をコントローラとアクターにセット
	EnemyController->SetControlRotation(ControllerRot);
	SetActorRotation(ControllerRot);

	// 回転前後の差分が一定値に達したら処理終了とする
	float DifferValue = FMath::Abs(NewRot.Yaw - TargetRot.Yaw);
	if (DifferValue >= TurnedAllowableError) {
		return false;
	}

	return true;
}

/// <summary>
/// 移動行動
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns> true:移動完了 </returns>
bool AEnemy::Move(float DeltaTime) {

	bool IsCheck = IsArrivedPathPoint();
	if (IsCheck) {
		bool IsFinish = UpdatePathIndex();
		if (IsFinish) {
			return true;
		}
	}

	FVector ForwardVector = GetActorForwardVector();
	AddMovementInput(ForwardVector);

	return false;
}

/// <summary>
/// 移動地点までの経路探索
/// </summary>
/// <param name="Goal"> 移動地点 </param>
/// <returns> true:経路探索完了 </returns>
bool AEnemy::SearchRoute(FVector Goal) {

	PathIndex = MinIndex;

	//-------------------------------
	// 経路探索実施
	//-------------------------------
	NavigationPath = GetMoveRoute(Goal);
	if (!IsValid(NavigationPath)) {
		return false;
	}

	// 経路が1つ以上存在しない場合は処理失敗
	if (NavigationPath->PathPoints.Num() < One) {
		return false;
	}

	return true;
}

/// <summary>
/// 移動経路の取得
/// </summary>
/// <param name="Position"> 移動目標位置 </param>
/// <returns> 移動経路情報 </returns>
UNavigationPath* AEnemy::GetMoveRoute(FVector Position) {

	UNavigationSystemV1* NaviSystem = FNavigationSystem::GetCurrent<UNavigationSystemV1>(GetWorld());
	if (!IsValid(NaviSystem)) {
		return nullptr;
	}
	UNavigationPath* Path = NaviSystem->FindPathToLocationSynchronously(this, GetActorLocation(), Position);

	return Path;
}

/// <summary>
/// 移動経路の終端に到達したかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:到着 </returns>
bool AEnemy::IsArrivedPathPoint() {

	bool IsArrivedX = false;
	bool IsArrivedY = false;
	bool IsArrivedZ = false;

	FVector FootPos = GetSocketPosition(FootSocketName);

	if (FootPos.X >= NavigationPath->PathPoints[PathIndex].X - ArrivedAllowableError &&
		FootPos.X <= NavigationPath->PathPoints[PathIndex].X + ArrivedAllowableError) {
		IsArrivedX = true;
	}

	if (FootPos.Y >= NavigationPath->PathPoints[PathIndex].Y - ArrivedAllowableError &&
		FootPos.Y <= NavigationPath->PathPoints[PathIndex].Y + ArrivedAllowableError) {
		IsArrivedY = true;
	}

	if (FootPos.Z >= NavigationPath->PathPoints[PathIndex].Z - ArrivedAllowableError &&
		FootPos.Z <= NavigationPath->PathPoints[PathIndex].Z + ArrivedAllowableError) {
		IsArrivedZ = true;
	}

	if (IsArrivedX && IsArrivedY && IsArrivedZ) {
		return true;
	}

	return false;
}

/// <summary>
/// パスインデックスの更新
/// </summary>
/// <param name=""></param>
/// <returns> true:目的位置到着 </returns>
bool AEnemy::UpdatePathIndex() {

	++PathIndex;
	if (PathIndex >= NavigationPath->PathPoints.Num()) {
		PathIndex = MinIndex;
		return true;
	}

	return false;
}

/// <summary>
/// 移動先を探索する
/// </summary>
/// <param name=""></param>
/// <returns> true:探索終了 </returns>
bool AEnemy::SearchDestination() {

	switch (CurrentSearchPhase) {
	case ESearchPhase::None: {

		break;
	}
	case ESearchPhase::SearchChaseMove: {

		//------------------------------------------------
		// ターゲットまでの直線距離で探索方法を決める
		//------------------------------------------------
		float TotalDistance = CheckTargetLinearDistance(GetActorLocation(), TargetPosition);
		if (TotalDistance >= PatrolDistance) {
			ExecQuerySurroundNearPoint();
		}
		else {
			ExecQuerySurroundFarPoint();
		}

		ChangeCurrentSearchPhase(ESearchPhase::WaitSearch);
		break;
	}
	case ESearchPhase::SearchHideMove: {

		//------------------------------------------------
		// ターゲットから見えない位置を探索
		//------------------------------------------------
		ExecQueryHidePoint();

		ChangeCurrentSearchPhase(ESearchPhase::WaitSearch);
		break;
	}
	case ESearchPhase::WaitSearch: {

		if (!bFinishSearch) {
			break;
		}
		bFinishSearch = false;

		ChangeCurrentSearchPhase(ESearchPhase::DecideAction);
		break;
	}
	case ESearchPhase::DecideAction: {

		//------------------------------------------------
		// 目的地までの移動経路を探索、
		// 移動総距離から次の行動を決める
		//------------------------------------------------
		bool IsFinish = SearchRoute(MovePosition);
		if (!IsFinish) {
			break;
		}
		ChangeCurrentSearchPhase(ESearchPhase::Finish);
		break;
	}
	case ESearchPhase::Finish: {

		return true;
	}
	}

	return false;
}

/// <summary>
/// 環境クエリの種類のセット
/// </summary>
/// <param name="Type"> 環境クエリの種類 </param>
/// <returns></returns>
void AEnemy::SetEnvQueryType(EEnvQueryType Type) {

	EnvQueryType = Type;
}

/// <summary>
/// 環境クエリの種類の取得
/// </summary>
/// <param name=""></param>
/// <returns> 環境クエリの種類 </returns>
EEnvQueryType AEnemy::GetEnvQueryType() {

	return EnvQueryType;
}

/// <summary>
/// 環境クエリの実行
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemy::ExecQuerySurroundNearPoint() {

	bFinishSearch = false;

	if (!IsValid(SearchSurroundNearPoint)) {
		return;
	}

	SetEnvQueryType(EEnvQueryType::SurroundNear);

	FEnvQueryRequest QueryRequest = FEnvQueryRequest(SearchSurroundNearPoint, this);
	QueryRequest.SetFloatParam(FName(TEXT("OnCircle.CircleRadius")), CreateItemRadius);
	QueryRequest.SetFloatParam(FName(TEXT("OnCircle.SpaceBetween")), CreateItemSpace);
	QueryRequest.Execute(EEnvQueryRunMode::SingleResult, this, &AEnemy::RecieveEnvQueryResult);
}

/// <summary>
/// 環境クエリ(巡回位置)の実行
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemy::ExecQuerySurroundFarPoint() {

	bFinishSearch = false;

	if (!IsValid(SearchSurroundFarPoint)) {
		return;
	}

	SetEnvQueryType(EEnvQueryType::SurroundFar);

	FEnvQueryRequest QueryRequest = FEnvQueryRequest(SearchSurroundFarPoint, this);
	QueryRequest.SetFloatParam(FName(TEXT("OnCircle.CircleRadius")), CreateItemRadius);
	QueryRequest.SetFloatParam(FName(TEXT("OnCircle.SpaceBetween")), CreateItemSpace);
	QueryRequest.Execute(EEnvQueryRunMode::SingleResult, this, &AEnemy::RecieveEnvQueryResult);
}

/// <summary>
/// 環境クエリの実行 (プレイヤーから隠れる)
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemy::ExecQueryHidePoint() {

	bFinishSearch = false;

	if (!IsValid(SearchHidePoint)) {
		return;
	}

	SetEnvQueryType(EEnvQueryType::Hide);

	FEnvQueryRequest QueryRequest = FEnvQueryRequest(SearchHidePoint, this);
	QueryRequest.Execute(EEnvQueryRunMode::SingleResult, this, &AEnemy::RecieveEnvQueryResult);
}

/// <summary>
/// 環境クエリの結果受け取り
/// </summary>
/// <param name="Result"> クエリ実行結果 </param>
/// <returns></returns>
void AEnemy::RecieveEnvQueryResult(TSharedPtr<FEnvQueryResult> Result) {

	if (Result == nullptr || !Result->IsSuccsessful()) {
		return;
	}

	if (Result->Items.Num() <= 0) {
		return;
	}

	bFinishSearch = true;

	for (int32 i = 0; i < Result->Items.Num(); ++i) {
		FVector Location = Result->GetItemAsLocation(i);
		float Score = Result->GetItemScore(i);
		//UE_LOG(LogTemp, Display, TEXT("Score : %f"), Score)
	}

	EEnvQueryType QueryType = GetEnvQueryType();
	switch (QueryType) {
	case EEnvQueryType::SurroundFar: {

		for (int32 i = 0; i < Result->Items.Num(); ++i) {
			float Score = Result->GetItemScore(i);
			if (Score <= ZeroScore) {
				break;
			}
			MovePosition = Result->GetItemAsLocation(i);
		}
		break;
	}
	case EEnvQueryType::SurroundNear: {

		MovePosition = Result->GetItemAsLocation(MinIndex);
		break;
	}
	case EEnvQueryType::Hide: {

		MovePosition = Result->GetItemAsLocation(MinIndex);
		UE_LOG(LogTemp, Display, TEXT("MovePosition X:%f, Y:%f, Z:%f"), MovePosition.X, MovePosition.Y, MovePosition.Z)
		break;
	}
	}
}

/// <summary>
/// 射撃処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AEnemy::Shoot(float DeltaTime) {

	switch (CurrentShootPhase) {
	case EShootPhase::InitWait: {

		TurnPlayer();
		WaitTotalTime += DeltaTime;

		float WaitTimeValue = WaitNormalShootTimeValue;
		switch (EnemyType) {
		case EEnemyType::Sniper: {
			WaitTimeValue = WaitSniperShootTimeValue;
			break;
		}
		}
		if (WaitTotalTime < WaitTimeValue) {
			break;
		}
		WaitTotalTime = ResetTime;

		ChangeCurrentShootPhase(EShootPhase::StartShoot);
		break;
	}
	case EShootPhase::StartShoot: {

		ACharacterBase::Shoot();

		// 射撃を止める時間を設定
		StopShootTime = SingleShootTime;
		switch (EnemyType) {
		case EEnemyType::Normal: {
			StopShootTime = FMath::RandRange(ShootMinTime, ShootMaxTime);
			break;
		}
		}
		ShootTime = ResetTime;

		ChangeCurrentShootPhase(EShootPhase::Shoot);
		break;
	}
	case EShootPhase::Shoot: {

		// 射撃中、プレイヤーの方を向く
		TurnPlayer();

		ShootTime += DeltaTime;
		if (ShootTime < StopShootTime) {
			break;
		}
		ShootTime = ResetTime;

		ChangeCurrentShootPhase(EShootPhase::StopShoot);
		break;
	}
	case EShootPhase::StopShoot: {

		ACharacterBase::StopShooting();

		ChangeCurrentShootPhase(EShootPhase::None);
		break;
	}
	}
}

/// <summary>
/// プレイヤーの方へ向く
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemy::TurnPlayer() {

	AEnemyAIController* EnemyController = GetEnemyController();
	if (!IsValid(EnemyController)) {
		return;
	}
	FRotator ControllerRot = EnemyController->GetControlRotation();

	FVector ActorPos = GetActorLocation();
	FRotator TargetRot = UKismetMathLibrary::FindLookAtRotation(ActorPos, TargetPosition);
	TargetRot.Yaw -= ShootAdjustAngle;
	ControllerRot.Yaw = TargetRot.Yaw;

	// 移動方向をコントローラとアクターにセット
	EnemyController->SetControlRotation(ControllerRot);
	SetActorRotation(ControllerRot);
}

/// <summary>
/// ターゲットまでの移動総距離をチェック
/// </summary>
/// <param name=""></param>
/// <returns> ターゲットまでの移動総距離 </returns>
float AEnemy::CheckTargetTotalDistance() {

	float TotalDistance = 0.0f;

	for (int32 i = 0; i < NavigationPath->PathPoints.Num(); ++i) {

		if (i == MinIndex) {
			continue;
		}
		TotalDistance += (NavigationPath->PathPoints[i] - NavigationPath->PathPoints[i - 1]).Size();
	}

	return TotalDistance;
}

/// <summary>
/// ターゲットまでの直線距離をチェック
/// </summary>
/// <param name="Start"> 開始位置 </param>
/// <param name="Goal"> 終了位置 </param>
/// <returns> ターゲットまでの直線距離 </returns>
float AEnemy::CheckTargetLinearDistance(FVector Start, FVector Goal) {

	return UKismetMathLibrary::Vector_Distance(Goal, Start);
}

/// <summary>
/// 現在の警戒レベルを取得する
/// </summary>
/// <param name=""></param>
/// <returns> レベルの種類 </returns>
EAlertLevel AEnemy::GetCurrentAlertLevel() {

	return CurrentAlertLevel;
}

/// <summary>
/// 経路探索を再度実行する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemy::ResetSearchingRoute() {

	switch (CurrentAlertLevel) {
	case EAlertLevel::Fight: {

		bResetSearchRoute = true;
		break;
	}
	case EAlertLevel::Beware: {

		if (BewareActionType != EBewareActionType::Approach) {
			break;
		}
		bResetSearchRoute = true;
		break;
	}
	}
}

/// <summary>
/// 警戒ランプで異変を感じた
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemy::NotifiedBeware() {

	ChangeCurrentPatrolPhase(EPatrolPhase::NotifyWait);
}

/// <summary>
/// パトロール中かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:パトロール中 </returns>
bool AEnemy::IsPatrol() {

	return CurrentAlertLevel == EAlertLevel::Patrol;
}

/// <summary>
/// 戦闘中かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:戦闘中 </returns>
bool AEnemy::IsFight() {

	return CurrentAlertLevel == EAlertLevel::Fight;
}

/// <summary>
/// 警戒中行動指示を設定する
/// </summary>
/// <param name="Type"> 警戒中行動の種類 </param>
/// <returns></returns>
void AEnemy::ChangeBewareActionType(EBewareActionType Type) {

	BewareActionType = Type;

	switch (Type) {
	case EBewareActionType::Approach: {

		ChangeCurrentBewarePhase(EBewarePhase::Chase);
		ChangeCurrentChasePhase(EChasePhase::SearchPosition);
		ChangeCurrentSearchPhase(ESearchPhase::SearchChaseMove);
		break;
	}
	default: {

		ChangeCurrentBewarePhase(EBewarePhase::Hide);
		ChangeCurrentHidePhase(EHidePhase::SearchPosition);
		ChangeCurrentSearchPhase(ESearchPhase::SearchHideMove);
		break;
	}
	}
}

/// <summary>
/// 現在の警戒レベルを「巡回」に変更する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemy::OnSwitchPatrolLevel() {

	//----------------------------------------
	//警戒レベルを変更し、プレイヤー発見度を
	//リセットしておく
	//----------------------------------------
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, TEXT("警戒レベル：巡回"));

	bPressCrouchButton = false;
	FoundGrade = ResetFoundGradeValue;
	FoundGradeResetTime = ResetTime;
	WaitTotalTime = ResetTime;
	CurrentAlertLevel = EAlertLevel::Patrol;

	ChangeCurrentFightPhase(EFightPhase::None);
	ChangeCurrentShootPhase(EShootPhase::None);

	ChangeCurrentBewarePhase(EBewarePhase::None);
	ChangeCurrentPatrolPhase(EPatrolPhase::NonCombat);
	ChangeCurrentChasePhase(EChasePhase::None);
	ChangeCurrentHidePhase(EHidePhase::None);
	ChangeCurrentSearchPhase(ESearchPhase::None);

	GetWorld()->GetTimerManager().ClearTimer(LevelSwitchTimerHandle);
}

/// <summary>
/// 現在の警戒レベルを「警戒」に変更する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemy::OnSwitchBewareLevel() {

	//----------------------------------------
	//警戒レベルを変更し、巡回レベルへ
	//遷移するためのタイマーをセットする
	//----------------------------------------
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, TEXT("警戒レベル：警戒"));
	GetWorld()->GetTimerManager().ClearTimer(LevelSwitchTimerHandle);

	bPressCrouchButton = false;
	ChangeCurrentFightPhase(EFightPhase::None);
	ChangeCurrentShootPhase(EShootPhase::None);

	switch (CurrentAlertLevel) {
	case EAlertLevel::Fight: {
		ChangeBewareActionType(EBewareActionType::Approach);
		break;
	}
	default: {
		ChangeBewareActionType(EBewareActionType::Hide);
		break;
	}
	}
	CurrentAlertLevel = EAlertLevel::Beware;
}

/// <summary>
/// 現在の警戒レベルを「戦闘」に変更する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemy::SwitchFightLevel() {

	//----------------------------------------
	//警戒レベルを変更し、警戒レベルへ
	//遷移するためのタイマーをセットする
	//----------------------------------------
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, TEXT("警戒レベル：戦闘"));

	CurrentAlertLevel = EAlertLevel::Fight;
	bPressCrouchButton = false;
	GetWorld()->GetTimerManager().ClearTimer(LevelSwitchTimerHandle);
	GetWorld()->GetTimerManager().SetTimer(LevelSwitchTimerHandle, this, &AEnemy::OnSwitchBewareLevel, CancelFightTime, true);

	if (CurrentFightPhase != EFightPhase::None) {
		return;
	}
	ChangeCurrentFightPhase(EFightPhase::Chase);
	ChangeCurrentShootPhase(EShootPhase::None);
	ChangeCurrentChasePhase(EChasePhase::SearchPosition);
	ChangeCurrentSearchPhase(ESearchPhase::SearchChaseMove);
}

/// <summary>
/// 撃たれた方向を向く
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemy::TurnTowardsShooted() {

	AEnemyAIController* EnemyController = GetEnemyController();
	if (!IsValid(EnemyController)) {
		return;
	}
	FRotator ControllerRot = EnemyController->GetControlRotation();
	FRotator EnemyRot = GetActorRotation();
	ControllerRot.Yaw -= HitRotYaw;

	// 移動方向をコントローラとアクターにセット
	EnemyController->SetControlRotation(ControllerRot);
	SetActorRotation(ControllerRot);
}

/// <summary>
/// 現在の巡回フェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void AEnemy::ChangeCurrentPatrolPhase(EPatrolPhase PhaseType) {

	CurrentPatrolPhase = PhaseType;
}

/// <summary>
/// 現在の警戒フェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void AEnemy::ChangeCurrentBewarePhase(EBewarePhase PhaseType) {

	CurrentBewarePhase = PhaseType;
}

/// <summary>
/// 現在の戦闘フェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void AEnemy::ChangeCurrentFightPhase(EFightPhase PhaseType) {

	CurrentFightPhase = PhaseType;
}

/// <summary>
/// 現在の追跡フェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void AEnemy::ChangeCurrentChasePhase(EChasePhase PhaseType) {

	CurrentChasePhase = PhaseType;
}

/// <summary>
/// 現在の隠れるフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void AEnemy::ChangeCurrentHidePhase(EHidePhase PhaseType) {

	CurrentHidePhase = PhaseType;
}

/// <summary>
/// 現在の接近フェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void AEnemy::ChangeCurrentApproachPhase(EApproachPhase PhaseType) {

	CurrentApproachPhase = PhaseType;
}

/// <summary>
/// 現在の射撃フェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void AEnemy::ChangeCurrentShootPhase(EShootPhase PhaseType) {

	CurrentShootPhase = PhaseType;
}

/// <summary>
/// 現在の探索フェーズを変更する
/// </summary>
/// <param name="Type"> 探索の種類 </param>
/// <returns></returns>
void AEnemy::ChangeCurrentSearchPhase(ESearchPhase Type) {

	CurrentSearchPhase = Type;
}

/// <summary>
/// 撃たれた時の処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemy::DisplayShootedWidget() {

	// キャラクターに対する角度から方角を取得
	EDirectionType DirectionType = SearchDirection(HitRotYaw);

	// ウィジェットの表示
	ABattleController* BattleController = Cast<ABattleController>(UGameplayStatics::GetPlayerController(this, 0));
	if (!IsValid(BattleController)) {
		return;
	}
	BattleController->GetShootedWidget()->PlayShootedAnimation(DirectionType);
}