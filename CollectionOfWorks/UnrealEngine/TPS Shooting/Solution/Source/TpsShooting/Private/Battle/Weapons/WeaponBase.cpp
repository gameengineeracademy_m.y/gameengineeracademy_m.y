﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "Battle/Weapons/WeaponBase.h"
#include "Battle/Character/CharacterBase.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/SkeletalMeshComponent.h"
#include "Battle/Controller/BattleController.h"
#include "DrawDebugHelpers.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AWeaponBase::AWeaponBase()
	: WeaponData()
	, WeaponSocketData()
	, WeaponSoundData()
	, WeaponEffectData()
	, AimCameraData()
	, WeaponAnimList()
	, bReload(false)
	, WeaponType(EWeaponType::None)
	, WeaponDataTable(nullptr)
	, WeaponSocketTable(nullptr)
	, WeaponSoundTable(nullptr)
	, WeaponEffectTable(nullptr)
	, ImpactSoundTable(nullptr)
	, ImpactEffectTable(nullptr)
	, AimCameraTable(nullptr)
	, AnimationTable(nullptr)
	, AimDistance(0.0f)
	, ShootCameraShakeClass()
	, Owner(nullptr)
	, ShootSound()
	, MuzzleEffect()
	, CurrentPhase(EWeaponPhase::Wait)
	, AccumulateTime(0.0f)
	, LoadedAmmos(0)
	, PossessAmmos(0) {

 	// Tick処理を有効化
	PrimaryActorTick.bCanEverTick = true;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	WeaponMesh->SetupAttachment(RootComponent);
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::BeginPlay() {

	Super::BeginPlay();

	WeaponType = EWeaponType::None;
	CurrentPhase = EWeaponPhase::Wait;

	bReload = false;
	AccumulateTime = ResetTime;

	// 射撃時の画面揺らしクラス生成
	CreateShootCameraShake();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AWeaponBase::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

	// 指定時間後、射撃可能にする
	PrepareShoot(DeltaTime);

	switch (WeaponData.ShootType) {
	case EShootType::Single: {

		ShootSingle();
		break;
	}
	case EShootType::Continuous: {

		ShootContinuous();
		break;
	}
	}
}

/// <summary>
/// 連続射撃処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::ShootContinuous() {

	switch (CurrentPhase) {
	case EWeaponPhase::Wait: {
		break;
	}
	case EWeaponPhase::StartShoot: {

		// 装填された弾が無くなったらリロードする
		if (!IsExistsLoadedAmmos()) {
			if (!IsExistsPossessAmmos()) {
				PlaySound(WeaponSoundData.EmptyShootSound);
				ChangeCurrentWeaponPhase(EWeaponPhase::Wait);
				break;
			}
			StopForciblyFire();
			StartReload();
			break;
		}

		//--------------------------------
		//射撃音を鳴らし始める
		//その後、射撃処理に遷移する
		//--------------------------------
		StartSoundShoot();
		StartMuzzleEffect();

		ChangeCurrentWeaponPhase(EWeaponPhase::Shoot);
		break;
	}
	case EWeaponPhase::Shoot: {

		//--------------------------------
		//指定時間置きにレイを用いた
		//射撃処理実行
		//--------------------------------

		if (!IsShootable()) {
			break;
		}

		// 装填された弾が無くなったらリロードする
		if (!IsExistsLoadedAmmos()) {
			if (!IsExistsPossessAmmos()) {
				StopForciblyFire();
				ChangeCurrentWeaponPhase(EWeaponPhase::Wait);
				break;
			}
			StopForciblyFire();
			StartReload();
			break;
		}

		Shoot();
		PlayWeaponAnimation(CurrentPhase);
		break;
	}
	case EWeaponPhase::StopShoot: {

		//--------------------------------
		//射撃音とエフェクトを止める
		//射撃終了音を鳴らす
		//その後、待機処理に遷移
		//※射撃処理をすぐ開始できるように
		//累積時間を調整しておく
		//--------------------------------
		StopSoundShoot();
		StopMuzzleEffect();

		// 装填弾薬または所持弾薬があるときのみサウンドを鳴らす
		if (IsExistsLoadedAmmos() || IsExistsPossessAmmos()) {
			EndSoundShoot();
		}

		bShootable = true;
		AccumulateTime = ResetTime;
		ChangeCurrentWeaponPhase(EWeaponPhase::Wait);
		break;
	}
	case EWeaponPhase::Reload: {

		if (bReload) {
			break;
		}
		Reload();

		AccumulateTime = WeaponData.ShootDuration;
		ChangeCurrentWeaponPhase(EWeaponPhase::Wait);
		break;
	}
	}
}

/// <summary>
/// 単発射撃処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::ShootSingle() {

	switch (CurrentPhase) {
	case EWeaponPhase::Wait: {
		break;
	}
	case EWeaponPhase::StartShoot: {

		ChangeCurrentWeaponPhase(EWeaponPhase::Shoot);
		break;
	}
	case EWeaponPhase::Shoot: {

		if (!IsShootable()) {
			break;
		}

		// 装填された弾が無くなったらリロードする
		if (!IsExistsLoadedAmmos()) {
			if (!IsExistsPossessAmmos()) {
				StopForciblyFire();
				PlaySound(WeaponSoundData.EmptyShootSound);
				ChangeCurrentWeaponPhase(EWeaponPhase::Wait);
				break;
			}
			StopForciblyFire();
			StartReload();
			break;
		}

		PlayWeaponAnimation(CurrentPhase);
		StartSoundShoot();
		StartMuzzleEffect();
		Shoot();

		ChangeCurrentWeaponPhase(EWeaponPhase::Wait);
		break;
	}
	case EWeaponPhase::StopShoot: {

		//--------------------------------
		//射撃音とエフェクトを止める
		//その後、待機処理に遷移
		//--------------------------------
		StopSoundShoot();
		StopMuzzleEffect();

		ChangeCurrentWeaponPhase(EWeaponPhase::Wait);
		break;
	}
	case EWeaponPhase::Reload: {

		//---------------------------------------
		//リロードフラグが立っている時は
		//リロードアニメーション中
		//アニメーション終了後にフラグが降りる
		//---------------------------------------
		if (bReload) {
			break;
		}

		ChangeCurrentWeaponPhase(EWeaponPhase::Wait);
		break;
	}
	}
}

/// <summary>
/// ウェポン情報の設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::SetupWeaponData() {

	//------------------------------------
	//データテーブルから対象データを取得
	//------------------------------------
	if (!IsValid(WeaponDataTable) || !IsValid(WeaponSocketTable) ||
		!IsValid(WeaponSoundTable) || !IsValid(WeaponEffectTable) ||
		!IsValid(ImpactSoundTable) || !IsValid(ImpactEffectTable) ||
		!IsValid(AnimationTable)) {
		return;
	}

	// ウェポン情報
	TArray<FName> RowNames = WeaponDataTable->GetRowNames();
	for (FName RowName : RowNames) {
		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		// ウェポンの種類が一致しないなら次の行へ
		FWeaponData* WeaponRowData = WeaponDataTable->FindRow<FWeaponData>(RowName, "");
		if (WeaponRowData->WeaponType != WeaponType) {
			continue;
		}
		WeaponData.WeaponType = WeaponRowData->WeaponType;
		WeaponData.ShootType = WeaponRowData->ShootType;
		WeaponData.Power = WeaponRowData->Power;
		WeaponData.ShootDuration = WeaponRowData->ShootDuration;
		WeaponData.MaxLoadedAmmos = WeaponRowData->MaxLoadedAmmos;
		WeaponData.MaxPossessAmmos = WeaponRowData->MaxPossessAmmos;
		WeaponData.FOV = WeaponRowData->FOV;

		AccumulateTime = WeaponData.ShootDuration;
		break;
	}

	// ウェポンのソケット情報
	RowNames = WeaponSocketTable->GetRowNames();
	for (FName RowName : RowNames) {
		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		// ウェポンの種類が一致しないなら次の行へ
		FWeaponSocketData* WeaponSocket = WeaponSocketTable->FindRow<FWeaponSocketData>(RowName, "");
		if (WeaponSocket->WeaponType != WeaponType) {
			continue;
		}
		WeaponSocketData.WeaponType = WeaponSocket->WeaponType;
		WeaponSocketData.MuzzleSocketName = WeaponSocket->MuzzleSocketName;
		WeaponSocketData.AmmoEjectSocketName = WeaponSocket->AmmoEjectSocketName;
		break;
	}

	// ウェポンのサウンド情報
	RowNames = WeaponSoundTable->GetRowNames();
	for (FName RowName : RowNames) {
		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		// ウェポンの種類が一致しないなら次の行へ
		FWeaponSoundData* WeaponSound = WeaponSoundTable->FindRow<FWeaponSoundData>(RowName, "");
		if (WeaponSound->WeaponType != WeaponType) {
			continue;
		}
		WeaponSoundData.WeaponType = WeaponSound->WeaponType;
		WeaponSoundData.ShootSound = WeaponSound->ShootSound;
		WeaponSoundData.EndShootSound = WeaponSound->EndShootSound;
		WeaponSoundData.EmptyShootSound = WeaponSound->EmptyShootSound;
		WeaponSoundData.ReloadRemoveSound = WeaponSound->ReloadRemoveSound;
		WeaponSoundData.ReloadInsertSound = WeaponSound->ReloadInsertSound;
		break;
	}

	// ウェポンのエフェクト情報
	RowNames = WeaponEffectTable->GetRowNames();
	for (FName RowName : RowNames) {
		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		// ウェポンの種類が一致しないなら次の行へ
		FWeaponEffectData* WeaponEffect = WeaponEffectTable->FindRow<FWeaponEffectData>(RowName, "");
		if (WeaponEffect->WeaponType != WeaponType) {
			continue;
		}
		WeaponEffectData.WeaponType = WeaponEffect->WeaponType;
		WeaponEffectData.MuzzleFlash = WeaponEffect->MuzzleFlash;
		WeaponEffectData.ShootTrail = WeaponEffect->ShootTrail;
		break;
	}

	// 着弾サウンド情報
	RowNames = ImpactSoundTable->GetRowNames();
	for (FName RowName : RowNames) {
		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		// ウェポンの種類が一致しないなら次の行へ
		FImpactSoundData* WeaponSound = ImpactSoundTable->FindRow<FImpactSoundData>(RowName, "");
		if (WeaponSound->WeaponType != WeaponType) {
			continue;
		}
		ImpactSoundData.WeaponType = WeaponSound->WeaponType;
		ImpactSoundData.BloodSplatter = WeaponSound->BloodSplatter;
		ImpactSoundData.Repelled = WeaponSound->Repelled;
		ImpactSoundData.Critical = WeaponSound->Critical;
		break;
	}

	// 着弾エフェクト情報
	RowNames = ImpactEffectTable->GetRowNames();
	for (FName RowName : RowNames) {
		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		// ウェポンの種類が一致しないなら次の行へ
		FImpactEffectData* ImpactEffect = ImpactEffectTable->FindRow<FImpactEffectData>(RowName, "");
		if (ImpactEffect->WeaponType != WeaponType) {
			continue;
		}
		ImpactEffectData.WeaponType = ImpactEffect->WeaponType;
		ImpactEffectData.BloodSplatter = ImpactEffect->BloodSplatter;
		ImpactEffectData.Repelled = ImpactEffect->Repelled;
		break;
	}

	// アニメーション情報
	RowNames = AnimationTable->GetRowNames();
	for (FName RowName : RowNames) {
		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		// ウェポンの種類が一致しないなら次の行へ
		FWeaponAnimData* WeaponAnim = AnimationTable->FindRow<FWeaponAnimData>(RowName, "");
		if (WeaponAnim->WeaponType != WeaponType) {
			continue;
		}
		// 既に登録済みなら次の行へ
		if (WeaponAnimList.Contains(WeaponAnim->WeaponPhase)) {
			continue;
		}
		WeaponAnimList.Add(WeaponAnim->WeaponPhase, WeaponAnim->AnimSequence);
	}
}

/// <summary>
/// カメラ情報の設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::SetupAimCameraData() {

	//------------------------------------
	//データテーブルから対象データを取得
	//------------------------------------
	if (!IsValid(AimCameraTable)) {
		return;
	}

	// ウェポン情報
	TArray<FName> RowNames = AimCameraTable->GetRowNames();
	for (FName RowName : RowNames) {
		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		// ウェポンの種類が一致しないなら次の行へ
		FAimCameraData* AimCameraRowData = AimCameraTable->FindRow<FAimCameraData>(RowName, "");
		if (AimCameraRowData->WeaponType != WeaponType) {
			continue;
		}
		AimCameraData.WeaponType = AimCameraRowData->WeaponType;
		AimCameraData.PlayerDistance = AimCameraRowData->PlayerDistance;
		AimCameraData.HorizontalPos = AimCameraRowData->HorizontalPos;
		AimCameraData.FOV = AimCameraRowData->FOV;
		break;
	}
}

/// <summary>
/// ウェポンの種類のセット
/// </summary>
/// <param name="Type"> ウェポンの種類 </param>
/// <returns></returns>
void AWeaponBase::SetWeaponType(EWeaponType Type) {

	WeaponType = Type;
}

/// <summary>
/// ウェポンの種類の取得
/// </summary>
/// <param name=""></param>
/// <returns> ウェポンの種類 </returns>
EWeaponType AWeaponBase::GetWeaponType() {

	return WeaponType;
}

/// <summary>
/// 所有者のセット
/// </summary>
/// <param name="Character"> 所有者 </param>
/// <returns></returns>
void AWeaponBase::SetCharaOwner(ACharacterBase* Character) {

	Owner = Character;
}

/// <summary>
/// 所有者の取得
/// </summary>
/// <param name=""></param>
/// <returns> 所有者 </returns>
ACharacterBase* AWeaponBase::GetCharaOwner() {

	return Owner;
}

/// <summary>
/// 発砲する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::Fire() {

	ChangeCurrentWeaponPhase(EWeaponPhase::StartShoot);
}

/// <summary>
/// 発砲をやめる
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::StopFire() {

	ChangeCurrentWeaponPhase(EWeaponPhase::StopShoot);
}

/// <summary>
/// リロード処理開始
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::StartReload() {

	bReload = true;
	ChangeCurrentWeaponPhase(EWeaponPhase::Reload);
}

/// <summary>
/// リロード終了
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::FinishReload() {

	bReload = false;
	Reload();
}

/// <summary>
/// 装填されている弾を減らす
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::DecreaseAmmo() {

	--LoadedAmmos;
}

/// <summary>
/// リロード処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::Reload() {

	// 装填する弾数を算出し、装填弾数に加算
	// 所持弾数 < 装填する弾数 の場合は
	// 装填する弾数を所持弾数で再セットする
	int32 AddAmmos = WeaponData.MaxLoadedAmmos - LoadedAmmos;
	if (PossessAmmos < AddAmmos) {
		AddAmmos = PossessAmmos;
		PossessAmmos = Zero;
	}
	else {
		PossessAmmos = PossessAmmos - AddAmmos;
	}
	LoadedAmmos += AddAmmos;
}

/// <summary>
/// リロードが必要かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:必要 </returns>
bool AWeaponBase::IsReload() {

	return bReload;
}

/// <summary>
/// 装填された弾薬の数を取得
/// </summary>
/// <param name=""></param>
/// <returns> 装填された弾薬の数 </returns>
int32 AWeaponBase::GetLoadedAmmos() {

	return LoadedAmmos;
}

/// <summary>
/// 所持弾薬数の取得
/// </summary>
/// <param name=""></param>
/// <returns> 所持弾薬数 </returns>
int32 AWeaponBase::GetPossessAmmos() {

	return PossessAmmos;
}

/// <summary>
/// 装填された弾があるかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:ある </returns>
bool AWeaponBase::IsExistsLoadedAmmos() {

	return LoadedAmmos > Zero;
}

/// <summary>
/// 所持弾数があるかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:ある </returns>
bool AWeaponBase::IsExistsPossessAmmos() {

	return PossessAmmos > Zero;
}

/// <summary>
/// 装填された弾薬が最大かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:ある </returns>
bool AWeaponBase::IsExistsMaxLoadedAmmos() {

	return LoadedAmmos == WeaponData.MaxLoadedAmmos;
}

/// <summary>
/// 所持弾薬数が最大かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:ある </returns>
bool AWeaponBase::IsExistsMaxPossessAmmos() {

	return PossessAmmos == WeaponData.MaxPossessAmmos;
}

/// <summary>
/// 強制的に射撃を止める
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::StopForciblyFire() {

	StopSoundShoot();
	StopMuzzleEffect();

	bShootable = true;
	AccumulateTime = ResetTime;

	switch (WeaponData.ShootType) {
	case EShootType::Single: {

		break;
	}
	case EShootType::Continuous: {
		EndSoundShoot();
		break;
	}
	}
}

/// <summary>
/// 所持弾薬を上限までセットする
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::SetMaxPossessAmmos() {

	PossessAmmos = WeaponData.MaxPossessAmmos;
}

/// <summary>
/// 所持弾薬を上限 + 未装填分セットする
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::SetupExceedMaxPossessAmmos() {

	SetMaxPossessAmmos();

	int32 AddAmmos = WeaponData.MaxLoadedAmmos - LoadedAmmos;
	PossessAmmos += AddAmmos;
}

/// <summary>
/// 装填弾薬を上限までセットする
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::SetMaxLoadedAmmos() {

	LoadedAmmos = WeaponData.MaxLoadedAmmos;
}

/// <summary>
/// 所持弾薬が上限かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:上限 </returns>
bool AWeaponBase::IsExistsMaxAmmos() {

	if (PossessAmmos + LoadedAmmos < WeaponData.MaxPossessAmmos + WeaponData.MaxLoadedAmmos) {
		return false;
	}
	return true;
}

/// <summary>
/// 射撃処理を中止する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::CancelShooting() {

	StopSoundShoot();
	StopMuzzleEffect();
}

/// <summary>
/// 射撃時のカメラ揺らしクラスの生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::CreateShootCameraShake() {

	// 射撃時のカメラ揺らしクラスの生成
	FString CameraShakePath;
	GConfig->GetString(TEXT("CameraSetting"), TEXT("ShootCameraShakePath"), CameraShakePath, GGameIni);

	ShootCameraShakeClass = TSoftClassPtr<UShootCameraShake>(FSoftObjectPath(*CameraShakePath)).LoadSynchronous();
	if (!IsValid(ShootCameraShakeClass)) {
		return;
	}
}

/// <summary>
/// 射撃サウンドの再生
/// </summary>
/// <param name="SoundCue"> サウンドキュー </param>
/// <returns> サウンド </returns>
UAudioComponent* AWeaponBase::PlayShootSound(USoundCue* SoundCue) {

	if (!IsValid(SoundCue)) {
		return nullptr;
	}
	UAudioComponent* Sound;
	Sound = UGameplayStatics::SpawnSoundAttached(SoundCue, WeaponMesh, WeaponSocketData.MuzzleSocketName);

	// 単発のサウンドは管理せずに自動破棄に一任する
	if (WeaponData.ShootType == EShootType::Single) {
		return nullptr;
	}
	
	return Sound;
}

/// <summary>
/// 射撃サウンドを開始する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::StartSoundShoot() {

	ShootSound.Add(PlayShootSound(WeaponSoundData.ShootSound));
}

/// <summary>
/// 射撃終了サウンドを鳴らす
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::EndSoundShoot() {

	ShootSound.Add(PlayShootSound(WeaponSoundData.EndShootSound));
}

/// <summary>
/// 射撃終了サウンドの再生
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::StopSoundShoot() {

	switch (WeaponData.ShootType) {
	case EShootType::Continuous: {

		for (UAudioComponent* Sound : ShootSound) {
			if (!IsValid(Sound)) {
				continue;
			}
			Sound->FadeOut(FadeOutDuration, FadeOutVolumeLevel);
			Sound = nullptr;
		}
		ShootSound.Empty();
		break;
	}
	}
}

/// <summary>
/// サウンドの再生
/// </summary>
/// <param name="SoundCue"> サウンドキュー </param>
/// <returns></returns>
void AWeaponBase::PlaySound(USoundCue* SoundCue) {

	if (!IsValid(SoundCue)) {
		return;
	}
	UGameplayStatics::PlaySound2D(GetWorld(), SoundCue);
}

/// <summary>
/// マズルフラッシュ実行
/// </summary>
/// <param name=""> エフェクト </param>
/// <returns></returns>
void AWeaponBase::StartMuzzleEffect() {

	MuzzleEffect.Add(UGameplayStatics::SpawnEmitterAttached(WeaponEffectData.MuzzleFlash, WeaponMesh, WeaponSocketData.MuzzleSocketName));
}

/// <summary>
/// マズルフラッシュ停止
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::StopMuzzleEffect() {

	for (UParticleSystemComponent* Effect : MuzzleEffect) {
		if (!IsValid(Effect)) {
			continue;
		}
		Effect->DeactivateSystem();
		Effect = nullptr;
	}
	MuzzleEffect.Empty();
}

/// <summary>
/// アニメーションの実行
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void AWeaponBase::PlayWeaponAnimation(EWeaponPhase PhaseType) {

	if (!WeaponAnimList.Contains(PhaseType)) {
		return;
	}
	if (!IsValid(WeaponAnimList[PhaseType])) {
		return;
	}

	WeaponMesh->PlayAnimation(WeaponAnimList[PhaseType], false);
}

/// <summary>
/// アニメーションの停止
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::ResetWeaponAnimation() {

	// アニメーションをメッシュから取り外すことで
	// アニメーションをキャンセルする
	WeaponMesh->SetAnimation(nullptr);
}

/// <summary>
/// 弾の軌跡表示
/// </summary>
/// <param name="StartPos"> 開始地点 </param>
/// <param name="EndPos"> 終端地点 </param>
/// <returns></returns>
void AWeaponBase::ShowShootTrail(FVector StartPos, FVector EndPos) {

	UParticleSystemComponent* ShowTrailEffect = UGameplayStatics::SpawnEmitterAttached(WeaponEffectData.ShootTrail, WeaponMesh, WeaponSocketData.MuzzleSocketName);
	if (!IsValid(ShowTrailEffect)) {
		return;
	}
	ShowTrailEffect->SetVectorParameter(TrailTargetParam, EndPos);
}

/// <summary>
/// 着弾処理実行
/// </summary>
/// <param name="Hit"> ヒット情報 </param>
/// <returns></returns>
void AWeaponBase::ImpactEffectAndSound(FHitResult Hit) {

	UParticleSystem* Effect = nullptr;
	USoundCue* Sound = nullptr;
	USoundCue* CriticalSound = nullptr;
	FRotator EffectRot = FRotator::ZeroRotator;
	FVector EffectScale = EffectDefaultScale;

	ACharacterBase* Character = Cast<ACharacterBase>(Hit.GetActor());
	if (!IsValid(Character)) {
		//-----------------------------
		// キャラクター以外
		//-----------------------------
		Sound = ImpactSoundData.Repelled;
		Effect = ImpactEffectData.Repelled;
	}
	else {
		//-----------------------------
		// キャラクター
		//-----------------------------
		Sound = ImpactSoundData.BloodSplatter;
		Effect = ImpactEffectData.BloodSplatter;

		EBodyPartsType PartsType = Character->GetBodyPartsType(Hit.BoneName);
		switch (PartsType) {
		case EBodyPartsType::Head: {

			EffectScale = EffectBigScale;

			// 対象キャラが生存中ならクリティカル音実行
			if (!Character->IsFine()) {
				break;
			}
			CriticalSound = ImpactSoundData.Critical;
			break;
		}
		case EBodyPartsType::Body: {

			break;
		}
		case EBodyPartsType::Arm: {

			EffectScale = EffectSmallScale;
			break;
		}
		case EBodyPartsType::Foot: {

			EffectScale = EffectSmallScale;
			break;
		}
		}
	}

	if (IsValid(Sound)) {
		UGameplayStatics::SpawnSoundAtLocation(Owner, Sound, Owner->GetActorLocation());
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), Sound, Hit.Location);
	}
	if (IsValid(Effect)) {
		UGameplayStatics::SpawnEmitterAtLocation(Owner, Effect, Hit.Location, EffectRot, EffectScale);
	}
	if (IsValid(CriticalSound)) {
		UGameplayStatics::SpawnSoundAtLocation(Owner, CriticalSound, Owner->GetActorLocation());
	}
}

/// <summary>
/// 射撃処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeaponBase::Shoot() {

	// 射撃不可能にしておく
	bShootable = false;
	// 装填されている弾を減らす
	DecreaseAmmo();

	//------------------------------------------
	//弾の開始位置と終了位置を算出
	//レイを出し、当たったアクターを出力する
	//------------------------------------------
	if (!IsValid(Owner)) {
		UE_LOG(LogTemp, Display, TEXT("Owner None"))
		return;
	}
	// レイによるコリジョンシミュレーション
	FVector MuzzlePos = WeaponMesh->GetSocketLocation(WeaponSocketData.MuzzleSocketName);
	FVector CameraPos = Owner->GetCamera()->GetComponentLocation();
	FRotator CameraRot = Owner->GetCamera()->GetComponentRotation();
	FVector EndPos = UKismetMathLibrary::GetForwardVector(CameraRot) * AimDistance;

	FHitResult HitData = SimulateRay(CameraPos, EndPos);
	if (!IsValid(HitData.GetActor())) {
		// 射撃トレースエフェクト
		ShowShootTrail(MuzzlePos, EndPos);
		return;
	}
	// 銃弾の軌跡と着弾エフェクト処理
	//DrawDebugLine(GetWorld(), MuzzlePos, HitData.Location, FColor::Red, true, 3.0f);
	ShowShootTrail(MuzzlePos, HitData.Location);
	ImpactEffectAndSound(HitData);
	CalcDamage(HitData, MuzzlePos);

	ECharacterType CharaType = Owner->GetCharaType();
	if (CharaType != ECharacterType::Player) {
		return;
	}
	// 画面を揺らす処理
	ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(BattleController)) {
		return;
	}
	BattleController->ClientStartCameraShake(ShootCameraShakeClass);
}

/// <summary>
/// 射撃できるかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:射撃可能 </returns>
bool AWeaponBase::IsShootable() {

	return bShootable;
}

/// <summary>
/// 射撃準備時間
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AWeaponBase::PrepareShoot(float DeltaTime) {

	if (bShootable) {
		return;
	}

	AccumulateTime += DeltaTime;
	if (AccumulateTime < WeaponData.ShootDuration) {
		return;
	}
	AccumulateTime = ResetTime;

	bShootable = true;
}

/// <summary>
/// シミュレーション
/// </summary>
/// <param name="StartPos"> 開始地点 </param>
/// <param name="EndPos"> 終端地点 </param>
/// <returns></returns>
FHitResult AWeaponBase::SimulateRay(FVector StartPos, FVector EndPos) {

	FHitResult HitResult;
	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(Owner);
	GetWorld()->LineTraceSingleByChannel(HitResult, StartPos, EndPos, ECollisionChannel::ECC_Visibility, QueryParams);

	return HitResult;
}

/// <summary>
/// ダメージ計算処理
/// </summary>
/// <param name="Hit"> ヒット情報 </param>
/// <param name="MuzzlePos"> 銃口の位置 </param>
/// <returns></returns>
void AWeaponBase::CalcDamage(FHitResult Hit, FVector MuzzlePos) {

	// キャラクター以外なら処理終了
	ACharacterBase* Character = Cast<ACharacterBase>(Hit.GetActor());
	if (!IsValid(Character)) {
		return;
	}
	
	// ヒット部位、ダメージ量をキャラクターに渡す
	EBodyPartsType PartsType = Character->GetBodyPartsType(Hit.BoneName);
	Character->CalcHitPoint(PartsType, WeaponData.Power);
	// 撃たれたリアクション実行
	Character->SetDamageReaction();
	Character->ExecuteAnimMontage(ECharaAnimType::RecieveDamage);
	Character->CancelReload();
	
	// 弾のベクトルをキャラクターに渡し、撃たれた方角を調べる
	FVector NormVector = (Hit.ImpactPoint - MuzzlePos).GetSafeNormal();
	Character->SetHitRotYaw(Character->CalcAngleToAim(NormVector, Character));
}

/// <summary>
/// 現在のウェポンフェーズを変更する
/// </summary>
/// <param name="Phase"> ウェポンフェーズ </param>
/// <returns></returns>
void AWeaponBase::ChangeCurrentWeaponPhase(EWeaponPhase Phase) {

	CurrentPhase = Phase;
}