﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/DataTable.h"
#include "Battle/WeatherSystem/Weather.h"
#include "../Public/TpsShootingGameInstance.h"
#include "WeatherManager.generated.h"

//----------------------------------
//前方宣言
//----------------------------------
class UBattleEvent;

/// <summary>
/// 天候の処理フェーズ
/// </summary>
enum class EWeatherPhase : uint8 {
	None,           // 何もしない
	Wait,           // 待機フェーズ
	StartFirst,     // 第1変化開始フェーズ
	ChangeFirst,    // 第1変化中フェーズ
	StartSecond,    // 第2変化開始フェーズ
	ChangeSecond,   // 第2変化中フェーズ
	Finish,         // 終了フェーズ
	MaxPhaseIndex
};

/// <summary>
/// 天候エフェクトの種類
/// </summary>
USTRUCT(BlueprintType)
struct FWeatherEffectData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// 天候の種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeatherManager")
	EWeatherType WeatherType;

	/// <summary>
	/// 天候エフェクト
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeatherManager")
	UParticleSystem* WeatherEffect;
};

/// <summary>
/// 天候のスポーン座標
/// </summary>
USTRUCT(BlueprintType)
struct FSpawnWeatherData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// レベルの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeatherManager")
	ELevelType LevelType;

	/// <summary>
	/// 天候の種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeatherManager")
	EWeatherType WeatherType;

	/// <summary>
	/// スポーン位置 X座標
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeatherManager")
	float SpawnPosX;

	/// <summary>
	/// スポーン位置 Y座標
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeatherManager")
	float SpawnPosY;

	/// <summary>
	/// スポーン位置 Z座標
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeatherManager")
	float SpawnPosZ;
};

/// <summary>
/// ウェザーマネージャー
/// </summary>
UCLASS()
class AWeatherManager : public AActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AWeatherManager();

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	/// <param name=""> イベントインターフェース </param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "WeatherManager")
	void SetEventInterface(const TScriptInterface<IBattleEvent>& Interface);

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Initialize();

	/// <summary>
	/// 晴れの時の太陽と雲の変化量の計算処理
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:処理終了 </returns>
	bool CalcSunnyChangeSunAndCloudValues();

	/// <summary>
	/// 雨の時の太陽と雲の変化量の計算処理
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:処理終了 </returns>
	bool CalcRainyChangeSunAndCloudValues();

	/// <summary>
	/// 明るさを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 明るさ </returns>
	float GetIntensityValue();

	/// <summary>
	/// 太陽の明るさを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 太陽の明るさ </returns>
	float GetSunBrightnessValue();

	/// <summary>
	/// 雲の量を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 雲の量 </returns>
	float GetCloudOpacityValue();

	/// <summary>
	/// 雲の色を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 雲の色 </returns>
	FLinearColor GetCloudColor();

	/// <summary>
	/// 空の色を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 空の色 </returns>
	FLinearColor GetSkyColor();

	/// <summary>
	/// 現在の天候の種類を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 現在の天候の種類 </returns>
	EWeatherType GetCurrentWeatherType();

	/// <summary>
	/// 天候の変化を停止する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StopChangeWeather();

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

private:

	/// <summary>
	/// 天候のロード処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void LoadWeatherClass();

	/// <summary>
	/// 天候クラスの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupWeatherEffect();

	/// <summary>
	/// 天候をランダムに決定する
	/// </summary>
	/// <param name=""></param>
	/// <returns> 決定した天候の種類 </returns>
	EWeatherType DecideRandomWeather();

	/// <summary>
	/// 天候を変更する
	/// </summary>
	/// <param name="Type"> 変更する天候の種類 </param>
	/// <returns></returns>
	void ChangeWeather(EWeatherType Type);

	/// <summary>
	/// 天候を晴れに変化させる処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void SwitchSunny(float DeltaTime);

	/// <summary>
	/// 天候を雨に変化させる処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void SwitchRainy(float DeltaTime);

	/// <summary>
	/// 現在のフェーズの種類を変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentWeatherPhase(EWeatherPhase PhaseType);

	/// <summary>
	/// 現在の天候の種類を変更する
	/// </summary>
	/// <param name="Type"> 天候の種類 </param>
	/// <returns></returns>
	void ChangeCurrentWeatherType(EWeatherType Type);

public:

	/// <summary>
	/// エフェクトの種類のテーブル
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeatherManager")
	UDataTable* WeatherEffectTable;

	/// <summary>
	/// エフェクトのスポーン情報テーブル
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeatherManager")
	UDataTable* SpawnPositionTable;

	/// <summary>
	/// 晴れの時の明るさ
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "WeatherManager")
	float SunnyIntensityValue;

	/// <summary>
	/// 晴れの時の太陽の明るさ
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "WeatherManager")
	float SunnySunBrightnessValue;

	/// <summary>
	/// 晴れの時の雲の量
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "WeatherManager")
	float SunnyCloudOpacityValue;

	/// <summary>
	/// 晴れの時の雲の色
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "WeatherManager")
	FLinearColor SunnyCloudColor;

	/// <summary>
	/// 晴れの時の空の色
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "WeatherManager")
	FLinearColor SunnySkyColor;

	/// <summary>
	/// 明るさ調整値
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeatherManager")
	float AdjustIntensityValue;

	/// <summary>
	/// 太陽の明るさ調整値
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeatherManager")
	float AdjustSunBrightnessValue;

	/// <summary>
	/// 雲の量調整値
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeatherManager")
	float AdjustCloudOpacityValue;

	/// <summary>
	/// 雲の色調整値
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeatherManager")
	float AdjustCloudColorValue;

	/// <summary>
	/// 空の色調整値
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeatherManager")
	float AdjustSkyColorValue;

	/// <summary>
	/// 天候切替の待機時間
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeatherManager")
	float SwitchWeatherWaitTime;

private:

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IBattleEvent> EventInterface;

	/// <summary>
	/// 現在の天候フェーズの種類
	/// </summary>
	EWeatherPhase CurrentWeatherPhase;

	/// <summary>
	/// 現在の天候の種類
	/// </summary>
	EWeatherType CurrentWeatherType;

	/// <summary>
	/// 天候エフェクトデータ
	/// </summary>
	TMap<EWeatherType, UParticleSystem*> WeatherEffectData;

	/// <summary>
	/// 天候クラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<AWeather> WeatherClass;

	/// <summary>
	/// 天候の配列
	/// </summary>
	UPROPERTY()
	TArray<AWeather*> ArrayWeather;

	/// <summary>
	/// 累積時間
	/// </summary>
	float AccumulateTime;

	/// <summary>
	/// 明るさ
	/// </summary>
	float IntensityValue;

	/// <summary>
	/// 太陽の明るさ
	/// </summary>
	float SunBrightnessValue;

	/// <summary>
	/// 雲の量
	/// </summary>
	float CloudOpacityValue;

	/// <summary>
	/// 雲の色
	/// </summary>
	FLinearColor CloudColor;

	/// <summary>
	/// 空の色
	/// </summary>
	FLinearColor SkyColor;

private:
	//--------------------------------
	//以下、定数宣言
	//--------------------------------
	/// <summary>
	/// 時間リセット
	/// </summary>
	const float ResetTime = 0.0f;

	/// <summary>
	/// 天候切替処理中の待機時間
	/// </summary>
	const float ProcessWaitTime = 2.5f;

	/// <summary>
	/// 雨の時の明るさ
	/// </summary>
	const float RainyIntensityValue = 0.0f;

	/// <summary>
	/// 雨の時の太陽の明るさ
	/// </summary>
	const float RainySunBrightnessValue = 0.0f;

	/// <summary>
	/// 雨の時の雲の量
	/// </summary>
	const float RainyCloudOpacityValue = 15.0f;

	/// <summary>
	/// 雨の時の雲の色
	/// </summary>
	const FLinearColor RainyCloudColor = FLinearColor(0.3f, 0.3f, 0.3f);

	/// <summary>
	/// 雨の時の空の色
	/// </summary>
	const FLinearColor RainySkyColor = FLinearColor(0.05f, 0.05f, 0.05f);
};
