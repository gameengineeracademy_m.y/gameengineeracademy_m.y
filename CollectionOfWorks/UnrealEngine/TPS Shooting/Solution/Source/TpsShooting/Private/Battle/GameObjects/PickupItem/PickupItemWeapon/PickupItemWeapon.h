﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Battle/GameObjects/PickupItem/PickupItemBase.h"
#include "PickupItemWeapon.generated.h"


/// <summary>
/// 武器の種類
/// </summary>
UENUM()
enum class EPickupWeaponType {
	None,           // なし
	AssaultRifle,   // アサルトライフル
	SniperRifle,    // スナイパーライフル
	MaxTypeIndex
};

/// <summary>
/// フェードアウトのフェーズ
/// </summary>
UENUM()
enum class EFadeOutPhase {
	None,            // なし
	Start,           // 開始
	PreProcess,      // 前処理
	Process,         // 処理中
	PostProcess,     // 後処理
	Finish,          // 終了
	MaxTypeIndex
};

/// <summary>
/// ピックアップアイテム 武器
/// </summary>
UCLASS()
class APickupItemWeapon : public APickupItemBase
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	APickupItemWeapon();

	/// <summary>
	/// 武器の種類をセット
	/// </summary>
	/// <param name="WeaponType"> 武器の種類 </param>
	/// <returns></returns>
	void SetPickupWeaponType(EPickupWeaponType WeaponType);

	/// <summary>
	/// 武器の種類を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 武器の種類 </returns>
	EPickupWeaponType GetPickupWeaponType();

	/// <summary>
	/// フェードアウト処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StartFadeOut();


protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

private:

	/// <summary>
	/// 現在のフェードアウトフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentFadeOutPhase(EFadeOutPhase PhaseType);


protected:

	/// <summary>
	/// 武器の種類
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "PickupItemWeapon")
	EPickupWeaponType PickupWeaponType;

private:

	/// <summary>
	/// 現在のフェードアウトフェーズ
	/// </summary>
	EFadeOutPhase CurrentFadeOutPhase;

	/// <summary>
	/// 透過率
	/// </summary>
	float RenderValue;
};
