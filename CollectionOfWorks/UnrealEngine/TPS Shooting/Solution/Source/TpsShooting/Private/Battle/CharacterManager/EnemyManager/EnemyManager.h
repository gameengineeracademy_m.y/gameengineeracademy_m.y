﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Battle/Character/Enemy/Enemy.h"
#include "Battle/Weapons/WeaponFactory.h"
#include "Engine/DataTable.h"
#include "../Public/TpsShootingGameInstance.h"
#include "Battle/GameObjects/WarningLamp/WarningLampManager.h"
#include "EnemyManager.generated.h"


/// <summary>
/// 発見処理フェーズの種類
/// </summary>
UENUM()
enum class EFoundPlayerPhaseType {
	None,              // 何もしない
	WaitNotifyBeware,  // 警戒通知待機フェーズ
	NotifyBeware,      // 警戒通知フェーズ
	WaitNotify,        // 情報通知待機フェーズ
	TransBeware,       // 警戒状態移行フェーズ
	WatchFight,        // 戦闘監視フェーズ
	WaitBeware,        // 警戒待機フェーズ
	NotifyPatrol,      // 巡回通知フェーズ
	ForciblyPatrol,    // 強制巡回遷移フェーズ
	MaxPhaseIndex
};

/// <summary>
/// エネミーマネージャクラス
/// </summary>
UCLASS()
class AEnemyManager : public AActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AEnemyManager();

	/// <summary>
	/// エネミーの生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreateEnemies();

	/// <summary>
	/// ウェポンファクトリーのセット
	/// </summary>
	/// <param name="Factory"> ウェポンファクトリー </param>
	/// <returns></returns>
	void SetWeaponFactory(AWeaponFactory* Factory);

	/// <summary>
	/// 警告ランプマネージャーのセット
	/// </summary>
	/// <param name="Manager"> 警告ランプマネージャー </param>
	/// <returns></returns>
	void SetWarningLampManager(AWarningLampManager* Manager);

	/// <summary>
	/// 警戒レベルリストのキー(エリアの種類)を追加
	/// </summary>
	/// <param name="StageType"> エリアの種類 </param>
	/// <returns></returns>
	void AddAlertLevelList(EStageType StageType);

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Initialize();

	/// <summary>
	/// エネミーインデックスリストを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> インデックスリスト </returns>
	TArray<int32> GetEnemyIndexList();

	/// <summary>
	/// 指定エネミーの取得
	/// </summary>
	/// <param name="Index"> インデックス </param>
	/// <returns> エネミー </returns>
	AEnemy* GetEnemy(int32 Index);

	/// <summary>
	/// 最新のターゲット位置をセット
	/// </summary>
	/// <param name="Position"> ターゲット位置 </param>
	/// <returns></returns>
	void SetNewTargetPosition(FVector Position);

	/// <summary>
	/// 最新のターゲット位置を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ターゲット位置 </returns>
	FVector GetNewTargetPosition();

	/// <summary>
	/// 指定エリアの現在の警戒レベルをセット
	/// </summary>
	/// <param name="StageType"> エリアの種類 </param>
	/// <param name="AlertLevel"> 警戒レベル </param>
	/// <returns></returns>
	void SetAlertLevel(EStageType StageType, EAlertLevel AlertLevel);

	/// <summary>
	/// 指定エリアの現在の警戒レベルを取得
	/// </summary>
	/// <param name="StageType"> エリアの種類 </param>
	/// <returns> 警戒レベル </returns>
	EAlertLevel GetAlertLevel(EStageType StageType);

	/// <summary>
	/// エネミーの目が見えるようにする
	/// </summary>
	/// <param name="StageType"> エリアの種類 </param>
	/// <returns></returns>
	void CanSeeEnemy(EStageType StageType);

	/// <summary>
	/// エネミーの処理を強制的に停止する
	/// </summary>
	/// <param name="StageType"> エリアの種類 </param>
	/// <returns></returns>
	void StopEnemy(EStageType StageType);

	/// <summary>
	/// エネミーの破棄
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void DisposeEnemies();

	/// <summary>
	/// エネミーの視覚設定
	/// </summary>
	/// <param name="bIsSight"> 視覚有無 </param>
	/// <returns></returns>
	void SetupDebugEnemySight(bool bIsSight);

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

private:

	/// <summary>
	/// エネミーのスポーン
	/// </summary>
	/// <param name="SpawnData"> スポーン情報 </param>
	/// <returns> スポーンしたエネミー </returns>
	AEnemy* SpawnEnemy(FSpawnData SpawnData);

	/// <summary>
	/// 装備する武器の取得
	/// </summary>
	/// <param name="RowNumber"> 行番号 </param>
	/// <returns> 装備する武器情報 </returns>
	FCharaInitWeapon ReadCharaEquipWeapon(FString RowNumber);

	/// <summary>
	/// 共通処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void ProcessCommon(float DeltaTime);

	/// <summary>
	/// プレイヤー発見後処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void ProcessFoundPlayer(float DeltaTime);

	/// <summary>
	/// 戦闘用・警戒接近リストの整理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OrganizeFightAndApproachEnemyList();

	/// <summary>
	/// プレイヤー発見マーカーを表示設定を行う
	/// </summary>
	/// <param name="bDisplay"> 表示有無 </param>
	/// <returns></returns>
	void SetupFoundPlayerMarkaer(bool bDisplay);

	/// <summary>
	/// 現在のプレイヤー発見フェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentFoundPlayerPhase(EFoundPlayerPhaseType PhaseType);

private:

	/// <summary>
	/// 現在のプレイヤー発見フェーズ
	/// </summary>
	EFoundPlayerPhaseType CurrentFoundPlayerPhase;

	/// <summary>
	/// エネミークラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<AEnemy> EnemyClass;

	/// <summary>
	/// エネミーリスト
	/// </summary>
	UPROPERTY()
	TMap<int32, AEnemy*> EnemyList;

	/// <summary>
	/// 戦闘中エネミーキーリスト
	/// </summary>
	UPROPERTY()
	TArray<int32> FightEnemyIDList;

	/// <summary>
	/// プレイヤーに接近するエネミーキーリスト
	/// </summary>
	UPROPERTY()
	TArray<int32> ApproachEnemyIDList;

	/// <summary>
	/// プレイヤースポーン情報テーブル
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "EnemyManager")
	UDataTable* EnemySpawnTable;

	/// <summary>
	/// プレイヤー初期装備テーブル
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "EnemyManager")
	UDataTable* EnemyInitWeaponTable;

	/// <summary>
	/// プレイヤーディテール
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "EnemyManager")
	UCharacterDetail* EnemyDetail;

	/// <summary>
	/// ウェポンファクトリー
	/// </summary>
	UPROPERTY()
	AWeaponFactory* WeaponFactory;

	/// <summary>
	/// 警告ランプマネージャー
	/// </summary>
	UPROPERTY()
	AWarningLampManager* WarningLampManager;

	/// <summary>
	/// エネミーインデックスリスト
	/// </summary>
	TArray<int32> EnemyIndexArray;

	/// <summary>
	/// 累積時間
	/// </summary>
	float AccumulateTime;

	/// <summary>
	/// 最新の目標位置
	/// </summary>
	FVector NewTargetPosition;

	/// <summary>
	/// エリアごとの警報レベル
	/// </summary>
	TMap<EStageType, EAlertLevel> AlertLevelList;

	/// <summary>
	/// プレイヤー発見フラグ
	/// </summary>
	bool bIsFoundPlayer;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// インデックス最小値
	/// </summary>
	const int32 InitIndex = 0;

	/// <summary>
	/// リセット時間
	/// </summary>
	const float ResetTime = 0.0f;

	/// <summary>
	/// 警戒通知待機時間
	/// </summary>
	const float NotifyBewareWaitTime = 2.5f;

	/// <summary>
	/// 警戒情報通達中時間
	/// </summary>
	const float NotifyInfomationWaitTime = 2.0f;

	/// <summary>
	/// 巡回通知待機時間
	/// </summary>
	const float NotifyPatrolWaitTime = 15.0f;

	/// <summary>
	/// 戦闘している最低数
	/// </summary>
	const int32 FightEnemyMinCount = 2;

	/// <summary>
	/// プレイヤーに接近する数
	/// </summary>
	const int32 ApproachEnemyMinCount = 2;

	/// <summary>
	/// ゼロ
	/// </summary>
	const int32 Zero = 0;
};
