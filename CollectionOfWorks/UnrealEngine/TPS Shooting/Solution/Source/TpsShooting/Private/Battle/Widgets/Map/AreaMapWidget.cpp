﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Widgets/Map/AreaMapWidget.h"
#include "Kismet/KismetMathLibrary.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Components/Image.h"
#include "Components/Border.h"
#include "Components/CanvasPanel.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UAreaMapWidget::UAreaMapWidget()
	: MapMaskTable(nullptr)
	, DisplayMapDataTable(nullptr)
	, MapMaskList()
	, MapDisplayDataList() {

}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UAreaMapWidget::NativeConstruct() {

	Super::NativeConstruct();

	CurrentStageType = EStageType::None;
	Hide();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="Geometry"> ジオメトリ </param>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void UAreaMapWidget::NativeTick(const FGeometry& Geometry, float DeltaTime) {

	Super::NativeTick(Geometry, DeltaTime);

	// プレイヤーアイコン更新
	UpdatePlayerIcon();
	// エネミーアイコン更新
	UpdateEnemyIcon();
	// アイテムアイコン更新
	UpdatePickupItemIcon();
	// ゴールアイコン更新
	UpdateGoalIcon();
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UAreaMapWidget::InitializeWidget() {

	UMapBaseWidget::InitializeWidget();

	//---------------------------------------
	// 現在のレベルを取得
	//---------------------------------------
	UTpsShootingGameInstance* GameInstance = Cast<UTpsShootingGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (!IsValid(GameInstance)) {
		return;
	}
	ELevelType CurrentLevelType = GameInstance->GetCurrentLevel();

	// テーブルが存在しない場合は処理終了
	if (!IsValid(MapMaskTable)) {
		return;
	}

	//---------------------------------------
	// マップマスクの設定
	//---------------------------------------
	TArray<FName> RowNames = MapMaskTable->GetRowNames();
	for (FName RowName : RowNames) {

		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}

		FAreaMapMaskType* MapMaskType = MapMaskTable->FindRow<FAreaMapMaskType>(RowName, "");
		if (MapMaskType->BattleLevelType != CurrentLevelType) {
			continue;
		}
		// 既にリストに登録済みなら次の行へ　　
		if (MapMaskList.Contains(MapMaskType->StageType)) {
			continue;
		}
		MapMaskList.Add(MapMaskType->StageType, MapMaskType->MaskTexture);
	}

	//---------------------------------------
	// エリア中心座標の設定
	//---------------------------------------
	RowNames = DisplayMapDataTable->GetRowNames();
	for (FName RowName : RowNames) {

		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}

		FDisplayMapData* DisplayMapData = DisplayMapDataTable->FindRow<FDisplayMapData>(RowName, "");
		if (DisplayMapData->BattleLevelType != CurrentLevelType) {
			continue;
		}
		// 既にリストに登録済みなら次の行へ
		if (MapDisplayDataList.Contains(DisplayMapData->StageType)) {
			continue;
		}
		MapDisplayDataList.Add(DisplayMapData->StageType, DisplayMapData);
	}
}

/// <summary>
/// エリアの切り替え
/// </summary>
/// <param name="StageType"> エリアの種類 </param>
/// <returns></returns>
void UAreaMapWidget::SwitchAreaMap(EStageType StageType) {

	// リストに登録されていないなら処理終了
	if (!MapMaskList.Contains(StageType) || !MapDisplayDataList.Contains(StageType)) {
		return;
	}

	SetStageType(StageType);
	UImage* MapMask = Cast<UImage>(GetWidgetFromName(ImageMaskName));
	if (!IsValid(MapMask)) {
		return;
	}
	MapMask->SetBrushFromTexture(MapMaskList[CurrentStageType]);

	// 画像サイズを変更しつつ、マップの中心座標を算出し、
	// マップ画像の表示位置を変更しておく
	ChangeMapImageSize(MapDisplayDataList[StageType]->ImageSize);
	FVector CenterPos = FVector(MapDisplayDataList[StageType]->CenterPosX, MapDisplayDataList[StageType]->CenterPosY, Zero);
	MapCenterPos = GetPositionOnMap(CenterPos) * OppositeDirection;

	// ミニマップの回転中心位置を算出
	// ※ミニマップの中心を原点として算出
	float MapHalfSize = MapDisplayDataList[StageType]->ImageSize * HalfValue;
	FVector2D MapRotCenterPos = (FVector2D(MapHalfSize, MapHalfSize) - MapCenterPos) / MapDisplayDataList[StageType]->ImageSize;

	UBorder* MapImage = Cast<UBorder>(GetWidgetFromName(BorderMapName));
	if (!IsValid(MapImage)) {
		return;
	}
	FWidgetTransform MapImageTransform;
	MapImageTransform.Translation = MapCenterPos;
	MapImageTransform.Angle = MapDisplayDataList[StageType]->Angle;
	MapImage->SetRenderTransform(MapImageTransform);
	MapImage->SetRenderTransformPivot(MapRotCenterPos);
}

/// <summary>
/// プレイヤー位置更新
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UAreaMapWidget::UpdatePlayerIcon() {

	if (!IsValid(PlayerManager)) {
		return;
	}

	AOperatePlayer* Player = PlayerManager->GetOperatePlayer();
	if (!IsValid(Player)) {
		return;
	}

	// 実エリア座標からエリア上に配置した基準ボリュームと
	// イメージマップのスケール比を用いてミニマップ上での位置を算出
	FVector CurrentPlayerPos = Player->GetActorLocation();
	FVector2D MapPosition = GetPositionOnMap(CurrentPlayerPos);
	// プレイヤーの向きを度数で算出
	// ※プレイヤーは90°回転させてスポーンさせているため、
	// その分を加算しておく
	FVector Direction = Player->GetActorForwardVector();
	float PlayerAngle = UKismetMathLibrary::DegAtan2(Direction.Y, Direction.X) + Angle90;

	UImage* PlayerImage = Cast<UImage>(GetWidgetFromName(ImagePlayerName));
	if (!IsValid(PlayerImage)) {
		return;
	}

	// プレイヤーアイコンにマップ上の位置と角度を渡す
	FWidgetTransform PlayerImageTransform;
	PlayerImageTransform.Translation = MapPosition;
	PlayerImageTransform.Angle = PlayerAngle;
	PlayerImage->SetRenderTransform(PlayerImageTransform);
}

/// <summary>
/// エネミーアイコンの更新
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UAreaMapWidget::UpdateEnemyIcon() {

	if (!IsValid(EnemyManager)) {
		return;
	}

	for (UBattleHUDMapIcon* MapIcon : EnemyIconList) {

		if (!IsValid(MapIcon)) {
			continue;
		}

		// 紐づけ番号を元にエネミーを取得、
		// 実エリアの座標を取得し、マップ上の座標を算出する
		int32 EnemyIndex = MapIcon->GetObjectIndex();
		AEnemy* Enemy = EnemyManager->GetEnemy(EnemyIndex);
		if (!IsValid(Enemy)) {
			MapIcon->SetDisplay(false);
			MapIcon->SetRenderOpacity(Transparent);
			continue;
		}

		// 敵が死亡しているならアイコンを非表示にする
		if (!Enemy->IsFine()) {
			MapIcon->SetDisplay(false);
			MapIcon->SetRenderOpacity(Transparent);
			continue;
		}

		// 敵が表示待機中ならアイコンを非表示にする
		if (Enemy->IsWaitAppearance()) {
			MapIcon->SetDisplay(false);
			MapIcon->SetRenderOpacity(Transparent);
			continue;
		}

		// 現在いるエリアでないならアイコンを非表示にする
		EStageType StageType = Enemy->GetStageType();
		if (StageType != CurrentStageType) {
			MapIcon->SetDisplay(false);
			MapIcon->SetRenderOpacity(Transparent);
			continue;
		}

		MapIcon->SetDisplay(true);
		MapIcon->SetRenderOpacity(Opacity);

		FVector EnemyPosition = Enemy->GetActorLocation();
		FVector2D MapPosition = GetPositionOnMap(EnemyPosition);

		FWidgetTransform ItemTransform;
		ItemTransform.Translation = FVector2D(MapPosition.X, MapPosition.Y);
		MapIcon->SetRenderTransform(ItemTransform);
	}
}

/// <summary>
/// アイテムアイコンの更新
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UAreaMapWidget::UpdatePickupItemIcon() {

	if (!IsValid(PickupItemManager)) {
		return;
	}

	for (UBattleHUDMapIcon* MapIcon : PickupItemIconList) {

		if (!IsValid(MapIcon)) {
			continue;
		}

		// 紐づけ番号を元にアイテムを取得、
		// 実エリアの座標を取得し、マップ上の座標を算出する
		int32 ItemIndex = MapIcon->GetObjectIndex();
		APickupItemBase* Item = PickupItemManager->GetPickupItem(ItemIndex);
		if (!IsValid(Item)) {
			MapIcon->SetDisplay(false);
			MapIcon->SetRenderOpacity(Transparent);
			continue;
		}

		// アイテムが拾われているならアイコンを非表示にする
		if (Item->IsPickup()) {
			MapIcon->SetDisplay(false);
			MapIcon->SetRenderOpacity(Transparent);
			continue;
		}

		// 現在いるエリアでないならアイコンを非表示にする
		EStageType StageType = Item->GetStageType();
		if (StageType != CurrentStageType) {
			MapIcon->SetDisplay(false);
			MapIcon->SetRenderOpacity(Transparent);
			continue;
		}

		MapIcon->SetDisplay(true);
		MapIcon->SetRenderOpacity(Opacity);

		FVector ItemPosition = Item->GetActorLocation();
		FVector2D MapPosition = GetPositionOnMap(ItemPosition);

		// マップの角度に合わせてアイコンを回転させる
		FWidgetTransform ItemTransform;
		ItemTransform.Translation = FVector2D(MapPosition.X, MapPosition.Y);
		ItemTransform.Angle = OppositeDirection * MapDisplayDataList[StageType]->Angle;
		MapIcon->SetRenderTransform(ItemTransform);
	}
}

/// <summary>
/// ゴールアイコンの更新
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UAreaMapWidget::UpdateGoalIcon() {

	if (!IsValid(TargetObjectManager)) {
		return;
	}

	for (UBattleHUDMapIcon* MapIcon : GoalIconList) {

		if (!IsValid(MapIcon)) {
			continue;
		}

		// 紐づけ番号を元にアイテムを取得
		int32 GoalIndex = MapIcon->GetObjectIndex();
		AGoalObject* GoalObject = TargetObjectManager->GetGoalObject(GoalIndex);
		if (!IsValid(GoalObject)) {
			continue;
		}

		// 現在いるエリアでないならアイコンを非表示にする
		EStageType StageType = GoalObject->GetStageType();
		if (StageType != CurrentStageType) {
			MapIcon->SetDisplay(false);
			MapIcon->SetRenderOpacity(Transparent);

			if (MapIcon->IsPlayBackAnim()) {
				MapIcon->SetupDisplayBackImage(ESlateVisibility::Collapsed);
				MapIcon->StopBackAnimation();
			}
			continue;
		}

		MapIcon->SetDisplay(true);
		MapIcon->SetRenderOpacity(Opacity);

		if (!MapIcon->IsPlayBackAnim()) {
			MapIcon->SetupDisplayBackImage(ESlateVisibility::Visible);
			MapIcon->PlayBackAnimation();
		}

		// マップの角度に合わせてアイコンを回転させる
		FVector GoalPosition = GoalObject->GetActorLocation();
		FVector2D MapPosition = GetPositionOnMap(GoalPosition);

		// マップの角度に合わせてアイコンを回転させる
		FWidgetTransform GoalTransform;
		GoalTransform.Translation = FVector2D(MapPosition.X, MapPosition.Y);
		GoalTransform.Angle = OppositeDirection * MapDisplayDataList[StageType]->Angle;
		MapIcon->SetRenderTransform(GoalTransform);
	}
}