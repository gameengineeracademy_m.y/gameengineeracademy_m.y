﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/GameObjects/WarningLamp/WarningLampManager.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AWarningLampManager::AWarningLampManager()
	: LampSpawnTable(nullptr)
	, WarningLampClass()
	, WarningLampList() {

	// Tick処理を無効化
	PrimaryActorTick.bCanEverTick = false;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWarningLampManager::BeginPlay() {

	Super::BeginPlay();

	// 警戒ランプクラスのロード処理
	LoadWarningLamp();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AWarningLampManager::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}

/// <summary>
/// 警戒ランプクラスのロード
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWarningLampManager::LoadWarningLamp() {

	FString Path;
	// 警戒ランプクラスのロード ソフト参照
	GConfig->GetString(TEXT("ActorSettings"), TEXT("WarningLampPath"), Path, GGameIni);
	WarningLampClass = TSoftClassPtr<AWarningLamp>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (!WarningLampClass) {
		UE_LOG(LogTemp, Display, TEXT("AWarningLampManager WarningLampClass Error"));
		return;
	}
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWarningLampManager::Initialize() {

	//---------------------------------------
	// 現在のレベルを取得
	//---------------------------------------
	UTpsShootingGameInstance* GameInstance = Cast<UTpsShootingGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (!IsValid(GameInstance)) {
		return;
	}
	ELevelType CurrentLevelType = GameInstance->GetCurrentLevel();

	//---------------------------------------
	// データテーブルからスポーン位置取得
	//---------------------------------------
	TArray<FName> RowNames = LampSpawnTable->GetRowNames();
	for (FName RowName : RowNames) {

		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		// 指定レベル以外なら次の行へ
		FLampSpawnData* LampSpawn = LampSpawnTable->FindRow<FLampSpawnData>(RowName, "");
		if (LampSpawn->BattleLevelType != CurrentLevelType) {
			continue;
		}

		FVector SpawnPosition = FVector::ZeroVector;
		FRotator SpawnRotation = FRotator::ZeroRotator;
		FActorSpawnParameters Params;

		SpawnPosition = FVector(LampSpawn->PositionX, LampSpawn->PositionY, LampSpawn->PositionZ);
		SpawnRotation.Yaw = LampSpawn->RotationYaw;

		//------------------------------------
		// アイテムのスポーン
		//------------------------------------
		AWarningLamp* WarningLamp = GetWorld()->SpawnActor<AWarningLamp>(WarningLampClass, SpawnPosition, SpawnRotation, Params);
		if (!IsValid(WarningLamp)) {
			continue;
		}
		WarningLamp->SetStageType(LampSpawn->StageType);
		// 警戒ランプをリストに登録
		WarningLampList.Add(WarningLamp);
	}
}

/// <summary>
/// 指定エリアの警戒ランプを点灯する
/// </summary>
/// <param name="StageType"> 指定のエリア </param>
/// <returns></returns>
void AWarningLampManager::LightLamp(EStageType StageType) {

	for (AWarningLamp* Lamp : WarningLampList) {

		if (!IsValid(Lamp)) {
			continue;
		}
		// 指定エリア以外のランプなら次へ
		EStageType LampStageType = Lamp->GetStageType();
		if (LampStageType != StageType) {
			continue;
		}
		Lamp->Light();
	}
}

/// <summary>
/// 警戒ランプを消灯する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWarningLampManager::TurnOffLamp() {

	for (AWarningLamp* Lamp : WarningLampList) {

		if (!IsValid(Lamp)) {
			continue;
		}
		Lamp->TurnOff();
	}
}