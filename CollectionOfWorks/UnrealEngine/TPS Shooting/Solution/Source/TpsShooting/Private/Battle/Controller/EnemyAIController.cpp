﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Controller/EnemyAIController.h"
#include "Battle/Character/OperatePlayer/OperatePlayer.h"
#include "DrawDebugHelpers.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AEnemyAIController::AEnemyAIController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer){

	// Tick処理の無効化
	PrimaryActorTick.bCanEverTick = false;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemyAIController::BeginPlay() {

	Super::BeginPlay();

	PawnSensor->bSeePawns = true;
	PawnSensor->SightRadius = SightRadiusDefaultValue;

	// デリゲートの登録
	FScriptDelegate OnSeePawnDelegate;
	OnSeePawnDelegate.BindUFunction(this, "OnSeePawn");
	PawnSensor->OnSeePawn.Add(OnSeePawnDelegate);
	FScriptDelegate OnHearNoiseDelegate;
	OnHearNoiseDelegate.BindUFunction(this, "OnHearNoise");
	PawnSensor->OnHearNoise.Add(OnHearNoiseDelegate);
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AEnemyAIController::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}

/// <summary>
/// ノイズを聴いた際に呼ぶ処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemyAIController::OnHearNoise(APawn* Object, const FVector& Location, float Volume) {

	UE_LOG(LogTemp, Display, TEXT("NoisePos X:%f, Y:%f, Z:%f"), Location.X, Location.Y, Location.Z)
}

/// <summary>
/// オブジェクトを視認した時に呼ぶ処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemyAIController::OnSeePawn(APawn* Object) {

	AOperatePlayer* Player = Cast<AOperatePlayer>(Object);
	if (!IsValid(Player)) {
		return;
	}

	AEnemy* Enemy = GetPossesPawn();
	if (!IsValid(Enemy)) {
		return;
	}

	//操作プレイヤーが死んでいるなら処理終了
	bool bFine = Player->IsFine();
	if (!bFine) {
		return;
	}

	//--------------------------------------------------------
	// プレイヤー位置と自身(敵)の頭の位置を取得
	// レイを飛ばし、ヒット先が操作プレイヤー以外なら処理終了
	//--------------------------------------------------------
	FVector PlayerHeadPos = Player->GetSocketPosition(HeadSocketName);
	FVector EnemyHeadPos = Enemy->GetSocketPosition(HeadSocketName);
	FHitResult Hit = CheckSeePlayer(EnemyHeadPos, PlayerHeadPos);
	if (!IsValid(Cast<AOperatePlayer>(Hit.GetActor()))) {
		return;
	}

	// 何かに気づいたことにする
	Enemy->FoundSomething();
	Enemy->SetTargetPosition(Player->GetActorLocation());
}

/// <summary>
/// 本当にプレイヤーが見えているかシミュレーションする
/// </summary>
/// <param name="StartPos"> 開始地点 </param>
/// <param name="EndPos"> 終端地点 </param>
/// <returns></returns>
FHitResult AEnemyAIController::CheckSeePlayer(FVector StartPos, FVector EndPos) {

	FHitResult HitResult;
	FCollisionQueryParams QueryParams;

	AEnemy* Enemy = GetPossesPawn();
	if (!IsValid(Enemy)) {
		return HitResult;
	}
	QueryParams.AddIgnoredActor(Enemy);
	GetWorld()->LineTraceSingleByChannel(HitResult, StartPos, EndPos, ECollisionChannel::ECC_Visibility, QueryParams);

	return HitResult;
}

/// <summary>
/// 所有しているポーンの取得
/// </summary>
/// <param name=""></param>
/// <returns> 所有しているポーン </returns>
AEnemy* AEnemyAIController::GetPossesPawn() {

	return Cast<AEnemy>(GetPawn());
}

/// <summary>
/// 視覚設定をする
/// </summary>
/// <param name="bBlind"> 視覚有無 </param>
/// <returns></returns>
void AEnemyAIController::SetupSee(bool bBlind) {

	PawnSensor->bSeePawns = bBlind;
}

/// <summary>
/// 視野の設定をする
/// </summary>
/// <param name="EnemyType"> 敵の種類 </param>
/// <returns></returns>
void AEnemyAIController::SetSightRadius(EEnemyType EnemyType) {

	switch (EnemyType) {
	case EEnemyType::Normal: {

		PawnSensor->SightRadius = SightRadiusNormal;
		break;
	}
	case EEnemyType::Sniper: {

		PawnSensor->SightRadius = SightRadiusSniper;
		break;
	}
	}
}