﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Public/TpsShootingGameInstance.h"
#include "Battle/GameObjects/WarningLamp/WarningLamp.h"
#include "Engine/DataTable.h"
#include "WarningLampManager.generated.h"


/// <summary>
/// 警戒ランプのスポーン位置
/// </summary>
USTRUCT(BlueprintType)
struct FLampSpawnData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// バトルレベルの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WarningLampManager")
	ELevelType BattleLevelType;

	/// <summary>
	/// エリアの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WarningLampManager")
	EStageType StageType;

	/// <summary>
	/// スポーン位置 X座標
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WarningLampManager")
	float PositionX;

	/// <summary>
	/// スポーン位置 Y座標
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WarningLampManager")
	float PositionY;

	/// <summary>
	/// スポーン位置 Z座標
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WarningLampManager")
	float PositionZ;

	/// <summary>
	/// スポーン角度 Z軸周り
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WarningLampManager")
	float RotationYaw;
};

/// <summary>
/// 警告ランプマネージャークラス
/// </summary>
UCLASS()
class AWarningLampManager : public AActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AWarningLampManager();

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Initialize();

	/// <summary>
	/// 指定エリアの警戒ランプを点灯する
	/// </summary>
	/// <param name="StageType"> 指定エリア </param>
	/// <returns></returns>
	void LightLamp(EStageType StageType);

	/// <summary>
	/// 警戒ランプを消灯する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void TurnOffLamp();

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

private:

	/// <summary>
	/// 警戒ランプクラスのロード
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void LoadWarningLamp();

public:

	/// <summary>
	/// 警戒ランプのスポーンテーブル
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WarningLampManager")
	UDataTable* LampSpawnTable;

private:

	/// <summary>
	/// 警戒ランプクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<AWarningLamp> WarningLampClass;

	/// <summary>
	/// 警戒ランプリスト
	/// </summary>
	UPROPERTY()
	TArray<AWarningLamp*> WarningLampList;
};
