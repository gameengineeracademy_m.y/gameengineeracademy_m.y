﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Battle/Character/CharacterBase.h"
#include "Battle/Interface/BattleEvent.h"
#include "Sound/SoundCue.h"
#include "OperatePlayer.generated.h"


/// <summary>
/// Dying状態の処理
/// </summary>
UENUM()
enum class EPlayerDyingPhase {
	None,     // 何もしない
	Start,    // 開始フェーズ
	Play,     // 処理中フェーズ
	Finish,   // 終了フェーズ
	MaxPhaseType
};

/// <summary>
/// 瀕死ウィジェットの表示状態の種類
/// </summary>
UENUM()
enum class EDyingDisplayStateType {
	None,     // 何もしない
	Pause,    // 一時停止
	Resume,   // 再開
	MaxPhaseType
};

/// <summary>
/// 操作プレイヤー
/// </summary>
UCLASS()
class AOperatePlayer : public ACharacterBase
{
	GENERATED_BODY()

public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AOperatePlayer();

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	/// <param name=""> イベントインターフェース </param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "OperatePlayer")
	void SetEventInterface(const TScriptInterface<IBattleEvent>& Interface);

	/// <summary>
	/// 所持武器リストを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	TMap<EEquipPositionType, EWeaponType> GetPossessWeaponList();

	/// <summary>
	/// アイテム取得イベント
	/// </summary>
	/// <param name="Actor"> アクター </param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "OperatePlayer")
	void OnObtainItem(AActor* Actor);

	/// <summary>
	/// 各種ヒットポイントの境界値をセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupHitPointLine();

	/// <summary>
	/// エイムキャンセル
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CancelAim();

	/// <summary>
	/// 瀕死状態の一時停止設定
	/// </summary>
	/// <param name="StateType"> 設定の種類 </param>
	/// <returns></returns>
	void PauseDying(EDyingDisplayStateType StateType);

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

	/// <summary>
	/// プレイヤーインプットコンポーネント設定
	/// </summary>
	/// <param name="PlayerInputComponent"> インプットコンポーネント </param>
	/// <returns></returns>
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	/// <summary>
	/// 狙う
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Aim();

	/// <summary>
	/// 狙うのをやめる
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StopAiming();

	/// <summary>
	/// マウスホイール操作
	/// </summary>
	/// <param name="Value"> 入力方向 </param>
	/// <returns></returns>
	void OperateMouseWheel(float Value);

	/// <summary>
	/// 武器を切り替える
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "OperatePlayer")
	void SwitchWeapon();

	/// <summary>
	/// 武器を切り替え終了処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "OperatePlayer")
	void CompletedSwitchWeapon();

	/// <summary>
	/// 瀕死状態になる
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void BeDying();

	/// <summary>
	/// 瀕死状態から回復する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void RecoverFromDying();

	/// <summary>
	/// 撃たれた時のウィジェット処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void DisplayShootedWidget();

	/// <summary>
	/// プレイヤーが瀕死かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:瀕死 </returns>
	bool IsDying();

	/// <summary>
	/// 瀕死ウィジェットを開く
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OpenDyingWidget();

	/// <summary>
	/// 瀕死ウィジェットを閉じる
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CloseDyingWidget();

	/// <summary>
	/// 瀕死ウィジェットの表示有無
	/// </summary>
	/// <param name="StateType"> 設定の種類 </param>
	/// <returns></returns>
	void DisplayDyingWidget(EDyingDisplayStateType StateType);

	/// <summary>
	/// 現在の瀕死フェーズの変更
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentDyingPhase(EPlayerDyingPhase PhaseType);

	/// <summary>
	/// 現在の瀕死ウィジェットの表示状態の種類
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentDyingDisplayStateType(EDyingDisplayStateType StateType);

protected:

	/// <summary>
	/// 心音サウンド
	/// </summary>
	UPROPERTY(EditAnywhere, Category="OperatePlayer")
	USoundCue* HeartBeatSound;

private:

	/// <summary>
	/// 現在の瀕死フェーズ
	/// </summary>
	EPlayerDyingPhase CurrentDyingPhase;

	/// <summary>
	/// 現在の瀕死ウィジェットの表示状態の種類
	/// </summary>
	EDyingDisplayStateType CurrentDyingDisplayStateType;

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IBattleEvent> EventInterface;

	/// <summary>
	/// ホイール受付量
	/// </summary>
	float InputWheel;

	/// <summary>
	/// 瀕死とするヒットポイント
	/// </summary>
	int32 DyingHitPoint;

	/// <summary>
	/// インプットコンポーネント
	/// </summary>
	UInputComponent* PlayerInputComp;

	/// <summary>
	/// オーディオコンポーネント 心音
	/// </summary>
	UPROPERTY()
	UAudioComponent* AudioCompHeartBeat;
};