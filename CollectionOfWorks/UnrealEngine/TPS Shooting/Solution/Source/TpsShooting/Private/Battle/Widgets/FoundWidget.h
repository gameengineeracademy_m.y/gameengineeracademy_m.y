﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/UiAssignmentWidget.h"
#include "FoundWidget.generated.h"

/// <summary>
/// 発見ウィジェット
/// </summary>
UCLASS()
class UFoundWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFoundWidget();

	/// <summary>
	/// アニメーション再生
	/// </summary>
	/// <param name="DirectionType"> 方向 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FoundWidget")
	void PlayFoundAnimation(EDirectionType DirectionType);

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

private:

	/// <summary>
	/// アニメーション再生
	/// </summary>
	/// <param name="DirectionType"> 方向 </param>
	/// <returns></returns>
	virtual void PlayFoundAnimation_Implementation(EDirectionType DirectionType);
};