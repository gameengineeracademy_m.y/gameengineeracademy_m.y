﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Battle/Widgets/Map/MapBaseWidget.h"
#include "MiniMapWidget.generated.h"


/// <summary>
/// ミニマップウィジェット
/// </summary>
UCLASS()
class UMiniMapWidget : public UMapBaseWidget
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UMiniMapWidget();

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="Geometry"> ジオメトリ </param>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void NativeTick(const FGeometry& Geometry, float DeltaTime) override;

	/// <summary>
	/// 発見マーカー再生
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, Category="MiniMapWidget")
	void PlayFoundMarkerAnimation();

	/// <summary>
	/// 発見マーカー停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, Category = "MiniMapWidget")
	void StopFoundMarkerAnimation();

	/// <summary>
	/// 発見マーカーキャンセル
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, Category = "MiniMapWidget")
	void CancelFoundMarkerAnimation();

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

	/// <summary>
	/// プレイヤー位置更新
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void UpdatePlayerIcon() override;

	/// <summary>
	/// エネミーアイコンの更新
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void UpdateEnemyIcon() override;

	/// <summary>
	/// アイテムアイコンの更新
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void UpdatePickupItemIcon() override;

	/// <summary>
	/// ゴールアイコンの更新
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void UpdateGoalIcon() override;

private:

	/// <summary>
	/// 発見マーカー再生
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayFoundMarkerAnimation_Implementation();

	/// <summary>
	/// 発見マーカー停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void StopFoundMarkerAnimation_Implementation();

	/// <summary>
	/// 発見マーカーキャンセル
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void CancelFoundMarkerAnimation_Implementation();

private:

	/// <summary>
	/// マップの回転中心座標
	/// </summary>
	FVector2D MapRotCenterPos;

	/// <summary>
	/// カメラの回転角度
	/// </summary>
	float CameraRotAngle;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// マップサイズ 最大値
	/// </summary>
	const float MapSize = 2048.0f;

	/// <summary>
	/// マップサイズ 半分の値
	/// </summary>
	const float MapHalfSize = 1024.0f;
};
