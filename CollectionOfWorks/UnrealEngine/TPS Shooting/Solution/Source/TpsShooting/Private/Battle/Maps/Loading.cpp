﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Maps/Loading.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ALoading::ALoading()
	: BattleController(nullptr)
	, CurrentPhase(ELoadPhaseType::Initialize) {

	// Tickイベントの有効化
	PrimaryActorTick.bCanEverTick = false;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ALoading::BeginPlay() {

	Super::BeginPlay();

	//---------------------------------------
	//変数の初期化処理
	//---------------------------------------
	CurrentPhase = ELoadPhaseType::Load;

	//---------------------------------------
	// 各ウィジェットの生成、表示準備処理
	// 各ウィジェットの表示処理
	//---------------------------------------
	// バトルコントローラを取得
	BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(BattleController)) {
		return;
	}
	BattleController->CreateUiForLoading();
	BattleController->PrepareOpenUiForLoading();
	BattleController->OpenUiForLoading();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ALoading::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}

/// <summary>
/// 現在のフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ALoading::ChangeCurrentPhase(ELoadPhaseType PhaseType) {

	CurrentPhase = PhaseType;
}