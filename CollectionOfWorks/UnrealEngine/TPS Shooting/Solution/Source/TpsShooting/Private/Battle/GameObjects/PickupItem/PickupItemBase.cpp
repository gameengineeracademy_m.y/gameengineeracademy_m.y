﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "Battle/GameObjects/PickupItem/PickupItemBase.h"
#include "Kismet/GameplayStatics.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
APickupItemBase::APickupItemBase()
	: ParticleSystemComp(nullptr)
	, PickupItem(nullptr)
	, PickupEffect(nullptr)
	, PickupSound(nullptr)
	, PickupItemType(EPickupItemType::None)
	, PickupType(EPickupType::None)
	, StageType(EStageType::None)
	, bIsPickup(false) {

	// Tickイベントを有効化
	PrimaryActorTick.bCanEverTick = true;

	// コリジョンコンポーネントの生成
	// ルートコンポーネントに指定
	CapsuleComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CollisionComp"));
	CapsuleComp->InitCapsuleSize(InitSizeX, InitSizeY);
	CapsuleComp->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	CapsuleComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CapsuleComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	CapsuleComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	RootComponent = CapsuleComp;

	// パーティクルシステムコンポーネントの生成
	ParticleSystemComp = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystemComp"));
	ParticleSystemComp->bAutoActivate = false;
	ParticleSystemComp->bAutoDestroy = false;
	ParticleSystemComp->SetupAttachment(RootComponent);
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APickupItemBase::BeginPlay() {

	Super::BeginPlay();
	
	PickupItemType = EPickupItemType::None;
	PickupType = EPickupType::None;
	bIsPickup = false;

	// Tickイベントは必要時以外無効にしておく
	SetActorTickEnabled(false);

	// アイテムをコンポーネントにセットし、実行
	if (!IsValid(PickupItem)) {
		return;
	}
	ParticleSystemComp->SetTemplate(PickupItem);
	ParticleSystemComp->ActivateSystem();

	// 追加エフェクト実行
	if (!IsValid(PickupItemEffect)) {
		return;
	}
	UGameplayStatics::SpawnEmitterAttached(PickupItemEffect, ParticleSystemComp);
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void APickupItemBase::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}

/// <summary>
/// アイテムの種類をセット
/// </summary>
/// <param name="Type"> アイテム収拾の種類 </param>
/// <returns></returns>
void APickupItemBase::SetPickupItemType(EPickupItemType Type) {

	PickupItemType = Type;
}

/// <summary>
/// アイテムの種類を取得
/// </summary>
/// <param name=""></param>
/// <returns> アイテム収拾の種類 </returns>
EPickupItemType APickupItemBase::GetPickupItemType() {

	return PickupItemType;
}

/// <summary>
/// アイテム収拾の種類をセット
/// </summary>
/// <param name="Type"> アイテム収拾の種類 </param>
/// <returns></returns>
void APickupItemBase::SetPickupType(EPickupType Type) {

	PickupType = Type;
}

/// <summary>
/// アイテム収拾の種類を取得
/// </summary>
/// <param name=""></param>
/// <returns> アイテム収拾の種類 </returns>
EPickupType APickupItemBase::GetPickupType() {

	return PickupType;
}

/// <summary>
/// 設置エリアの種類をセット
/// </summary>
/// <param name="Type"> エリアの種類 </param>
/// <returns></returns>
void APickupItemBase::SetStageType(EStageType Type) {

	StageType = Type;
}

/// <summary>
/// 設置エリアの種類を取得
/// </summary>
/// <param name=""></param>
/// <returns> エリアの種類 </returns>
EStageType APickupItemBase::GetStageType() {

	return StageType;
}

/// <summary>
/// パーティクルを停止する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APickupItemBase::DeactiveParticleSystem() {

	bIsPickup = true;
	ParticleSystemComp->DeactivateSystem();
}

/// <summary>
/// 再度拾うことができるようにする
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APickupItemBase::SetupPickupSystem() {

	if (!bIsPickup) {
		return;
	}

	bIsPickup = false;
	ParticleSystemComp->ActivateSystem();
	// ポーンとのコリジョン設定を「オーバーラップ」に変更
	CapsuleComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	// 透過率を不透明に再設定する
	ParticleSystemComp->SetFloatParameter(RenderParamName, RenderMaxValue);
}

/// <summary>
/// 拾われたかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:拾われた </returns>
bool APickupItemBase::IsPickup() {

	return bIsPickup;
}