﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/GameObjects/PickupItem/PickupItemHeal/PickupItemHeal.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
APickupItemHeal::APickupItemHeal() {

	// Tickイベントは無効化
	PrimaryActorTick.bCanEverTick = false;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APickupItemHeal::BeginPlay() {

	Super::BeginPlay();

	PickupItemType = EPickupItemType::Healing;
	PickupType = EPickupType::Permanent;
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void APickupItemHeal::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}
