﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "Battle/Enum/CommonEnum.h"
#include "SwitchAreaTriggerBox.generated.h"

/// <summary>
/// エリア切替用トリガーボックス
/// </summary>
UCLASS()
class ASwitchAreaTriggerBox : public ATriggerBox
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ASwitchAreaTriggerBox();

	/// <summary>
	/// 現在のエリアを更新する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category="SwitchAreaTriggerBox")
	void UpdateCurrentStageType();

private:

	/// <summary>
	/// 更新処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Update();

	/// <summary>
	/// セーブ処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Save();

public:

	/// <summary>
	/// エリアの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SwitchArea")
	EStageType SettingStageType;
};
