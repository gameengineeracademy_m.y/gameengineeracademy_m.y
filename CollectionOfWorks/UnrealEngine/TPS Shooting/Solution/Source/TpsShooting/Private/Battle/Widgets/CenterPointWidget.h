﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/UiAssignmentWidget.h"
#include "CenterPointWidget.generated.h"

/// <summary>
/// 照準の中心点ウィジェット
/// </summary>
UCLASS()
class UCenterPointWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UCenterPointWidget();

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;
};
