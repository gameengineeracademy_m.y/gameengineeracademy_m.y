﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Trigger/TriggerBox/SwitchAreaTriggerBox.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Battle/GameState/BattleGameState.h"
#include "Battle/Controller/BattleController.h"
#include "Battle/CharacterManager/PlayerManager/PlayerManager.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ASwitchAreaTriggerBox::ASwitchAreaTriggerBox()
	: SettingStageType(EStageType::None) {

}

/// <summary>
/// 現在のエリアを更新する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ASwitchAreaTriggerBox::UpdateCurrentStageType() {

	if (SettingStageType == EStageType::None) {
		return;
	}

	// エリア情報の更新処理
	ABattleGameState* BattleGameState = Cast<ABattleGameState>(GetWorld()->GetGameState());
	if (!IsValid(BattleGameState)) {
		return;
	}
	Update();

	// ボックスに設定されたエリアの種類よりも現在のエリアの種類が
	// 以前のエリアの場合、各種データを保存する
	EStageType CurrentStageType = BattleGameState->GetCurrentStageType();
	int32 CurrentStage = static_cast<int32>(CurrentStageType);
	int32 SettingStage = static_cast<int32>(SettingStageType);

	UE_LOG(LogTemp, Display, TEXT("FinishUpdate CurrentStage : %d"), CurrentStage)
	UE_LOG(LogTemp, Display, TEXT("FinishUpdate SettingStage : %d"), SettingStage)
	if (CurrentStage > SettingStage) {
		return;
	}
	Save();
}

/// <summary>
/// 更新処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ASwitchAreaTriggerBox::Update() {

	ABattleGameState* BattleGameState = Cast<ABattleGameState>(GetWorld()->GetGameState());
	if (!IsValid(BattleGameState)) {
		return;
	}
	ABattleController* BattleController = Cast<ABattleController>(UGameplayStatics::GetPlayerController(this, 0));
	if (!IsValid(BattleController)) {
		return;
	}

	//------------------------------------------------
	//トリガーボックスに設定エリアと現在のエリアが
	//異なる場合、現在のエリアにいる敵を強制的に
	//巡回状態にし、敵の視覚情報を停止する
	//現在のエリアにいる敵の目が見えるようにしておく
	//------------------------------------------------
	EStageType StageType = BattleGameState->GetCurrentStageType();
	if (StageType != SettingStageType) {

		AEnemyManager* EnemyManager = BattleGameState->GetEnemyManager();
		if (!IsValid(EnemyManager)) {
			return;
		}
		EnemyManager->StopEnemy(StageType);
		EnemyManager->SetAlertLevel(StageType, EAlertLevel::Patrol);
		EnemyManager->SetAlertLevel(SettingStageType, EAlertLevel::Patrol);

		bool IsCheck = BattleGameState->IsEnemySight();
		if (IsCheck) {
			EnemyManager->CanSeeEnemy(SettingStageType);
		}
	}

	//------------------------------------------------
	//現在のエリアを更新し、エリアマップを切り替える
	//------------------------------------------------
	int32 CurrentStage = static_cast<int32>(StageType);
	int32 SettingStage = static_cast<int32>(SettingStageType);
	UE_LOG(LogTemp, Display, TEXT("BeforeUpdate CurrentStage : %d"), CurrentStage)
	UE_LOG(LogTemp, Display, TEXT("BeforeUpdate SettingStage : %d"), SettingStage)

	BattleGameState->ChangeCurrentStageType(SettingStageType);
	BattleController->GetAreaMapWidget()->SwitchAreaMap(SettingStageType);
	BattleController->GetMiniMapWidget()->SetStageType(SettingStageType);
}

/// <summary>
/// セーブ処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ASwitchAreaTriggerBox::Save() {

	ABattleGameState* BattleGameState = Cast<ABattleGameState>(GetWorld()->GetGameState());
	if (!IsValid(BattleGameState)) {
		return;
	}
	APlayerManager* PlayerManager = BattleGameState->GetPlayerManager();
	if (!IsValid(PlayerManager)) {
		return;
	}
	AOperatePlayer* Player = PlayerManager->GetOperatePlayer();
	if (!IsValid(Player)) {
		return;
	}
	// エリアの種類の保存と所持武器の保存
	BattleGameState->SaveStageType(SettingStageType);
	PlayerManager->SaveEquipWeapon(Player->GetPossessWeaponList());
}