﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/GameState/BattleGameState.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ABattleGameState::ABattleGameState()
	: CurrentGameState(EGameStateType::None)
	, CurrentStageType(EStageType::First)
	, SavedStageType(EStageType::First)
	, PlayerManager(nullptr)
	, EnemyManager(nullptr)
	, bIsEnemySight(false)
	, bIsFinishCameraDirection(false) {

}

/// <summary>
/// 生成後実行イベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleGameState::BeginPlay() {

	CurrentGameState = EGameStateType::None;
	CurrentStageType = EStageType::First;
	SavedStageType = EStageType::First;

	// カメラ演出処理の終了フラグ
	bIsFinishCameraDirection = false;

	// エネミーの視覚情報
	// ※デバッグ用のフラグ
	bIsEnemySight = true;
}

/// <summary>
/// 現在のゲームの進行状況を変更する
/// </summary>
/// <param name="Type"> ゲームの進行状況 </param>
/// <returns></returns>
void ABattleGameState::ChangeCurrentGameState(EGameStateType Type) {

	CurrentGameState = Type;
}

/// <summary>
/// 現在のゲームの進行状況を取得する
/// </summary>
/// <param name=""></param>
/// <returns> ゲームの進行状況 </returns>
EGameStateType ABattleGameState::GetCurrentGameState() {

	return CurrentGameState;
}

/// <summary>
/// ゲームをプレイ中かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:プレイ中 </returns>
bool ABattleGameState::IsPlay() {

	return CurrentGameState == EGameStateType::Play;
}

/// <summary>
/// 進行状況のリセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleGameState::ResetGameState() {

	CurrentGameState = EGameStateType::Play;
}

/// <summary>
/// 現在の進行中のエリアを変更する
/// </summary>
/// <param name="Type"> 進行中のエリア </param>
/// <returns></returns>
void ABattleGameState::ChangeCurrentStageType(EStageType Type) {

	CurrentStageType = Type;
}

/// <summary>
/// 現在の進行中のエリアを取得する
/// </summary>
/// <param name=""></param>
/// <returns> 進行中のエリア </returns>
EStageType ABattleGameState::GetCurrentStageType() {

	return CurrentStageType;
}

/// <summary>
/// 進行中のエリアを保存する
/// </summary>
/// <param name="Type"> 進行中のエリア </param>
/// <returns></returns>
void ABattleGameState::SaveStageType(EStageType Type) {

	SavedStageType = Type;
}

/// <summary>
/// 保存済みのエリアを取得する
/// </summary>
/// <param name=""></param>
/// <returns> 保存済みのエリア </returns>
EStageType ABattleGameState::GetSavedStageType() {

	return SavedStageType;
}

/// <summary>
/// 保存されたデータを読みこむ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleGameState::LoadStageType() {

	CurrentStageType = SavedStageType;
}

/// <summary>
/// 進行中のエリアのリセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleGameState::ResetStageType() {

	CurrentStageType = EStageType::First;
}

/// <summary>
/// プレイヤーマネージャのセット
/// </summary>
/// <param name="Manager"> プレイヤーマネージャ </param>
/// <returns></returns>
void ABattleGameState::SetPlayerManager(APlayerManager* Manager) {

	PlayerManager = Manager;
}

/// <summary>
/// プレイヤーマネージャの取得
/// </summary>
/// <param name=""></param>
/// <returns> プレイヤーマネージャ </returns>
APlayerManager* ABattleGameState::GetPlayerManager() {

	return PlayerManager;
}

/// <summary>
/// エネミーマネージャのセット
/// </summary>
/// <param name="Manager"> エネミーマネージャ </param>
/// <returns></returns>
void ABattleGameState::SetEnemyManager(AEnemyManager* Manager) {

	EnemyManager = Manager;
}


/// <summary>
/// エネミーマネージャの取得
/// </summary>
/// <param name=""></param>
/// <returns> エネミーマネージャ </returns>
AEnemyManager* ABattleGameState::GetEnemyManager() {

	return EnemyManager;
}

/// <summary>
/// エネミー視覚情報設定
/// </summary>
/// <param name="bIsSight"> 視覚有無 </param>
/// <returns></returns>
void ABattleGameState::SetupDebugEnemySight(bool bIsSight) {

	bool IsPackage = UKismetSystemLibrary::IsPackagedForDistribution();
	if (IsPackage) {
		return;
	}
	bIsEnemySight = bIsSight;
}

/// <summary>
/// エネミー視覚情報の取得
/// </summary>
/// <param name=""></param>
/// <returns> true:見える </returns>
bool ABattleGameState::IsEnemySight() {

	return bIsEnemySight;
}

/// <summary>
/// カメラ演出処理フラグのセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleGameState::SetFinishCameraDirection() {

	bIsFinishCameraDirection = true;
}

/// <summary>
/// カメラ演出処理フラグのリセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleGameState::ResetFinishCameraDirection() {

	bIsFinishCameraDirection = false;
}

/// <summary>
/// カメラ演出が終了したかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:終了 </returns>
bool ABattleGameState::IsFinishCameraDirection() {

	return bIsFinishCameraDirection;
}