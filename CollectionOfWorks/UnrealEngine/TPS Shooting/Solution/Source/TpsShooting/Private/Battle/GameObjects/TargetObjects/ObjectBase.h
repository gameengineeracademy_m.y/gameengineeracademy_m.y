﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Battle/Enum/CommonEnum.h"
#include "ObjectBase.generated.h"

/// <summary>
/// ターゲットオブジェクトベースクラス
/// </summary>
UCLASS()
class AObjectBase : public AActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AObjectBase();

	/// <summary>
	/// エリアの種類をセット
	/// </summary>
	/// <param name="Type"> エリアの種類 </param>
	/// <returns></returns>
	void SetStageType(EStageType Type);

	/// <summary>
	/// エリアの種類を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> エリアの種類 </returns>
	EStageType GetStageType();

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

protected:

	/// <summary>
	/// エリアの種類
	/// </summary>
	EStageType StageType;
};
