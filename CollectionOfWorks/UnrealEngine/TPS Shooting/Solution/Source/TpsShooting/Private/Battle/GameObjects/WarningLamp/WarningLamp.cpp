﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/GameObjects/WarningLamp/WarningLamp.h"
#include "Kismet/GameplayStatics.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AWarningLamp::AWarningLamp()
	: LampMeshComp(nullptr)
	, LampMaterialInstance(nullptr)
	, WarningSound(nullptr)
	, BlinkingParamName()
	, InitBlinkingValue(0.0f)
	, BrightnessParamName()
	, BrightnessValue(0.0f)
	, FlashingSpeedParamName()
	, FlashingSpeedValue(0.0f)
	, StageType(EStageType::None)
	, WarningSoundComp(nullptr) {

	// Tick処理を無効化
	PrimaryActorTick.bCanEverTick = false;

	LampMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("LampMeshComponent"));
	LampMeshComp->SetupAttachment(RootComponent);
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWarningLamp::BeginPlay() {

	Super::BeginPlay();

	StageType = EStageType::None;

	Initialize();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AWarningLamp::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}

/// <summary>
/// 設置エリアのセット
/// </summary>
/// <param name="Type"> エリアの種類 </param>
/// <returns></returns>
void AWarningLamp::SetStageType(EStageType Type) {

	StageType = Type;
}

/// <summary>
/// 設置エリアのセット
/// </summary>
/// <param name=""></param>
/// <returns> エリアの種類 </returns>
EStageType AWarningLamp::GetStageType() {

	return StageType;
}

/// <summary>
/// 初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWarningLamp::Initialize() {

	if (!IsValid(LampMaterialInstance)) {
		return;
	}
	LampMaterialInstance->SetScalarParameterValue(BlinkingParamName, InitBlinkingValue);
	LampMaterialInstance->SetScalarParameterValue(BrightnessParamName, BrightnessValue);
	LampMaterialInstance->SetScalarParameterValue(FlashingSpeedParamName, FlashingSpeedValue);
}

/// <summary>
/// ランプを点灯する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWarningLamp::Light() {

	//---------------------------------------------
	// ランプを点灯し、サウンドを生成する
	//---------------------------------------------
	if (!IsValid(LampMaterialInstance)) {
		return;
	}
	LampMaterialInstance->SetScalarParameterValue(BlinkingParamName, LightValue);

	if (!IsValid(LampMeshComp) || !IsValid(WarningSound)) {
		return;
	}
	WarningSoundComp = UGameplayStatics::SpawnSoundAttached(WarningSound, LampMeshComp);
}

/// <summary>
/// ランプを消灯する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWarningLamp::TurnOff() {

	//---------------------------------------------
	// ランプを消灯し、サウンドを破棄する
	//---------------------------------------------
	if (!IsValid(LampMaterialInstance)) {
		return;
	}
	LampMaterialInstance->SetScalarParameterValue(BlinkingParamName, TurnOffValue);

	if (!IsValid(WarningSoundComp)) {
		return;
	}
	WarningSoundComp->Stop();
	WarningSoundComp->DestroyComponent();
}