﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "Battle/GameObjects/TargetObjects/TargetObjectManager.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ATargetObjectManager::ATargetObjectManager()
	: GoalObjectTable(nullptr)
	, GoalObjectList()
	, GoalIndexList() {

	// Tick処理を無効化
	PrimaryActorTick.bCanEverTick = false;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATargetObjectManager::BeginPlay() {

	Super::BeginPlay();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ATargetObjectManager::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}

/// <summary>
/// オブジェクト初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATargetObjectManager::SpawnObjects() {

	SpawnGoalObjects();
}

/// <summary>
/// ゴールオブジェクトクラスのロード
/// </summary>
/// <param name=""></param>
/// <returns> ゴールオブジェクトクラス </returns>
TSubclassOf<AGoalObject>  ATargetObjectManager::LoadGoalObjectClass() {

	FString Path;
	GConfig->GetString(TEXT("ActorSettings"), TEXT("GoalObjectPath"), Path, GGameIni);
	return TSoftClassPtr<AGoalObject>(FSoftObjectPath(*Path)).LoadSynchronous();
}

/// <summary>
/// ゴールオブジェクトの生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATargetObjectManager::SpawnGoalObjects() {


	if (!IsValid(GoalObjectTable)) {
		return;
	}

	// ゴールオブジェクトクラスのロード
	TSubclassOf<AGoalObject> GoalObjectClass = LoadGoalObjectClass();
	if (!GoalObjectClass) {
		UE_LOG(LogTemp, Display, TEXT("ATargetObjectManager GoalObjectClass Error"));
		return;
	}

	//---------------------------------------
	// 現在のレベルを取得
	//---------------------------------------
	UTpsShootingGameInstance* GameInstance = Cast<UTpsShootingGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (!IsValid(GameInstance)) {
		return;
	}
	ELevelType CurrentLevelType = GameInstance->GetCurrentLevel();

	int32 ListIndex = MinIndex;
	TArray<FName> RowNames = GoalObjectTable->GetRowNames();
	for (FName RowName : RowNames) {

		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		FTargetObjectSpawnData* GoalObjectSpawn = GoalObjectTable->FindRow<FTargetObjectSpawnData>(RowName, "");
		if (GoalObjectSpawn->BattleLevelType != CurrentLevelType) {
			continue;
		}

		FVector SpawnPosition = FVector::ZeroVector;
		FRotator SpawnRotation = FRotator::ZeroRotator;
		SpawnPosition = FVector(GoalObjectSpawn->PositionX, GoalObjectSpawn->PositionY, GoalObjectSpawn->PositionZ);

		//------------------------------------
		// ゴールオブジェクトのスポーン
		//------------------------------------
		FActorSpawnParameters Params;
		AGoalObject* GoalObject = GetWorld()->SpawnActor<AGoalObject>(GoalObjectClass, SpawnPosition, FRotator::ZeroRotator, Params);
		if (!IsValid(GoalObject)) {
			continue;
		}

		GoalObject->SetStageType(GoalObjectSpawn->StageType);
		// ゴールオブジェクトをリストに登録
		GoalObjectList.Add(ListIndex, GoalObject);
		GoalIndexList.Add(ListIndex);
		++ListIndex;
	}
}

/// <summary>
/// ゴールインデックスリストを取得
/// </summary>
/// <param name=""></param>
/// <returns> ゴールインデックスリスト </returns>
TArray<int32> ATargetObjectManager::GetListIndexArray() {

	return GoalIndexList;
}

/// <summary>
/// 指定のゴールオブジェクトを取得
/// </summary>
/// <param name="Index"> インデックス </param>
/// <returns> ゴールオブジェクト </returns>
AGoalObject* ATargetObjectManager::GetGoalObject(int32 Index) {

	if (!GoalObjectList.Contains(Index)) {
		return nullptr;
	}
	return GoalObjectList[Index];
}