﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Widgets/BattleHUDItem/BattleHUDWeaponIcon/BattleHUDWeaponIcon.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UBattleHUDWeaponIcon::UBattleHUDWeaponIcon() {

}

/// <summary>
/// 武器切り替え開始アニメーション
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleHUDWeaponIcon::PlaySwitchStartWeaponAnimation_Implementation() {

}

/// <summary>
/// 武器切り替え終了アニメーション
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleHUDWeaponIcon::PlaySwitchEndWeaponAnimation_Implementation() {

}