﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Trigger/TriggerBox/CameraDirectionTriggerBox.h"
#include "Battle/Interface/BattleEvent.h"
#include "Kismet/KismetMathLibrary.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ACameraDirectionTriggerBox::ACameraDirectionTriggerBox()
	: CameraDirectionType(ECameraDirectionType::None)
	, DirectionPlayerPos()
	, DirectionPlayerRot(0.0f)
	, PlayingLevelSequenceActor(nullptr)
	, bCancel(false)
	, EventInterface(nullptr)
	, CurrentTutorialPhase(ETutorialPhase::None)
	, CurrentFirstAreaPhase(EFirstAreaPhase::None)
	, CurrentSecondAreaPhase(ESecondAreaPhase::None)
	, CurrentThirdAreaPhase(EThirdAreaPhase::None)
	, CurrentFoundGoalPhase(EFoundGoalPhase::None)
	, CurrentStartScenePhase(EStartScenePhase::None)
	, CurrentFinishScenePhase(EFinishScenePhase::None)
	, AccumulateTime(0.0f)
	, InitCameraPos()
	, EnemyIndex(0) {

	// Tickイベントの有効化
	PrimaryActorTick.bCanEverTick = true;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACameraDirectionTriggerBox::BeginPlay() {

	Super::BeginPlay();

	// Tick処理は必要時以外止めておく
	SetActorTickEnabled(false);

	CurrentTutorialPhase = ETutorialPhase::None;
	CurrentFirstAreaPhase = EFirstAreaPhase::None;
	CurrentSecondAreaPhase = ESecondAreaPhase::None;
	CurrentThirdAreaPhase = EThirdAreaPhase::None;
	CurrentFoundGoalPhase = EFoundGoalPhase::None;
	CurrentStartScenePhase = EStartScenePhase::None;
	CurrentFinishScenePhase = EFinishScenePhase::None;

	bCancel = false;
	InitCameraPos = FVector::ZeroVector;
	AccumulateTime = ResetTime;
	EnemyIndex = MinIndex;
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ACameraDirectionTriggerBox::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

	//---------------------------------
	// カメラ演出 開始フェード処理
	//---------------------------------
	ProcessStartScene(DeltaTime);

	//---------------------------------
	// カメラ演出処理
	//---------------------------------
	ProcessScene(DeltaTime);

	//---------------------------------
	// カメラ演出 終了フェード処理
	//---------------------------------
	ProcessFinishScene(DeltaTime);
}

/// <summary>
/// イベントインターフェース
/// </summary>
/// <param name=""> イベントインターフェース </param>
/// <returns></returns>
void ACameraDirectionTriggerBox::SetEventInterface(const TScriptInterface<IBattleEvent>& Interface) {

	if (EventInterface != nullptr) {
		return;
	}
	EventInterface = Interface;
}

/// <summary>
/// カメラ演出の設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACameraDirectionTriggerBox::SetupCameraDirection() {


	switch (CameraDirectionType) {
	case ECameraDirectionType::None: 
	case ECameraDirectionType::MaxTypeIndex: {

		EventInterface->OnFinishCameraDirection();
		break;
	}
	default: {

		// 現在のシーン開始フェーズを「フェードアウト開始」に変更
		ChangeCurrentStartScenePhase(EStartScenePhase::StartFadeOut);
		// Tick処理を実行する
		SetActorTickEnabled(true);
		break;
	}
	}
}

/// <summary>
/// 現在開始処理中かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:開始処理中 </returns>
bool ACameraDirectionTriggerBox::IsStartPhase() {

	bool bIsStart = false;

	switch (CurrentStartScenePhase) {
	case EStartScenePhase::StartFadeOut:
	case EStartScenePhase::FadeOut:
	case EStartScenePhase::FinishFadeOut:
	case EStartScenePhase::Setup: {

		bIsStart = true;
		break;
	}
	}

	return bIsStart;
}

/// <summary>
/// 現在終了処理中かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:終了処理中 </returns>
bool ACameraDirectionTriggerBox::IsFinishPhase() {

	return CurrentFinishScenePhase != EFinishScenePhase::None;
}

/// <summary>
/// ムービーをキャンセルする
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACameraDirectionTriggerBox::SetupCancel() {

	//----------------------------------------------------
	//中止フラグをセット、終了処理を開始する
	// ※実行中のシーケンサは現時点ではそのまま
	//   実行中のシーケンサは終了処理時にポーズする
	//----------------------------------------------------
	bCancel = true;
	ChangeCurrentFinishScenePhase(EFinishScenePhase::StartFadeOut);
}

/// <summary>
/// カットシーン処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ACameraDirectionTriggerBox::ProcessScene(float DeltaTime) {

	//---------------------------------
	// カメラ演出の種類ごとの処理
	//---------------------------------
	switch (CameraDirectionType) {
	case ECameraDirectionType::Tutorial: {

		ProcessTutorialScene(DeltaTime);
		break;
	}
	case ECameraDirectionType::FirstArea: {

		ProcessFirstAreaScene(DeltaTime);
		break;
	}
	case ECameraDirectionType::SecondArea: {

		ProcessSecondAreaScene(DeltaTime);
		break;
	}
	case ECameraDirectionType::ThirdArea: {

		ProcessThirdAreaScene(DeltaTime);
		break;
	}
	case ECameraDirectionType::FoundGoal: {

		ProcessFoundGoalScene(DeltaTime);
		break;
	}
	default: {

		break;
	}
	}
}

/// <summary>
/// チュートリアルのシーン処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ACameraDirectionTriggerBox::ProcessTutorialScene(float DeltaTime) {

	switch (CurrentTutorialPhase) {
	case ETutorialPhase::SpawnEnemy01: {

		AEnemy* Enemy = GetEnemy(EnemyIndex);
		++EnemyIndex;
		if (!IsValid(Enemy)) {

			ChangeCurrentTutorialPhase(ETutorialPhase::SpawnEnemy02);
		}
		else {
			// エネミーのディゾルブフェーズを変更
			// 演出用のカメラ開始処理
			Enemy->ChangeDissolvePhase(EDissolvePhase::Appearance);
			EventInterface->OnSwitchDirectionCamera();
			ChangeCurrentTutorialPhase(ETutorialPhase::WaitSpawn01);
		}
		break;
	}
	case ETutorialPhase::WaitSpawn01: {

		bool bIsFinish = IsFinishCameraDirection();
		if (!bIsFinish) {
			break;
		}
		ResetFinishCameraDirection();
		ChangeCurrentTutorialPhase(ETutorialPhase::SpawnEnemy02);
		break;
	}
	case ETutorialPhase::SpawnEnemy02: {

		AEnemy* Enemy = GetEnemy(EnemyIndex);
		++EnemyIndex;
		if (!IsValid(Enemy)) {

			ChangeCurrentTutorialPhase(ETutorialPhase::SpawnEnemy03);
		}
		else {
			// エネミーのディゾルブフェーズを変更
			// 演出用のカメラ開始処理
			Enemy->ChangeDissolvePhase(EDissolvePhase::Appearance);
			EventInterface->OnSwitchDirectionCamera();
			ChangeCurrentTutorialPhase(ETutorialPhase::WaitSpawn02);
		}
		break;
	}
	case ETutorialPhase::WaitSpawn02: {

		bool bIsFinish = IsFinishCameraDirection();
		if (!bIsFinish) {
			break;
		}
		ResetFinishCameraDirection();
		ChangeCurrentTutorialPhase(ETutorialPhase::SpawnEnemy03);
		break;
	}
	case ETutorialPhase::SpawnEnemy03: {

		AEnemy* Enemy = GetEnemy(EnemyIndex);
		++EnemyIndex;
		if (!IsValid(Enemy)) {

			ChangeCurrentTutorialPhase(ETutorialPhase::SpawnEnemy04);
		}
		else {
			// エネミーのディゾルブフェーズを変更
			// 演出用のカメラ開始処理
			Enemy->ChangeDissolvePhase(EDissolvePhase::Appearance);
			EventInterface->OnSwitchDirectionCamera();
			ChangeCurrentTutorialPhase(ETutorialPhase::WaitSpawn03);
		}
		break;
	}
	case ETutorialPhase::WaitSpawn03: {

		bool bIsFinish = IsFinishCameraDirection();
		if (!bIsFinish) {
			break;
		}
		ResetFinishCameraDirection();
		ChangeCurrentTutorialPhase(ETutorialPhase::SpawnEnemy04);
		break;
	}
	case ETutorialPhase::SpawnEnemy04: {

		AEnemy* Enemy = GetEnemy(EnemyIndex);
		++EnemyIndex;
		if (!IsValid(Enemy)) {

			ChangeCurrentTutorialPhase(ETutorialPhase::WaitFinish);
		}
		else {
			// エネミーのディゾルブフェーズを変更
			// 演出用のカメラ開始処理
			Enemy->ChangeDissolvePhase(EDissolvePhase::Appearance);
			EventInterface->OnSwitchDirectionCamera();
			ChangeCurrentTutorialPhase(ETutorialPhase::WaitSpawn04);
		}
		break;
	}
	case ETutorialPhase::WaitSpawn04: {

		bool bIsFinish = IsFinishCameraDirection();
		if (!bIsFinish) {
			break;
		}
		ResetFinishCameraDirection();
		ChangeCurrentTutorialPhase(ETutorialPhase::Finish);
		break;
	}
	case ETutorialPhase::WaitFinish: {

		ChangeCurrentTutorialPhase(ETutorialPhase::Finish);
		break;
	}
	case ETutorialPhase::Finish: {

		ChangeCurrentFinishScenePhase(EFinishScenePhase::SetupMoveCamera);
		ChangeCurrentTutorialPhase(ETutorialPhase::None);
		break;
	}
	default: {

		break;
	}
	}
}

/// <summary>
/// 第1エリアのシーン処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ACameraDirectionTriggerBox::ProcessFirstAreaScene(float DeltaTime) {


	switch (CurrentFirstAreaPhase) {
	case EFirstAreaPhase::MoveCamera01: {

		// 演出用のカメラ処理開始
		EventInterface->OnSwitchDirectionCamera();
		ChangeCurrentFirstAreaPhase(EFirstAreaPhase::WaitCamera01);
		break;
	}
	case EFirstAreaPhase::WaitCamera01: {

		bool bIsFinish = IsFinishCameraDirection();
		if (!bIsFinish) {
			break;
		}
		ResetFinishCameraDirection();
		ChangeCurrentFirstAreaPhase(EFirstAreaPhase::MoveCamera02);
		break;
	}
	case EFirstAreaPhase::MoveCamera02: {

		// 演出用のカメラ処理開始
		EventInterface->OnSwitchDirectionCamera();
		ChangeCurrentFirstAreaPhase(EFirstAreaPhase::WaitCamera02);
		break;
	}
	case EFirstAreaPhase::WaitCamera02: {

		bool bIsFinish = IsFinishCameraDirection();
		if (!bIsFinish) {
			break;
		}
		ResetFinishCameraDirection();
		ChangeCurrentFirstAreaPhase(EFirstAreaPhase::Finish);
		break;
	}
	case EFirstAreaPhase::Finish: {

		if (!IsFinishPhase()) {
			ChangeCurrentFinishScenePhase(EFinishScenePhase::StartFadeOut);
		}
		ChangeCurrentFirstAreaPhase(EFirstAreaPhase::None);
		break;
	}
	default: {

		break;
	}
	}
}

/// <summary>
/// 第2エリアのシーン処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ACameraDirectionTriggerBox::ProcessSecondAreaScene(float DeltaTime) {


	switch (CurrentSecondAreaPhase) {
	case ESecondAreaPhase::MoveCamera01: {

		// 演出用のカメラ処理開始
		EventInterface->OnSwitchDirectionCamera();
		ChangeCurrentSecondAreaPhase(ESecondAreaPhase::WaitCamera01);
		break;
	}
	case ESecondAreaPhase::WaitCamera01: {

		bool bIsFinish = IsFinishCameraDirection();
		if (!bIsFinish) {
			break;
		}
		ResetFinishCameraDirection();
		ChangeCurrentSecondAreaPhase(ESecondAreaPhase::MoveCamera02);
		break;
	}
	case ESecondAreaPhase::MoveCamera02: {

		// 演出用のカメラ処理開始
		EventInterface->OnSwitchDirectionCamera();
		ChangeCurrentSecondAreaPhase(ESecondAreaPhase::WaitCamera02);
		break;
	}
	case ESecondAreaPhase::WaitCamera02: {

		bool bIsFinish = IsFinishCameraDirection();
		if (!bIsFinish) {
			break;
		}
		ResetFinishCameraDirection();
		ChangeCurrentSecondAreaPhase(ESecondAreaPhase::MoveCamera03);
		break;
	}
	case ESecondAreaPhase::MoveCamera03: {

		// 演出用のカメラ処理開始
		EventInterface->OnSwitchDirectionCamera();
		ChangeCurrentSecondAreaPhase(ESecondAreaPhase::WaitCamera03);
		break;
	}
	case ESecondAreaPhase::WaitCamera03: {

		bool bIsFinish = IsFinishCameraDirection();
		if (!bIsFinish) {
			break;
		}
		ResetFinishCameraDirection();
		ChangeCurrentSecondAreaPhase(ESecondAreaPhase::Finish);
		break;
	}
	case ESecondAreaPhase::Finish: {

		if (!IsFinishPhase()) {
			ChangeCurrentFinishScenePhase(EFinishScenePhase::StartFadeOut);
		}
		ChangeCurrentSecondAreaPhase(ESecondAreaPhase::None);
		break;
	}
	default: {

		break;
	}
	}
}

/// <summary>
/// 第3エリアのシーン処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ACameraDirectionTriggerBox::ProcessThirdAreaScene(float DeltaTime) {


	switch (CurrentThirdAreaPhase) {
	case EThirdAreaPhase::MoveCamera01: {

		// 演出用のカメラ処理開始
		EventInterface->OnSwitchDirectionCamera();
		ChangeCurrentThirdAreaPhase(EThirdAreaPhase::WaitCamera01);
		break;
	}
	case EThirdAreaPhase::WaitCamera01: {

		bool bIsFinish = IsFinishCameraDirection();
		if (!bIsFinish) {
			break;
		}
		ResetFinishCameraDirection();
		ChangeCurrentThirdAreaPhase(EThirdAreaPhase::MoveCamera02);
		break;
	}
	case EThirdAreaPhase::MoveCamera02: {

		// 演出用のカメラ処理開始
		EventInterface->OnSwitchDirectionCamera();
		ChangeCurrentThirdAreaPhase(EThirdAreaPhase::WaitCamera02);
		break;
	}
	case EThirdAreaPhase::WaitCamera02: {

		bool bIsFinish = IsFinishCameraDirection();
		if (!bIsFinish) {
			break;
		}
		ResetFinishCameraDirection();
		ChangeCurrentThirdAreaPhase(EThirdAreaPhase::Finish);
		break;
	}
	case EThirdAreaPhase::Finish: {

		if (!IsFinishPhase()) {
			ChangeCurrentFinishScenePhase(EFinishScenePhase::StartFadeOut);
		}
		ChangeCurrentThirdAreaPhase(EThirdAreaPhase::None);
		break;
	}
	default: {

		break;
	}
	}
}

/// <summary>
/// ゴール発見のシーン処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ACameraDirectionTriggerBox::ProcessFoundGoalScene(float DeltaTime) {


	switch (CurrentFoundGoalPhase) {
	case EFoundGoalPhase::MoveCamera01: {

		// 演出用のカメラ処理開始
		EventInterface->OnSwitchDirectionCamera();
		ChangeCurrentFoundGoalPhase(EFoundGoalPhase::WaitCamera01);
		break;
	}
	case EFoundGoalPhase::WaitCamera01: {

		bool bIsFinish = IsFinishCameraDirection();
		if (!bIsFinish) {
			break;
		}
		ResetFinishCameraDirection();
		ChangeCurrentFoundGoalPhase(EFoundGoalPhase::Finish);
		break;
	}
	case EFoundGoalPhase::Finish: {

		if (!IsFinishPhase()) {
			ChangeCurrentFinishScenePhase(EFinishScenePhase::StartFadeOut);
		}
		ChangeCurrentFoundGoalPhase(EFoundGoalPhase::None);
		break;
	}
	default: {

		break;
	}
	}
}

/// <summary>
/// シーン開始処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ACameraDirectionTriggerBox::ProcessStartScene(float DeltaTime) {

	switch (CurrentStartScenePhase) {
	case EStartScenePhase::StartFadeOut: {
		//---------------------------------------
		// フェードアウト開始処理
		//---------------------------------------
		UFadeWidget* FadeWidget = GetFadeWidget();
		if (!IsValid(FadeWidget)) {
			ChangeCurrentFinishScenePhase(EFinishScenePhase::SetupCamera);
			break;
		}
		// フェードアウト実行
		FadeWidget->Open();
		FadeWidget->PlayFadeOutAnimation();

		ChangeCurrentStartScenePhase(EStartScenePhase::FadeOut);
		break;
	}
	case EStartScenePhase::FadeOut: {
		//---------------------------------------
		// フェードアウト中処理
		//---------------------------------------
		UFadeWidget* FadeWidget = GetFadeWidget();
		if (!IsValid(FadeWidget)) {
			ChangeCurrentFinishScenePhase(EFinishScenePhase::Finish);
			break;
		}
		bool IsFinish = FadeWidget->IsAnimFinish();
		if (!IsFinish) {
			break;
		}
		// フェードウィジェットのフラグリセット
		FadeWidget->ResetAnimFlag();
		ChangeCurrentStartScenePhase(EStartScenePhase::FinishFadeOut);
		break;
	}
	case EStartScenePhase::FinishFadeOut: {
		//---------------------------------------
		// フェードアウト終了処理
		//---------------------------------------
		ChangeCurrentStartScenePhase(EStartScenePhase::Setup);
		break;
	}
	case EStartScenePhase::Setup: {
		//---------------------------------------
		// 準備処理
		//---------------------------------------
		// プレイヤーの位置と向きの設定
		SetupPlayerPosAndAngle();
		// カメラ演出時用の操作に切り替える
		SwitchOperate();

		AccumulateTime = ResetTime;
		ChangeCurrentStartScenePhase(EStartScenePhase::Wait);
		break;
	}
	case EStartScenePhase::Wait: {
		//---------------------------------------
		// 待機処理
		//---------------------------------------
		AccumulateTime += DeltaTime;
		if (AccumulateTime < FadeWaitTime) {
			break;
		}
		AccumulateTime = ResetTime;

		//---------------------------------
		// 設定されているカメラ演出の種類に
		// 応じてフェーズの初期設定を行う
		//---------------------------------
		switch (CameraDirectionType) {
		case ECameraDirectionType::Tutorial: {

			CurrentTutorialPhase = ETutorialPhase::SpawnEnemy01;
			break;
		}
		case ECameraDirectionType::FirstArea: {

			CurrentFirstAreaPhase = EFirstAreaPhase::MoveCamera01;
			break;
		}
		case ECameraDirectionType::SecondArea: {

			CurrentSecondAreaPhase = ESecondAreaPhase::MoveCamera01;
			break;
		}
		case ECameraDirectionType::ThirdArea: {

			CurrentThirdAreaPhase = EThirdAreaPhase::MoveCamera01;
			break;
		}
		case ECameraDirectionType::FoundGoal: {

			CurrentFoundGoalPhase = EFoundGoalPhase::MoveCamera01;
			break;
		}
		}

		ChangeCurrentStartScenePhase(EStartScenePhase::StartFadeIn);
		break;
	}
	case EStartScenePhase::StartFadeIn: {
		//---------------------------------------
		// フェードイン開始処理
		//---------------------------------------
		UFadeWidget* FadeWidget = GetFadeWidget();
		if (!IsValid(FadeWidget)) {
			ChangeCurrentFinishScenePhase(EFinishScenePhase::Finish);
			break;
		}
		// フェードイン実行
		FadeWidget->Open();
		FadeWidget->PlayFadeInAnimation();

		ChangeCurrentStartScenePhase(EStartScenePhase::FadeIn);
		break;
	}
	case EStartScenePhase::FadeIn: {
		//---------------------------------------
		// フェードイン中処理
		//---------------------------------------
		UFadeWidget* FadeWidget = GetFadeWidget();
		if (!IsValid(FadeWidget)) {
			ChangeCurrentFinishScenePhase(EFinishScenePhase::Finish);
			break;
		}
		bool IsFinish = FadeWidget->IsAnimFinish();
		if (!IsFinish) {
			break;
		}
		// フェードウィジェットのフラグリセット
		FadeWidget->ResetAnimFlag();
		FadeWidget->Closed();
		ChangeCurrentStartScenePhase(EStartScenePhase::FinishFadeIn);
		break;
	}
	case EStartScenePhase::FinishFadeIn: {
		//---------------------------------------
		// フェードイン終了処理
		//---------------------------------------
		ChangeCurrentStartScenePhase(EStartScenePhase::Finish);
		break;
	}
	case EStartScenePhase::Finish: {
		//---------------------------------------
		// 終了処理
		//---------------------------------------		
		ChangeCurrentStartScenePhase(EStartScenePhase::None);
		break;
	}
	default: {

		break;
	}
	}
}

/// <summary>
/// シーン終了処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ACameraDirectionTriggerBox::ProcessFinishScene(float DeltaTime) {

	switch (CurrentFinishScenePhase) {
	case EFinishScenePhase::StartFadeOut: {
		//---------------------------------------
		// フェードアウト開始処理
		//---------------------------------------
		// カメラ演出時用の操作に切り替える
		SwitchOperate();

		UFadeWidget* FadeWidget = GetFadeWidget();
		if (!IsValid(FadeWidget)) {
			ChangeCurrentFinishScenePhase(EFinishScenePhase::SetupCamera);
			break;
		}
		// フェードアウト実行
		FadeWidget->Open();
		FadeWidget->PlayFadeOutAnimation();

		ChangeCurrentFinishScenePhase(EFinishScenePhase::FadeOut);
		break;
	}
	case EFinishScenePhase::FadeOut: {
		//---------------------------------------
		// フェードアウト中
		//---------------------------------------
		UFadeWidget* FadeWidget = GetFadeWidget();
		if (!IsValid(FadeWidget)) {
			ChangeCurrentFinishScenePhase(EFinishScenePhase::Finish);
			break;
		}
		bool IsFinish = FadeWidget->IsAnimFinish();
		if (!IsFinish) {
			break;
		}

		// フェードウィジェットのフラグリセット
		FadeWidget->ResetAnimFlag();
		ChangeCurrentFinishScenePhase(EFinishScenePhase::FinishFadeOut);
		break;
	}
	case EFinishScenePhase::FinishFadeOut: {
		//---------------------------------------
		// フェードアウト終了処理
		//---------------------------------------
		ChangeCurrentFinishScenePhase(EFinishScenePhase::Cancel);
		break;
	}
	case EFinishScenePhase::Cancel: {
		//---------------------------------------
		// キャンセル処理
		// ※スキップ処理実行時のみ実行
		//---------------------------------------
		if (bCancel) {
			PauseMovie();
		}
		ChangeCurrentFinishScenePhase(EFinishScenePhase::SetupCamera);
		break;
	}
	case EFinishScenePhase::SetupCamera: {
		//---------------------------------------
		// カメラ設定処理
		//---------------------------------------
		ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
		AOperatePlayer* Player = GetOperatePlayer();
		if (IsValid(Player) && IsValid(BattleController)) {
			// プレイヤーカメラから映す
			BattleController->SetViewTargetWithBlend(Player);
		}

		AccumulateTime = ResetTime;
		ChangeCurrentFinishScenePhase(EFinishScenePhase::Wait);
		break;
	}
	case EFinishScenePhase::Wait: {
		//---------------------------------------
		// 待機処理
		//---------------------------------------
		AccumulateTime += DeltaTime;
		if (AccumulateTime < FadeWaitTime) {
			break;
		}
		AccumulateTime = ResetTime;

		ChangeCurrentFinishScenePhase(EFinishScenePhase::StartFadeIn);
		break;
	}
	case EFinishScenePhase::StartFadeIn: {
		//---------------------------------------
		// フェードイン開始処理
		//---------------------------------------
		UFadeWidget* FadeWidget = GetFadeWidget();
		if (!IsValid(FadeWidget)) {
			ChangeCurrentFinishScenePhase(EFinishScenePhase::Finish);
			break;
		}
		// フェードイン実行
		FadeWidget->Open();
		FadeWidget->PlayFadeInAnimation();

		ChangeCurrentFinishScenePhase(EFinishScenePhase::FadeIn);
		break;
	}
	case EFinishScenePhase::FadeIn: {
		//---------------------------------------
		// フェードイン中
		//---------------------------------------
		UFadeWidget* FadeWidget = GetFadeWidget();
		if (!IsValid(FadeWidget)) {
			ChangeCurrentFinishScenePhase(EFinishScenePhase::Finish);
			break;
		}
		bool IsFinish = FadeWidget->IsAnimFinish();
		if (!IsFinish) {
			break;
		}
		// フェードウィジェットのフラグリセット
		FadeWidget->ResetAnimFlag();
		FadeWidget->Closed();
		ChangeCurrentFinishScenePhase(EFinishScenePhase::FinishFadeIn);
		break;
	}
	case EFinishScenePhase::FinishFadeIn: {
		//---------------------------------------
		// フェードイン終了処理
		//---------------------------------------
		ChangeCurrentFinishScenePhase(EFinishScenePhase::Finish);
		break;
	}
	case EFinishScenePhase::SetupMoveCamera: {
		//---------------------------------------
		// カメラ移動準備処理
		//---------------------------------------
		// カメラ演出時用の操作に切り替える
		SwitchOperate();

		AOperatePlayer* Player = GetOperatePlayer();
		if (IsValid(Player)) {

			FVector CurCameraPos = InitCameraPos;
			CurCameraPos.Z += AddCameraHeight;
			// カメラに値を渡す
			Player->FollowCamera->SetRelativeLocation(CurCameraPos);
		}

		ChangeCurrentFinishScenePhase(EFinishScenePhase::SwitchCamera);
		break;
	}
	case EFinishScenePhase::SwitchCamera: {
		//---------------------------------------
		// カメラ切り替え処理
		//---------------------------------------
		ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
		AOperatePlayer* Player = GetOperatePlayer();
		if (IsValid(Player) && IsValid(BattleController)) {
			// プレイヤーカメラから映す
			BattleController->SetViewTargetWithBlend(Player);
		}

		ChangeCurrentFinishScenePhase(EFinishScenePhase::WaitCamera);
		break;
	}
	case EFinishScenePhase::WaitCamera: {
		//---------------------------------------
		// カメラ移動待機処理
		//---------------------------------------
		AccumulateTime += DeltaTime;
		if (AccumulateTime < BeforeMoveCameraWaitTime) {
			break;
		}
		AccumulateTime = ResetTime;

		ChangeCurrentFinishScenePhase(EFinishScenePhase::MoveCamera);
		break;
	}
	case EFinishScenePhase::MoveCamera: {
		//---------------------------------------
		// カメラ移動処理
		//---------------------------------------
		// カメラの位置を徐々に下げてくる
		AOperatePlayer* Player = GetOperatePlayer();
		if (!IsValid(Player)) {
			ChangeCurrentFinishScenePhase(EFinishScenePhase::FinishMoveCamera);
			break;
		}

		FVector CurCameraPos = Player->FollowCamera->GetRelativeLocation();
		FVector MovePos = UKismetMathLibrary::VInterpTo(CurCameraPos, InitCameraPos, DeltaTime, CameraHeightLowerSpeed);
		Player->FollowCamera->SetRelativeLocation(MovePos);
		if ((MovePos.Z - InitCameraPos.Z) > HeightAllowableValue) {
			break;
		}
		Player->FollowCamera->SetRelativeLocation(InitCameraPos);

		ChangeCurrentFinishScenePhase(EFinishScenePhase::FinishMoveCamera);
		break;
	}
	case EFinishScenePhase::FinishMoveCamera: {
		//---------------------------------------
		// カメラ移動終了処理
		//---------------------------------------

		ChangeCurrentFinishScenePhase(EFinishScenePhase::Finish);
		break;
	}
	case EFinishScenePhase::Finish: {
		//---------------------------------------
		// 終了処理
		//---------------------------------------
		// レベルへ終了通知
		EventInterface->OnFinishCameraDirection();
		ChangeCurrentFinishScenePhase(EFinishScenePhase::None);

		// Tick処理を止めておく
		SetActorTickEnabled(false);
		break;
	}
	default: {

		break;
	}
	}
}

/// <summary>
/// プレイヤーの位置と向きの設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACameraDirectionTriggerBox::SetupPlayerPosAndAngle() {

	ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
	AOperatePlayer* Player = GetOperatePlayer();
	if (!IsValid(Player) || !IsValid(BattleController)) {
		return;
	}

	// プレイヤーの向きを指定の値にセット
	FRotator PlayerRot = Player->GetActorRotation();
	PlayerRot.Yaw = DirectionPlayerRot;
	// プレイヤーの位置と向きを設定
	Player->SetActorLocation(DirectionPlayerPos);
	Player->SetActorRotation(PlayerRot);
	BattleController->SetControlRotation(PlayerRot);

	// カメラの初期座標を取得しておく
	FVector CurCameraPos = Player->FollowCamera->GetRelativeLocation();
	InitCameraPos = CurCameraPos;
}

/// <summary>
/// カメラ演出時用の操作に切り替える
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACameraDirectionTriggerBox::SwitchOperate() {

	ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(BattleController)) {
		return;
	}

	switch (CurrentStartScenePhase) {
	case EStartScenePhase::Setup: {

		// キーバインドの変更
		BattleController->AddInputType(EBattleInputType::CameraDirection);
		// 操作案内を表示
		UCameraGuideWidget* GuideWidget = BattleController->GetCameraGuideWidget();
		if (IsValid(GuideWidget)) {
			if (!GuideWidget->IsDisplay()) {
				GuideWidget->Open();
			}
		}
		return;
	}
	}

	switch (CurrentFinishScenePhase) {
	case EFinishScenePhase::StartFadeOut:
	case EFinishScenePhase::SetupMoveCamera: {

		// キーバインドの変更
		BattleController->DeleteInputType();
		// 操作案内を非表示
		UCameraGuideWidget* GuideWidget = BattleController->GetCameraGuideWidget();
		if (IsValid(GuideWidget)) {
			if (GuideWidget->IsDisplay()) {
				GuideWidget->Closed();
			}
		}
		return;
	}
	}
}

/// <summary>
/// フェードウィジェットを取得する
/// </summary>
/// <param name=""></param>
/// <returns> フェードウィジェット </returns>
UFadeWidget* ACameraDirectionTriggerBox::GetFadeWidget() {

	ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(BattleController)) {
		return nullptr;
	}

	return BattleController->GetFadeWidget();
}

/// <summary>
/// 操作プレイヤーを取得する
/// </summary>
/// <param name=""></param>
/// <returns> 操作プレイヤー </returns>
AOperatePlayer* ACameraDirectionTriggerBox::GetOperatePlayer() {

	ABattleGameState* BattleGameState = Cast<ABattleGameState>(GetWorld()->GetGameState());
	if (!IsValid(BattleGameState)) {
		return nullptr;
	}

	APlayerManager* PlayerManager = BattleGameState->GetPlayerManager();
	if (!IsValid(PlayerManager)) {
		return nullptr;
	}

	return PlayerManager->GetOperatePlayer();
}

/// <summary>
/// エネミーを取得する
/// </summary>
/// <param name="Index"> インデックス </param>
/// <returns> エネミー </returns>
AEnemy* ACameraDirectionTriggerBox::GetEnemy(int32 Index) {

	ABattleGameState* BattleGameState = Cast<ABattleGameState>(GetWorld()->GetGameState());
	if (!IsValid(BattleGameState)) {
		return nullptr;
	}

	AEnemyManager* EnemyManager = BattleGameState->GetEnemyManager();
	if (!IsValid(EnemyManager)) {
		return nullptr;
	}

	return EnemyManager->GetEnemy(Index);
}

/// <summary>
/// シーケンサが終了フラグのリセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACameraDirectionTriggerBox::ResetFinishCameraDirection() {

	ABattleGameState* BattleGameState = Cast<ABattleGameState>(GetWorld()->GetGameState());
	if (!IsValid(BattleGameState)) {
		return;
	}
	BattleGameState->ResetFinishCameraDirection();
}

/// <summary>
/// シーケンサが終了したかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:終了 </returns>
bool ACameraDirectionTriggerBox::IsFinishCameraDirection() {

	ABattleGameState* BattleGameState = Cast<ABattleGameState>(GetWorld()->GetGameState());
	if (!IsValid(BattleGameState)) {
		return true;
	}

	return BattleGameState->IsFinishCameraDirection();
}

/// <summary>
/// カメラ演出の停止処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACameraDirectionTriggerBox::PauseMovie() {

	// フェーズ切り替えの停止
	StopCameraDirectionPhase();

	//--------------------------------
	//再生中のムービーを一時停止する
	//--------------------------------
	if (!IsValid(PlayingLevelSequenceActor)) {
		return;
	}
	ULevelSequencePlayer* LSPlayer = PlayingLevelSequenceActor->GetSequencePlayer();
	if (!IsValid(LSPlayer)) {
		return;
	}
	bool bIsPause = LSPlayer->IsPaused();
	if (bIsPause) {
		return;
	}
	LSPlayer->Pause();
}

/// <summary>
/// カメラ演出のフェーズ切り替え停止
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACameraDirectionTriggerBox::StopCameraDirectionPhase() {

	switch (CameraDirectionType) {
	case ECameraDirectionType::Tutorial: {

		ChangeCurrentTutorialPhase(ETutorialPhase::None);
		break;
	}
	case ECameraDirectionType::FirstArea: {

		ChangeCurrentFirstAreaPhase(EFirstAreaPhase::None);
		break;
	}
	case ECameraDirectionType::SecondArea: {

		ChangeCurrentSecondAreaPhase(ESecondAreaPhase::None);
		break;
	}
	case ECameraDirectionType::ThirdArea: {

		ChangeCurrentThirdAreaPhase(EThirdAreaPhase::None);
		break;
	}
	case ECameraDirectionType::FoundGoal: {

		ChangeCurrentFoundGoalPhase(EFoundGoalPhase::None);
		break;
	}
	}
}

/// <summary>
/// チュートリアルのシーンフェーズを取得
/// </summary>
/// <param name=""></param>
/// <returns> フェーズの種類 </returns>
ETutorialPhase ACameraDirectionTriggerBox::GetTutorialPhase() {

	return CurrentTutorialPhase;
}

/// <summary>
/// 第1エリアのシーンフェーズを取得
/// </summary>
/// <param name=""></param>
/// <returns> フェーズの種類 </returns>
EFirstAreaPhase ACameraDirectionTriggerBox::GetFirstAreaPhase() {

	return CurrentFirstAreaPhase;
}

/// <summary>
/// 第2エリアのシーンフェーズを取得
/// </summary>
/// <param name=""></param>
/// <returns> フェーズの種類 </returns>
ESecondAreaPhase ACameraDirectionTriggerBox::GetSecondAreaPhase() {

	return CurrentSecondAreaPhase;
}

/// <summary>
/// 第3エリアのシーンフェーズを取得
/// </summary>
/// <param name=""></param>
/// <returns> フェーズの種類 </returns>
EThirdAreaPhase ACameraDirectionTriggerBox::GetThirdAreaPhase() {

	return CurrentThirdAreaPhase;
}

/// <summary>
/// ゴール発見時のシーンフェーズを取得
/// </summary>
/// <param name=""></param>
/// <returns> フェーズの種類 </returns>
EFoundGoalPhase ACameraDirectionTriggerBox::GetFoundGoalPhase() {

	return CurrentFoundGoalPhase;
}

/// <summary>
/// 現在のチュートリアルフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ACameraDirectionTriggerBox::ChangeCurrentTutorialPhase(ETutorialPhase PhaseType) {

	CurrentTutorialPhase = PhaseType;
}

/// <summary>
/// 現在の第1エリアフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ACameraDirectionTriggerBox::ChangeCurrentFirstAreaPhase(EFirstAreaPhase PhaseType) {

	CurrentFirstAreaPhase = PhaseType;
}

/// <summary>
/// 現在の第2エリアフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ACameraDirectionTriggerBox::ChangeCurrentSecondAreaPhase(ESecondAreaPhase PhaseType) {

	CurrentSecondAreaPhase = PhaseType;
}

/// <summary>
/// 現在の第3エリアフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ACameraDirectionTriggerBox::ChangeCurrentThirdAreaPhase(EThirdAreaPhase PhaseType) {

	CurrentThirdAreaPhase = PhaseType;
}

/// <summary>
/// 現在のゴール発見フェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ACameraDirectionTriggerBox::ChangeCurrentFoundGoalPhase(EFoundGoalPhase PhaseType) {

	CurrentFoundGoalPhase = PhaseType;
}

/// <summary>
/// 現在の開始シーンフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ACameraDirectionTriggerBox::ChangeCurrentStartScenePhase(EStartScenePhase PhaseType) {

	CurrentStartScenePhase = PhaseType;
}

/// <summary>
/// 現在の終了シーンフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ACameraDirectionTriggerBox::ChangeCurrentFinishScenePhase(EFinishScenePhase PhaseType) {

	CurrentFinishScenePhase = PhaseType;
}