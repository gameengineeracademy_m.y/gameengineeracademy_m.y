﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/UiAssignmentWidget.h"
#include "ShootedWidget.generated.h"


/// <summary>
/// 被弾ウィジェット
/// </summary>
UCLASS()
class UShootedWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UShootedWidget();

	/// <summary>
	/// アニメーション再生
	/// </summary>
	/// <param name="DirectionType"> 撃たれた方向 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="ShootedWidget")
	void PlayShootedAnimation(EDirectionType DirectionType);

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

private:

	/// <summary>
	/// アニメーション再生
	/// </summary>
	/// <param name="DirectionType"> 撃たれた方向 </param>
	/// <returns></returns>
	virtual void PlayShootedAnimation_Implementation(EDirectionType DirectionType);
};
