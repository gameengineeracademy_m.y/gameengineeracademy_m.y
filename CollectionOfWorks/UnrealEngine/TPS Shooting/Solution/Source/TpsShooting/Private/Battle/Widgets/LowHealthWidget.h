﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/UiAssignmentWidget.h"
#include "LowHealthWidget.generated.h"

/// <summary>
/// 瀕死ウィジェット
/// </summary>
UCLASS()
class ULowHealthWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ULowHealthWidget();

	/// <summary>
	/// ウィジェットを表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Open();

	/// <summary>
	/// ウィジェットを非表示にする
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Closed();

	/// <summary>
	/// アニメーション開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "LowHealthWidget")
	void PlayLowHealthAnimation();

	/// <summary>
	/// アニメーション終了
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "LowHealthWidget")
	void FinishLowHealthAnimation();

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

private:

	/// <summary>
	/// アニメーション開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayLowHealthAnimation_Implementation();

	/// <summary>
	/// アニメーション終了
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void FinishLowHealthAnimation_Implementation();
};
