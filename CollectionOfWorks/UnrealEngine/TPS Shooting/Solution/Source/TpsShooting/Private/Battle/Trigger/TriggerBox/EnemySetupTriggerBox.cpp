﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Trigger/TriggerBox/EnemySetupTriggerBox.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AEnemySetupTriggerBox::AEnemySetupTriggerBox()
	: TargetPointList() {

}

/// <summary>
/// エネミーにターゲットポイントをセットする
/// </summary>
/// <param name="Enemy"> エネミー </param>
/// <returns></returns>
void AEnemySetupTriggerBox::SetupTargetPoint(AEnemy* Enemy) {

	if (!IsValid(Enemy)) {
		return;
	}

	for (ATargetPoint* TargetPoint : TargetPointList) {

		if (!IsValid(TargetPoint)) {
			continue;
		}
		Enemy->AddPatrolPoint(TargetPoint);
	}
}