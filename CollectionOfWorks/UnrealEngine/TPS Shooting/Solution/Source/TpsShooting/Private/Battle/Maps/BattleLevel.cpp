﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Maps/BattleLevel.h"
#include "Kismet/GameplayStatics.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ABattleLevel::ABattleLevel()
	: CurrentPhase(ETransitionPhaseType::Initialize)
	, bCompleted(false) {

	// Tickイベントの有効化
	PrimaryActorTick.bCanEverTick = true;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::BeginPlay() {

	Super::BeginPlay();

	//---------------------------------------
	//変数の初期化処理
	//---------------------------------------
	CurrentPhase = ETransitionPhaseType::Initialize;
	UnLoadSubLevel = ELevelType::None;
	bCompleted = false;

	// バトルコントローラを取得
	BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ABattleLevel::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

	//------------------------------------------
	//レベルの遷移処理を行う
	//
	//------------------------------------------

	switch (CurrentPhase) {
	case ETransitionPhaseType::Initialize: {
		//------------------------------------
		//初期化処理フェーズ
		//------------------------------------

		ChangeCurrentPhase(ETransitionPhaseType::ShowLoadingWidget);
		break;
	}
	case ETransitionPhaseType::ShowLoadingWidget: {
		//------------------------------------
		//ロード画面表示フェーズ
		//------------------------------------
		LoadStreamLevel(LoadingLevelName, CompletedFunctionName);

		ChangeCurrentPhase(ETransitionPhaseType::LoadNextTransLevel);
		break;
	}
	case ETransitionPhaseType::LoadNextTransLevel: {
		//------------------------------------
		//遷移レベルロードフェーズ
		//------------------------------------
		if (!IsCompleted()) {
			return;
		}

		UTpsShootingGameInstance* GameInstance = Cast<UTpsShootingGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
		if (!IsValid(GameInstance)) {
			return;
		}
		// 現在ロードしているレベルを取得しておく
		UnLoadSubLevel = GameInstance->GetCurrentLevel();
		// 遷移するレベルをロードする
		ELevelType NextLevelType = GameInstance->GetNextTransLevel();
		switch (NextLevelType) {
		case ELevelType::Tutorial: {
			LoadStreamLevel(TutorialLevelName, CompletedFunctionName);
			break;
		}
		case ELevelType::MainField: {
			LoadStreamLevel(MainFieldLevelName, CompletedFunctionName);
			break;
		}
		default: {

			OnCompleted();
			break;
		}
		}

		ChangeCurrentPhase(ETransitionPhaseType::HandOver);
		break;
	}
	case ETransitionPhaseType::HandOver: {
		//------------------------------------
		//レベル間引継ぎフェーズ
		//------------------------------------
		// レベルを切り替えておく
		SwitchLevel();

		ChangeCurrentPhase(ETransitionPhaseType::WaitLoading);
		break;
	}
	case ETransitionPhaseType::WaitLoading: {
		//------------------------------------
		//ロード待機フェーズ
		//------------------------------------
		if (!IsCompleted()) {
			return;
		}

		ChangeCurrentPhase(ETransitionPhaseType::UnLoadCurrentLevel);
		break;
	}
	case ETransitionPhaseType::UnLoadCurrentLevel: {
		//------------------------------------
		//現在レベルのアンロードフェーズ
		//------------------------------------
		// 現在ロードしていた不要なレベルをアンロードする
		switch (UnLoadSubLevel) {
		case ELevelType::Tutorial: {
			UnloadStreamLevel(TutorialLevelName, CompletedFunctionName);
			break;
		}
		case ELevelType::MainField: {
			UnloadStreamLevel(MainFieldLevelName, CompletedFunctionName);
			break;
		}
		default: {

			OnCompleted();
			break;
		}
		}

		ChangeCurrentPhase(ETransitionPhaseType::PostProcess);
		break;
	}
	case ETransitionPhaseType::PostProcess: {
		//------------------------------------
		//後処理フェーズ
		//------------------------------------
		if (!IsCompleted()) {
			return;
		}

		ChangeCurrentPhase(ETransitionPhaseType::HideLoadingWidget);
		break;
	}
	case ETransitionPhaseType::HideLoadingWidget: {
		//------------------------------------
		//ロード画面非表示フェーズ
		//------------------------------------
		BattleController->PrepareCloseUiForLoading();
		BattleController->CloseUiForLoading();
		UnloadStreamLevel(LoadingLevelName, CompletedFunctionName);

		ChangeCurrentPhase(ETransitionPhaseType::Wait);
		break;
	}
	case ETransitionPhaseType::Wait: {
		//------------------------------------
		//サブレベル処理待ちフェーズ
		//------------------------------------

		//-----------------------------------------------------
		// メインゲームとチュートリアル処理中はここで待機
		// 次に遷移するレベルが指定された際に処理が開始される
		//-----------------------------------------------------

		UTpsShootingGameInstance* GameInstance = Cast<UTpsShootingGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
		if (!IsValid(GameInstance)) {
			return;
		}
		
		ELevelType CurrentLevelType = GameInstance->GetCurrentLevel();
		switch (CurrentLevelType) {
		case ELevelType::Title: {
			// レベルを変更する
			UGameplayStatics::OpenLevel(GetWorld(), TitleLevelName, true);
			ChangeCurrentPhase(ETransitionPhaseType::Finish);
			return;
		}
		}
		
		ELevelType NextLevelType = GameInstance->GetNextTransLevel();
		switch (NextLevelType) {
		case ELevelType::Title: {

			// 現在ロードしているレベルを取得しておく
			UnLoadSubLevel = CurrentLevelType;
			SwitchLevel();
			ChangeCurrentPhase(ETransitionPhaseType::UnLoadCurrentLevel);
			break;
		}
		case ELevelType::Tutorial: 
		case ELevelType::MainField: {

			ChangeCurrentPhase(ETransitionPhaseType::Initialize);
			break;
		}
		}

		break;
	}
	case ETransitionPhaseType::Finish: {
		//------------------------------------
		//終了フェーズ
		//------------------------------------
		break;
	}
	}
}

/// <summary>
/// レベルのロード処理
/// </summary>
/// <param name="LevelName"> ロードするレベル名 </param>
/// <param name="FunctionName"> ロード後の実行関数名 </param>
/// <returns></returns>
void ABattleLevel::LoadStreamLevel(FName LevelName, FName FunctionName) {

	//----------------------------------------------
	//レベルロード完了フラグを降ろした後、
	//指定レベルを非同期処理にてロードする
	//----------------------------------------------
	ResetCompleted();

	FLatentActionInfo LatentAction;
	LatentAction.CallbackTarget = this;
	LatentAction.ExecutionFunction = FunctionName;
	LatentAction.UUID += UUIDIndex;
	LatentAction.Linkage = LinkageValue;
	UGameplayStatics::LoadStreamLevel(this, LevelName, true, false, LatentAction);
}

/// <summary>
/// レベルのアンロード処理
/// </summary>
/// <param name="LevelName"> ロードするレベル名 < / param>
/// <param name="FunctionName"> ロード後の実行関数名 </param>
/// <returns></returns>
void ABattleLevel::UnloadStreamLevel(FName LevelName, FName FunctionName) {

	//----------------------------------------------
	//レベルロード完了フラグを降ろした後、
	//指定レベルを非同期処理にてアンロードする
	//----------------------------------------------
	ResetCompleted();

	FLatentActionInfo LatentAction;
	LatentAction.CallbackTarget = this;
	LatentAction.ExecutionFunction = FunctionName;
	LatentAction.UUID += UUIDIndex;
	LatentAction.Linkage = LinkageValue;
	UGameplayStatics::UnloadStreamLevel(this, LevelName, LatentAction, false);
}

/// <summary>
/// 処理完了通知処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnCompleted() {

	SetCompleted();
}

/// <summary>
/// ロード完了フラグのセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::SetCompleted() {

	bCompleted = true;
}

/// <summary>
/// ロード完了フラグのリセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::ResetCompleted() {

	bCompleted = false;
}

/// <summary>
/// ロード完了したかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:ロード完了 </returns>
bool ABattleLevel::IsCompleted() {

	return bCompleted;
}

/// <summary>
/// レベルの切替処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::SwitchLevel() {

	UTpsShootingGameInstance* GameInstance = Cast<UTpsShootingGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (!IsValid(GameInstance)) {
		return;
	}
	ELevelType NextLevelType = GameInstance->GetNextTransLevel();

	GameInstance->SetCurrentLevel(NextLevelType);
	GameInstance->SetNextTransLevel(ELevelType::None);
}

/// <summary>
/// 現在のフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ABattleLevel::ChangeCurrentPhase(ETransitionPhaseType PhaseType) {

	CurrentPhase = PhaseType;
}