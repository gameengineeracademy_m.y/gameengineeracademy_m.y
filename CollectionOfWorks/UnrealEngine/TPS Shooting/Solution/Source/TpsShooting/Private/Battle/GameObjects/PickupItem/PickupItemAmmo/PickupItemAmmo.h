﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Battle/GameObjects/PickupItem/PickupItemBase.h"
#include "PickupItemAmmo.generated.h"

/// <summary>
/// ピックアップアイテム 武器
/// </summary>
UCLASS()
class APickupItemAmmo : public APickupItemBase
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	APickupItemAmmo();

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;
};
