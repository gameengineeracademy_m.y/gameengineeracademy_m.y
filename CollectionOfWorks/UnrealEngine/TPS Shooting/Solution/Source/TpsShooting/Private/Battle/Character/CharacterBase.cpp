﻿
#include "Battle/Character/CharacterBase.h"
#include "Battle/Character/OperatePlayer/OperatePlayer.h"
#include "Battle/Character/Enemy/Enemy.h"
#include "Battle/GameState/BattleGameState.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/GameViewportClient.h"
#include "DrawDebugHelpers.h"
#include "Battle/GameState/BattleGameState.h"
#include "Kismet/GameplayStatics.h"
#include "Battle/Controller/BattleController.h"
#include "Kismet/KismetSystemLibrary.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ACharacterBase::ACharacterBase()
	: CameraBoom(nullptr)
	, FollowCamera(nullptr)
	, HandWeaponMesh(nullptr)
	, BackWeaponMesh(nullptr)
	, bPressRunButton(false)
	, bPressJumpButton(false)
	, bPressCrouchButton(false)
	, bPressAimButton(false)
	, bPressShootButton(false)
	, bPressSwitchButton(false)
	, bPressReloadButton(false)
	, bFinishDissolve(false)
	, PossessWeaponList()
	, WeaponList()
	, CameraDataTable(nullptr)
	, BodyPartsTable(nullptr)
	, DamageRateTable(nullptr)
	, EquipSocketTable(nullptr)
	, CameraDistance(0.0f)
	, DisappearanceTime(0.0f)
	, DetailData()
	, CharaType(ECharacterType::None)
	, CharaState(ECharaStateType::None)
	, DissolveType(EDissolveType::None)
	, HitPoint(0)
	, bRecieveDamage(false)
	, HitRotYaw(0.0f)
	, CameraData()
	, AdjustCameraPhase(EAdjustCameraPhase::Wait)
	, CurrentDissolvePhase(EDissolvePhase::None)
	, MoveCameraValue(0.0f)
	, AccumulateTime(0.0f)
	, WaitTime(0.0f)
	, BodyPartsList()
	, DamageRateList()
	, EquipSocketList() {

	// Tick処理の無効化
	PrimaryActorTick.bCanEverTick = false;

	// 本体
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// 武器用メッシュの作成
	USkeletalMeshComponent* CharaMesh = GetMesh();
	if (!IsValid(CharaMesh)) {
		UE_LOG(LogTemp, Warning, TEXT("ACharacterBase Mesh None"))
		return;
	}
	HandWeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("HandWeaponMesh"));
	BackWeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("BackWeaponMesh"));
	// BP画面上で武器メッシュの親をキャラクターメッシュとするため一旦アタッチしておく
	HandWeaponMesh->SetupAttachment(CharaMesh);
	BackWeaponMesh->SetupAttachment(CharaMesh);

	// キャラクタームーブメント設定
	GetCharacterMovement()->RotationRate = InitRotRate;
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->AirControl = AirControlValue;
	GetCharacterMovement()->GravityScale = Gravity;

	// カメラアームの生成と設定
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = CameraDistance;
	CameraBoom->bDoCollisionTest = true;
	CameraBoom->bUsePawnControlRotation = true;
	CameraBoom->bInheritPitch = true;       // Pitch制御あり
	CameraBoom->bInheritYaw = true;         // Yaw制御あり
	CameraBoom->bInheritRoll = true;        // Roll制御あり

	// カメラの生成と設定
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = true;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::BeginPlay() {

	Super::BeginPlay();

	// 各種初期化処理
	CharaType = ECharacterType::None;
	CharaState = ECharaStateType::None;
	AdjustCameraPhase = EAdjustCameraPhase::Wait;
	CurrentDissolvePhase = EDissolvePhase::PrepareAppearance;
	DissolveType = EDissolveType::None;

	AccumulateTime = ResetTime;
	WaitTime = ResetTime;
	bRecieveDamage = false;

	// カメラとの距離設定
	CameraBoom->TargetArmLength = CameraDistance;

	PossessWeaponList.Empty();
	PossessWeaponList.Add(EEquipPositionType::Hand, EWeaponType::None);
	PossessWeaponList.Add(EEquipPositionType::Back, EWeaponType::None);

	// リストの初期化
	InitializeDataList();
	// カメラの初期設定
	InitializeCameraData();
	// メッシュの当たり判定を無効にしておく
	SetupMeshCollision(ECollisionType::NoCollision);
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ACharacterBase::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

	// ディゾルブ処理
	ExecuteDissolve(DeltaTime);

	//---------------------------------------
	//キャラクター死亡処理と生存処理
	//---------------------------------------
	switch (CharaState) {
	case ECharaStateType::Fine:
	case ECharaStateType::Dying: {

		//---------------------------------------
		//弾切れの場合はリロードを行う
		//---------------------------------------
		AWeaponBase* EquipWeapon = GetEquipWeapon();
		if (!IsValid(EquipWeapon)) {
			return;
		}
		bool IsCheck = EquipWeapon->IsReload();
		if (IsCheck && !bPressReloadButton) {
			Reload();
		}

		//---------------------------------------
		//エイム時のプレイヤー向き調整
		//---------------------------------------
		if (!bPressAimButton && !bPressShootButton) {
			break;
		}
		// 正面に向ける
		FaceForward();

		break;
	}
	case ECharaStateType::Dead: {

		break;
	}
	case ECharaStateType::Disappearance: {

		return;
	}
	}

	// カメラのリフトアップ、リフトダウン処理
	LiftUpAndDownCamera();
}

/// <summary>
/// プレイヤーインプットコンポーネント設定
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ACharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {

	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

/// <summary>
/// キャラクターの種類のセット
/// </summary>
/// <param name="Type"> キャラクターの種類 </param>
/// <returns></returns>
void ACharacterBase::SetCharaType(ECharacterType Type) {

	CharaType = Type;
}

/// <summary>
/// キャラクターの種類の取得
/// </summary>
/// <param name=""></param>
/// <returns> キャラクターの種類 </returns>
ECharacterType ACharacterBase::GetCharaType() {

	return CharaType;
}

/// <summary>
/// キャラクターの状態のセット
/// </summary>
/// <param name="State"> キャラクターの状態 </param>
/// <returns></returns>
void ACharacterBase::SetCharaStateType(ECharaStateType State) {

	CharaState = State;
}

/// <summary>
/// キャラクターの状態の取得
/// </summary>
/// <param name=""></param>
/// <returns> キャラクターの状態 </returns>
ECharaStateType ACharacterBase::GetCharaStateType() {

	return CharaState;
}

/// <summary>
/// キャラクターの状態が生存かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:生存中 </returns>
bool ACharacterBase::IsFine() {

	return CharaState == ECharaStateType::Fine || CharaState == ECharaStateType::Dying;
}

/// <summary>
/// キャラクターの状態が消滅終了かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:消滅 </returns>
bool ACharacterBase::IsDisappearance() {

	return CharaState == ECharaStateType::Disappearance;
}

/// <summary>
/// ディゾルブの種類のセット
/// </summary>
/// <param name="Type"> ディゾルブの種類 </param>
/// <returns></returns>
void ACharacterBase::SetDissolveType(EDissolveType Type) {

	DissolveType = Type;
}

/// <summary>
/// 現在のディゾルブフェーズが表示待機中かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:表示待機中 </returns>
bool ACharacterBase::IsWaitAppearance() {

	return CurrentDissolvePhase == EDissolvePhase::WaitAppearance;
}

/// <summary>
/// 現在のディゾルブフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ACharacterBase::ChangeDissolvePhase(EDissolvePhase PhaseType) {

	CurrentDissolvePhase = PhaseType;
}

/// <summary>
/// ディゾルブのフェーズ処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ACharacterBase::ExecuteDissolve(float DeltaTime) {

	switch (CurrentDissolvePhase) {
	case EDissolvePhase::None: {
		break;
	}
	case EDissolvePhase::PrepareAppearance: {

		ChangeDissolvePhase(EDissolvePhase::WaitAppearance);
		break;
	}
	case EDissolvePhase::WaitAppearance: {

		break;
	}
	case EDissolvePhase::Appearance: {

		Dissolve();

		if (!bFinishDissolve) {
			break;
		}
		ChangeDissolvePhase(EDissolvePhase::FinishAppearance);
		break;
	}
	case EDissolvePhase::FinishAppearance: {

		// 消滅用のディゾルブ設定に切り替える
		SetDissolveType(EDissolveType::Disappearance);
		SetupDissolve();

		// メッシュのコリジョンを有効にする
		SetupMeshCollision(ECollisionType::QueryOnly);
		// キャラのステータスを「生存」に設定する
		SetCharaStateType(ECharaStateType::Fine);

		// 武器を表示する
		for (auto WeaponData : PossessWeaponList) {
			if (WeaponData.Value == EWeaponType::None) {
				continue;
			}
			AWeaponBase* Weapon = GetWeapon(WeaponData.Value);
			if (!IsValid(Weapon)) {
				continue;
			}
			Weapon->SetHidden(false);
		}

		ChangeDissolvePhase(EDissolvePhase::None);
		break;
	}
	case EDissolvePhase::PrepareDisappearance: {

		AccumulateTime = ResetTime;
		ChangeDissolvePhase(EDissolvePhase::WaitDisappearance);
		break;
	}
	case EDissolvePhase::WaitDisappearance: {

		AccumulateTime += DeltaTime;
		if (AccumulateTime < DisappearanceTime) {
			break;
		}
		AccumulateTime = ResetTime;

		// メッシュのコリジョンを無効にしておく
		SetupMeshCollision(ECollisionType::NoCollision);
		
		ChangeDissolvePhase(EDissolvePhase::Disappearance);
		break;
	}
	case EDissolvePhase::Disappearance: {

		Dissolve();

		if (!bFinishDissolve) {
			break;
		}
		ChangeDissolvePhase(EDissolvePhase::FinishDisappearance);
		break;
	}
	case EDissolvePhase::FinishDisappearance: {

		SetCharaStateType(ECharaStateType::Disappearance);
		ChangeDissolvePhase(EDissolvePhase::Finish);
		break;
	}
	case EDissolvePhase::Finish: {
		break;
	}
	}
}

/// <summary>
/// 部位の種類の取得
/// </summary>
/// <param name="BoneName"> ボーンの名前 </param>
/// <returns> 部位の種類 </returns>
EBodyPartsType ACharacterBase::GetBodyPartsType(FName BoneName) {

	if (!BodyPartsList.Contains(BoneName)) {
		return EBodyPartsType::None;
	}

	return BodyPartsList[BoneName];
}

/// <summary>
/// カメラの取得
/// </summary>
/// <param name=""></param>
/// <returns> カメラ </returns>
UCameraComponent* ACharacterBase::GetCamera() {

	return FollowCamera;
}

/// <summary>
/// キャラクターディテールをセット
/// </summary>
/// <param name="Detail"> キャラクターディテール </param>
/// <returns></returns>
void ACharacterBase::SetDetailData(FCharacterDetailData Detail) {

	DetailData = Detail;
}

/// <summary>
/// ディテールの反映
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::ReflectDetail() {

	GetCharacterMovement()->JumpZVelocity = DetailData.JumpingPower;
	SetWalkSpeed(DetailData.WalkSpeed);
	HitPoint = DetailData.HP;
}

/// <summary>
/// ヒットポイントの計算処理
/// </summary>
/// <param name="PartsType"> 部位の種類 </param>
/// <param name="Damage"> ダメージ値 </param>
/// <returns></returns>
void ACharacterBase::CalcHitPoint(EBodyPartsType PartsType, int32 Damage) {

	// ゲームの状況が「プレイ中」以外は処理を行わない
	bool IsCheck = IsGameStatePlay();
	if (!IsCheck) {
		return;
	}

	//--------------------------------------------
	//ヒット部位のダメージ倍率を取得し、
	//ダメージ値の補正を行う
	//その後、ヒットポイントからダメージ値を引く
	//--------------------------------------------

	float DamageRate = SearchDamageRate(PartsType);
	float DamageValue = FMath::TruncToInt(static_cast<float>(Damage) * DamageRate);

	HitPoint -= DamageValue;

	//--------------------------------------------
	//ヒットポイントが「0」以下になった場合、
	//キャラクターの状態を「死亡」に変更
	//死亡アニメーションを実行する
	//--------------------------------------------

	if (HitPoint <= DeadLine && IsFine()) {

		HitPoint = DeadLine;

		if (bPressShootButton) {
			AWeaponBase* EquipWeapon = GetEquipWeapon();
			if (!IsValid(EquipWeapon)) {
				return;
			}
			EquipWeapon->StopFire();
		}

		switch (CharaType) {
		case ECharacterType::Player: {

			//-----------------------------------------
			//ゲームの状況を「ゲームオーバー」にする
			//-----------------------------------------
			ABattleGameState* BattleGameState = Cast<ABattleGameState>(GetWorld()->GetGameState());
			if (!IsValid(BattleGameState)) {
				return;
			}
			BattleGameState->ChangeCurrentGameState(EGameStateType::GameOver);
			break;
		}
		}

		SetCharaStateType(ECharaStateType::Dead);
		ChangeDissolvePhase(EDissolvePhase::PrepareDisappearance);
		Died();
	}
}

/// <summary>
/// 目標キャラクターに対する角度を算出
/// </summary>
/// <param name="Vector"> 対象物のベクトル </param>
/// <param name="AimChara"> 目標キャラ </param>
/// <returns> 目標キャラクターに対する角度 </returns>
float ACharacterBase::CalcAngleToAim(FVector Vector, ACharacterBase* AimChara) {

	//-------------------------------------------------
	// 2つのベクトルから内積を求め、角度を算出する
	// ※角度算出のため、ベクトルは共に正規化しておく
	//-------------------------------------------------

	float Angle = Angle0;
	if (!IsValid(AimChara)) {
		return Angle;
	}

	//-------------------------------------------------
	// キャラクターの向いている方向
	//FVector AimVector = AimChara->GetActorForwardVector();
	//-------------------------------------------------
	// カメラの向いている方向
	FRotator AimRot = FRotator::ZeroRotator;
	AimRot.Yaw = AimChara->GetControlRotation().Yaw;
	FVector AimVector = UKismetMathLibrary::GetForwardVector(AimRot);
	//-------------------------------------------------

	FVector2D ActorNormVector = FVector2D(AimVector.X, AimVector.Y).GetSafeNormal();
	// ベクトルを2次元平面上の大きさに変換しておく
	FVector2D HitNormDirection = FVector2D(Vector.X, Vector.Y).GetSafeNormal();
	float Dot = UKismetMathLibrary::DotProduct2D(ActorNormVector, HitNormDirection);
	Angle = UKismetMathLibrary::DegAcos(Dot);

	// 内積は180°までしか判定できないため、ベクトルを
	// プレイヤーのローカル座標上に変換し、Y座標が負の値なら360°を加算する
	FVector LocalPos = UKismetMathLibrary::InverseTransformDirection(AimChara->GetActorTransform(), Vector);
	if (LocalPos.Y < Angle0) {
		Angle = Angle360 - Angle;
	}

	//UE_LOG(LogTemp, Display, TEXT("Vector X:%f, Y:%f"), Vector.X, Vector.Y)
	//UE_LOG(LogTemp, Display, TEXT("ActNormVec X:%f, Y:%f"), ActorNormVector.X, ActorNormVector.Y)
	//UE_LOG(LogTemp, Display, TEXT("HitDirection X:%f, Y:%f"), HitNormDirection.X, HitNormDirection.Y)
	//UE_LOG(LogTemp, Display, TEXT("LocalPos X:%f, Y:%f, Z%f"), LocalPos.X, LocalPos.Y, LocalPos.Z)
	//UE_LOG(LogTemp, Display, TEXT("Dot :%f"), Dot)
	//UE_LOG(LogTemp, Display, TEXT("Angle :%f"), Angle)

	return Angle;
}

/// <summary>
/// 弾の進入角度をセット
/// </summary>
/// <param name="Angle"> 角度 </param>
/// <returns></returns>
void ACharacterBase::SetHitRotYaw(float Angle) {

	HitRotYaw = Angle;
}

/// <summary>
/// 弾の進入角度の取得
/// </summary>
/// <param name=""></param>
/// <returns> 弾の進入角度 </returns>
float ACharacterBase::GetHitRotYaw() {

	return HitRotYaw;
}

/// <summary>
/// ウェポンのセット
/// </summary>
/// <param name="WeaponType"> ウェポンの種類 </param>
/// <param name="EquipPos"> 装備位置 </param>
/// <returns></returns>
void ACharacterBase::SetupPossessWeapon(EWeaponType WeaponType, EEquipPositionType EquipPos) {

	if (WeaponType == EWeaponType::None) {
		return;
	}

	if (!PossessWeaponList.Contains(EquipPos)) {
		return;
	}
	PossessWeaponList[EquipPos] = WeaponType;
}

/// <summary>
/// 装備武器の取得
/// </summary>
/// <param name=""></param>
/// <returns> 装備武器リスト </returns>
TMap<EEquipPositionType, EWeaponType> ACharacterBase::GetPossessWeapon() {

	return PossessWeaponList;
}

/// <summary>
/// 装備中のウェポンの取得
/// </summary>
/// <param name=""></param>
/// <returns> ウェポン </returns>
AWeaponBase* ACharacterBase::GetEquipWeapon() {
	
	if (!PossessWeaponList.Contains(EEquipPositionType::Hand)) {
		return nullptr;
	}

	EWeaponType WeaponType = PossessWeaponList[EEquipPositionType::Hand];
	if (WeaponType == EWeaponType::None) {
		return nullptr;
	}

	return GetWeapon(WeaponType);
}

/// <summary>
/// 武器のセット
/// </summary>
/// <param name="WeaponType"> 武器の種類 </param>
/// <param name="Weapon"> 武器 </param>
/// <returns></returns>
void ACharacterBase::SetWeapon(EWeaponType Type, AWeaponBase* Weapon) {

	// 既にリスト登録済みか武器が存在しない場合は処理終了
	if (WeaponList.Contains(Type) || !IsValid(Weapon)) {
		return;
	}
	WeaponList.Add(Type, Weapon);
}

/// <summary>
/// 武器の破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::DestroyWeapon() {

	// 所持している武器情報の初期化
	PossessWeaponList[EEquipPositionType::Hand] = EWeaponType::None;
	PossessWeaponList[EEquipPositionType::Back] = EWeaponType::None;

	// 実際の武器データを破棄し、リストの初期化
	for (auto Weapon : WeaponList) {
		if (!IsValid(Weapon.Value)) {
			continue;
		}
		Weapon.Value->Destroy();
		Weapon.Value = nullptr;
	}
	WeaponList.Empty();
}

/// <summary>
/// 指定ウェポンの取得
/// </summary>
/// <param name="Type"> 武器の種類 </param>
/// <returns> ウェポン </returns>
AWeaponBase* ACharacterBase::GetWeapon(EWeaponType Type) {

	if (Type == EWeaponType::None) {
		return nullptr;
	}

	return WeaponList[Type];
}

/// <summary>
/// サウンドの再生
/// </summary>
/// <param name="Sound"> サウンド </param>
/// <returns></returns>
void ACharacterBase::PlaySound(USoundCue* Sound) {

	if (!IsValid(GetMesh()) || !IsValid(Sound)) {
		return;
	}
	UGameplayStatics::SpawnSoundAttached(Sound, GetMesh());
}

/// <summary>
/// エフェクトの再生
/// </summary>
/// <param name="Effect"> エフェクト </param>
/// <returns></returns>
void ACharacterBase::PlayEffect(UParticleSystem* Effect) {

	if (!IsValid(GetMesh()) || !IsValid(Effect)) {
		return;
	}
	UGameplayStatics::SpawnEmitterAttached(Effect, GetMesh());
}

/// <summary>
/// ヒットポイントのセット
/// </summary>
/// <param name="HP"> ヒットポイント </param>
/// <returns></returns>
void ACharacterBase::SetHitPoint(int32 HP) {

	HitPoint = HP;
}

/// <summary>
/// 前方・後方移動
/// </summary>
/// <param name="Value"> 入力方向 </param>
/// <returns></returns>
void ACharacterBase::MoveForward(float Value) {

	//-------------------------------------------------
	//指定移動量分だけキャラクターを前後方向へ移動する
	//キャラクターの向く方向は
	//コントローラのZ軸周りの回転量から取得する
	//-------------------------------------------------
	float ControllerRotYaw = GetControlRotation().Yaw;
	FRotator CharaRot = FRotator(InitRotValue, ControllerRotYaw, InitRotValue);
	FVector CharaForwardVec = UKismetMathLibrary::GetForwardVector(CharaRot);
	AddMovementInput(CharaForwardVec, Value);
}

/// <summary>
/// 左右方向移動
/// </summary>
/// <param name="Value"> 入力方向 </param>
/// <returns></returns>
void ACharacterBase::MoveRight(float Value) {

	//-------------------------------------------------
	//指定移動量分だけキャラクターを左右方向へ移動する
	//キャラクターの向く方向は
	//コントローラのZ軸周りの回転量から取得する
	//-------------------------------------------------
	float ControllerRotYaw = GetControlRotation().Yaw;
	FRotator CharaRot = FRotator(InitRotValue, ControllerRotYaw, InitRotValue);
	FVector CharaRightVec = UKismetMathLibrary::GetRightVector(CharaRot);
	AddMovementInput(CharaRightVec, Value);
}

/// <summary>
/// ターン
/// </summary>
/// <param name="Value"> 入力方向 </param>
/// <returns></returns>
void ACharacterBase::Turn(float Value) {

	float TurnValue = Value * TurnValueRate;
	AddControllerYawInput(TurnValue);
}

/// <summary>
/// 見上げる
/// </summary>
/// <param name="Value"> 入力方向 </param>
/// <returns></returns>
void ACharacterBase::LookUp(float Value) {

	AddControllerPitchInput(Value);
}

/// <summary>
/// ジャンプ開始
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::TouchStartedJump() {

	bool bPackageCheck = UKismetSystemLibrary::IsPackagedForDistribution();
	if (bPackageCheck) {
		return;
	}

	// ゲームの状況が「プレイ中」以外は処理を行わない
	bool IsCheck = IsGameStatePlay();
	if (!IsCheck) {
		return;
	}

	bPressJumpButton = true;
	Jump();
}

/// <summary>
/// ジャンプ終了
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::TouchStoppedJump() {

	bPressJumpButton = false;
	StopJumping();
}

/// <summary>
/// 走る
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::Run() {
	
	// ゲームの状況が「プレイ中」以外は処理を行わない
	bool IsCheck = IsGameStatePlay();
	if (!IsCheck) {
		return;
	}

	if (!bPressAimButton) {
		StopShooting();
	}

	bPressRunButton = true;

	if (bPressCrouchButton || bPressAimButton) {
		return;
	}

	SetWalkSpeed(DetailData.RunSpeed);
}

/// <summary>
/// 走るのをやめる
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::StopRunning() {

	bPressRunButton = false;
	SetWalkSpeed(DetailData.WalkSpeed);
}

/// <summary>
/// 歩行スピードの設定
/// </summary>
/// <param name="Speed"> スピード </param>
/// <returns></returns>
void ACharacterBase::SetWalkSpeed(float Speed) {

	GetCharacterMovement()->MaxWalkSpeed = Speed;
}

/// <summary>
/// 狙う
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::Aim() {

	// ゲームの状況が「プレイ中」以外は処理を行わない
	bool IsCheck = IsGameStatePlay();
	if (!IsCheck) {
		return;
	}

	// 撃たれている場合は撃たれていないことにする
	//ResetDamageReaction();

	//-------------------------------------------
	//エイムをやめ、カメラとプレイヤーが一緒に
	//動くように設定を変更する
	//-------------------------------------------
	bPressAimButton = true;
	SetupCamera(EPhisicalPosition::Aim);

	// 歩行スピードを「歩く」に設定する
	// 走っている状態を継続しているかどうかは
	// ここでは判断をせず、走る操作処理内でのみ判断する
	SetWalkSpeed(DetailData.WalkSpeed);
}

/// <summary>
/// 狙うのをやめる
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::StopAiming() {

	//-------------------------------------------
	//エイムをやめ、カメラとプレイヤーが一緒に
	//動かさないように設定を変更する
	//-------------------------------------------
	bPressAimButton = false;
	SetupCamera(EPhisicalPosition::Stand);

	// 走っている状態なら走る処理を実行する
	if (!bPressShootButton && bPressRunButton) {
		Run();
	}
}

/// <summary>
/// しゃがむ処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::CrouchAndStand() {

	// ゲームの状況が「プレイ中」以外は処理を行わない
	bool IsCheck = IsGameStatePlay();
	if (!IsCheck) {
		return;
	}

	if (!bPressCrouchButton) {
		bPressCrouchButton = true;
		ChangeAdjustCameraPhase(EAdjustCameraPhase::LiftDown);
		StopRunning();
	}
	else {
		bPressCrouchButton = false;
		ChangeAdjustCameraPhase(EAdjustCameraPhase::LiftUp);
		if (bPressRunButton) {
			Run();
		}
	}
}

/// <summary>
/// 正面を向く
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::FaceForward() {

	//------------------------------------
	//カメラが向いている方向に
	//プレイヤーを向ける
	//------------------------------------

	// カメラの向いている方向
	float ControllerRotYaw = GetControlRotation().Yaw;
	// 操作プレイヤーが向いている方向
	FRotator CharaRot = GetActorRotation();
	CharaRot.Yaw = ControllerRotYaw;

	SetActorRotation(CharaRot);
}

/// <summary>
/// 弾を撃つ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::Shoot() {

	// ゲームの状況が「プレイ中」以外は処理を行わない
	bool IsCheck = IsGameStatePlay();
	if (!IsCheck) {
		return;
	}

	// リロード中、武器切替中は処理終了
	if (bPressReloadButton || bPressSwitchButton) {
		return;
	}

	//------------------------------------------
	//走っている状態での射撃は禁止
	//歩行スピードに設定し直す
	//------------------------------------------
	if (bPressRunButton && !bPressAimButton) {
		SetWalkSpeed(DetailData.WalkSpeed);
	}

	// 撃たれている場合は撃たれていないことにする
	//ResetDamageReaction();

	AWeaponBase* EquipWeapon = GetEquipWeapon();
	if (!IsValid(EquipWeapon)) {
		return;
	}

	//------------------------------------------
	//弾を撃つ際は必ずカメラの向いている方向が
	//正面として処理を行う
	//------------------------------------------
	bPressShootButton = true;
	// カメラの向いている方向にプレイヤーを向ける
	FaceForward();
	// 射撃実行
	EquipWeapon->Fire();

	return;
}

/// <summary>
/// 弾を撃つのをやめる
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::StopShooting() {

	if (!bPressShootButton) {
		return;
	}

	bPressShootButton = false;

	AWeaponBase* EquipWeapon = GetEquipWeapon();
	if (!IsValid(EquipWeapon)) {
		return;
	}
	EquipWeapon->StopFire();

	// 走っている状態なら走る処理を実行する
	if (bPressRunButton) {
		Run();
	}
}

/// <summary>
/// リロード処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::Reload() {

	// リロード中、武器切り替え中は実行不可
	if (bPressSwitchButton || bPressReloadButton) {
		return;
	}

	AWeaponBase* EquipWeapon = GetEquipWeapon();
	if (!IsValid(EquipWeapon)) {
		return;
	}
	// 装填されている弾薬が満タン、または
	// 所持弾薬がない場合は処理終了
	bool IsCheckLoadedAmmos = EquipWeapon->IsExistsMaxLoadedAmmos();
	bool IsCheckPossessAmmos = EquipWeapon->IsExistsPossessAmmos();
	if (IsCheckLoadedAmmos || !IsCheckPossessAmmos) {
		return;
	}

	// リロード時は一旦カメラを直立時の状態にセットする
	SetupCamera(EPhisicalPosition::Stand);

	bPressReloadButton = true;
	ExecuteAnimMontage(ECharaAnimType::Reload);
	EquipWeapon->PlayWeaponAnimation(EWeaponPhase::Reload);
}

/// <summary>
/// リロードをキャンセル
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::CancelReload() {

	AWeaponBase* EquipWeapon = GetEquipWeapon();
	if (!IsValid(EquipWeapon)) {
		return;
	}
	bPressReloadButton = false;
	EquipWeapon->bReload = false;
	EquipWeapon->ResetWeaponAnimation();
}

/// <summary>
/// リロード終了
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::CompletedReload() {

	bPressReloadButton = false;

	AWeaponBase* EquipWeapon = GetEquipWeapon();
	if (!IsValid(EquipWeapon)) {
		return;
	}
	EquipWeapon->FinishReload();

	// エイムしている場合はカメラをセットし直す
	if (bPressAimButton) {
		FaceForward();
		SetupCamera(EPhisicalPosition::Aim);
	}
	// 射撃している場合は射撃を続ける
	if (bPressShootButton) {
		Shoot();
	}
}

/// <summary>
/// 撃たれたかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:撃たれた </returns>
bool ACharacterBase::IsReceiveDamage() {

	return bRecieveDamage;
}

/// <summary>
/// リストの初期化
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::InitializeDataList() {
	
	if (!IsValid(BodyPartsTable) || !IsValid(DamageRateTable)) {
		return;
	}

	//------------------------------------
	//データテーブルから対象データを取得
	//------------------------------------
	// ボーン別の部位情報
	TArray<FName> RowNames = BodyPartsTable->GetRowNames();
	for (FName RowName : RowNames) {
		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		// リストのキーが既に登録済みなら処理スキップ
		FBodyPartsData* BodyPartsData = BodyPartsTable->FindRow<FBodyPartsData>(RowName, "");
		if (BodyPartsList.Contains(BodyPartsData->BoneName)) {
			continue;
		}
		BodyPartsList.Add(BodyPartsData->BoneName, BodyPartsData->BodyPartsType);
	}

	// 部位別のダメージ倍率
	RowNames = DamageRateTable->GetRowNames();
	for (FName RowName : RowNames) {
		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		// リストのキーが既に登録済みなら処理スキップ
		FDamageRateData* DamageRateData = DamageRateTable->FindRow<FDamageRateData>(RowName, "");
		if (DamageRateList.Contains(DamageRateData->BodyPartsType)) {
			continue;
		}
		DamageRateList.Add(DamageRateData->BodyPartsType, DamageRateData->DamageRate);
	}

	// 装備位置ごとのソケット名
	RowNames = EquipSocketTable->GetRowNames();
	for (FName RowName : RowNames) {
		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		// リストのキーが既に登録済みなら処理スキップ
		FEquipSocketData* EquipSocketData = EquipSocketTable->FindRow<FEquipSocketData>(RowName, "");
		if (EquipSocketList.Contains(EquipSocketData->EquipPosition)) {
			continue;
		}
		EquipSocketList.Add(EquipSocketData->EquipPosition, EquipSocketData->SocketName);
	}
}

/// <summary>
/// カメラ情報の初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::InitializeCameraData() {

	if (!IsValid(CameraDataTable)) {
		return;
	}

	//------------------------------------
	//データテーブルから対象データを取得
	//------------------------------------
	// 通常時のカメラ情報
	TArray<FName> RowNames = CameraDataTable->GetRowNames();
	for (FName RowName : RowNames) {
		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		FCameraData* CameraRowData = CameraDataTable->FindRow<FCameraData>(RowName, "");
		// 既に登録済みのデータなら次の行へ
		if (CameraData.Contains(CameraRowData->PhisicalPosition)) {
			continue;
		}
		FCameraDetail CameraDetail;
		CameraDetail.PlayerDistance = CameraRowData->PlayerDistance;
		CameraDetail.HorizontalPos = CameraRowData->HorizontalPos;
		CameraDetail.VerticalPos = CameraRowData->VerticalPos;
		CameraDetail.FOV = CameraRowData->FOV;

		CameraData.Add(CameraRowData->PhisicalPosition, CameraDetail);
	}

	MoveCameraValue = (CameraData[EPhisicalPosition::Stand].VerticalPos - CameraData[EPhisicalPosition::Crouch].VerticalPos) / AdjustFrames;
}

/// <summary>
/// カメラ位置の設定
/// </summary>
/// <param name="Type"> プレイヤーの姿勢 </param>
/// <returns></returns>
void ACharacterBase::SetupCamera(EPhisicalPosition Type) {

	//-------------------------------------------
	//カメラの位置の設定や
	//プレイヤー操作時にカメラも一緒に動かすか
	//設定を行う
	//-------------------------------------------
	if (bPressReloadButton || bPressSwitchButton) {
		return;
	}

	AWeaponBase* EquipWeapon = GetEquipWeapon();
	if (!IsValid(EquipWeapon)) {
		return;
	}

	FVector Pos;
	FRotator Rot;
	float FOV = CameraData[Type].FOV;
	bool bCameraCollision = false;
	bool bOrientRotationMove = false;
	bool bControllerRotationYaw = false;

	Pos.X = CameraData[Type].PlayerDistance;
	Pos.Y = CameraData[Type].HorizontalPos;
	Pos.Z = FollowCamera->GetRelativeLocation().Z;
	Rot = FollowCamera->GetRelativeRotation();

	switch (Type) {
	case EPhisicalPosition::Stand: {

		FOV = CameraData[Type].FOV;
		bCameraCollision = true;
		bOrientRotationMove = true;
		bControllerRotationYaw = false;
		break;
	}
	case EPhisicalPosition::Aim: {

		Pos.X = EquipWeapon->AimCameraData.PlayerDistance;
		Pos.Y = EquipWeapon->AimCameraData.HorizontalPos;
		FOV = EquipWeapon->WeaponData.FOV;
		bCameraCollision = false;
		bOrientRotationMove = false;
		bControllerRotationYaw = true;
		break;
	}
	}

	CameraBoom->bDoCollisionTest = bCameraCollision;
	FollowCamera->SetRelativeLocationAndRotation(Pos, Rot);
	FollowCamera->FieldOfView = FOV;
	GetCharacterMovement()->bOrientRotationToMovement = bOrientRotationMove;
	bUseControllerRotationYaw = bControllerRotationYaw;
}

/// <summary>
/// カメラの上下処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::LiftUpAndDownCamera() {

	switch (AdjustCameraPhase) {
	case EAdjustCameraPhase::LiftUp: {

		FVector Location = FollowCamera->GetRelativeLocation();
		Location.Z += MoveCameraValue;
		if (Location.Z >= CameraData[EPhisicalPosition::Stand].VerticalPos) {
			Location.Z = CameraData[EPhisicalPosition::Stand].VerticalPos;
			ChangeAdjustCameraPhase(EAdjustCameraPhase::Wait);
		}
		FollowCamera->SetRelativeLocation(Location);
		break;
	}
	case EAdjustCameraPhase::LiftDown: {

		FVector Location = FollowCamera->GetRelativeLocation();
		Location.Z -= MoveCameraValue;
		if (Location.Z <= CameraData[EPhisicalPosition::Crouch].VerticalPos) {
			Location.Z = CameraData[EPhisicalPosition::Crouch].VerticalPos;
			ChangeAdjustCameraPhase(EAdjustCameraPhase::Wait);
		}
		FollowCamera->SetRelativeLocation(Location);
		break;
	}
	case EAdjustCameraPhase::Wait: {
		break;
	}
	}
}

/// <summary>
/// メッシュコリジョンの設定
/// </summary>
/// <param name="Type"> コリジョンの種類 </param>
/// <returns></returns>
void ACharacterBase::SetupMeshCollision(ECollisionType Type) {

	USkeletalMeshComponent* CharaMesh = GetMesh();
	if (!IsValid(CharaMesh)) {
		UE_LOG(LogTemp, Warning, TEXT("ACharacterBase Mesh None"))
		return;
	}

	switch (Type) {
	case ECollisionType::NoCollision: {
		CharaMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		break;
	}
	case ECollisionType::QueryOnly: {
		CharaMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		break;
	}
	}
}

/// <summary>
/// 撃たれたことにする
/// </summary>
/// <param name=""></param>
/// <returns> ウェポン </returns>
void ACharacterBase::SetDamageReaction() {

	bRecieveDamage = true;
}

/// <summary>
/// 指定ソケット位置を取得する
/// </summary>
/// <param name="SocketName"> ソケット名 </param>
/// <returns></returns>
FVector ACharacterBase::GetSocketPosition(FName SocketName) {

	FVector Pos = FVector::ZeroVector;

	USkeletalMeshComponent* CharaMesh = GetMesh();
	if (!IsValid(CharaMesh)) {
		return Pos;
	}
	Pos = CharaMesh->GetSocketLocation(SocketName);

	return Pos;
}

/// <summary>
/// 部位ごとのダメージ倍率を取得
/// </summary>
/// <param name="PartsType"> 部位の種類 </param>
/// <returns> ダメージ倍率 </returns>
float ACharacterBase::SearchDamageRate(EBodyPartsType PartsType) {

	float DamageRate = DamageDefaultRate;

	if (!DamageRateList.Contains(PartsType)) {
		return DamageRate;
	}
	DamageRate = DamageRateList[PartsType];

	return DamageRate;
}

/// <summary>
/// 現在のゲーム状況が「プレイ中」かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:プレイ中 </returns>
bool ACharacterBase::IsGameStatePlay() {

	ABattleGameState* BattleGameState = Cast<ABattleGameState>(GetWorld()->GetGameState());
	if (!IsValid(BattleGameState)) {
		return false;
	}
	return BattleGameState->IsPlay();
}

/// <summary>
/// 所持武器が複数あるかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:複数武器所持 </returns>
bool ACharacterBase::IsPossessWeaponMultiple() {

	int32 WeaponCount = 0;
	for (auto Weapon : PossessWeaponList) {
		if (Weapon.Value == EWeaponType::None) {
			continue;
		}
		++WeaponCount;
	}

	return WeaponCount > OneWeapon;
}

/// <summary>
/// 装備位置からソケット名を取得
/// </summary>
/// <param name="EquipPos"> 装備位置 </param>
/// <returns> ソケット名 </returns>
FName ACharacterBase::GetEquipSocketName(EEquipPositionType EquipPos) {

	if (!EquipSocketList.Contains(EquipPos)) {
		return "";
	}
	
	return EquipSocketList[EquipPos];
}

/// <summary>
/// プレイヤーから見た方角を調べる
/// </summary>
/// <param name="Angle"> キャラクターに対する角度 </param>
/// <returns> 方角 </returns>
EDirectionType ACharacterBase::SearchDirection(float Angle) {

	EDirectionType DirectionType = EDirectionType::Forward;
	// 撃たれた方向を元にウィジェットアニメーションを実行する
	if (Angle >= ForwardRangeMax && Angle <= RightRangeMax) {
		DirectionType = EDirectionType::Left;
	}
	else if (Angle > RightRangeMax && Angle < BackRangeMax) {
		DirectionType = EDirectionType::Forward;
	}
	else if (Angle >= BackRangeMax && Angle <= LeftRangeMax) {
		DirectionType = EDirectionType::Right;
	}
	else {
		DirectionType = EDirectionType::Back;
	}

	return DirectionType;
}

/// <summary>
/// カメラ調整フェーズの変更
/// </summary>
/// <param name="CameraPhase"> カメラ調整フェーズ </param>
/// <returns></returns>
void ACharacterBase::ChangeAdjustCameraPhase(EAdjustCameraPhase CameraPhase) {

	AdjustCameraPhase = CameraPhase;
}

/// <summary>
/// 所持している武器かどうか
/// </summary>
/// <param name="Type"> 武器の種類 </param>
/// <returns> true:所持済み </returns>
bool ACharacterBase::IsPossessWeapon(EWeaponType Type) {

	for (auto Weapon : PossessWeaponList) {
		if (Weapon.Value != Type) {
			continue;
		}
		return true;
	}
	return false;
}

/// <summary>
/// 武器メッシュを親のソケットにアタッチする
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::AttachParantSocket_Implementation(AWeaponBase* Weapon, EEquipPositionType EquipPos) {

}

/// <summary>
/// 死亡処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::Died_Implementation() {

}

/// <summary>
/// ディゾルブ処理の初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::InitializeDissolve_Implementation() {

}

/// <summary>
/// ディゾルブ処理の初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::SetupDissolve_Implementation() {

}

/// <summary>
/// ディゾルブ処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ACharacterBase::Dissolve_Implementation() {

}

/// <summary>
/// アニメーション実行
/// </summary>
/// <param name="AnimType"> アニメーションの種類 </param>
/// <returns></returns>
void ACharacterBase::ExecuteAnimMontage_Implementation(ECharaAnimType AnimType) {

}

/// <summary>
/// アニメーションモンタージュ停止
/// </summary>
/// <param name="AnimType"> アニメーションの種類 </param>
/// <returns></returns>
void ACharacterBase::CancelAnimMontage_Implementation(ECharaAnimType AnimType) {

}