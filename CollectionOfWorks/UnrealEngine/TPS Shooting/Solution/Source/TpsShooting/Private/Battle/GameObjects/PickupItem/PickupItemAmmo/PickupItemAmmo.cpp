﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/GameObjects/PickupItem/PickupItemAmmo/PickupItemAmmo.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
APickupItemAmmo::APickupItemAmmo() {

	// Tickイベントは無効化
	PrimaryActorTick.bCanEverTick = false;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APickupItemAmmo::BeginPlay() {

	Super::BeginPlay();

	PickupItemType = EPickupItemType::Ammo;
	PickupType = EPickupType::Reborn;
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void APickupItemAmmo::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}