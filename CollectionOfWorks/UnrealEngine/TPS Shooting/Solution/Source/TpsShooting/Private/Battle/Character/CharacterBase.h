﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Battle/Enum/CommonEnum.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/DataTable.h"
#include "Battle/DataAssets/CharacterDetail/CharacterDetail.h"
#include "Battle/Weapons/WeaponBase.h"
#include "../Public/TpsShootingGameInstance.h"
#include "CharacterBase.generated.h"


/// <summary>
/// キャラクターの状態
/// </summary>
UENUM(BlueprintType)
enum class ECharaStateType : uint8 {
	None,                 // 設定なし
	Fine,                 // 元気
	Dying,                // 瀕死
	Dead,                 // 死亡
	Disappearance,        // 消滅
	MaxTypeIndex          // 種類数
};

/// <summary>
/// キャラクターアニメーションの種類
/// </summary>
UENUM(BlueprintType)
enum class ECharaAnimType : uint8 {
	None,                 // 設定なし
	Reload,               // リロード
	SwitchWeapon,         // 武器切り替え
	RecieveDamage,        // 被弾
	MaxTypeIndex          // 種類数
};

/// <summary>
/// ディゾルブのフェーズ
/// </summary>
UENUM(BlueprintType)
enum class EDissolvePhase : uint8 {
	None,                 // 設定なし
	PrepareAppearance,    // 出現準備
	WaitAppearance,       // 出現待機
	Appearance,           // 出現
	FinishAppearance,     // 出現終了
	PrepareDisappearance, // 消滅準備
	WaitDisappearance,    // 消滅待機
	Disappearance,        // 消滅
	FinishDisappearance,  // 消滅終了
	Finish,               // 処理終了
	MaxTypeIndex          // 種類数
};

/// <summary>
/// ディゾルブの種類
/// </summary>
UENUM(BlueprintType)
enum class EDissolveType : uint8 {
	None,           // 設定なし
	Appearance,     // 出現表現
	Disappearance,  // 消滅表現
	MaxTypeIndex    // 種類数
};

/// <summary>
/// コリジョンの種類
/// </summary>
UENUM()
enum class ECollisionType {
	None,           // 設定なし
	NoCollision,    // コリジョンなし
	QueryOnly,      // コリジョンあり
	MaxTypeIndex    // 種類数
};

/// <summary>
/// カメラ高さの調整
/// </summary>
UENUM()
enum class EAdjustCameraPhase {
	Wait,           // 待機フェーズ
	LiftUp,         // リフトアップフェーズ
	LiftDown,       // リフトダウンフェーズ
	MaxTypeIndex    // 種類数
};

/// <summary>
/// プレイヤーの状態
/// </summary>
UENUM()
enum class EPhisicalPosition {
	Stand,          // 立っている状態
	Aim,            // エイム状態
	Crouch,         // しゃがんでいる状態
	MaxTypeIndex    // 種類数
};

/// <summary>
/// キャラクターの部位の種類
/// </summary>
UENUM(BlueprintType)
enum class EBodyPartsType : uint8 {
	None,           // 設定なし
	Head,           // 頭
	Body,           // 身体
	Arm,            // 腕
	Foot,           // 脚
	MaxTypeIndex    // 種類数
};

/// <summary>
/// 武器の装備位置
/// </summary>
UENUM(BlueprintType)
enum class EEquipPositionType : uint8 {
	None,           // 設定なし
	Hand,           // 手
	Back,           // 背中
	MaxTypeIndex    // 種類数
};

/// <summary>
/// 装備位置ソケット情報 構造体
/// </summary>
USTRUCT(BlueprintType)
struct FEquipSocketData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// 装備位置
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CameraDetail")
	EEquipPositionType EquipPosition;

	/// <summary>
	/// ソケット名
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CameraDetail")
	FName SocketName;
};

/// <summary>
/// カメラディテール 構造体
/// </summary>
USTRUCT(BlueprintType)
struct FCameraData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// プレイヤーの状態
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CameraDetail")
	EPhisicalPosition PhisicalPosition;

	/// <summary>
	/// カメラとプレイヤーの距離
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CameraDetail")
	float PlayerDistance;

	/// <summary>
	/// 水平位置
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CameraDetail")
	float HorizontalPos;

	/// <summary>
	/// 垂直位置
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CameraDetail")
	float VerticalPos;

	/// <summary>
	/// 視野角
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CameraDetail")
	float FOV;
};

/// <summary>
/// カメラの情報 構造体
/// </summary>
USTRUCT()
struct FCameraDetail {

	GENERATED_BODY()

public:

	/// <summary>
	/// カメラとプレイヤーの距離
	/// </summary>
	float PlayerDistance;

	/// <summary>
	/// 水平位置
	/// </summary>
	float HorizontalPos;

	/// <summary>
	/// 垂直位置
	/// </summary>
	float VerticalPos;

	/// <summary>
	/// 視野角
	/// </summary>
	float FOV;
};

/// <summary>
/// キャラクター部位情報 構造体
/// </summary>
USTRUCT(BlueprintType)
struct FBodyPartsData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// ボーンの名前
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CameraDetail")
	FName BoneName;

	/// <summary>
	/// 部位の種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CameraDetail")
	EBodyPartsType BodyPartsType;
};

/// <summary>
/// 部位ごとのダメージ倍率 構造体
/// </summary>
USTRUCT(BlueprintType)
struct FDamageRateData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// 部位の種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CameraDetail")
	EBodyPartsType BodyPartsType;

	/// <summary>
	/// ダメージ倍率
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CameraDetail")
	float DamageRate;
};

/// <summary>
/// 初期武器設定
/// </summary>
USTRUCT(BlueprintType)
struct FCharaInitWeapon : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// スポーン情報の行番号
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterBase")
	FString SpawnNumber;

	/// <summary>
	/// 武器の種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterBase")
	EWeaponType WeaponType;

	/// <summary>
	/// 装備位置
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterBase")
	EEquipPositionType EquipPosition;
};

/// <summary>
/// スポーン情報
/// </summary>
USTRUCT(BlueprintType)
struct FCharacterSpawn : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// バトルレベルの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterBase")
	ELevelType BattleLevelType;

	/// <summary>
	/// エリアの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterBase")
	EStageType StageType;

	/// <summary>
	/// スポーン位置 X座標
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterBase")
	float PositionX;

	/// <summary>
	/// スポーン位置 Y座標
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterBase")
	float PositionY;

	/// <summary>
	/// スポーン位置 Z座標
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterBase")
	float PositionZ;

	/// <summary>
	/// キャラクターの向き (Z軸周りの回転量)
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterBase")
	float RotationYaw;
};

/// <summary>
/// スポーン情報 受け取り用
/// </summary>
USTRUCT()
struct FSpawnData {

	GENERATED_BODY()

public:

	/// <summary>
	/// スポーン位置
	/// </summary>
	FVector Position;

	/// <summary>
	/// 回転情報
	/// </summary>
	FRotator Rotation;
};

/// <summary>
/// キャラクターベースクラス
/// </summary>
UCLASS()
class ACharacterBase : public ACharacter
{
	GENERATED_BODY()

public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ACharacterBase();

	/// <summary>
	/// キャラクターの種類のセット
	/// </summary>
	/// <param name="Type"> キャラクターの種類 </param>
	/// <returns></returns>
	void SetCharaType(ECharacterType Type);

	/// <summary>
	/// キャラクターの種類の取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> キャラクターの種類 </returns>
	ECharacterType GetCharaType();

	/// <summary>
	/// キャラクターの状態のセット
	/// </summary>
	/// <param name="State"> キャラクターの状態 </param>
	/// <returns></returns>
	void SetCharaStateType(ECharaStateType State);

	/// <summary>
	/// キャラクターの状態の取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> キャラクターの状態 </returns>
	ECharaStateType GetCharaStateType();

	/// <summary>
	/// キャラクターの状態が生存かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:生存中 </returns>
	UFUNCTION(BlueprintCallable, Category = "CharacterBase")
	bool IsFine();

	/// <summary>
	/// キャラクターの状態が消滅かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:消滅 </returns>
	bool IsDisappearance();

	/// <summary>
	/// ディゾルブの種類のセット
	/// </summary>
	/// <param name="Type"> ディゾルブの種類 </param>
	/// <returns></returns>
	void SetDissolveType(EDissolveType Type);

	/// <summary>
	/// 現在のディゾルブフェーズが表示待機中かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:表示待機中 </returns>
	bool IsWaitAppearance();

	/// <summary>
	/// 現在のディゾルブフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeDissolvePhase(EDissolvePhase PhaseType);

	/// <summary>
	/// 部位の種類の取得
	/// </summary>
	/// <param name="BoneName"> ボーンの名前 </param>
	/// <returns> 部位の種類 </returns>
	EBodyPartsType GetBodyPartsType(FName BoneName);

	/// <summary>
	/// カメラの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> カメラ </returns>
	UCameraComponent* GetCamera();

	/// <summary>
	/// キャラクターディテールをセット
	/// </summary>
	/// <param name="Detail"> キャラクターディテール </param>
	/// <returns></returns>
	void SetDetailData(FCharacterDetailData Detail);

	/// <summary>
	/// ディテールの反映
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ReflectDetail();

	/// <summary>
	/// 武器のセット
	/// </summary>
	/// <param name="WeaponType"> 武器の種類 </param>
	/// <param name="Weapon"> 武器 </param>
	/// <returns></returns>
	void SetWeapon(EWeaponType Type, AWeaponBase* Weapon);

	/// <summary>
	/// 武器の破棄
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void DestroyWeapon();

	/// <summary>
	/// ヒットポイントの計算処理
	/// </summary>
	/// <param name="PartsType"> 部位の種類 </param>
	/// <param name="Damage"> ダメージ値 </param>
	/// <returns></returns>
	void CalcHitPoint(EBodyPartsType PartsType, int32 Damage);

	/// <summary>
	/// 目標キャラクターに対する角度を算出
	/// </summary>
	/// <param name="Vector"> 対象物のベクトル </param>
	/// <param name="AimChara"> 目標キャラ </param>
	/// <returns> 目標キャラクターに対する角度 </returns>
	float CalcAngleToAim(FVector Vector, ACharacterBase* AimChara);

	/// <summary>
	/// 弾の進入角度をセット
	/// </summary>
	/// <param name="Angle"> 角度 </param>
	/// <returns></returns>
	void SetHitRotYaw(float Angle);

	/// <summary>
	/// 弾の進入角度の取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 弾の進入角度 </returns>
	float GetHitRotYaw();

	/// <summary>
	/// 装備武器の設定
	/// </summary>
	/// <param name="WeaponType"> ウェポンの種類 </param>
	/// <param name="EquipPos"> 装備位置 </param>
	/// <returns></returns>
	void SetupPossessWeapon(EWeaponType WeaponType, EEquipPositionType EquipPos);

	/// <summary>
	/// 装備武器の取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 装備武器リスト </returns>
	TMap<EEquipPositionType, EWeaponType> GetPossessWeapon();

	/// <summary>
	/// 撃たれたことにする
	/// </summary>
	/// <param name=""></param>
	/// <returns> ウェポン </returns>
	void SetDamageReaction();

	/// <summary>
	/// 指定ソケット位置を取得する
	/// </summary>
	/// <param name="SocketName"> ソケット名 </param>
	/// <returns></returns>
	FVector GetSocketPosition(FName SocketName);

	/// <summary>
	/// ウェポンメッシュコンポーネントを親のソケットにアタッチする
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "CharacterBase")
	void AttachParantSocket(AWeaponBase* Weapon, EEquipPositionType EquipPos);

	/// <summary>
	/// 死亡処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "CharacterBase")
	void Died();

	/// <summary>
	/// ディゾルブ処理の初期設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "CharacterBase")
	void InitializeDissolve();

	/// <summary>
	/// ディゾルブ処理の設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "CharacterBase")
	void SetupDissolve();

	/// <summary>
	/// ディゾルブ処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "CharacterBase")
	void Dissolve();

	/// <summary>
	/// アニメーションモンタージュ実行
	/// </summary>
	/// <param name="AnimType"> アニメーションの種類 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "CharacterBase")
	void ExecuteAnimMontage(ECharaAnimType AnimType);

	/// <summary>
	/// アニメーションモンタージュ停止
	/// </summary>
	/// <param name="AnimType"> アニメーションの種類 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "CharacterBase")
	void CancelAnimMontage(ECharaAnimType AnimType);

	/// <summary>
	/// 装備中のウェポンの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ウェポン </returns>
	UFUNCTION(BlueprintCallable, Category = "CharacterBase")
	AWeaponBase* GetEquipWeapon();

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

	/// <summary>
	/// 現在のゲーム状況が「プレイ中」かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:プレイ中 </returns>
	bool IsGameStatePlay();

	/// <summary>
	/// 所持武器が複数あるかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:複数武器所持 </returns>
	bool IsPossessWeaponMultiple();

	/// <summary>
	/// 指定ウェポンの取得
	/// </summary>
	/// <param name="Type"> 武器の種類 </param>
	/// <returns> ウェポン </returns>
	AWeaponBase* GetWeapon(EWeaponType Type);

	/// <summary>
	/// サウンドの再生
	/// </summary>
	/// <param name="Sound"> サウンド </param>
	/// <returns></returns>
	void PlaySound(USoundCue* Sound);

	/// <summary>
	/// エフェクトの再生
	/// </summary>
	/// <param name="Effect"> エフェクト </param>
	/// <returns></returns>
	void PlayEffect(UParticleSystem* Effect);

	/// <summary>
	/// 装備位置からソケット名を取得
	/// </summary>
	/// <param name="EquipPos"> 装備位置 </param>
	/// <returns> ソケット名 </returns>
	UFUNCTION(BlueprintCallable, Category = "CharacterBase")
	FName GetEquipSocketName(EEquipPositionType EquipPos);

	/// <summary>
	/// プレイヤーから見た方角を調べる
	/// </summary>
	/// <param name="Angle"> キャラクターに対する角度 </param>
	/// <returns> 方角 </returns>
    EDirectionType SearchDirection(float Angle);

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

	/// <summary>
	/// プレイヤーインプットコンポーネント設定
	/// </summary>
	/// <param name="PlayerInputComponent"> インプットコンポーネント </param>
	/// <returns></returns>
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:

	/// <summary>
	/// ヒットポイントのセット
	/// </summary>
	/// <param name="HP"> ヒットポイント </param>
	/// <returns></returns>
	void SetHitPoint(int32 HP);

	/// <summary>
	/// 前方・後方移動
	/// </summary>
	/// <param name="Value"> 入力方向 </param>
	/// <returns></returns>
	void MoveForward(float Value);

	/// <summary>
	/// 左右方向移動
	/// </summary>
	/// <param name="Value"> 入力方向 </param>
	/// <returns></returns>
	void MoveRight(float Value);

	/// <summary>
	/// ターン
	/// </summary>
	/// <param name="Value"> 入力方向 </param>
	/// <returns></returns>
	void Turn(float Value);

	/// <summary>
	/// 見上げる
	/// </summary>
	/// <param name="Value"> 入力方向 </param>
	/// <returns></returns>
	void LookUp(float Value);

	/// <summary>
	/// 走る
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Run();

	/// <summary>
	/// 走るのをやめる
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StopRunning();

	/// <summary>
	/// 歩行スピードの設定
	/// </summary>
	/// <param name="Speed"> スピード </param>
	/// <returns></returns>
	void SetWalkSpeed(float Speed);

	/// <summary>
	/// ジャンプ開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void TouchStartedJump();

	/// <summary>
	/// ジャンプ終了
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void TouchStoppedJump();

	/// <summary>
	/// しゃがむ・立ち上がる処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CrouchAndStand();

	/// <summary>
	/// 正面を向く
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void FaceForward();

	/// <summary>
	/// 狙う
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Aim();

	/// <summary>
	/// 狙うのをやめる
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StopAiming();

	/// <summary>
	/// 弾を撃つ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Shoot();

	/// <summary>
	/// 弾を撃つのをやめる
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StopShooting();

	/// <summary>
	/// リロード処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Reload();

	/// <summary>
	/// 撃たれたかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:撃たれた </returns>
	bool IsReceiveDamage();

	/// <summary>
	/// リロードをキャンセル
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CancelReload();

	/// <summary>
	/// リロード終了
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "CharacterBase")
	void CompletedReload();

	/// <summary>
	/// カメラ位置の設定
	/// </summary>
	/// <param name="Type"> プレイヤーの姿勢 </param>
	/// <returns></returns>
	void SetupCamera(EPhisicalPosition Type);

	/// <summary>
	/// カメラ調整フェーズの変更
	/// </summary>
	/// <param name="CameraPhase"> カメラ調整フェーズ </param>
	/// <returns></returns>
	void ChangeAdjustCameraPhase(EAdjustCameraPhase CameraPhase);

	/// <summary>
	/// 所持している武器かどうか
	/// </summary>
	/// <param name="Type"> 武器の種類 </param>
	/// <returns> true:所持済み </returns>
	bool IsPossessWeapon(EWeaponType Type);

private:

	/// <summary>
	/// リストの初期化
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeDataList();

	/// <summary>
	/// カメラ情報の初期設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeCameraData();

	/// <summary>
	/// カメラの上下処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void LiftUpAndDownCamera();

	/// <summary>
	/// メッシュコリジョンの設定
	/// </summary>
	/// <param name="Type"> コリジョンの種類 </param>
	/// <returns></returns>
	void SetupMeshCollision(ECollisionType Type);

	/// <summary>
	/// 部位ごとのダメージ倍率を取得
	/// </summary>
	/// <param name="PartsType"> 部位の種類 </param>
	/// <returns> ダメージ倍率 </returns>
	float SearchDamageRate(EBodyPartsType PartsType);

	/// <summary>
	/// ディゾルブのフェーズ処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void ExecuteDissolve(float DeltaTime);

	/// <summary>
	/// ウェポンメッシュを親のソケットにアタッチする
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void AttachParantSocket_Implementation(AWeaponBase* Weapon, EEquipPositionType EquipPos);

	/// <summary>
	/// 死亡処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void Died_Implementation();

	/// <summary>
	/// ディゾルブ処理の初期設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void InitializeDissolve_Implementation();

	/// <summary>
	/// ディゾルブ処理の初期設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void SetupDissolve_Implementation();

	/// <summary>
	/// ディゾルブ処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void Dissolve_Implementation();

	/// <summary>
	/// アニメーションモンタージュ実行
	/// </summary>
	/// <param name="AnimType"> アニメーションの種類 </param>
	/// <returns></returns>
	virtual void ExecuteAnimMontage_Implementation(ECharaAnimType AnimType);

	/// <summary>
	/// アニメーションモンタージュ停止
	/// </summary>
	/// <param name="AnimType"> アニメーションの種類 </param>
	/// <returns></returns>
	virtual void CancelAnimMontage_Implementation(ECharaAnimType AnimType);

public:

	/// <summary>
	/// スプリングアームコンポーネント
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterBase")
	USpringArmComponent* CameraBoom;

	/// <summary>
	/// カメラコンポーネント
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterBase")
	UCameraComponent* FollowCamera;

	/// <summary>
	/// 手に持つ武器のメッシュコンポーネント
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterBase")
	USkeletalMeshComponent* HandWeaponMesh;

	/// <summary>
	/// 背負う武器のメッシュコンポーネント
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterBase")
	USkeletalMeshComponent* BackWeaponMesh;

protected:

	/// <summary>
	/// 走る
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "CharacterBase")
	bool bPressRunButton;

	/// <summary>
	/// ジャンプする
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "CharacterBase")
	bool bPressJumpButton;

	/// <summary>
	/// しゃがんでいる
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "CharacterBase")
	bool bPressCrouchButton;

	/// <summary>
	/// 銃を構える
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "CharacterBase")
	bool bPressAimButton;

	/// <summary>
	/// 弾を撃つ
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "CharacterBase")
	bool bPressShootButton;

	/// <summary>
	/// 武器切り替え
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "CharacterBase")
	bool bPressSwitchButton;

	/// <summary>
	/// リロード中
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "CharacterBase")
	bool bPressReloadButton;

	/// <summary>
	/// ディゾルブ処理済み
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "CharacterBase")
	bool bFinishDissolve;

	/// <summary>
	/// 所持しているウェポン
	/// </summary>
	UPROPERTY(BlueprintReadOnly, Category = "CharacterBase")
	TMap<EEquipPositionType, EWeaponType> PossessWeaponList;

	/// <summary>
	/// ウェポンリスト
	/// </summary>
	UPROPERTY(BlueprintReadOnly, Category = "CharacterBase")
	TMap<EWeaponType, AWeaponBase*> WeaponList;

	/// <summary>
	/// カメラの情報
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterBase")
	UDataTable* CameraDataTable;

	/// <summary>
	/// 部位の種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterBase")
	UDataTable* BodyPartsTable;

	/// <summary>
	/// 部位ごとのダメージ倍率
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterBase")
	UDataTable* DamageRateTable;

	/// <summary>
	/// 装備位置ごとのソケット名
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterBase")
	UDataTable* EquipSocketTable;

	/// <summary>
	/// カメラ距離
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "CharacterBase")
	float CameraDistance;

	/// <summary>
	/// 消滅までの時間 プレイヤー
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "CharacterBase")
	float DisappearanceTime;

	/// <summary>
	/// キャラクターの情報
	/// </summary>
	FCharacterDetailData DetailData;

public:

	/// <summary>
	/// キャラクターの種類
	/// </summary>
	UPROPERTY(BlueprintReadOnly, Category = "CharacterBase")
	ECharacterType CharaType;

	/// <summary>
	/// キャラクターの状態
	/// </summary>
	UPROPERTY(BlueprintReadOnly, Category = "CharacterBase")
	ECharaStateType CharaState;

	/// <summary>
	/// ディゾルブの種類
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "CharacterBase")
	EDissolveType DissolveType;

protected:

	/// <summary>
	/// ヒットポイント
	/// </summary>
	int32 HitPoint;

	/// <summary>
	/// 撃たれる
	/// </summary>
	bool bRecieveDamage;

	/// <summary>
	/// 弾の進入角度
	/// </summary>
	float HitRotYaw;

private:

	/// <summary>
	/// カメラの情報 起立時
	/// </summary>
	TMap<EPhisicalPosition, FCameraDetail> CameraData;

	/// <summary>
	/// カメラ調整フェーズ
	/// </summary>
	EAdjustCameraPhase AdjustCameraPhase;

	/// <summary>
	/// 現在のディゾルブフェーズ
	/// </summary>
	EDissolvePhase CurrentDissolvePhase;

	/// <summary>
	/// カメラ移動量
	/// </summary>
	float MoveCameraValue;

	/// <summary>
	/// 累積時間
	/// </summary>
	float AccumulateTime;

	/// <summary>
	/// 待機時間
	/// </summary>
	float WaitTime;

	/// <summary>
	/// 部位の種類リスト
	/// </summary>
	TMap<FName, EBodyPartsType> BodyPartsList;

	/// <summary>
	/// 部位ごとのダメージ倍率
	/// </summary>
	TMap<EBodyPartsType, float> DamageRateList;

	/// <summary>
	/// 装備位置ごとのソケット名
	/// </summary>
	TMap<EEquipPositionType, FName> EquipSocketList;

protected:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// 回転量初期値
	/// </summary>
	const float InitRotValue = 0.0f;

	/// <summary>
	/// 回転量 Roll 初期値
	/// </summary>
	const FRotator InitRotRate = FRotator(0.0f, 0.0f, 360.0f);

	/// <summary>
	/// 空中操作量
	/// </summary>
	const float AirControlValue = 0.5f;

	/// <summary>
	/// 重力
	/// </summary>
	const float Gravity = 1.0f;

	/// <summary>
	/// 調整フレーム数
	/// </summary>
	const float AdjustFrames = 10.0f;

	/// <summary>
	/// ターン割合
	/// </summary>
	const float TurnValueRate = 0.5f;

	/// <summary>
	/// プレイヤーが瀕死状態になるHPの割合
	/// </summary>
	const float DyingLineRate = 0.3f;

	/// <summary>
	/// プレイヤーが死亡状態になるHP
	/// </summary>
	const int32 DeadLine = 0;

	/// <summary>
	/// ダメージ倍率 デフォルト値
	/// </summary>
	const float DamageDefaultRate = 1.0f;

	/// <summary>
	/// 時間リセット
	/// </summary>
	const float ResetTime = 0.0f;

	/// <summary>
	/// 所持武器が1つ
	/// </summary>
	const int32 OneWeapon = 1;

	/// <summary>
	/// ホイール操作なし
	/// </summary>
	const float NoneWheelValue = 0.0f;

	/// <summary>
	/// ホイール操作を受け付ける
	/// </summary>
	const float InputWheelValue = 3.0f;

	/// <summary>
	/// 45°
	/// </summary>
	const float ForwardRangeMax = 45.0f;

	/// <summary>
	/// 135°
	/// </summary>
	const float RightRangeMax = 135.0f;

	/// <summary>
	/// 225°
	/// </summary>
	const float BackRangeMax = 225.0f;

	/// <summary>
	/// 315°
	/// </summary>
	const float LeftRangeMax = 315.0f;

	/// <summary>
	/// 0.0°
	/// </summary>
	const float Angle0 = 0.0f;

	/// <summary>
	/// 360°
	/// </summary>
	const float Angle360 = 360.0f;
};