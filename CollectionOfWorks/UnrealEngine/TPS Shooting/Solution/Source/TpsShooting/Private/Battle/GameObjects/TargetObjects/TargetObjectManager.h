﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Battle/GameObjects/TargetObjects/GoalObject.h"
#include "Engine/DataTable.h"
#include "../Public/TpsShootingGameInstance.h"
#include "TargetObjectManager.generated.h"


/// <summary>
/// オブジェクトのスポーン位置
/// </summary>
USTRUCT(BlueprintType)
struct FTargetObjectSpawnData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// バトルレベルの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TargetObjectManager")
	ELevelType BattleLevelType;

	/// <summary>
	/// エリアの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TargetObjectManager")
	EStageType StageType;

	/// <summary>
	/// スポーン位置 X座標
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TargetObjectManager")
	float PositionX;

	/// <summary>
	/// スポーン位置 Y座標
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TargetObjectManager")
	float PositionY;

	/// <summary>
	/// スポーン位置 Z座標
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TargetObjectManager")
	float PositionZ;
};

/// <summary>
/// ターゲットオブジェクトマネージャークラス
/// </summary>
UCLASS()
class ATargetObjectManager : public AActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ATargetObjectManager();

	/// <summary>
	/// オブジェクトの生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SpawnObjects();

	/// <summary>
	/// ゴールインデックスリストを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ゴールインデックスリスト </returns>
	TArray<int32> GetListIndexArray();

	/// <summary>
	/// 指定のゴールオブジェクトを取得
	/// </summary>
	/// <param name="Index"> インデックス </param>
	/// <returns> ゴールオブジェクト </returns>
	AGoalObject* GetGoalObject(int32 Index);

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

private:

	/// <summary>
	/// ゴールオブジェクトクラスのロード
	/// </summary>
	/// <param name=""></param>
	/// <returns> ゴールオブジェクトクラス </returns>
	TSubclassOf<AGoalObject> LoadGoalObjectClass();

	/// <summary>
	/// ゴールオブジェクトの生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SpawnGoalObjects();

protected:

	/// <summary>
	/// ゴールオブジェクトデータテーブル
	/// </summary>
	UPROPERTY(EditAnywhere, Category="TargetObjectManager")
	UDataTable* GoalObjectTable;

private:

	/// <summary>
	/// ゴールオブジェクトリスト
	/// </summary>
	UPROPERTY()
	TMap<int32, AGoalObject*> GoalObjectList;

	/// <summary>
	/// ゴールオブジェクトインデックスリスト
	/// </summary>
	UPROPERTY()
	TArray<int32> GoalIndexList;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	const int32 MinIndex = 0;
};
