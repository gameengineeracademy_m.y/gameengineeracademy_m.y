﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Widgets/LowHealthWidget.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ULowHealthWidget::ULowHealthWidget() {

}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ULowHealthWidget::NativeConstruct() {

}

/// <summary>
/// ウィジェットを表示する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ULowHealthWidget::Open() {

	Super::Open();

	// ウィジェットを透過して見えないようにしておく
	SetRenderOpacity(Transparent);
}

/// <summary>
/// ウィジェットを非表示にする
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ULowHealthWidget::Closed() {

	// アニメーションを終了する
	FinishLowHealthAnimation();
}

/// <summary>
/// アニメーション開始
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ULowHealthWidget::PlayLowHealthAnimation_Implementation() {

}

/// <summary>
/// アニメーション終了
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ULowHealthWidget::FinishLowHealthAnimation_Implementation() {

}