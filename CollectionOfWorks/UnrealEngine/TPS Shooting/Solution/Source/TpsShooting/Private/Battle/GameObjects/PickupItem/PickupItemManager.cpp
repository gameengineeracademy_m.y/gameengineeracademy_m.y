﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/GameObjects/PickupItem/PickupItemManager.h"
#include "Battle/Weapons/WeaponBase.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
APickupItemManager::APickupItemManager()
	: ItemSpawnTable(nullptr)
	, PickupItemAmmoClass()
	, PickupItemHealClass()
	, PickupItemWeaponClass()
	, EventInterface()
	, PickupItemList() {
 	
	// Tickイベントの無効化
	PrimaryActorTick.bCanEverTick = false;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APickupItemManager::BeginPlay() {

	Super::BeginPlay();

	LoadPickupItemClass();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void APickupItemManager::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}

/// <summary>
/// イベントインターフェース
/// </summary>
/// <param name=""> イベントインターフェース </param>
/// <returns></returns>
void APickupItemManager::SetEventInterface(const TScriptInterface<IBattleEvent>& Interface) {

	EventInterface = Interface;
}

/// <summary>
/// クラスのロード
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APickupItemManager::LoadPickupItemClass() {

	// 弾薬アイテムクラスのロード ソフト参照
	FString PickupItemAmmoPath;
	GConfig->GetString(TEXT("ActorSettings"), TEXT("PickupItemAmmoPath"), PickupItemAmmoPath, GGameIni);
	PickupItemAmmoClass = TSoftClassPtr<APickupItemAmmo>(FSoftObjectPath(*PickupItemAmmoPath)).LoadSynchronous();
	if (!PickupItemAmmoClass) {
		UE_LOG(LogTemp, Display, TEXT("APickupItemManager PickupItemAmmoClass Error"));
		return;
	}

	// 回復アイテムクラスのロード ソフト参照
	FString PickupItemHealPath;
	GConfig->GetString(TEXT("ActorSettings"), TEXT("PickupItemHealPath"), PickupItemHealPath, GGameIni);
	PickupItemHealClass = TSoftClassPtr<APickupItemHeal>(FSoftObjectPath(*PickupItemHealPath)).LoadSynchronous();
	if (!PickupItemHealClass) {
		UE_LOG(LogTemp, Display, TEXT("APickupItemManager PickupItemHealClass Error"));
		return;
	}

	// 武器アイテムクラスのロード ソフト参照
	FString PickupItemWeaponPath;
	GConfig->GetString(TEXT("ActorSettings"), TEXT("PickupItemWeaponPath"), PickupItemWeaponPath, GGameIni);
	PickupItemWeaponClass = TSoftClassPtr<APickupItemWeapon>(FSoftObjectPath(*PickupItemWeaponPath)).LoadSynchronous();
	if (!PickupItemWeaponClass) {
		UE_LOG(LogTemp, Display, TEXT("APickupItemManager PickupItemWeaponClass Error"));
		return;
	}
}

/// <summary>
/// アイテムのスポーン
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APickupItemManager::SpawnPickupItem() {

	//---------------------------------------
	// 現在のレベルを取得
	//---------------------------------------
	UTpsShootingGameInstance* GameInstance = Cast<UTpsShootingGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (!IsValid(GameInstance)) {
		return;
	}
	ELevelType CurrentLevelType = GameInstance->GetCurrentLevel();

	//---------------------------------------
	// データテーブルからスポーン位置取得
	//---------------------------------------
	if (!IsValid(ItemSpawnTable)) {
		return;
	}

	int32 ListIndex = MinIndex;
	int32 AmmoIndex = MinIndex;
	int32 HealIndex = MinIndex;
	int32 WeaponIndex = MinIndex;

	TArray<FName> RowNames = ItemSpawnTable->GetRowNames();
	for (FName RowName : RowNames) {

		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}
		FItemSpawnData* ItemSpawn = ItemSpawnTable->FindRow<FItemSpawnData>(RowName, "");
		if (ItemSpawn->BattleLevelType != CurrentLevelType) {
			continue;
		}
		
		FVector SpawnPosition = FVector::ZeroVector;
		FRotator SpawnRotation = FRotator::ZeroRotator;
		SpawnPosition = FVector(ItemSpawn->PositionX, ItemSpawn->PositionY, ItemSpawn->PositionZ);

		//------------------------------------
		// アイテムのスポーン
		//------------------------------------
		APickupItemBase* PickupItem = nullptr;
		FActorSpawnParameters Params;
		int32 ItemIndex = MinIndex;
		EPickupItemType ItemType = ItemSpawn->PickupItemType;
		switch (ItemType) {
		case EPickupItemType::Ammo: {
			if (!PickupItemAmmoClass) {
				break;
			}
			ItemIndex = ++AmmoIndex;
			PickupItem = GetWorld()->SpawnActor<APickupItemAmmo>(PickupItemAmmoClass, SpawnPosition, FRotator::ZeroRotator, Params);
			break;
		}
		case EPickupItemType::Healing: {
			if (!PickupItemHealClass) {
				break;
			}
			ItemIndex = ++HealIndex;
			PickupItem = GetWorld()->SpawnActor<APickupItemHeal>(PickupItemHealClass, SpawnPosition, FRotator::ZeroRotator, Params);
			break;
		}
		case EPickupItemType::Weapon: {

			if (!PickupItemWeaponClass) {
				break;
			}
			ItemIndex = ++WeaponIndex;
			PickupItem = GetWorld()->SpawnActor<APickupItemWeapon>(PickupItemWeaponClass, SpawnPosition, FRotator::ZeroRotator, Params);
			break;
		}
		}

		if (!IsValid(PickupItem)) {
			continue;
		}
		PickupItem->SetStageType(ItemSpawn->StageType);
		// アイテムをリストに登録
		PickupItemList.Add(ListIndex, PickupItem);
		ListIndexArray.Add(ListIndex);
		++ListIndex;
	}
}

/// <summary>
/// アイテムリストのインデックスリスト取得
/// </summary>
/// <param name=""></param>
/// <returns> インデックスリスト </returns>
TArray<int32> APickupItemManager::GetListIndexArray() {

	return ListIndexArray;
}

/// <summary>
/// リストからアイテムを取得
/// </summary>
/// <param name="Index"> インデックス </param>
/// <returns> アイテム </returns>
APickupItemBase* APickupItemManager::GetPickupItem(int32 Index) {

	if (!PickupItemList.Contains(Index)) {
		return nullptr;
	}

	return PickupItemList[Index];
}

/// <summary>
/// アイテムの状態をリセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APickupItemManager::ResetPickupItem() {

	//---------------------------------------
	//一度きりの入手アイテムを再度取得できる
	//ようにするか判断し、再設定を行う
	//---------------------------------------

	for (auto PickupItem : PickupItemList) {

		if (!IsValid(PickupItem.Value)) {
			continue;
		}
		EPickupType PickupType = PickupItem.Value->GetPickupType();
		if (PickupType != EPickupType::OnlyOnce) {
			continue;
		}

		EPickupItemType PickupItemType = PickupItem.Value->GetPickupItemType();
		switch (PickupItemType) {
		case EPickupItemType::Weapon: {

			APickupItemWeapon* ItemWeapon = Cast<APickupItemWeapon>(PickupItem.Value);
			if (!IsValid(ItemWeapon)) {
				break;
			}
			EPickupWeaponType WeaponItemType = ItemWeapon->GetPickupWeaponType();
			EWeaponType WeaponType = EWeaponType::None;
			switch (WeaponItemType) {
			case EPickupWeaponType::AssaultRifle: {
				WeaponType = EWeaponType::AssaultRifle;
				break;
			}
			case EPickupWeaponType::SniperRifle: {
				WeaponType = EWeaponType::SniperRifle;
				break;
			}
			}

			// 指定の武器がセーブデータにあるかどうか確認
			if (EventInterface->OnIsSavePossessWeapon(WeaponType)) {
				break;
			}
			PickupItem.Value->SetupPickupSystem();
			break;
		}
		}
	}
}
