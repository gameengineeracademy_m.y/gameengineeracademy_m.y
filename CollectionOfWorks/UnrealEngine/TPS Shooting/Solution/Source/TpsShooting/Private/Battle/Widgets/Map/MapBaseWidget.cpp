﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Widgets/Map/MapBaseWidget.h"
#include "Kismet/KismetMathLibrary.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Battle/GameState/BattleGameState.h"
#include "Components/Overlay.h"
#include "Components/OverlaySlot.h"
#include "Components/Image.h"
#include "Components/Border.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UMapBaseWidget::UMapBaseWidget()
	: MapTable(nullptr)
	, MapIconTable(nullptr)
	, ImageSize()
	, CurrentStageType(EStageType::None)
	, PlayerManager(nullptr)
	, EnemyManager(nullptr)
	, PickupItemManager(nullptr)
	, TargetObjectManager(nullptr)
	, MapIconList()
	, EnemyIconList()
	, PickupItemIconList()
	, GoalIconList()
	, MapCenterPos() {

}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMapBaseWidget::NativeConstruct() {

	Super::NativeConstruct();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="Geometry"> ジオメトリ </param>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void UMapBaseWidget::NativeTick(const FGeometry& Geometry, float DeltaTime) {

	Super::NativeTick(Geometry, DeltaTime);
}

/// <summary>
/// エリアの種類のセット
/// </summary>
/// <param name="Type"> エリアの種類 </param>
/// <returns></returns>
void UMapBaseWidget::SetStageType(EStageType Type) {

	CurrentStageType = Type;
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMapBaseWidget::InitializeWidget() {

	//---------------------------------------
	// 現在のレベルを取得
	//---------------------------------------
	UTpsShootingGameInstance* GameInstance = Cast<UTpsShootingGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (!IsValid(GameInstance)) {
		return;
	}
	ELevelType CurrentLevelType = GameInstance->GetCurrentLevel();

	// テーブルが存在しない場合は処理終了
	if (!IsValid(MapTable) || !IsValid(MapIconTable)) {
		return;
	}

	//---------------------------------------
	// マップの設定
	//---------------------------------------
	TArray<FName> RowNames = MapTable->GetRowNames();
	for (FName RowName : RowNames) {

		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}

		FMapType* MapType = MapTable->FindRow<FMapType>(RowName, "");
		if (MapType->BattleLevelType != CurrentLevelType) {
			continue;
		}
		SetMapImage(MapType->MapTexture);
	}

	//---------------------------------------
	// マップアイコンリストの設定
	//---------------------------------------
	RowNames = MapIconTable->GetRowNames();
	for (FName RowName : RowNames) {

		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}

		FMapIconData* MapIconData = MapIconTable->FindRow<FMapIconData>(RowName, "");
		// 既にリストに登録済みなら次の行へ
		if (MapIconList.Contains(MapIconData->MapIconType)) {
			continue;
		}
		MapIconList.Add(MapIconData->MapIconType, MapIconData->MapIcon);
	}
}

/// <summary>
/// マップテクスチャのセット
/// </summary>
/// <param name="Texture"> テクスチャ </param>
/// <returns></returns>
void UMapBaseWidget::SetMapImage(UTexture2D* Texture) {

	UBorder* MapImage = Cast<UBorder>(GetWidgetFromName(BorderMapName));
	if (!IsValid(MapImage)) {
		return;
	}
	MapImage->SetBrushFromTexture(Texture);
}

/// <summary>
/// プレイヤーマネージャをセット
/// </summary>
/// <param name="Manager"> プレイヤーマネージャ </param>
/// <returns></returns>
void UMapBaseWidget::SetPlayerManager(APlayerManager* Manager) {

	PlayerManager = Manager;
}

/// <summary>
/// エネミーマネージャをセット
/// </summary>
/// <param name="Manager"> エネミーマネージャ </param>
/// <returns></returns>
void UMapBaseWidget::SetEnemyManager(AEnemyManager* Manager) {

	EnemyManager = Manager;
}

/// <summary>
/// アイテムマネージャをセット
/// </summary>
/// <param name="Manager"> アイテムマネージャ </param>
/// <returns></returns>
void UMapBaseWidget::SetPickupItemManager(APickupItemManager* Manager) {

	PickupItemManager = Manager;
}

/// <summary>
/// ターゲットオブジェクトマネージャをセット
/// </summary>
/// <param name="Manager"> ターゲットオブジェクトマネージャ </param>
/// <returns></returns>
void UMapBaseWidget::SetTargetObjectManager(ATargetObjectManager* Manager) {

	TargetObjectManager = Manager;
}

/// <summary>
/// 初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMapBaseWidget::SetupWidget() {

	// マップ画像のサイズを取得しておく
	SetupMapImageSize();

	// エネミー用のアイコンを作成
	SetupEnemyIcon();
	// アイテム用のアイコンを作成
	SetupItemIcon();
	// ゴール用のアイコンを作成
	SetupGoalIcon();
}

/// <summary>
/// エネミーアイコンの設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMapBaseWidget::SetupEnemyIcon() {

	if (!IsValid(EnemyManager)) {
		return;
	}

	// エネミーアイコンが設定されていない場合は処理終了
	if (!MapIconList.Contains(EMapIconType::Enemy)) {
		return;
	}
	if (!IsValid(MapIconList[EMapIconType::Enemy])) {
		return;
	}

	TArray<int32> IndexList = EnemyManager->GetEnemyIndexList();
	for (int32 Index : IndexList) {

		AEnemy* Enemy = EnemyManager->GetEnemy(Index);
		if (!IsValid(Enemy)) {
			continue;
		}

		UBattleHUDMapIcon* MapIcon = CreateMapIcon();
		if (!IsValid(MapIcon)) {
			continue;
		}
		// マップアイコンの各種設定
		MapIcon->SetBattleHUDItemType(EBattleHUDItemType::MapIcon);
		MapIcon->SetBattleHUDIcon(MapIconList[EMapIconType::Enemy], ImageIconName);
		MapIcon->SetMapIconType(EMapIconType::Enemy);
		MapIcon->SetStageType(Enemy->GetStageType());
		MapIcon->SetObjectIndex(Index);
		MapIcon->SetupDisplayBackImage(ESlateVisibility::Collapsed);
		MapIcon->ResetPlayAnimFlag();
		// マップ表示用のキャンバスパネルの子に設定
		AddChildOverlay(MapIcon);
		// 表示しておく
		MapIcon->SetDisplay(true);
		MapIcon->SetRenderOpacity(Opacity);

		EnemyIconList.Add(MapIcon);
	}
}

/// <summary>
/// アイテムアイコンの設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMapBaseWidget::SetupItemIcon() {

	if (!IsValid(PickupItemManager)) {
		return;
	}

	// アイテムアイコンが設定されていない場合は処理終了
	if (!MapIconList.Contains(EMapIconType::ItemAmmo) ||
		!MapIconList.Contains(EMapIconType::ItemHeal) ||
		!MapIconList.Contains(EMapIconType::ItemWeapon)) {
		return;
	}
	if (!IsValid(MapIconList[EMapIconType::ItemAmmo]) ||
		!IsValid(MapIconList[EMapIconType::ItemHeal]) ||
		!IsValid(MapIconList[EMapIconType::ItemWeapon])) {
		return;
	}

	TArray<int32> IndexList = PickupItemManager->GetListIndexArray();
	for (int32 Index : IndexList) {

		APickupItemBase* Item = PickupItemManager->GetPickupItem(Index);
		if (!IsValid(Item)) {
			continue;
		}

		UBattleHUDMapIcon* MapIcon = CreateMapIcon();
		if (!IsValid(MapIcon)) {
			continue;
		}

		// アイテムの種類を取得し、マップアイコンの種類を決定する
		EPickupItemType ItemType = Item->GetPickupItemType();
		EMapIconType MapIconType = EMapIconType::None;
		switch (ItemType) {
		case EPickupItemType::Ammo: {
			MapIconType = EMapIconType::ItemAmmo;
			break;
		}
		case EPickupItemType::Healing: {
			MapIconType = EMapIconType::ItemHeal;
			break;
		}
		case EPickupItemType::Weapon: {
			MapIconType = EMapIconType::ItemWeapon;
			break;
		}
		}

		// マップアイコンの各種設定
		MapIcon->SetBattleHUDItemType(EBattleHUDItemType::MapIcon);
		MapIcon->SetBattleHUDIcon(MapIconList[MapIconType], ImageIconName);
		MapIcon->SetMapIconType(MapIconType);
		MapIcon->SetStageType(Item->GetStageType());
		MapIcon->SetObjectIndex(Index);
		MapIcon->SetupDisplayBackImage(ESlateVisibility::Collapsed);
		MapIcon->ResetPlayAnimFlag();
		// マップ表示用のキャンバスパネルの子に設定
		AddChildOverlay(MapIcon);
		// 表示しておく
		MapIcon->SetDisplay(true);
		MapIcon->SetRenderOpacity(Opacity);

		// マップ上に配置する
		FVector ItemPosition = Item->GetActorLocation();
		FVector2D MapPosition = GetPositionOnMap(ItemPosition);
		FWidgetTransform ItemTransform;
		ItemTransform.Translation = FVector2D(MapPosition.X, MapPosition.Y);
		MapIcon->SetRenderTransform(ItemTransform);

		PickupItemIconList.Add(MapIcon);
	}
}

/// <summary>
/// ゴールアイコンの設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMapBaseWidget::SetupGoalIcon() {

	if (!IsValid(TargetObjectManager)) {
		return;
	}

	// ゴールアイコンリストが空でない場合は処理終了
	if (GoalIconList.IsValidIndex(MinIndex)) {
		return;
	}

	// アイテムアイコンが設定されていない場合は処理終了
	if (!MapIconList.Contains(EMapIconType::TargetPoint)) {
		return;
	}
	if (!IsValid(MapIconList[EMapIconType::TargetPoint])) {
		return;
	}

	TArray<int32> IndexList = TargetObjectManager->GetListIndexArray();
	for (int32 Index : IndexList) {

		AGoalObject* GoalObject = TargetObjectManager->GetGoalObject(Index);
		if (!IsValid(GoalObject)) {
			continue;
		}

		UBattleHUDMapIcon* MapIcon = CreateMapIcon();
		if (!IsValid(MapIcon)) {
			continue;
		}

		// マップアイコンの種類を設定
		EMapIconType MapIconType = EMapIconType::TargetPoint;

		// マップアイコンの各種設定
		MapIcon->SetBattleHUDItemType(EBattleHUDItemType::MapIcon);
		MapIcon->SetBattleHUDIcon(MapIconList[MapIconType], ImageIconName);
		MapIcon->SetMapIconType(MapIconType);
		MapIcon->SetStageType(GoalObject->GetStageType());
		MapIcon->SetObjectIndex(Index);
		MapIcon->SetupDisplayBackImage(ESlateVisibility::SelfHitTestInvisible);
		MapIcon->ResetPlayAnimFlag();
		// マップ表示用のキャンバスパネルの子に設定
		AddChildOverlay(MapIcon);
		// 表示しておく
		MapIcon->SetDisplay(true);
		MapIcon->SetRenderOpacity(Opacity);

		// マップ上に配置する
		FVector GoalPosition = GoalObject->GetActorLocation();
		FVector2D MapPosition = GetPositionOnMap(GoalPosition);
		FWidgetTransform ItemTransform;
		ItemTransform.Translation = FVector2D(MapPosition.X, MapPosition.Y);
		MapIcon->SetRenderTransform(ItemTransform);

		GoalIconList.Add(MapIcon);
	}
}

/// <summary>
/// マップアイコンの作成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UBattleHUDMapIcon* UMapBaseWidget::CreateMapIcon() {

	UBattleHUDMapIcon* MapIcon = nullptr;
	FString Path;
	GConfig->GetString(TEXT("UMGBattleHUDSettings"), TEXT("BattleHUDMapIcon"), Path, GGameIni);

	TSubclassOf<UBattleHUDMapIcon> MapIconClass = TSoftClassPtr<UBattleHUDMapIcon>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (!MapIconClass) {
		return nullptr;
	}
	MapIcon = CreateWidget<UBattleHUDMapIcon>(GetWorld(), MapIconClass);

	return MapIcon;
}

/// <summary>
/// マップアイコン用のオーバーレイの子に設定
/// </summary>
/// <param name="MapIcon"> マップアイコン </param>
/// <returns></returns>
void UMapBaseWidget::AddChildOverlay(UBattleHUDMapIcon* MapIcon) {

	UOverlay* Overlay = Cast<UOverlay>(GetWidgetFromName(OverlayMapIconName));
	if (!IsValid(Overlay)) {
		return;
	}
	UOverlaySlot* OverlaySlot = Overlay->AddChildToOverlay(MapIcon);
	OverlaySlot->SetHorizontalAlignment(EHorizontalAlignment::HAlign_Center);
	OverlaySlot->SetVerticalAlignment(EVerticalAlignment::VAlign_Center);
}

/// <summary>
/// マップアイコンのリセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMapBaseWidget::ResetMapIcon() {

	// エネミーマップアイコンの初期化
	ResetMapIconEnemy();
	// アイテムマップアイコンの初期化
	ResetMapIconItem();
}

/// <summary>
/// エネミーデータの再設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMapBaseWidget::ResetEnemyData() {

	// エネミーマップアイコンの初期化と再設定
	ResetMapIconEnemy();
	SetupEnemyIcon();
}

/// <summary>
/// エネミーアイコンのリセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMapBaseWidget::ResetMapIconEnemy() {

	// オーバーレイを取得しておく
	UOverlay* Overlay = Cast<UOverlay>(GetWidgetFromName(OverlayMapIconName));

	for (UBattleHUDMapIcon* MapIcon : EnemyIconList) {
		if (!IsValid(MapIcon)) {
			continue;
		}
		// オーバーレイコンポーネントから切り離す
		if (IsValid(Overlay)) {
			Overlay->RemoveChild(MapIcon);
		}
		MapIcon = nullptr;
	}
	EnemyIconList.Empty();
}

/// <summary>
/// アイテムアイコンのリセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMapBaseWidget::ResetMapIconItem() {

	// オーバーレイを取得しておく
	UOverlay* Overlay = Cast<UOverlay>(GetWidgetFromName(OverlayMapIconName));

	for (UBattleHUDMapIcon* MapIcon : PickupItemIconList) {
		if (!IsValid(MapIcon)) {
			continue;
		}
		// オーバーレイコンポーネントから切り離す
		if (IsValid(Overlay)) {
			Overlay->RemoveChild(MapIcon);
		}
		MapIcon = nullptr;
	}
	PickupItemIconList.Empty();
}

/// <summary>
/// 表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMapBaseWidget::Display() {

	// プレイヤーアイコン更新
	UpdatePlayerIcon();
	// エネミーアイコン更新
	UpdateEnemyIcon();
	// アイテムアイコン更新
	UpdatePickupItemIcon();

	SetVisibility(ESlateVisibility::SelfHitTestInvisible);
}

/// <summary>
/// 非表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMapBaseWidget::Hide() {

	//-----------------------------------
	//非表示中はTick処理を止めるために
	//Visibilityの「Collapsed」を使用
	//-----------------------------------
	SetVisibility(ESlateVisibility::Collapsed);
}

/// <summary>
/// 実際の位置からマップ上の位置を取得
/// </summary>
/// <param name="Position"> 実際の位置 </param>
/// <returns> マップ上の位置 </returns>
FVector2D UMapBaseWidget::GetPositionOnMap(FVector Position) {

	//------------------------------------------------
	//レベル上に配置したブロッキングボリュームと
	//マップ画像のサイズの比率からマップ上の座標を算出
	//------------------------------------------------

	FVector MapPosition = (Position - VolumePosition) / (VolumeSize * SizeRate) * ImageSize.X;

	return FVector2D(MapPosition.X, MapPosition.Y);
}

/// <summary>
/// マップ画像のサイズを変更
/// </summary>
/// <param name="Size"> マップサイズ </param>
/// <returns></returns>
void UMapBaseWidget::ChangeMapImageSize_Implementation(float Size) {

}

/// <summary>
/// マップ画像のサイズを取得
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMapBaseWidget::SetupMapImageSize_Implementation() {

}