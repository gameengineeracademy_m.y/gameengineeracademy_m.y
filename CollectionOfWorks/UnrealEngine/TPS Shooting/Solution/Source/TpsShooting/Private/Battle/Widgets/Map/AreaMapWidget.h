﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Battle/Widgets/Map/MapBaseWidget.h"
#include "AreaMapWidget.generated.h"


/// <summary>
/// マップのマスクの種類
/// </summary>
USTRUCT(BlueprintType)
struct FAreaMapMaskType : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// バトルレベルの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AreaMapWidget")
	ELevelType BattleLevelType;

	/// <summary>
	/// エリアの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AreaMapWidget")
	EStageType StageType;

	/// <summary>
	/// マスクテクスチャ
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AreaMapWidget")
	UTexture2D* MaskTexture;
};

/// <summary>
/// マップ表示情報
/// </summary>
USTRUCT(BlueprintType)
struct FDisplayMapData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// バトルレベルの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AreaMapWidget")
	ELevelType BattleLevelType;

	/// <summary>
	/// エリアの種類
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AreaMapWidget")
	EStageType StageType;

	/// <summary>
	/// 中心座標 X座標
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AreaMapWidget")
	float CenterPosX;

	/// <summary>
	/// 中心座標 Y座標
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AreaMapWidget")
	float CenterPosY;

	/// <summary>
	/// 画像サイズ
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AreaMapWidget")
	float ImageSize;

	/// <summary>
	/// 回転角度
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AreaMapWidget")
	float Angle;
};

/// <summary>
/// エリアマップウィジェット
/// </summary>
UCLASS()
class UAreaMapWidget : public UMapBaseWidget
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UAreaMapWidget();

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="Geometry"> ジオメトリ </param>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void NativeTick(const FGeometry& Geometry, float DeltaTime) override;

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeWidget();

	/// <summary>
	/// エリアの切り替え
	/// </summary>
	/// <param name="StageType"> エリアの種類 </param>
	/// <returns></returns>
	void SwitchAreaMap(EStageType StageType);

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

	/// <summary>
	/// プレイヤー位置更新
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void UpdatePlayerIcon() override;

	/// <summary>
	/// エネミーアイコンの更新
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void UpdateEnemyIcon() override;

	/// <summary>
	/// アイテムアイコンの更新
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void UpdatePickupItemIcon() override;

	/// <summary>
	/// ゴールアイコンの更新
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void UpdateGoalIcon() override;

public:

	/// <summary>
	/// マップマスクテーブル
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "AreaMapWidget")
	UDataTable* MapMaskTable;

	/// <summary>
	/// マップ表示情報テーブル
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "AreaMapWidget")
	UDataTable* DisplayMapDataTable;

private:

	/// <summary>
	/// マップマスクリスト
	/// </summary>
	TMap<EStageType, UTexture2D*> MapMaskList;

	/// <summary>
	/// マップ表示情報
	/// </summary>
	TMap<EStageType, FDisplayMapData*> MapDisplayDataList;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// イメージ マップマスク名
	/// </summary>
	FName ImageMaskName = "Image_Mask";

	/// <summary>
	/// パネル マップ名
	/// </summary>
	FName CanvasPanelName = "CanvasPanel_AreaMap";
};