﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/WeatherSystem/Weather.h"
#include "Kismet/GameplayStatics.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AWeather::AWeather()
	: ParticleSystemComp(nullptr)
	, WeatherEffect(nullptr) {

	// Tick処理の無効化
	PrimaryActorTick.bCanEverTick = false;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeather::BeginPlay() {

	Super::BeginPlay();
}

/// <summary>
/// 天候エフェクトを設定する
/// </summary>
/// <param name="Effect"> 天候エフェクト </param>
/// <returns></returns>
void AWeather::SetupWeatherEffect(UParticleSystem* Effect) {

	if (!IsValid(Effect)) {
		return;
	}
	WeatherEffect = Effect;
	ParticleSystemComp = UGameplayStatics::SpawnEmitterAttached(WeatherEffect, RootComponent);
}

/// <summary>
/// エフェクトの破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AWeather::DestroyEffect() {

	if (!IsValid(ParticleSystemComp) || !IsValid(WeatherEffect)) {
		return;
	}
	ParticleSystemComp->DeactivateSystem();
	ParticleSystemComp->DestroyComponent(true);
	WeatherEffect = nullptr;
}