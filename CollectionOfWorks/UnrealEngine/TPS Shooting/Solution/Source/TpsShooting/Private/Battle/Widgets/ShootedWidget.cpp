﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Widgets/ShootedWidget.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UShootedWidget::UShootedWidget() {

}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UShootedWidget::NativeConstruct() {

}

/// <summary>
/// アニメーション再生
/// </summary>
/// <param name="DirectionType"> 撃たれた方向 </param>
/// <returns></returns>
void UShootedWidget::PlayShootedAnimation_Implementation(EDirectionType DirectionType) {

}