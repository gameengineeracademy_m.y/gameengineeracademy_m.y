﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Battle/Character/CharacterBase.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Runtime/Engine/Classes/Engine/TargetPoint.h"
#include "NavigationPath.h"
#include "EnvironmentQuery/EnvQueryTypes.h"
#include "EnvironmentQuery/EnvQuery.h"
#include "Enemy.generated.h"


/// <summary>
/// エネミーの種類
/// </summary>
enum class EEnemyType {
	None,
	Normal,
	Sniper,
	MaxTypeIndex
};

/// <summary>
/// 警戒レベル
/// </summary>
UENUM(BlueprintType)
enum class EAlertLevel : uint8 {
	None,         // なにもしない
	Patrol,       // 巡回中
	Beware,       // 警戒中
	Fight,        // 戦闘中
	MaxTypeIndex
};

/// <summary>
/// 巡回中行動フェーズ
/// </summary>
UENUM(BlueprintType)
enum class EPatrolPhase : uint8 {
	None,            // なにもしない
	DecidePos,       // 移動先決定
	SearchRoute,     // 経路探索
	Turn,            // 方向転換
	Move,            // 移動
	Wait,            // 待機
	Look,            // じっと見る
	ApproachSlowly,  // ゆっくり近づく
	NonCombat,       // 非戦闘態勢
	Return,          // 帰還
	NotifyWait,      // 通知待機
	MaxPhaseIndex
};

/// <summary>
/// 警戒中行動フェーズ
/// </summary>
UENUM(BlueprintType)
enum class EBewarePhase : uint8 {
	None,             // なにもしない
	Chase,            // 追跡処理
	Hide,             // 隠れる処理
	MaxPhaseIndex
};

/// <summary>
/// 戦闘中行動フェーズ
/// </summary>
UENUM(BlueprintType)
enum class EFightPhase : uint8 {
	None,             // なにもしない
	Chase,            // 追跡処理
	Shoot,            // 射撃処理
	MaxPhaseIndex
};

/// <summary>
/// 追跡中行動フェーズ
/// </summary>
UENUM(BlueprintType)
enum class EChasePhase : uint8 {
	None,             // なにもしない
	Wait,             // その場で待機
	Move,             // 移動
	Patrol,           // パトロール
	SearchPosition,   // 移動先位置探索
	MaxPhaseIndex
};

/// <summary>
/// 隠れる行動フェーズ
/// </summary>
UENUM(BlueprintType)
enum class EHidePhase : uint8 {
	None,             // なにもしない
	Wait,             // その場で待機
	Move,             // 移動
	SearchPosition,   // 移動先位置探索
	MaxPhaseIndex
};

/// <summary>
/// 接近行動フェーズ
/// </summary>
UENUM(BlueprintType)
enum class EApproachPhase : uint8 {
	None,          // なにもしない
	SearchRoute,   // 経路探索
	Move,          // 移動
	Return,        // 帰還
	MaxPhaseIndex
};

/// <summary>
/// 射撃処理フェーズ
/// </summary>
UENUM(BlueprintType)
enum class EShootPhase : uint8 {
	None,             // なにもしない
	InitWait,         // 初期待機時間
	StartShoot,       // 射撃を始める
	Shoot,            // 射撃処理
	StopShoot,        // 射撃を止める
	MaxPhaseIndex
};

/// <summary>
/// 探索処理フェーズ
/// </summary>
UENUM(BlueprintType)
enum class ESearchPhase : uint8 {
	None,              // なにもしない
	SearchChaseMove,   // 追跡移動の探索
	SearchHideMove,    // 隠れ先の探索
	WaitSearch,        // 探索待機中
	DecideAction,      // 行動フェーズの決定
	Finish,            // 処理終了
	MaxPhaseIndex
};

/// <summary>
/// 環境クエリの種類
/// </summary>
UENUM(BlueprintType)
enum class EEnvQueryType : uint8 {
	None,              // なにもしない
	SurroundFar,       // ターゲット周囲 遠方
	SurroundNear,      // ターゲット周囲 近辺
	Hide,              // プレイヤーから隠れる
	MaxTypeIndex
};

/// <summary>
/// 警戒中行動指示の種類
/// </summary>
UENUM(BlueprintType)
enum class EBewareActionType : uint8 {
	None,              // なにもしない
	Hide,              // 隠れる指示
	Approach,          // 接近指示
	MaxTypeIndex
};

/// <summary>
/// エネミークラス
/// </summary>
UCLASS()
class AEnemy : public ACharacterBase
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AEnemy();

	/// <summary>
	/// エネミーの種類のセット
	/// </summary>
	/// <param name="Type"> エネミーの種類 </param>
	/// <returns></returns>
	void SetEnemyType(EEnemyType Type);

	/// <summary>
	/// エネミーの種類の取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> エネミーの種類 </returns>
	EEnemyType GetEnemyType();

	/// <summary>
	/// エリアの種類のセット
	/// </summary>
	/// <param name="Type"> エリアの種類 </param>
	/// <returns></returns>
	void SetStageType(EStageType Type);

	/// <summary>
	/// エリアの種類の取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> エリアの種類 </returns>
	EStageType GetStageType();

	/// <summary>
	/// 何かに気づいたことにする
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void FoundSomething();

	/// <summary>
	/// ターゲットまでの直線距離をチェック
	/// </summary>
	/// <param name="Start"> 開始位置 </param>
	/// <param name="Goal"> 終了位置 </param>
	/// <returns> ターゲットまでの直線距離 </returns>
	float CheckTargetLinearDistance(FVector Start, FVector Goal);

	/// <summary>
	/// 警戒レベルを強制的に巡回に変更する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ChangeForciblyAlertLevelPatrol();

	/// <summary>
	/// パトロールポイント配列が空かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:空 </returns>
	UFUNCTION(BlueprintCallable, Category="Enemy")
	bool IsNullPatrolPoint();

	/// <summary>
	/// ターゲットの位置をセット
	/// </summary>
	/// <param name="Position"> ターゲットの位置 </param>
	/// <returns></returns>
	void SetTargetPosition(FVector Position);

	/// <summary>
	/// ターゲット位置を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ターゲット位置 </returns>
	UFUNCTION(BlueprintCallable, Category = "Enemy")
	FVector GetTargetPosition();

	/// <summary>
	/// パトロールポイントを配列に追加
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void AddPatrolPoint(ATargetPoint* Target);

	/// <summary>
	/// コントローラの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> コントローラ </returns>
	class AEnemyAIController* GetEnemyController();

	/// <summary>
	/// 何かに気づいたかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:気づいた </returns>
	bool IsFoundSomething();

	/// <summary>
	/// パトロール中かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:パトロール中 </returns>
	bool IsPatrol();

	/// <summary>
	/// 戦闘中かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:戦闘中 </returns>
	bool IsFight();

	/// <summary>
	/// 警戒中行動指示を設定する
	/// </summary>
	/// <param name="Type"> 警戒中行動の種類 </param>
	/// <returns></returns>
	void ChangeBewareActionType(EBewareActionType Type);

	/// <summary>
	/// 経路探索を再度実行する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetSearchingRoute();

	/// <summary>
	/// 警戒ランプで異変を感じた
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void NotifiedBeware();

	/// <summary>
	/// 現在の警戒レベルを「巡回」に変更する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OnSwitchPatrolLevel();

	/// <summary>
	/// 現在の警戒レベルを「警戒」に変更する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OnSwitchBewareLevel();

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

private:

	/// <summary>
	/// 所持弾薬の自動補給
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SupplyAmmosAuto();

	/// <summary>
	/// 巡回インデックスを加算して取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> インデックス </returns>
	int32 AddPatrolIndex();

	/// <summary>
	/// パトロールポイントの取得
	/// </summary>
	/// <param name="Index"> インデックス </param>
	/// <returns> パトロールポイント </returns>
	UFUNCTION()
	ATargetPoint* GetPatrolPoint(int32 Index);

	/// <summary>
	/// 状況に応じて行動を行う
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void AccordSituation(float DeltaTime);

	/// <summary>
	/// プレイヤー発見したかどうか
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns> true:発見した </returns>
	bool CheckFoundPlayer(float DeltaTime);

	/// <summary>
	/// プレイヤーを発見度を加算
	/// </summary>
	/// <param name="Grade"> 発見度合い </param>
	/// <returns></returns>
	void AddFoundGrade(float Grade);

	/// <summary>
	/// プレイヤー発見度のリセット処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void ResetFoundGrade(float DeltaTime);

	/// <summary>
	/// プレイヤーを発見
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void FoundPlayer();

	/// <summary>
	/// 巡回行動
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void PatrolAction(float DeltaTime);

	/// <summary>
	/// 警戒行動
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void BewareAction(float DeltaTime);

	/// <summary>
	/// 戦闘行動
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void FightAction(float DeltaTime);

	/// <summary>
	/// 追跡行動
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void Chase(float DeltaTime);

	/// <summary>
	/// 隠れる行動
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void Hide(float DeltaTime);

	/// <summary>
	/// 接近行動
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns> true:接近終了 </returns>
	bool Approach(float DeltaTime);

	/// <summary>
	/// 移動経路の取得
	/// </summary>
	/// <param name="Position"> 移動目標位置 </param>
	/// <returns> 移動経路情報 </returns>
	UFUNCTION()
	UNavigationPath* GetMoveRoute(FVector Position);

	/// <summary>
	/// 環境クエリの種類のセット
	/// </summary>
	/// <param name="Type"> 環境クエリの種類 </param>
	/// <returns></returns>
	void SetEnvQueryType(EEnvQueryType Type);

	/// <summary>
	/// 環境クエリの種類の取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 環境クエリの種類 </returns>
	EEnvQueryType GetEnvQueryType();

	/// <summary>
	/// 環境クエリの実行 (ターゲット周囲の近い場所)
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ExecQuerySurroundNearPoint();

	/// <summary>
	/// 環境クエリの実行 (ターゲット周囲の遠い場所)
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ExecQuerySurroundFarPoint();

	/// <summary>
	/// 環境クエリの実行 (プレイヤーから隠れる)
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ExecQueryHidePoint();

	/// <summary>
	/// 環境クエリの結果受け取り
	/// </summary>
	/// <param name="Result"> クエリ実行結果 </param>
	/// <returns></returns>
	void RecieveEnvQueryResult(TSharedPtr<FEnvQueryResult> Result);

	/// <summary>
	/// 移動地点までの経路探索
	/// </summary>
	/// <param name="Goal"> 移動地点 </param>
	/// <returns> true:経路探索完了 </returns>
	bool SearchRoute(FVector Goal);

	/// <summary>
	/// プレイヤーの向きを変える
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <param name="Index"> パスインデックス </param>
	/// <returns> true:処理終了 </returns>
	bool Turn(float DeltaTime, int32 Index);

	/// <summary>
	/// 移動行動
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns> true:移動完了 </returns>
	bool Move(float DeltaTime);

	/// <summary>
	/// 移動経路の終端に到達したかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:到着 </returns>
	bool IsArrivedPathPoint();

	/// <summary>
	/// パスインデックスの更新
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:目的位置到着 </returns>
	bool UpdatePathIndex();

	/// <summary>
	/// 移動先を探索する
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:探索終了 </returns>
	bool SearchDestination();

	/// <summary>
	/// 射撃処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void Shoot(float DeltaTime);

	/// <summary>
	/// プレイヤーの方へ向く(左右方向)
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void TurnPlayer();

	/// <summary>
	/// プレイヤーの方へ向く(上下方向)
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void LookupPlayer();

	/// <summary>
	/// ターゲットまでの移動総距離をチェック
	/// </summary>
	/// <param name=""></param>
	/// <returns> ターゲットまでの移動総距離 </returns>
	float CheckTargetTotalDistance();

	/// <summary>
	/// 見失うまでの補正処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns> false:見失う </returns>
	bool CorrectNotFoundPlayer(float DeltaTime);

	/// <summary>
	/// 撃たれた方向を向く
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void TurnTowardsShooted();

	/// <summary>
	/// 現在の警戒レベルを「戦闘」に変更する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SwitchFightLevel();

	/// <summary>
	/// 現在の警戒レベルを取得する
	/// </summary>
	/// <param name=""></param>
	/// <returns> レベルの種類 </returns>
	EAlertLevel GetCurrentAlertLevel();

	/// <summary>
	/// 現在の巡回フェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentPatrolPhase(EPatrolPhase PhaseType);

	/// <summary>
	/// 現在の警戒フェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentBewarePhase(EBewarePhase PhaseType);

	/// <summary>
	/// 現在の戦闘フェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentFightPhase(EFightPhase PhaseType);

	/// <summary>
	/// 現在の追跡フェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentChasePhase(EChasePhase PhaseType);

	/// <summary>
	/// 現在の隠れるフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentHidePhase(EHidePhase PhaseType);

	/// <summary>
	/// 現在の接近フェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentApproachPhase(EApproachPhase PhaseType);

	/// <summary>
	/// 現在の射撃フェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentShootPhase(EShootPhase PhaseType);

	/// <summary>
	/// 現在の探索フェーズを変更する
	/// </summary>
	/// <param name="Type"> 探索の種類 </param>
	/// <returns></returns>
	void ChangeCurrentSearchPhase(ESearchPhase Type);

	/// <summary>
	/// 撃たれた時のウィジェット処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void DisplayShootedWidget();

protected:

	/// <summary>
	/// 環境クエリ 周囲の位置取得
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "Enemy")
	UEnvQuery* SearchSurroundNearPoint;

	/// <summary>
	/// 環境クエリ 巡回位置取得
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "Enemy")
	UEnvQuery* SearchSurroundFarPoint;

	/// <summary>
	/// 環境クエリ 隠れる位置取得
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "Enemy")
	UEnvQuery* SearchHidePoint;

	/// <summary>
	/// プレイヤー発見音
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "Enemy")
	USoundCue* FoundPlayerSound;

private:

	/// <summary>
	/// エネミーの種類
	/// </summary>
	EEnemyType EnemyType;

	/// <summary>
	/// エリアの種類
	/// </summary>
	EStageType StageType;

	/// <summary>
	/// 現在の警戒レベル
	/// </summary>
	EAlertLevel CurrentAlertLevel;

	/// <summary>
	/// 現在の巡回フェーズ
	/// </summary>
	EPatrolPhase CurrentPatrolPhase;

	/// <summary>
	/// 現在の警戒フェーズ
	/// </summary>
	EBewarePhase CurrentBewarePhase;

	/// <summary>
	/// 現在の戦闘フェーズ
	/// </summary>
	EFightPhase CurrentFightPhase;

	/// <summary>
	/// 現在の追跡フェーズ
	/// </summary>
	EChasePhase CurrentChasePhase;

	/// <summary>
	/// 追跡の次の移動先フェーズ
	/// </summary>
	EChasePhase NextChasePhase;

	/// <summary>
	/// 現在の隠れるフェーズ
	/// </summary>
	EHidePhase CurrentHidePhase;

	/// <summary>
	/// 現在の接近フェーズ
	/// </summary>
	EApproachPhase CurrentApproachPhase;

	/// <summary>
	/// 現在の射撃フェーズ
	/// </summary>
	EShootPhase CurrentShootPhase;

	/// <summary>
	/// 現在の探索フェーズ
	/// </summary>
	ESearchPhase CurrentSearchPhase;

	/// <summary>
	/// 環境クエリの種類
	/// </summary>
	EEnvQueryType EnvQueryType;

	/// <summary>
	/// 警戒中行動指示の種類
	/// </summary>
	EBewareActionType BewareActionType;

	/// <summary>
	/// パトロールポイント
	/// </summary>
	UPROPERTY()
	TArray<ATargetPoint*> PatrolPoint;

	/// <summary>
	/// パトロールインデックス
	/// </summary>
	int32 PatrolIndex;

	/// <summary>
	/// 経路情報
	/// </summary>
	UPROPERTY()
	UNavigationPath* NavigationPath;

	/// <summary>
	/// 経路インデックス
	/// </summary>
	int32 PathIndex;

	/// <summary>
	/// 移動目標位置
	/// </summary>
	FVector TargetPosition;

	/// <summary>
	/// 移動目標位置
	/// </summary>
	FVector MovePosition;

	/// <summary>
	/// 何かに気づいたかどうか
	/// </summary>
	bool bFoundSomething;

	/// <summary>
	/// 1フレーム前に何かに気づいていたかどうか
	/// </summary>
	bool bOldFoundSomething;

	/// <summary>
	/// 警戒レベル切替用タイマー
	/// </summary>
	FTimerHandle LevelSwitchTimerHandle;

	/// <summary>
	/// 見失うまでの累積時間
	/// </summary>
	float NotFoundTime;

	/// <summary>
	/// 待機累積時間
	/// </summary>
	float WaitTotalTime;

	/// <summary>
	/// 隠れる待機時間
	/// </summary>
	float HideWaitTime;

	/// <summary>
	/// 射撃累積時間
	/// </summary>
	float ShootTime;

	/// <summary>
	/// 射撃時間
	/// </summary>
	float StopShootTime;

	/// <summary>
	/// 発見度リセット用累積時間
	/// </summary>
	float FoundGradeResetTime;

	/// <summary>
	/// プレイヤー発見度
	/// </summary>
	float FoundGrade;

	/// <summary>
	/// 探索終了
	/// </summary>
	bool bFinishSearch;

	/// <summary>
	/// 経路再探索
	/// </summary>
	bool bResetSearchRoute;


private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// パスインデックス初期値
	/// </summary>
	const int32 MinIndex = 0;

	/// <summary>
	/// HP デフォルト値
	/// </summary>
	const int32 DefaultHP = 200;

	/// <summary>
	/// 1フレームで振り向く量
	/// </summary>
	const float TurnValue = 2.0f;

	/// <summary>
	/// 時間リセット
	/// </summary>
	const float ResetTime = 0.0f;

	/// <summary>
	/// 見失うまでの時間
	/// </summary>
	const float NotFoundTimeValue = 0.75f;

	/// <summary>
	/// パトロール時の待機時間
	/// </summary>
	const float PatrolWaitTime = 5.0f;

	/// <summary>
	/// 追いかけている時の待機時間
	/// </summary>
	const float ChaseWaitTime = 1.5f;

	/// <summary>
	/// 射撃までの待機時間 通常
	/// </summary>
	const float WaitNormalShootTimeValue = 0.5f;

	/// <summary>
	/// 射撃までの待機時間 スナイパー
	/// </summary>
	const float WaitSniperShootTimeValue = 3.0f;

	/// <summary>
	/// 到達許容誤差
	/// </summary>
	const float ArrivedAllowableError = 50.0f;

	/// <summary>
	/// ターン許容誤差
	/// </summary>
	const float TurnedAllowableError = 3.0f;

	/// <summary>
	/// 脚のソケット名
	/// </summary>
	const FName FootSocketName = "foot_r";

	/// <summary>
	/// 0
	/// </summary>
	const int32 Zero = 0;

	/// <summary>
	/// 1
	/// </summary>
	const int32 One = 1;

	/// <summary>
	/// パスインデックス初動
	/// </summary>
	const int32 StartIndex = 1;

	/// <summary>
	/// 環境クエリ アイテム生成半径
	/// </summary>
	const float CreateItemRadius = 750.0f;

	/// <summary>
	/// 環境クエリ アイテム生成間隔
	/// </summary>
	const float CreateItemSpace = 100.0f;

	/// <summary>
	/// スコア無し
	/// </summary>
	const float ZeroScore = 0.0f;

	/// <summary>
	/// 射撃時間 単発
	/// </summary>
	const float SingleShootTime = 0.0f;

	/// <summary>
	/// 射撃時間 上限
	/// </summary>
	const float ShootMaxTime = 0.6f;

	/// <summary>
	/// 射撃時間 下限
	/// </summary>
	const float ShootMinTime = 0.1f;

	/// <summary>
	/// プレイヤーを見つける距離の基準値
	/// </summary>
	const float FoundPlayerDistance = 1000.0f;

	/// <summary>
	/// プレイヤーを見つけた
	/// </summary>
	const float FoundPlayerValue = 100.0f;

	/// <summary>
	/// プレイヤー発見度リセットまでの時間
	/// </summary>
	const float ResetTimeFoundGrade = 5.0f;

	/// <summary>
	/// プレイヤー発見度リセット値
	/// </summary>
	const float ResetFoundGradeValue = 0.0f;

	/// <summary>
	/// 侵入者がいると疑う閾値
	/// </summary>
	const float FoundSomethingAmount = 2.5f;

	/// <summary>
	/// プレイヤーだと認識する閾値
	/// </summary>
	const float FoundPlayerAmount = 5.0f;

	/// <summary>
	/// 戦闘解除までの時間
	/// </summary>
	const float CancelFightTime = 15.0f;

	/// <summary>
	/// 警戒解除までの時間
	/// </summary>
	const float CancelBewareTime = 10.0f;

	/// <summary>
	/// 隠れている最短時間
	/// </summary>
	const float HideMinTime = 10.0f;

	/// <summary>
	/// 隠れている最長時間
	/// </summary>
	const float HideMaxTime = 15.0f;

	/// <summary>
	/// パトロールするかどうか境界距離
	/// </summary>
	const float PatrolDistance = 2000.0f;

	/// <summary>
	/// 走るかどうか境界距離
	/// </summary>
	const float FarDistance = 2000.0f;

	/// <summary>
	/// パトロール中接近するかどうかの距離
	/// </summary>
	const float ApproachDistance = 4000.0f;

	/// <summary>
	/// プレイヤーとの直線距離の許容範囲
	/// </summary>
	const float DistanceAllowableError = 3000.0f;

	/// <summary>
	/// プレイヤーとの高さ位置の差の許容範囲
	/// </summary>
	const float HeightAllowableError = 35.0f;

	/// <summary>
	/// 0度
	/// </summary>
	const float Angle0 = 0.0f;

	/// <summary>
	/// 射撃時の調整角度
	/// </summary>
	const float ShootAdjustAngle = 1.0f;
};