﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BridgeCrane.generated.h"


/// <summary>
/// ブリッジクレーンのフェーズの種類
/// </summary>
UENUM(BlueprintType)
enum class EBridgeCranePhase : uint8 {
	None,
	BeforeSetup,
	Initialize,
	Move,
	Stop,
	MaxPhaseIndex
};

/// <summary>
/// ブリッジクレーンの移動フェーズの種類
/// </summary>
UENUM(BlueprintType)
enum class EBridgeCraneMovePhase : uint8 {
	None,
	Forward,
	Back,
	MaxPhaseIndex
};

/// <summary>
/// ブリッジクレーンクラス
/// </summary>
UCLASS()
class ABridgeCrane : public AActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ABridgeCrane();

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

private:

	/// <summary>
	/// 移動処理
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:処理完了 </returns>
	bool Move();

	/// <summary>
	/// 目標地点に到達したかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:到達 </returns>
	bool IsArrivedGoal();

	/// <summary>
	/// 待機時間をランダムに設定し取得する
	/// </summary>
	/// <param name=""></param>
	/// <returns> 待機時間 </returns>
	float GetRandamWaitTime();

	/// <summary>
	/// 現在のフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> 処理時間 </param>
	/// <returns></returns>
	void ChangeCurrentPhase(EBridgeCranePhase PhaseType);

	/// <summary>
	/// 現在の移動フェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> 処理時間 </param>
	/// <returns></returns>
	void ChangeCurrentMovePhase(EBridgeCraneMovePhase PhaseType);

public:

	/// <summary>
	/// 移動開始位置
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "BridgeCrane")
	FVector MoveStartPosition;

	/// <summary>
	/// 移動終了位置
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "BridgeCrane")
	FVector MoveEndPosition;

	/// <summary>
	/// 初期設定フラグ
	/// </summary>
	UPROPERTY(BlueprintReadWrite, Category = "BridgeCrane")
	bool bSetupFinish;

private:

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	EBridgeCranePhase CurrentPhase;

	/// <summary>
	/// 現在の移動フェーズ
	/// </summary>
	EBridgeCraneMovePhase CurrentMovePhase;

	/// <summary>
	/// 移動量
	/// </summary>
	FVector MoveVector;

	/// <summary>
	/// 総移動距離
	/// </summary>
	FVector MoveDistance;

	/// <summary>
	/// 待機時間
	/// </summary>
	float WaitTime;

	/// <summary>
	/// 累積時間
	/// </summary>
	float AccumulateTime;


private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// 待機時間範囲 最小値
	/// </summary>
	const float MinWaitTime = 5.0f;

	/// <summary>
	/// 待機時間範囲 最大値
	/// </summary>
	const float MaxWaitTime = 20.0f;

	/// <summary>
	/// リセット時間
	/// </summary>
	const float ResetTime = 0.0f;

	/// <summary>
	/// 初期設定前のZ方向移動量
	/// </summary>
	const float InitMoveValue = 2.0f;

	/// <summary>
	/// 設定前移動処理時間
	/// </summary>
	const float BeforeSetupTime = 1.0f;

	/// <summary>
	/// 移動処理時間
	/// </summary>
	const float ArrivalMoveTime = 7.5f;
};
