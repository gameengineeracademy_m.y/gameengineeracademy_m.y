﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Battle/Widgets/CenterPointWidget.h"
#include "Battle/Widgets/LowHealthWidget.h"
#include "Battle/Weapons/WeaponBase.h"
#include "BattleEvent.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UBattleEvent : public UInterface
{
	GENERATED_BODY()
};

/// <summary>
/// バトルレベルイベントインターフェース
/// </summary>
class IBattleEvent
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// ポーズ画面の表示・非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSwitchPause() = 0;

	/// <summary>
	/// マップ画面の表示・非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSwitchMap() = 0;

	/// <summary>
	/// 敵の視覚情報ON・OFF
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSetupSight() = 0;

	/// <summary>
	/// 武器の生成処理
	/// </summary>
	/// <param name="WeaponType"> 武器の種類 </param>
	/// <returns> 武器 </returns>
	virtual AWeaponBase* OnCreateWeapon(EWeaponType WeaponType) = 0;

	/// <summary>
	/// セーブデータに指定の武器があるかどうか
	/// </summary>
	/// <param name="WeaponType"> 武器の種類 </param>
	/// <returns> true:ある </returns>
	virtual bool OnIsSavePossessWeapon(EWeaponType WeaponType) = 0;

	/// <summary>
	/// HUDの武器入れ替えアニメーション開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnStartSwitchWeaponAnimation() = 0;

	/// <summary>
	/// HUDの武器入れ替え処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSwitchWeapon() = 0;

	/// <summary>
	/// バトルHUD情報の武器の更新
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnUpdateBattleHUDWeapon() = 0;

	/// <summary>
	/// バトルHUD情報の弾薬数の更新
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnUpdateBattleHUDWeaponAmmos() = 0;

	/// <summary>
	/// 太陽と雲の設定を変更する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnChangeSettingSunAndCloud() = 0;

	/// <summary>
	/// カメラ演出時のカメラ切り替え
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSwitchDirectionCamera() = 0;

	/// <summary>
	/// カメラ演出実行終了処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnFinishCameraDirection() = 0;

	/// <summary>
	/// 演出スキップ処理
	/// </summary>
	/// <param name="Value"> 押したかどうか </param>
	/// <returns></returns>
	virtual void OnSkipDirection(float Value) = 0;
};
