﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/OperateBaseWidget/LevelUIBaseWidget/LevelUIBaseWidget.h"
#include "Engine/Texture2DDynamic.h"
#include "Components/HorizontalBox.h"
#include "ResultWidget.generated.h"

/// <summary>
/// リザルトウィジェット
/// </summary>
UCLASS()
class UResultWidget : public ULevelUIBaseWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UResultWidget();

	/// <summary>
	/// ウィジェットの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupWidget() override;

	/// <summary>
	/// リザルトウィジェット初期化
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeResultWidget();

	/// <summary>
	/// アニメーション初期化
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeAnimation();

	/// <summary>
	/// テクスチャのセット
	/// </summary>
	/// <param name="Texture"> テクスチャ </param>
	/// <returns></returns>
	void SetupTexture(UTexture2DDynamic* Texture);

	/// <summary>
	/// アニメーションの開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ResultWidget")
	void StartAnimation();

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

private:

	/// <summary>
	/// ホリゾンタルボックスの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ホリゾンタルボックス </returns>
	UHorizontalBox* GetHorizontalBox();

	/// <summary>
	/// アニメーションの開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void StartAnimation_Implementation();

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// 背景 ゲームオーバー用
	/// </summary>
	const FName ImageGameOverName = "Image_GameOverBack";

	/// <summary>
	/// 背景 ゲームクリア用
	/// </summary>
	const FName ImageGameClearName = "Image_GameClearBack";

	/// <summary>
	/// 背景 リザルト
	/// </summary>
	const FName ImageResultName = "Image_Result";

	/// <summary>
	/// タイトル用のパネル
	/// </summary>
	const FName CanvasPanelTitleName = "CanvasPanel_TitleText";
	
	/// <summary>
	/// 案内文用のパネル
	/// </summary>
	const FName CanvasPanelGuideName = "CanvasPanel_GuideText";

	/// <summary>
	/// ホリゾンタルボックス
	/// </summary>
	const FName HorizontalBoxName = "HorizontalBox_Button";
};