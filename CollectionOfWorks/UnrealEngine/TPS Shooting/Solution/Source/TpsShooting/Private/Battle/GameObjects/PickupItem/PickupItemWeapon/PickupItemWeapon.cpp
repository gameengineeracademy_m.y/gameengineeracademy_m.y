﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/GameObjects/PickupItem/PickupItemWeapon/PickupItemWeapon.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
APickupItemWeapon::APickupItemWeapon()
	: PickupWeaponType(EPickupWeaponType::None) {

	// Tickイベントを有効化
	PrimaryActorTick.bCanEverTick = true;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APickupItemWeapon::BeginPlay() {

	Super::BeginPlay();

	PickupItemType = EPickupItemType::Weapon;
	PickupType = EPickupType::OnlyOnce;

	UE_LOG(LogTemp, Display, TEXT("PickupItemWeapon : BeginPlay"))

	CurrentFadeOutPhase = EFadeOutPhase::None;
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void APickupItemWeapon::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

	//------------------------------------------
	//パーティクルのフェードアウト処理
	//※Tick処理はフェード処理時のみ実行可能にする
	//------------------------------------------
	switch (CurrentFadeOutPhase) {
	case EFadeOutPhase::None: {
		break;
	}
	case EFadeOutPhase::Start: {

		ChangeCurrentFadeOutPhase(EFadeOutPhase::PreProcess);
		break;
	}
	case EFadeOutPhase::PreProcess: {

		// 透過率を最大値に設定しておく
		RenderValue = RenderMaxValue;
		// ポーンとのコリジョン設定を「無視」に変更
		CapsuleComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);

		ChangeCurrentFadeOutPhase(EFadeOutPhase::Process);
		break;
	}
	case EFadeOutPhase::Process: {

		RenderValue -= RenderAdjustValue;

		ParticleSystemComp->SetFloatParameter(RenderParamName, RenderValue);
		if (RenderValue > RenderMinValue) {
			break;
		}

		ChangeCurrentFadeOutPhase(EFadeOutPhase::PostProcess);
		break;
	}
	case EFadeOutPhase::PostProcess: {

		// Tickイベントを無効化
		DeactiveParticleSystem();
		SetActorTickEnabled(false);
		ChangeCurrentFadeOutPhase(EFadeOutPhase::Finish);
		break;
	}
	case EFadeOutPhase::Finish: {
		
		break;
	}
	}
}

/// <summary>
/// 武器の種類をセット
/// </summary>
/// <param name="WeaponType"> 武器の種類 </param>
/// <returns></returns>
void APickupItemWeapon::SetPickupWeaponType(EPickupWeaponType WeaponType) {

	PickupWeaponType = WeaponType;
}

/// <summary>
/// 武器の種類を取得
/// </summary>
/// <param name=""></param>
/// <returns> 武器の種類 </returns>
EPickupWeaponType APickupItemWeapon::GetPickupWeaponType() {

	return PickupWeaponType;
}

/// <summary>
/// フェードアウト処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APickupItemWeapon::StartFadeOut() {

	// Tickイベントを有効化
	SetActorTickEnabled(true);
	ChangeCurrentFadeOutPhase(EFadeOutPhase::Start);
}

/// <summary>
/// 現在のフェードアウトフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void APickupItemWeapon::ChangeCurrentFadeOutPhase(EFadeOutPhase PhaseType) {

	CurrentFadeOutPhase = PhaseType;
}