﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/CharacterManager/EnemyManager/EnemyManager.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Battle/GameState/BattleGameState.h"
#include "Battle/Controller/EnemyAIController.h"
#include "Battle/Controller/BattleController.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AEnemyManager::AEnemyManager()
	: CurrentFoundPlayerPhase(EFoundPlayerPhaseType::None)
	, EnemyClass()
	, EnemyList()
	, FightEnemyIDList()
	, ApproachEnemyIDList()
	, EnemySpawnTable(nullptr)
	, EnemyDetail(nullptr)
	, WeaponFactory(nullptr)
	, WarningLampManager(nullptr)
	, EnemyIndexArray()
	, AccumulateTime(0.0f)
	, NewTargetPosition()
	, AlertLevelList()
	, bIsFoundPlayer(false) {

	// Tick処理を有効化
	PrimaryActorTick.bCanEverTick = true;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemyManager::BeginPlay() {

	Super::BeginPlay();

	// 変数の初期化
	CurrentFoundPlayerPhase = EFoundPlayerPhaseType::None;
	bIsFoundPlayer = false;

	// エネミークラスのロード ソフト参照
	FString OperatePlayerPath;
	GConfig->GetString(TEXT("CharacterSettings"), TEXT("EnemyPlayerPath"), OperatePlayerPath, GGameIni);

	EnemyClass = TSoftClassPtr<AEnemy>(FSoftObjectPath(*OperatePlayerPath)).LoadSynchronous();
	if (!EnemyClass) {
		UE_LOG(LogTemp, Display, TEXT("AEnemyManager EnemyClass Error"));
		return;
	}
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AEnemyManager::Tick(float DeltaTime) {

	//-----------------------------------------
	//各エネミーのTick処理を実行する
	//エネミーの状態を確認し、死亡していたら
	//対象のエネミーをリストから削除する
	//-----------------------------------------

	Super::Tick(DeltaTime);

	// 共通処理
	ProcessCommon(DeltaTime);

	// 戦闘中処理
	ProcessFoundPlayer(DeltaTime);
}

/// <summary>
/// 共通処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AEnemyManager::ProcessCommon(float DeltaTime) {

	// 現在プレイヤーが進行中のエリアを取得する
	ABattleGameState* BattleGameState = Cast<ABattleGameState>(GetWorld()->GetGameState());
	if (!IsValid(BattleGameState)) {
		return;
	}
	EStageType StageType = BattleGameState->GetCurrentStageType();

	TArray<int32> DeadEnemyList;
	bool bIsPatrol = true;
	// エネミーの状態を確認しつつ、Tick処理実施
	for (auto Enemy : EnemyList) {

		if (!IsValid(Enemy.Value)) {
			continue;
		}

		bool IsDisappearance = Enemy.Value->IsDisappearance();
		if (IsDisappearance) {
			DeadEnemyList.Add(Enemy.Key);
			continue;
		}

		// 表示待機中は処理を行わない
		bool IsWait = Enemy.Value->IsWaitAppearance();
		if (IsWait) {
			continue;
		}

		// 視界にプレイヤーを捉えている場合、最新の目標位置をセット
		// ※エネミーのTick処理内で毎回発見フラグをOFFにしているためここで処理実行
		if (Enemy.Value->IsFight() && Enemy.Value->IsFoundSomething()) {
			SetNewTargetPosition(Enemy.Value->GetTargetPosition());
		}

		//---------------------------
		// エネミーの毎フレーム処理
		//---------------------------
		Enemy.Value->Tick(DeltaTime);

		//---------------------------
		// 現在エリアのエネミー処理
		//---------------------------
		EStageType ExistsStageType = Enemy.Value->GetStageType();
		if (ExistsStageType != StageType) {
			continue;
		}

		if (!Enemy.Value->IsFine()) {
			continue;
		}

		if (!Enemy.Value->IsPatrol()) {
			bIsPatrol = false;
		}

		// エネミーが戦闘中以外なら次の処理へ
		if (!Enemy.Value->IsFight()) {
			continue;
		}

		// 戦闘中リストに登録されている場合、次の処理へ
		if (FightEnemyIDList.Contains(Enemy.Key)) {
			continue;
		}
		FightEnemyIDList.Add(Enemy.Key);

		// 現在のプレイヤー発見フェーズに応じて遷移フェーズを変更
		switch (CurrentFoundPlayerPhase) {
		case EFoundPlayerPhaseType::None: {
			AccumulateTime = ResetTime;
			ChangeCurrentFoundPlayerPhase(EFoundPlayerPhaseType::WaitNotifyBeware);
			break;
		}
		case EFoundPlayerPhaseType::WaitBeware: {
			ChangeCurrentFoundPlayerPhase(EFoundPlayerPhaseType::WatchFight);
			break;
		}
		default: {

			break;
		}
		}
	}

	//----------------------------------------------
	// エネミーの破棄処理
	// 
	//   死亡状態のエネミーと装備している武器を破壊、
	//   その後リストから対象エネミーを削除する
	//----------------------------------------------
	for (int32 Index : DeadEnemyList) {

		if (!EnemyList.Contains(Index)) {
			continue;
		}

		if (IsValid(EnemyList[Index])) {
			EnemyList[Index]->Destroy();
		}
		if (IsValid(EnemyList[Index]->GetEquipWeapon())) {
			EnemyList[Index]->GetEquipWeapon()->Destroy();
		}
		EnemyList[Index] = nullptr;
		EnemyList.Remove(Index);
	}

	//----------------------------------------------
	// 発見マーカー表示設定
	// 
	//   パトロール中ではないエネミーが存在する場合、
	//   発見マーカーをミニマップのフレームに表示する
	//   そうでない場合は発見マーカーを非表示にする
	//----------------------------------------------
	if (!bIsPatrol) {

		EAlertLevel CurrentAlertLevel = GetAlertLevel(StageType);
		switch (CurrentAlertLevel) {
		case EAlertLevel::Patrol: {

			// エネミーがひとりでも戦闘中なら
			// エリア警戒レベルも戦闘中にしておく
			bIsFoundPlayer = true;
			SetAlertLevel(StageType, EAlertLevel::Fight);
			// ミニマップのフレームに情報伝達アニメーションを再生する
			SetupFoundPlayerMarkaer(true);
			break;
		}
		}
	}
	else {

		EAlertLevel CurrentAlertLevel = GetAlertLevel(StageType);
		switch (CurrentAlertLevel) {
		case EAlertLevel::Beware:
		case EAlertLevel::Fight: {

			SetAlertLevel(StageType, EAlertLevel::Patrol);
			// ミニマップのフレームに情報伝達アニメーションを停止する
			SetupFoundPlayerMarkaer(false);

			AccumulateTime = ResetTime;
			ChangeCurrentFoundPlayerPhase(EFoundPlayerPhaseType::NotifyPatrol);
			break;
		}
		case EAlertLevel::Patrol: {

			if (!bIsFoundPlayer) {
				break;
			}
			bIsFoundPlayer = false;
			AccumulateTime = ResetTime;
			ChangeCurrentFoundPlayerPhase(EFoundPlayerPhaseType::None);

			// ミニマップのフレームに情報伝達アニメーションを停止する
			SetupFoundPlayerMarkaer(false);
			break;
		}
		}
	}
}

/// <summary>
/// プレイヤー発見後処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AEnemyManager::ProcessFoundPlayer(float DeltaTime) {

	// 現在プレイヤーが進行中のエリアを取得する
	ABattleGameState* BattleGameState = Cast<ABattleGameState>(GetWorld()->GetGameState());
	if (!IsValid(BattleGameState)) {
		return;
	}
	EStageType StageType = BattleGameState->GetCurrentStageType();

	switch (CurrentFoundPlayerPhase) {
	case EFoundPlayerPhaseType::WaitNotifyBeware: {
		//-----------------------------------
		//警戒通知待機フェーズ
		//-----------------------------------
		AccumulateTime += DeltaTime;
		if (AccumulateTime < NotifyBewareWaitTime) {
			break;
		}
		AccumulateTime = ResetTime;

		ABattleController* BattleController = Cast<ABattleController>(UGameplayStatics::GetPlayerController(this, 0));
		if (IsValid(BattleController)) {

			AAudioManager* AudioManager = BattleController->GetAudioManager();
			if (IsValid(AudioManager)) {
				AudioManager->FadeOutBGM();
				AudioManager->SetNextBGMSound(EBGMType::Battle);
			}
		}

		// 警戒通知フェーズに変更
		ChangeCurrentFoundPlayerPhase(EFoundPlayerPhaseType::NotifyBeware);
		break;
	}
	case EFoundPlayerPhaseType::NotifyBeware: {
		//-----------------------------------
		//警戒通知フェーズ
		//-----------------------------------

		// 警戒ランプを作動させる
		if (IsValid(WarningLampManager)) {
			WarningLampManager->LightLamp(StageType);
		}

		// 敵が異変を察知する
		for (auto Enemy : EnemyList) {

			if (!IsValid(Enemy.Value)) {
				continue;
			}
			// 現在のエリア以外のエネミーなら次の処理へ
			if (Enemy.Value->GetStageType() != StageType) {
				continue;
			}
			// 死亡しているなら次の処理へ
			if (!Enemy.Value->IsFine()) {
				continue;
			}
			// 巡回中以外なら次の処理へ
			if (!Enemy.Value->IsPatrol()) {
				continue;
			}
			Enemy.Value->NotifiedBeware();
		}

		// 通知待機フェーズに変更
		ChangeCurrentFoundPlayerPhase(EFoundPlayerPhaseType::WaitNotify);
		break;
	}
	case EFoundPlayerPhaseType::WaitNotify: {
		//-----------------------------------
		//通知待機フェーズ
		//-----------------------------------

		AccumulateTime += DeltaTime;
		if (AccumulateTime < NotifyInfomationWaitTime) {
			break;
		}
		AccumulateTime = ResetTime;

		// 警戒行動フェーズに変更
		ChangeCurrentFoundPlayerPhase(EFoundPlayerPhaseType::TransBeware);
		break;
	}
	case EFoundPlayerPhaseType::TransBeware: {
		//-----------------------------------
		//警戒状態移行フェーズ
		//-----------------------------------
		
		for (auto Enemy : EnemyList) {

			if (!IsValid(Enemy.Value)) {
				continue;
			}

			// 現在のエリア以外のエネミーなら次の処理へ
			if (Enemy.Value->GetStageType() != StageType) {
				continue;
			}
			// 死亡しているなら次の処理へ
			if (!Enemy.Value->IsFine()) {
				continue;
			}

			// 巡回中以外なら次の処理へ
			if (!Enemy.Value->IsPatrol()) {
				continue;
			}
			Enemy.Value->OnSwitchBewareLevel();
			Enemy.Value->SetTargetPosition(GetNewTargetPosition());
			Enemy.Value->ResetSearchingRoute();
		}

		// 戦闘監視フェーズに変更
		ChangeCurrentFoundPlayerPhase(EFoundPlayerPhaseType::WatchFight);
		break;
	}
	case EFoundPlayerPhaseType::WatchFight: {
		//-----------------------------------
		//戦闘監視フェーズ
		//-----------------------------------

		// 戦闘用・警戒接近リストの整理
		OrganizeFightAndApproachEnemyList();

		// 戦闘中のエネミーがいないなら巡回指令伝達までの待機フェーズに遷移
		if (FightEnemyIDList.Num() == Zero) {
			AccumulateTime = ResetTime;
			ChangeCurrentFoundPlayerPhase(EFoundPlayerPhaseType::WaitBeware);
			break;
		}

		// 戦闘中のエネミーが2人以上いる場合は処理終了
		if (FightEnemyIDList.Num() >= FightEnemyMinCount) {
			break;
		}
		// 警戒接近中のエネミーが2人以上いるなら処理終了
		if (ApproachEnemyIDList.Num() >= ApproachEnemyMinCount) {
			break;
		}

		//--------------------------------------------
		// 目標位置との直線距離が一番近いエネミーを
		// 目標位置に接近させる
		//--------------------------------------------
		TMap<int32, float> DistanceList;
		for (auto Enemy : EnemyList) {

			if (!IsValid(Enemy.Value)) {
				continue;
			}
			// 現在のエリア以外のエネミーなら次の処理へ
			if (Enemy.Value->GetStageType() != StageType) {
				continue;
			}
			// 生存していないエネミーなら次の処理へ
			if (!Enemy.Value->IsFine()) {
				continue;
			}

			// プレイヤーに接近できない種類なら次の処理へ
			if (Enemy.Value->GetEnemyType() != EEnemyType::Normal) {
				continue;
			}
			// 既に戦闘中のエネミーなら次の処理へ
			if (Enemy.Value->IsFight()) {
				continue;
			}
			// 既に警戒接近中リスト登録済みなら次の処理へ
			if (ApproachEnemyIDList.Contains(Enemy.Key)) {
				continue;
			}

			float DistanceToAim = Enemy.Value->CheckTargetLinearDistance(Enemy.Value->GetActorLocation(), Enemy.Value ->GetTargetPosition());
			DistanceList.Add(Enemy.Key, DistanceToAim);
		}

		//-------------------------------------------------------
		// 目標位置に近い順にソートし、警戒接近リストに登録する
		//-------------------------------------------------------
		if (DistanceList.Num() > Zero) {

			DistanceList.ValueSort([](const float& A, const float& B) { return A < B; });

			for (auto Distance : DistanceList) {

				AEnemy* Enemy = GetEnemy(Distance.Key);
				if (!IsValid(Enemy)) {
					continue;
				}
				Enemy->ChangeBewareActionType(EBewareActionType::Approach);
				Enemy->SetTargetPosition(GetNewTargetPosition());
				ApproachEnemyIDList.Add(Distance.Key);

				// 登録数が2人以上いるなら処理終了
				if (ApproachEnemyIDList.Num() >= ApproachEnemyMinCount) {
					break;
				}
			}
		}

		break;
	}
	case EFoundPlayerPhaseType::WaitBeware: {
		//-----------------------------------
		//警戒待機フェーズ
		//-----------------------------------
		AccumulateTime += DeltaTime;
		if (AccumulateTime < NotifyPatrolWaitTime) {
			break;
		}
		AccumulateTime = ResetTime;

		// 巡回通知フェーズに変更
		ChangeCurrentFoundPlayerPhase(EFoundPlayerPhaseType::NotifyPatrol);
		break;
	}
	case EFoundPlayerPhaseType::NotifyPatrol: {
		//-----------------------------------
		//巡回通知フェーズ
		//-----------------------------------

		ABattleController* BattleController = Cast<ABattleController>(UGameplayStatics::GetPlayerController(this, 0));
		if (IsValid(BattleController)) {

			AAudioManager* AudioManager = BattleController->GetAudioManager();
			if (IsValid(AudioManager)) {
				if (AudioManager->IsPlayingBattleBGM()) {

					AudioManager->FadeOutBGM();
					AudioManager->SetNextBGMSound(EBGMType::Game);
				}
			}
		}

		// 警戒ランプを停止させる
		if (IsValid(WarningLampManager)) {
			WarningLampManager->TurnOffLamp();
		}

		for (auto Enemy : EnemyList) {
			if (!IsValid(Enemy.Value)) {
				continue;
			}
			Enemy.Value->OnSwitchPatrolLevel();
		}

		// 戦闘用リスト、接近用リストからエネミーの
		// アドレスを削除しリストを空にしておく
		FightEnemyIDList.Empty();
		ApproachEnemyIDList.Empty();

		// 何もしないフェーズに変更
		ChangeCurrentFoundPlayerPhase(EFoundPlayerPhaseType::None);
		break;
	}
	case EFoundPlayerPhaseType::ForciblyPatrol: {
		//-----------------------------------
		//強制巡回遷移フェーズ
		//-----------------------------------
		ABattleController* BattleController = Cast<ABattleController>(UGameplayStatics::GetPlayerController(this, 0));
		if (IsValid(BattleController)) {

			AAudioManager* AudioManager = BattleController->GetAudioManager();
			if (IsValid(AudioManager)) {
				if (AudioManager->IsPlayingBattleBGM()) {

					AudioManager->FadeOutBGM();
					AudioManager->SetNextBGMSound(EBGMType::Game);
				}
			}
		}

		for (auto Enemy : EnemyList) {
			if (!IsValid(Enemy.Value)) {
				continue;
			}
			Enemy.Value->ChangeForciblyAlertLevelPatrol();
		}
		// 戦闘用リスト、接近用リストからエネミーの
		// アドレスを削除しリストを空にしておく
		FightEnemyIDList.Empty();
		ApproachEnemyIDList.Empty();

		// 何もしないフェーズに変更
		ChangeCurrentFoundPlayerPhase(EFoundPlayerPhaseType::None);
		break;
	}
	default: {

		break;
	}
	}
}

/// <summary>
/// 戦闘用・警戒接近リストの整理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemyManager::OrganizeFightAndApproachEnemyList() {

	TArray<int32> DeleteNumList;
	// 戦闘用リストのエネミーの状態を確認し、
	// リストを整理しておく
	for (int32 EnemyKey : FightEnemyIDList) {
		
		AEnemy* Enemy = GetEnemy(EnemyKey);

		// 生存していないなら削除リストに登録
		if (!IsValid(Enemy)) {
			DeleteNumList.Add(EnemyKey);
			continue;
		}
		if (!Enemy->IsFine()) {
			DeleteNumList.Add(EnemyKey);
			continue;
		}
		// 戦闘中以外なら削除リストに登録
		if (!Enemy->IsFight()) {
			DeleteNumList.Add(EnemyKey);
			continue;
		}

		// 最新の目標位置を渡しておく
		Enemy->SetTargetPosition(GetNewTargetPosition());
		Enemy->ResetSearchingRoute();
	}
	for (int32 EnemyKey : DeleteNumList) {
		FightEnemyIDList.Remove(EnemyKey);
	}

	// 接近用リストのエネミーの状態を確認し、
	// リストを整理しておく
	for (int32 EnemyKey : ApproachEnemyIDList) {
		
		AEnemy* Enemy = GetEnemy(EnemyKey);
		
		// 生存していないなら削除リストに登録
		if (!IsValid(Enemy)) {
			DeleteNumList.Add(EnemyKey);
			continue;
		}
		if (!Enemy->IsFine()) {
			DeleteNumList.Add(EnemyKey);
			continue;
		}
		// 戦闘中になっていたら削除リストに登録
		if (Enemy->IsFight()) {
			DeleteNumList.Add(EnemyKey);
			continue;
		}

		// 最新の目標位置を渡しておく
		Enemy->SetTargetPosition(GetNewTargetPosition());
		Enemy->ResetSearchingRoute();
	}
	for (int32 EnemyKey : DeleteNumList) {
		ApproachEnemyIDList.Remove(EnemyKey);
	}
}

/// <summary>
/// プレイヤー発見マーカーを表示設定を行う
/// </summary>
/// <param name="bDisplay"> 表示有無 </param>
/// <returns></returns>
void AEnemyManager::SetupFoundPlayerMarkaer(bool bDisplay) {

	ABattleController* BattleController = Cast<ABattleController>(UGameplayStatics::GetPlayerController(this, 0));
	if (!IsValid(BattleController)) {
		return;
	}
	UMiniMapWidget* Widget = BattleController->GetMiniMapWidget();
	if (!IsValid(Widget)) {
		return;
	}

	if (bDisplay) {
		// プレイヤー発見マーカーを表示
		Widget->PlayFoundMarkerAnimation();
	}
	else {
		// プレイヤー発見マーカーを非表示
		Widget->StopFoundMarkerAnimation();
	}	
}

/// <summary>
/// 指定エリアの現在の警戒レベルをセット
/// </summary>
/// <param name="StageType"> エリアの種類 </param>
/// <param name="AlertLevel"> 警戒レベル </param>
/// <returns></returns>
void AEnemyManager::SetAlertLevel(EStageType StageType, EAlertLevel AlertLevel) {

	if (!AlertLevelList.Contains(StageType)) {
		return;
	}

	AlertLevelList[StageType] = AlertLevel;
}

/// <summary>
/// 指定エリアの現在の警戒レベルを取得
/// </summary>
/// <param name="StageType"> エリアの種類 </param>
/// <returns> 警戒レベル </returns>
EAlertLevel AEnemyManager::GetAlertLevel(EStageType StageType) {

	if (!AlertLevelList.Contains(StageType)) {
		return EAlertLevel::None;
	}

	return AlertLevelList[StageType];
}

/// <summary>
/// ウェポンファクトリーのセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemyManager::SetWeaponFactory(AWeaponFactory* Factory) {

	WeaponFactory = Factory;
}

/// <summary>
/// 警告ランプマネージャーのセット
/// </summary>
/// <param name="Manager"> 警告ランプマネージャー </param>
/// <returns></returns>
void AEnemyManager::SetWarningLampManager(AWarningLampManager* Manager) {

	WarningLampManager = Manager;
}

/// <summary>
/// エネミーの生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemyManager::CreateEnemies() {

	//--------------------------------------------
	//エネミーをスポーンし、ディテール情報を設定
	//エネミー生成後、武器の生成を実施
	//--------------------------------------------

	//---------------------------------------
	// 現在のレベルを取得
	//---------------------------------------
	UTpsShootingGameInstance* GameInstance = Cast<UTpsShootingGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (!IsValid(GameInstance)) {
		return;
	}
	ELevelType CurrentLevelType = GameInstance->GetCurrentLevel();

	//---------------------------------------
	// データテーブルから
	// スポーン位置とエネミーの向き取得
	//---------------------------------------
	int32 EnemyIndex = InitIndex;

	if (!EnemyClass) {
		UE_LOG(LogTemp, Display, TEXT("AEnemyManager EnemyClass Error"));
		return;
	}

	if (!IsValid(EnemySpawnTable)) {
		return;
	}

	TArray<FName> RowNames = EnemySpawnTable->GetRowNames();
	for (FName RowName : RowNames) {

		FCharacterSpawn* CharaSpawn = EnemySpawnTable->FindRow<FCharacterSpawn>(RowName, "");
		if (CharaSpawn->BattleLevelType != CurrentLevelType) {
			continue;
		}
		FSpawnData SpawnData;
		SpawnData.Position = FVector::ZeroVector;
		SpawnData.Rotation = FRotator::ZeroRotator;

		SpawnData.Position = FVector(CharaSpawn->PositionX, CharaSpawn->PositionY, CharaSpawn->PositionZ);
		SpawnData.Rotation.Yaw = CharaSpawn->RotationYaw;

		// 装備する武器情報を取得する
		FCharaInitWeapon WeaponData = ReadCharaEquipWeapon(RowName.ToString());

		// エネミーのスポーン
		AEnemy* Enemy = SpawnEnemy(SpawnData);
		if (!IsValid(Enemy)) {
			continue;
		}
		EnemyList.Add(EnemyIndex, Enemy);
		EnemyIndexArray.Add(EnemyIndex);
		++EnemyIndex;
		Enemy->SetDetailData(EnemyDetail->CharacterDetail);
		Enemy->ReflectDetail();
		Enemy->SetCharaType(ECharacterType::Enemy);
		Enemy->SetCharaStateType(ECharaStateType::Fine);
		Enemy->SetStageType(CharaSpawn->StageType);

		// 警戒レベルリストにエリアを追加
		// ※登録済みのエリアは自動的にはじかれる
		AddAlertLevelList(CharaSpawn->StageType);

		// 武器の種類によって敵の種類を決定する
		switch (WeaponData.WeaponType) {
		case EWeaponType::AssaultRifle: {
			Enemy->SetEnemyType(EEnemyType::Normal);
			break;
		}
		case EWeaponType::SniperRifle: {
			Enemy->SetEnemyType(EEnemyType::Sniper);
			break;
		}
		}

		// 視野半径の設定
		AEnemyAIController* EnemyController = Enemy->GetEnemyController();
		if (IsValid(EnemyController)) {
			EnemyController->SetSightRadius(Enemy->GetEnemyType());
		}

		// 武器の生成
		AWeaponBase* Weapon = WeaponFactory->CreateWeapon(WeaponData.WeaponType);
		if (!IsValid(Weapon)) {
			continue;
		}
		//---------------------------------
		//武器に対して所有者の設定
		//エネミー側に武器を装備させる
		//---------------------------------
		Weapon->SetCharaOwner(Enemy);
		Weapon->SetHidden(true);
		Weapon->SetMaxLoadedAmmos();
		Weapon->SetMaxPossessAmmos();
		EWeaponType WeaponType = Weapon->GetWeaponType();
		Enemy->SetupPossessWeapon(WeaponType, WeaponData.EquipPosition);
		Enemy->SetWeapon(WeaponType, Weapon);
		Enemy->AttachParantSocket(Weapon, WeaponData.EquipPosition);

		//---------------------------------
		//出現時にディゾルブ処理を実行する
		//※チュートリアルのみ
		//---------------------------------
		if (CurrentLevelType == ELevelType::Tutorial) {
			Enemy->SetDissolveType(EDissolveType::Appearance);
			Enemy->ChangeDissolvePhase(EDissolvePhase::WaitAppearance);
		}
		else {
			Enemy->SetDissolveType(EDissolveType::None);
			Enemy->ChangeDissolvePhase(EDissolvePhase::Appearance);
		}
		Enemy->InitializeDissolve();
		Enemy->SetupDissolve();
	}
}

/// <summary>
/// 装備する武器の取得
/// </summary>
/// <param name="RowNumber"> 行番号 </param>
/// <returns> 装備する武器情報 </returns>
FCharaInitWeapon AEnemyManager::ReadCharaEquipWeapon(FString RowNumber) {

	// 武器情報の初期化
	FCharaInitWeapon WeaponData;
	WeaponData.EquipPosition = EEquipPositionType::Hand;
	WeaponData.WeaponType = EWeaponType::AssaultRifle;

	TArray<FName> RowNames = EnemyInitWeaponTable->GetRowNames();
	for (FName RowName : RowNames) {

		// 行の名前が数値でないなら次の行へ
		if (!FCString::IsNumeric(*RowName.ToString())) {
			continue;
		}

		// スポーン情報の行番号と一致しない場合は処理終了
		FCharaInitWeapon* InitWeapon = EnemyInitWeaponTable->FindRow<FCharaInitWeapon>(RowName, "");
		if (InitWeapon->SpawnNumber != RowNumber) {
			continue;
		}
		WeaponData.EquipPosition = InitWeapon->EquipPosition;
		WeaponData.WeaponType = InitWeapon->WeaponType;
		break;
	}

	return WeaponData;
}

/// <summary>
/// エネミーのスポーン
/// </summary>
/// <param name="SpawnData"> スポーン情報 </param>
/// <returns> スポーンしたエネミー </returns>
AEnemy* AEnemyManager::SpawnEnemy(FSpawnData SpawnData) {

	// エネミーのスポーン
	FActorSpawnParameters Params;
	AEnemy* Enemy = GetWorld()->SpawnActor<AEnemy>(EnemyClass, SpawnData.Position, SpawnData.Rotation, Params);
	if (!IsValid(Enemy)) {
		UE_LOG(LogTemp, Display, TEXT("AEnemyManager EnemySpawn Error"));
		return nullptr;
	}

	return Enemy;
}

/// <summary>
/// 警戒レベルリストのキー(エリアの種類)を追加
/// </summary>
/// <param name="StageType"> エリアの種類 </param>
/// <returns></returns>
void AEnemyManager::AddAlertLevelList(EStageType StageType) {

	if (AlertLevelList.Contains(StageType)) {
		return;
	}
	AlertLevelList.Add(StageType, EAlertLevel::Patrol);
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemyManager::Initialize() {

	AccumulateTime = ResetTime;
	ChangeCurrentFoundPlayerPhase(EFoundPlayerPhaseType::None);
}

/// <summary>
/// エネミーインデックスリストを取得
/// </summary>
/// <param name=""></param>
/// <returns> インデックスリスト </returns>
TArray<int32> AEnemyManager::GetEnemyIndexList() {

	return EnemyIndexArray;
}

/// <summary>
/// 指定エネミーの取得
/// </summary>
/// <param name="Index"> インデックス </param>
/// <returns> エネミー </returns>
AEnemy* AEnemyManager::GetEnemy(int32 Index) {

	if (!EnemyList.Contains(Index)) {
		return nullptr;
	}

	return EnemyList[Index];
}

/// <summary>
/// 最新のターゲット位置をセット
/// </summary>
/// <param name="Position"> ターゲット位置 </param>
/// <returns></returns>
void AEnemyManager::SetNewTargetPosition(FVector Position) {

	NewTargetPosition = Position;
}

/// <summary>
/// 最新のターゲット位置を取得
/// </summary>
/// <param name=""></param>
/// <returns> ターゲット位置 </returns>
FVector AEnemyManager::GetNewTargetPosition() {

	return NewTargetPosition;
}

/// <summary>
/// エネミーの目が見えるようにする
/// </summary>
/// <param name="StageType"> エリアの種類 </param>
/// <returns></returns>
void AEnemyManager::CanSeeEnemy(EStageType StageType) {

	// 視覚情報をONにする
	for (auto Enemy : EnemyList) {
		if (!IsValid(Enemy.Value)) {
			continue;
		}
		EStageType Type = Enemy.Value->GetStageType();
		if (Type != StageType) {
			continue;
		}

		AEnemyAIController* EnemyController = Enemy.Value->GetEnemyController();
		if (!IsValid(EnemyController)) {
			continue;
		}
		EnemyController->SetupSee(true);
	}
}

/// <summary>
/// エネミーの処理を強制的に停止する
/// </summary>
/// <param name="StageType"> エリアの種類 </param>
/// <returns></returns>
void AEnemyManager::StopEnemy(EStageType StageType) {

	// エネミーを強制的に巡回フェーズに変更
	// 視覚情報をOFFにする
	for (auto Enemy : EnemyList) {
		if (!IsValid(Enemy.Value)) {
			continue;
		}
		EStageType Type = Enemy.Value->GetStageType();
		if (Type != StageType) {
			continue;
		}

		Enemy.Value->ChangeForciblyAlertLevelPatrol();
		AEnemyAIController* EnemyController = Enemy.Value->GetEnemyController();
		if (!IsValid(EnemyController)) {
			continue;
		}
		EnemyController->SetupSee(false);
	}

	// 戦闘中BGMが流れている場合は通常BGMに切り替える
	ABattleController* BattleController = Cast<ABattleController>(UGameplayStatics::GetPlayerController(this, 0));
	if (IsValid(BattleController)) {

		AAudioManager* AudioManager = BattleController->GetAudioManager();
		if (IsValid(AudioManager)) {
			if (AudioManager->IsPlayingBattleBGM()) {

				AudioManager->FadeOutBGM();
				AudioManager->SetNextBGMSound(EBGMType::Game);
			}
		}
	}

	// 警戒ランプを停止しておく
	if (!IsValid(WarningLampManager)) {
		return;
	}
	WarningLampManager->TurnOffLamp();
}

/// <summary>
/// エネミーの破棄
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AEnemyManager::DisposeEnemies() {

	TArray<int32> DeadEnemyList;

	// エネミーを全て死亡リストに格納
	for (auto Enemy : EnemyList) {
		if (!IsValid(Enemy.Value)) {
			continue;
		}
		
		DeadEnemyList.Add(Enemy.Key);
	}

	//エネミーと装備している武器を破壊、
	//その後リストから対象エネミーを削除する
	for (int32 Index : DeadEnemyList) {

		if (!EnemyList.Contains(Index)) {
			continue;
		}

		AWeaponBase* Weapon = EnemyList[Index]->GetEquipWeapon();
		if (IsValid(Weapon)) {
			Weapon->CancelShooting();
			Weapon->Destroy();
		}
		if (IsValid(EnemyList[Index])) {
			EnemyList[Index]->Destroy();
		}

		EnemyList[Index] = nullptr;
		EnemyList.Remove(Index);
	}

	// エネミーのインデックスリストを初期化しておく
	EnemyIndexArray.Empty();
	// 戦闘用リスト、接近用リストからエネミーの
	// アドレスを削除しリストを空にしておく
	FightEnemyIDList.Empty();
	ApproachEnemyIDList.Empty();
	// アラートレベルの初期化
	AlertLevelList.Empty();
}

/// <summary>
/// 現在のプレイヤー発見フェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void AEnemyManager::ChangeCurrentFoundPlayerPhase(EFoundPlayerPhaseType PhaseType) {

	CurrentFoundPlayerPhase = PhaseType;
}

/// <summary>
/// エネミーの視覚設定
/// </summary>
/// <param name="bIsSight"> 視覚有無 </param>
/// <returns></returns>
void AEnemyManager::SetupDebugEnemySight(bool bIsSight) {

	// 現在プレイヤーが進行中のエリアを取得する
	ABattleGameState* BattleGameState = Cast<ABattleGameState>(GetWorld()->GetGameState());
	if (!IsValid(BattleGameState)) {
		return;
	}
	EStageType StageType = BattleGameState->GetCurrentStageType();

	if (bIsSight) {

		// 視覚情報をONにする
		// ※対象はプレイヤーがいるエリアのみ
		for (auto Enemy : EnemyList) {
			if (!IsValid(Enemy.Value)) {
				continue;
			}
			if (Enemy.Value->GetStageType() != StageType) {
				continue;
			}
			AEnemyAIController* EnemyController = Enemy.Value->GetEnemyController();
			if (!IsValid(EnemyController)) {
				continue;
			}
			EnemyController->SetupSee(bIsSight);
		}
	}
	else {

		// 視覚情報をOFFにする
		for (auto Enemy : EnemyList) {
			if (!IsValid(Enemy.Value)) {
				continue;
			}
			AEnemyAIController* EnemyController = Enemy.Value->GetEnemyController();
			if (!IsValid(EnemyController)) {
				continue;
			}
			EnemyController->SetupSee(bIsSight);
		}
	}
}