﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/CapsuleComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Sound/SoundCue.h"
#include "Battle/Enum/CommonEnum.h"
#include "PickupItemBase.generated.h"


/// <summary>
/// アイテムの種類
/// </summary>
UENUM()
enum class EPickupItemType {
	None,          // なし
	Ammo,          // 弾薬
	Healing,       // 回復
	Weapon,        // 武器
	MaxTypeIndex
};

/// <summary>
/// アイテム収拾の種類
/// </summary>
UENUM()
enum class EPickupType {
	None,          // なし
	OnlyOnce,      // 一度きり
	Permanent,     // 永続的
	Reborn,        // 復活
	MaxTypeIndex
};

/// <summary>
/// ピックアップアイテムベースクラス
/// </summary>
UCLASS()
class APickupItemBase : public AActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	APickupItemBase();

	/// <summary>
	/// アイテムの種類をセット
	/// </summary>
	/// <param name="Type"> アイテム収拾の種類 </param>
	/// <returns></returns>
	void SetPickupItemType(EPickupItemType Type);

	/// <summary>
	/// アイテムの種類を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> アイテム収拾の種類 </returns>
	EPickupItemType GetPickupItemType();

	/// <summary>
	/// アイテム収拾の種類をセット
	/// </summary>
	/// <param name="Type"> アイテム収拾の種類 </param>
	/// <returns></returns>
	void SetPickupType(EPickupType Type);

	/// <summary>
	/// アイテム収拾の種類を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> アイテム収拾の種類 </returns>
	EPickupType GetPickupType();

	/// <summary>
	/// 設置エリアの種類をセット
	/// </summary>
	/// <param name="Type"> エリアの種類 </param>
	/// <returns></returns>
	void SetStageType(EStageType Type);

	/// <summary>
	/// 設置エリアの種類を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> エリアの種類 </returns>
	EStageType GetStageType();

	/// <summary>
	/// パーティクルを停止する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void DeactiveParticleSystem();

	/// <summary>
	/// 再度拾うことができるようにする
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupPickupSystem();

	/// <summary>
	/// 拾われたかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:拾われた </returns>
	bool IsPickup();

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

public:

	/// <summary>
	/// カプセルコンポーネント
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PickupItemBase")
	UCapsuleComponent* CapsuleComp;

	/// <summary>
	/// パーティクルシステムコンポーネント
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PickupItemBase")
	UParticleSystemComponent* ParticleSystemComp;

	/// <summary>
	/// 収集物
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PickupItemBase")
	UParticleSystem* PickupItem;

	/// <summary>
	/// 収集物の追加エフェクト
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PickupItemBase")
	UParticleSystem* PickupItemEffect;

	/// <summary>
	/// 拾得時のエフェクト
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PickupItemBase")
	UParticleSystem* PickupEffect;

	/// <summary>
	/// 収拾時のサウンド
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PickupItemBase")
	USoundCue* PickupSound;

protected:

	/// <summary>
	/// アイテムの種類
	/// </summary>
	EPickupItemType PickupItemType;

	/// <summary>
	/// アイテム収拾の種類
	/// </summary>
	EPickupType PickupType;

	/// <summary>
	/// 設置エリアの種類
	/// </summary>
	EStageType StageType;

	/// <summary>
	/// アイテム拾ったかどうか
	/// </summary>
	bool bIsPickup;

protected:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// カプセルコンポーネント初期サイズ X方向
	/// </summary>
	const float InitSizeX = 60.0f;

	/// <summary>
	/// カプセルコンポーネント初期サイズ Y方向
	/// </summary>
	const float InitSizeY = 60.0f;

	/// <summary>
	/// 透過率 パラメータ名
	/// </summary>
	const FName RenderParamName = "RenderValue";

	/// <summary>
	/// 透過率 最小値
	/// </summary>
	const float RenderMinValue = 0.0f;

	/// <summary>
	/// 透過率 最大値
	/// </summary>
	const float RenderMaxValue = 1.0f;

	/// <summary>
	/// 透過率 調整値
	/// </summary>
	const float RenderAdjustValue = 0.05f;
};
