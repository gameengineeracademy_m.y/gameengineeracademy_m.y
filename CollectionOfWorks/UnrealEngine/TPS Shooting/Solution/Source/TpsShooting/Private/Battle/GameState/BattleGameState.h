﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "Battle/Enum/CommonEnum.h"
#include "Battle/CharacterManager/PlayerManager/PlayerManager.h"
#include "Battle/CharacterManager/EnemyManager/EnemyManager.h"
#include "BattleGameState.generated.h"


/// <summary>
/// ゲームの進行状況の種類
/// </summary>
UENUM(BlueprintType)
enum class EGameStateType : uint8 {
	None,          // なし
	Play,          // プレイ中
	GameOver,      // ゲームオーバー
	GameClear,     // ゲームクリア
	MaxTypeIndex
};

/// <summary>
/// バトルレベルのゲームステート
/// </summary>
UCLASS()
class ABattleGameState : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ABattleGameState();

	/// <summary>
	/// 現在のゲームの進行状況を変更する
	/// </summary>
	/// <param name="Type"> ゲームの進行状況 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category="BattleGameState")
	void ChangeCurrentGameState(EGameStateType Type);

	/// <summary>
	/// 現在のゲームの進行状況を取得する
	/// </summary>
	/// <param name=""></param>
	/// <returns> ゲームの進行状況 </returns>
	EGameStateType GetCurrentGameState();

	/// <summary>
	/// ゲームをプレイ中かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:プレイ中 </returns>
	bool IsPlay();

	/// <summary>
	/// 進行状況のリセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetGameState();

	/// <summary>
	/// 現在の進行中のエリアを変更する
	/// </summary>
	/// <param name="Type"> 進行中のエリア </param>
	/// <returns></returns>
	void ChangeCurrentStageType(EStageType Type);

	/// <summary>
	/// 現在の進行中のエリアを取得する
	/// </summary>
	/// <param name=""></param>
	/// <returns> 進行中のエリア </returns>
	EStageType GetCurrentStageType();

	/// <summary>
	/// 進行中のエリアのリセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetStageType();

	/// <summary>
	/// 進行中のエリアを保存する
	/// </summary>
	/// <param name="Type"> 進行中のエリア </param>
	/// <returns></returns>
	void SaveStageType(EStageType Type);

	/// <summary>
	/// 保存済みのエリアを取得する
	/// </summary>
	/// <param name=""></param>
	/// <returns> 保存済みのエリア </returns>
	EStageType GetSavedStageType();

	/// <summary>
	/// 保存されたデータを読みこむ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void LoadStageType();

	/// <summary>
	/// プレイヤーマネージャのセット
	/// </summary>
	/// <param name="Manager"> プレイヤーマネージャ </param>
	/// <returns></returns>
	void SetPlayerManager(APlayerManager* Manager);

	/// <summary>
	/// プレイヤーマネージャの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> プレイヤーマネージャ </returns>
	APlayerManager* GetPlayerManager();

	/// <summary>
	/// エネミーマネージャのセット
	/// </summary>
	/// <param name="Manager"> エネミーマネージャ </param>
	/// <returns></returns>
	void SetEnemyManager(AEnemyManager* Manager);

	/// <summary>
	/// エネミーマネージャの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> エネミーマネージャ </returns>
	AEnemyManager* GetEnemyManager();

	/// <summary>
	/// エネミー視覚情報設定
	/// </summary>
	/// <param name="bIsSight"> 視覚有無 </param>
	/// <returns></returns>
	void SetupDebugEnemySight(bool bIsSight);

	/// <summary>
	/// エネミー視覚情報の取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:見える </returns>
	bool IsEnemySight();

	/// <summary>
	/// カメラ演出処理フラグのセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category="BattleGameState")
	void SetFinishCameraDirection();

	/// <summary>
	/// カメラ演出処理フラグのリセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetFinishCameraDirection();

	/// <summary>
	/// カメラ演出が終了したかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:終了 </returns>
	bool IsFinishCameraDirection();

protected:

	/// <summary>
	/// 生成後実行イベント
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

private:

	/// <summary>
	/// 現在のゲームの進行状況
	/// </summary>
	EGameStateType CurrentGameState;

	/// <summary>
	/// 進行中のエリア
	/// </summary>
	EStageType CurrentStageType;

	/// <summary>
	/// セーブ済みエリア
	/// </summary>
	EStageType SavedStageType;

	/// <summary>
	/// プレイヤーマネージャ
	/// </summary>
	APlayerManager* PlayerManager;

	/// <summary>
	/// エネミーマネージャ
	/// </summary>
	AEnemyManager* EnemyManager;

	/// <summary>
	/// エネミーの視覚情報
	/// </summary>
	bool bIsEnemySight;

	/// <summary>
	/// カメラ演出終了フラグ
	/// </summary>
	bool bIsFinishCameraDirection;
};