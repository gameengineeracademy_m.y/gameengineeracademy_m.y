﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/UiAssignmentWidget.h"
#include "FadeWidget.generated.h"

/// <summary>
/// フェード用ウィジェット
/// </summary>
UCLASS()
class UFadeWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFadeWidget();

	/// <summary>
	/// フェード用イメージの透過率セット
	/// </summary>
	/// <param name="OpacityValue"> 透過率 </param>
	/// <returns></returns>
	void SetImageRenderOpacity(float OpacityValue);

	/// <summary>
	/// フェードインのアニメーションを開始する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FadeWidget")
	void PlayFadeInAnimation();

	/// <summary>
	/// フェードアウトのアニメーションを開始する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FadeWidget")
	void PlayFadeOutAnimation();

private:

	/// <summary>
	/// フェードインのアニメーションを開始する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayFadeInAnimation_Implementation();

	/// <summary>
	/// フェードアウトのアニメーションを開始する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayFadeOutAnimation_Implementation();
};
