﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UiAssignmentWidget/FadeWidget/FadeWidget.h"
#include "Components/Image.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UFadeWidget::UFadeWidget() {

}

/// <summary>
/// フェード用イメージの透過率セット
/// </summary>
/// <param name="OpacityValue"> 透過率 </param>
/// <returns></returns>
void UFadeWidget::SetImageRenderOpacity(float OpacityValue) {

	// フェード用のイメージを不透明にしておく
	UImage* Image = Cast<UImage>(GetWidgetFromName("Image_BlackBoard"));
	if (!IsValid(Image)) {
		return;
	}
	Image->SetRenderOpacity(OpacityValue);
}

/// <summary>
/// フェードインのアニメーションを開始する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UFadeWidget::PlayFadeInAnimation_Implementation() {

}

/// <summary>
/// フェードアウトのアニメーションを開始する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UFadeWidget::PlayFadeOutAnimation_Implementation() {

}