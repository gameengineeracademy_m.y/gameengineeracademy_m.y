﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "GuideTextWidget.generated.h"

/// <summary>
/// ガイド用テキストウィジェット
/// </summary>
UCLASS()
class UGuideTextWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativePreConstruct() override;

	/// <summary>
	/// テキストブロックの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> テキストブロック </returns>
	UTextBlock* GetTextBlock();

protected:

	/// <summary>
	/// テキスト
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "GuideText")
	FText GuideText;

	/// <summary>
	/// テキストの色
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "GuideText")
	FSlateColor TextColor;

protected:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// テキストブロック テキスト
	/// </summary>
	const FName TextBlockName = "TextBlock_Text";
};
