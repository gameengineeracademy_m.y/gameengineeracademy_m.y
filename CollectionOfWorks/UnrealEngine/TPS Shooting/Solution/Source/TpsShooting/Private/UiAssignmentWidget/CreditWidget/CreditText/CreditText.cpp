﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UiAssignmentWidget/CreditWidget/CreditText/CreditText.h"

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UCreditText::NativeConstruct() {

}

/// <summary>
/// テキストのセット
/// </summary>
/// <param name="Text"> 表示するテキスト </param>
/// <returns></returns>
void UCreditText::SetupText(FString Text) {

	UTextBlock* TextBlock = GetTextBlock();
	if (!IsValid(TextBlock)) {
		return;
	}
	TextBlock->SetText(FText::FromString(Text));
}

/// <summary>
/// テキストブロックの取得
/// </summary>
/// <param name=""></param>
/// <returns> テキストブロック </returns>
UTextBlock* UCreditText::GetTextBlock() {

	return Cast<UTextBlock>(GetWidgetFromName(TextBlockName));
}