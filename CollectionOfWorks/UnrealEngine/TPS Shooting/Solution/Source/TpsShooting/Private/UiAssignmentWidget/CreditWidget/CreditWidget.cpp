﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UiAssignmentWidget/CreditWidget/CreditWidget.h"
#include "Components/CanvasPanel.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UCreditWidget::UCreditWidget() {

}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UCreditWidget::NativeConstruct() {

	Super::NativeConstruct();

	ResetOperation();
}

/// <summary>
/// クレジットの設定
/// </summary>
/// <param name="Type"> ウィジェットの種類 </param>
/// <returns></returns>
void UCreditWidget::SetupCredit(EWidgetType Type) {

	if (Type == EWidgetType::None ||
		Type == EWidgetType::MaxTypeIndex) {
		return;
	}
	SetWidgetType(Type);

	//-----------------------------------------------
	//クレジットの生成を行い、画面にセットする
	//-----------------------------------------------
	Credit = CreateCredit();
	if (!IsValid(Credit)) {
		return;
	}
	AddChildCredit(Credit);
}

/// <summary>
/// クレジットの生成
/// </summary>
/// <param name=""></param>
/// <returns> クレジット </returns>
UCredit* UCreditWidget::CreateCredit() {

	UCredit* CreditObject = nullptr;
	FString WidgetPath;
	GConfig->GetString(TEXT("UMGCreditSettings"), TEXT("CreditPath"), WidgetPath, GGameIni);

	TSubclassOf<UCredit> CreditClass = TSoftClassPtr<UCredit>(FSoftObjectPath(*WidgetPath)).LoadSynchronous();
	if (!CreditClass) {
		return nullptr;
	}
	CreditObject = CreateWidget<UCredit>(GetWorld(), CreditClass);

	return CreditObject;
}

/// <summary>
/// 上方向への操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UCreditWidget::ScrollUp() {

	if (!IsValid(Credit)) {
		return;
	}
	Credit->ScrollUp();
}

/// <summary>
/// 下方向への操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UCreditWidget::ScrollDown() {

	if (!IsValid(Credit)) {
		return;
	}
	Credit->ScrollDown();
}


/// <summary>
/// 戻る操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UCreditWidget::Return() {

	PressReturn();

	// サウンドを鳴らす
	PlaySound(ReturnSound);
}

/// <summary>
/// クレジットをキャンバスに子要素として設定
/// </summary>
/// <param name="Widget"> 子ウィジェット </param>
/// <returns></returns>
void UCreditWidget::AddChildCredit(UWidget* Widget) {

	UCanvasPanel* CanvasPanel = Cast<UCanvasPanel>(GetWidgetFromName(CanvasPanelName));
	if (!IsValid(CanvasPanel)) {
		return;
	}
	CanvasPanel->AddChild(Widget);
}