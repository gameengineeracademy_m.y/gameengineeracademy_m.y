﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "UiAssignmentWidget/GuideWidget/GuideBaseWidget.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UGuideBaseWidget::UGuideBaseWidget() {

}

/// <summary>
/// 表示する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UGuideBaseWidget::Display() {

	SetVisibility(ESlateVisibility::SelfHitTestInvisible);
}

/// <summary>
/// 非表示にする
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UGuideBaseWidget::Hide() {

	SetVisibility(ESlateVisibility::Collapsed);
}