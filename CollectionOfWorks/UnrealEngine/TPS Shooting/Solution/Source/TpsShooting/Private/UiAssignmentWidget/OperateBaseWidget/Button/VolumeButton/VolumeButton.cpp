﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UiAssignmentWidget/OperateBaseWidget/Button/VolumeButton/VolumeButton.h"


/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UVolumeButton::NativePreConstruct() {

	Super::NativePreConstruct();

	USlider* Slider = GetVolumeSlider();
	if (!IsValid(Slider)) {
		return;
	}
	Slider->SetMaxValue(SliderMaxVolume);
	Slider->SetMinValue(SliderMinVolume);
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UVolumeButton::NativeConstruct() {

	Super::NativeConstruct();
}

/// <summary>
/// スライダーの値変更イベント設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UVolumeButton::SetupValueChanged() {

	USlider* Slider = GetVolumeSlider();
	if (!IsValid(Slider)) {
		return;
	}
	//------------------------------------
	//スライダーの値変更イベント処理を
	//デリゲート追加
	//------------------------------------
	if (Slider->OnValueChanged.IsBound()) {
		return;
	}
	Slider->OnValueChanged.AddDynamic(this, &UVolumeButton::OnValueChanged);
}

/// <summary>
/// スライダーの表示設定を変更する
/// </summary>
/// <param name="VisibilityType"> 表示設定 </param>
/// <returns></returns>
void UVolumeButton::SetupSliderVisibility(ESlateVisibility VisibilityType) {

	USlider* Slider = GetVolumeSlider();
	if (!IsValid(Slider)) {
		return;
	}
	Slider->SetVisibility(VisibilityType);
}

/// <summary>
/// ボリューム値のセット
/// </summary>
/// <param name="Value"> ボリューム </param>
/// <returns></returns>
void UVolumeButton::SetVolume(float Value) {

	USlider* Slider = GetVolumeSlider();
	if (!IsValid(Slider)) {
		return;
	}

	float FixedValue = CheckVolumeValue(Value);

	Slider->SetValue(FixedValue);
	SetupVolumeText(FixedValue);
	Volume = FixedValue;
}

/// <summary>
/// ボリューム値のセット
/// </summary>
/// <param name=""></param>
/// <returns> ボリューム値 </returns>
float UVolumeButton::GetVolume() {

	USlider* Slider = GetVolumeSlider();
	if (!IsValid(Slider)) {
		return MinVolume;
	}
	return Slider->GetValue();
}

/// <summary>
/// ボリュームボタンの種類を取得
/// </summary>
/// <param name=""></param>
/// <returns> ボリュームボタンの種類 </returns>
EVolumeButtonType UVolumeButton::GetVolumeButtonType() {

	return VolumeButtonType;
}

/// <summary>
/// テキストとゲージのボリューム設定
/// </summary>
/// <param name="Volume"> ボリューム </param>
/// <returns></returns>
void UVolumeButton::SetupVolumeText(float Value) {

	//--------------------------------------
	//ボリュームをセットし、再取得
	//ボリュームをテキストとゲージそれぞれに設定
	//--------------------------------------
	float VolumePerf = Hundred * Value;
	int32 VolumePer = static_cast<int32>(VolumePerf);

	// ボリュームテキスト
	UTextBlock* TextBlock = Cast<UTextBlock>(GetWidgetFromName(TextBlockVolumeName));
	if (!IsValid(TextBlock)) {
		return;
	}
	TextBlock->SetText(FText::FromString(FString::FromInt(VolumePer)));
}

/// <summary>
/// ボリュームが変化した
/// </summary>
/// <param name="Value"> ボリューム </param>
/// <returns></returns>
void UVolumeButton::OnValueChanged(float Value) {

	Volume = Value;
	SetupVolumeText(Value);
}

/// <summary>
/// ボリュームスライダーの取得
/// </summary>
/// <param name=""></param>
/// <returns></returns>
USlider* UVolumeButton::GetVolumeSlider() {

	return Cast<USlider>(GetWidgetFromName(SliderVolumeName));
}

/// <summary>
/// ボリューム値のチェック
/// </summary>
/// <param name="Value"> ボリューム値 </param>
/// <returns> ボリューム値 </returns>
float UVolumeButton::CheckVolumeValue(float Value) {

	float CheckValue = Value;

	if (CheckValue <= SliderMinVolume) {
		CheckValue = SliderMinVolume;
	}
	else if (CheckValue >= SliderMaxVolume) {
		CheckValue = SliderMaxVolume;
	}

	return CheckValue;
}

/// <summary>
/// BGMボリューム調整
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UVolumeButton::AdjustBgmVolume_Implementation() {

}

/// <summary>
/// SEボリューム調整
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UVolumeButton::AdjustSeVolume_Implementation() {

}