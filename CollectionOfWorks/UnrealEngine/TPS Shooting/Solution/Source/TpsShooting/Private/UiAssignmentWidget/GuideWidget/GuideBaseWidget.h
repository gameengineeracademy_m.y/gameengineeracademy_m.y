﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/UiAssignmentWidget.h"
#include "GuideBaseWidget.generated.h"

/// <summary>
/// ガイドベースウィジェット
/// </summary>
UCLASS()
class UGuideBaseWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UGuideBaseWidget();

	/// <summary>
	/// 表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Display();

	/// <summary>
	/// 非表示にする
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Hide();

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// クレジット設定先
	/// </summary>
	const FName CanvasPanelName = "CanvasPanel_Object";
};
