﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UiAssignmentWidget/GuideWidget/CameraGuideWidget/CameraGuideWidget.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UCameraGuideWidget::UCameraGuideWidget() {

}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UCameraGuideWidget::NativeConstruct() {

	Super::NativeConstruct();
}

/// <summary>
/// スキップゲージのセット
/// </summary>
/// <param name="Value"> ゲージ値 </param>
/// <returns></returns>
void UCameraGuideWidget::AdjustSkipGaugeValue(float Value) {

	if (Value > MaxGaugeValue) {
		Value = MaxGaugeValue;
	}
	SetSkipGaugeValue(Value);
}

/// <summary>
/// スキップゲージのセット
/// </summary>
/// <param name="Value"> ゲージ値 </param>
/// <returns></returns>
void UCameraGuideWidget::SetSkipGaugeValue_Implementation(float Value) {

}