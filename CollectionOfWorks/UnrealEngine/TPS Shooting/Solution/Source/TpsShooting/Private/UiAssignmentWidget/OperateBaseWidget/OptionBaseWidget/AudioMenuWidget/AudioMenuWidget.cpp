﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "UiAssignmentWidget/OperateBaseWidget/OptionBaseWidget/AudioMenuWidget/AudioMenuWidget.h"
#include "UiAssignmentWidget/OperateBaseWidget/Button/VolumeButton/VolumeButton.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UAudioMenuWidget::UAudioMenuWidget() {

}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UAudioMenuWidget::NativeConstruct() {

	Super::NativeConstruct();

	InitializeWidget();
}

/// <summary>
/// ウィジェットの設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UAudioMenuWidget::SetupWidget() {

	//-----------------------------------------------
	//オーディオメニュー画面上のボタンを取得
	//ボタンの初期化処理とイベントデリゲート処理
	//-----------------------------------------------

	UScrollBox* ScrollBox = GetScrollBox();
	if (!IsValid(ScrollBox)) {
		return;
	}

	TArray<UWidget*> ChildrenArray = ScrollBox->GetAllChildren();
	int32 Index = MinIndex;
	for (UWidget* Widget : ChildrenArray) {

		UVolumeButton* Button = Cast<UVolumeButton>(Widget);
		if (!IsValid(Button)) {
			continue;
		}
		Button->StopButtonAnimation();
		Button->SetupHoverEvent();
		Button->SetupValueChanged();
		EButtonType Type = Button->GetButtonType();

		ButtonList.Add(Index, Button);
		++Index;
	}

	if (ButtonList.Num() <= Zero) {
		return;
	}
	// 先頭メニューを選択状態にする
	CurrentButtonIndex = MinIndex;
	UButtonBase* Button = GetSelectButton();
	if (!IsValid(Button)) {
		return;
	}
	Button->PlayButtonAnimation();
}

/// <summary>
/// ウィジェット初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UAudioMenuWidget::InitializeWidget() {

	ResetOperation();
	CurrentButtonIndex = MinIndex;

	for (auto ButtonData : ButtonList) {

		UVolumeButton* Button = Cast<UVolumeButton>(ButtonData.Value);
		if (!IsValid(Button)) {
			continue;
		}
		Button->StopButtonAnimation();
		Button->SetupSliderVisibility(ESlateVisibility::SelfHitTestInvisible);
	}

	UVolumeButton* Button = Cast<UVolumeButton>(GetSelectButton());
	if (!IsValid(Button)) {
		return;
	}
	Button->PlayButtonAnimation();
	Button->SetupSliderVisibility(ESlateVisibility::Visible);
}

/// <summary>
/// オーディオボリュームの設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UAudioMenuWidget::SetupAudioVolume(FAudioVolumeData Volume) {

	AudioVolume = Volume;

	UVolumeButton* Button = nullptr;
	Button = Cast<UVolumeButton>(ButtonList[SeIndex]);
	if (IsValid(Button)) {
		Button->SetVolume(AudioVolume.Se);
	}
	Button = Cast<UVolumeButton>(ButtonList[BgmIndex]);
	if (IsValid(Button)) {
		Button->SetVolume(AudioVolume.Bgm);
	}
}

/// <summary>
/// 設定後のオーディオボリュームを取得する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
FAudioVolumeData UAudioMenuWidget::GetAfterSettingAudioVolume() {

	FAudioVolumeData Volume;
	UVolumeButton* Button = nullptr;

	Button = Cast<UVolumeButton>(ButtonList[BgmIndex]);
	if (IsValid(Button)) {
		Volume.Bgm = Button->GetVolume();
		UE_LOG(LogTemp, Warning, TEXT("AfterSettingAudioVolume BGM : %f"), Volume.Bgm)
	}
	Button = Cast<UVolumeButton>(ButtonList[SeIndex]);
	if (IsValid(Button)) {
		Volume.Se = Button->GetVolume();
		UE_LOG(LogTemp, Warning, TEXT("AfterSettingAudioVolume SE : %f"), Volume.Se)
	}

	return Volume;
}

/// <summary>
/// 左方向へ移動処理 ボリューム調整
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UAudioMenuWidget::AdjustVolumeDown() {

	//-----------------------------------------
	//画面上のボリューム値を下げる
	//オーディオ画面なら音量の設定も変更する
	//-----------------------------------------
	UVolumeButton* Button = Cast<UVolumeButton>(GetSelectButton());
	if (!IsValid(Button)) {
		return;
	}

	float Volume = Button->GetVolume();
	Volume -= AdjustVolume;
	Button->SetVolume(Volume);

	// 音量設定
	EVolumeButtonType ButtonType = Button->GetVolumeButtonType();
	switch (ButtonType) {
	case EVolumeButtonType::SE: {
		Button->AdjustSeVolume();
		break;
	}
	case EVolumeButtonType::BGM: {
		Button->AdjustBgmVolume();
		break;
	}
	}
}

/// <summary>
/// 右方向へ移動処理 ボリューム調整
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UAudioMenuWidget::AdjustVolumeUp() {

	//-----------------------------------------
	//画面上のボリューム値を上げる
	//オーディオ画面なら音量の設定も変更する
	//-----------------------------------------
	UVolumeButton* Button = Cast<UVolumeButton>(GetSelectButton());
	if (!IsValid(Button)) {
		return;
	}

	float Volume = Button->GetVolume();
	Volume += AdjustVolume;
	Button->SetVolume(Volume);

	// 音量設定
	EVolumeButtonType ButtonType = Button->GetVolumeButtonType();
	switch (ButtonType) {
	case EVolumeButtonType::SE: {
		
		Button->AdjustSeVolume();
		break;
	}
	case EVolumeButtonType::BGM: {
		
		Button->AdjustBgmVolume();
		break;
	}
	}
}

/// <summary>
/// 上方向へ移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UAudioMenuWidget::PressUp() {

	OperateKeyboardSelect(SelectUp);
}

/// <summary>
/// 下方向へ移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UAudioMenuWidget::PressDown() {

	OperateKeyboardSelect(SelectDown);
}

/// <summary>
/// マウス操作
/// </summary>
/// <param name="bMoveCursor"> マウスカーソル移動有無 </param>
/// <returns></returns>
void UAudioMenuWidget::OperateMouse(bool bMoveCursor) {


	for (auto ButtonData : ButtonList) {

		int32 ButtonIndex = ButtonData.Key;
		UVolumeButton* Button = Cast<UVolumeButton>(ButtonData.Value);
		if (!IsValid(Button)) {
			continue;
		}

		if (Button->IsPressed()) {
			// 選択したボタンを一旦ホバー状態にしておく
			Button->SwitchSelectMouseCursor();
			Decide();
			return;
		}

		//----------------------------------------
		// 別ボタンへのホバー直後は強制的に
		// ボタンを切り替える
		// それ以外はマウスカーソルが動いたか
		// どうかで処理の実行有無を決める
		//----------------------------------------
		if (Button->IsSwitchButton()) {

			// ホバー開始フラグを降ろしておく
			Button->ResetSwitchStartHovered();

			// 対象のボタンが現在選択されているボタンと同じかどうか
			if (ButtonIndex == CurrentButtonIndex) {
				continue;
			}
			OperateMouseSelect(ButtonIndex);
		}
		else {

			// マウスが移動したかどうか
			if (!bMoveCursor) {
				continue;
			}
			// 対象のボタンが選択(ホバー)されているかどうか
			if (!Button->IsSelected()) {
				continue;
			}
			// 対象のボタンが現在選択されているボタンと同じかどうか
			if (ButtonIndex == CurrentButtonIndex) {
				continue;
			}
			OperateMouseSelect(ButtonIndex);
		}
	}
}

/// <summary>
/// キーボード選択操作
/// </summary>
/// <param name="Direction"> 選択移動方向 </param>
/// <returns></returns>
void UAudioMenuWidget::OperateKeyboardSelect(int32 Direction) {

	//---------------------------------------
	//現在選択中のボタンを未選択状態にする
	//選択中インデックスを移動方向分加算
	//次に選択するボタンを選択状態にする
	//---------------------------------------

	PlaySound(SelectSound);

	UVolumeButton* CurrentButton = Cast<UVolumeButton>(GetSelectButton());
	if (!IsValid(CurrentButton)) {
		return;
	}
	CurrentButton->StopButtonAnimation();
	
	UOperateBaseWidget::OperateKeyboardSelect(Direction);

	CurrentButton = nullptr;
	CurrentButton = Cast<UVolumeButton>(GetSelectButton());
	if (!IsValid(CurrentButton)) {
		return;
	}
	CurrentButton->PlayButtonAnimation();

	// ※キー操作時はボタンのスライダーの表示設定は切り替えない
	// 　マウス操作時のみ切り替えを行う
	// →スライダー上にマウスカーソルが存在する状態でキー操作にて
	//   選択ボタンを切り替えた時に、表示設定を切り替えてしまうと
	//   スライダーの後ろにあるボタンがホバー判定となってしまい、
	//   キー操作ではボタンの切り替えが出来なくなってしまうため
}

/// <summary>
/// マウス選択操作
/// </summary>
/// <param name="Index"> インデックス </param>
/// <returns></returns>
void UAudioMenuWidget::OperateMouseSelect(int32 Index) {

	//---------------------------------------
	//現在選択中のボタンを未選択状態にする
	//選択中インデックスを移動方向分加算
	//次に選択するボタンを選択状態にする
	//---------------------------------------

	PlaySound(SelectSound);

	UVolumeButton* CurrentButton = Cast<UVolumeButton>(GetSelectButton());
	if (!IsValid(CurrentButton)) {
		return;
	}
	CurrentButton->StopButtonAnimation();
	CurrentButton->ResetSelectMouseCursor();
	CurrentButton->SetupSliderVisibility(ESlateVisibility::SelfHitTestInvisible);

	UOperateBaseWidget::OperateMouseSelect(Index);

	CurrentButton = nullptr;
	CurrentButton = Cast<UVolumeButton>(GetSelectButton());
	if (!IsValid(CurrentButton)) {
		return;
	}
	CurrentButton->PlayButtonAnimation();
	CurrentButton->SetupSliderVisibility(ESlateVisibility::Visible);
}

/// <summary>
/// 音量を上げる
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UAudioMenuWidget::OnVolumeUp() {

	AdjustVolumeUp();
}

/// <summary>
/// 音量を下げる
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UAudioMenuWidget::OnVolumeDown() {

	AdjustVolumeDown();
}