﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/GuideWidget/GuideBaseWidget.h"
#include "CameraGuideWidget.generated.h"

/// <summary>
/// カメラ演出ガイドウィジェット
/// </summary>
UCLASS()
class UCameraGuideWidget : public UGuideBaseWidget
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UCameraGuideWidget();

	/// <summary>
	/// スキップゲージのセット
	/// </summary>
	/// <param name="Value"> ゲージ値 </param>
	/// <returns></returns>
	void AdjustSkipGaugeValue(float Value);

	/// <summary>
	/// スキップゲージのセット
	/// </summary>
	/// <param name="Value"> ゲージ値 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GuideBaseWidget")
	void SetSkipGaugeValue(float Value);

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

private:

	/// <summary>
	/// スキップゲージのセット
	/// </summary>
	/// <param name="Value"> ゲージ値 </param>
	/// <returns></returns>
	void SetSkipGaugeValue_Implementation(float Value);

private:
	//--------------------------------
	//以下、定数宣言
	//--------------------------------
	/// <summary>
	/// ゲージ初期値
	/// </summary>
	const float InitGaugeValue = 0.0f;

	/// <summary>
	/// ゲージ最大値
	/// </summary>
	const float MaxGaugeValue = 1.0f;
};