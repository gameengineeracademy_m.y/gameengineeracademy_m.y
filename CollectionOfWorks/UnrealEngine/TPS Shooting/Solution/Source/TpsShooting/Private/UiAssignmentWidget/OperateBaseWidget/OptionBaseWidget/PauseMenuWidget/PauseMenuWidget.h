﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/OperateBaseWidget/OptionBaseWidget/OptionBaseWidget.h"
#include "PauseMenuWidget.generated.h"

/// <summary>
/// ポーズメニューウィジェット
/// </summary>
UCLASS()
class UPauseMenuWidget : public UOptionBaseWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPauseMenuWidget();

	/// <summary>
	/// 決定したボタンのオプションボタンの種類を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オプションボタンの種類 </returns>
	EOptionButtonType GetDecideOptionButtonType();

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;
};
