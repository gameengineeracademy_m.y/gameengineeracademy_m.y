﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UiAssignmentWidget/UiAssignmentWidget.h"
#include "Framework/Application/NavigationConfig.h"

/// <summary>
/// ウィジェット表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UUiAssignmentWidget::Open_Implementation() {

	bool IsCheck = IsDisplay();
	if (IsCheck) {
		return;
	}
	bNowDisplay = true;
	AddToViewport(ZOrder);
}

/// <summary>
/// ウィジェット終了処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UUiAssignmentWidget::Close_Implementation() {

}

/// <summary>
/// ウィジェット非表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UUiAssignmentWidget::Closed_Implementation() {

	bool IsCheck = IsDisplay();
	if (!IsCheck) {
		return;
	}
	bNowDisplay = false;
	RemoveFromParent();
}

/// <summary>
/// ナビゲーションのOFF設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UUiAssignmentWidget::SetupNavigationOFF() {

	// 現在設定されている、UIナビゲーション設定を取得
	TSharedRef<FNavigationConfig> NavigationConfig = FSlateApplication::Get().GetNavigationConfig();
	// UIナビゲーション操作の設定を変更
	NavigationConfig->bAnalogNavigation = false;   // コントローラなどのアナログスティックや十字キーを用いた操作
	NavigationConfig->bKeyNavigation = false;      // キーボードの上下左右のキーを用いた操作
	NavigationConfig->bTabNavigation = false;      // Tabキーを用いた操作
	// 使用するUIナビゲーション設定を反映
	FSlateApplication::Get().SetNavigationConfig(NavigationConfig);
}

/// <summary>
/// アニメーション終了フラグ初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UUiAssignmentWidget::ResetAnimFlag() {

	bAnimFinishFlag = false;
}

/// <summary>
/// アニメーション終了したかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:終了済み, false:実行中 </returns>
bool UUiAssignmentWidget::IsAnimFinish() {

	return bAnimFinishFlag;
}

/// <summary>
/// 表示有無フラグ初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UUiAssignmentWidget::ResetNowDisplay() {

	bNowDisplay = false;
}

/// <summary>
/// 表示中かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:表示中 </returns>
bool UUiAssignmentWidget::IsDisplay() {

	return bNowDisplay;
}