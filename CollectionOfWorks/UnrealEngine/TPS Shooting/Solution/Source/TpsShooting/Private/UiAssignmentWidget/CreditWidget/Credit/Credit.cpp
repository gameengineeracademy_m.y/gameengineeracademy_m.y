﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UiAssignmentWidget/CreditWidget/Credit/Credit.h"
#include "Components/ScrollBox.h"


/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UCredit::NativePreConstruct() {

	Super::NativePreConstruct();

	SetupCreditText();
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UCredit::NativeConstruct() {

}

/// <summary>
/// クレジットテキストを設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UCredit::SetupCreditText() {

	// データテーブルの行の名前を取得し、
	// テキストをクレジット画面に反映する
	TArray<FName> RowNames = CreditTextTable->GetRowNames();
	for (FName RowName : RowNames) {
		FCreditData* CreditData = CreditTextTable->FindRow<FCreditData>(RowName, "");
		UCreditText* CreditText = CreateCreditText();
		if (!IsValid(CreditText)) {
			continue;
		}
		// テキストのセット
		CreditText->SetupText(CreditData->Text);
		// クレジット画面上にクレジットテキストを設定
		AddChildCreditText(CreditText);
	}
}

/// <summary>
/// クレジットテキスト生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UCreditText* UCredit::CreateCreditText() {

	UCreditText* CreditObject = nullptr;
	FString WidgetPath;
	GConfig->GetString(TEXT("UMGCreditSetting"), TEXT("CreditTextPath"), WidgetPath, GGameIni);

	TSubclassOf<UCreditText> CreditTextClass = TSoftClassPtr<UCreditText>(FSoftObjectPath(*WidgetPath)).LoadSynchronous();
	if (!CreditTextClass) {
		return nullptr;
	}
	CreditObject = CreateWidget<UCreditText>(GetWorld(), CreditTextClass);

	return CreditObject;
}

/// <summary>
/// スクロールボックスに子要素として設定
/// </summary>
/// <param name="Widget"> 子ウィジェット </param>
/// <returns></returns>
void UCredit::AddChildCreditText(UWidget* Widget) {

	if (!IsValid(Widget)) {
		return;
	}

	UScrollBox* ScrollBox = Cast<UScrollBox>(GetWidgetFromName(ScrollBoxCreditText));
	if (!IsValid(ScrollBox)) {
		return;
	}
	ScrollBox->AddChild(Widget);
}

/// <summary>
/// スクロールアップ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UCredit::ScrollUp_Implementation() {

}

/// <summary>
/// スクロールダウン
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UCredit::ScrollDown_Implementation() {

}