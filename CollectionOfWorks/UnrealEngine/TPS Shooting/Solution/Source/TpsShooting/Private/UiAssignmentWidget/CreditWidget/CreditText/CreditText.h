﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "CreditText.generated.h"

/// <summary>
/// クレジットテキスト
/// </summary>
UCLASS()
class UCreditText : public UUserWidget
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// テキストのセット
	/// </summary>
	/// <param name="Text"> 表示するテキスト </param>
	/// <returns></returns>
	void SetupText(FString Text);

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

	/// <summary>
	/// テキストブロックの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> テキストブロック </returns>
	UTextBlock* GetTextBlock();

protected:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// テキストブロック テキスト
	/// </summary>
	const FName TextBlockName = "TextBlock_Text";
};
