﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "ButtonBase.generated.h"

/// <summary>
/// ボタンの種類
/// </summary>
UENUM(BlueprintType)
enum class EButtonType : uint8 {
	None,            // なし
	GameStart,       // ゲームスタート
	Tutorial,        // チュートリアル
	Option,          // オプション
	Credit,          // クレジット
	GameEnd,         // ゲーム終了
	Retry,           // リトライ
	ReturnTitle,     // タイトルへ戻る
	MaxTypeIndex
};

/// <summary>
/// ボタンの状態
/// </summary>
UENUM(BlueprintType)
enum class EButtonStateType : uint8 {
	None,            // なし
	Hovered,         // 選択されている
	Pressed,         // 押されている
	MaxTypeIndex
};

/// <summary>
/// ボタンベースウィジェットクラス
/// </summary>
UCLASS()
class UButtonBase : public UUserWidget
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// ボタンの種類を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ボタンの種類 </returns>
	EButtonType GetButtonType();

	/// <summary>
	/// ホバーとクリックイベントの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupHoverAndPressEvent();

	/// <summary>
	/// ホバーイベントの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupHoverEvent();

	/// <summary>
	/// ボタンが切り替ったかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:切り替ったか </returns>
	bool IsSwitchButton();

	/// <summary>
	/// ボタンが選択中かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:選択されている </returns>
	bool IsSelected();

	/// <summary>
	/// ボタンが押されたかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:押された </returns>
	bool IsPressed();

	/// <summary>
	/// マウスカーソル未選択状態にする
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetSelectMouseCursor();

	/// <summary>
	/// マウスカーソルの状態をホバーにしておく
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SwitchSelectMouseCursor();

	/// <summary>
	/// ホバー切り替わりフラグをリセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetSwitchStartHovered();

	/// <summary>
	/// プレス処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION()
	void OnPressed();

	/// <summary>
	/// ホバー処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION()
	void OnHovered();

	/// <summary>
	/// カーソルアニメーションの停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, Category = "ButtonBase")
	void StopButtonAnimation();

	/// <summary>
	/// カーソルアニメーションの開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, Category = "ButtonBase")
	void PlayButtonAnimation();

	/// <summary>
	/// ボタン選択アニメーションの開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, Category = "ButtonBase")
	void PlayPressButtonAnimation();

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativePreConstruct() override;

	/// <summary>
	/// ボタンの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ボタン </returns>
	UButton* GetButton();

	/// <summary>
	/// テキストブロックの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> テキストブロック </returns>
	UTextBlock* GetTextBlock();

	/// <summary>
	/// ボタンの状態を変更する
	/// </summary>
	/// <param name="StateType"> ボタンの状態の種類 </param>
	/// <returns></returns>
	void ChangeCurrentButtonStateType(EButtonStateType StateType);

	/// <summary>
	/// カーソルアニメーションの停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void StopButtonAnimation_Implementation();

	/// <summary>
	/// カーソルアニメーションの開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayButtonAnimation_Implementation();

	/// <summary>
	/// ボタン選択アニメーションの開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayPressButtonAnimation_Implementation();

protected:

	/// <summary>
	/// ボタンの種類
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "ButtonBase")
	EButtonType ButtonType;

	/// <summary>
	/// ボタンテキスト
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "ButtonBase")
	FText ButtonText;

	/// <summary>
	/// 現在のボタンの状態
	/// </summary>
	EButtonStateType CurrentButtonStateType;

	/// <summary>
	/// ホバー状態になった
	/// </summary>
	bool bStartHovered;

protected:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// ボタン
	/// </summary>
	const FName ButtonName = "Button";

	/// <summary>
	/// テキストブロック ボタンテキスト
	/// </summary>
	const FName TextBlockTitle = "TextBlock_ButtonText";
};
