﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UiAssignmentWidget/OperateBaseWidget/Button/ButtonBase.h"
#include "Components/Image.h"


/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UButtonBase::NativePreConstruct() {

	Super::NativePreConstruct();

	UTextBlock* TextBlock = GetTextBlock();
	if (IsValid(TextBlock)) {
		TextBlock->SetText(ButtonText);
	}
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UButtonBase::NativeConstruct() {

	Super::NativeConstruct();

	bStartHovered = false;
	CurrentButtonStateType = EButtonStateType::None;
}

/// <summary>
/// ボタンの種類を取得
/// </summary>
/// <param name=""></param>
/// <returns> ボタンの種類 </returns>
EButtonType UButtonBase::GetButtonType() {

	return ButtonType;
}

/// <summary>
/// イベントの設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UButtonBase::SetupHoverAndPressEvent() {

	UButton* Button = GetButton();
	if (!IsValid(Button)) {
		return;
	}
	//------------------------------------
	//ボタンのクリック処理とホバー処理を
	//デリゲート追加
	//------------------------------------
	if (!Button->OnPressed.IsBound()) {
		Button->OnPressed.AddDynamic(this, &UButtonBase::OnPressed);
	}
	if (!Button->OnHovered.IsBound()) {
		Button->OnHovered.AddDynamic(this, &UButtonBase::OnHovered);
	}
}

/// <summary>
/// ホバーイベントの設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UButtonBase::SetupHoverEvent() {

	UButton* Button = GetButton();
	if (!IsValid(Button)) {
		return;
	}
	//------------------------------------
	//ボタンのホバー処理をデリゲート追加
	//------------------------------------
	if (!Button->OnHovered.IsBound()) {
		Button->OnHovered.AddDynamic(this, &UButtonBase::OnHovered);
	}
}

/// <summary>
/// ボタンが切り替ったかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:切り替ったか </returns>
bool UButtonBase::IsSwitchButton() {

	return bStartHovered;
}

/// <summary>
/// ボタンが選択中かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:選択されている </returns>
bool UButtonBase::IsSelected() {

	return CurrentButtonStateType == EButtonStateType::Hovered;
}

/// <summary>
/// ボタンが押されたかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:押された </returns>
bool UButtonBase::IsPressed() {

	return CurrentButtonStateType == EButtonStateType::Pressed;
}

/// <summary>
/// プレス処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UButtonBase::OnPressed() {

	ChangeCurrentButtonStateType(EButtonStateType::Pressed);
}

/// <summary>
/// ホバー処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UButtonBase::OnHovered() {

	bStartHovered = true;
	ChangeCurrentButtonStateType(EButtonStateType::Hovered);
}

/// <summary>
/// ボタンの取得
/// </summary>
/// <param name=""></param>
/// <returns> ボタン </returns>
UButton* UButtonBase::GetButton() {

	return Cast<UButton>(GetWidgetFromName(ButtonName));
}

/// <summary>
/// テキストブロックの取得
/// </summary>
/// <param name=""></param>
/// <returns> テキストブロック </returns>
UTextBlock* UButtonBase::GetTextBlock() {

	return Cast<UTextBlock>(GetWidgetFromName(TextBlockTitle));
}

/// <summary>
/// ボタンの状態を変更する
/// </summary>
/// <param name="StateType"> ボタンの状態の種類 </param>
/// <returns></returns>
void UButtonBase::ChangeCurrentButtonStateType(EButtonStateType StateType) {

	CurrentButtonStateType = StateType;
}

/// <summary>
/// マウスカーソル未選択状態にする
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UButtonBase::ResetSelectMouseCursor() {

	ChangeCurrentButtonStateType(EButtonStateType::None);
}

/// <summary>
/// マウスカーソルの状態をホバーにしておく
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UButtonBase::SwitchSelectMouseCursor() {

	ChangeCurrentButtonStateType(EButtonStateType::Hovered);
}

/// <summary>
/// ホバー切り替わりフラグをリセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UButtonBase::ResetSwitchStartHovered() {

	bStartHovered = false;
}

/// <summary>
/// カーソルアニメーションの停止
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UButtonBase::StopButtonAnimation_Implementation() {

}

/// <summary>
/// カーソルアニメーションの開始
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UButtonBase::PlayButtonAnimation_Implementation() {

}

/// <summary>
/// ボタン選択アニメーションの開始
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UButtonBase::PlayPressButtonAnimation_Implementation() {

}