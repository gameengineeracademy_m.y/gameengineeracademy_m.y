﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UiAssignmentWidget/GuideWidget/GuideTextWidget.h"


/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UGuideTextWidget::NativePreConstruct() {

	Super::NativePreConstruct();

	UTextBlock* TextBlock = GetTextBlock();
	if (IsValid(TextBlock)) {
		TextBlock->SetText(GuideText);
		TextBlock->SetColorAndOpacity(TextColor);
	}
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UGuideTextWidget::NativeConstruct() {

	Super::NativeConstruct();
}

/// <summary>
/// テキストブロックの取得
/// </summary>
/// <param name=""></param>
/// <returns> テキストブロック </returns>
UTextBlock* UGuideTextWidget::GetTextBlock() {

	return Cast<UTextBlock>(GetWidgetFromName(TextBlockName));
}