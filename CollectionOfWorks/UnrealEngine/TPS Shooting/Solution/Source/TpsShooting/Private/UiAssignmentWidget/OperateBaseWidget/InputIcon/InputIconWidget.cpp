﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UiAssignmentWidget/OperateBaseWidget/InputIcon/InputIconWidget.h"


/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UInputIconWidget::NativePreConstruct() {

	Super::NativePreConstruct();

	UTextBlock* TextBlock = GetTextBlock();
	if (IsValid(TextBlock)) {
		TextBlock->SetText(DisplayText);
	}
}

/// <summary>
/// テキストブロックの取得
/// </summary>
/// <param name=""></param>
/// <returns> テキストブロック </returns>
UTextBlock* UInputIconWidget::GetTextBlock() {

	return Cast<UTextBlock>(GetWidgetFromName(TextBlockName));
}