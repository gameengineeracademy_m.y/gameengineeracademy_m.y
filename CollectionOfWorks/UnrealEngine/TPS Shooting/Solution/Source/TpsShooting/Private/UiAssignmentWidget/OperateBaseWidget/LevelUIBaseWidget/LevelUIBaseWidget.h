﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/OperateBaseWidget/OperateBaseWidget.h"
#include "LevelUIBaseWidget.generated.h"

/// <summary>
/// レベルUIウィジェットベースクラス
/// </summary>
/// <remarks>
/// タイトル、リザルトのベースクラス
/// </remarks>
UCLASS()
class ULevelUIBaseWidget : public UOperateBaseWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ULevelUIBaseWidget();

	/// <summary>
	/// ウィジェットの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupWidget() override;

	/// <summary>
	/// ウィジェット初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeWidget();

	/// <summary>
	/// 上方向へ移動
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressUp() override;

	/// <summary>
	/// 下方向へ移動
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressDown() override;

	/// <summary>
	/// 右方向へ移動
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressRight() override;

	/// <summary>
	/// 左方向へ移動
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressLeft() override;

	/// <summary>
	/// 決定操作
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Decide();

	/// <summary>
	/// マウス操作
	/// </summary>
	/// <param name="bMoveCursor"> マウスカーソル移動有無 </param>
	/// <returns></returns>
	void OperateMouse(bool bMoveCursor) override;

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

	/// <summary>
	/// キーボード選択操作
	/// </summary>
	/// <param name="Direction"> 選択移動方向 </param>
	/// <returns></returns>
	void OperateKeyboardSelect(int32 Direction);

	/// <summary>
	/// マウス選択操作
	/// </summary>
	/// <param name="Index"> インデックス </param>
	/// <returns></returns>
	void OperateMouseSelect(int32 Index);
};
