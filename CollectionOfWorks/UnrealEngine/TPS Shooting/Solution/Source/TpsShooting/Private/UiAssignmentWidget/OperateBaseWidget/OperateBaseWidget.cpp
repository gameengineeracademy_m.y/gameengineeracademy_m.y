﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UiAssignmentWidget/OperateBaseWidget/OperateBaseWidget.h"
#include "Kismet/GameplayStatics.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UOperateBaseWidget::UOperateBaseWidget() {

}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UOperateBaseWidget::NativeConstruct() {

	Super::NativeConstruct();

	ResetOperation();
}

/// <summary>
/// ウィジェットの種類のセット
/// </summary>
/// <param name="Type"> ウィジェットの種類 </param>
/// <returns></returns>
void UOperateBaseWidget::SetWidgetType(EWidgetType Type) {

	WidgetType = Type;
}

/// <summary>
/// ウィジェットの種類のセット
/// </summary>
/// <param name=""></param>
/// <returns> ウィジェットの種類 </returns>
EWidgetType UOperateBaseWidget::GetWidgetType() {

	return WidgetType;
}

/// <summary>
/// バーティカルボックスの取得
/// </summary>
/// <param name=""></param>
/// <returns> バーティカルボックス </returns>
UVerticalBox* UOperateBaseWidget::GetVerticalBox() {

	return Cast<UVerticalBox>(GetWidgetFromName(VerticalBoxName));
}

/// <summary>
/// スクロールボックスの取得
/// </summary>
/// <param name=""></param>
/// <returns> スクロールボックス </returns>
UScrollBox* UOperateBaseWidget::GetScrollBox() {

	return Cast<UScrollBox>(GetWidgetFromName(ScrollBoxName));
}

/// <summary>
/// 決定処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UOperateBaseWidget::PressDecide() {

	UButtonBase* Button = GetSelectButton();
	if (!IsValid(Button)) {
		return;
	}

	DecideButton = Button;
	ChangeCurrentOperateType(EOperateType::Decide);
}

/// <summary>
/// 戻る処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UOperateBaseWidget::PressReturn() {

	PlaySound(ReturnSound);

	ChangeCurrentOperateType(EOperateType::Return);
}

/// <summary>
/// 現在の操作の種類を取得
/// </summary>
/// <param name=""></param>
/// <returns> 操作の種類の種類 </returns>
EOperateType UOperateBaseWidget::GetCurrentOperateType() {

	return CurrentOperateType;
}

/// <summary>
/// 決定したボタンの種類を取得
/// </summary>
/// <param name=""></param>
/// <returns> 決定したボタンの種類 </returns>
EButtonType UOperateBaseWidget::GetDecideButtonType() {

	if (!IsValid(DecideButton)) {
		return EButtonType::None;
	}

	return DecideButton->GetButtonType();
}

/// <summary>
/// 処理をリセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UOperateBaseWidget::ResetOperation() {

	DecideButton = nullptr;
	CurrentOperateType = EOperateType::Operation;
}

/// <summary>
/// 現在の操作フェーズが操作不可かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:操作不可 </returns>
bool UOperateBaseWidget::IsLock() {

	return CurrentOperateType == EOperateType::Lock;
}

/// <summary>
/// 現在の操作フェーズが決定かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:決定 </returns>
bool UOperateBaseWidget::IsDecide() {

	return CurrentOperateType == EOperateType::Decide;
}

/// <summary>
/// 現在の操作フェーズが戻るかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:戻る </returns>
bool UOperateBaseWidget::IsReturn() {

	return CurrentOperateType == EOperateType::Return;
}

/// <summary>
/// 現在の操作の種類を変更する
/// </summary>
/// <param name="Type"> 操作の種類 </param>
/// <returns></returns>
void UOperateBaseWidget::ChangeCurrentOperateType(EOperateType Type) {

	CurrentOperateType = Type;
}

/// <summary>
/// キーボード選択操作
/// </summary>
/// <param name="Direction"> 選択移動方向 </param>
/// <returns></returns>
void UOperateBaseWidget::OperateKeyboardSelect(int32 Direction) {

	CurrentButtonIndex += Direction;
	if (CurrentButtonIndex < MinIndex) {
		CurrentButtonIndex = ButtonList.Num() - One;
	}
	else if (CurrentButtonIndex > ButtonList.Num() - One) {
		CurrentButtonIndex = MinIndex;
	}
}

/// <summary>
/// マウス選択操作
/// </summary>
/// <param name="Index"> インデックス </param>
/// <returns></returns>
void UOperateBaseWidget::OperateMouseSelect(int32 Index) {

	CurrentButtonIndex = Index;
}

/// <summary>
/// 選択中のボタンの取得
/// </summary>
/// <param name=""></param>
/// <returns> 選択中のボタン </returns>
UButtonBase* UOperateBaseWidget::GetSelectButton() {

	if (!ButtonList.Contains(CurrentButtonIndex)) {
		return nullptr;
	}

	return ButtonList[CurrentButtonIndex];
}

/// <summary>
/// 決定したボタンの取得
/// </summary>
/// <param name=""></param>
/// <returns> 選択中のボタン </returns>
UButtonBase* UOperateBaseWidget::GetDecideButton() {

	return DecideButton;
}

/// <summary>
/// サウンドを鳴らす
/// </summary>
/// <param name="Sound"> サウンド </param>
/// <returns></returns>
void UOperateBaseWidget::PlaySound(USoundCue* Sound) {

	if (!IsValid(Sound)) {
		return;
	}
	UGameplayStatics::PlaySound2D(GetWorld(), Sound);
}