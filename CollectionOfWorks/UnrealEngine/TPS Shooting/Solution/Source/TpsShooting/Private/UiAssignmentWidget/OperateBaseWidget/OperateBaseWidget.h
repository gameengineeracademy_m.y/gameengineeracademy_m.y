﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/UiAssignmentWidget.h"
#include "UiAssignmentWidget/OperateBaseWidget/Button/ButtonBase.h"
#include "UiAssignmentWidget/OperateBaseWidget/Text/TextBase.h"
#include "Engine/DataTable.h"
#include "Components/VerticalBox.h"
#include "Components/ScrollBox.h"
#include "Sound/SoundCue.h"
#include "OperateBaseWidget.generated.h"

/// <summary>
/// ウィジェットの種類
/// </summary>
UENUM(BlueprintType)
enum class EWidgetType : uint8 {
	None,          // なし
	Title,         // タイトル
	TutorialRetry, // チュートリアルリトライ
	TutorialClear, // チュートリアルクリア
	GameOver,      // ゲームオーバー
	GameClear,     // ゲームクリア
	Credit,        // クレジット
	MaxTypeIndex
};

/// <summary>
/// 操作の種類
/// </summary>
UENUM(BlueprintType)
enum class EOperateType : uint8 {
	None,            // なし
	Lock,            // 処理不可
	Operation,       // 操作中
	Decide,          // 決定
	Return,          // 戻る
	MaxTypeIndex
};

/// <summary>
/// ボタン操作ウィジェットベースクラス
/// </summary>
/// <remarks>
/// ボタン操作実行のベースクラス
/// </remarks>
UCLASS()
class UOperateBaseWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UOperateBaseWidget();

	/// <summary>
	/// ウィジェットの種類のセット
	/// </summary>
	/// <param name="Type"> ウィジェットの種類 </param>
	/// <returns></returns>
	void SetWidgetType(EWidgetType Type);

	/// <summary>
	/// ウィジェットの種類のセット
	/// </summary>
	/// <param name=""></param>
	/// <returns> ウィジェットの種類 </returns>
	EWidgetType GetWidgetType();

	/// <summary>
	/// ウィジェットの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void SetupWidget() {};

	/// <summary>
	/// 現在の操作の種類を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 操作の種類の種類 </returns>
	EOperateType GetCurrentOperateType();

	/// <summary>
	/// 決定したボタンの種類を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 決定したボタンの種類 </returns>
	EButtonType GetDecideButtonType();

	/// <summary>
	/// 処理をリセット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetOperation();

	/// <summary>
	/// 現在の操作フェーズが操作不可かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:操作不可 </returns>
	bool IsLock();

	/// <summary>
	/// 現在の操作フェーズが決定かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:決定 </returns>
	bool IsDecide();

	/// <summary>
	/// 現在の操作フェーズが戻るかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:戻る </returns>
	bool IsReturn();

	/// <summary>
	/// 上方向へ移動
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PressUp() {};

	/// <summary>
	/// 下方向へ移動
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PressDown() {};

	/// <summary>
	/// 右方向へ移動
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PressRight() {};

	/// <summary>
	/// 左方向へ移動
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PressLeft() {};

	/// <summary>
	/// 決定処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressDecide();

	/// <summary>
	/// 戻る処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressReturn();

	/// <summary>
	/// マウス操作
	/// </summary>
	/// <param name="bMoveCursor"> マウスカーソル移動有無 </param>
	/// <returns></returns>
	virtual void OperateMouse(bool bMoveCursor) {};

	/// <summary>
	/// 選択中のボタンの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 選択中のボタン </returns>
	UFUNCTION(BlueprintCallable, Category = "OperateBaseWidget")
	UButtonBase* GetSelectButton();

	/// <summary>
	/// 決定したボタンの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 選択中のボタン </returns>
	UButtonBase* GetDecideButton();

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

	/// <summary>
	/// バーティカルボックスの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> バーティカルボックス </returns>
	UVerticalBox* GetVerticalBox();

	/// <summary>
	/// スクロールボックスの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> スクロールボックス </returns>
	UScrollBox* GetScrollBox();

	/// <summary>
	/// キーボード選択操作
	/// </summary>
	/// <param name="Direction"> 選択移動方向 </param>
	/// <returns></returns>
	void OperateKeyboardSelect(int32 Direction);

	/// <summary>
	/// マウス選択操作
	/// </summary>
	/// <param name="Index"> インデックス </param>
	/// <returns></returns>
	void OperateMouseSelect(int32 Index);

	/// <summary>
	/// サウンドを鳴らす
	/// </summary>
	/// <param name="Sound"> サウンド </param>
	/// <returns></returns>
	void PlaySound(USoundCue* Sound);

	/// <summary>
	/// 現在の操作の種類を変更する
	/// </summary>
	/// <param name="Type"> 操作の種類 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category="OperateBaseWidget")
	void ChangeCurrentOperateType(EOperateType Type);

protected:

	/// <summary>
	/// ボタン選択サウンド
	/// </summary>
	UPROPERTY(EditAnywhere, Category="OperateBaseWidget")
	USoundCue* SelectSound;

	/// <summary>
	/// ボタン決定サウンド
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "OperateBaseWidget")
	USoundCue* DecideSound;

	/// <summary>
	/// 戻るサウンド
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "OperateBaseWidget")
	USoundCue* ReturnSound;

	/// <summary>
	/// 更新サウンド
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "OperateBaseWidget")
	USoundCue* UpdateSound;

	/// <summary>
	/// ゲームスタートサウンド
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "OperateBaseWidget")
	USoundCue* GameStartSound;

	/// <summary>
	/// ウィジェットの種類
	/// </summary>
	UPROPERTY()
	EWidgetType WidgetType;

	/// <summary>
	/// 現在選択中のボタンインデックス
	/// </summary>
	int32 CurrentButtonIndex;

	/// <summary>
	/// ボタンリスト
	/// </summary>
	UPROPERTY(BlueprintReadOnly, Category = "OperateBaseWidget")
	TMap<int32, UButtonBase*> ButtonList;

	/// <summary>
	/// 決定したボタン
	/// </summary>
	UPROPERTY()
	UButtonBase* DecideButton;

	/// <summary>
	/// 現在の操作の種類
	/// </summary>
	EOperateType CurrentOperateType;

protected:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// バーティカルボックス
	/// </summary>
	const FName VerticalBoxName = "VerticalBox_Button";

	/// <summary>
	/// スクロールボックス
	/// </summary>
	const FName ScrollBoxName = "ScrollBox_Button";

	/// <summary>
	/// 下方向指定
	/// </summary>
	const int32 SelectDown = 1;

	/// <summary>
	/// 上方向指定
	/// </summary>
	const int32 SelectUp = -1;

	/// <summary>
	/// 最小インデックス
	/// </summary>
	const int32 MinIndex = 0;

	/// <summary>
	/// 0
	/// </summary>
	const int32 Zero = 0;

	/// <summary>
	/// 1
	/// </summary>
	const int32 One = 1;

	/// <summary>
	/// 透過率 透明
	/// </summary>
	const float Transparent = 0.0f;

	/// <summary>
	/// 透過率 不透明
	/// </summary>
	const float Opacity = 1.0f;
};
