﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TextBase.generated.h"

/// <summary>
/// テキストの種類
/// </summary>
UENUM(BlueprintType)
enum class ETextType : uint8 {
	None,          // なし
	Title,         // タイトルテキスト
	Guide,         // ガイドテキスト
	MaxTypeIndex
};

/// <summary>
/// テキストの色の種類
/// </summary>
UENUM(BlueprintType)
enum class ETextColorType : uint8 {
	Black,         // 黒
	White,         // 白
	Red,           // 赤
	Yellow,        // 黄
	MaxTypeIndex
};

/// <summary>
/// アニメーションの種類
/// </summary>
UENUM(BlueprintType)
enum class ETextAnimType : uint8 {
	None,          // なし
	Flashing,      // 点滅
	MaxTypeIndex
};

/// <summary>
/// テキストウィジェットクラス
/// </summary>
UCLASS()
class UTextBase : public UUserWidget
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// テキストの種類のセット
	/// </summary>
	/// <param name="Type"> テキストの種類 </param>
	/// <returns></returns>
	void SetTextType(ETextType Type);

	/// <summary>
	/// ウィジェットの種類の取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ウィジェットの種類 </returns>
	ETextType GetTextType();

	/// <summary>
	/// アニメーションの種類のセット
	/// </summary>
	/// <param name="Type"> アニメーションの種類 </param>
	/// <returns></returns>
	void SetTextAnimType(ETextAnimType Type);

	/// <summary>
	/// アニメーションの種類の取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> アニメーションの種類 </returns>
	ETextAnimType GetTextAnimType();

	/// <summary>
	/// テキストの設定
	/// </summary>
	/// <param name="Text"> テキスト </param>
	/// <returns></returns>
	void SetText(FText Text);

	/// <summary>
	/// テキストの色設定
	/// </summary>
	/// <param name="ColorType"> 色情報 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "TextBase")
	void SetupTextColor(ETextColorType ColorType);

	/// <summary>
	/// テキストのフォントサイズ設定
	/// </summary>
	/// <param name="FontSize"> フォントサイズ </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "TextBase")
	void SetupFontSize(int32 FontSize);

	/// <summary>
	/// アニメーションの開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "TextBase")
	void PlayTextAnimation();

	/// <summary>
	/// アニメーションの停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "TextBase")
	void StopTextAnimation();

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

private:

	/// <summary>
	/// テキストの色設定
	/// </summary>
	/// <param name="ColorType"> 色情報 </param>
	/// <returns></returns>
	virtual void SetupTextColor_Implementation(ETextColorType ColorType);

	/// <summary>
	/// テキストのフォントサイズ設定
	/// </summary>
	/// <param name="FontSize"> フォントサイズ </param>
	/// <returns></returns>
	virtual void SetupFontSize_Implementation(int32 FontSize);

	/// <summary>
	/// アニメーションの開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayTextAnimation_Implementation();

	/// <summary>
	/// アニメーションの停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void StopTextAnimation_Implementation();

protected:

	/// <summary>
	/// テキストの種類
	/// </summary>
	ETextType TextType;

	/// <summary>
	/// アニメーションの種類
	/// </summary>
	UPROPERTY(BlueprintReadOnly, Category="TextBase")
	ETextAnimType TextAnimType;

protected:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// テキストブロック
	/// </summary>
	const FName TextBlockName = "TextBlock_Text";
};
