﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/OperateBaseWidget/OptionBaseWidget/OptionBaseWidget.h"
#include "Audio/DataAssets/AudioVolume/AudioVolumeData.h"
#include "AudioMenuWidget.generated.h"

/// <summary>
/// オーディオメニューウィジェット
/// </summary>
UCLASS()
class UAudioMenuWidget : public UOptionBaseWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UAudioMenuWidget();

	/// <summary>
	/// ウィジェットの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupWidget() override;

	/// <summary>
	/// ウィジェット初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeWidget();

	/// <summary>
	/// オーディオボリュームの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupAudioVolume(FAudioVolumeData Volume);

	/// <summary>
	/// 設定後のオーディオボリュームを取得する
	/// </summary>
	/// <param name=""></param>
	/// <returns> オーディオボリューム </returns>
	FAudioVolumeData GetAfterSettingAudioVolume();

	/// <summary>
	/// 左方向へ移動処理 ボリューム調整
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void AdjustVolumeDown();

	/// <summary>
	/// 右方向へ移動処理 ボリューム調整
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void AdjustVolumeUp();

	/// <summary>
	/// 上方向へ移動
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressUp() override;

	/// <summary>
	/// 下方向へ移動
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressDown() override;

	/// <summary>
	/// マウス操作
	/// </summary>
	/// <param name="bMoveCursor"> マウスカーソル移動有無 </param>
	/// <returns></returns>
	void OperateMouse(bool bMoveCursor) override;

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

	/// <summary>
	/// キーボード選択操作
	/// </summary>
	/// <param name="Direction"> 選択移動方向 </param>
	/// <returns></returns>
	void OperateKeyboardSelect(int32 Direction);

	/// <summary>
	/// マウス選択操作
	/// </summary>
	/// <param name="Index"> インデックス </param>
	/// <returns></returns>
	void OperateMouseSelect(int32 Index);

	/// <summary>
	/// 音量を上げる
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION()
	void OnVolumeUp();

	/// <summary>
	/// 音量を下げる
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION()
	void OnVolumeDown();

private:

	/// <summary>
	/// 設定前の音量データ
	/// </summary>
	UPROPERTY()
	FAudioVolumeData AudioVolume;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// 効果音インデックス
	/// </summary>
	const int32 SeIndex = 0;

	/// <summary>
	/// BGMインデックス
	/// </summary>
	const int32 BgmIndex = 1;

	/// <summary>
	/// 音量調整値
	/// </summary>
	const float AdjustVolume = 0.01f;
};
