﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/OperateBaseWidget/Button/ButtonBase.h"
#include "LevelUIButton.generated.h"

/// <summary>
/// レベル配置ボタン
/// </summary>
UCLASS()
class ULevelUIButton : public UButtonBase
{
	GENERATED_BODY()
	
protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;
};
