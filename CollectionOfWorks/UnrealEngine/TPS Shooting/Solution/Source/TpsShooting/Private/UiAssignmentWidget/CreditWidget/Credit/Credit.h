﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/UiAssignmentWidget.h"
#include "UiAssignmentWidget/CreditWidget/CreditText/CreditText.h"
#include "Engine/DataTable.h"
#include "Credit.generated.h"

/// <summary>
/// データテーブル用構造体
/// </summary>
USTRUCT(BlueprintType)
struct FCreditData : public FTableRowBase {

	GENERATED_BODY()

public:

	/// <summary>
	/// クレジットテキスト
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString Text;
};

/// <summary>
/// クレジットウィンドウ
/// </summary>
UCLASS()
class UCredit : public UUiAssignmentWidget
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// クレジットテキストを設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupCreditText();

	/// <summary>
	/// スクロールアップ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Credit")
	void ScrollUp();

	/// <summary>
	/// スクロールダウン
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Credit")
	void ScrollDown();

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativePreConstruct() override;

private:

	/// <summary>
	/// クレジットテキスト生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UCreditText* CreateCreditText();

	/// <summary>
	/// スクロールボックスに子要素として設定
	/// </summary>
	/// <param name="Widget"> 子ウィジェット </param>
	/// <returns></returns>
	void AddChildCreditText(UWidget* Widget);

	/// <summary>
	/// スクロールアップ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ScrollUp_Implementation();

	/// <summary>
	/// スクロールダウン
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ScrollDown_Implementation();

public:

	/// <summary>
	/// クレジットテキストデータ
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "Credit")
	UDataTable* CreditTextTable;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// クレジットテキスト設定先
	/// </summary>
	const FName ScrollBoxCreditText = "ScrollBox_CreditText";
};
