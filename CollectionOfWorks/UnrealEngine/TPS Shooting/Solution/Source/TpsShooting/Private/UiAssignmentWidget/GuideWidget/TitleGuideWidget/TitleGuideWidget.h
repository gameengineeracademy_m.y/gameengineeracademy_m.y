﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/GuideWidget/GuideBaseWidget.h"
#include "TitleGuideWidget.generated.h"

/// <summary>
/// タイトルガイドウィジェット
/// </summary>
UCLASS()
class UTitleGuideWidget : public UGuideBaseWidget
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UTitleGuideWidget();

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;
};
