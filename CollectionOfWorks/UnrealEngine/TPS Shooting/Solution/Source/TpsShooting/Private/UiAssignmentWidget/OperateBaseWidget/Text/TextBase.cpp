﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UiAssignmentWidget/OperateBaseWidget/Text/TextBase.h"
#include "Components/TextBlock.h"

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTextBase::NativeConstruct() {

}

/// <summary>
/// テキストの種類のセット
/// </summary>
/// <param name="Type"> テキストの種類 </param>
/// <returns></returns>
void UTextBase::SetTextType(ETextType Type) {

	TextType = Type;
}

/// <summary>
/// ウィジェットの種類の取得
/// </summary>
/// <param name=""></param>
/// <returns> ウィジェットの種類 </returns>
ETextType UTextBase::GetTextType() {

	return TextType;
}

/// <summary>
/// アニメーションの種類のセット
/// </summary>
/// <param name="Type"> アニメーションの種類 </param>
/// <returns></returns>
void UTextBase::SetTextAnimType(ETextAnimType Type) {

	TextAnimType = Type;
}

/// <summary>
/// アニメーションの種類の取得
/// </summary>
/// <param name=""></param>
/// <returns> アニメーションの種類 </returns>
ETextAnimType UTextBase::GetTextAnimType() {

	return TextAnimType;
}

/// <summary>
/// テキストの設定
/// </summary>
/// <param name="Text"> テキスト </param>
/// <returns></returns>
void UTextBase::SetText(FText Text) {

	UTextBlock* TextBlock = Cast<UTextBlock>(GetWidgetFromName(TextBlockName));
	if (!IsValid(TextBlock)) {
		return;
	}
	TextBlock->SetText(Text);
}

/// <summary>
/// テキストの色設定
/// </summary>
/// <param name="ColorType"> 色情報 </param>
/// <returns></returns>
void UTextBase::SetupTextColor_Implementation(ETextColorType ColorType) {

}

/// <summary>
/// テキストのフォントサイズ設定
/// </summary>
/// <param name="FontSize"> フォントサイズ </param>
/// <returns></returns>
void UTextBase::SetupFontSize_Implementation(int32 FontSize) {

}

/// <summary>
/// アニメーションの開始
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTextBase::PlayTextAnimation_Implementation() {

}

/// <summary>
/// アニメーションの停止
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTextBase::StopTextAnimation_Implementation() {

}