﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/OperateBaseWidget/Button/ButtonBase.h"
#include "Components/Slider.h"
#include "VolumeButton.generated.h"


/// <summary>
/// ボリュームボタンの種類
/// </summary>
UENUM(BlueprintType)
enum class EVolumeButtonType : uint8 {
	None,
	SE,
	BGM,
	MaxTypeIndex
};

/// <summary>
/// 大きさ設定ボタン
/// </summary>
UCLASS()
class UVolumeButton : public UButtonBase
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// スライダーの値変更イベント設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupValueChanged();

	/// <summary>
	/// スライダーの表示設定を変更する
	/// </summary>
	/// <param name="VisibilityType"> 表示設定 </param>
	/// <returns></returns>
	void SetupSliderVisibility(ESlateVisibility VisibilityType);

	/// <summary>
	/// ボリューム値のセット
	/// </summary>
	/// <param name="Value"> ボリューム </param>
	/// <returns></returns>
	void SetVolume(float Value);

	/// <summary>
	/// ボリューム値の取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ボリューム値 </returns>
	float GetVolume();

	/// <summary>
	/// ボリュームボタンの種類を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ボリュームボタンの種類 </returns>
	EVolumeButtonType GetVolumeButtonType();

	/// <summary>
	/// BGMボリューム調整
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, Category = "VolumeButton")
	void AdjustBgmVolume();

	/// <summary>
	/// SEボリューム調整
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, Category = "VolumeButton")
	void AdjustSeVolume();

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativePreConstruct() override;

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

	/// <summary>
	/// ボリュームが変化した
	/// </summary>
	/// <param name="Value"> ボリューム </param>
	/// <returns></returns>
	UFUNCTION()
	void OnValueChanged(float Value);

private:

	/// <summary>
	/// ボリュームスライダーの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	USlider* GetVolumeSlider();

	/// <summary>
	/// ボリューム値のチェック
	/// </summary>
	/// <param name="Value"> ボリューム値 </param>
	/// <returns> ボリューム値 </returns>
	float CheckVolumeValue(float Value);

	/// <summary>
	/// テキストとゲージのボリューム設定
	/// </summary>
	/// <param name="Value"> ボリューム値 </param>
	/// <returns></returns>
	void SetupVolumeText(float Value);

	/// <summary>
	/// BGMボリューム調整
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void AdjustBgmVolume_Implementation();

	/// <summary>
	/// SEボリューム調整
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void AdjustSeVolume_Implementation();

protected:

	/// <summary>
	/// ボリュームボタンの種類
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "VolumeButton")
	EVolumeButtonType VolumeButtonType;

	/// <summary>
	/// ボリューム最大値
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "VolumeButton")
	float SliderMaxVolume;

	/// <summary>
	/// ボリューム最小値
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "VolumeButton")
	float SliderMinVolume;

	/// <summary>
	/// ボリューム
	/// </summary>
	UPROPERTY(BlueprintReadOnly, Category = "VolumeButton")
	float Volume;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// テキストブロック ボリューム(%)
	/// </summary>
	const FName TextBlockVolumeName = "TextBlock_VolumeText";

	/// <summary>
	/// ボリューム設定スライダー
	/// </summary>
	const FName SliderVolumeName = "Slider_Volume";

	/// <summary>
	/// 100
	/// </summary>
	const float Hundred = 100.0f;

	/// <summary>
	/// 最小値
	/// </summary>
	const float MinVolume = 0.0f;
};
