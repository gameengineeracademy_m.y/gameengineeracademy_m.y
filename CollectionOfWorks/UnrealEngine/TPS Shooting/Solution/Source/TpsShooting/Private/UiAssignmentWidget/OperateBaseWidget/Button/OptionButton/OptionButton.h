﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/OperateBaseWidget/Button/ButtonBase.h"
#include "OptionButton.generated.h"


/// <summary>
/// オプション用ボタンの種類
/// </summary>
UENUM()
enum class EOptionButtonType : uint8 {
	None,          // 設定なし
	Resume,        // ゲーム再開
	OperateList,   // 操作一覧
	AudioMenu,     // オーディオメニュー
	ReturnTitle,   // タイトルへ戻る
	MaxTypeIndex
};

/// <summary>
/// オプション用ボタン
/// </summary>
UCLASS()
class UOptionButton : public UButtonBase
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// オプションボタンの種類の取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オプションボタンの種類 </returns>
	EOptionButtonType GetOptionButtonType();

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

protected:

	/// <summary>
	/// オプションボタンの種類
	/// </summary>
	UPROPERTY(EditAnywhere, Category="OptionButton")
	EOptionButtonType OptionButtonType;
};
