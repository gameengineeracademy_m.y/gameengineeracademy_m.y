﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/OperateBaseWidget/OptionBaseWidget/OptionBaseWidget.h"
#include "OperateListWidget.generated.h"

/// <summary>
/// 操作一覧ウィジェット
/// </summary>
UCLASS()
class UOperateListWidget : public UOptionBaseWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UOperateListWidget();

	/// <summary>
	/// ウィジェットの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupWidget() override;

protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;
};
