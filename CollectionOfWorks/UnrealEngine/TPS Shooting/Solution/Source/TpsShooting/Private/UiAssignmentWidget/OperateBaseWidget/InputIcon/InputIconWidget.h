﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "InputIconWidget.generated.h"

/// <summary>
/// 入力アイコンウィジェット
/// </summary>
UCLASS()
class UInputIconWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativePreConstruct() override;

private:

	/// <summary>
	/// テキストブロックの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> テキストブロック </returns>
	UTextBlock* GetTextBlock();

protected:

	/// <summary>
	/// 表示テキスト
	/// </summary>
	UPROPERTY(EditAnywhere, Category = "InputIconWidget")
	FText DisplayText;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// テキストブロック ボタンテキスト
	/// </summary>
	const FName TextBlockName = "TextBlock_Text";
};
