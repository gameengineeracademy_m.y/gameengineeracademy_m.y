﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UiAssignmentWidget/OperateBaseWidget/Button/OptionButton/OptionButton.h"


/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UOptionButton::NativeConstruct() {

	Super::NativeConstruct();
}

/// <summary>
/// オプションボタンの種類の取得
/// </summary>
/// <param name=""></param>
/// <returns> オプションボタンの種類 </returns>
EOptionButtonType UOptionButton::GetOptionButtonType() {

	return OptionButtonType;
}