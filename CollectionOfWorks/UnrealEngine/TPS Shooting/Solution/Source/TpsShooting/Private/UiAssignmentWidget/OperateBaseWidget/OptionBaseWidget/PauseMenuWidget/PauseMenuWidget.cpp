﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UiAssignmentWidget/OperateBaseWidget/OptionBaseWidget/PauseMenuWidget/PauseMenuWidget.h"
#include "UiAssignmentWidget/OperateBaseWidget/Button/OptionButton/OptionButton.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UPauseMenuWidget::UPauseMenuWidget() {

}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UPauseMenuWidget::NativeConstruct() {

	Super::NativeConstruct();
}

/// <summary>
/// 決定したボタンのオプションボタンの種類を取得
/// </summary>
/// <param name=""></param>
/// <returns> オプションボタンの種類 </returns>
EOptionButtonType UPauseMenuWidget::GetDecideOptionButtonType() {

	UOptionButton* OptionButton = Cast<UOptionButton>(GetDecideButton());
	if (!IsValid(OptionButton)) {
		return EOptionButtonType::None;
	}

	return OptionButton->GetOptionButtonType();
}