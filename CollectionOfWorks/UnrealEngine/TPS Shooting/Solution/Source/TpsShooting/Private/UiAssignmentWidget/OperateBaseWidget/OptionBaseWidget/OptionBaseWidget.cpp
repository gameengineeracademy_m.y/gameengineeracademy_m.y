﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UiAssignmentWidget/OperateBaseWidget/OptionBaseWidget/OptionBaseWidget.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UOptionBaseWidget::UOptionBaseWidget() {

}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UOptionBaseWidget::NativeConstruct() {

	Super::NativeConstruct();

	InitializeWidget();
}

/// <summary>
/// ウィジェット設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UOptionBaseWidget::SetupWidget() {

	//-----------------------------------------------
	//タイトル画面上のボタンを取得
	//ボタンの初期化処理とイベントデリゲート処理
	//-----------------------------------------------

	UVerticalBox* VerticalBox = GetVerticalBox();
	if (!IsValid(VerticalBox)) {
		return;
	}

	TArray<UWidget*> ChildrenArray = VerticalBox->GetAllChildren();
	int32 Index = MinIndex;
	for (UWidget* Widget : ChildrenArray) {

		UOptionButton* Button = Cast<UOptionButton>(Widget);
		if (!IsValid(Button)) {
			continue;
		}
		Button->StopButtonAnimation();
		Button->SetupHoverAndPressEvent();
		EButtonType Type = Button->GetButtonType();

		ButtonList.Add(Index, Button);
		++Index;
	}

	if (ButtonList.Num() <= Zero) {
		return;
	}
	// 先頭メニューを選択状態にする
	CurrentButtonIndex = MinIndex;
	UOptionButton* Button = Cast<UOptionButton>(GetSelectButton());
	if (!IsValid(Button)) {
		return;
	}
	Button->PlayButtonAnimation();
}

/// <summary>
/// ウィジェット初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UOptionBaseWidget::InitializeWidget() {

	ResetOperation();
	CurrentButtonIndex = MinIndex;

	for (auto ButtonData : ButtonList) {

		UOptionButton* Button = Cast<UOptionButton>(ButtonData.Value);
		if (!IsValid(Button)) {
			continue;
		}
		Button->StopButtonAnimation();
	}

	UOptionButton* Button = Cast<UOptionButton>(GetSelectButton());
	if (!IsValid(Button)) {
		return;
	}
	Button->PlayButtonAnimation();
}

/// <summary>
/// 上方向へ移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UOptionBaseWidget::PressUp() {

	OperateKeyboardSelect(SelectUp);
}

/// <summary>
/// 下方向へ移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UOptionBaseWidget::PressDown() {

	OperateKeyboardSelect(SelectDown);
}

/// <summary>
/// 右方向へ移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UOptionBaseWidget::PressRight() {

	OperateKeyboardSelect(SelectDown);
}

/// <summary>
/// 左方向へ移動
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UOptionBaseWidget::PressLeft() {

	OperateKeyboardSelect(SelectUp);
}

/// <summary>
/// キーボード選択操作
/// </summary>
/// <param name="Direction"> 選択移動方向 </param>
/// <returns></returns>
void UOptionBaseWidget::OperateKeyboardSelect(int32 Direction) {

	//---------------------------------------
	//現在選択中のボタンを未選択状態にする
	//選択中インデックスを移動方向分加算
	//次に選択するボタンを選択状態にする
	//---------------------------------------

	PlaySound(SelectSound);

	UOptionButton* CurrentButton = Cast<UOptionButton>(GetSelectButton());
	if (!IsValid(CurrentButton)) {
		return;
	}
	CurrentButton->StopButtonAnimation();

	UOperateBaseWidget::OperateKeyboardSelect(Direction);

	CurrentButton = nullptr;
	CurrentButton = Cast<UOptionButton>(GetSelectButton());
	if (!IsValid(CurrentButton)) {
		return;
	}
	CurrentButton->PlayButtonAnimation();
}

/// <summary>
/// マウス選択操作
/// </summary>
/// <param name="Index"> インデックス </param>
/// <returns></returns>
void UOptionBaseWidget::OperateMouseSelect(int32 Index) {

	//---------------------------------------
	//現在選択中のボタンを未選択状態にする
	//選択中インデックスを移動方向分加算
	//次に選択するボタンを選択状態にする
	//---------------------------------------

	PlaySound(SelectSound);

	UOptionButton* CurrentButton = Cast<UOptionButton>(GetSelectButton());
	if (!IsValid(CurrentButton)) {
		return;
	}
	CurrentButton->StopButtonAnimation();
	CurrentButton->ResetSelectMouseCursor();

	UOperateBaseWidget::OperateMouseSelect(Index);

	CurrentButton = nullptr;
	CurrentButton = Cast<UOptionButton>(GetSelectButton());
	if (!IsValid(CurrentButton)) {
		return;
	}
	CurrentButton->PlayButtonAnimation();
}

/// <summary>
/// 決定操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UOptionBaseWidget::Decide() {

	// 現在選択中のボタンをセット
	PressDecide();

	
	UOptionButton* Button = Cast<UOptionButton>(GetDecideButton());
	if (!IsValid(Button)) {
		return;
	}

	EButtonType ButtonType = Button->GetButtonType();
	switch (ButtonType) {
	case EButtonType::Option: {

		PlaySound(UpdateSound);
		break;
	}
	default: {

		PlaySound(DecideSound);
		break;
	}
	}

	// ボタンアニメーションの実行
	Button->PlayPressButtonAnimation();
}

/// <summary>
/// マウス操作
/// </summary>
/// <param name="bMoveCursor"> マウスカーソル移動有無 </param>
/// <returns></returns>
void UOptionBaseWidget::OperateMouse(bool bMoveCursor) {


	for (auto ButtonData : ButtonList) {

		int32 ButtonIndex = ButtonData.Key;
		UOptionButton* Button = Cast<UOptionButton>(ButtonData.Value);
		if (!IsValid(Button)) {
			continue;
		}

		if (Button->IsPressed()) {
			// 選択したボタンを一旦ホバー状態にしておく
			Button->SwitchSelectMouseCursor();
			Decide();
			return;
		}

		//----------------------------------------
		// 別ボタンへのホバー直後は強制的に
		// ボタンを切り替える
		// それ以外はマウスカーソルが動いたか
		// どうかで処理の実行有無を決める
		//----------------------------------------
		if (Button->IsSwitchButton()) {

			// ホバー開始フラグを降ろしておく
			Button->ResetSwitchStartHovered();

			// 対象のボタンが現在選択されているボタンと同じかどうか
			if (ButtonIndex == CurrentButtonIndex) {
				continue;
			}
			OperateMouseSelect(ButtonIndex);
		}
		else {

			// マウスが移動したかどうか
			if (!bMoveCursor) {
				continue;
			}
			// 対象のボタンが選択(ホバー)されているかどうか
			if (!Button->IsSelected()) {
				continue;
			}
			// 対象のボタンが現在選択されているボタンと同じかどうか
			if (ButtonIndex == CurrentButtonIndex) {
				continue;
			}
			OperateMouseSelect(ButtonIndex);
		}
	}
}