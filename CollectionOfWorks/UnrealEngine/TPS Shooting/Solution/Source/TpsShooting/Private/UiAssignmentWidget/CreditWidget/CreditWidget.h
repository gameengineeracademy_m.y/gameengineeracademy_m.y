﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/OperateBaseWidget/OperateBaseWidget.h"
#include "UiAssignmentWidget/CreditWidget/Credit/Credit.h"
#include "CreditWidget.generated.h"


/// <summary>
/// クレジットウィジェット
/// </summary>
UCLASS()
class UCreditWidget : public UOperateBaseWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UCreditWidget();

	/// <summary>
	/// クレジットの設定
	/// </summary>
	/// <param name="Type"> ウィジェットの種類 </param>
	/// <returns></returns>
	void SetupCredit(EWidgetType Type);

	/// <summary>
	/// 上方向への操作
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ScrollUp();

	/// <summary>
	/// 下方向への操作
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ScrollDown();

	/// <summary>
	/// 戻る操作
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Return();


protected:

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

private:

	/// <summary>
	/// クレジットの生成
	/// </summary>
	/// <param name=""></param>
	/// <returns> クレジット </returns>
	UCredit* CreateCredit();

	/// <summary>
	/// クレジットをキャンバスに子要素として設定
	/// </summary>
	/// <param name="Widget"> 子ウィジェット </param>
	/// <returns></returns>
	void AddChildCredit(UWidget* Widget);

private:

	/// <summary>
	/// クレジット
	/// </summary>
	UPROPERTY()
	UCredit* Credit;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// クレジット設定先
	/// </summary>
	const FName CanvasPanelName = "CanvasPanel_Object";
};
