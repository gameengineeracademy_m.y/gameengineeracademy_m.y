﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Battle/Enum/CommonEnum.h"
#include "UiAssignmentWidget.generated.h"


/// <summary>
/// ウィジェット基底クラス
/// </summary>
UCLASS()
class UUiAssignmentWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// ウィジェット表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "UiAssignmentWidget")
	void Open();

	/// <summary>
	/// ウィジェット終了処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "UiAssignmentWidget")
	void Close();

	/// <summary>
	/// ウィジェット非表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "UiAssignmentWidget")
	void Closed();

	/// <summary>
	/// ナビゲーションのOFF設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupNavigationOFF();

	/// <summary>
	/// アニメーション終了フラグ初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetAnimFlag();

	/// <summary>
	/// アニメーション終了したかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:終了済み, false:実行中 </returns>
	bool IsAnimFinish();

	/// <summary>
	/// 表示有無フラグ初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ResetNowDisplay();

	/// <summary>
	/// 表示中かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:表示中 </returns>
	bool IsDisplay();

private:

	/// <summary>
	/// ウィジェット表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void Open_Implementation();

	/// <summary>
	/// ウィジェット終了処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void Close_Implementation();

	/// <summary>
	/// ウィジェット非表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void Closed_Implementation();

public:

	/// <summary>
	/// アニメーション終了フラグ
	/// </summary>
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "UiAssignmentWidget")
	bool bAnimFinishFlag;

private:

	/// <summary>
	/// 表示有無フラグ
	/// </summary>
	bool bNowDisplay;

protected:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// Zオーダー
	/// </summary>
	const int ZOrder = 0;

	/// <summary>
	/// 透明
	/// </summary>
	const float Transparent = 0.0f;

	/// <summary>
	/// 不透明
	/// </summary>
	const float Opacity = 1.0f;
};
