﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "Boot/Controller/BootController.h"
#include "BootLevel.generated.h"


/// <summary>
/// レベル実行フェーズの種類
/// </summary>
UENUM()
enum class EBootPhaseType {
	FadeIn,          // フェードインフェーズ
	TextAnimation,   // テキストアニメーションフェーズ
	FadeOut,         // フェードアウトフェーズ
	ChangeLevel,     // レベル切替フェーズ
	Finish,          // 終了処理フェーズ
	MaxTypeIndex
};

/// <summary>
/// ブートレベル
/// </summary>
UCLASS()
class ABootLevel : public ALevelScriptActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ABootLevel();

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

private:

	/// <summary>
	/// 現在のフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentPhase(EBootPhaseType PhaseType);

private:

	/// <summary>
	/// ブートコントローラ
	/// </summary>
	UPROPERTY()
	ABootController* BootController;

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	EBootPhaseType CurrentPhase;
};
