﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Boot/Maps/BootLevel.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ABootLevel::ABootLevel()
	: BootController(nullptr)
	, CurrentPhase(EBootPhaseType::FadeIn) {

	// Tickイベントの有効化
	PrimaryActorTick.bCanEverTick = true;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootLevel::BeginPlay() {

	Super::BeginPlay();

	UKismetSystemLibrary::ExecuteConsoleCommand(GetWorld(), "r.SetRes 1920x1080w");

	//---------------------------------------
	//変数の初期化処理
	//---------------------------------------
	// 現在のフェーズ
	CurrentPhase = EBootPhaseType::FadeIn;

	//---------------------------------------
	// 各ウィジェットの表示前準備処理
	// 各ウィジェットの表示処理
	//---------------------------------------
	// ブートコントローラを取得
	BootController = Cast<ABootController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(BootController)) {
		return;
	}
	BootController->PrepareOpenWidget();
	BootController->OpenWidget();

	FLatentActionInfo LatentActionInfo;
	UGameplayStatics::LoadStreamLevel(GetWorld(), TEXT("TitleLevel"), false, false, LatentActionInfo);
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ABootLevel::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

	switch (CurrentPhase) {
	case EBootPhaseType::FadeIn: {
		//--------------------------
		//フェードインフェーズ
		//--------------------------
		// ブートコントローラ、フェードウィジェットが
		// 見つからない場合は強制的に次のフェーズに進む
		if (IsValid(BootController)) {
			UFadeWidget* FadeWidget = BootController->GetFadeWidget();
			if (IsValid(FadeWidget)) {
				// フェードイン実行
				FadeWidget->PlayFadeInAnimation();
			}
		}

		// 現在のフェーズを「アニメーション」フェーズに変更
		ChangeCurrentPhase(EBootPhaseType::TextAnimation);
		break;
	}
	case EBootPhaseType::TextAnimation: {
		//--------------------------
		//アニメーションフェーズ
		//※フェードインが終了するまで待機
		//--------------------------
		// ブートコントローラ、各ウィジェットが
		// 見つからない場合は強制的に次のフェーズに進む
		if (IsValid(BootController)) {

			bool IsFinish = true;
			UFadeWidget* FadeWidget = BootController->GetFadeWidget();
			if (IsValid(FadeWidget)) {
				IsFinish = FadeWidget->IsAnimFinish();
			}
			if (!IsFinish) {
				break;
			}
			// フェードウィジェットのフラグリセット
			FadeWidget->ResetAnimFlag();

			UBootWidget* BootWidget = BootController->GetBootWidget();
			if (IsValid(BootWidget)) {
				// テキストアニメーション開始
				BootWidget->PlayTextAnimation();
			}
		}

		// 現在のフェーズを「フェードアウト」フェーズに変更
		ChangeCurrentPhase(EBootPhaseType::FadeOut);
		break;
	}
	case EBootPhaseType::FadeOut: {
		//--------------------------
		//フェードアウトフェーズ
		//※アニメーションが終了するまで待機
		//--------------------------
		// ブートコントローラ、各ウィジェットが
		// 見つからない場合は強制的に次のフェーズに進む
		if (IsValid(BootController)) {

			bool IsFinish = true;
			UBootWidget* BootWidget = BootController->GetBootWidget();
			if (IsValid(BootWidget)) {
				IsFinish = BootWidget->IsAnimFinish();
			}

			if (!IsFinish) {
				break;
			}
			// ブートウィジェットのフラグリセット
			BootWidget->ResetAnimFlag();

			UFadeWidget* FadeWidget = BootController->GetFadeWidget();
			if (IsValid(FadeWidget)) {
				// フェードアウト実行
				FadeWidget->PlayFadeOutAnimation();
			}
		}

		// 現在のフェーズを「レベル切替」フェーズに変更
		ChangeCurrentPhase(EBootPhaseType::ChangeLevel);
		break;
	}
	case EBootPhaseType::ChangeLevel: {
		//--------------------------
		//レベル切替フェーズ
		//※フェードアウトが終了するまで待機
		//--------------------------
		// ブートコントローラ、フェードウィジェットが
		// 見つからない場合は強制的に次のフェーズに進む
		if (IsValid(BootController)) {
			bool IsFinish = true;
			UFadeWidget* FadeWidget = BootController->GetFadeWidget();
			if (IsValid(FadeWidget)) {
				IsFinish = FadeWidget->IsAnimFinish();
			}

			if (!IsFinish) {
				break;
			}
			// フェードウィジェットのフラグリセット
			FadeWidget->ResetAnimFlag();

			//--------------------------
			//ウィジェットを非表示にする
			//--------------------------
			BootController->PrepareCloseWidget();
			BootController->CloseWidget();
		}

		//--------------------------
		// レベルを変更する
		//--------------------------
		UGameplayStatics::OpenLevel(GetWorld(), TEXT("TitleLevel"), true);
		// 現在のフェーズを「終了処理」に変更
		ChangeCurrentPhase(EBootPhaseType::Finish);
		break;
	}
	case EBootPhaseType::Finish: {
		//--------------------------
		//終了処理フェーズ
		//--------------------------
		break;
	}
	}
}

/// <summary>
/// 現在のフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ABootLevel::ChangeCurrentPhase(EBootPhaseType PhaseType) {

	CurrentPhase = PhaseType;
}