﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget/UiAssignmentWidget.h"
#include "BootWidget.generated.h"

/// <summary>
/// ブートレベルウィジェット
/// </summary>
UCLASS()
class UBootWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UBootWidget();

	/// <summary>
	/// 表示文字を透明にしておく
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BootWidget")
	void PrepareRenderOpacityTextBlock();

	/// <summary>
	/// 表示文字のアニメーションを開始する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BootWidget")
	void PlayTextAnimation();

private:

	/// <summary>
	/// 表示文字を透明にしておく
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PrepareRenderOpacityTextBlock_Implementation();

	/// <summary>
	/// 表示文字のアニメーションを開始する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayTextAnimation_Implementation();
};
