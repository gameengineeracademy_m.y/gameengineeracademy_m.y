﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Boot/Controller/BootController.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ABootController::ABootController() {

}

/// <summary>
/// SetupinputComponent
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootController::SetupInputComponent() {

	Super::SetupInputComponent();
}

/// <summary>
/// 各ウィジェットの表示準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootController::PrepareOpenWidget() {

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABootUiManager* UiManager = Cast<ABootUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 各ウィジェットの表示準備処理
	UiManager->PrepareOpenWidget();
}

/// <summary>
/// 各ウィジェットの表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootController::OpenWidget() {

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABootUiManager* UiManager = Cast<ABootUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 各ウィジェットの表示処理
	UiManager->OpenWidget();
}

/// <summary>
/// 各ウィジェットの非表示準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootController::PrepareCloseWidget() {

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABootUiManager* UiManager = Cast<ABootUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 各ウィジェットの非表示準備処理
	UiManager->PrepareCloseWidget();
}

/// <summary>
/// 各ウィジェットの非表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootController::CloseWidget() {

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABootUiManager* UiManager = Cast<ABootUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 非表示処理実行
	UiManager->CloseWidget();
}

/// <summary>
/// ブートウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UBootWidget* ABootController::GetBootWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABootUiManager* UiManager = Cast<ABootUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}
	// ブートウィジェット取得
	UBootWidget* BootWidget = UiManager->GetBootWidget();
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	return BootWidget;
}

/// <summary>
/// フェードウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UFadeWidget* ABootController::GetFadeWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABootUiManager* UiManager = Cast<ABootUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}
	// フェードウィジェット取得
	UFadeWidget* FadeWidget = UiManager->GetFadeWidget();
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	return FadeWidget;
}