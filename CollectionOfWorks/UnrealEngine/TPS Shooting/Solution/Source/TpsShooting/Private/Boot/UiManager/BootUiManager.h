﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Boot/Widgets/BootWidget.h"
#include "UiAssignmentWidget/FadeWidget/FadeWidget.h"
#include "BootUiManager.generated.h"

/// <summary>
/// ブートレベルのUIマネージャー処理
/// </summary>
UCLASS()
class ABootUiManager : public AHUD
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ABootUiManager();

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PreInitializeComponents() override;

	/// <summary>
	/// 各ウィジェット表示準備処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PrepareOpenWidget();

	/// <summary>
	/// 各ウィジェット表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OpenWidget();

	/// <summary>
	/// 各ウィジェット非表示準備処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PrepareCloseWidget();

	/// <summary>
	/// 各ウィジェット非表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CloseWidget();

	/// <summary>
	/// ブートウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UBootWidget* GetBootWidget();

	/// <summary>
	/// フェードウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFadeWidget* GetFadeWidget();


private:

	/// <summary>
	/// ブートウィジェットクラス
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	TSubclassOf<UBootWidget> BootWidgetClass;

	/// <summary>
	/// ブートウィジェット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	UBootWidget* BootWidget;

	/// <summary>
	/// フェードウィジェットクラス
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	TSubclassOf<UFadeWidget> FadeWidgetClass;

	/// <summary>
	/// フェードウィジェット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	UFadeWidget* FadeWidget;

private:
	//-------------------------------------
	// 以下、定数宣言
	//-------------------------------------
	/// <summary>
	/// 不透明
	/// </summary>
	const float Opacity = 1.0f;

	/// <summary>
	/// 透明
	/// </summary>
	const float Transparent = 0.0f;
};
