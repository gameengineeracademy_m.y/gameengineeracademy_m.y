﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Boot/UiManager/BootUiManager.h"
#include "BootController.generated.h"

/// <summary>
/// ブートレベル用コントローラ
/// </summary>
UCLASS()
class ABootController : public APlayerController
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ABootController();

	/// <summary>
	/// 各ウィジェットの表示準備処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PrepareOpenWidget();

	/// <summary>
	/// 各ウィジェットの表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OpenWidget();

	/// <summary>
	/// 各ウィジェットの非表示準備処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PrepareCloseWidget();

	/// <summary>
	/// 各ウィジェットの非表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CloseWidget();

	/// <summary>
	/// ブートウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UBootWidget* GetBootWidget();

	/// <summary>
	/// フェードウィジェットの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFadeWidget* GetFadeWidget();


protected:

	/// <summary>
	/// SetupinputComponent
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void SetupInputComponent() override;
};
