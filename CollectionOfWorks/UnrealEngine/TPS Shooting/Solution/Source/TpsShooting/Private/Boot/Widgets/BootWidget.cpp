﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Boot/Widgets/BootWidget.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UBootWidget::UBootWidget() {

}

/// <summary>
/// 表示文字のアニメーションを開始する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBootWidget::PlayTextAnimation_Implementation() {

}

/// <summary>
/// 表示文字を透明にしておく
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBootWidget::PrepareRenderOpacityTextBlock_Implementation() {

}