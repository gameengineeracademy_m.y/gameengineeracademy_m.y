﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Boot/UiManager/BootUiManager.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ABootUiManager::ABootUiManager() 
	: BootWidgetClass()
	, BootWidget(nullptr)
	, FadeWidgetClass()
	, FadeWidget(nullptr) {

}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootUiManager::PreInitializeComponents() {

	//-----------------------------------------------------
	// 各ウィジェットパスを「DefaultGame.ini」から取得し、
	// 各ウィジェットを生成する
	//-----------------------------------------------------
	// ブートウィジェット
	FString BootWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("BootWidgetPath"), BootWidgetPath, GGameIni);
	
	BootWidgetClass = TSoftClassPtr<UBootWidget>(FSoftObjectPath(*BootWidgetPath)).LoadSynchronous();
	if (!BootWidgetClass) {
		return;
	}
	BootWidget = CreateWidget<UBootWidget>(GetWorld(), BootWidgetClass);
	
	// フェードウィジェット
	FString FadeWidgetPath;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("FadeWidgetPath"), FadeWidgetPath, GGameIni);
	
	FadeWidgetClass = TSoftClassPtr<UFadeWidget>(FSoftObjectPath(*FadeWidgetPath)).LoadSynchronous();
	if (!FadeWidgetClass) {
		return;
	}
	FadeWidget = CreateWidget<UFadeWidget>(GetWorld(), FadeWidgetClass);
}

/// <summary>
/// 各ウィジェット表示準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootUiManager::PrepareOpenWidget() {

	//-----------------------------------------------------
	// 各ウィジェットのアニメーションフラグ・不透明度の初期化
	//-----------------------------------------------------
	// ブートウィジェット
	if (!IsValid(BootWidget)) {
		return;
	}
	BootWidget->ResetAnimFlag();

	// テキストを透明にしておく
	BootWidget->PrepareRenderOpacityTextBlock();

	// フェードウィジェット
	if (!IsValid(FadeWidget)) {
		return;
	}
	FadeWidget->ResetAnimFlag();

	// イメージを不透明にしておく
	UImage* Image = Cast<UImage>(FadeWidget->GetWidgetFromName("Image_BlackBoard"));
	if (!IsValid(Image)) {
		return;
	}
	Image->SetRenderOpacity(Opacity);
}

/// <summary>
/// 各ウィジェット表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootUiManager::OpenWidget() {

	//-----------------------------------------------------
	// 各ウィジェットを表示
	//-----------------------------------------------------
	// ブートウィジェット
	if (!IsValid(BootWidget)) {
		return;
	}
	BootWidget->Open();

	// フェードウィジェット
	if (!IsValid(FadeWidget)) {
		return;
	}
	FadeWidget->Open();
}

/// <summary>
/// 各ウィジェット非表示準備処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootUiManager::PrepareCloseWidget() {

	//-----------------------------------------------------
	// 各ウィジェットを非表示前準備処理
	//-----------------------------------------------------
	// ブートウィジェット
	if (!IsValid(BootWidget)) {
		return;
	}
	BootWidget->Close();

	// フェードウィジェット
	if (!IsValid(FadeWidget)) {
		return;
	}
	FadeWidget->Close();

	// フェードウィジェットを透明にしておく
	UImage* Image = Cast<UImage>(FadeWidget->GetWidgetFromName("Image_BlackBoard"));
	if (!IsValid(Image)) {
		return;
	}
	Image->SetRenderOpacity(Transparent);
}

/// <summary>
/// 各ウィジェット非表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootUiManager::CloseWidget() {

	//-----------------------------------------------------
	// 各ウィジェットを非表示
	// ※フェードウィジェットは非表示にしない
	//-----------------------------------------------------
	// ブートウィジェット
	if (!IsValid(BootWidget)) {
		return;
	}
	BootWidget->Closed();
}

/// <summary>
/// ブートウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UBootWidget* ABootUiManager::GetBootWidget() {

	if (!IsValid(BootWidget)) {
		return nullptr;
	}

	return BootWidget;
}

/// <summary>
/// フェードウィジェットの取得
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UFadeWidget* ABootUiManager::GetFadeWidget() {

	if (!IsValid(FadeWidget)) {
		return nullptr;
	}

	return FadeWidget;
}