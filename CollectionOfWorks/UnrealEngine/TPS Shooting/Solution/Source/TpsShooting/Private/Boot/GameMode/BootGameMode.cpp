﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Boot/GameMode/BootGameMode.h"
#include "Boot/Controller/BootController.h"
#include "Boot/UiManager/BootUiManager.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ABootGameMode::ABootGameMode() {

	// Tickイベントの無効化
	PrimaryActorTick.bCanEverTick = false;

	HUDClass = ABootUiManager::StaticClass();
	PlayerControllerClass = ABootController::StaticClass();
	DefaultPawnClass = nullptr;
}

/// <summary>
/// 生成後実行イベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootGameMode::BeginPlay() {


}