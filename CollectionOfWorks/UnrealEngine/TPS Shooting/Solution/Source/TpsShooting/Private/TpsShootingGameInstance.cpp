﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TpsShootingGameInstance.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UTpsShootingGameInstance::UTpsShootingGameInstance()
	: CurrentLevel(ELevelType::None)
	, NextTransLevel(ELevelType::None) {

}

/// <summary>
/// OnStart
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTpsShootingGameInstance::OnStart() {

	CurrentLevel = ELevelType::None;
	NextTransLevel = ELevelType::Tutorial;
}

/// <summary>
/// 現在のレベルのセット
/// </summary>
/// <param name="LevelType"> レベルの種類 </param>
/// <returns></returns>
void UTpsShootingGameInstance::SetCurrentLevel(ELevelType LevelType) {

	CurrentLevel = LevelType;
}

/// <summary>
/// 現在のレベルを取得
/// </summary>
/// <param name=""></param>
/// <returns> レベルの種類 </returns>
ELevelType UTpsShootingGameInstance::GetCurrentLevel() {

	return CurrentLevel;
}

/// <summary>
/// 次に遷移するレベルのセット
/// </summary>
/// <param name="LevelType"> レベルの種類 </param>
/// <returns></returns>
void UTpsShootingGameInstance::SetNextTransLevel(ELevelType LevelType) {

	NextTransLevel = LevelType;
}

/// <summary>
/// 次に遷移するレベルの取得
/// </summary>
/// <param name=""></param>
/// <returns> レベルの種類 </returns>
ELevelType UTpsShootingGameInstance::GetNextTransLevel() {

	return NextTransLevel;
}