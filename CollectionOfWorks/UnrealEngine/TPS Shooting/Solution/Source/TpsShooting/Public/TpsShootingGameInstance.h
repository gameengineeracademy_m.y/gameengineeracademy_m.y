﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TpsShootingGameInstance.generated.h"


/// <summary>
/// レベルの種類
/// </summary>
UENUM(BlueprintType)
enum class ELevelType : uint8 {
	None,
	Title,
	Tutorial,
	MainField,
	MaxIndex
};

/// <summary>
/// ゲームインスタンス
/// </summary>
UCLASS()
class TPSSHOOTING_API UTpsShootingGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UTpsShootingGameInstance();

	/// <summary>
	/// 現在のレベルのセット
	/// </summary>
	/// <param name="LevelType"> レベルの種類 </param>
	/// <returns></returns>
	void SetCurrentLevel(ELevelType LevelType);

	/// <summary>
	/// 現在のレベルを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> レベルの種類 </returns>
	ELevelType GetCurrentLevel();

	/// <summary>
	/// 次に遷移するレベルのセット
	/// </summary>
	/// <param name="LevelType"> レベルの種類 </param>
	/// <returns></returns>
	void SetNextTransLevel(ELevelType LevelType);

	/// <summary>
	/// 次に遷移するレベルの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> レベルの種類 </returns>
	ELevelType GetNextTransLevel();

protected:

	/// <summary>
	/// OnStart
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnStart() override;

private:

	/// <summary>
	/// 現在のレベル
	/// </summary>
	ELevelType CurrentLevel;

	/// <summary>
	/// 次に遷移するレベル
	/// </summary>
	ELevelType NextTransLevel;
};
