﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "UiAssignmentGameInstance.generated.h"

/// <summary>
/// UIゲームインスタンス情報
/// </summary>
UCLASS()
class BECOMINGRICH_API UUiAssignmentGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UUiAssignmentGameInstance();

	/// <summary>
	/// FadeWidgetのパスを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> パス </returns>
	FString GetFadeWidgetPath();

	/// <summary>
	/// BootWidgetのパスを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> パス </returns>
	FString GetBootWidgetPath();

	/// <summary>
	/// TitleWidgetのパスを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> パス </returns>
	FString GetTitleWidgetPath();

	/// <summary>
	/// BattleWidgetのパスを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> パス </returns>
	FString GetBattleWidgetPath();

	/// <summary>
	/// ResultWidgetのパスを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> パス </returns>
	FString GetResultWidgetPath();

	/// <summary>
	/// CreditWidgetのパスを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> パス </returns>
	FString GetCreditWidgetPath();

	/// <summary>
	/// DiceWidgetのパスを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> パス </returns>
	FString GetDiceWidgetPath();

	/// <summary>
	/// DialogWidgetのパスを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> パス </returns>
	FString GetDialogWidgetPath();


private:

	/// <summary>
	/// FadeWidgetパス
	/// </summary>
	FString FadeWidgetPath;

	/// <summary>
	/// BootWidgetパス
	/// </summary>
	FString BootWidgetPath;

	/// <summary>
	/// TitleWidgetパス
	/// </summary>
	FString TitleWidgetPath;

	/// <summary>
	/// BattleWidgetパス
	/// </summary>
	FString BattleWidgetPath;

	/// <summary>
	/// ResultWidgetパス
	/// </summary>
	FString ResultWidgetPath;

	/// <summary>
	/// CreditWidgetパス
	/// </summary>
	FString CreditWidgetPath;

	/// <summary>
	/// DiceWidgetパス
	/// </summary>
	FString DiceWidgetPath;

	/// <summary>
	/// DialogWidgetパス
	/// </summary>
	FString DialogWidgetPath;


protected:

	/// <summary>
	/// OnStart
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnStart() override;
};
