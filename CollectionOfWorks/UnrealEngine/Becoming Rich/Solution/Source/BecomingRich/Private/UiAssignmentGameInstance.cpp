﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UiAssignmentGameInstance.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
UUiAssignmentGameInstance::UUiAssignmentGameInstance() {

	UE_LOG(LogTemp, Display, TEXT("UUiAssignmentGameInstance Constructor"));
}

/// <summary>
/// FadeWidgetのパスを取得
/// </summary>
/// <param name=""></param>
/// <returns> パス </returns>
FString UUiAssignmentGameInstance::GetFadeWidgetPath() {

	return FadeWidgetPath;
}

/// <summary>
/// BootWidgetのパスを取得
/// </summary>
/// <param name=""></param>
/// <returns> パス </returns>
FString UUiAssignmentGameInstance::GetBootWidgetPath() {

	return BootWidgetPath;
}

/// <summary>
/// TitleWidgetのパスを取得
/// </summary>
/// <param name=""></param>
/// <returns> パス </returns>
FString UUiAssignmentGameInstance::GetTitleWidgetPath() {

	return TitleWidgetPath;
}

/// <summary>
/// BattleWidgetのパスを取得
/// </summary>
/// <param name=""></param>
/// <returns> パス </returns>
FString UUiAssignmentGameInstance::GetBattleWidgetPath() {

	return BattleWidgetPath;
}

/// <summary>
/// ResultWidgetのパスを取得
/// </summary>
/// <param name=""></param>
/// <returns> パス </returns>
FString UUiAssignmentGameInstance::GetResultWidgetPath() {

	return ResultWidgetPath;
}

/// <summary>
/// CreditWidgetのパスを取得
/// </summary>
/// <param name=""></param>
/// <returns> パス </returns>
FString UUiAssignmentGameInstance::GetCreditWidgetPath() {

	return CreditWidgetPath;
}

/// <summary>
/// DiceWidgetのパスを取得
/// </summary>
/// <param name=""></param>
/// <returns> パス </returns>
FString UUiAssignmentGameInstance::GetDiceWidgetPath() {

	return DiceWidgetPath;
}

/// <summary>
/// DialogWidgetのパスを取得
/// </summary>
/// <param name=""></param>
/// <returns> パス </returns>
FString UUiAssignmentGameInstance::GetDialogWidgetPath() {

	return DialogWidgetPath;
}

/// <summary>
/// OnStart
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UUiAssignmentGameInstance::OnStart() {

	UE_LOG(LogTemp, Display, TEXT("UUiAssignmentGameInstance OnStart"));

	FString Path;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("FadeWidgetPath"), Path, GGameIni);
	FadeWidgetPath = Path;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("BootWidgetPath"), Path, GGameIni);
	BootWidgetPath = Path;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("TitleWidgetPath"), Path, GGameIni);
	TitleWidgetPath = Path;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("BattleWidgetPath"), Path, GGameIni);
	BattleWidgetPath = Path;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("ResultWidgetPath"), Path, GGameIni);
	ResultWidgetPath = Path;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("CreditWidgetPath"), Path, GGameIni);
	CreditWidgetPath = Path;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("DiceWidgetPath"), Path, GGameIni);
	DiceWidgetPath = Path;
	GConfig->GetString(TEXT("UMGSettings"), TEXT("DialogWidgetPath"), Path, GGameIni);
	DialogWidgetPath = Path;
}