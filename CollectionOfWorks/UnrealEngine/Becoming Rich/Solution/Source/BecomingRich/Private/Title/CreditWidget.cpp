// Fill out your copyright notice in the Description page of Project Settings.


#include "Title/CreditWidget.h"
#include "Components/TextBlock.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UCreditWidget::NativeConstruct() {

	Super::NativeConstruct();

	// ファイルパスの準備
	FString ProjectPath = FPaths::ProjectDir();
	FString FilePath = ProjectPath + CreditTextFilePath;
	// ファイルパスからテキストを取得
	FString CreditText = ReadCreditText(FilePath);

	// テキストブロックを取得
	UTextBlock* TextBlock = Cast<UTextBlock>(GetWidgetFromName("TextBlock_Credit"));
	if (!IsValid(TextBlock)) {
		return;
	}
	//------------------------------
	// テキストをセット
	//------------------------------
	TextBlock->SetText(FText::FromString(CreditText));

	// セットしたテキスト内の改行文字を改行コードに変換
	ConvertCrlf();
}

/// <summary>
/// クレジットテキスト取得
/// </summary>
/// <param name=""> ファイルパス </param>
/// <returns> テキスト </returns>
FString UCreditWidget::ReadCreditText(FString FilePath) {

	FString TextData = "";
	FFileHelper::LoadFileToString(TextData, *FilePath);

	// 各行で分割した値の保持
	TArray<FString> RowText;
	TextData.ParseIntoArrayLines(RowText);

	// 配列に格納したテキストを1行にする
	FString Text = "";
	for (int32 i = 0; i < RowText.Num(); ++i) {
		Text += RowText[i];
	}

	return Text;
}

/// <summary>
/// クレジット表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UCreditWidget::Show_Implementation() {

}

/// <summary>
/// クレジット非表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UCreditWidget::Hide_Implementation() {

}

/// <summary>
/// スクロールアップ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UCreditWidget::ScrollUp_Implementation() {

}

/// <summary>
/// スクロールダウン
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UCreditWidget::ScrollDown_Implementation() {

}

/// <summary>
/// 指定文字を改行コードに変換
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UCreditWidget::ConvertCrlf_Implementation() {

}