// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget.h"
#include "Components/Border.h"
#include "TitleWidget.generated.h"

/// <summary>
/// ボタンの種類
/// </summary>
UENUM(BlueprintType)
enum class ETitleButtonType : uint8 {
	Start,
	Credit,
	End,
	MaxTypeIndex
};

/// <summary>
/// UTitleWidget
/// </summary>
UCLASS()
class UTitleWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()

public:

	/// <summary>
	/// 選択中のボタンを設定
	/// </summary>
	/// <param name="Button"> ボタン </param>
	/// <returns></returns>
	void SetupSelectButton(ETitleButtonType Button);

	/// <summary>
	/// 選択中のボタンを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 選択中のボタン </returns>
	ETitleButtonType GetSelectButton();

	/// <summary>
	/// ゲーム開始ボタンのアニメーション実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="TitleWidget")
	void ExecStartButtonAnimation();

	/// <summary>
	/// クレジットボタンのアニメーション実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "TitleWidget")
	void ExecCreditButtonAnimation();

	/// <summary>
	/// ゲーム終了ボタンのアニメーション実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "TitleWidget")
	void ExecEndButtonAnimation();

	/// <summary>
	/// ゲーム開始ボタンのアニメーション停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "TitleWidget")
	void StopStartButtonAnimation();

	/// <summary>
	/// クレジットボタンのアニメーション停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "TitleWidget")
	void StopCreditButtonAnimation();

	/// <summary>
	/// ゲーム終了ボタンのアニメーション停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "TitleWidget")
	void StopEndButtonAnimation();


protected:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

	/// <summary>
	/// 毎フレームの処理
	/// </summary>
	/// <param name="MyGeometry"> ジオメトリー </param>
	/// <param name="InDaltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void NativeTick(const FGeometry& MyGeometry, float InDaltaTime) override;


private:

	/// <summary>
	/// ボタンを選択中にする
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StartButtonAnimation();

	/// <summary>
	/// ボタンを選択外にする
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StopButtonAnimation();

	/// <summary>
	/// ボーダー
	/// </summary>
	ETitleButtonType SelectButton;

private:

	/// <summary>
	/// ゲーム開始ボタンのアニメーション実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ExecStartButtonAnimation_Implementation();

	/// <summary>
	/// クレジットボタンのアニメーション実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ExecCreditButtonAnimation_Implementation();

	/// <summary>
	/// ゲーム終了ボタンのアニメーション実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ExecEndButtonAnimation_Implementation();

	/// <summary>
	/// ゲーム開始ボタンのアニメーション停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void StopStartButtonAnimation_Implementation();

	/// <summary>
	/// クレジットボタンのアニメーション停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void StopCreditButtonAnimation_Implementation();

	/// <summary>
	/// ゲーム終了ボタンのアニメーション停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void StopEndButtonAnimation_Implementation();
};
