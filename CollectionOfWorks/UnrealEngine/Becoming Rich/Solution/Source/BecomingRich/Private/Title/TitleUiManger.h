// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "../UiAssignmentWidget.h"
#include "Title/TitleWidget.h"
#include "Title/CreditWidget.h"
#include "TitleUiManger.generated.h"


/// <summary>
/// タイトルUIマネージャ処理
/// </summary>
UCLASS()
class ATitleUiManger : public AHUD
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ATitleUiManger();

	/// <summary>
	/// PreInitializeComponents
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PreInitializeComponents() override;

	/// <summary>
	/// OpenTitle
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OpenTitle();

	/// <summary>
	/// ClosePreparing
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ClosePreparing();

	/// <summary>
	/// CloseTitle
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CloseTitle();

	/// <summary>
	/// タイトルウィジェットを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> タイトルウィジェット </returns>
	UTitleWidget* GetTitleWidget();

	/// <summary>
	/// クレジットウィジェットを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> クレジットウィジェット </returns>
	UCreditWidget* GetCreditWidget();


private:

	/// <summary>
	/// タイトルウィジェットクラス
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	TSubclassOf<UTitleWidget> TitleWidgetClass;

	/// <summary>
	/// タイトルウィジェット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	UTitleWidget* TitleWidget;

	/// <summary>
	/// クレジットウィジェットクラス
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	TSubclassOf<UCreditWidget> CreditWidgetClass;

	/// <summary>
	/// クレジットウィジェット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	UCreditWidget* CreditWidget;

	/// <summary>
	/// フェードウィジェットクラス
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	TSubclassOf<UUiAssignmentWidget> FadeWidgetClass;

	/// <summary>
	/// フェードウィジェット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	UUiAssignmentWidget* FadeWidget;
};
