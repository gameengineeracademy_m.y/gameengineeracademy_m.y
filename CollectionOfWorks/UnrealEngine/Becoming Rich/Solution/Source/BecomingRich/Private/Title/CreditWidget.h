
#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget.h"
#include "CreditWidget.generated.h"

/// <summary>
/// クレジットウィジェット
/// </summary>
UCLASS()
class UCreditWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()

public:

	/// <summary>
	/// クレジット表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "CreditWidget")
	void Show();

	/// <summary>
	/// クレジット非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "CreditWidget")
	void Hide();

	/// <summary>
	/// スクロールアップ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "CreditWidget")
	void ScrollUp();

	/// <summary>
	/// スクロールダウン
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "CreditWidget")
	void ScrollDown();

	/// <summary>
	/// 指定文字を改行コードに変換
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "CreditWidget")
	void ConvertCrlf();


protected:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

private:

	/// <summary>
	/// クレジットテキスト取得
	/// </summary>
	/// <param name=""> ファイルパス </param>
	/// <returns> テキスト </returns>
	FString ReadCreditText(FString FilePath);

	/// <summary>
	/// クレジット表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void Show_Implementation();

	/// <summary>
	/// クレジット非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void Hide_Implementation();

	/// <summary>
	/// スクロールアップ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ScrollUp_Implementation();

	/// <summary>
	/// スクロールダウン
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ScrollDown_Implementation();

	/// <summary>
	/// 指定文字を改行コードに変換
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ConvertCrlf_Implementation();

private:

	/// <summary>
	/// クレジットテキストファイルパス
	/// </summary>
	const FString CreditTextFilePath = "Content/File/CreditText.txt";
};
