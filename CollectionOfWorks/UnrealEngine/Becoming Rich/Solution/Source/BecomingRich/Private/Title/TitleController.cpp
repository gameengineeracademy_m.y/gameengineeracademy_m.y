﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Title/TitleController.h"
#include "GameFramework/PlayerInput.h"

/// <summary>
/// SetupinputComponent
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::SetupInputComponent() {

	Super::SetupInputComponent();

	UE_LOG(LogTemp, Display, TEXT("ATitleController SetupInputComponent"));

	check(InputComponent); //UE4のアサートマクロ

	if (!IsValid(InputComponent)) {
		return;
	}

	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Up", EKeys::Up));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Up", EKeys::W));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Down", EKeys::Down));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Down", EKeys::S));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Decide", EKeys::Enter));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Escape", EKeys::SpaceBar));
	InputComponent->BindAction("Up", IE_Pressed, this, &ATitleController::Up);
	InputComponent->BindAction("Down", IE_Pressed, this, &ATitleController::Down);
	InputComponent->BindAction("Decide", IE_Pressed, this, &ATitleController::Decide);
	InputComponent->BindAction("Escape", IE_Pressed, this, &ATitleController::Escape);

	UE_LOG(LogTemp, Display, TEXT("ATitleController BindAction"));
}

/// <summary>
/// BeginPlay
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::BeginPlay() {

	// アクタクラス取得
	SoundManagerClass = TSoftClassPtr<ASoundManager>(FSoftObjectPath(*SoundManagerPath)).LoadSynchronous();
	if (!IsValid(SoundManagerClass)) {
		return;
	}
	SoundManager = GetWorld()->SpawnActor<ASoundManager>(SoundManagerClass);
	UE_LOG(LogTemp, Display, TEXT("ATitleController CreateSoundManager"));
}

/// <summary>
/// 毎フレームの処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ATitleController::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}

/// <summary>
/// OpenTitle
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::OpenTitle() {

	UE_LOG(LogTemp, Display, TEXT("ATitleController OpenTitle"));

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ATitleUiManger* UiManager = Cast<ATitleUiManger>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 各ウィジェットの呼び出し
	UiManager->OpenTitle();

	UE_LOG(LogTemp, Display, TEXT("ATitleController UiManager->OpenTitle()"));
}

/// <summary>
/// ClosePreparing
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::ClosePreparing() {

	UE_LOG(LogTemp, Display, TEXT("ATitleController ClosePrepare"));

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ATitleUiManger* UiManager = Cast<ATitleUiManger>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 終了準備処理実行
	UiManager->ClosePreparing();

	UE_LOG(LogTemp, Display, TEXT("ATitleController UiManager->ClosePrepare()"));
}

/// <summary>
/// CloseTitle
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::CloseTitle() {

	UE_LOG(LogTemp, Display, TEXT("ATitleController CloseTitle"));

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ATitleUiManger* UiManager = Cast<ATitleUiManger>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 終了処理実行
	UiManager->CloseTitle();

	UE_LOG(LogTemp, Display, TEXT("ATitleController UiManager->CloseTitle()"));
}

/// <summary>
/// 1回押し仕様に変更
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::ChangePressed() {

	UE_LOG(LogTemp, Display, TEXT("ATitleController ChangePressed"));

	// 一度全てのキーバインドを外す
	InputComponent->ClearActionBindings();
	// キーのバインド設定
	InputComponent->BindAction("Up", IE_Pressed, this, &ATitleController::Up);
	InputComponent->BindAction("Down", IE_Pressed, this, &ATitleController::Down);
	InputComponent->BindAction("Decide", IE_Pressed, this, &ATitleController::Decide);
	InputComponent->BindAction("Escape", IE_Pressed, this, &ATitleController::Escape);
}

/// <summary>
/// 長押し仕様に変更
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::ChangeRepeat() {

	UE_LOG(LogTemp, Display, TEXT("ATitleController ChangeRepeat"));

	// 一度全てのキーバインドを外す
	InputComponent->ClearActionBindings();
	// キーのバインド設定
	InputComponent->BindAction("Up", IE_Repeat, this, &ATitleController::Up);
	InputComponent->BindAction("Down", IE_Repeat, this, &ATitleController::Down);
	InputComponent->BindAction("Decide", IE_Pressed, this, &ATitleController::Decide);
	InputComponent->BindAction("Escape", IE_Pressed, this, &ATitleController::Escape);
}

/// <summary>
/// 決定ボタン処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::Decide() {

	UE_LOG(LogTemp, Display, TEXT("ATitleController Decide"));

	EventInterface->OnDecide();
}

/// <summary>
/// 戻るボタン処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::Escape() {

	UE_LOG(LogTemp, Display, TEXT("ATitleController Escape"));

	EventInterface->OnEscape();
}

/// <summary>
/// 上キー(Wキー)ボタン処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::Up() {

	UE_LOG(LogTemp, Display, TEXT("ATitleController Up"));

	EventInterface->OnSelectUp();
}

/// <summary>
/// 下キー(Sキー)ボタン処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleController::Down() {

	UE_LOG(LogTemp, Display, TEXT("ATitleController Down"));

	EventInterface->OnSelectDown();
}

/// <summary>
/// イベントインターフェース
/// </summary>
/// <param name="Interface"> イベントインターフェース </param>
/// <returns></returns>
void ATitleController::SetEventInterface(const TScriptInterface<ITitleEvent>& Interface) {

	EventInterface = Interface;
}

/// <summary>
/// タイトルウィジェットを取得
/// </summary>
/// <param name=""></param>
/// <returns> タイトルウィジェット </returns>
UTitleWidget* ATitleController::GetTitleWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ATitleUiManger* UiManager = Cast<ATitleUiManger>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	UTitleWidget* TitleWidget = UiManager->GetTitleWidget();

	return TitleWidget;
}

/// <summary>
/// クレジットウィジェットを取得
/// </summary>
/// <param name=""></param>
/// <returns> クレジットウィジェット </returns>
UCreditWidget* ATitleController::GetCreditWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ATitleUiManger* UiManager = Cast<ATitleUiManger>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	UCreditWidget* CreditWidget = UiManager->GetCreditWidget();

	return CreditWidget;
}

/// <summary>
/// サウンドマネージャを取得
/// </summary>
/// <param name=""></param>
/// <returns> サウンドマネージャ </returns>
ASoundManager* ATitleController::GetSoundManager() {

	if (!IsValid(SoundManager)) {
		return nullptr;
	}

	return SoundManager;
}