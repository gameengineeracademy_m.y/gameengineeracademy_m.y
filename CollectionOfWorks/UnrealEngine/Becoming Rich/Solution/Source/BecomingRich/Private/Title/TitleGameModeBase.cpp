﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Title/TitleGameModeBase.h"
#include "Title/TitleUiManger.h"
#include "Title/TitleController.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ATitleGameModeBase::ATitleGameModeBase() {

	HUDClass = ATitleUiManger::StaticClass();
	PlayerControllerClass = ATitleController::StaticClass();
	DefaultPawnClass = nullptr;

	UE_LOG(LogTemp, Display, TEXT("ATitleGameModeBase Constructor"));
}