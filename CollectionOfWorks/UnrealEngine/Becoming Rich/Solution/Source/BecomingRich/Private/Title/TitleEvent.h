﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TitleEvent.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTitleEvent : public UInterface
{
	GENERATED_BODY()
};

/// <summary>
/// タイトルイベント
/// </summary>
class ITitleEvent
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	/// <summary>
	/// 上方向操作
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSelectUp() = 0;

	/// <summary>
	/// 下方向操作
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSelectDown() = 0;

	/// <summary>
	/// 決定処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnDecide() = 0;

	/// <summary>
	/// 戻る処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnEscape() = 0;
};
