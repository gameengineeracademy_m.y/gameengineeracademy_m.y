﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "Title/TitleEvent.h"
#include "Title/TitleController.h"
#include "TitleLevel.generated.h"

/// <summary>
/// フェーズの種類
/// </summary>
UENUM()
enum class ETitlePhaseType {
	Initialize,
	Idle,
	Credit,
	ChangeLevelPreparing,
	ChangeLevel,
	Finish,
	EndGame,
	MaxPhaseIndex
};

/// <summary>
/// タイトルレベル
/// </summary>
UCLASS()
class ATitleLevel : public ALevelScriptActor, public ITitleEvent
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ATitleLevel();

	/// <summary>
	/// 上方向操作
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSelectUp() override;

	/// <summary>
	/// 下方向操作
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSelectDown() override;

	/// <summary>
	/// 決定処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnDecide() override;

	/// <summary>
	/// 戻る処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnEscape() override;

	/// <summary>
	/// ゲーム開始処理実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StartGame();

	/// <summary>
	/// クレジット表示処理実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ShowCredit();

	/// <summary>
	/// ゲーム終了処理実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void EndGame();


private:

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	UPROPERTY()
	ETitlePhaseType CurrentPhase;

	/// <summary>
	/// 累積時間
	/// </summary>
	UPROPERTY()
	float AccumulateTime;

	/// <summary>
	/// 現在のフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentPhase(ETitlePhaseType PhaseType);

	/// <summary>
	/// タイトルウィジェットを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> タイトルウィジェット </returns>
	UTitleWidget* GetTitleWidget();

	/// <summary>
	/// クレジットウィジェットを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> クレジットウィジェット </returns>
	UCreditWidget* GetCreditWidget();

	/// <summary>
	/// サウンドマネージャを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> サウンドマネージャ </returns>
	ASoundManager* GetSoundManager();

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

private:
	//-----------------------------
	//以下、定数宣言
	//-----------------------------
	/// <summary>
	/// フェードアウト終了待機時間
	/// </summary>
	const float FinishWaitTime = 0.5f;

	/// <summary>
	/// リセット時間
	/// </summary>
	const float ResetTime = 0.0f;
};
