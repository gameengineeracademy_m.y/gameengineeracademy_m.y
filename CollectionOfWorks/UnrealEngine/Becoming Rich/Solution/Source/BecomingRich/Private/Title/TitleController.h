﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Title/TitleEvent.h"
#include "Title/TitleUiManger.h"
#include "../SoundManager.h"
#include "TitleController.generated.h"


/// <summary>
/// タイトルコントローラ
/// </summary>
UCLASS()
class ATitleController : public APlayerController
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// OpenTitle
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OpenTitle();

	/// <summary>
	/// ClosePreparing
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ClosePreparing();

	/// <summary>
	/// CloseTitle
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CloseTitle();

	/// <summary>
	/// 1回押し仕様に変更
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ChangePressed();

	/// <summary>
	/// 長押し仕様に変更
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ChangeRepeat();

	/// <summary>
	/// タイトルウィジェットを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> タイトルウィジェット </returns>
	UTitleWidget* GetTitleWidget();

	/// <summary>
	/// クレジットウィジェットを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> クレジットウィジェット </returns>
	UCreditWidget* GetCreditWidget();

	/// <summary>
	/// サウンドマネージャを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> サウンドマネージャ </returns>
	ASoundManager* GetSoundManager();

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	/// <param name=""> イベントインターフェース </param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "EventInterface")
	void SetEventInterface(const TScriptInterface<ITitleEvent>& Interface);


private:

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<ITitleEvent> EventInterface;

	/// <summary>
	/// サウンドマネージャクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<ASoundManager> SoundManagerClass;

	/// <summary>
	/// サウンドマネージャ
	/// </summary>
	UPROPERTY()
	ASoundManager* SoundManager;


protected:

	/// <summary>
	/// SetupinputComponent
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void SetupInputComponent() override;

	/// <summary>
	/// BeginPlay
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

	/// <summary>
	/// 毎フレームの処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;


private:

	/// <summary>
	/// 決定ボタン処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Decide();

	/// <summary>
	/// 戻るボタン処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Escape();

	/// <summary>
	/// 上キー(Wキー)ボタン処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Up();

	/// <summary>
	/// 下キー(Sキー)ボタン処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Down();


private:
	//--------------------------------
	//以下、定数宣言
	//--------------------------------
	/// <summary>
	/// サウンドマネージャパス
	/// </summary>
	const FString SoundManagerPath = "/Game/Blueprints/BP_SoundManager.BP_SoundManager_C";
};
