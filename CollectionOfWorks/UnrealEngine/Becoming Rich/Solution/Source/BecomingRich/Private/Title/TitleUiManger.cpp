// Fill out your copyright notice in the Description page of Project Settings.


#include "Title/TitleUiManger.h"
#include "Kismet/GameplayStatics.h"
#include "../Public/UiAssignmentGameInstance.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ATitleUiManger::ATitleUiManger()
	: TitleWidgetClass()
	, TitleWidget(nullptr)
	, FadeWidgetClass()
	, FadeWidget(nullptr) {

}

/// <summary>
/// PreInitializeComponents
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleUiManger::PreInitializeComponents() {

	UE_LOG(LogTemp, Display, TEXT("ATitleUiManger PreInitializeComponents"));

	UUiAssignmentGameInstance* GameInstance = Cast<UUiAssignmentGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (!IsValid(GameInstance)) {
		return;
	}

	// フェードウィジェットパスを取得し、フェードウィジェットを生成する
	FString FadeWidgetPath = GameInstance->GetFadeWidgetPath();
	FadeWidgetClass = TSoftClassPtr<UUiAssignmentWidget>(FSoftObjectPath(*FadeWidgetPath)).LoadSynchronous();
	if (!FadeWidgetClass) {
		return;
	}
	FadeWidget = CreateWidget<UUiAssignmentWidget>(GetWorld(), FadeWidgetClass);
	UE_LOG(LogTemp, Display, TEXT("ATitleUiManger CreateFadeWidget"))

	// タイトルウィジェットパスを取得し、タイトルウィジェットを生成する
	FString TitleWidgetPath = GameInstance->GetTitleWidgetPath();
	UE_LOG(LogTemp, Display, TEXT("%s"), *TitleWidgetPath)
	TitleWidgetClass = TSoftClassPtr<UTitleWidget>(FSoftObjectPath(*TitleWidgetPath)).LoadSynchronous();
	if (!TitleWidgetClass) {
		return;
	}
	TitleWidget = CreateWidget<UTitleWidget>(GetWorld(), TitleWidgetClass);
	UE_LOG(LogTemp, Display, TEXT("ATitleUiManger CreateTitleWidget"));

	// クレジットウィジェットパスを取得し、クレジットウィジェットを生成する
	FString CreditWidgetPath = GameInstance->GetCreditWidgetPath();
	UE_LOG(LogTemp, Display, TEXT("%s"), *CreditWidgetPath)
	CreditWidgetClass = TSoftClassPtr<UCreditWidget>(FSoftObjectPath(*CreditWidgetPath)).LoadSynchronous();
	if (!CreditWidgetClass) {
		return;
	}
	CreditWidget = CreateWidget<UCreditWidget>(GetWorld(), CreditWidgetClass);
	UE_LOG(LogTemp, Display, TEXT("ATitleUiManger CreateCreditWidget"));
}

/// <summary>
/// OpenTitle
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleUiManger::OpenTitle() {

	UE_LOG(LogTemp, Display, TEXT("ATitleUiManger OpenTitle"));

	// タイトルウィジェット
	if (!IsValid(TitleWidget)) {
		return;
	}
	TitleWidget->Open();

	UE_LOG(LogTemp, Display, TEXT("ATitleUiManger TitleWidget->Open()"));

	// クレジットウィジェット
	if (!IsValid(CreditWidget)) {
		return;
	}
	CreditWidget->Open();

	UE_LOG(LogTemp, Display, TEXT("ATitleUiManger CreditWidget->Open()"));

	// フェードウィジェット
	if (!IsValid(FadeWidget)) {
		return;
	}
	FadeWidget->Open();

	UE_LOG(LogTemp, Display, TEXT("ATitleUiManger FadeWidget->Open()"));
}

/// <summary>
/// ClosePreparing
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleUiManger::ClosePreparing() {

	if (!IsValid(FadeWidget)) {
		return;
	}
	FadeWidget->Close();

	UE_LOG(LogTemp, Display, TEXT("ATitleUiManger FadeWidget->Close()"));
}

/// <summary>
/// CloseTitle
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleUiManger::CloseTitle() {

	UE_LOG(LogTemp, Display, TEXT("ATitleUiManger CloseBoot"));

	// タイトルウィジェット
	if (!IsValid(TitleWidget)) {
		return;
	}
	TitleWidget->Close();

	UE_LOG(LogTemp, Display, TEXT("ATitleUiManger TitleWidget->Close()"));

	// クレジットウィジェット
	if (!IsValid(CreditWidget)) {
		return;
	}
	CreditWidget->Close();

	UE_LOG(LogTemp, Display, TEXT("ATitleUiManger CreditWidget->Close()"));

	// フェードウィジェット
	if (!IsValid(FadeWidget)) {
		return;
	}
	FadeWidget->Closed();

	UE_LOG(LogTemp, Display, TEXT("ATitleUiManger FadeWidget->Closed()"));
}

/// <summary>
/// タイトルウィジェットを取得
/// </summary>
/// <param name=""></param>
/// <returns> タイトルウィジェット </returns>
UTitleWidget* ATitleUiManger::GetTitleWidget() {

	return TitleWidget;
}

/// <summary>
/// クレジットウィジェットを取得
/// </summary>
/// <param name=""></param>
/// <returns> クレジットウィジェット </returns>
UCreditWidget* ATitleUiManger::GetCreditWidget() {

	return CreditWidget;
}