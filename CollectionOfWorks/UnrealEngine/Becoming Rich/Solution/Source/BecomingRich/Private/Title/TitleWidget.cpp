// Fill out your copyright notice in the Description page of Project Settings.


#include "Title/TitleWidget.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTitleWidget::NativeConstruct() {

	Super::NativeConstruct();

	UE_LOG(LogTemp, Display, TEXT("UTitleWidget NativeConstruct"));

	// 現在選択されているボタンを「スタート」に設定
	SelectButton = ETitleButtonType::Start;
}

/// <summary>
/// 毎フレームの処理
/// </summary>
/// <param name="MyGeometry"> ジオメトリー </param>
/// <param name="InDaltaTime"> 処理時間 </param>
/// <returns></returns>
void UTitleWidget::NativeTick(const FGeometry& MyGeometry, float InDaltaTime) {

	Super::NativeTick(MyGeometry, InDaltaTime);
}

/// <summary>
/// 選択中のボタンを設定
/// </summary>
/// <param name="Button"> ボタン </param>
/// <returns></returns>
void UTitleWidget::SetupSelectButton(ETitleButtonType Button) {

	// 現在選択中のボタンアニメーションを停止
	StopButtonAnimation();

	// 選択中のボタンをセット
	SelectButton = Button;

	// 現在選択中のボタンアニメーションを実行
	StartButtonAnimation();
}

/// <summary>
/// 選択中のボタンを取得
/// </summary>
/// <param name=""></param>
/// <returns> 選択中のボタン </returns>
ETitleButtonType UTitleWidget::GetSelectButton() {

	return SelectButton;
}

/// <summary>
/// ボタンを選択中にする
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTitleWidget::StartButtonAnimation() {

	switch (SelectButton) {
	case ETitleButtonType::Start: {

		ExecStartButtonAnimation();
		break;
	}
	case ETitleButtonType::Credit: {

		ExecCreditButtonAnimation();
		break;
	}
	case ETitleButtonType::End: {

		ExecEndButtonAnimation();
		break;
	}
	}
}

/// <summary>
/// ボタンを選択外にする
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTitleWidget::StopButtonAnimation() {

	switch (SelectButton) {
	case ETitleButtonType::Start: {

		StopStartButtonAnimation();
		break;
	}
	case ETitleButtonType::Credit: {

		StopCreditButtonAnimation();
		break;
	}
	case ETitleButtonType::End: {

		StopEndButtonAnimation();
		break;
	}
	}
}

/// <summary>
/// ゲーム開始ボタンのアニメーション実行
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTitleWidget::ExecStartButtonAnimation_Implementation() {

}

/// <summary>
/// クレジットボタンのアニメーション実行
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTitleWidget::ExecCreditButtonAnimation_Implementation() {

}

/// <summary>
/// ゲーム終了ボタンを設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTitleWidget::ExecEndButtonAnimation_Implementation() {

}

/// <summary>
/// ゲーム開始ボタンのアニメーション停止
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTitleWidget::StopStartButtonAnimation_Implementation() {

}

/// <summary>
/// クレジットボタンのアニメーション停止
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTitleWidget::StopCreditButtonAnimation_Implementation() {

}

/// <summary>
/// ゲーム終了ボタンのアニメーション停止
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UTitleWidget::StopEndButtonAnimation_Implementation() {

}