﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Title/TitleLevel.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ATitleLevel::ATitleLevel()
	: CurrentPhase(ETitlePhaseType::Idle)
	, AccumulateTime(0.0f) {

	// Tickイベントの有効化
	PrimaryActorTick.bCanEverTick = true;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleLevel::BeginPlay() {

	Super::BeginPlay();

	UE_LOG(LogTemp, Display, TEXT("ATitleLevel BeginPlay"));

	// 累積時間を初期化
	AccumulateTime = 0.0f;
	// 現在のフェーズを「初期設定」にセット
	CurrentPhase = ETitlePhaseType::Initialize;

	// タイトルコントローラを取得、開始処理を実行
	ATitleController* TitleController = Cast<ATitleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(TitleController)) {
		return;
	}
	// イベントインターフェースのセット
	TitleController->SetEventInterface(this);
	// UIマネージャ処理の実行
	TitleController->OpenTitle();

	UE_LOG(LogTemp, Display, TEXT("ATitleLevel TitleController->OpenTitle()"));

	// クレジットウィジェットを取得
	UCreditWidget* CreditWidget = GetCreditWidget();
	if (!IsValid(CreditWidget)) {
		return;
	}
	CreditWidget->Hide();

	UE_LOG(LogTemp, Display, TEXT("ATitleLevel CreditWidget->Hide()"));

	// タイトルウィジェット取得
	UTitleWidget* TitleWidget = GetTitleWidget();
	TitleWidget->SetupSelectButton(ETitleButtonType::Start);
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ATitleLevel::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

	switch (CurrentPhase) {
	case ETitlePhaseType::Initialize: {
		//------------------------------------
		// 初期化処理
		//------------------------------------
		// サウンドマネージャを取得
		ASoundManager* SoundManager = GetSoundManager();
		if (!IsValid(SoundManager)) {
			return;
		}
		// BGMの設定と再生
		SoundManager->SetBgmSound(EPlaySoundType::TitleBgm);
		SoundManager->PlayBgmSound();

		UE_LOG(LogTemp, Display, TEXT("ATitleLevel SoundManager->PlayBgmSound()"));

		// 現在のフェーズを「アイドル」にする
		ChangeCurrentPhase(ETitlePhaseType::Idle);
		break;
	}
	case ETitlePhaseType::ChangeLevelPreparing: {
		//------------------------------------
		// レベル変更準備処理
		//------------------------------------
		// タイトルコントローラを取得、終了準備処理を実行
		ATitleController* TitleController = Cast<ATitleController>(GetWorld()->GetFirstPlayerController());
		if (!IsValid(TitleController)) {
			return;
		}
		// 累積時間をリセット
		AccumulateTime = ResetTime;
		// フェードアウト処理実行
		TitleController->ClosePreparing();
		// 現在のフェーズを「レベル変更」にする
		ChangeCurrentPhase(ETitlePhaseType::ChangeLevel);
		break;
	}
	case ETitlePhaseType::ChangeLevel: {
		//------------------------------------
		// レベル変更処理
		//------------------------------------
		// 累積時間を加算
		AccumulateTime += DeltaTime;
		if (AccumulateTime < FinishWaitTime) {
			return;
		}
		// 累積時間をリセット
		AccumulateTime = ResetTime;
		// レベルを「バトルレベル」に変更する
		UGameplayStatics::OpenLevel(GetWorld(), TEXT("BattleLevel"), true);
		// 現在のフェーズを「終了」にする
		ChangeCurrentPhase(ETitlePhaseType::Finish);
		break;
	}
	case ETitlePhaseType::Idle:
	case ETitlePhaseType::Credit:
	case ETitlePhaseType::Finish: {
		//------------------------------------
		// その他
		//------------------------------------
		break;
	}
	}
}

/// <summary>
/// 現在のフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ATitleLevel::ChangeCurrentPhase(ETitlePhaseType PhaseType) {

	CurrentPhase = PhaseType;
}

/// <summary>
/// 上方向操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleLevel::OnSelectUp() {

	switch (CurrentPhase) {
	case ETitlePhaseType::Idle: {
		//-----------------------------
		// アイドルフェーズ
		//-----------------------------
		// タイトルウィジェットを取得
		UTitleWidget* TitleWidget = GetTitleWidget();
		// 選択中のボタンを取得
		ETitleButtonType SelectButton = TitleWidget->GetSelectButton();
		switch (SelectButton) {
		case ETitleButtonType::Start: {
			//-----------------------------
			// ゲーム開始
			//-----------------------------
			TitleWidget->SetupSelectButton(ETitleButtonType::End);
			break;
		}
		case ETitleButtonType::Credit: {
			//-----------------------------
			// クレジット
			//-----------------------------
			TitleWidget->SetupSelectButton(ETitleButtonType::Start);
			break;
		}
		case ETitleButtonType::End: {
			//-----------------------------
			// ゲーム終了
			//-----------------------------
			TitleWidget->SetupSelectButton(ETitleButtonType::Credit);
			break;
		}
		}

		// サウンドマネージャを取得
		ASoundManager* SoundManager = GetSoundManager();
		if (!IsValid(SoundManager)) {
			break;
		}
		// 操作音を鳴らす
		SoundManager->PlaySESound(EPlaySoundType::PressButton);
		break;
	}
	case ETitlePhaseType::Credit: {
		//-----------------------------
		// クレジットフェーズ
		//-----------------------------
		// クレジットウィジェットを取得する
		UCreditWidget* Widget = GetCreditWidget();
		if (!IsValid(Widget)) {
			return;
		}
		// スクロールを上げる
		Widget->ScrollUp();
		break;
	}
	}
}

/// <summary>
/// 下方向操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleLevel::OnSelectDown() {

	switch (CurrentPhase) {
	case ETitlePhaseType::Idle: {
		//-----------------------------
		// アイドルフェーズ
		//----------------------------
		// タイトルウィジェットを取得
		UTitleWidget* TitleWidget = GetTitleWidget();
		// 選択中のボタンを取得
		ETitleButtonType SelectButton = TitleWidget->GetSelectButton();
		switch (SelectButton) {
		case ETitleButtonType::Start: {
			//-----------------------------
			// ゲーム開始
			//-----------------------------
			TitleWidget->SetupSelectButton(ETitleButtonType::Credit);
			break;
		}
		case ETitleButtonType::Credit: {
			//-----------------------------
			// クレジット
			//-----------------------------
			TitleWidget->SetupSelectButton(ETitleButtonType::End);
			break;
		}
		case ETitleButtonType::End: {
			//-----------------------------
			// ゲーム終了
			//-----------------------------
			TitleWidget->SetupSelectButton(ETitleButtonType::Start);
			break;
		}
		}
		// サウンドマネージャを取得
		ASoundManager* SoundManager = GetSoundManager();
		if (!IsValid(SoundManager)) {
			break;
		}
		// 操作音を鳴らす
		SoundManager->PlaySESound(EPlaySoundType::PressButton);
		break;
	}
	case ETitlePhaseType::Credit: {
		//-----------------------------
		// クレジットフェーズ
		//-----------------------------
		// クレジットウィジェットを取得する
		UCreditWidget* Widget = GetCreditWidget();
		if (!IsValid(Widget)) {
			return;
		}
		// スクロールを下げる
		Widget->ScrollDown();
		break;
	}
	}
}

/// <summary>
/// 決定処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleLevel::OnDecide() {

	if (CurrentPhase != ETitlePhaseType::Idle) {
		return;
	}

	// タイトルウィジェットを取得
	UTitleWidget* TitleWidget = GetTitleWidget();
	// 選択中のボタンを取得
	ETitleButtonType SelectButton = TitleWidget->GetSelectButton();

	switch (SelectButton) {
	case ETitleButtonType::Start: {
		//-----------------------------
		// ゲーム開始
		//-----------------------------
		StartGame();

		// サウンドマネージャを取得
		ASoundManager* SoundManager = GetSoundManager();
		if (!IsValid(SoundManager)) {
			break;
		}
		// 決定音を鳴らす
		SoundManager->PlaySESound(EPlaySoundType::Decide);
		break;
	}
	case ETitleButtonType::Credit: {
		//-----------------------------
		// クレジット
		//-----------------------------
		ShowCredit();

		// サウンドマネージャを取得
		ASoundManager* SoundManager = GetSoundManager();
		if (!IsValid(SoundManager)) {
			break;
		}
		// 操作音を鳴らす
		SoundManager->PlaySESound(EPlaySoundType::PressButton);
		break;
	}
	case ETitleButtonType::End: {
		//-----------------------------
		// ゲーム終了
		//-----------------------------
		EndGame();
		break;
	}
	}
}

/// <summary>
/// 戻る処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleLevel::OnEscape() {

	UE_LOG(LogTemp, Display, TEXT("ATitleLevel OnEscape"));

	if (CurrentPhase != ETitlePhaseType::Credit) {
		return;
	}

	// クレジットウィジェットを取得する
	UCreditWidget* Widget = GetCreditWidget();
	if (!IsValid(Widget)) {
		return;
	}
	// クレジット非表示
	Widget->Hide();
	// 現在のフェーズを「アイドル」に変更
	ChangeCurrentPhase(ETitlePhaseType::Idle);

	// タイトルコントローラを取得、上下移動処理を1回押しに変更
	ATitleController* TitleController = Cast<ATitleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(TitleController)) {
		return;
	}
	TitleController->ChangePressed();
}

/// <summary>
/// ゲーム開始処理実行
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleLevel::StartGame() {

	UE_LOG(LogTemp, Display, TEXT("ATitleLevel StartGame"));

	if (CurrentPhase != ETitlePhaseType::Idle) {
		return;
	}

	UE_LOG(LogTemp, Display, TEXT("ATitleLevel ChangeLevelPreparing"));
	// 現在のフェーズを「レベル変更準備処理」にする
	ChangeCurrentPhase(ETitlePhaseType::ChangeLevelPreparing);
}

/// <summary>
/// クレジット表示処理実行
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleLevel::ShowCredit() {

	UE_LOG(LogTemp, Display, TEXT("ATitleLevel ShowCredit"));

	if (CurrentPhase != ETitlePhaseType::Idle) {
		return;
	}

	UE_LOG(LogTemp, Display, TEXT("ATitleLevel TitlePhaseType::Idle"));
	// クレジットウィジェットを取得する
	UCreditWidget* Widget = GetCreditWidget();
	if (!IsValid(Widget)) {
		return;
	}
	// クレジット表示
	Widget->Show();
	// 現在のフェーズを「クレジット」に変更
	ChangeCurrentPhase(ETitlePhaseType::Credit);

	// タイトルコントローラを取得、上下移動処理を長押しに変更
	ATitleController* TitleController = Cast<ATitleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(TitleController)) {
		return;
	}
	TitleController->ChangeRepeat();
}

/// <summary>
/// ゲーム終了処理実行
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ATitleLevel::EndGame() {

	UE_LOG(LogTemp, Display, TEXT("ATitleLevel EndGame"));

	if (CurrentPhase != ETitlePhaseType::Idle) {
		return;
	}

	// ゲームを終了する
	UKismetSystemLibrary::QuitGame(GetWorld(), nullptr, EQuitPreference::Quit, false);
	// 現在のフェーズを「ゲーム終了」に変更
	ChangeCurrentPhase(ETitlePhaseType::EndGame);
}

/// <summary>
/// タイトルウィジェットを取得
/// </summary>
/// <param name=""></param>
/// <returns> タイトルウィジェット </returns>
UTitleWidget* ATitleLevel::GetTitleWidget() {

	// タイトルコントローラを取得
	ATitleController* TitleController = Cast<ATitleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(TitleController)) {
		return nullptr;
	}
	// タイトルウィジェットを取得
	UTitleWidget* TitleWidget = TitleController->GetTitleWidget();
	if (!IsValid(TitleWidget)) {
		return nullptr;
	}

	return TitleWidget;
}

/// <summary>
/// クレジットウィジェットを取得
/// </summary>
/// <param name=""></param>
/// <returns> クレジットウィジェット </returns>
UCreditWidget* ATitleLevel::GetCreditWidget() {

	// タイトルコントローラを取得
	ATitleController* TitleController = Cast<ATitleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(TitleController)) {
		return nullptr;
	}
	// クレジットウィジェットを取得
	UCreditWidget* Widget = TitleController->GetCreditWidget();
	if (!IsValid(Widget)) {
		return nullptr;
	}

	return Widget;
}

/// <summary>
/// サウンドマネージャを取得
/// </summary>
/// <param name=""></param>
/// <returns> サウンドマネージャ </returns>
ASoundManager* ATitleLevel::GetSoundManager() {

	// タイトルコントローラを取得
	ATitleController* TitleController = Cast<ATitleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(TitleController)) {
		return nullptr;
	}
	ASoundManager* SoundManager = TitleController->GetSoundManager();

	return SoundManager;
}