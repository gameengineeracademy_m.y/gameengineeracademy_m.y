// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget.h"
#include "FadeWidget.generated.h"

/// <summary>
/// UFadeWidget
/// </summary>
UCLASS()
class UFadeWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()
};
