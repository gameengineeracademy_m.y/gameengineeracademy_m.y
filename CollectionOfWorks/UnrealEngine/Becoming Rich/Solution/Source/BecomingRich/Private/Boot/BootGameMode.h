﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BootGameMode.generated.h"

/// <summary>
/// ブート用ゲームモード
/// </summary>
UCLASS()
class ABootGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ABootGameMode();
};
