// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "../UiAssignmentWidget.h"
#include "BootUiManager.generated.h"


/// <summary>
/// ABootUiManager
/// </summary>
UCLASS()
class ABootUiManager : public AHUD
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ABootUiManager();

	/// <summary>
	/// PreInitializeComponents
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PreInitializeComponents() override;

	/// <summary>
	/// OpenBoot
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OpenBoot();

	/// <summary>
	/// ClosePreparing
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ClosePreparing();

	/// <summary>
	/// CloseBoot
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CloseBoot();


private:

	/// <summary>
	/// ブートウィジェットクラス
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	TSubclassOf<UUiAssignmentWidget> BootWidgetClass;

	/// <summary>
	/// ブートウィジェット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	UUiAssignmentWidget* BootWidget;

	/// <summary>
	/// フェードウィジェットクラス
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	TSubclassOf<UUiAssignmentWidget> FadeWidgetClass;

	/// <summary>
	/// フェードウィジェット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	UUiAssignmentWidget* FadeWidget;
};
