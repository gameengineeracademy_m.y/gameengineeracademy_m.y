// Fill out your copyright notice in the Description page of Project Settings.


#include "Boot/BootController.h"
#include "Boot/BootUiManager.h"

/// <summary>
/// SetupinputComponent
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootController::SetupInputComponent() {

	Super::SetupInputComponent();

	UE_LOG(LogTemp, Display, TEXT("ABootController SetupInputComponent"));
}

/// <summary>
/// OpenBoot
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootController::OpenBoot() {

	UE_LOG(LogTemp, Display, TEXT("ABootController OpenBoot"));

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABootUiManager* UiManager = Cast<ABootUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 各ウィジェットの呼び出し
	UiManager->OpenBoot();

	UE_LOG(LogTemp, Display, TEXT("ABootController UiManager->OpenBoot()"));
}

/// <summary>
/// ClosePreparing
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootController::ClosePreparing() {

	UE_LOG(LogTemp, Display, TEXT("ABootController ClosePrepare"));

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABootUiManager* UiManager = Cast<ABootUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 終了準備処理実行
	UiManager->ClosePreparing();

	UE_LOG(LogTemp, Display, TEXT("ABootController UiManager->ClosePrepare()"));
}

/// <summary>
/// CloseBoot
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootController::CloseBoot() {

	UE_LOG(LogTemp, Display, TEXT("ABootController CloseBoot"));

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABootUiManager* UiManager = Cast<ABootUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 終了処理実行
	UiManager->CloseBoot();

	UE_LOG(LogTemp, Display, TEXT("ABootController UiManager->CloseBoot()"));
}