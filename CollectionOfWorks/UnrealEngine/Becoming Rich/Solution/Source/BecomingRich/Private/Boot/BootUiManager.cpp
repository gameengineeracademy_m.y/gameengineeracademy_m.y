// Fill out your copyright notice in the Description page of Project Settings.


#include "Boot/BootUiManager.h"
#include "Kismet/GameplayStatics.h"
#include "../Public/UiAssignmentGameInstance.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ABootUiManager::ABootUiManager()
	: BootWidgetClass()
	, BootWidget(nullptr)
	, FadeWidgetClass()
	, FadeWidget(nullptr) {

}

/// <summary>
/// PreInitializeComponents
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootUiManager::PreInitializeComponents() {

	UE_LOG(LogTemp, Display, TEXT("ABootUiManager PreInitializeComponents"));

	UUiAssignmentGameInstance* GameInstance = Cast<UUiAssignmentGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (!IsValid(GameInstance)) {
		return;
	}

	// フェードウィジェットパスを取得し、フェードウィジェットを生成する
	FString FadeWidgetPath = GameInstance->GetFadeWidgetPath();
	FadeWidgetClass = TSoftClassPtr<UUiAssignmentWidget>(FSoftObjectPath(*FadeWidgetPath)).LoadSynchronous();
	if (!FadeWidgetClass) {
		return;
	}
	FadeWidget = CreateWidget<UUiAssignmentWidget>(GetWorld(), FadeWidgetClass);
	UE_LOG(LogTemp, Display, TEXT("ABootUiManager CreateFadeWidget"))

	// ブートウィジェットパスを取得し、ブートウィジェットを生成する
	FString BootWidgetPath = GameInstance->GetBootWidgetPath();
	BootWidgetClass = TSoftClassPtr<UUiAssignmentWidget>(FSoftObjectPath(*BootWidgetPath)).LoadSynchronous();
	if (!BootWidgetClass) {
		return;
	}
	BootWidget = CreateWidget<UUiAssignmentWidget>(GetWorld(), BootWidgetClass);
	UE_LOG(LogTemp, Display, TEXT("ABootUiManager CreateBootWidget"));
}

/// <summary>
/// OpenBoot
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootUiManager::OpenBoot() {

	UE_LOG(LogTemp, Display, TEXT("ABootUiManager OpenBoot"));

	if (!IsValid(BootWidget)) {
		return;
	}
	BootWidget->Open();

	UE_LOG(LogTemp, Display, TEXT("ABootUiManager BootWidget->Open()"));

	if (!IsValid(FadeWidget)) {
		return;
	}
	FadeWidget->Open();

	UE_LOG(LogTemp, Display, TEXT("ABootUiManager FadeWidget->Open()"));
}

/// <summary>
/// ClosePreparing
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootUiManager::ClosePreparing() {

	if (!IsValid(FadeWidget)) {
		return;
	}
	FadeWidget->Close();

	UE_LOG(LogTemp, Display, TEXT("ABootUiManager FadeWidget->Close()"));
}

/// <summary>
/// CloseBoot
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootUiManager::CloseBoot() {

	UE_LOG(LogTemp, Display, TEXT("ABootUiManager CloseBoot"));

	if (!IsValid(BootWidget)) {
		return;
	}
	BootWidget->Close();

	UE_LOG(LogTemp, Display, TEXT("ABootUiManager BootWidget->Close()"));

	if (!IsValid(FadeWidget)) {
		return;
	}
	FadeWidget->Closed();

	UE_LOG(LogTemp, Display, TEXT("ABootUiManager FadeWidget->Closed()"));
}