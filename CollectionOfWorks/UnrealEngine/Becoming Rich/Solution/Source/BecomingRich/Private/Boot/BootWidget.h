// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget.h"
#include "BootWidget.generated.h"

/// <summary>
/// フェーズの種類
/// </summary>
UENUM()
enum class BootHudPhaseType {
	StartUp,
	Display,
	Finish,
	MaxPhaseType
};

/// <summary>
/// UBootWidget
/// </summary>
UCLASS()
class UBootWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()
	
private:

	/// <summary>
	/// 累積時間
	/// </summary>
	float AccumulateTime;

	/// <summary>
	/// 文字配列
	/// </summary>
	TArray<FString> TextArray;

	/// <summary>
	/// インデックス
	/// </summary>
	int32 TextIndex;

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	BootHudPhaseType CurrentPhase;

	/// <summary>
	/// 現在のフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentPhase(BootHudPhaseType PhaseType);


protected:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

	/// <summary>
	/// Tick処理
	/// </summary>
	/// <param name=""> ジオメトリー </param>
	/// <param name=""> 処理時間 </param>
	/// <returns></returns>
	virtual void NativeTick(const FGeometry& MyGeometry, float InDaltaTime) override;

private:

	/// <summary>
	/// 待機時間
	/// </summary>
	const float WaitTime = 0.5f;

	/// <summary>
	/// リセット時間
	/// </summary>
	const float ResetTime = 0.0f;

	/// <summary>
	/// 文字配列 インデックス最大値
	/// </summary>
	const int MaxIndex = 3;

	/// <summary>
	/// 文字配列最大値
	/// </summary>
	const int MaxNum = 4;

	/// <summary>
	/// 文字配列 インデックス初期値
	/// </summary>
	const int InitIndex = 0;
};
