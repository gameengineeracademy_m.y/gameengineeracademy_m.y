﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Boot/BootGameMode.h"
#include "Boot/BootUiManager.h"
#include "Boot/BootController.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ABootGameMode::ABootGameMode() {

	HUDClass = ABootUiManager::StaticClass();
	PlayerControllerClass = ABootController::StaticClass();
	DefaultPawnClass = nullptr;

	UE_LOG(LogTemp, Display, TEXT("ABootGameMode Constructor"));
}