// Fill out your copyright notice in the Description page of Project Settings.


#include "Boot/BootWidget.h"
#include "Components/TextBlock.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBootWidget::NativeConstruct() {

	Super::NativeConstruct();

	// 累積時間の初期化
	AccumulateTime = ResetTime;
	// 配列の初期化
	TextArray.Empty();
	// テキストインデックス
	TextIndex = InitIndex;
	// 現在のフェーズを「起動中」にセット
	CurrentPhase = BootHudPhaseType::StartUp;

	// 配列の初期設定
	FString Period = "";
	// 配列の作成
	for (int i = 0; i < MaxNum; ++i) {
		TextArray.Add("Boot" + Period);
		// ピリオドの数を追加
		Period += ".";
	}
}

/// <summary>
/// Tick処理
/// </summary>
/// <param name="MyGeometry"> ジオメトリー </param>
/// <param name="InDaltaTime"> 処理時間 </param>
/// <returns></returns>
void UBootWidget::NativeTick(const FGeometry& MyGeometry, float InDaltaTime) {

	Super::NativeTick(MyGeometry, InDaltaTime);

	switch (CurrentPhase) {
	case BootHudPhaseType::StartUp: {
		//-------------------------------
		//起動中
		//-------------------------------
		UE_LOG(LogTemp, Display, TEXT("UBootWidget StartUp"));

		// 現在のフェーズを「表示中」に変更
		ChangeCurrentPhase(BootHudPhaseType::Display);

		break;
	}
	case BootHudPhaseType::Display: {
		//-------------------------------
		//表示中
		//-------------------------------
		UE_LOG(LogTemp, Display, TEXT("UBootWidget Display"));

		// 累積時間を加算
		AccumulateTime += InDaltaTime;
		if (AccumulateTime >= WaitTime) {
			// 累積時間を初期化
			AccumulateTime = ResetTime;
			++TextIndex;
		}

		if (TextIndex >= MaxIndex) {
			TextIndex = MaxIndex;
			// 現在のフェーズを「終了待機」に変更
			ChangeCurrentPhase(BootHudPhaseType::Finish);
		}

		break;
	}
	}

	UTextBlock* TextBlock = Cast<UTextBlock>(GetWidgetFromName("TextBlock_0"));
	if (!IsValid(TextBlock)) {
		return;
	}
	FString StringText = TextArray[TextIndex];
	TextBlock->SetText(FText::FromString(StringText));

	UE_LOG(LogTemp, Display, TEXT("UBootWidget TextBlock->SetText()"));
}

/// <summary>
/// 現在のフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void UBootWidget::ChangeCurrentPhase(BootHudPhaseType PhaseType) {

	CurrentPhase = PhaseType;
}