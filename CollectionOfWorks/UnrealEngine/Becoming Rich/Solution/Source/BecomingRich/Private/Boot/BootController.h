// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"
#include "BootController.generated.h"


/// <summary>
/// ブートコントローラ
/// </summary>
UCLASS()
class ABootController : public APlayerController
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// OpenBoot
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OpenBoot();

	/// <summary>
	/// ClosePreparing
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ClosePreparing();

	/// <summary>
	/// CloseBoot
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CloseBoot();


protected:

	/// <summary>
	/// SetupinputComponent
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void SetupInputComponent() override;
};
