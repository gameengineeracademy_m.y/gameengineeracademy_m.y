﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "BootLevel.generated.h"

/// <summary>
/// フェーズの種類
/// </summary>
UENUM()
enum class BootPhaseType {
	Idle,
	ChangeLevel,
	End,
	MaxPhaseIndex
};

/// <summary>
/// ブートレベル
/// </summary>
UCLASS()
class ABootLevel : public ALevelScriptActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ABootLevel();

private:

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	UPROPERTY()
	BootPhaseType CurrentPhase;

	/// <summary>
	/// 累積時間
	/// </summary>
	UPROPERTY()
	float AccumulateTime;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

private:

	/// <summary>
	/// 現在のフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentPhase(BootPhaseType PhaseType);


private:

	/// <summary>
	/// 画面表示時間
	/// </summary>
	const float DisplayTime = 1.6f;

	/// <summary>
	/// フェードアウト終了待機時間
	/// </summary>
	const float FinishWaitTime = 0.5f;

	/// <summary>
	/// リセット時間
	/// </summary>
	const float ResetTime = 0.0f;
};