﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Boot/BootLevel.h"
#include "Boot/BootController.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ABootLevel::ABootLevel()
	: CurrentPhase(BootPhaseType::Idle)
	, AccumulateTime(0.0f) {

	// Tickイベントの有効化
	PrimaryActorTick.bCanEverTick = true;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABootLevel::BeginPlay() {

	UKismetSystemLibrary::ExecuteConsoleCommand(GetWorld(), "r.SetRes 1280x720w");

	Super::BeginPlay();

	UE_LOG(LogTemp, Display, TEXT("ABootLevel BeginPlay"));

	// 累積時間を初期化
	AccumulateTime = 0.0f;
	// 現在のフェーズを「アイドル」にセット
	CurrentPhase = BootPhaseType::Idle;

	// ブートコントローラを取得、開始処理を実行
	ABootController* BootController = Cast<ABootController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(BootController)) {
		return;
	}
	BootController->OpenBoot();

	UE_LOG(LogTemp, Display, TEXT("ABootLevel BootController->OpenBoot()"));
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ABootLevel::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

	switch (CurrentPhase) {
	case BootPhaseType::Idle: {
		//---------------------------------
		// アイドルフェーズ
		//---------------------------------
		// 指定秒数待機し、タイトルレベルへ遷移する
		AccumulateTime += DeltaTime;
		UE_LOG(LogTemp, Display, TEXT("ABootLevel Idle : %f"), AccumulateTime);
		if (AccumulateTime < DisplayTime) {
			break;
		}
		AccumulateTime = ResetTime;
		// ブートコントローラを取得、終了準備処理を実行
		ABootController* BootController = Cast<ABootController>(GetWorld()->GetFirstPlayerController());
		if (!IsValid(BootController)) {
			return;
		}
		BootController->ClosePreparing();
		// 現在のフェーズを「レベル変更」に変更
		ChangeCurrentPhase(BootPhaseType::ChangeLevel);
		break;
	}
	case BootPhaseType::ChangeLevel: {
		//---------------------------------
		// レベル変更フェーズ
		//---------------------------------
		AccumulateTime += DeltaTime;
		UE_LOG(LogTemp, Display, TEXT("ABootLevel ChangeLevelWait : %f"), AccumulateTime);
		if (AccumulateTime < FinishWaitTime) {
			break;
		}
		AccumulateTime = ResetTime;

		// ブートコントローラを取得、終了処理を実行
		ABootController* BootController = Cast<ABootController>(GetWorld()->GetFirstPlayerController());
		if (!IsValid(BootController)) {
			return;
		}
		BootController->CloseBoot();

		// タイトルレベルに遷移する
		UE_LOG(LogTemp, Display, TEXT("ABootLevel ChangeLevel"));
		// レベルを変更する
		UGameplayStatics::OpenLevel(GetWorld(), TEXT("TitleLevel"), true);
		// 現在のフェーズを「終了待機」に変更
		ChangeCurrentPhase(BootPhaseType::End);
		break;
	}
	case BootPhaseType::End: {
		//---------------------------------
		// 終了待機
		//---------------------------------
		break;
	}
	}
}

/// <summary>
/// 現在のフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ABootLevel::ChangeCurrentPhase(BootPhaseType PhaseType) {

	CurrentPhase = PhaseType;
}