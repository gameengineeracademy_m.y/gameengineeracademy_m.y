﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UiAssignmentWidget.h"


/// <summary>
/// Open_Implementation
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UUiAssignmentWidget::Open_Implementation() {

	AddToViewport(ZOrder);

	UE_LOG(LogTemp, Display, TEXT("UUiAssignmentWidget Open_Implementation"));
}

/// <summary>
/// Close_Implementation
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UUiAssignmentWidget::Close_Implementation() {

	UE_LOG(LogTemp, Display, TEXT("UUiAssignmentWidget Close_Implementation"));
}

/// <summary>
/// Closed_Implementation
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UUiAssignmentWidget::Closed_Implementation() {

	RemoveFromParent();

	UE_LOG(LogTemp, Display, TEXT("UUiAssignmentWidget Closed_Implementation"));
}