﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UiAssignmentWidget.generated.h"

/// <summary>
/// UiAssignmentWidget
/// </summary>
UCLASS()
class UUiAssignmentWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// Open
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "UiAssignmentWidget")
	void Open();

	/// <summary>
	/// Close
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "UiAssignmentWidget")
	void Close();

	/// <summary>
	/// Closed
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "UiAssignmentWidget")
	void Closed();

private:

	/// <summary>
	/// Open_Implementation
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void Open_Implementation();

	/// <summary>
	/// Close_Implementation
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void Close_Implementation();

	/// <summary>
	/// Closed_Implementation
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void Closed_Implementation();

private:

	/// <summary>
	/// Zオーダー
	/// </summary>
	const int ZOrder = 0;
};
