// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/AudioComponent.h"
#include "Runtime/Engine/Classes/Sound/SoundCue.h"
#include "SoundManager.generated.h"

/// <summary>
/// サウンドの種類
/// </summary>
UENUM(BlueprintType)
enum class EPlaySoundType : uint8 {
	TitleBgm,
	BattleBgm,
	ResultBgm,
	Decide,
	PressButton,
	StartRollDice,
	StopRollDice,
	ConfirmDice,
	RollDice,
	StopDice,
	GetMoney,
	LostMoney,
	WarpPlayer,
	Goal01,
	Goal02,
	MaxTypeIndex
};

/// <summary>
/// サウンドマネージャ処理
/// </summary>
UCLASS()
class ASoundManager : public AActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ASoundManager();

	/// <summary>
	/// BGM設定
	/// </summary>
	/// <param name="SoundType"> サウンドの種類 </param>
	/// <returns></returns>
	void SetBgmSound(EPlaySoundType SoundType);

	/// <summary>
	/// BGM再生
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PlayBgmSound();

	/// <summary>
	/// BGM停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StopBgmSound();

	/// <summary>
	/// BGMフェードアウト
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void FadeOut();

	/// <summary>
	/// SE再生
	/// </summary>
	/// <param name="SoundType"> サウンドの種類 </param>
	/// <returns></returns>
	void PlaySESound(EPlaySoundType SoundType);


protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:	

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

private:

	/// <summary>
	/// サウンドパスリスト
	/// </summary>
	TMap<EPlaySoundType, FString> SoundPathList;

	/// <summary>
	/// サウンドリスト
	/// </summary>
	UPROPERTY()
	TMap<EPlaySoundType, USoundBase*> SoundList;

	/// <summary>
	/// BGM
	/// </summary>
	UPROPERTY()
	UAudioComponent* BgmSound;


private:
	//----------------------------------------
	//以下、定数宣言
	//----------------------------------------
	/// <summary>
	/// サウンドパス
	/// </summary>
	const FString TitleBgmPath = "/Game/Sounds/TitleBgm.TitleBgm";
	const FString BattleBgmPath = "/Game/Sounds/BattleBgm.BattleBgm";
	const FString ResultBgmPath = "/Game/Sounds/ResultBgm.ResultBgm";
	const FString DecideSePath = "/Game/Sounds/game_start.game_start";
	const FString PressButtonSePath = "/Game/Sounds/PressButton38.PressButton38";
	const FString StartRollDiceSePath = "/Game/Sounds/PressButton33.PressButton33";
	const FString StopRollDiceSePath = "/Game/Sounds/PressButton33.PressButton33";
	const FString ConfirmDiceSePath = "/Game/Sounds/PressButton33.PressButton33";
	const FString RollDiceSePath = "/Game/Sounds/RollDice.RollDice";
	const FString StopDiceSePath = "/Game/Sounds/StopDice.StopDice";
	const FString GetMoneySePath = "/Game/Sounds/GetMoney.GetMoney";
	const FString LostMoneySePath = "/Game/Sounds/LostMoney.LostMoney";
	const FString WarpPlayerSePath = "/Game/Sounds/maou_se_magic_fire06.maou_se_magic_fire06";
	const FString Goal01SePath = "/Game/Sounds/HandsClap.HandsClap";
	const FString Goal02SePath = "/Game/Sounds/maou_game_jingle05.maou_game_jingle05";

	/// <summary>
	/// フェードアウト時間
	/// </summary>
	const float FadeOutTime = 1.0f;

	/// <summary>
	/// フェードボリューム値
	/// </summary>
	const float FadeVolumeValue = 0.1f;
};