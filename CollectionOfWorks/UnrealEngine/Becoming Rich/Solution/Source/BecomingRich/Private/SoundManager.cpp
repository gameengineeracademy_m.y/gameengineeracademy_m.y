// Fill out your copyright notice in the Description page of Project Settings.


#include "SoundManager.h"
#include "Kismet/GameplayStatics.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ASoundManager::ASoundManager() {
 	
	PrimaryActorTick.bCanEverTick = true;

	// サウンドパスリストの作成
	SoundPathList.Add(EPlaySoundType::TitleBgm, TitleBgmPath);
	SoundPathList.Add(EPlaySoundType::BattleBgm, BattleBgmPath);
	SoundPathList.Add(EPlaySoundType::ResultBgm, ResultBgmPath);
	SoundPathList.Add(EPlaySoundType::Decide, DecideSePath);
	SoundPathList.Add(EPlaySoundType::PressButton, PressButtonSePath);
	SoundPathList.Add(EPlaySoundType::StartRollDice, StartRollDiceSePath);
	SoundPathList.Add(EPlaySoundType::StopRollDice, StopRollDiceSePath);
	SoundPathList.Add(EPlaySoundType::ConfirmDice, ConfirmDiceSePath);
	SoundPathList.Add(EPlaySoundType::RollDice, RollDiceSePath);
	SoundPathList.Add(EPlaySoundType::StopDice, StopDiceSePath);
	SoundPathList.Add(EPlaySoundType::GetMoney, GetMoneySePath);
	SoundPathList.Add(EPlaySoundType::LostMoney, LostMoneySePath);
	SoundPathList.Add(EPlaySoundType::WarpPlayer, WarpPlayerSePath);
	SoundPathList.Add(EPlaySoundType::Goal01, Goal01SePath);
	SoundPathList.Add(EPlaySoundType::Goal02, Goal02SePath);

	// サウンドパスリストからサウンドリストの作成
	for (auto SoundData : SoundPathList) {
		ConstructorHelpers::FObjectFinder<USoundBase> find_sound((TEXT("%s"), *SoundData.Value));
		if (find_sound.Succeeded()) {
			USoundBase* Sound = find_sound.Object;
			SoundList.Add(SoundData.Key, Sound);
			UE_LOG(LogTemp, Display, TEXT("ASoundManager Constractor AddSound"));
		}
	}
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ASoundManager::BeginPlay() {

	Super::BeginPlay();

	// オーディオコンポーネントの生成
	BgmSound = NewObject<UAudioComponent>(this);
	BgmSound->RegisterComponent();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ASoundManager::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

	if (BgmSound->IsPlaying()) {
		return;
	}
	// BGMを再度再生
	PlayBgmSound();
}

/// <summary>
/// BGM設定
/// </summary>
/// <param name="SoundType"> サウンドの種類 </param>
/// <returns></returns>
void ASoundManager::SetBgmSound(EPlaySoundType SoundType) {

	// リストに存在しないサウンドなら処理終了
	if (!SoundList.Contains(SoundType)) {
		return;
	}

	USoundBase* Sound = SoundList[SoundType];
	BgmSound->SetSound(Sound);

	UE_LOG(LogTemp, Display, TEXT("ASoundManager SoundManager->SetBgmSound()"));
}

/// <summary>
/// BGM再生
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ASoundManager::PlayBgmSound() {

	BgmSound->Play();

	UE_LOG(LogTemp, Display, TEXT("ASoundManager SoundManager->PlayBgm()"));
}

/// <summary>
/// BGM停止
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ASoundManager::StopBgmSound() {

	// 再生中でないなら処理しない
	if (!BgmSound->IsPlaying()) {
		return;
	}

	BgmSound->Stop();

	UE_LOG(LogTemp, Display, TEXT("ASoundManager SoundManager->StopBgm()"));
}

/// <summary>
/// BGMフェードアウト
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ASoundManager::FadeOut() {

	BgmSound->FadeOut(FadeOutTime, FadeVolumeValue);

	UE_LOG(LogTemp, Display, TEXT("ASoundManager SoundManager->FadeOut()"));
}

/// <summary>
/// SE再生
/// </summary>
/// <param name="SoundType"> サウンドの種類 </param>
/// <returns></returns>
void ASoundManager::PlaySESound(EPlaySoundType SoundType) {

	// リストに存在しないサウンドなら処理終了
	if (!SoundList.Contains(SoundType)) {
		return;
	}

	USoundBase* Sound = SoundList[SoundType];
	UGameplayStatics::PlaySound2D(GetWorld(), Sound);

	UE_LOG(LogTemp, Display, TEXT("ASoundManager SoundManager->PlaySound2D()"));
}