﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget.h"
#include "DiceWidget.generated.h"

/// <summary>
/// フェーズの種類
/// </summary>
UENUM()
enum class EDicePhaseType {
	Initialize,
	WaitRoll,
	Roll,
	Confirm,
	ConfirmEffect,
	Finish,
	MaxPhaseIndex
};

/// <summary>
/// 演出フェーズの種類
/// </summary>
UENUM()
enum class EDiceEffectPhase {
	Start,
	Wait,
	Finish,
	MaxPhaseIndex
};

/// <summary>
/// ダイスウィジェット処理
/// </summary>
UCLASS()
class UDiceWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()

public:

	/// <summary>
	/// 現在のフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentPhase(EDicePhaseType PhaseType);

	/// <summary>
	/// 演出フェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeEffectPhase(EDiceEffectPhase PhaseType);

	/// <summary>
	/// 現在のダイスの出目を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 現在のダイスの出目 </returns>
	int32 GetCurrentDiceRoll();

	/// <summary>
	/// サイコロのテクスチャをセット
	/// </summary>
	/// <param name=""> インデックス </param>
	/// <returns></returns>
	void ChangeDiceTexture(int32 Index);

	/// <summary>
	/// サイコロの残りマス数をセット
	/// </summary>
	/// <param name=""> 残りマス数 </param>
	/// <returns></returns>
	void ChangeRemainAdvancedNum(int32 Number);

	/// <summary>
	/// ダイス切り替え時間の変更
	/// </summary>
	/// <param name="Time"> 待機時間 </param>
	/// <returns></returns>
	void ChangeWaitTime(float Time);

	/// <summary>
	/// サイコロのインデックスを反映
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ReflectDiceWidget();

	/// <summary>
	/// サイコロのインデックスを初期化
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	int32 InitializeDiceIndex();

	/// <summary>
	/// ガイドテキスト変更
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ChangeGuideText();

	/// <summary>
	/// 待機アニメーション開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "DiceWidget")
	void PlayWaitAnimation();

	/// <summary>
	/// 待機アニメーション停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "DiceWidget")
	void StopWaitAnimation();

	/// <summary>
	/// ダイス操作ガイド表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "DiceWidget")
	void ShowDiceGuide();

	/// <summary>
	/// ダイス操作ガイド非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "DiceWidget")
	void HideDiceGuide();

	/// <summary>
	/// ダイス操作ガイドアニメーション開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "DiceWidget")
	void PlayDiceGuideAnimation();

	/// <summary>
	/// ダイス操作ガイドアニメーション停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "DiceWidget")
	void StopDiceGuideAnimation();

	/// <summary>
	/// ダイス操作ガイドフェードアウトアニメーション
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="DiceWidget")
	void PlayFadeOutDiceGuideAnimation();

	/// <summary>
	/// 開始アニメーション実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "DiceWidget")
	void PlayStartAnimation();

	/// <summary>
	/// 終了アニメーション実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="DiceWidget")
	void PlayDecideAnimation();

	/// <summary>
	/// ウィジェットの表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "DiceWidget")
	void ShowWidget();

	/// <summary>
	/// ウィジェットの非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "DiceWidget")
	void HideWidget();

	/// <summary>
	/// ダイス入力待機中かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:入力待機中, false:それ以外 </returns>
	bool IsWaitRollDice();

	/// <summary>
	/// ダイスを振っているかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:振っている, false:それ以外 </returns>
	bool IsRollDice();

	/// <summary>
	/// 終了したかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:終了済み, false:処理中 </returns>
	bool IsFinish();

protected:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

	/// <summary>
	/// Tick処理
	/// </summary>
	/// <param name=""> ジオメトリー </param>
	/// <param name=""> 処理時間 </param>
	/// <returns></returns>
	virtual void NativeTick(const FGeometry& MyGeometry, float InDaltaTime) override;

private:

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	EDicePhaseType CurrentPhase;

	/// <summary>
	/// 演出フェーズ
	/// </summary>
	EDiceEffectPhase EffectPhase;

	/// <summary>
	/// ダイスの目の配列
	/// </summary>
	TArray<int32> ArrayDiceRoll;

	/// <summary>
	/// ダイスの目の配列
	/// </summary>
	TArray<FString> ArrayTexturePath;

	/// <summary>
	/// 配列のインデックス
	/// </summary>
	int32 DiceRollIndex;

	/// <summary>
	/// 切り替え回数カウンター
	/// </summary>
	int32 SwitchCount;

	/// <summary>
	/// 累積時間
	/// </summary>
	float AccumulateTime;

	/// <summary>
	/// 待機時間 ※確定後
	/// </summary>
	float WaitTime;

	/// <summary>
	/// ダイスをランダムに表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void RandomDiceWidget();

	/// <summary>
	/// 待機アニメーション開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PlayWaitAnimation_Implementation();

	/// <summary>
	/// 待機アニメーション停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StopWaitAnimation_Implementation();

	/// <summary>
	/// ダイス操作ガイド表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ShowDiceGuide_Implementation();

	/// <summary>
	/// ダイス操作ガイド非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void HideDiceGuide_Implementation();

	/// <summary>
	/// ダイス操作ガイドアニメーション開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PlayDiceGuideAnimation_Implementation();

	/// <summary>
	/// ダイス操作ガイドアニメーション停止
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StopDiceGuideAnimation_Implementation();

	/// <summary>
	/// ダイス操作ガイドフェードアウトアニメーション
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PlayFadeOutDiceGuideAnimation_Implementation();

	/// <summary>
	/// PlayStartAnimation_Implementation
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayStartAnimation_Implementation();

	/// <summary>
	/// PlayDecideAnimation_Implementation
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayDecideAnimation_Implementation();

	/// <summary>
	/// ウィジェットの表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ShowWidget_Implementation();

	/// <summary>
	/// ウィジェットの非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void HideWidget_Implementation();

private:

	/// <summary>
	/// ダイスの0の目
	/// </summary>
	const int32 DiceRollZero = 0;

	/// <summary>
	/// ダイスの1の目
	/// </summary>
	const int32 DiceRollOne = 1;

	/// <summary>
	/// ダイスの2の目
	/// </summary>
	const int32 DiceRollTwo = 2;

	/// <summary>
	/// ダイスの3の目
	/// </summary>
	const int32 DiceRollThree = 3;

	/// <summary>
	/// ダイスの4の目
	/// </summary>
	const int32 DiceRollFour = 4;

	/// <summary>
	/// ダイスの5の目
	/// </summary>
	const int32 DiceRollFive = 5;

	/// <summary>
	/// ダイスの6の目
	/// </summary>
	const int32 DiceRollSix = 6;

	/// <summary>
	/// ダイスの1の目のテクスチャ
	/// </summary>
	const FString TextureDiceOnePath = "/Game/Textures/Dice_1";

	/// <summary>
	/// ダイスの2の目のテクスチャ
	/// </summary>
	const FString TextureDiceTwoPath = "/Game/Textures/Dice_2";

	/// <summary>
	/// ダイスの3の目のテクスチャ
	/// </summary>
	const FString TextureDiceThreePath = "/Game/Textures/Dice_3";

	/// <summary>
	/// ダイスの4の目のテクスチャ
	/// </summary>
	const FString TextureDiceFourPath = "/Game/Textures/Dice_4";

	/// <summary>
	/// ダイスの5の目のテクスチャ
	/// </summary>
	const FString TextureDiceFivePath = "/Game/Textures/Dice_5";

	/// <summary>
	/// ダイスの6の目のテクスチャ
	/// </summary>
	const FString TextureDiceSixPath = "/Game/Textures/Dice_6";

	/// <summary>
	/// インデックス初期値
	/// </summary>
	const int32 InitIndex = 0;

	/// <summary>
	/// カウンターリセット
	/// </summary>
	const int32 ResetCount = 0;

	/// <summary>
	/// スピード切り替え処理停止回数
	/// </summary>
	const int32 SwitchSpeedTimeCount = 9;

	/// <summary>
	/// 確定後の切り替え処理停止回数
	/// </summary>
	const int32 StopTimeCount = 12;

	/// <summary>
	/// リセット時間
	/// </summary>
	const float ResetTime = 0.0f;

	/// <summary>
	/// ウィジェット切り替え待機時間 ダイス確定後 その2
	/// </summary>
	const float SwitchWaitTime01 = 0.05f;

	/// <summary>
	/// ウィジェット切り替え待機時間 ダイス確定後 その2
	/// </summary>
	const float SwitchWaitTime02 = 0.20f;

	/// <summary>
	/// 演出までの待機時間
	/// </summary>
	const float EffectWaitTime = 0.75f;

	/// <summary>
	/// アニメーションの開始時間
	/// </summary>
	const float StartTime = 0.0f;
};