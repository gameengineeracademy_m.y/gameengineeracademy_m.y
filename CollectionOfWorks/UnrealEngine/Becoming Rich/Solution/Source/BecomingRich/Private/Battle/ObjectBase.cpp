﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/ObjectBase.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AObjectBase::AObjectBase() {
 	
	PrimaryActorTick.bCanEverTick = false;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AObjectBase::BeginPlay() {

	Super::BeginPlay();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AObjectBase::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}

/// <summary>
/// オブジェクト識別番号をセット
/// </summary>
/// <param name="TextNumber"> オブジェクト識別番号 </param>
/// <returns></returns>
void AObjectBase::SetObjectStrNum(FString TextNumber) {

	ObjectStrNum = TextNumber;
}

/// <summary>
/// オブジェクト識別番号を取得
/// </summary>
/// <param name=""></param>
/// <returns> オブジェクト識別番号 </returns>
FString AObjectBase::GetObjectStrNum() {

	return ObjectStrNum;
}