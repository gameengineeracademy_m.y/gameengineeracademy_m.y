﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Battle/FloorBase.h"
#include "FloorManager.generated.h"

/// <summary>
/// フロアマネージャー
/// </summary>
UCLASS()
class AFloorManager : public AActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AFloorManager();


protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

private:

	/// <summary>
	/// フロアデータCSV読み込み処理
	/// </summary>
	/// <param name="FilePath"> ファイルパス </param>
	/// <returns> フィールド配列 </returns>
	TMap<FString, FString> ReadCsvFloorDataFile(FString FilePath);

	/// <summary>
	/// フロアCSV読み込み処理
	/// </summary>
	/// <param name="FilePath"> ファイルパス </param>
	/// <returns> フィールド配列 </returns>
	TMap<int32, TArray<FString>> ReadCsvFloorMapFile(FString FilePath);

	/// <summary>
	/// フロア生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreateFloor();


private:

	/// <summary>
	/// フロア情報
	/// </summary>
	TMap<int32, TArray<FString>> FloorMap;

	/// <summary>
	/// フロアマス情報
	/// </summary>
	UPROPERTY()
	TMap<FString, AFloorBase*> FloorDataDetail;

	/// <summary>
	/// フロアデータ
	/// </summary>
	TMap<FString, FString> FloorData;

	/// <summary>
	/// フロアキーデータ
	/// </summary>
	TArray<FString> FloorKeyData;


private:

	/// <summary>
	/// フロアマップ情報CSVファイルパス
	/// </summary>
	const FString FloorMapCsvFilePath = "Content/File/FloorMap.csv";

	/// <summary>
	/// フロアデータ情報CSVファイルパス
	/// </summary>
	const FString FloorDataCsvFilePath = "Content/File/FloorData.csv";

	/// <summary>
	/// フロアクラスパス
	/// </summary>
	const FString FloorClassPath = "/Game/Blueprints/BP_FloorBase.BP_FloorBase_C";

	/// <summary>
	/// フロアキーデータ 地面
	/// </summary>
	const FString KeyDataGround = "A";

	/// <summary>
	/// フロアキーデータ 湿地
	/// </summary>
	const FString KeyDataWetland = "B";

	/// <summary>
	/// フロアキーデータ 宮殿
	/// </summary>
	const FString KeyDataPalace = "C";

	/// <summary>
	/// フロアサイズ 幅
	/// </summary>
	const float FloorSizeWidth = 200.0f;

	/// <summary>
	/// フロアサイズ 高さ
	/// </summary>
	const float FloorSizeHeight = 200.0f;

	/// <summary>
	/// フロア配置 X座標 初期値
	/// </summary>
	const float FloorPosX = -200.0f;

	/// <summary>
	/// フロア配置 Y座標 初期値
	/// </summary>
	const float FloorPosY = -200.0f;

	/// <summary>
	/// フロア配置高さ
	/// </summary>
	const float FloorPosZ = 150.0f;

	/// <summary>
	/// フロア倍率 X方向
	/// </summary>
	const float FloorScaleX = 2.0f;

	/// <summary>
	/// フロア倍率 Y方向
	/// </summary>
	const float FloorScaleY = 2.0f;

	/// <summary>
	/// フロア倍率 Z方向
	/// </summary>
	const float FloorScaleZ = 1.0f;

	/// <summary>
	/// フロア回転角度 初期値
	/// </summary>
	const float FloorRotAngle = 0.0f;

	/// <summary>
	/// ゼロ
	/// </summary>
	const int32 Zero = 0;

	/// <summary>
	/// 半分
	/// </summary>
	const float Half = 2.0f;

	/// <summary>
	/// 列情報 フロアキー
	/// </summary>
	const int32 FloorNumIndex = 0;

	/// <summary>
	/// 列情報 マテリアルパス
	/// </summary>
	const int32 PathIndex = 1;
};
