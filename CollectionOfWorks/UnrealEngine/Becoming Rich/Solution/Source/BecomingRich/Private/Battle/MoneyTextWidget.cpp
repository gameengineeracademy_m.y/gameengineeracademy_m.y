﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/MoneyTextWidget.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UMoneyTextWidget::NativeConstruct() {

	Super::NativeConstruct();
}

/// <summary>
/// Tick処理
/// </summary>
/// <param name="MyGeometry"> ジオメトリー </param>
/// <param name="InDaltaTime"> 処理時間 </param>
/// <returns></returns>
void UMoneyTextWidget::NativeTick(const FGeometry& MyGeometry, float InDaltaTime) {

	Super::NativeTick(MyGeometry, InDaltaTime);

}
