﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/BattleController.h"
#include "GameFramework/PlayerInput.h"


/// <summary>
/// SetupinputComponent
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::SetupInputComponent() {

	Super::SetupInputComponent();

	UE_LOG(LogTemp, Display, TEXT("ABattleController SetupInputComponent"));

	UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("MoveForward", EKeys::W, ForwardMoveValue));
	UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("MoveForward", EKeys::S, BackMoveValue));
	UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("MoveRight", EKeys::A, LeftMoveValue));
	UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("MoveRight", EKeys::D, RightMoveValue));
	UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("ZoomIn", EKeys::Up, ForwardMoveValue));
	UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("ZoomOut", EKeys::Down, BackMoveValue));

	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Front", EKeys::W));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Left", EKeys::A));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Right", EKeys::D));

	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("ChangeDiceTime", EKeys::L));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("SwitchCamera", EKeys::C));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("PressSpace", EKeys::SpaceBar));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("PressEnter", EKeys::Enter));
	UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("GameEnd", EKeys::P));
	
	InputComponent->BindAction("ChangeDiceTime", IE_Pressed, this, &ABattleController::ChangeDiceTime);
	InputComponent->BindAction("SwitchCamera", IE_Pressed, this, &ABattleController::SwitchMapCamera);
	InputComponent->BindAction("PressSpace", IE_Pressed, this, &ABattleController::PressSpace);
	InputComponent->BindAction("PressEnter", IE_Pressed, this, &ABattleController::PressEnter);
	InputComponent->BindAction("GameEnd", IE_Pressed, this, &ABattleController::FinishGame);
}

/// <summary>
/// BeginPlay
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::BeginPlay() {

	UE_LOG(LogTemp, Display, TEXT("ABattleController BeginPlay"));

	// アクタクラス取得
	SoundManagerClass = TSoftClassPtr<ASoundManager>(FSoftObjectPath(*SoundManagerPath)).LoadSynchronous();
	if (!IsValid(SoundManagerClass)) {
		return;
	}
	SoundManager = GetWorld()->SpawnActor<ASoundManager>(SoundManagerClass);

	UE_LOG(LogTemp, Display, TEXT("ABattleController CreateSoundManager"));
}

/// <summary>
/// 毎フレームの処理
/// </summary>
/// <param name="InDeltaTime"> 処理時間 </param>
/// <returns></returns>
void ABattleController::Tick(float InDeltaTime) {

	Super::Tick(InDeltaTime);
}

/// <summary>
/// OpenBattle
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::OpenBattle() {

	UE_LOG(LogTemp, Display, TEXT("ABattleController OpenBattle"));

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 各ウィジェットの呼び出し
	UiManager->OpenBattle();

	UE_LOG(LogTemp, Display, TEXT("ABattleController UiManager->OpenBattle()"));
}

/// <summary>
/// ClosePreparing
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::ClosePreparing() {

	UE_LOG(LogTemp, Display, TEXT("ABattleController ClosePrepare"));

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 終了準備処理実行
	UiManager->ClosePreparing();

	UE_LOG(LogTemp, Display, TEXT("ABattleController UiManager->ClosePrepare()"));
}

/// <summary>
/// CloseBattle
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::CloseBattle() {

	UE_LOG(LogTemp, Display, TEXT("ABattleController CloseBattle"));

	if (!IsValid(MyHUD)) {
		return;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return;
	}
	// 終了処理実行
	UiManager->CloseBattle();

	UE_LOG(LogTemp, Display, TEXT("ABattleController UiManager->CloseBattle()"));
}

/// <summary>
/// バトルウィジェットを取得
/// </summary>
/// <param name=""></param>
/// <returns> バトルウィジェット </returns>
UBattleWidget* ABattleController::GetBattleWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	UBattleWidget* BattleWidget = UiManager->GetBattleWidget();

	return BattleWidget;
}

/// <summary>
/// リザルトウィジェットを取得
/// </summary>
/// <param name=""></param>
/// <returns> リザルトウィジェット </returns>
UResultWidget* ABattleController::GetResultWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	UResultWidget* ResultWidget = UiManager->GetResultWidget();

	return ResultWidget;
}

/// <summary>
/// ダイスウィジェットを取得
/// </summary>
/// <param name=""></param>
/// <returns> ダイスウィジェット </returns>
UDiceWidget* ABattleController::GetDiceWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	UDiceWidget* DiceWidget = UiManager->GetDiceWidget();

	return DiceWidget;
}

/// <summary>
/// ダイアログウィジェットを取得
/// </summary>
/// <param name=""></param>
/// <returns> ダイアログウィジェット </returns>
UDialogWidget* ABattleController::GetDialogWidget() {

	if (!IsValid(MyHUD)) {
		return nullptr;
	}
	// UIマネージャを取得する
	ABattleUiManager* UiManager = Cast<ABattleUiManager>(MyHUD);
	if (!IsValid(UiManager)) {
		return nullptr;
	}

	UDialogWidget* DialogWidget = UiManager->GetDialogWidget();

	return DialogWidget;
}

/// <summary>
/// イベントインターフェース
/// </summary>
/// <param name=""> イベントインターフェース </param>
/// <returns></returns>
void ABattleController::SetEventInterface(const TScriptInterface<IBattleEvent>& Interface) {

	EventInterface = Interface;
}

/// <summary>
/// ダイス切り替え時間変更
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::ChangeDiceTime() {

	// 操作可能かチェック
	bool IsEnable = EventInterface->OnCheckEnable();
	if (!IsEnable) {
		return;
	}
	// ダイス切り替え時間変更
	EventInterface->OnChangeDiceTime();
}

/// <summary>
/// マップ用カメラの切り替え
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::SwitchMapCamera() {

	// 操作可能かチェック
	bool IsEnable = EventInterface->OnCheckEnable();
	if (!IsEnable) {
		return;
	}
	// マップ用カメラ切り替え処理
	EventInterface->OnSwitchMapCamera();
}

/// <summary>
/// サイコロを振る
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::PressSpace() {

	// 操作可能かチェック
	bool IsEnable = EventInterface->OnCheckEnable();
	if (!IsEnable) {
		return;
	}

	EventInterface->OnRollDice();
}

/// <summary>
/// 決定処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::PressEnter() {

	// 操作可能かチェック
	bool IsEnable = EventInterface->OnCheckEnable();
	if (!IsEnable) {
		return;
	}

	EventInterface->OnDecide();
}

/// <summary>
/// バトルレベル終了
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleController::FinishGame() {

	EventInterface->OnEndGame();
}

/// <summary>
/// サウンドマネージャを取得
/// </summary>
/// <param name=""></param>
/// <returns> サウンドマネージャ </returns>
ASoundManager* ABattleController::GetSoundManager() {

	if (!IsValid(SoundManager)) {
		return nullptr;
	}

	return SoundManager;
}