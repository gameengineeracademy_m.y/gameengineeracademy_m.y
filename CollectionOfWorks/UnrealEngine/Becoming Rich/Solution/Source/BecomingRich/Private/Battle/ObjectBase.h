﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ObjectBase.generated.h"

/// <summary>
/// オブジェクトベースクラス
/// </summary>
UCLASS()
class AObjectBase : public AActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AObjectBase();

	/// <summary>
	/// オブジェクト識別番号をセット
	/// </summary>
	/// <param name="TextNumber"> オブジェクト識別番号 </param>
	/// <returns></returns>
	void SetObjectStrNum(FString TextNumber);

	/// <summary>
	/// オブジェクト識別番号を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> オブジェクト識別番号 </returns>
	FString GetObjectStrNum();

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

private:

	/// <summary>
	/// オブジェクト識別番号
	/// </summary>
	FString ObjectStrNum;
};
