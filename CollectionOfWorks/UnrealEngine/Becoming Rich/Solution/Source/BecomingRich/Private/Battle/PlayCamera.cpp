
#include "Battle/PlayCamera.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
APlayCamera::APlayCamera() {

	// Tick処理を有効化
	PrimaryActorTick.bCanEverTick = true;

}

/// <summary>
/// 実行開始時処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayCamera::BeginPlay() {

	Super::BeginPlay();
	
}

/// <summary>
/// 毎フレームの処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void APlayCamera::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

}

