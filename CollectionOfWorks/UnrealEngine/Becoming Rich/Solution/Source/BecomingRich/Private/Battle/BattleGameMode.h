﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BattleGameMode.generated.h"


/// <summary>
/// ターンフェーズ
/// </summary>
UENUM()
enum class ETurnPhase {
	WaitStart,         // ゲーム開始待機フェーズ
	StartTurn,         // ターンの開始
	ConfirmMap,        // マップ確認フェーズ
	RollDice,          // サイコロを振るフェーズ
	MovePlayer,        // プレイヤー移動フェーズ
	SelectRoute,       // 分岐選択フェーズ
	DecideRoute,       // 道決定フェーズ
	OccurEvent,        // マスイベント発生フェーズ
	OccuredEvent,      // マスイベント発生後フェーズ
	FinishTurn,        // ターン終了フェーズ
	SwitchNextPlayer,  // プレイヤー切り替えフェーズ
	FinishBattle,      // バトル終了フェーズ
	PrepareResult,     // リザルト準備フェーズ
	ShowResult,        // リザルト表示フェーズ
	WaitFinishResult,  // リザルト終了待機フェーズ
	MaxPhaseIndex      // フェーズ数
};

/// <summary>
/// バトル用ゲームモード
/// </summary>
UCLASS()
class ABattleGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ABattleGameMode();

	/// <summary>
	/// 現在のフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentPhase(ETurnPhase PhaseType);

	/// <summary>
	/// 現在のフェーズを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> フェーズの種類 </returns>
	ETurnPhase GetCurrentPhase();

	/// <summary>
	/// 現在のフェーズが「サイコロを振る」かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:サイコロを振る, false:それ以外 </returns>
	bool IsRollDice();

	/// <summary>
	/// 現在のフェーズが「分岐選択中」かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:分岐選択中, false:それ以外 </returns>
	bool IsSelectRoute();

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	/// <param name=""> イベントインターフェース </param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "EventInterface")
	void SetEventInterface(const TScriptInterface<IBattleEvent>& Interface);


protected:

	/// <summary>
	/// 実行開始イベント
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	// Called every frame
	/// <summary>
	/// 毎フレームの処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

private:

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	ETurnPhase CurrentPhase;

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IBattleEvent> EventInterface;

	/// <summary>
	/// 累積時間
	/// </summary>
	float AccumulateTime;

private:

	/// <summary>
	/// リセット時間
	/// </summary>
	const float ResetTime = 0.0f;

	/// <summary>
	/// ターン開始待機時間
	/// </summary>
	const float TurnStartWaitTime = 1.8f;

	/// <summary>
	/// 待機時間
	/// </summary>
	const float WaitTime = 0.5f;

	/// <summary>
	/// バトル終了前待機時間
	/// </summary>
	const float FinishBattleWaitTime = 0.75f;

	/// <summary>
	/// リザルト表示前待機時間
	/// </summary>
	const float ResultWaitTime = 2.3f;
};
