﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget.h"
#include "Engine/DataTable.h"
#include "DialogWidget.generated.h"

/// <summary>
/// テキスト出力シーンの種類
/// </summary>
enum class EOutputScene {
	WaitRollDice,
	WaitDecideRollDice,
	SelectRoute,
	EventNone,
	EventGetMoney,
	EventLostMoney,
	EventMove,
	OperateCamera,
	ArriveGoal,
	OperateCpu,
	MaxTypeIndex
};

/// <summary>
/// ダイアログウィジェット
/// </summary>
UCLASS()
class UDialogWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// ウィジェットの非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "DialogWidget")
	void Hide();

	/// <summary>
	/// 背景ウィジェットの表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "DialogWidget")
	void Show();

	/// <summary>
	/// 指定文字を改行コードに変換
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "DialogWidget")
	void ConvertCrlf();

	/// <summary>
	/// 指定シーンのテキストを出力
	/// </summary>
	/// <param name="SceneType"> 指定シーン </param>
	/// <param name="Money"> 金額 < / param>
	/// <param name="GoalRank"> 着順 </param>
	/// <returns></returns>
	void OutputSceneText(EOutputScene SceneType, int32 Money, int32 GoalOrder);


protected:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

	/// <summary>
	/// Tick処理
	/// </summary>
	/// <param name="MyGeometry"> ジオメトリー </param>
	/// <param name="InDaltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void NativeTick(const FGeometry& MyGeometry, float InDaltaTime) override;

private:

	/// <summary>
	/// ウィジェットの非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void Hide_Implementation();

	/// <summary>
	/// 背景ウィジェットの表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void Show_Implementation();

	/// <summary>
	/// 指定文字を改行コードに変換
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ConvertCrlf_Implementation();


private:

	/// <summary>
	/// CSV読込処理
	/// </summary>
	/// <param name="FilePath"> ファイルパス </param>
	/// <returns> true:処理成功, false:処理失敗 </returns>
	bool ReadCsvFile(FString FilePath);

public:

	/// <summary>
	/// テキストデータ
	/// </summary>
	TMap<EOutputScene, FString> SceneTextData;

private:

	/// <summary>
	/// CSVファイルパス
	/// </summary>
	const FString SceneTextCsvFilePath = "Content/File/Dialog.csv";

	/// <summary>
	/// 金額変換文字
	/// </summary>
	const FString ConvertMoneyText = "ABC";

	/// <summary>
	/// 着順変換文字
	/// </summary>
	const FString ConvertOrderText = "XYZ";

	/// <summary>
	/// ゼロ
	/// </summary>
	const int32 Zero = 0;

	/// <summary>
	/// 参
	/// </summary>
	const int32 Three = 3;
};