﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/PlayerBase.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Math/UnrealMathUtility.h"
#include "Components/InputComponent.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
APlayerBase::APlayerBase() {
 	
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(InitRotValue, InitRotValue, InitRotValue); // ...at this rotation rate
	GetCharacterMovement()->AirControl = 0.15f;
	GetCharacterMovement()->JumpZVelocity = 500.0f;
	GetCharacterMovement()->GravityScale = 1.0f;
	GetCharacterMovement()->MaxWalkSpeed = 600.0f;
}

/// <summary>
/// SetupPlayerInputComponent
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);
	//Super::SetupPlayerInputComponent(PlayerInputComponent);
}

/// <summary>
/// 実行開始イベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerBase::BeginPlay()
{
	Super::BeginPlay();
	
	// 状態の初期化
	CurrentState = EStateType::None;
	// 所持金のリセット
	PocketMoney = InitMoney;
}

/// <summary>
/// 毎フレームの処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void APlayerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

/// <summary>
/// 初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerBase::Initialize() {

	FString MaterialPath;

	switch (PlayerOrder) {
	case EPlayerOrder::First: {
		//-----------------------------
		//1Pカラー
		//-----------------------------
		MaterialPath = FirstMaterialPath;
		UE_LOG(LogTemp, Display, TEXT("APlayerBase FirstMaterial"));
		break;
	}
	case EPlayerOrder::Second: {
		//-----------------------------
		//2Pカラー
		//-----------------------------
		MaterialPath = SecondMaterialPath;
		UE_LOG(LogTemp, Display, TEXT("APlayerBase SecondMaterial"));
		break;
	}
	case EPlayerOrder::Third: {
		//-----------------------------
		//3Pカラー
		//-----------------------------
		MaterialPath = ThirdMaterialPath;
		UE_LOG(LogTemp, Display, TEXT("APlayerBase ThirdMaterial"));
		break;
	}
	case EPlayerOrder::Fourth: {
		//-----------------------------
		//4Pカラー
		//-----------------------------
		MaterialPath = FourthMaterialPath;
		UE_LOG(LogTemp, Display, TEXT("APlayerBase FourthMaterial"));
		break;
	}
	default: {
		UE_LOG(LogTemp, Display, TEXT("APlayerBase OutOfRange"));
		return;
	}
	}

	// マテリアルをロードしてセット
	UMaterial* Material = LoadObject<UMaterial>(nullptr, *MaterialPath);
	if (!IsValid(Material)) {
		return;
	}
	USkeletalMeshComponent* CurrentMesh = GetMesh();
	if (!IsValid(CurrentMesh)) {
		return;
	}
	CurrentMesh->SetMaterial(MaterialIndex0, Material);
	CurrentMesh->RegisterComponent();

	// 非表示設定にする
	Hide();
	SetShowFlag(false);
}

/// <summary>
/// プレイヤーの表示設定
/// </summary>
/// <param name="Flag"> 表示フラグ </param>
/// <returns></returns>
void APlayerBase::SetShowFlag(bool Flag) {

	ShowFlag = Flag;
}

/// <summary>
/// プレイヤーが表示済みかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:表示済み, false:非表示 </returns>
bool APlayerBase::IsShow() {

	return ShowFlag == true;
}

/// <summary>
/// 操作種類をセット
/// </summary>
/// <param name="Type"> 操作種類 </param>
/// <returns></returns>
void APlayerBase::SetOperationType(EOperationType Type) {

	OperationType = Type;
}

/// <summary>
/// 操作種類を取得
/// </summary>
/// <param name=""></param>
/// <returns> 操作種類 </returns>
EOperationType APlayerBase::GetOperationType() {

	return OperationType;
}

/// <summary>
/// 所持金に取得金額を加算
/// </summary>
/// <param name=""> 取得金額 </param>
/// <returns></returns>
void APlayerBase::AddPocketMoney(int32 MoneyValue) {

	PocketMoney += MoneyValue;
}

/// <summary>
/// 所持金を取得
/// </summary>
/// <param name=""></param>
/// <returns> 所持金 </returns>
int32 APlayerBase::GetPocketMoney() {

	return PocketMoney;
}

/// <summary>
/// プレイヤー番号をセット
/// </summary>
/// <param name="Order"> プレイヤー番号 </param>
/// <returns></returns>
void APlayerBase::SetPlayerOrder(EPlayerOrder Order) {

	PlayerOrder = Order;
}

/// <summary>
/// プレイヤー番号を取得
/// </summary>
/// <param name=""></param>
/// <returns> プレイヤー番号 </returns>
EPlayerOrder APlayerBase::GetPlayerOrder() {

	return PlayerOrder;
}

/// <summary>
/// 操作の順番をセット
/// </summary>
/// <param name="Order"> 操作の順番 </param>
/// <returns></returns>
void APlayerBase::SetOperationOrder(int32 Order) {

	OperationOrder = Order;
}

/// <summary>
/// 操作の順番を取得
/// </summary>
/// <param name=""></param>
/// <returns> 操作の順番 </returns>
int32 APlayerBase::GetOperationOrder() {

	return OperationOrder;
}

/// <summary>
/// フィールド番号をセット
/// </summary>
/// <param name="Num"> フィールド番号 </param>
/// <returns></returns>
void APlayerBase::SetFieldNum(int32 Num) {

	FieldNum = Num;
}

/// <summary>
/// フィールド番号を取得
/// </summary>
/// <param name=""></param>
/// <returns> フィールド番号 </returns>
int32 APlayerBase::GetFieldNum() {

	return FieldNum;
}

/// <summary>
/// 現在のゲーム状態を変更する
/// </summary>
/// <param name="StateType"> 状態の種類 </param>
/// <returns></returns>
void APlayerBase::ChangeStateType(EStateType StateType) {

	CurrentState = StateType;
}

/// <summary>
/// 現在のゲーム状態を取得
/// </summary>
/// <param name=""></param>
/// <returns> 状態の種類 </returns>
EStateType APlayerBase::GetStateType() {

	return CurrentState;
}

/// <summary>
/// 重力を変更
/// </summary>
/// <param name="Value"> 重力の値 </param>
/// <returns> 状態の種類 </returns>
void APlayerBase::ChangeGravityScale(float Value) {

	GetCharacterMovement()->GravityScale = Value;
}

/// <summary>
/// プレイヤーがゴールしたかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:ゴール済み, false:プレイ中 </returns>
bool APlayerBase::IsArriveGoal() {

	return CurrentState == EStateType::Goal;
}

/// <summary>
/// プレイヤーを表示する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerBase::Show_Implementation() {

}

/// <summary>
/// プレイヤーを非表示する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerBase::Hide_Implementation() {

}

/// <summary>
/// アニメーション初期化
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerBase::InitializeAnimation_Implementation() {

}

/// <summary>
/// お金獲得イベント開始
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerBase::StartGetMoney_Implementation() {

}

/// <summary>
/// お金損失イベント開始
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerBase::StartLostMoney_Implementation() {

}

/// <summary>
/// お金損失イベント終了
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerBase::ResetAnimation_Implementation() {

}