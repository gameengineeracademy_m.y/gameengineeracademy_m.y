﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/FloorBase.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AFloorBase::AFloorBase() {

	// Tick処理は行わない
	PrimaryActorTick.bCanEverTick = false;

	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	RootComponent = StaticMeshComp;
	UStaticMesh* StaticMesh = LoadObject<UStaticMesh>(NULL, TEXT("/Engine/BasicShapes/Plane"), NULL, LOAD_None, NULL);
	StaticMeshComp->SetStaticMesh(StaticMesh);
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AFloorBase::BeginPlay() {

	Super::BeginPlay();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AFloorBase::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}

/// <summary>
/// フロア識別番号をセット
/// </summary>
/// <param name="TextNumber"> フロア識別番号 </param>
/// <returns></returns>
void AFloorBase::SetFloorStrNum(FString TextNumber) {

	FloorStrNum = TextNumber;
}

/// <summary>
/// フロア識別番号を取得
/// </summary>
/// <param name=""></param>
/// <returns> フロア識別番号 </returns>
FString AFloorBase::GetFloorStrNum() {

	return FloorStrNum;
}

/// <summary>
/// マテリアルのセット
/// </summary>
/// <param name="MaterialPath"> マテリアルパス </param>
/// <returns></returns>
void AFloorBase::SetMaterial(FString MaterialPath) {

	// マテリアルをロードしてセット
	UMaterial* Material = LoadObject<UMaterial>(nullptr, *MaterialPath);
	if (!IsValid(Material)) {
		return;
	}
	// マテリアルをセット
	StaticMeshComp->SetMaterial(MaterialIndex0, Material);
	StaticMeshComp->RegisterComponent();

	UE_LOG(LogTemp, Display, TEXT("AFloorBase SetMaterial"));
}