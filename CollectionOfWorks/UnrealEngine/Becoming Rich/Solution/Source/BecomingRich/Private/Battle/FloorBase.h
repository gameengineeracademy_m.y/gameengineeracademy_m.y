﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FloorBase.generated.h"

/// <summary>
/// 床
/// </summary>
UCLASS()
class AFloorBase : public AActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AFloorBase();

	/// <summary>
	/// フロア識別番号をセット
	/// </summary>
	/// <param name="TextNumber"> フロア識別番号 </param>
	/// <returns></returns>
	void SetFloorStrNum(FString TextNumber);

	/// <summary>
	/// フロア識別番号を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> フロア識別番号 </returns>
	FString GetFloorStrNum();

	/// <summary>
	/// マテリアルのセット
	/// </summary>
	/// <param name="MaterialPath"> マテリアルパス </param>
	/// <returns></returns>
	void SetMaterial(FString MaterialPath);


protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

public:

	/// <summary>
	/// デフォルトシーンルート
	/// </summary>
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Floor")
	USceneComponent* DefaultSceneRoot;

	/// <summary>
	/// スタティックメッシュコンポーネント
	/// </summary>
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Floor")
	UStaticMeshComponent* StaticMeshComp;


private:

	/// <summary>
	/// フロア識別番号
	/// </summary>
	FString FloorStrNum;


private:
	//------------------------------
	//以下、定数宣言
	//------------------------------
	/// <summary>
	/// マテリアルインデックス
	/// </summary>
	const int32 MaterialIndex0 = 0;
};
