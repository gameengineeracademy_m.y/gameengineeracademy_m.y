﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Battle/ObjectBase.h"
#include "ObjectManager.generated.h"

/// <summary>
/// オブジェクトリスト
/// </summary>
enum class EObjectList {
	PosX,
	PosY,
	PosZ,
	RotX,
	RotY,
	RotZ,
	ScaleX,
	ScaleY,
	ScaleZ,
	ListMaxIndex
};

/// <summary>
/// オブジェクトマネージャー
/// </summary>
UCLASS()
class AObjectManager : public AActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AObjectManager();

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

private:

	/// <summary>
	/// オブジェクトリストCSV読み込み処理
	/// </summary>
	/// <param name="FilePath"> ファイルパス </param>
	/// <returns> オブジェクトリスト </returns>
	TMap<FString, TMap<EObjectList, float>> ReadCsvObjectListFile(FString FilePath);

	/// <summary>
	/// オブジェクトデータCSV読み込み処理
	/// </summary>
	/// <param name="FilePath"> ファイルパス </param>
	/// <returns> オブジェクトデータ </returns>
	TMap<FString, FString> ReadCsvObjectDataFile(FString FilePath);

	/// <summary>
	/// オブジェクト生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreateObject();

	/// <summary>
	/// オブジェクトのスポーン
	/// </summary>
	/// <param name="オブジェクトクラスパス"> ObjectClassPath </param>
	/// <returns> オブジェクト </returns>
	AObjectBase* SpawnObject(FString ObjectClassPath);


private:

	/// <summary>
	/// オブジェクトリスト情報
	/// </summary>
	TMap<FString, TMap<EObjectList, float>> ObjectList;

	/// <summary>
	/// オブジェクト情報
	/// </summary>
	UPROPERTY()
	TMap<FString, AObjectBase*> ObjectDataDetail;

	/// <summary>
	/// オブジェクトデータ
	/// </summary>
	TMap<FString, FString> ObjectData;



private:
	//-----------------------------------
	//以下、定数宣言
	//-----------------------------------
	/// <summary>
	/// オブジェクトリスト情報CSVファイルパス
	/// </summary>
	const FString ObjectListCsvFilePath = "Content/File/ObjectList.csv";

	/// <summary>
	/// オブジェクトデータ情報CSVファイルパス
	/// </summary>
	const FString ObjectDataCsvFilePath = "Content/File/ObjectData.csv";

	/// <summary>
	/// 最小インデックス
	/// </summary>
	const int32 MinIndex = 0;

	/// <summary>
	/// ゼロ
	/// </summary>
	const int32 Zero = 0;
};
