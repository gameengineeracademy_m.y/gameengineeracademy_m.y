﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/DiceWidget.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDiceWidget::NativeConstruct() {

	Super::NativeConstruct();

	// 累積時間、確定後の待機時間をリセット
	AccumulateTime = ResetTime;
	WaitTime = SwitchWaitTime01;
	// 配列インデックスの初期化
	DiceRollIndex = InitIndex;
	// 切り替えカウンター初期化
	SwitchCount = ResetCount;

	// ダイスの目をセット
	ArrayDiceRoll.Empty();
	ArrayDiceRoll.Add(DiceRollZero);
	ArrayDiceRoll.Add(DiceRollOne);
	ArrayDiceRoll.Add(DiceRollFive);
	ArrayDiceRoll.Add(DiceRollThree);
	ArrayDiceRoll.Add(DiceRollSix);
	ArrayDiceRoll.Add(DiceRollFour);
	ArrayDiceRoll.Add(DiceRollTwo);

	// ダイステクスチャパスをセット
	ArrayTexturePath.Add(TextureDiceOnePath);
	ArrayTexturePath.Add(TextureDiceOnePath);
	ArrayTexturePath.Add(TextureDiceFivePath);
	ArrayTexturePath.Add(TextureDiceThreePath);
	ArrayTexturePath.Add(TextureDiceSixPath);
	ArrayTexturePath.Add(TextureDiceFourPath);
	ArrayTexturePath.Add(TextureDiceTwoPath);
}

/// <summary>
/// Tick処理
/// </summary>
/// <param name="MyGeometry"> ジオメトリー </param>
/// <param name="InDaltaTime"> 処理時間 </param>
/// <returns></returns>
void UDiceWidget::NativeTick(const FGeometry& MyGeometry, float InDaltaTime) {

	Super::NativeTick(MyGeometry, InDaltaTime);

	switch (CurrentPhase) {
	case EDicePhaseType::Initialize: {
		//--------------------------
		//初期化処理フェーズ
		//--------------------------
		break;
	}
	case EDicePhaseType::WaitRoll: {
		//--------------------------
		//入力待機フェーズ
		//--------------------------
		AccumulateTime = ResetTime;
		break;
	}
	case EDicePhaseType::Roll: 
	case EDicePhaseType::Confirm: {
		//--------------------------
		//ダイスを振っているフェーズ
		//ダイス確定フェーズ
		//--------------------------
		// 累積時間を加算
		AccumulateTime += InDaltaTime;
		if (AccumulateTime < WaitTime) {
			return;
		}
		AccumulateTime = ResetTime;
		// ダイスをランダムに表示
		RandomDiceWidget();

		// 以降の処理は「ダイス確定」後に実施
		if (CurrentPhase != EDicePhaseType::Confirm) {
			break;
		}

		// カウントアップ
		++SwitchCount;
		if (SwitchCount == SwitchSpeedTimeCount) {
			ChangeWaitTime(SwitchWaitTime02);
		}
		else if (SwitchCount == StopTimeCount) {
			SwitchCount = ResetCount;
			ChangeWaitTime(SwitchWaitTime01);
			// 現在のフェーズを「ダイス確定演出」に変更
			ChangeCurrentPhase(EDicePhaseType::ConfirmEffect);
		}
		break;
	}
	case EDicePhaseType::ConfirmEffect: {
		//--------------------------
		//ダイス確定演出フェーズ
		//--------------------------
		switch (EffectPhase) {
		case EDiceEffectPhase::Start: {

			AccumulateTime += InDaltaTime;
			if (AccumulateTime < EffectWaitTime) {
				return;
			}
			AccumulateTime = ResetTime;
			// 決定アニメーションを実行
			PlayDecideAnimation();
			// テキストブロックの数字を置き換える
			ChangeRemainAdvancedNum(DiceRollIndex);

			// 演出フェーズを「待機」に変更
			ChangeEffectPhase(EDiceEffectPhase::Wait);
			break;
		}
		case EDiceEffectPhase::Wait: {

			AccumulateTime += InDaltaTime;
			if (AccumulateTime < EffectWaitTime) {
				return;
			}
			AccumulateTime = ResetTime;
			// 演出フェーズを「終了」に変更
			ChangeEffectPhase(EDiceEffectPhase::Finish);
			break;
		}
		case EDiceEffectPhase::Finish: {
			// 演出フェーズを元に戻す
			ChangeEffectPhase(EDiceEffectPhase::Start);
			// 現在のフェーズを「終了処理」に変更
			ChangeCurrentPhase(EDicePhaseType::Finish);
			break;
		}
		}
		break;
	}
	case EDicePhaseType::Finish: {
		//--------------------------
		//終了フェーズ
		//--------------------------
		break;
	}
	}
}

/// <summary>
/// サイコロのテクスチャをセット
/// </summary>
/// <param name=""> インデックス </param>
/// <returns></returns>
void UDiceWidget::ChangeDiceTexture(int32 Index) {

	FString TexturePath = ArrayTexturePath[Index];

	// マテリアルをロードしてセット
	UTexture2D* Texture = LoadObject<UTexture2D>(nullptr, *TexturePath);
	if (!IsValid(Texture)) {
		return;
	}
	UImage* Image = Cast<UImage>(GetWidgetFromName("Image_Dice"));
	if (!IsValid(Image)) {
		return;
	}
	Image->SetBrushFromTexture(Texture);
}

/// <summary>
/// サイコロの残りマス数をセット
/// </summary>
/// <param name=""> インデックス </param>
/// <returns></returns>
void UDiceWidget::ChangeRemainAdvancedNum(int32 Index) {

	// テキストブロックの数字を置き換える
	UTextBlock* TextBlock = Cast<UTextBlock>(GetWidgetFromName("TextBlock_Number"));
	if (!IsValid(TextBlock)) {
		return;
	}
	FString StringText = FString::FromInt(ArrayDiceRoll[Index]);
	TextBlock->SetText(FText::FromString(StringText));
}

/// <summary>
/// ダイス切り替え時間の変更
/// </summary>
/// <param name="Time"> 待機時間 </param>
/// <returns></returns>
void UDiceWidget::ChangeWaitTime(float Time) {

	WaitTime = Time;
}

/// <summary>
/// ダイスをランダムに表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDiceWidget::RandomDiceWidget() {

	// インデックスを加算
	++DiceRollIndex;
	if (DiceRollIndex > DiceRollSix) {
		DiceRollIndex = DiceRollOne;
	}

	ChangeDiceTexture(DiceRollIndex);
}

/// <summary>
/// サイコロのインデックスを反映
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDiceWidget::ReflectDiceWidget() {

	// インデックスを減らす
	if (DiceRollIndex > DiceRollOne) {
		int32 Dice = ArrayDiceRoll[DiceRollIndex];
		for (int32 i = 0; i < ArrayDiceRoll.Num(); ++i) {
			if (ArrayDiceRoll[i] != Dice - 1) {
				continue;
			}
			DiceRollIndex = i;
			break;
		}
	}
	else if (DiceRollIndex == DiceRollOne) {
		--DiceRollIndex;
	}
	else {
		DiceRollIndex = InitIndex;
	}
	// ダイスウィジェットのテクスチャ変更
	ChangeDiceTexture(DiceRollIndex);
	// 残りマス数更新
	ChangeRemainAdvancedNum(DiceRollIndex);
}

/// <summary>
/// サイコロのインデックスを初期化
/// </summary>
/// <param name=""></param>
/// <returns> インデックス </returns>
int32 UDiceWidget::InitializeDiceIndex() {

	DiceRollIndex = InitIndex;

	return DiceRollIndex;
}

/// <summary>
/// ガイドテキスト変更
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDiceWidget::ChangeGuideText() {

	UTextBlock* TextBlock = Cast<UTextBlock>(GetWidgetFromName("TextBlock_Guide"));
	if (!IsValid(TextBlock)) {
		return;
	}
	FString GuideText = "";

	switch (CurrentPhase) {
	case EDicePhaseType::WaitRoll: {
		//---------------------------
		//サイコロ待機中
		//---------------------------
		GuideText = TEXT("投げる：Space");
		break;
	}
	case EDicePhaseType::Roll: {
		//---------------------------
		//サイコロ振っている最中
		//---------------------------
		GuideText = TEXT("止める：Enter");
		break;
	}
	}
	// テキストをセット
	TextBlock->SetText(FText::FromString(GuideText));
}

/// <summary>
/// 現在のフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void UDiceWidget::ChangeCurrentPhase(EDicePhaseType PhaseType) {

	CurrentPhase = PhaseType;
}

/// <summary>
/// 演出フェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void UDiceWidget::ChangeEffectPhase(EDiceEffectPhase PhaseType) {

	EffectPhase = PhaseType;
}

/// <summary>
/// 現在のダイスの出目を取得
/// </summary>
/// <param name=""></param>
/// <returns> 現在のダイスの出目 </returns>
int32 UDiceWidget::GetCurrentDiceRoll() {

	return ArrayDiceRoll[DiceRollIndex];
}

/// <summary>
/// ダイス入力待機中かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:入力待機中, false:それ以外 </returns>
bool UDiceWidget::IsWaitRollDice() {

	return CurrentPhase == EDicePhaseType::WaitRoll;
}

/// <summary>
/// ダイスを振っているかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:振っている, false:それ以外 </returns>
bool UDiceWidget::IsRollDice() {

	return CurrentPhase == EDicePhaseType::Roll;
}

/// <summary>
/// 終了したかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:終了済み, false:処理中 </returns>
bool UDiceWidget::IsFinish() {

	return CurrentPhase == EDicePhaseType::Finish;
}

/// <summary>
/// 待機アニメーション開始
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDiceWidget::PlayWaitAnimation_Implementation() {

}

/// <summary>
/// 待機アニメーション停止
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDiceWidget::StopWaitAnimation_Implementation() {

}

/// <summary>
/// ダイス操作ガイドアニメーション開始
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDiceWidget::PlayDiceGuideAnimation_Implementation() {

}

/// <summary>
/// ダイス操作ガイドアニメーション停止
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDiceWidget::StopDiceGuideAnimation_Implementation() {

}

/// <summary>
/// ダイス操作ガイドフェードアウトアニメーション
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDiceWidget::PlayFadeOutDiceGuideAnimation_Implementation() {

}

/// <summary>
/// ダイス操作ガイド表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDiceWidget::ShowDiceGuide_Implementation() {

}

/// <summary>
/// ダイス操作ガイド非表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDiceWidget::HideDiceGuide_Implementation() {

}

/// <summary>
/// PlayStartAnimation_Implementation
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDiceWidget::PlayStartAnimation_Implementation() {


}

/// <summary>
/// PlayDecideAnimation_Implementation
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDiceWidget::PlayDecideAnimation_Implementation() {


}

/// <summary>
/// ウィジェットの表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDiceWidget::ShowWidget_Implementation() {


}

/// <summary>
/// ウィジェットの非表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDiceWidget::HideWidget_Implementation() {


}