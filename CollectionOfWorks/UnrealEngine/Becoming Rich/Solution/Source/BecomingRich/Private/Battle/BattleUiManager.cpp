// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/BattleUiManager.h"
#include "Kismet/GameplayStatics.h"
#include "../Public/UiAssignmentGameInstance.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ABattleUiManager::ABattleUiManager()
	: BattleWidgetClass()
	, BattleWidget(nullptr)
	, ResultWidgetClass()
	, ResultWidget(nullptr)
	, DiceWidgetClass()
	, DiceWidget(nullptr)
	, DialogWidgetClass()
	, DialogWidget(nullptr)
	, FadeWidgetClass()
	, FadeWidget(nullptr) {

}

/// <summary>
/// PreInitializeComponents
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleUiManager::PreInitializeComponents() {

	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager PreInitializeComponents"));

	UUiAssignmentGameInstance* GameInstance = Cast<UUiAssignmentGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (!IsValid(GameInstance)) {
		return;
	}

	// フェードウィジェットパスを取得し、フェードウィジェットを生成する
	FString FadeWidgetPath = GameInstance->GetFadeWidgetPath();
	FadeWidgetClass = TSoftClassPtr<UUiAssignmentWidget>(FSoftObjectPath(*FadeWidgetPath)).LoadSynchronous();
	if (!FadeWidgetClass) {
		return;
	}
	FadeWidget = CreateWidget<UUiAssignmentWidget>(GetWorld(), FadeWidgetClass);
	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager CreateFadeWidget"))

	// バトルウィジェットパスを取得し、バトルウィジェットを生成する
	FString BattleWidgetPath = GameInstance->GetBattleWidgetPath();
	UE_LOG(LogTemp, Display, TEXT("%s"), *BattleWidgetPath)
	BattleWidgetClass = TSoftClassPtr<UBattleWidget>(FSoftObjectPath(*BattleWidgetPath)).LoadSynchronous();
	if (!BattleWidgetClass) {
		return;
	}
	BattleWidget = CreateWidget<UBattleWidget>(GetWorld(), BattleWidgetClass);
	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager CreateBattleWidget"));

	// リザルトウィジェットパスを取得し、リザルトウィジェットを生成する
	FString ResultWidgetPath = GameInstance->GetResultWidgetPath();
	UE_LOG(LogTemp, Display, TEXT("%s"), *ResultWidgetPath)
		ResultWidgetClass = TSoftClassPtr<UResultWidget>(FSoftObjectPath(*ResultWidgetPath)).LoadSynchronous();
	if (!ResultWidgetClass) {
		return;
	}
	ResultWidget = CreateWidget<UResultWidget>(GetWorld(), ResultWidgetClass);
	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager CreateResultWidget"));

	// ダイスウィジェットパスを取得し、ダイスウィジェットを生成する
	FString DiceWidgetPath = GameInstance->GetDiceWidgetPath();
	UE_LOG(LogTemp, Display, TEXT("%s"), *DiceWidgetPath)
	DiceWidgetClass = TSoftClassPtr<UDiceWidget>(FSoftObjectPath(*DiceWidgetPath)).LoadSynchronous();
	if (!DiceWidgetClass) {
		return;
	}
	DiceWidget = CreateWidget<UDiceWidget>(GetWorld(), DiceWidgetClass);
	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager CreateDiceWidget"));

	// ダイアログウィジェットパスを取得し、ダイアログウィジェットを生成する
	FString DialogWidgetPath = GameInstance->GetDialogWidgetPath();
	UE_LOG(LogTemp, Display, TEXT("%s"), *DialogWidgetPath)
		DialogWidgetClass = TSoftClassPtr<UDialogWidget>(FSoftObjectPath(*DialogWidgetPath)).LoadSynchronous();
	if (!DialogWidgetClass) {
		return;
	}
	DialogWidget = CreateWidget<UDialogWidget>(GetWorld(), DialogWidgetClass);
	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager CreateDialogWidget"));
}

/// <summary>
/// OpenBattle
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleUiManager::OpenBattle() {

	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager OpenBattle"));

	// バトルレベルUI全般ウィジェット
	if (!IsValid(BattleWidget)) {
		return;
	}
	BattleWidget->Open();

	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager BattleWidget->Open()"));

	// ダイスウィジェット
	if (!IsValid(DiceWidget)) {
		return;
	}
	DiceWidget->Open();

	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager DiceWidget->Open()"));

	// ダイアログウィジェット
	if (!IsValid(DialogWidget)) {
		return;
	}
	DialogWidget->Open();

	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager DialogWidget->Open()"));

	// リザルトウィジェット
	if (!IsValid(ResultWidget)) {
		return;
	}
	ResultWidget->Open();

	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager ResultWidget->Open()"));

	if (!IsValid(FadeWidget)) {
		return;
	}
	FadeWidget->Open();

	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager FadeWidget->Open()"));
}

/// <summary>
/// ClosePreparing
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleUiManager::ClosePreparing() {

	if (!IsValid(FadeWidget)) {
		return;
	}
	FadeWidget->Close();

	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager FadeWidget->Close()"));
}

/// <summary>
/// CloseBattle
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleUiManager::CloseBattle() {

	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager CloseBattle"));

	if (!IsValid(DialogWidget)) {
		return;
	}
	DialogWidget->Close();

	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager DialogWidget->Close()"));


	if (!IsValid(DiceWidget)) {
		return;
	}
	DiceWidget->Close();

	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager DiceWidget->Close()"));

	if (!IsValid(BattleWidget)) {
		return;
	}
	BattleWidget->Close();

	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager BattleWidget->Close()"));

	if (!IsValid(ResultWidget)) {
		return;
	}
	ResultWidget->Close();

	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager ResultWidget->Close()"));

	if (!IsValid(FadeWidget)) {
		return;
	}
	FadeWidget->Closed();

	UE_LOG(LogTemp, Display, TEXT("ABattleUiManager FadeWidget->Closed()"));
}

/// <summary>
/// バトルウィジェットを取得
/// </summary>
/// <param name=""></param>
/// <returns> バトルウィジェット </returns>
UBattleWidget* ABattleUiManager::GetBattleWidget() {

	if (!IsValid(BattleWidget)) {
		return nullptr;
	}

	return BattleWidget;
}

/// <summary>
/// リザルトウィジェットを取得
/// </summary>
/// <param name=""></param>
/// <returns> リザルトウィジェット </returns>
UResultWidget* ABattleUiManager::GetResultWidget() {

	if (!IsValid(ResultWidget)) {
		return nullptr;
	}

	return ResultWidget;
}

/// <summary>
/// ダイスウィジェットを取得
/// </summary>
/// <param name=""></param>
/// <returns> ダイスウィジェット </returns>
UDiceWidget* ABattleUiManager::GetDiceWidget() {

	if (!IsValid(DiceWidget)) {
		return nullptr;
	}

	return DiceWidget;
}

/// <summary>
/// ダイアログウィジェットを取得
/// </summary>
/// <param name=""></param>
/// <returns> ダイアログウィジェット </returns>
UDialogWidget* ABattleUiManager::GetDialogWidget() {

	if (!IsValid(DialogWidget)) {
		return nullptr;
	}

	return DialogWidget;
}