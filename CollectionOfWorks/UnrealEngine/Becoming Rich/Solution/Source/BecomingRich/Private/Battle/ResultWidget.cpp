﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/ResultWidget.h"
#include "Components/TextBlock.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UResultWidget::NativeConstruct() {

	Super::NativeConstruct();

	RankPanelPosition.Empty();
	CurrentPhase = EResultPhaseType::Wait;
	AccumulateTime = ResetTime;

	// ウィジェットを一旦非表示にしておく
	HideWidget();

	// 順位ごとの表示位置
	FVector2D DispPos;
	DispPos = FVector2D(RankingPosX, RankingFirstPosY);
	RankPanelPosition.Add(ERanking::First, DispPos);
	DispPos = FVector2D(RankingPosX, RankingSecondPosY);
	RankPanelPosition.Add(ERanking::Second, DispPos);
	DispPos = FVector2D(RankingPosX, RankingThirdPosY);
	RankPanelPosition.Add(ERanking::Third, DispPos);
	DispPos = FVector2D(RankingPosX, RankingFourthPosY);
	RankPanelPosition.Add(ERanking::Fourth, DispPos);

	// アニメーション配列を初期化する
	ArrayAnimPhase.Empty();
	AnimIndex = InitAnimIndex;
};

/// <summary>
/// Tick処理
/// </summary>
/// <param name="MyGeometry"> ジオメトリー </param>
/// <param name="InDaltaTime"> 処理時間 </param>
/// <returns></returns>
void UResultWidget::NativeTick(const FGeometry& MyGeometry, float InDaltaTime) {

	Super::NativeTick(MyGeometry, InDaltaTime);

	switch (CurrentPhase) {
	case EResultPhaseType::Wait: {
		//----------------------------
		// 待機フェーズ
		//----------------------------
		break;
	}
	case EResultPhaseType::Prepare: {
		//----------------------------
		// 準備フェーズ
		//----------------------------
		// アニメーション配列の最大要素をインデックスにセット
		AnimIndex = ArrayAnimPhase.Num();
		// 現在のフェーズを「背景表示」に変更
		ChangeCurrentPhase(EResultPhaseType::ShowBack);
		break;
	}
	case EResultPhaseType::ShowBack: {
		//----------------------------
		// 背景表示フェーズ
		//----------------------------
		// 背景表示
		ShowBack();
		// 現在のフェーズを「リザルトボード表示」に変更
		ChangeCurrentPhase(EResultPhaseType::ShowResultBoard);
		break;
	}
	case EResultPhaseType::ShowResultBoard: {
		//----------------------------
		// リザルトボード表示フェーズ
		//----------------------------
		AccumulateTime += InDaltaTime;
		if (AccumulateTime < BoardWaitTime) {
			break;
		}
		AccumulateTime = ResetTime;

		// リザルトボード表示
		ShowResultBoard();
		// 現在のフェーズを「アニメーション待機」に変更
		ChangeCurrentPhase(EResultPhaseType::WaitAniation);
		break;
	}
	case EResultPhaseType::WaitAniation: {
		//----------------------------
		// アニメーション待機フェーズ
		//----------------------------
		AccumulateTime += InDaltaTime;
		if (AccumulateTime < ShowBoardWaitTime) {
			break;
		}
		AccumulateTime = ResetTime;

		// 現在のフェーズを「アニメーション表示」に変更
		ChangeCurrentPhase(EResultPhaseType::RankAnimation);
		break;
	}
	case EResultPhaseType::RankAnimation: {
		//----------------------------
		// アニメーション表示フェーズ
		//----------------------------
		// 指定時間待機する
		AccumulateTime += InDaltaTime;
		if (AccumulateTime < AnimWaitTime) {
			break;
		}
		AccumulateTime = ResetTime;

		// インデックスを減算
		--AnimIndex;

		if (AnimIndex < InitAnimIndex) {
			// 現在のフェーズを「案内表示」に変更
			ChangeCurrentPhase(EResultPhaseType::ShowGuide);
			break;
		}

		switch (ArrayAnimPhase[AnimIndex]) {
		case EAnimationPhase::AnimFourth: {
			//----------------------------
			// 4Pアニメーション表示
			//----------------------------
			ShowRankPanel(EAnimationPhase::AnimFourth);
			break;
		}
		case EAnimationPhase::AnimThird: {
			//----------------------------
			// 3Pアニメーション表示
			//----------------------------
			ShowRankPanel(EAnimationPhase::AnimThird);
			break;
		}
		case EAnimationPhase::AnimSecond: {
			//----------------------------
			// 2Pアニメーション表示
			//----------------------------
			ShowRankPanel(EAnimationPhase::AnimSecond);
			break;
		}
		case EAnimationPhase::AnimFirst: {
			//----------------------------
			// 1Pアニメーション表示
			//----------------------------
			ShowRankPanel(EAnimationPhase::AnimFirst);
			break;
		}
		default: {
			//----------------------------
			// それ以外
			//----------------------------
			break;
		}
		}
		break;
	}
	case EResultPhaseType::ShowGuide: {
		//----------------------------
		// 案内表示フェーズ
		//----------------------------
		// 案内表示
		ShowGuideText();
		// 現在のフェーズを「レベル遷移待機」に変更
		ChangeCurrentPhase(EResultPhaseType::WaitTransition);
		break;
	}
	case EResultPhaseType::WaitTransition: {
		//----------------------------
		// レベル遷移待機フェーズ
		//----------------------------
		break;
	}
	}
}

/// <summary>
/// 順位情報のセット
/// </summary>
/// <param name="Order"> プレイヤー識別 </param>
/// <param name="Rank"> ランキング </param>
/// <param name="Money"> 所持金 </param>
/// <returns></returns>
void UResultWidget::SetupRankingData(EPlayerOrder Order, ERanking Rank, int32 Money) {

	UTextBlock* TextBlock_Player = nullptr;
	UTextBlock* TextBlock_Money = nullptr;

	// プレイヤー情報を文字列化
	FString StringText = FString::FromInt(static_cast<int>(Order));
	StringText.Append(PlayerText);
	// 所持金を文字列化
	FString OldText = FString::FromInt(Money);
	FString NewText;
	while (OldText.Len() > Three) {  // 「,」をつける必要があるまでループ
		NewText = "," + OldText.Mid(OldText.Len() - Three, Three) + NewText;  // 3文字ごとに「,」で区切る
		OldText = OldText.Mid(Zero, OldText.Len() - Three);  // 文字列を更新
	}
	NewText = OldText.Append(NewText);

	switch (Order) {
	case EPlayerOrder::First: {
		//----------------------------
		// 1P
		//----------------------------
		// プレイヤー情報をテキストブロックにセット
		TextBlock_Player = Cast<UTextBlock>(GetWidgetFromName("TextBlock_First_Player"));
		if (!IsValid(TextBlock_Player)) {
			return;
		}
		TextBlock_Player->SetText(FText::FromString(StringText));

		// 所持金をテキストブロックにセット
		TextBlock_Money = Cast<UTextBlock>(GetWidgetFromName("TextBlock_First_Money"));
		if (!IsValid(TextBlock_Money)) {
			return;
		}
		TextBlock_Money->SetText(FText::FromString(NewText));

		break;
	}
	case EPlayerOrder::Second: {
		//----------------------------
		// 2P
		//----------------------------
		// プレイヤー情報をテキストブロックにセット
		TextBlock_Player = Cast<UTextBlock>(GetWidgetFromName("TextBlock_Second_Player"));
		if (!IsValid(TextBlock_Player)) {
			return;
		}
		TextBlock_Player->SetText(FText::FromString(StringText));

		// 所持金をテキストブロックにセット
		TextBlock_Money = Cast<UTextBlock>(GetWidgetFromName("TextBlock_Second_Money"));
		if (!IsValid(TextBlock_Money)) {
			return;
		}
		TextBlock_Money->SetText(FText::FromString(NewText));

		break;
	}
	case EPlayerOrder::Third: {
		//----------------------------
		// 3P
		//----------------------------
		// プレイヤー情報をテキストブロックにセット
		TextBlock_Player = Cast<UTextBlock>(GetWidgetFromName("TextBlock_Third_Player"));
		if (!IsValid(TextBlock_Player)) {
			return;
		}
		TextBlock_Player->SetText(FText::FromString(StringText));

		// 所持金をテキストブロックにセット
		TextBlock_Money = Cast<UTextBlock>(GetWidgetFromName("TextBlock_Third_Money"));
		if (!IsValid(TextBlock_Money)) {
			return;
		}
		TextBlock_Money->SetText(FText::FromString(NewText));

		break;
	}
	case EPlayerOrder::Fourth: {
		//----------------------------
		// 4P
		//----------------------------
		// プレイヤー情報をテキストブロックにセット
		TextBlock_Player = Cast<UTextBlock>(GetWidgetFromName("TextBlock_Fourth_Player"));
		if (!IsValid(TextBlock_Player)) {
			return;
		}
		TextBlock_Player->SetText(FText::FromString(StringText));

		// 所持金をテキストブロックにセット
		TextBlock_Money = Cast<UTextBlock>(GetWidgetFromName("TextBlock_Fourth_Money"));
		if (!IsValid(TextBlock_Money)) {
			return;
		}
		TextBlock_Money->SetText(FText::FromString(NewText));

		break;
	}
	}

	// ランキングの背景パネルの座標取得
	FVector2D DispPosition = RankPanelPosition[Rank];
	// ランキングの背景パネルのセット
	SetRankPanelPosition(Order, DispPosition);
}

/// <summary>
/// 現在のフェーズを変更
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void UResultWidget::ChangeCurrentPhase(EResultPhaseType PhaseType) {

	CurrentPhase = PhaseType;
}

/// <summary>
/// アニメーション配列に追加
/// </summary>
/// <param name="AnimPhase"> フェーズの種類 </param>
/// <returns></returns>
void UResultWidget::AddAnimationArray(EAnimationPhase AnimPhase) {

	ArrayAnimPhase.Add(AnimPhase);
}

/// <summary>
/// ウィジェットの非表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UResultWidget::HideWidget_Implementation() {

}

/// <summary>
/// ランクパネルの非表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UResultWidget::HideRankPanel_Implementation() {

}

/// <summary>
/// ガイドパネルの非表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UResultWidget::HideGuidePanel_Implementation() {

}

/// <summary>
/// 背景ウィジェットの表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UResultWidget::ShowBack_Implementation() {

}

/// <summary>
/// リザルトボードウィジェットの表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UResultWidget::ShowResultBoard_Implementation() {

}

/// <summary>
/// ランキングパネルの座標セット
/// </summary>
/// <param name="Order"> プレイヤー識別 </param>
/// <param name="Position"> 座標 </param>
/// <returns></returns>
void UResultWidget::SetRankPanelPosition_Implementation(EPlayerOrder Order, FVector2D Position) {

}


/// <summary>
/// ランク情報パネルの表示
/// </summary>
/// <param name="PhaseType"></param>
/// <returns></returns>
void UResultWidget::ShowRankPanel_Implementation(EAnimationPhase PhaseType) {

}

/// <summary>
/// 案内ウィジェットの表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UResultWidget::ShowGuideText_Implementation() {

}