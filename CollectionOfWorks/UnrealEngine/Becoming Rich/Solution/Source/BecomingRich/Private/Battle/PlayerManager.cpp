﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/PlayerManager.h"
#include "Battle/BattleController.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
APlayerManager::APlayerManager()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

/// <summary>
/// 実行開始イベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerManager::BeginPlay()
{
	Super::BeginPlay();

	// 現在のフェーズの初期化
	CurrentPhase = EOperatePhase::Wait;
	CurrentMovePhase = EEventMovePhase::None;

	// プレイヤー人数を初期化
	AllPlayer.Empty();
	ForwardNum = 5;

	// 時間の初期化
	NormalMoveAccumuTime = ResetTime;
	SpecialMoveAccumuTime = ResetTime;
	WaitTime = ResetTime;
}

/// <summary>
/// 毎フレームの処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void APlayerManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// 現在プレイ中のプレイヤーを取得
	APlayerBase* Player = GetPlayer(OperationOrderList[CurrentTurn]);
	if (!IsValid(Player)) {
		return;
	}

	EStateType CurrentState = Player->GetStateType();
	// ゴールに到達しているなら処理終了
	if (CurrentState == EStateType::Goal) {
		return;
	}

	//------------------------------------
	// 通常移動処理
	//------------------------------------
	MoveNormalPlayer(DeltaTime, Player);

	//------------------------------------
	// 特殊移動処理
	//------------------------------------
	MoveSpecialPlayer(DeltaTime, Player);
}

/// <summary>
/// SetupPlayerInputComponent
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerManager::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

/// <summary>
/// イベントインターフェース
/// </summary>
/// <param name=""> イベントインターフェース </param>
/// <returns></returns>
UFUNCTION(BlueprintCallable, Category = "EventInterface")
void APlayerManager::SetEventInterface(const TScriptInterface<IBattleEvent>& Interface) {

	EventInterface = Interface;
}

/// <summary>
/// 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerManager::Initialize() {
	//---------------------
	// プレイヤー生成処理
	//---------------------
	CreatePlayer();

	//---------------------
	// 先頭プレイヤー設定
	//---------------------
	// 順番の初期設定
	SetCurrentTurn(One);
	// 先頭は表示しておく
	EPlayerOrder Order = OperationOrderList[CurrentTurn];
	APlayerBase* Player = GetPlayer(Order);
	Player->Show();
	Player->SetShowFlag(true);
	// フィールド中央に配置
	int32 Number = Player->GetFieldNum();
	FVector Pos = EventInterface->OnGetFieldPosition(Number);
	Pos.Z += AdjustPosZ;
	Player->SetActorLocation(Pos);
}

/// <summary>
/// プレイヤー生成
/// </summary>
/// <param name=""></param>
/// <returns> true:処理成功, false:処理失敗 </returns>
bool APlayerManager::CreatePlayer() {

	UE_LOG(LogTemp, Display, TEXT("APlayerManager ReadCsvFile"));

	FString ProjectPath = FPaths::ProjectDir();
	FString FilePath = ProjectPath + PlayerCsvFilePath;

	// CSVファイルからデータを取得
	FString CsvDataFile;
	FFileHelper::LoadFileToString(CsvDataFile, *FilePath);
	if (CsvDataFile.IsEmpty()) {
		UE_LOG(LogTemp, Display, TEXT("APlayerManager CsvFile Empty"));
		return false;
	}

	// 各行で分割した値の保持
	TArray<FString> Row;
	// カンマ区切りごとの値
	TArray<FString> Element;
	TArray<int32> IntElement;

	CsvDataFile.ParseIntoArrayLines(Row);
	// キャラクタクラス取得
	TSubclassOf<class APlayerBase> PlayerCharacter = TSoftClassPtr<APlayerBase>(FSoftObjectPath(*PlayerClassPath)).LoadSynchronous();
	if (!IsValid(PlayerCharacter)) {
		UE_LOG(LogTemp, Display, TEXT("APlayerManager PlayerClass Error"));
		return false;
	}

	for (int i = 1; i < Row.Num(); ++i) {

		if (Row[i].IsEmpty()) {
			break;
		}
		// カンマ区切りで取得
		Row[i].ParseIntoArray(Element, TEXT(","), true);
		// 取得した値を文字列から数値に変換
		for (int j = 0; j < Element.Num(); ++j) {
			IntElement.Add(FCString::Atoi(*Element[j]));
		}

		// ユーザー操作かAI操作かの設定
		if (IntElement[ColPlayerType] < static_cast<int32>(EOperationType::Player) ||
			IntElement[ColPlayerType] > static_cast<int32>(EOperationType::CpuTypeF)) {
			UE_LOG(LogTemp, Display, TEXT("APlayerManager OperationType OutOfRange"));
			return false;
		}
		EOperationType OperationType = static_cast<EOperationType>(IntElement[ColPlayerType]);
		
		// プレイヤーの識別
		if (i < static_cast<int32>(EPlayerOrder::First) ||
			i > static_cast<int32>(EPlayerOrder::Fourth)) {
			UE_LOG(LogTemp, Display, TEXT("APlayerManager PlayerOrder OutOfRange"));
			return false;
		}
		EPlayerOrder PlayerOrder = static_cast<EPlayerOrder>(i);
		FVector AdjustPos = GetWaitAdjustPosition(PlayerOrder);

		// プレイヤーの座標、回転情報を取得しセット
		FVector PlayerPosition = EventInterface->OnGetFieldPosition(IntElement[ColFieldNum]);
		PlayerPosition += AdjustPos;
		FRotator PlayerRotation = FRotator(PlayerAngleX, PlayerAngleY, PlayerAngleZ);

		FActorSpawnParameters Params;
		// プレイヤーの生成
		APlayerBase* Player = GetWorld()->SpawnActor<APlayerBase>(PlayerCharacter, PlayerPosition, PlayerRotation, Params);
		if (!IsValid(Player)) {
			UE_LOG(LogTemp, Display, TEXT("APlayerManager PlayerSpawn Error"));
			return false;
		}

		// 配置座標をセット、初期化処理実行
		Player->SetOperationType(OperationType);
		Player->SetPlayerOrder(PlayerOrder);
		Player->SetFieldNum(IntElement[ColFieldNum]);
		Player->SetOperationOrder(i);
		Player->AddPocketMoney(IntElement[ColPocketMoney]);
		Player->ChangeStateType(EStateType::Play);
		Player->Initialize();
		// プレイヤーリスト / 順番リストに登録
		PlayerList.Add(PlayerOrder, Player);
		OperationOrderList.Add(i, PlayerOrder);
		AllPlayer.Add(PlayerOrder);

		// 一時配列の初期化
		Element.Empty();
		IntElement.Empty();
	}

	return true;
}

/// <summary>
/// プレイヤー通常移動処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <param name="Player"> プレイヤー </param>
/// <returns></returns>
void APlayerManager::MoveNormalPlayer(float DeltaTime, APlayerBase* Player) {

	// プレイヤーの操作タイプと現在のフェーズを取得
	EOperationType OperationType = Player->GetOperationType();
	switch (CurrentPhase) {
	case EOperatePhase::Wait: {
		//--------------------------
		//待機フェーズ
		//--------------------------
		NormalMoveAccumuTime = ResetTime;
		break;
	}
	case EOperatePhase::WaitRollDice: {
		//--------------------------
		//ダイス振り待機フェーズ
		//--------------------------
		switch (OperationType) {
		case EOperationType::Player: {
			break;
		}
		case EOperationType::CpuTypeA:
		case EOperationType::CpuTypeB:
		case EOperationType::CpuTypeC:
		case EOperationType::CpuTypeD:
		case EOperationType::CpuTypeE:
		case EOperationType::CpuTypeF: {

			// 累積時間を加算
			NormalMoveAccumuTime += DeltaTime;
			if (NormalMoveAccumuTime < RollDiceWaitTime) {
				break;
			}
			NormalMoveAccumuTime = ResetTime;
			// ダイスを振る
			OperationCpuRollDice();
			break;
		}
		}
		break;
	}
	case EOperatePhase::RollDice: {
		//--------------------------
		//ダイス振り中フェーズ
		//--------------------------
		switch (OperationType) {
		case EOperationType::Player: {
			break;
		}
		case EOperationType::CpuTypeA:
		case EOperationType::CpuTypeB:
		case EOperationType::CpuTypeC:
		case EOperationType::CpuTypeD:
		case EOperationType::CpuTypeE:
		case EOperationType::CpuTypeF: {

			// 累積時間を加算
			NormalMoveAccumuTime += DeltaTime;
			if (NormalMoveAccumuTime < WaitTime) {
				break;
			}
			NormalMoveAccumuTime = ResetTime;
			// ダイスを停止する
			OperationCpuStopDice();
			break;
		}
		}

		break;
	}
	case EOperatePhase::PrepareMove: {
		//--------------------------
		//移動準備フェーズ
		//--------------------------
		NormalMoveAccumuTime += DeltaTime;
		if (NormalMoveAccumuTime < 0.1f) {
			break;
		}
		NormalMoveAccumuTime = ResetTime;

		// 進む方向を取得
		TArray<int32> ArrayForward = EventInterface->OnSearchForward();
		if (ArrayForward.Num() == Zero) {
			// 進む移動マスが存在しない場合、
			// 現在のフェーズを「移動終了」に変更
			ChangeCurrentPhase(EOperatePhase::FinishMove);
		}
		else if (ArrayForward.Num() > One) {
			// 進む移動マスが複数存在する場合
			switch (OperationType) {
			case EOperationType::Player: {
				//------------------------
				// プレイヤー操作
				//------------------------
				// 現在のフェーズを「移動待機」に変更
				ChangeCurrentPhase(EOperatePhase::WaitMove);
				break;
			}
			case EOperationType::CpuTypeA:
			case EOperationType::CpuTypeB: 
			case EOperationType::CpuTypeC:
			case EOperationType::CpuTypeD:
			case EOperationType::CpuTypeE:
			case EOperationType::CpuTypeF: {
				//------------------------
				// CPU操作
				//------------------------
				// 現在のフェーズを「移動先検討」に変更
				ChangeCurrentPhase(EOperatePhase::ThinkMoveDirection);
				break;
			}
			}

			// 分岐選択開始処理
			EventInterface->OnPrepareSelectRoute();
		}
		else {
			// 進む移動マスが１つのみ存在する場合、
			// 現在のフェーズを「移動」に変更
			ExecuteMove(Player, ArrayForward[MinIndex], DeltaTime);
		}
		break;
	}
	case EOperatePhase::Move: {
		//--------------------------
		//移動フェーズ
		//--------------------------
		Player->SetActorLocation(CalcMovedPosition());
		EventInterface->OnFollowPlayCamera();

		bool IsArrive = IsArriveGoalPosition();
		if (!IsArrive) {
			break;
		}
		Player->SetActorLocation(GetGoalPosition());
		EventInterface->OnFollowPlayCamera();
		Player->StopJumping();

		// ダイスウィジェットの更新
		EventInterface->OnUpdateDiceWidget();
		// 現在のフェーズを「移動後」に変更
		ChangeCurrentPhase(EOperatePhase::Moved);
		break;
	}
	case EOperatePhase::Moved: {
		//--------------------------
		//移動後フェーズ
		//--------------------------
		// 現在のフェーズを「移動準備」に変更
		ChangeCurrentPhase(EOperatePhase::PrepareMove);

		// ゴールに到達したか確認
		bool IsGoal = EventInterface->OnCheckArriveGoal();
		if (IsGoal) {
			// プレイヤーの状態をゴールに変更
			Player->ChangeStateType(EStateType::Goal);
			// 現在のフェーズを「移動終了」に変更
			ChangeCurrentPhase(EOperatePhase::FinishMove);
			break;
		}

		// 残りのマスを算出
		ForwardNum -= 1;
		if (ForwardNum > Zero) {
			break;
		}
		// 現在のフェーズを「移動終了」に変更
		ChangeCurrentPhase(EOperatePhase::FinishMove);
		break;
	}
	case EOperatePhase::WaitMove: {
		//--------------------------
		//移動待機フェーズ
		//--------------------------
		break;
	}
	case EOperatePhase::ThinkMoveDirection: {
		//--------------------------
		//移動方向検討フェーズ
		//--------------------------
		NormalMoveAccumuTime += DeltaTime;
		if (NormalMoveAccumuTime < DecideMoveWaitTime) {
			break;
		}
		NormalMoveAccumuTime = ResetTime;

		ThinkMoveDirection();

		// 現在のフェーズを「CPUの移動先確定処理」に変更
		ChangeCurrentPhase(EOperatePhase::DecideCpu);
		break;
	}
	case EOperatePhase::DecideCpu: {
		//--------------------------
		//CPUの移動先確定フェーズ
		//--------------------------
		NormalMoveAccumuTime += DeltaTime;
		if (NormalMoveAccumuTime < DecideMoveWaitTime) {
			break;
		}
		NormalMoveAccumuTime = ResetTime;

		// 決定ボタンを押したことにする
		EventInterface->OnDecide();

		break;
	}
	case EOperatePhase::DecideMoveDirection: {
		//--------------------------
		//移動先決定フェーズ
		//--------------------------
		NormalMoveAccumuTime += DeltaTime;
		if (NormalMoveAccumuTime < DecideMoveWaitTime) {
			break;
		}
		NormalMoveAccumuTime = ResetTime;

		// 進むマスを取得する
		int32 NextPos = EventInterface->OnGetNextMovePos();
		// 移動処理を実行する
		ExecuteMove(Player, NextPos, DeltaTime);

		break;
	}
	}
}

/// <summary>
/// プレイヤー特殊移動処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <param name="Player"> プレイヤー </param>
/// <returns></returns>
void APlayerManager::MoveSpecialPlayer(float DeltaTime, APlayerBase* Player) {

	switch (CurrentMovePhase) {
	case EEventMovePhase::None: {
		//--------------------------
		//何もしないフェーズ
		//--------------------------
		break;
	}
	case EEventMovePhase::MovePrepare: {
		//--------------------------
		//移動準備フェーズ
		//--------------------------
		// 現在の座標と目標座標の差分から毎フレームの移動量を算出
		// プレイヤーの向きを変える
		SetMoveValue(NoramlMoveRate, DeltaTime);
		FRotator PlayerRot = FRotator(0.0f, 180.0f, 0.0f);
		Player->SetActorRotation(PlayerRot);

		// プレイヤーを無重力状態にし、宙に浮かせる
		Player->ChangeGravityScale(ZeroGravityValue);
		FVector Pos = Player->GetActorLocation();
		Pos.Z = WarpInitHeight;
		Player->SetActorLocation(Pos);

		// 現在の移動フェーズを「移動待機」に変更
		ChangeCurrentMovePhase(EEventMovePhase::MoveWait);
		break;
	}
	case EEventMovePhase::MoveWait: {
		//--------------------------
		//移動待機フェーズ
		//--------------------------
		SpecialMoveAccumuTime += DeltaTime;
		if (SpecialMoveAccumuTime <= MoveWaitTime) {
			return;
		}
		SpecialMoveAccumuTime = ResetTime;

		// プレイヤーを非表示にする
		Player->Hide();

		// 現在の移動フェーズを「移動」に変更
		ChangeCurrentMovePhase(EEventMovePhase::Move);
		break;
	}
	case EEventMovePhase::Move: {
		//--------------------------
		//移動フェーズ
		//--------------------------
		Player->SetActorLocation(CalcMovedPosition());
		EventInterface->OnFollowPlayCamera();

		// 目標座標に到達したかどうか
		bool IsArrive = IsArriveGoalPosition();
		if (!IsArrive) {
			break;
		}
		Player->ChangeGravityScale(GravityValue);
		Player->SetActorLocation(GetGoalPosition());
		EventInterface->OnFollowPlayCamera();
		Player->StopJumping();

		// プレイヤーを表示にする
		Player->Show();

		// 現在の移動フェーズを「移動終了」に変更
		ChangeCurrentMovePhase(EEventMovePhase::FinishMove);
		break;
	}
	case EEventMovePhase::FinishMove: {
		//--------------------------
		//移動終了フェーズ
		//--------------------------
		SpecialMoveAccumuTime += DeltaTime;
		if (SpecialMoveAccumuTime <= MoveWaitTime) {
			return;
		}
		SpecialMoveAccumuTime = ResetTime;

		// イベント処理を終了する
		EventInterface->OnFinishEvent();
		// 現在の移動フェーズを「何もしない」に変更
		ChangeCurrentMovePhase(EEventMovePhase::None);
		break;
	}
	}
}

/// <summary>
/// 指定プレイヤーを取得
/// </summary>
/// <param name="Order"> 識別番号 </param>
/// <returns> 指定プレイヤー </returns>
APlayerBase* APlayerManager::GetPlayer(EPlayerOrder Order) {

	if (!PlayerList.Contains(Order)) {
		return nullptr;
	}

	return PlayerList[Order];
}

/// <summary>
/// ターンをセット
/// </summary>
/// <param name="Turn"> 順番 </param>
/// <returns></returns>
void APlayerManager::SetCurrentTurn(int32 Turn) {

	CurrentTurn = Turn;
}

/// <summary>
/// ターンを取得
/// </summary>
/// <param name=""></param>
/// <returns> 順番 </returns>
int32 APlayerManager::GetCurrentTurn() {

	return CurrentTurn;
}

/// <summary>
/// プレイヤー識別を取得
/// </summary>
/// <param name="Turn"> 順番 </param>
/// <returns> 順番 </returns>
EPlayerOrder APlayerManager::GetOperationOrder(int32 Turn) {

	return OperationOrderList[Turn];
}

/// <summary>
/// 全てのプレイヤーの取得
/// </summary>
/// <param name=""></param>
/// <returns> プレイヤー配列 </returns>
TArray<EPlayerOrder> APlayerManager::GetAllPlayer() {

	return AllPlayer;
}

/// <summary>
/// プレイ人数を取得
/// </summary>
/// <param name=""></param>
/// <returns> プレイ人数 </returns>
int32 APlayerManager::GetPlayerNum() {

	return AllPlayer.Num();
}

/// <summary>
/// 進むマス数をセット
/// </summary>
/// <param name="Roll"> ダイスの出目 </param>
/// <returns></returns>
void APlayerManager::SetForwardNum(int32 Roll) {

	ForwardNum = Roll;
}

/// <summary>
/// 操作プレイヤーの切り替え
/// </summary>
/// <param name=""></param>
/// <returns> true:全プレイヤーゴール, false:プレイ中 </returns>
bool APlayerManager::SwitchOperationPlayer() {

	bool IsDecide = false;
	int32 Count = 0;
	int32 Turn = CurrentTurn;

	int32 FirstMoney = PlayerList[EPlayerOrder::First]->GetPocketMoney();
	int32 SecondMoney = PlayerList[EPlayerOrder::Second]->GetPocketMoney();
	int32 ThirdMoney = PlayerList[EPlayerOrder::Third]->GetPocketMoney();
	int32 FourthMoney = PlayerList[EPlayerOrder::Fourth]->GetPocketMoney();

	UE_LOG(LogTemp, Warning, TEXT("First:%d"), FirstMoney);
	UE_LOG(LogTemp, Warning, TEXT("Second:%d"), SecondMoney);
	UE_LOG(LogTemp, Warning, TEXT("Third:%d"), ThirdMoney);
	UE_LOG(LogTemp, Warning, TEXT("Fourth:%d"), FourthMoney);

	while (!IsDecide) {
		// 順番を１つ加算、プレイ人数を超えた場合は1番目に戻る
		++Turn;
		if (Turn > AllPlayer.Num()) {
			Turn = One;
		}

		// 順番からプレイヤー識別を取得、プレイヤーを取得する
		EPlayerOrder PlayerOrder = OperationOrderList[Turn];
		APlayerBase* Player = PlayerList[PlayerOrder];
		bool IsGoal = Player->IsArriveGoal();
		if (!IsGoal) {
			// ゴールしていない場合はフラグを立てて、
			// 操作プレイヤーをセットする
			IsDecide = true;
			SetCurrentTurn(Turn);
		}
		else {
			// ループカウントを加算
			++Count;
			// ループ回数がプレイ人数に達したら全員ゴール済み
			if (Count >= AllPlayer.Num()) {
				return true;
			}
		}
	}



	return false;
}

/// <summary>
/// 現在のフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void APlayerManager::ChangeCurrentPhase(EOperatePhase PhaseType) {

	CurrentPhase = PhaseType;
}

/// <summary>
/// 現在の移動フェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void APlayerManager::ChangeCurrentMovePhase(EEventMovePhase PhaseType) {

	CurrentMovePhase = PhaseType;
}

/// <summary>
/// 現在のフェーズを取得する
/// </summary>
/// <param name=""></param>
/// <returns> フェーズの種類 </returns>
EOperatePhase APlayerManager::GetCurrentPhase() {

	return CurrentPhase;
}

/// <summary>
/// プレイヤーがサイコロ振り待機中かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:待機中, false:それ以外 </returns>
bool APlayerManager::IsWaitRollDice() {

	return CurrentPhase == EOperatePhase::WaitRollDice;
}

/// <summary>
/// プレイヤーが移動待機中かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:待機中, false:それ以外 </returns>
bool APlayerManager::IsWaitMove() {

	return CurrentPhase == EOperatePhase::WaitMove;
}

/// <summary>
/// プレイヤーが思考中かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:思考中, false:思考中以外 </returns>
bool APlayerManager::IsThinkMoveDirection() {

	return CurrentPhase == EOperatePhase::ThinkMoveDirection;
}

/// <summary>
/// プレイヤーが移動中かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:移動中, false:移動以外 </returns>
bool APlayerManager::IsMove() {

	return CurrentPhase == EOperatePhase::Move;
}

/// <summary>
/// 移動操作が終了したかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:移動中, false:移動以外 </returns>
bool APlayerManager::IsFinishMove() {

	return CurrentPhase == EOperatePhase::FinishMove;
}

/// <summary>
/// ゴールの着順を取得
/// </summary>
/// <param name=""></param>
/// <returns> ゴールの着順 </returns>
int32 APlayerManager::GetArrivalGoalOrder() {

	int32 Order = Zero;
	for (auto Player : PlayerList) {
		// プレイヤーの状態を取得
		EStateType State = Player.Value->GetStateType();
		if (State != EStateType::Goal) {
			continue;
		}
		++Order;
	}

	return Order;
}

/// <summary>
/// サイコロを振る ※CPU操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerManager::OperationCpuRollDice() {

	EventInterface->OnRollDice();
	// サイコロ停止時間を乱数にて取得
	WaitTime = FMath::FRandRange(StopDiceLowWaitTime, StopDiceHighWaitTime);
}

/// <summary>
/// サイコロを止める ※CPU操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerManager::OperationCpuStopDice() {

	EventInterface->OnDecide();
}

/// <summary>
/// 移動処理を実行する
/// </summary>
/// <param name="Player"> プレイヤー </param>
/// <param name="Number"> フィールド番号 </param>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void APlayerManager::ExecuteMove(APlayerBase* Player, int32 Number, float DeltaTime) {

	// 現在のフェーズを「移動」に変更
	ChangeCurrentPhase(EOperatePhase::Move);

	// 進む座標を目標座標にセット
	Player->SetFieldNum(Number);
	FVector NextPos = EventInterface->OnGetFieldPosition(Number);
	FVector CurPos = Player->GetActorLocation();
	NextPos.Z = CurPos.Z;
	SetGoalPosition(NextPos);

	// 移動量をセット
	// プレイヤーをジャンプさせる
	if (ForwardNum == One) {
		SetMoveValue(FinalMoveRate, DeltaTime);
		AddImpulsePlayer(ImpValueX, ImpValueY, FinalImpValueZ);
	}
	else {
		SetMoveValue(NoramlMoveRate, DeltaTime);
		AddImpulsePlayer(ImpValueX, ImpValueY, NormalImpValueZ);
	}
}

/// <summary>
/// 移動方向を決定する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void APlayerManager::ThinkMoveDirection() {

	EventInterface->OnThinkSelectRoute();
}

/// <summary>
/// 目標座標をセット
/// </summary>
/// <param name="Position"> 目標座標 </param>
/// <returns></returns>
void APlayerManager::SetGoalPosition(FVector Position) {

	GoalPosition = Position;
}

/// <summary>
/// 目標座標を取得
/// </summary>
/// <param name=""></param>
/// <returns> 目標座標 </returns>
FVector APlayerManager::GetGoalPosition() {

	return GoalPosition;
}

/// <summary>
/// 待機座標の取得
/// </summary>
/// <param name="Order"> プレイヤー識別 </param>
/// <returns> 待機座標 </returns>
FVector APlayerManager::GetWaitAdjustPosition(EPlayerOrder Order) {

	FVector AdjustPos;

	switch (Order) {
	case EPlayerOrder::First: {
		AdjustPos = FVector(MinusAdjust, MinusAdjust, AdjustPosZ);
		break;
	}
	case EPlayerOrder::Second: {
		AdjustPos = FVector(PlusAdjust, MinusAdjust, AdjustPosZ);
		break;
	}
	case EPlayerOrder::Third: {
		AdjustPos = FVector(MinusAdjust, PlusAdjust, AdjustPosZ);
		break;
	}
	case EPlayerOrder::Fourth: {
		AdjustPos = FVector(PlusAdjust, PlusAdjust, AdjustPosZ);
		break;
	}
	}

	return AdjustPos;
}

/// <summary>
/// 移動量をセット
/// </summary>
/// <param name="Rate"> 移動時間の割合 </param>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void APlayerManager::SetMoveValue(float Rate, float DeltaTime) {

	// 現在操作中のプレイヤーを取得
	EPlayerOrder Order = OperationOrderList[CurrentTurn];
	APlayerBase* Player = GetPlayer(Order);
	FVector CurPos = Player->GetActorLocation();

	// 目標位置と現在位置の差分を取得
	FVector DiffDistance = GoalPosition - CurPos;
	// 指定時間で移動するように移動量を設定
	MoveValue = DiffDistance * DeltaTime * Rate;
	MoveValue.Z = InitMoveValue;
}

/// <summary>
/// 移動量を取得
/// </summary>
/// <param name=""></param>
/// <returns> 移動量 </returns>
FVector APlayerManager::GetMoveValue() {

	return MoveValue;
}

/// <summary>
/// 移動後の座標を取得
/// </summary>
/// <param name=""></param>
/// <returns> 移動後の座標 </returns>
FVector APlayerManager::CalcMovedPosition() {

	// 現在操作中のプレイヤーを取得
	EPlayerOrder Order = OperationOrderList[CurrentTurn];
	APlayerBase* Player = GetPlayer(Order);
	FVector CurPos = Player->GetActorLocation();

	CurPos.X += MoveValue.X;
	CurPos.Y += MoveValue.Y;

	return CurPos;
}

/// <summary>
/// プレイヤーに力積を与える
/// </summary>
/// <param name="ValueX"> X軸方向のベクトル </param>
/// <param name="ValueY"> Y軸方向のベクトル </param>
/// <param name="ValueZ"> Z軸方向のベクトル </param>
/// <returns></returns>
void APlayerManager::AddImpulsePlayer(float ValueX, float ValueY, float ValueZ) {

	// 対象プレイヤーを取得
	EPlayerOrder Order = OperationOrderList[CurrentTurn];
	APlayerBase* Player = GetPlayer(Order);

	// 力積のベクトルを生成
	FVector Power = FVector(ValueX, ValueY, ValueZ);
	Player->GetCharacterMovement()->AddImpulse(Power);
}

/// <summary>
/// 目標座標に到達したかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:到達した, flase:到達していない </returns>
bool APlayerManager::IsArriveGoalPosition() {

	bool IsArrivePosX = false;
	bool IsArrivePosY = false;

	// 各座標の取得
	EPlayerOrder Order = OperationOrderList[CurrentTurn];
	APlayerBase* Player = GetPlayer(Order);
	FVector CurrentPos = Player->GetActorLocation();
	FVector GoalPos = GetGoalPosition();
	FVector MoveVector = GetMoveValue();

	//------------------------------------
	// X座標
	//------------------------------------
	if (MoveVector.X > Zerof) {
		// 移動量が「0」より大きい数
		if (CurrentPos.X >= GoalPos.X) {
			// 目標座標以上になったら到達したとする
			CurrentPos.X = GoalPos.X;
			IsArrivePosX = true;
		}
	}
	else if (MoveVector.X < Zerof) {
		// 移動量が「0」より小さい数
		if (CurrentPos.X <= GoalPos.X) {
			// 目標座標以下になったら到達したとする
			CurrentPos.X = GoalPos.X;
			IsArrivePosX = true;
		}
	}
	else {
		// それ以外
		IsArrivePosX = true;
	}

	//------------------------------------
	// Y座標
	//------------------------------------
	if (MoveVector.Y > Zerof) {
		// 移動量が「0」より大きい数
		if (CurrentPos.Y >= GoalPos.Y - 10.0f) {
			// 目標座標以上になったら到達したとする
			IsArrivePosY = true;
		}
	}
	else if (MoveVector.Y < Zerof) {
		// 移動量が「0」より小さい数
		if (CurrentPos.Y <= GoalPos.Y - 10.0f) {
			// 目標座標以下になったら到達したとする
			IsArrivePosY = true;
		}
	}
	else {
		// それ以外
		IsArrivePosY = true;
	}

	if (IsArrivePosX && IsArrivePosY) {
		return true;
	}

	return false;
}