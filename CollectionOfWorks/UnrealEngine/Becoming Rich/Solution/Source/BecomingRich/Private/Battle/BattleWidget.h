﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget.h"
#include "Battle/PlayerBase.h"
#include "BattleWidget.generated.h"

/// <summary>
/// 所持金情報
/// </summary>
struct MoneyData {
	int32 CurrentMoney;
	int32 AmountMoney;
	int32 AddMoney;
};

/// <summary>
/// 所持金パネル位置情報
/// </summary>
struct PlayerMoneyPos {
	FVector2D CurrentPos;
	FVector2D GoalPos;
	FVector2D AddPos;
};

/// <summary>
/// プレイヤーの状態
/// </summary>
struct PlayerState {
	EStateType OldState;
	EStateType CurrentState;
};

/// <summary>
/// 所持金パネル切り替えフェーズの種類
/// </summary>
UENUM()
enum class EMoneyPanelPhase {
	Wait,
	Sort,
	MovePrepare,
	MoveStart,
	MoveWait,
	Move,
	MoveEnd,
	MaxPhaseIndex
};

/// <summary>
/// ターン切り替えフェーズの種類
/// </summary>
UENUM()
enum class ETurnPanelPhase {
	Wait,
	HidePanel,
	MovePanel,
	ShowPanel,
	MaxPhaseIndex
};

/// <summary>
/// 順位
/// </summary>
UENUM()
enum class ERank {
	First,
	Second,
	Third,
	Fourth,
	MaxRankIndex
};

/// <summary>
/// バトルレベルHUD
/// </summary>
UCLASS()
class UBattleWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// 現在のプレイヤーターンをセット
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <returns></returns>
	void SetCurrentTurn(EPlayerOrder Order);

	/// <summary>
	/// 順位情報のセット
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <returns></returns>
	void SetRank(EPlayerOrder Order);

	/// <summary>
	/// プレイヤーの状態をセット
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <param name="State"> 状態の種類 </param>
	/// <returns></returns>
	void SetInitPlayerState(EPlayerOrder Order, EStateType State);

	/// <summary>
	/// プレイヤーの状態をゴール到達にする
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <returns></returns>
	void SetPlayerStateGoal(EPlayerOrder Order);

	/// <summary>
	/// 指定プレイヤーがプレイ中かどうか
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <returns> true:プレイ中, false:それ以外 </returns>
	bool IsPlayerPlaying(EPlayerOrder Order);

	/// <summary>
	/// 所持金を獲得する
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <param name="AddMoney"> 加算額 </param>
	/// <returns></returns>
	void MakeMoney(EPlayerOrder Order, int32 AddMoney);

	/// <summary>
	/// プレイヤーの所持金をテキストセット
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <param name="MoneyValue"> 所持金 </param>
	/// <returns></returns>
	void SetPlayerMoney(EPlayerOrder Order, int32 MoneyValue);

	/// <summary>
	/// 所持金パネル切り替え処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void SwitchMoneyPanel(float DeltaTime);

	/// <summary>
	/// ターンパネル切り替え処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void SwitchTurnPanel(float DeltaTime);

	/// <summary>
	/// ターンフレーム切り替え処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SwitchTurnFrame();

	/// <summary>
	/// ターンフレームを表示処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ShowTurnFrame();

	/// <summary>
	/// ターンフレーム停止処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void StopTurnFrame();

	/// <summary>
	/// 現在のターン切り替えフェーズを変更
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ChangeCurrentTurnPhase(ETurnPanelPhase PhaseType);

	/// <summary>
	/// 現在の所持金パネル切り替えフェーズを変更
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ChangeCurrentMoneyPhase(EMoneyPanelPhase PhaseType);

	/// <summary>
	/// 指定順位のプレイヤー識別を取得
	/// </summary>
	/// <param name="Rank"> ランク </param>
	/// <returns> プレイヤー識別 </returns>
	EPlayerOrder GetPlayerOrder(ERank Rank);

	/// <summary>
	/// 指定プレイヤーの所持金を取得
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <returns> 所持金 </returns>
	int32 GetPocketMoney(EPlayerOrder Order);

	/// <summary>
	/// 指定プレイヤーの所持金パネルを表示する
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void ShowMoneyPanelWidget(EPlayerOrder Order);

	/// <summary>
	/// 指定プレイヤーの所持金パネルを非表示する
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void HideMoneyPanelWidget(EPlayerOrder Order);

	/// <summary>
	/// ターンフレームを表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void ShowTurnFrame01();

	/// <summary>
	/// ターンフレームを表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void ShowTurnFrame02();

	/// <summary>
	/// ターンフレームを表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void ShowTurnFrame03();

	/// <summary>
	/// ターンフレームを表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void ShowTurnFrame04();

	/// <summary>
	/// ターンフレームを非表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void HideTurnFrame();

	/// <summary>
	/// 指定プレイヤーのターンパネルを表示する
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void ShowTurnPanelWidget(EPlayerOrder Order);

	/// <summary>
	/// 指定プレイヤーのターンパネルを表示する ※透過率は「0」
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void ShowTurnPanelWidgetInvisible(EPlayerOrder Order);

	/// <summary>
	/// 指定プレイヤーのターンパネルを非表示する
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void HideTurnPanelWidget(EPlayerOrder Order);

	/// <summary>
	/// ターンパネル横の矢印を表示する
	/// </summary>
	/// <param name="Number"> プレイ中の人数 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void ShowTurnArrowWidget(int32 Number);

	/// <summary>
	/// ターンパネル横の矢印を非表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void HideTurnArrowWidget();

	/// <summary>
	/// Finishウィジェットの非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void HideFinishWidget();

	/// <summary>
	/// Finishウィジェットの表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void ShowFinishWidget();

	/// <summary>
	/// ターン表示パネルの移動
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <param name="Position"> 座標 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void MovePositionTurnPanel(EPlayerOrder Order, FVector2D Position);

	/// <summary>
	/// ターン開始アニメーション1P用 実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void PlayTurnStart01Animation();

	/// <summary>
	/// ターン開始アニメーション2P用 実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void PlayTurnStart02Animation();

	/// <summary>
	/// ターン開始アニメーション3P用 実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void PlayTurnStart03Animation();

	/// <summary>
	/// ターン開始アニメーション4P用 実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void PlayTurnStart04Animation();

	/// <summary>
	/// 順番入れ替えフェードアウトアニメーション実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void PlayOrderFadeOutAnimation();

	/// <summary>
	/// 順番入れ替えフェードインアニメーション実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void PlayOrderFadeInAnimation();

	/// <summary>
	/// ターンフレームのアニメーションを開始する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void PlayTurnFrame01Animation();

	/// <summary>
	/// ターンフレームのアニメーションを開始する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void PlayTurnFrame02Animation();

	/// <summary>
	/// ターンフレームのアニメーションを開始する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void PlayTurnFrame03Animation();

	/// <summary>
	/// ターンフレームのアニメーションを開始する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void PlayTurnFrame04Animation();

	/// <summary>
	/// ターンフレームのアニメーションを停止する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void StopTurnFrame01Animation();

	/// <summary>
	/// ターンフレームのアニメーションを停止する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void StopTurnFrame02Animation();

	/// <summary>
	/// ターンフレームのアニメーションを停止する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void StopTurnFrame03Animation();

	/// <summary>
	/// ターンフレームのアニメーションを停止する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void StopTurnFrame04Animation();

	/// <summary>
	/// 所持金パネル拡大演出 1P用
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void ExpandMoneyPanel1P();

	/// <summary>
	/// 所持金パネル拡大演出 2P用
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void ExpandMoneyPanel2P();

	/// <summary>
	/// 所持金パネル拡大演出 3P用
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void ExpandMoneyPanel3P();

	/// <summary>
	/// 所持金パネル拡大演出 4P用
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void ExpandMoneyPanel4P();

	/// <summary>
	/// 所持金パネル移動処理 1P用
	/// </summary>
	/// <param name="Position"> 配置座標 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void MovePositionMoneyPanel1P(FVector2D Position);

	/// <summary>
	/// 所持金パネル移動処理 2P用
	/// </summary>
	/// <param name="Position"> 配置座標 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void MovePositionMoneyPanel2P(FVector2D Position);

	/// <summary>
	/// 所持金パネル移動処理 3P用
	/// </summary>
	/// <param name="Position"> 配置座標 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void MovePositionMoneyPanel3P(FVector2D Position);

	/// <summary>
	/// 所持金パネル移動処理 4P用
	/// </summary>
	/// <param name="Position"> 配置座標 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BattleWidget")
	void MovePositionMoneyPanel4P(FVector2D Position);


protected:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

	/// <summary>
	/// Tick処理
	/// </summary>
	/// <param name="MyGeometry"> ジオメトリー </param>
	/// <param name="InDaltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void NativeTick(const FGeometry& MyGeometry, float InDaltaTime) override;

private:

	/// <summary>
	/// 所持金パネルウィジェットの表示
	/// </summary>
	/// <param name="PlayerOrder"> プレイヤー識別 </param>
	/// <returns></returns>
	virtual void ShowMoneyPanelWidget_Implementation(EPlayerOrder PlayerOrder);

	/// <summary>
	/// 所持金パネルウィジェットの非表示
	/// </summary>
	/// <param name="PlayerOrder"> プレイヤー識別 </param>
	/// <returns></returns>
	virtual void HideMoneyPanelWidget_Implementation(EPlayerOrder PlayerOrder);

	/// <summary>
	/// ターンフレームを表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ShowTurnFrame01_Implementation();

	/// <summary>
	/// ターンフレームを表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ShowTurnFrame02_Implementation();

	/// <summary>
	/// ターンフレームを表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ShowTurnFrame03_Implementation();

	/// <summary>
	/// ターンフレームを表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ShowTurnFrame04_Implementation();

	/// <summary>
	/// ターンフレームを非表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void HideTurnFrame_Implementation();

	/// <summary>
	/// ターンパネルウィジェットの表示
	/// </summary>
	/// <param name="PlayerOrder"> プレイヤー識別 </param>
	/// <returns></returns>
	virtual void ShowTurnPanelWidget_Implementation(EPlayerOrder PlayerOrder);

	/// <summary>
	/// 指定プレイヤーのターンパネルを表示する ※透過率は「0」
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <returns></returns>
	virtual void ShowTurnPanelWidgetInvisible_Implementation(EPlayerOrder Order);

	/// <summary>
	/// ターンパネルウィジェットの非表示
	/// </summary>
	/// <param name="PlayerOrder"> プレイヤー識別 </param>
	/// <returns></returns>
	virtual void HideTurnPanelWidget_Implementation(EPlayerOrder PlayerOrder);

	/// <summary>
	/// ターンパネル横の矢印を表示する
	/// </summary>
	/// <param name="Number"> プレイ中の人数 </param>
	/// <returns></returns>
	virtual void ShowTurnArrowWidget_Implementation(int32 Number);

	/// <summary>
	/// ターンパネル横の矢印を非表示する
	/// </summary>
	/// <param name="Number"> プレイ中の人数 </param>
	/// <returns></returns>
	virtual void HideTurnArrowWidget_Implementation();

	/// <summary>
	/// Finishウィジェットの非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void HideFinishWidget_Implementation();

	/// <summary>
	/// Finishウィジェットの表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ShowFinishWidget_Implementation();

	/// <summary>
	/// ターン表示パネルの移動
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <param name="Position"> 座標 </param>
	/// <returns></returns>
	virtual void MovePositionTurnPanel_Implementation(EPlayerOrder Order, FVector2D Position);

	/// <summary>
	/// ターン開始アニメーション実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayTurnStart01Animation_Implementation();

	/// <summary>
	/// ターン開始アニメーション実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayTurnStart02Animation_Implementation();

	/// <summary>
	/// ターン開始アニメーション実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayTurnStart03Animation_Implementation();

	/// <summary>
	/// ターン開始アニメーション実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayTurnStart04Animation_Implementation();

	/// <summary>
	/// 順番入れ替えフェードアウトアニメーション実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayOrderFadeOutAnimation_Implementation();

	/// <summary>
	/// 順番入れ替えフェードインアニメーション実行
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayOrderFadeInAnimation_Implementation();

	/// <summary>
	/// ターンフレームのアニメーションを開始する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayTurnFrame01Animation_Implementation();

	/// <summary>
	/// ターンフレームのアニメーションを開始する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayTurnFrame02Animation_Implementation();

	/// <summary>
	/// ターンフレームのアニメーションを開始する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayTurnFrame03Animation_Implementation();

	/// <summary>
	/// ターンフレームのアニメーションを開始する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PlayTurnFrame04Animation_Implementation();

	/// <summary>
	/// ターンフレームのアニメーションを停止する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void StopTurnFrame01Animation_Implementation();

	/// <summary>
	/// ターンフレームのアニメーションを停止する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void StopTurnFrame02Animation_Implementation();

	/// <summary>
	/// ターンフレームのアニメーションを停止する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void StopTurnFrame03Animation_Implementation();

	/// <summary>
	/// ターンフレームのアニメーションを停止する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void StopTurnFrame04Animation_Implementation();

	/// <summary>
	/// 所持金パネル拡大演出 1P用
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ExpandMoneyPanel1P_Implementation();

	/// <summary>
	/// 所持金パネル拡大演出 2P用
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ExpandMoneyPanel2P_Implementation();

	/// <summary>
	/// 所持金パネル拡大演出 3P用
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ExpandMoneyPanel3P_Implementation();

	/// <summary>
	/// 所持金パネル拡大演出 4P用
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ExpandMoneyPanel4P_Implementation();

	/// <summary>
	/// 所持金パネル移動処理 1P用
	/// </summary>
	/// <param name="Position"> 配置座標 </param>
	/// <returns></returns>
	virtual void MovePositionMoneyPanel1P_Implementation(FVector2D Position);

	/// <summary>
	/// 所持金パネル移動処理 2P用
	/// </summary>
	/// <param name="Position"> 配置座標 </param>
	/// <returns></returns>
	virtual void MovePositionMoneyPanel2P_Implementation(FVector2D Position);

	/// <summary>
	/// 所持金パネル移動処理 3P用
	/// </summary>
	/// <param name="Position"> 配置座標 </param>
	/// <returns></returns>
	virtual void MovePositionMoneyPanel3P_Implementation(FVector2D Position);

	/// <summary>
	/// 所持金パネル移動処理 4P用
	/// </summary>
	/// <param name="Position"> 配置座標 </param>
	/// <returns></returns>
	virtual void MovePositionMoneyPanel4P_Implementation(FVector2D Position);

	/// <summary>
	/// ターンフレーム移動処理
	/// </summary>
	/// <param name="Position"> 配置座標 </param>
	/// <returns></returns>
	virtual void MovePositionTurnFrame_Implementation(FVector2D Position);

	/// <summary>
	/// ターンごとのターンパネル表示位置
	/// </summary>
	TMap<int32, FVector2D> TurnPanelPosition;

	/// <summary>
	/// プレイヤー情報 順位ごとの表示位置
	/// </summary>
	TMap<ERank, FVector2D> MoneyPanelPosition;

	/// <summary>
	/// 操作順序
	/// </summary>
	TMap<int32, EPlayerOrder> ArrayOperationOrder;

	/// <summary>
	/// プレイヤーの状態
	/// </summary>
	TMap<EPlayerOrder, PlayerState> ArrayPlayerState;

	/// <summary>
	/// 現在のターン
	/// </summary>
	EPlayerOrder CurrentTurn;

	/// <summary>
	/// 順位情報
	/// </summary>
	TMap<ERank, EPlayerOrder> ArrayPlayerOrder;

	/// <summary>
	/// プレイヤーの所持金
	/// </summary>
	TMap<EPlayerOrder, MoneyData> ArrayPlayerMoney;

	/// <summary>
	/// 所持金パネル位置情報
	/// </summary>
	TMap<EPlayerOrder, PlayerMoneyPos> ArrayPlayerMoneyPos;

	/// <summary>
	/// ターンパネル位置情報
	/// </summary>
	TMap<EPlayerOrder, FVector2D> ArrayTurnPanelPos;

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	ETurnPanelPhase CurrentTurnPhase;

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	EMoneyPanelPhase CurrentMoneyPhase;

	/// <summary>
	/// 累積時間 ターンパネル用
	/// </summary>
	float TurnAccumulateTime;

	/// <summary>
	/// 累積時間 所持金パネル移動用
	/// </summary>
	float MoneyAccumulateTime;


private:

	/// <summary>
	/// ターン表示
	/// </summary>
	const FString TurnText = "P TURN";

	/// <summary>
	/// ターン開始アニメーション 1P用
	/// </summary>
	const FName TurnStart1pAnimPath = "TurnStart1PAnim";

	/// <summary>
	/// ターン開始アニメーション 2P用
	/// </summary>
	const FName TurnStart2pAnimPath = "TurnStart2PAnim";

	/// <summary>
	/// ターン開始アニメーション 3P用
	/// </summary>
	const FName TurnStart3pAnimPath = "TurnStart3PAnim";

	/// <summary>
	/// ターン開始アニメーション 4P用
	/// </summary>
	const FName TurnStart4pAnimPath = "TurnStart4PAnim";

	/// <summary>
	/// 所持金パネル表示位置 X座標
	/// </summary>
	const float MoneyPanelPosX = 1365.0f;

	/// <summary>
	/// 所持金パネル表示位置 Y座標
	/// </summary>
	const float MoneyPanelPosY = 40.0f;

	/// <summary>
	/// 所持金パネル表示位置 Y座標
	/// </summary>
	const float MoneyPanelDifferY = 110.0f;

	/// <summary>
	/// ターンパネル表示位置 Y座標
	/// </summary>
	const float TurnPanelPosY = 50.0f;

	/// <summary>
	/// ターンパネル表示位置 X座標
	/// </summary>
	const float TurnPanelPosX = 50.0f;

	/// <summary>
	/// ターンパネル表示位置 X座標
	/// </summary>
	const float TurnPanelDifferX = 175.0f;

	/// <summary>
	/// ターンフレーム調整値
	/// </summary>
	const FVector2D AdjustPos = FVector2D(-5.0f, -5.0f);

	/// <summary>
	/// 初期位置
	/// </summary>
	const float InitPosition = 0.0f;

	/// <summary>
	/// 順番最大値
	/// </summary>
	const int32 OrderMax = 4;

	/// <summary>
	/// ゼロ
	/// </summary>
	const int32 Zero = 0;

	/// <summary>
	/// ゼロ
	/// </summary>
	const float Zerof = 0.0f;

	/// <summary>
	/// 壱
	/// </summary>
	const int32 One = 1;

	/// <summary>
	/// 参
	/// </summary>
	const int32 Three = 3;

	/// <summary>
	/// マイナス符号
	/// </summary>
	const int32 MinusSign = -1;

	/// <summary>
	/// 所持金計算処理時間
	/// </summary>
	const int32 ProcessSeconds = 2;

	/// <summary>
	/// フレーム数
	/// </summary>
	const float Frames = 30.0f;

	/// <summary>
	/// リセット時間
	/// </summary>
	const float ResetTime = 0.0f;

	/// <summary>
	/// ターンパネル待機時間
	/// </summary>
	const float ShowPanelWaitTime = 0.55f;

	/// <summary>
	/// 所持金パネル移動待機時間
	/// </summary>
	const float MoveWaitTime = 0.5f;

	/// <summary>
	/// 所持金パネル移動終了時間
	/// </summary>
	const float MoveFinishTime = 1.5f;

	/// <summary>
	/// 所持金パネル移動割合
	/// </summary>
	const float MoveRate = 1.1f;
};