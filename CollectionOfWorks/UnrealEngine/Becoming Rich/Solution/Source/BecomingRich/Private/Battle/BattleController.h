﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Battle/BattleEvent.h"
#include "Battle/BattleUiManager.h"
#include "../SoundManager.h"
#include "BattleController.generated.h"

/// <summary>
/// バトルコントローラ
/// </summary>
UCLASS()
class ABattleController : public APlayerController
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// OpenBattle
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OpenBattle();

	/// <summary>
	/// ClosePreparing
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ClosePreparing();

	/// <summary>
	/// CloseBattle
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CloseBattle();

	/// <summary>
	/// バトルウィジェットを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> バトルウィジェット </returns>
	UBattleWidget* GetBattleWidget();

	/// <summary>
	/// リザルトウィジェットを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> リザルトウィジェット </returns>
	UResultWidget* GetResultWidget();

	/// <summary>
	/// ダイスウィジェットを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ダイスウィジェット </returns>
	UDiceWidget* GetDiceWidget();

	/// <summary>
	/// ダイアログウィジェットを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ダイアログウィジェット </returns>
	UDialogWidget* GetDialogWidget();

	/// <summary>
	/// サウンドマネージャを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> サウンドマネージャ </returns>
	ASoundManager* GetSoundManager();

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	/// <param name=""> イベントインターフェース </param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "EventInterface")
	void SetEventInterface(const TScriptInterface<IBattleEvent>& Interface);

	/// <summary>
	/// ダイス切り替え時間変更
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ChangeDiceTime();

	/// <summary>
	/// マップ用カメラの切り替え
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SwitchMapCamera();

	/// <summary>
	/// サイコロを振る
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressSpace();

	/// <summary>
	/// 決定処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressEnter();

	/// <summary>
	/// バトルレベル終了
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void FinishGame();

private:

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IBattleEvent> EventInterface;

	/// <summary>
	/// サウンドマネージャクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<ASoundManager> SoundManagerClass;

	/// <summary>
	/// サウンドマネージャ
	/// </summary>
	UPROPERTY()
	ASoundManager* SoundManager;

protected:

	/// <summary>
	/// SetupinputComponent
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void SetupInputComponent() override;

	/// <summary>
	/// BeginPlay
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

	/// <summary>
	/// 毎フレームの処理
	/// </summary>
	/// <param name="InDeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float InDeltaTime) override;


private:
	//--------------------------------
	//以下、定数宣言
	//--------------------------------
	/// <summary>
	/// 前進移動量
	/// </summary>
	const float ForwardMoveValue = 1.0f;

	/// <summary>
	/// 後退移動量
	/// </summary>
	const float BackMoveValue = -1.0f;

	/// <summary>
	/// 右移動量
	/// </summary>
	const float RightMoveValue = 1.0f;

	/// <summary>
	/// 左移動量
	/// </summary>
	const float LeftMoveValue = -1.0f;

	/// <summary>
	/// サウンドマネージャパス
	/// </summary>
	const FString SoundManagerPath = "/Game/Blueprints/BP_SoundManager.BP_SoundManager_C";
};
