﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/BattleWidget.h"
#include "Components/TextBlock.h"
#include "Blueprint/WidgetLayoutLibrary.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::NativeConstruct() {

	Super::NativeConstruct();

	// 累積時間リセット
	TurnAccumulateTime = ResetTime;
	MoneyAccumulateTime = ResetTime;

	// フェーズセット
	CurrentTurnPhase = ETurnPanelPhase::Wait;
	CurrentMoneyPhase = EMoneyPanelPhase::Wait;

	// 所持金ベース表示位置セット
	FVector2D Pos = FVector2D(MoneyPanelPosX, MoneyPanelPosY);
	for (int32 i = 0; i < OrderMax; ++i) {
		ERank Rank = static_cast<ERank>(i);
		MoneyPanelPosition.Add(Rank, Pos);
		Pos.Y += MoneyPanelDifferY;
	}
	// ターン表示位置セット
	Pos = FVector2D(TurnPanelPosX, TurnPanelPosY);
	for (int32 i = 0; i < OrderMax; ++i) {
		TurnPanelPosition.Add(i, Pos);
		Pos.X += TurnPanelDifferX;
	}

	// 所持金 初期設定
	MoneyData Money;
	Money.CurrentMoney = Zero;
	Money.AmountMoney = Zero;
	Money.AddMoney = Zero;
	ArrayPlayerMoney.Add(EPlayerOrder::First, Money);
	ArrayPlayerMoney.Add(EPlayerOrder::Second, Money);
	ArrayPlayerMoney.Add(EPlayerOrder::Third, Money);
	ArrayPlayerMoney.Add(EPlayerOrder::Fourth, Money);

	// 順位 初期設定
	ArrayPlayerOrder.Add(ERank::First, EPlayerOrder::None);
	ArrayPlayerOrder.Add(ERank::Second, EPlayerOrder::None);
	ArrayPlayerOrder.Add(ERank::Third, EPlayerOrder::None);
	ArrayPlayerOrder.Add(ERank::Fourth, EPlayerOrder::None);

	// プレイヤーの状態
	PlayerState State;
	State.CurrentState = EStateType::None;
	State.OldState = EStateType::None;
	ArrayPlayerState.Add(EPlayerOrder::First, State);
	ArrayPlayerState.Add(EPlayerOrder::Second, State);
	ArrayPlayerState.Add(EPlayerOrder::Third, State);
	ArrayPlayerState.Add(EPlayerOrder::Fourth, State);

	// 所持金パネルの表示位置 初期設定
	PlayerMoneyPos MoneyPos;
	// 1P 
	MoneyPos.CurrentPos = MoneyPanelPosition[ERank::First];
	MoneyPos.GoalPos = MoneyPanelPosition[ERank::First];
	MoneyPos.AddPos = FVector2D(InitPosition, InitPosition);
	ArrayPlayerMoneyPos.Add(EPlayerOrder::First, MoneyPos);
	// 2P 
	MoneyPos.CurrentPos = MoneyPanelPosition[ERank::Second];
	MoneyPos.GoalPos = MoneyPanelPosition[ERank::Second];
	MoneyPos.AddPos = FVector2D(InitPosition, InitPosition);
	ArrayPlayerMoneyPos.Add(EPlayerOrder::Second, MoneyPos);
	// 3P 
	MoneyPos.CurrentPos = MoneyPanelPosition[ERank::Third];
	MoneyPos.GoalPos = MoneyPanelPosition[ERank::Third];
	MoneyPos.AddPos = FVector2D(InitPosition, InitPosition);
	ArrayPlayerMoneyPos.Add(EPlayerOrder::Third, MoneyPos);
	// 4P 
	MoneyPos.CurrentPos = MoneyPanelPosition[ERank::Fourth];
	MoneyPos.GoalPos = MoneyPanelPosition[ERank::Fourth];
	MoneyPos.AddPos = FVector2D(InitPosition, InitPosition);
	ArrayPlayerMoneyPos.Add(EPlayerOrder::Fourth, MoneyPos);

	// ターンパネルの初期表示位置
	ArrayTurnPanelPos.Add(EPlayerOrder::Fourth, TurnPanelPosition[0]);
	ArrayTurnPanelPos.Add(EPlayerOrder::First, TurnPanelPosition[1]);
	ArrayTurnPanelPos.Add(EPlayerOrder::Second, TurnPanelPosition[2]);
	ArrayTurnPanelPos.Add(EPlayerOrder::Third, TurnPanelPosition[3]);

	// ターンフレームを非表示にする
	HideTurnFrame();
	// フィニッシュウィジェットを非表示にする
	HideFinishWidget();
}

/// <summary>
/// Tick処理
/// </summary>
/// <param name="MyGeometry"> ジオメトリー </param>
/// <param name="InDaltaTime"> 処理時間 </param>
/// <returns></returns>
void UBattleWidget::NativeTick(const FGeometry& MyGeometry, float InDaltaTime) {

	Super::NativeTick(MyGeometry, InDaltaTime);

	//-----------------------
	// 所持金の計算処理
	//-----------------------
	for (auto Player : ArrayPlayerMoney) {

		// 所持金に変化がない場合は処理終了
		if (Player.Value.CurrentMoney == Player.Value.AmountMoney) {
			continue;
		}

		// 加算額を加算
		ArrayPlayerMoney[Player.Key].CurrentMoney += Player.Value.AddMoney;
		// 現在の金額が目標金額を超えないよう確認
		if (Player.Value.AddMoney > Zero) {
			if (ArrayPlayerMoney[Player.Key].CurrentMoney >= Player.Value.AmountMoney) {
				ArrayPlayerMoney[Player.Key].CurrentMoney = Player.Value.AmountMoney;
				ArrayPlayerMoney[Player.Key].AddMoney = Zero;
			}
		}
		else if (Player.Value.AddMoney < Zero) {
			if ((ArrayPlayerMoney[Player.Key].CurrentMoney <= Player.Value.AmountMoney)) {
				ArrayPlayerMoney[Player.Key].CurrentMoney = Player.Value.AmountMoney;
				ArrayPlayerMoney[Player.Key].AddMoney = Zero;
			}
		}

		// ウィジェットに金額出力
		SetPlayerMoney(Player.Key, ArrayPlayerMoney[Player.Key].CurrentMoney);
	}

	//-----------------------
	// 所持金パネル切り替え処理
	//-----------------------
	SwitchMoneyPanel(InDaltaTime);

	//-----------------------
	// ターンパネル切り替え処理
	//-----------------------
	//SwitchTurnPanel(InDaltaTime);
}

/// <summary>
/// 現在のターンをセット
/// </summary>
/// <param name="Order"> プレイヤー識別 </param>
/// <returns></returns>
void UBattleWidget::SetCurrentTurn(EPlayerOrder Order) {

	UTextBlock* TextBlock = Cast<UTextBlock>(GetWidgetFromName("TextBlock_TurnPanel"));
	if (!IsValid(TextBlock)) {
		return;
	}
	FString StringText = FString::FromInt(static_cast<int>(Order));
	StringText.Append(TurnText);
	TextBlock->SetText(FText::FromString(StringText));

	// 現在のターンをセット
	CurrentTurn = Order;
}

/// <summary>
/// 順位情報のセット
/// </summary>
/// <param name="Order"> プレイヤー識別 </param>
/// <returns></returns>
void UBattleWidget::SetRank(EPlayerOrder Order) {

	TArray<ERank> ArrayRank;
	ArrayRank.Add(ERank::First);
	ArrayRank.Add(ERank::Second);
	ArrayRank.Add(ERank::Third);
	ArrayRank.Add(ERank::Fourth);

	for (ERank Rank : ArrayRank) {
		// 既にプレイヤー識別がセットされていたら次のランクへ
		if (ArrayPlayerOrder[Rank] != EPlayerOrder::None) {
			continue;
		}
		ArrayPlayerOrder[Rank] = Order;
		break;
	}
}

/// <summary>
/// プレイヤーの状態をセット
/// </summary>
/// <param name="Order"> プレイヤー識別 </param>
/// <param name="State"> 状態の種類 </param>
/// <returns></returns>
void UBattleWidget::SetInitPlayerState(EPlayerOrder Order, EStateType State) {

	ArrayPlayerState[Order].CurrentState = State;
	ArrayPlayerState[Order].OldState = State;
}

/// <summary>
/// プレイヤーの状態をゴール到達にする
/// </summary>
/// <param name="Order"> プレイヤー識別 </param>
/// <returns></returns>
void UBattleWidget::SetPlayerStateGoal(EPlayerOrder Order) {

	ArrayPlayerState[Order].CurrentState = EStateType::Goal;
}

/// <summary>
/// 指定プレイヤーがプレイ中かどうか
/// </summary>
/// <param name="Order"> プレイヤー識別 </param>
/// <returns> true:プレイ中, false:それ以外 </returns>
bool UBattleWidget::IsPlayerPlaying(EPlayerOrder Order) {

	// 既にプレイヤー識別がセットされていたら次のランクへ
	if (ArrayPlayerState[Order].CurrentState == EStateType::Play) {
		return true;
	}
	return false;
}

/// <summary>
/// 所持金を獲得する
/// </summary>
/// <param name="Order"> プレイヤー識別 </param>
/// <param name="AddMoney"> 加算額 </param>
/// <returns></returns>
void UBattleWidget::MakeMoney(EPlayerOrder Order, int32 AddMoney) {

	// 加算額を総額に加算
	ArrayPlayerMoney[Order].AmountMoney += AddMoney;

	int32 IntDiff = ArrayPlayerMoney[Order].AmountMoney - ArrayPlayerMoney[Order].CurrentMoney;
	IntDiff /= (ProcessSeconds * Frames);
	ArrayPlayerMoney[Order].AddMoney = IntDiff;
}

/// <summary>
/// プレイヤーの所持金をセット
/// </summary>
/// <param name="Order"> プレイヤー識別 </param>
/// <param name="MoneyValue"> 所持金 </param>
/// <returns></returns>
void UBattleWidget::SetPlayerMoney(EPlayerOrder Order, int32 MoneyValue) {

	UTextBlock* TextBlock = nullptr;
	switch (Order) {
	case EPlayerOrder::First: {

		TextBlock = Cast<UTextBlock>(GetWidgetFromName("TextBlock_Money01"));
		break;
	}
	case EPlayerOrder::Second: {

		TextBlock = Cast<UTextBlock>(GetWidgetFromName("TextBlock_Money02"));
		break;
	}
	case EPlayerOrder::Third: {

		TextBlock = Cast<UTextBlock>(GetWidgetFromName("TextBlock_Money03"));
		break;
	}
	case EPlayerOrder::Fourth: {

		TextBlock = Cast<UTextBlock>(GetWidgetFromName("TextBlock_Money04"));
		break;
	}
	}
	if (!IsValid(TextBlock)) {
		return;
	}

	bool IsMinus = false;
	int32 Money = MoneyValue;
	// 対象の数値が負の値なら正の値にしておく
	if (Money < Zero) {
		IsMinus = true;
		Money *= MinusSign;
	}

	FString OldText = FString::FromInt(Money);
	FString NewText;
	while (OldText.Len() > Three) {  // 「,」をつける必要があるまでループ
		NewText = "," + OldText.Mid(OldText.Len() - Three, Three) + NewText;  // 3文字ごとに「,」で区切る
		OldText = OldText.Mid(Zero, OldText.Len() - Three);  // 文字列を更新
	}
	NewText = OldText.Append(NewText);

	if (IsMinus) {
		FString TransText = "-";
		TransText = TransText.Append(NewText);
		NewText = TransText;
	}
	TextBlock->SetText(FText::FromString(NewText));
}

/// <summary>
/// 所持金パネル切り替え処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void UBattleWidget::SwitchMoneyPanel(float DeltaTime) {

	switch (CurrentMoneyPhase) {
	case EMoneyPanelPhase::Wait: {
		//------------------------------
		// 待機フェーズ
		//------------------------------
		break;
	}
	case EMoneyPanelPhase::Sort: {
		//------------------------------
		// ソートフェーズ
		//------------------------------
		TArray<int32> ArrayMoney;
		TMap<ERank, TMap<EPlayerOrder, int32>> OldArrayOrder;
		TMap<ERank, TMap<EPlayerOrder, int32>> NewArrayOrder;

		for (int i = 0; i < ArrayPlayerOrder.Num(); ++i) {
			ERank Rank = static_cast<ERank>(i);
			EPlayerOrder Order = ArrayPlayerOrder[Rank];
			ArrayMoney.Add(ArrayPlayerMoney[Order].AmountMoney);
		}

		// 同一金額がないかチェック
		for (int i = 0; i < ArrayMoney.Num(); ++i) {
			for (int j = i + One; j < ArrayMoney.Num(); ++j) {
				if (ArrayMoney[i] != ArrayMoney[j]) {
					continue;
				}
				// 「1」だけ小さい値にしておく
				ArrayMoney[j] = ArrayMoney[i] - One;
			}
		}

		// 現時点の順位を確保しておく
		for (int i = 0; i < ArrayPlayerOrder.Num(); ++i) {
			ERank Rank = static_cast<ERank>(i);
			TMap<EPlayerOrder, int32> PlayerData;
			// プレイヤー識別と金額を紐づける
			PlayerData.Add(ArrayPlayerOrder[Rank], ArrayMoney[i]);
			// 順位とプレイヤー情報を紐づける
			OldArrayOrder.Add(Rank, PlayerData);
		}

		int32 TempMoney;
		// 所持金ソート実行   バブルソート
		for (int i = 0; i < ArrayMoney.Num(); ++i) {
			for (int j = i + 1; j < ArrayMoney.Num(); ++j) {
				if (ArrayMoney[i] > ArrayMoney[j]) {
					continue;
				}
				TempMoney = ArrayMoney[i];
				ArrayMoney[i] = ArrayMoney[j];
				ArrayMoney[j] = TempMoney;
			}
		}

		// 新しい順位を確保
		TMap<EPlayerOrder, int32> NewPlayerData;
		for (int i = 0; i < ArrayMoney.Num(); ++i) {
			for (auto PlayerOrder : OldArrayOrder) {
				NewPlayerData.Empty();
				for (auto PlayerData : PlayerOrder.Value) {
					// 金額が一致しない場合は処理継続
					if (ArrayMoney[i] != PlayerData.Value) {
						continue;
					}
					// プレイヤー識別と所持金を紐づける
					NewPlayerData.Add(PlayerData.Key, ArrayMoney[i]);
					break;
				}
				if (NewPlayerData.Num() > Zero) {
					break;
				}
			}
			// 順位とプレイヤー情報を紐づける
			ERank Rank = static_cast<ERank>(i);
			NewArrayOrder.Add(Rank, NewPlayerData);
		}

		// 現在の順位を格納しつつ、順位が変わったか確認
		bool IsCheck = false;
		for (auto Rank : NewArrayOrder) {
			// ソート前の指定順位のプレイヤー情報を取得
			TMap<EPlayerOrder, int32> OldRank = OldArrayOrder[Rank.Key];
			for (auto PlayerData : Rank.Value) {
				// 現在の順位を格納
				ArrayPlayerOrder[Rank.Key] = PlayerData.Key;
				for (auto OldPlayerData : OldRank) {
					// ソート前後で順位の変動があったらフラグを立てる
					if (OldPlayerData.Key == PlayerData.Key) {
						continue;
					}
					IsCheck = true;
					break;
				}
			}
		}

		if (IsCheck) {
			// フェーズを「移動開始準備」に変更
			ChangeCurrentMoneyPhase(EMoneyPanelPhase::MovePrepare);
		}
		else {
			// フェーズを「待機」に変更
			ChangeCurrentMoneyPhase(EMoneyPanelPhase::Wait);
		}

		break;
	}
	case EMoneyPanelPhase::MovePrepare: {
		//------------------------------
		// 移動準備フェーズ
		//------------------------------
		// 表示座標を入れ替える
		for (auto Data : ArrayPlayerOrder) {
			// 順位に応じたパネルの移動後の目標座標をセット
			// Data.Key:順位、Data.Value:プレイヤー識別、
			ArrayPlayerMoneyPos[Data.Value].GoalPos = MoneyPanelPosition[Data.Key];
			// 毎フレームの移動量を算出
			FVector2D DiffPos;
			DiffPos.X = ArrayPlayerMoneyPos[Data.Value].GoalPos.X - ArrayPlayerMoneyPos[Data.Value].CurrentPos.X;
			DiffPos.Y = ArrayPlayerMoneyPos[Data.Value].GoalPos.Y - ArrayPlayerMoneyPos[Data.Value].CurrentPos.Y;
			ArrayPlayerMoneyPos[Data.Value].AddPos = DiffPos * DeltaTime;
		}

		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 1P Current: PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::First].CurrentPos.X, ArrayPlayerMoneyPos[EPlayerOrder::First].CurrentPos.Y);
		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 1P Goal   : PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::First].GoalPos.X, ArrayPlayerMoneyPos[EPlayerOrder::First].GoalPos.Y);
		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 2P Current: PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::Second].CurrentPos.X, ArrayPlayerMoneyPos[EPlayerOrder::Second].CurrentPos.Y);
		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 2P Goal   : PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::Second].GoalPos.X, ArrayPlayerMoneyPos[EPlayerOrder::Second].GoalPos.Y);
		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 3P Current: PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::Third].CurrentPos.X, ArrayPlayerMoneyPos[EPlayerOrder::Third].CurrentPos.Y);
		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 3P Goal   : PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::Third].GoalPos.X, ArrayPlayerMoneyPos[EPlayerOrder::Third].GoalPos.Y);
		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 4P Current: PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::Fourth].CurrentPos.X, ArrayPlayerMoneyPos[EPlayerOrder::Fourth].CurrentPos.Y);
		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 4P Goal   : PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::Fourth].GoalPos.X, ArrayPlayerMoneyPos[EPlayerOrder::Fourth].GoalPos.Y);

		// フェーズを「移動開始」に変更
		ChangeCurrentMoneyPhase(EMoneyPanelPhase::MoveStart);
		break;
	}
	case EMoneyPanelPhase::MoveStart: {
		//------------------------------
		// 移動開始フェーズ
		//------------------------------
		// アニメーション実行
		switch (CurrentTurn) {
		case EPlayerOrder::First: {
			ExpandMoneyPanel1P();
			break;
		}
		case EPlayerOrder::Second: {
			ExpandMoneyPanel2P();
			break;
		}
		case EPlayerOrder::Third: {
			ExpandMoneyPanel3P();
			break;
		}
		case EPlayerOrder::Fourth: {
			ExpandMoneyPanel4P();
			break;
		}
		}

		// ターンフレームを非表示
		HideTurnFrame();

		// フェーズを「移動待機」に変更
		ChangeCurrentMoneyPhase(EMoneyPanelPhase::MoveWait);
		break;
	}
	case EMoneyPanelPhase::MoveWait: {
		//------------------------------
		// 移動待機フェーズ
		//------------------------------
		// 指定時間待機
		MoneyAccumulateTime += DeltaTime;
		if (MoneyAccumulateTime < MoveWaitTime) {
			return;
		}
		MoneyAccumulateTime = ResetTime;

		// フェーズを「移動」に変更
		ChangeCurrentMoneyPhase(EMoneyPanelPhase::Move);
		break;
	}
	case EMoneyPanelPhase::Move: {
		//------------------------------
		// 移動フェーズ
		//------------------------------
		for (auto PosData : ArrayPlayerMoneyPos) {

			// 現在の表示位置に対して移動量を加算
			ArrayPlayerMoneyPos[PosData.Key].CurrentPos.Y += PosData.Value.AddPos.Y;
			
			// 目標に到達した場合の補正処理
			if (PosData.Value.AddPos.Y > Zerof) {
				if (ArrayPlayerMoneyPos[PosData.Key].CurrentPos.Y >= PosData.Value.GoalPos.Y) {
					ArrayPlayerMoneyPos[PosData.Key].CurrentPos.Y = PosData.Value.GoalPos.Y;
				}
			}
			else if (PosData.Value.AddPos.Y < Zerof) {
				if (ArrayPlayerMoneyPos[PosData.Key].CurrentPos.Y <= PosData.Value.GoalPos.Y) {
					ArrayPlayerMoneyPos[PosData.Key].CurrentPos.Y = PosData.Value.GoalPos.Y;
				}
			}

			// パネルに位置情報を渡す
			switch (PosData.Key) {
			case EPlayerOrder::First: {
				MovePositionMoneyPanel1P(ArrayPlayerMoneyPos[PosData.Key].CurrentPos);
				break;
			}
			case EPlayerOrder::Second: {
				MovePositionMoneyPanel2P(ArrayPlayerMoneyPos[PosData.Key].CurrentPos);
				break;
			}
			case EPlayerOrder::Third: {
				MovePositionMoneyPanel3P(ArrayPlayerMoneyPos[PosData.Key].CurrentPos);
				break;
			}
			case EPlayerOrder::Fourth: {
				MovePositionMoneyPanel4P(ArrayPlayerMoneyPos[PosData.Key].CurrentPos);
				break;
			}
			}
		}

		// 指定時間後に移動処理フェーズを変更
		MoneyAccumulateTime += DeltaTime;
		if (MoneyAccumulateTime < MoveFinishTime) {
			return;
		}
		MoneyAccumulateTime = ResetTime;

		// フェーズを「移動終了」に変更
		ChangeCurrentMoneyPhase(EMoneyPanelPhase::MoveEnd);
		break;
	}
	case EMoneyPanelPhase::MoveEnd: {
		//------------------------------
		// 移動終了フェーズ
		//------------------------------
		// ターンフレームを表示
		ShowTurnFrame();

		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 1P Current: PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::First].CurrentPos.X, ArrayPlayerMoneyPos[EPlayerOrder::First].CurrentPos.Y);
		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 1P Goal   : PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::First].GoalPos.X, ArrayPlayerMoneyPos[EPlayerOrder::First].GoalPos.Y);
		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 1P Add    : PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::First].AddPos.X, ArrayPlayerMoneyPos[EPlayerOrder::First].AddPos.Y);
		
		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 2P Current: PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::Second].CurrentPos.X, ArrayPlayerMoneyPos[EPlayerOrder::Second].CurrentPos.Y);
		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 2P Goal   : PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::Second].GoalPos.X, ArrayPlayerMoneyPos[EPlayerOrder::Second].GoalPos.Y);
		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 2P Add    : PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::Second].AddPos.X, ArrayPlayerMoneyPos[EPlayerOrder::Second].AddPos.Y);

		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 3P Current: PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::Third].CurrentPos.X, ArrayPlayerMoneyPos[EPlayerOrder::Third].CurrentPos.Y);
		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 3P Goal   : PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::Third].GoalPos.X, ArrayPlayerMoneyPos[EPlayerOrder::Third].GoalPos.Y);
		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 3P Add    : PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::Third].AddPos.X, ArrayPlayerMoneyPos[EPlayerOrder::Third].AddPos.Y);
		
		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 4P Current: PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::Fourth].CurrentPos.X, ArrayPlayerMoneyPos[EPlayerOrder::Fourth].CurrentPos.Y);
		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 4P Goal   : PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::Fourth].GoalPos.X, ArrayPlayerMoneyPos[EPlayerOrder::Fourth].GoalPos.Y);
		UE_LOG(LogTemp, Display, TEXT("MoneyPanel 4P Add    : PosX : %f, PosY : %f"), ArrayPlayerMoneyPos[EPlayerOrder::Fourth].AddPos.X, ArrayPlayerMoneyPos[EPlayerOrder::Fourth].AddPos.Y);

		// 現在のフェーズを「待機」に変更
		ChangeCurrentMoneyPhase(EMoneyPanelPhase::Wait);
		break;
	}
	}
}

/// <summary>
/// ターンパネル切り替え処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void UBattleWidget::SwitchTurnPanel(float DeltaTime) {

	switch (CurrentTurnPhase) {
	case ETurnPanelPhase::HidePanel: {
		//------------------------------
		// パネルを隠すフェーズ
		//------------------------------
		PlayOrderFadeOutAnimation();

		// 現在のフェーズを「パネル移動」に変更
		ChangeCurrentTurnPhase(ETurnPanelPhase::MovePanel);
		break;
	}
	case ETurnPanelPhase::MovePanel: {
		//------------------------------
		// パネルを移動するフェーズ
		//------------------------------
		TurnAccumulateTime += DeltaTime;
		if (TurnAccumulateTime < ShowPanelWaitTime) {
			return;
		}
		TurnAccumulateTime = ResetTime;

		// 前回処理から状態が変わったプレイヤーがいるか確認
		for (auto PlayerState : ArrayPlayerState) {
			if (PlayerState.Value.CurrentState == PlayerState.Value.OldState) {
				continue;
			}
			// ゴールしている場合、ターンパネルを非表示にする
			if (PlayerState.Value.CurrentState != EStateType::Goal) {
				continue;
			}
			HideTurnPanelWidget(PlayerState.Key);
		}

		int32 PlayerNum = 0;
		// 現在プレイ中の人数を数える
		for (auto PlayerState: ArrayPlayerState) {
			if (PlayerState.Value.CurrentState != EStateType::Play) {
				continue;
			}
			// 人数を加算
			++PlayerNum;
		}

		// ターンパネルの表示位置を変更
		for (auto PlayerState : ArrayPlayerState) {

			if (PlayerState.Value.CurrentState != EStateType::Play) {
				continue;
			}
			// 現在のパネル位置を取得
			if (ArrayTurnPanelPos[PlayerState.Key].X == TurnPanelPosX) {
				ArrayTurnPanelPos[PlayerState.Key].X = TurnPanelPosition[PlayerNum - 1].X;
			}
			else {
				ArrayTurnPanelPos[PlayerState.Key].X -= TurnPanelDifferX;
			}
			// パネル座標を変更する
			MovePositionTurnPanel(PlayerState.Key, ArrayTurnPanelPos[PlayerState.Key]);
		}

		// 矢印も一回非表示設定にする
		HideTurnArrowWidget();
		// 矢印を表示する
		ShowTurnArrowWidget(PlayerNum);

		// 現在のフェーズを「パネル表示」に変更
		ChangeCurrentTurnPhase(ETurnPanelPhase::ShowPanel);
		break;
	}
	case ETurnPanelPhase::ShowPanel: {
		//------------------------------
		// パネルを表示するフェーズ
		//------------------------------
		// ターンパネルの表示位置を変更
		for (auto PlayerState : ArrayPlayerState) {

			if (PlayerState.Value.CurrentState != EStateType::Play) {
				continue;
			}
			// パネルを表示状態にする
			ShowTurnPanelWidgetInvisible(PlayerState.Key);
		}
		PlayOrderFadeInAnimation();

		// 現在のフェーズを「待機」に変更
		ChangeCurrentTurnPhase(ETurnPanelPhase::Wait);
		break;
	}
	default: {
		break;
	}
	}
}

/// <summary>
/// ターンフレーム切り替え処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::SwitchTurnFrame() {

	// ターンフレームの表示
	ShowTurnFrame();

	// アニメーションの開始
	switch (CurrentTurn) {
	case EPlayerOrder::First: {
		PlayTurnFrame01Animation();
		break;
	}
	case EPlayerOrder::Second: {
		PlayTurnFrame02Animation();
		break;
	}
	case EPlayerOrder::Third: {
		PlayTurnFrame03Animation();
		break;
	}
	case EPlayerOrder::Fourth: {
		PlayTurnFrame04Animation();
		break;
	}
	}
}

/// <summary>
/// ターンフレームを表示処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::ShowTurnFrame() {

	switch (CurrentTurn) {
	case EPlayerOrder::First: {
		ShowTurnFrame01();
		break;
	}
	case EPlayerOrder::Second: {
		ShowTurnFrame02();
		break;
	}
	case EPlayerOrder::Third: {
		ShowTurnFrame03();
		break;
	}
	case EPlayerOrder::Fourth: {
		ShowTurnFrame04();
		break;
	}
	}
}

/// <summary>
/// ターンフレーム停止処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::StopTurnFrame() {

	switch (CurrentTurn) {
	case EPlayerOrder::First: {
		StopTurnFrame01Animation();
		break;
	}
	case EPlayerOrder::Second: {
		StopTurnFrame02Animation();
		break;
	}
	case EPlayerOrder::Third: {
		StopTurnFrame03Animation();
		break;
	}
	case EPlayerOrder::Fourth: {
		StopTurnFrame04Animation();
		break;
	}
	}
}

/// <summary>
/// 現在のターン切り替えフェーズを変更
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void UBattleWidget::ChangeCurrentTurnPhase(ETurnPanelPhase PhaseType) {

	CurrentTurnPhase = PhaseType;
}

/// <summary>
/// 現在の所持金パネル切り替えフェーズを変更
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::ChangeCurrentMoneyPhase(EMoneyPanelPhase PhaseType) {

	CurrentMoneyPhase = PhaseType;
}

/// <summary>
/// 指定順位のプレイヤー識別を取得
/// </summary>
/// <param name="Rank"> ランク </param>
/// <returns> プレイヤー識別 </returns>
EPlayerOrder UBattleWidget::GetPlayerOrder(ERank Rank) {

	return ArrayPlayerOrder[Rank];
}

/// <summary>
/// 指定プレイヤーの所持金を取得
/// </summary>
/// <param name="Order"> プレイヤー識別 </param>
/// <returns> 所持金 </returns>
int32 UBattleWidget::GetPocketMoney(EPlayerOrder Order) {

	return ArrayPlayerMoney[Order].AmountMoney;
}

/// <summary>
/// 所持金パネルウィジェットの表示
/// </summary>
/// <param name="PlayerOrder"> プレイヤー識別 </param>
/// <returns></returns>
void UBattleWidget::ShowMoneyPanelWidget_Implementation(EPlayerOrder PlayerOrder) {

}

/// <summary>
/// 所持金パネルウィジェットの非表示
/// </summary>
/// <param name="PlayerOrder"> プレイヤー識別 </param>
/// <returns></returns>
void UBattleWidget::HideMoneyPanelWidget_Implementation(EPlayerOrder PlayerOrder) {

}

/// <summary>
/// ターンフレームを表示する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::ShowTurnFrame01_Implementation() {

}

/// <summary>
/// ターンフレームを表示する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::ShowTurnFrame02_Implementation() {

}

/// <summary>
/// ターンフレームを表示する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::ShowTurnFrame03_Implementation() {

}

/// <summary>
/// ターンフレームを表示する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::ShowTurnFrame04_Implementation() {

}

/// <summary>
/// ターンフレームを非表示する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::HideTurnFrame_Implementation() {

}

/// <summary>
/// ターンパネルウィジェットの表示
/// </summary>
/// <param name="PlayerOrder"> プレイヤー識別 </param>
/// <returns></returns>
void UBattleWidget::ShowTurnPanelWidget_Implementation(EPlayerOrder PlayerOrder) {

}

/// <summary>
/// 指定プレイヤーのターンパネルを表示する ※透過率は「0」
/// </summary>
/// <param name="Order"> プレイヤー識別 </param>
/// <returns></returns>
void UBattleWidget::ShowTurnPanelWidgetInvisible_Implementation(EPlayerOrder Order) {

}

/// <summary>
/// ターンパネルウィジェットの非表示
/// </summary>
/// <param name="PlayerOrder"> プレイヤー識別 </param>
/// <returns></returns>
void UBattleWidget::HideTurnPanelWidget_Implementation(EPlayerOrder PlayerOrder) {

}

/// <summary>
/// ターンパネル横の矢印を表示する
/// </summary>
/// <param name="Number"> プレイ中の人数 </param>
/// <returns></returns>
void UBattleWidget::ShowTurnArrowWidget_Implementation(int32 Number) {

}

/// <summary>
/// ターンパネル横の矢印を非表示する
/// </summary>
/// <param name="Number"> プレイ中の人数 </param>
/// <returns></returns>
void UBattleWidget::HideTurnArrowWidget_Implementation() {

}

/// <summary>
/// Finishウィジェットの非表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::HideFinishWidget_Implementation() {

}

/// <summary>
/// Finishウィジェットの表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::ShowFinishWidget_Implementation() {

}

/// <summary>
/// ターン表示パネルの移動
/// </summary>
/// <param name="Order"> プレイヤー識別 </param>
/// <param name="Position"> 座標 </param>
/// <returns></returns>
void UBattleWidget::MovePositionTurnPanel_Implementation(EPlayerOrder Order, FVector2D Position) {

}

/// <summary>
/// ターン開始アニメーション実行
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::PlayTurnStart01Animation_Implementation() {

}

/// <summary>
/// ターン開始アニメーション実行
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::PlayTurnStart02Animation_Implementation() {

}

/// <summary>
/// ターン開始アニメーション実行
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::PlayTurnStart03Animation_Implementation() {

}

/// <summary>
/// ターン開始アニメーション実行
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::PlayTurnStart04Animation_Implementation() {

}

/// <summary>
/// 順番入れ替えフェードアウトアニメーション実行
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::PlayOrderFadeOutAnimation_Implementation() {

}

/// <summary>
/// 順番入れ替えフェードインアニメーション実行
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::PlayOrderFadeInAnimation_Implementation() {

}

/// <summary>
/// ターンフレームのアニメーションを開始する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::PlayTurnFrame01Animation_Implementation() {

}

/// <summary>
/// ターンフレームのアニメーションを開始する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::PlayTurnFrame02Animation_Implementation() {

}

/// <summary>
/// ターンフレームのアニメーションを開始する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::PlayTurnFrame03Animation_Implementation() {

}

/// <summary>
/// ターンフレームのアニメーションを開始する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::PlayTurnFrame04Animation_Implementation() {

}

/// <summary>
/// ターンフレームのアニメーションを停止する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::StopTurnFrame01Animation_Implementation() {

}

/// <summary>
/// ターンフレームのアニメーションを停止する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::StopTurnFrame02Animation_Implementation() {

}

/// <summary>
/// ターンフレームのアニメーションを停止する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::StopTurnFrame03Animation_Implementation() {

}

/// <summary>
/// ターンフレームのアニメーションを停止する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::StopTurnFrame04Animation_Implementation() {

}

/// <summary>
/// 所持金パネル拡大演出 1P用
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::ExpandMoneyPanel1P_Implementation() {

}

/// <summary>
/// 所持金パネル拡大演出 2P用
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::ExpandMoneyPanel2P_Implementation() {

}

/// <summary>
/// 所持金パネル拡大演出 3P用
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::ExpandMoneyPanel3P_Implementation() {

}

/// <summary>
/// 所持金パネル拡大演出 4P用
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UBattleWidget::ExpandMoneyPanel4P_Implementation() {

}

/// <summary>
/// 所持金パネル移動処理 1P用
/// </summary>
/// <param name="Position"> 配置座標 </param>
/// <returns></returns>
void UBattleWidget::MovePositionMoneyPanel1P_Implementation(FVector2D Position) {

}

/// <summary>
/// 所持金パネル移動処理 2P用
/// </summary>
/// <param name="Position"> 配置座標 </param>
/// <returns></returns>
void UBattleWidget::MovePositionMoneyPanel2P_Implementation(FVector2D Position) {

}

/// <summary>
/// 所持金パネル移動処理 3P用
/// </summary>
/// <param name="Position"> 配置座標 </param>
/// <returns></returns>
void UBattleWidget::MovePositionMoneyPanel3P_Implementation(FVector2D Position) {

}

/// <summary>
/// 所持金パネル移動処理 4P用
/// </summary>
/// <param name="Position"> 配置座標 </param>
/// <returns></returns>
void UBattleWidget::MovePositionMoneyPanel4P_Implementation(FVector2D Position) {

}

/// <summary>
/// ターンフレーム移動処理
/// </summary>
/// <param name="Position"> 配置座標 </param>
/// <returns></returns>
void UBattleWidget::MovePositionTurnFrame_Implementation(FVector2D Position) {

}