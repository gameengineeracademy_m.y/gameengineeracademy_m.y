﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Battle/BattleEvent.h"
#include "PlayerBase.generated.h"


/// <summary>
/// プレイヤーの識別
/// </summary>
UENUM(BlueprintType)
enum class EPlayerOrder : uint8 {
	None,
	First,
	Second,
	Third,
	Fourth,
	MaxTypeIndex
};

/// <summary>
/// 操作種類
/// </summary>
UENUM()
enum class EOperationType {
	Player,     // プレイヤー操作
	CpuTypeA,   // 自動操作 最短距離
	CpuTypeB,   // 自動操作 最長距離
	CpuTypeC,   // 自動操作 プレイヤー追従
	CpuTypeD,   // 自動操作 リスクヘッジ
	CpuTypeE,   // 自動操作 目先の利益を追う
	CpuTypeF,   // 自動操作 最大利益を狙う
	MaxTypeIndex
};

/// <summary>
/// ゲーム進行状態の種類
/// </summary>
UENUM()
enum class EStateType {
	None,
	Play,
	Goal,
	MaxPhaseIndex
};

/// <summary>
/// プレイヤーアニメーションの種類
/// </summary>
UENUM(BlueprintType)
enum class ECharaAnimType : uint8 {
	Normal,
	GetMoney,
	LostMoney,
	Warp,
	MaxTypeIndex
};

/// <summary>
/// プレイヤー処理
/// </summary>
UCLASS()
class APlayerBase : public ACharacter
{
	GENERATED_BODY()

public:

	/// <summary>
	/// プレイヤーを表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "PlayerBase")
	void Show();

	/// <summary>
	/// プレイヤーを非表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "PlayerBase")
	void Hide();

	/// <summary>
	/// アニメーション初期化
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "PlayerBase")
	void InitializeAnimation();

	/// <summary>
	/// お金獲得イベント開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "PlayerBase")
	void StartGetMoney();

	/// <summary>
	/// お金損失イベント開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "PlayerBase")
	void StartLostMoney();

	/// <summary>
	/// プレイヤーアニメーションを元に戻す
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "PlayerBase")
	void ResetAnimation();


public:
	// Sets default values for this character's properties
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	APlayerBase();

	/// <summary>
	/// 初期設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Initialize();

	/// <summary>
	/// プレイヤーの表示設定
	/// </summary>
	/// <param name="Flag"> 表示フラグ </param>
	/// <returns></returns>
	void SetShowFlag(bool Flag);

	/// <summary>
	/// プレイヤーが表示済みかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:表示済み, false:非表示 </returns>
	bool IsShow();

	/// <summary>
	/// 操作種類をセット
	/// </summary>
	/// <param name="Type"> 操作種類 </param>
	/// <returns></returns>
	void SetOperationType(EOperationType Type);

	/// <summary>
	/// 操作種類を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 操作種類 </returns>
	EOperationType GetOperationType();

	/// <summary>
	/// 所持金に獲得金額を加算
	/// </summary>
	/// <param name=""> 獲得金額 </param>
	/// <returns></returns>
	void AddPocketMoney(int32);

	/// <summary>
	/// 所持金を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 所持金 </returns>
	int32 GetPocketMoney();

	/// <summary>
	/// プレイヤー番号をセット
	/// </summary>
	/// <param name=""> プレイヤー番号 </param>
	/// <returns></returns>
	void SetPlayerOrder(EPlayerOrder);

	/// <summary>
	/// プレイヤー番号を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> プレイヤー番号 </returns>
	EPlayerOrder GetPlayerOrder();

	/// <summary>
	/// 操作の順番をセット
	/// </summary>
	/// <param name="Order"> 操作の順番 </param>
	/// <returns></returns>
	void SetOperationOrder(int32 Order);

	/// <summary>
	/// 操作の順番を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 操作の順番 </returns>
	int32 GetOperationOrder();

	/// <summary>
	/// フィールド番号をセット
	/// </summary>
	/// <param name="Num"> フィールド番号 </param>
	/// <returns></returns>
	void SetFieldNum(int32 Num);

	/// <summary>
	/// フィールド番号を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> フィールド番号 </returns>
	int32 GetFieldNum();

	/// <summary>
	/// 現在のゲーム状態を変更する
	/// </summary>
	/// <param name="StateType"> 状態の種類 </param>
	/// <returns></returns>
	void ChangeStateType(EStateType StateType);

	/// <summary>
	/// 現在のゲーム状態を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 状態の種類 </returns>
	EStateType GetStateType();

	/// <summary>
	/// 重力を変更
	/// </summary>
	/// <param name="Value"> 重力の値 </param>
	/// <returns> 状態の種類 </returns>
	void ChangeGravityScale(float Value);

	/// <summary>
	/// プレイヤーがゴールしたかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:ゴール済み, false:プレイ中 </returns>
	bool IsArriveGoal();


protected:
	// Called when the game starts or when spawned
	/// <summary>
	/// 実行開始イベント
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:	

	// Called every frame
	/// <summary>
	/// 毎フレームの処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	/// <summary>
	/// SetupPlayerInputComponent
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	/// <summary>
	/// プレイヤーを表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void Show_Implementation();

	/// <summary>
	/// プレイヤーを非表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void Hide_Implementation();

	/// <summary>
	/// アニメーション初期化
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void InitializeAnimation_Implementation();

	/// <summary>
	/// お金獲得イベント開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void StartGetMoney_Implementation();

	/// <summary>
	/// お金損失イベント開始
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void StartLostMoney_Implementation();

	/// <summary>
	/// アニメーションを元に戻す
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ResetAnimation_Implementation();


private:

	/// <summary>
	/// 現在の状態
	/// </summary>
	EStateType CurrentState;

	/// <summary>
	/// 現在いるマスの番号
	/// </summary>
	int32 FieldNum;

	/// <summary>
	/// 所持金
	/// </summary>
	int32 PocketMoney;

	/// <summary>
	/// 操作種類
	/// </summary>
	EOperationType OperationType;

	/// <summary>
	/// プレイヤーの識別
	/// </summary>
	EPlayerOrder PlayerOrder;

	/// <summary>
	/// 操作の順番
	/// </summary>
	int32 OperationOrder;

	/// <summary>
	/// 表示有無
	/// </summary>
	bool ShowFlag;


private:

	/// <summary>
	/// 1Pマテリアルパス
	/// </summary>
	const FString FirstMaterialPath = "/Game/Materials/1PMaterial";

	/// <summary>
	/// 2Pマテリアルパス
	/// </summary>
	const FString SecondMaterialPath = "/Game/Materials/2PMaterial";

	/// <summary>
	/// 3Pマテリアルパス
	/// </summary>
	const FString ThirdMaterialPath = "/Game/Materials/3PMaterial";

	/// <summary>
	/// 4Pマテリアルパス
	/// </summary>
	const FString FourthMaterialPath = "/Game/Materials/4PMaterial";

	/// <summary>
	/// マテリアルインデックス
	/// </summary>
	const int MaterialIndex0 = 0;

	/// <summary>
	/// 所持金初期値
	/// </summary>
	const int32 InitMoney = 0;

	/// <summary>
	/// 回転量初期値
	/// </summary>
	const float InitRotValue = 0.0f;
};