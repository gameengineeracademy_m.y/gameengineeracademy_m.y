﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/DialogWidget.h"
#include "Components/TextBlock.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDialogWidget::NativeConstruct() {

	Super::NativeConstruct();

	// CSVファイル読込
	FString ProjectPath = FPaths::ProjectDir();
	FString FilePath = ProjectPath + SceneTextCsvFilePath;
	bool IsFinish = ReadCsvFile(FilePath);
	if (!IsFinish) {
		return;
	}
}

/// <summary>
/// Tick処理
/// </summary>
/// <param name="MyGeometry"> ジオメトリー </param>
/// <param name="InDaltaTime"> 処理時間 </param>
/// <returns></returns>
void UDialogWidget::NativeTick(const FGeometry& MyGeometry, float InDaltaTime) {

	Super::NativeTick(MyGeometry, InDaltaTime);
}

/// <summary>
/// ウィジェットの非表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDialogWidget::Hide_Implementation() {

}

/// <summary>
/// 背景ウィジェットの表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDialogWidget::Show_Implementation() {

}

/// <summary>
/// テキストをセット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void UDialogWidget::ConvertCrlf_Implementation() {

}

/// <summary>
/// CSV読込処理
/// </summary>
/// <param name="FilePath"> ファイルパス </param>
/// <returns> true:処理成功, false:処理失敗 </returns>
bool UDialogWidget::ReadCsvFile(FString FilePath) {

	UE_LOG(LogTemp, Display, TEXT("AFieldManager ReadCsvFieldMapFile"));

	// 配列の初期化
	SceneTextData.Empty();

	// CSVファイルからデータを取得
	FString CsvDataFile;
	FFileHelper::LoadFileToString(CsvDataFile, *FilePath);
	if (CsvDataFile.IsEmpty()) {
		return false;
	}

	// 各行で分割した値の保持
	TArray<FString> Row;
	// カンマ区切りごとの値
	TArray<FString> Element;
	EOutputScene SceneType;
	FString OutputText;
	CsvDataFile.ParseIntoArrayLines(Row);

	for (int i = 0; i < Row.Num(); ++i) {
		// カンマ区切りで取得
		Row[i].ParseIntoArray(Element, TEXT(","), true);
		// 取得した値を文字列から数値に変換
		for (int j = 0; j < Element.Num(); ++j) {
			if (j == 0) {
				SceneType = static_cast<EOutputScene>(FCString::Atoi(*Element[j]));
			}
			else {
				OutputText = Element[j];
			}
		}
		// 配列に格納
		SceneTextData.Add(SceneType, OutputText);
		// 一時配列の初期化
		Element.Reset();
	}

	return true;
}

/// <summary>
/// 指定シーンのテキストを出力
/// </summary>
/// <param name="SceneType"> 指定シーン </param>
/// <param name="Money"> 金額 < / param>
/// <param name="GoalRank"> 着順 </param>
/// <returns></returns>
void UDialogWidget::OutputSceneText(EOutputScene SceneType, int32 Money, int32 GoalRank) {

	//------------------------------
	// 指定シーンのテキスト取得
	//------------------------------
	FString Text = SceneTextData[SceneType];

	if (Money > Zero) {
		// 金額について3桁毎にカンマを入れる
		FString OldText = FString::FromInt(Money);
		FString NewText;
		while (OldText.Len() > Three) {  // 「,」をつける必要があるまでループ
			NewText = "," + OldText.Mid(OldText.Len() - Three, Three) + NewText;  // 3文字ごとに「,」で区切る
			OldText = OldText.Mid(Zero, OldText.Len() - Three);  // 文字列を更新
		}
		NewText = OldText.Append(NewText);
		// 特定の文字を変換
		Text = Text.Replace((TEXT("%s"), *ConvertMoneyText), (TEXT("%s"), *NewText));
	}

	if (GoalRank > Zero) {
		// 順位をテキストに置き換える
		FString RankText = FString::FromInt(GoalRank);
		// 特定の文字を変換
		Text = Text.Replace((TEXT("%s"), *ConvertOrderText), (TEXT("%s"), *RankText));
	}

	// テキストブロックを取得
	UTextBlock* TextBlock = Cast<UTextBlock>(GetWidgetFromName("TextBlock_Dialog"));
	if (!IsValid(TextBlock)) {
		return;
	}
	//------------------------------
	// テキストをセット
	//------------------------------
	TextBlock->SetText(FText::FromString(Text));

	// セットしたテキスト内の改行文字を改行コードに変換
	ConvertCrlf();
}