﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/FieldBase.h"
#include "Components/TextBlock.h"

AFieldBase::AFieldBase()
	: StaticMeshComp(nullptr)
	, WidgetComp(nullptr)
	, MoneyTextWidgetClass()
	, MoneyTextWidget(nullptr) {

	// StaticMeshComponentを作成する
	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	RootComponent = StaticMeshComp;

	WidgetComp = CreateDefaultSubobject<UWidgetComponent>(TEXT("WidgetComponent"));
	WidgetComp->SetupAttachment(RootComponent);

	MoneyTextWidget = nullptr;
}


void AFieldBase::BeginPlay()
{
	Super::BeginPlay();

	FieldType = EFieldType::None;
}

/// <summary>
/// 毎フレームの処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AFieldBase::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

	// フィールドの種類が「強制移動」以外は処理終了
	if (FieldType != EFieldType::Move) {
		return;
	}
}

/// <summary>
/// 初期化処理フェーズ
/// </summary>
/// <param name="FieldTypeValue"> フィールドの種類 </param>
/// <returns></returns>
void AFieldBase::Initialize(EFieldType FieldTypeValue) {

	// フィールドの種類をセット
	SetFieldType(FieldTypeValue);
	// メッシュにマテリアルをセットする
	SetupMeshMaterial();
}

/// <summary>
/// フィールドの種類をセット
/// </summary>
/// <param name="FieldTypeValue"> フィールドの種類 </param>
/// <returns></returns>
void AFieldBase::SetFieldType(EFieldType FieldTypeValue) {

	FieldType = FieldTypeValue;
}

/// <summary>
/// フィールドの種類を取得
/// </summary>
/// <param name=""></param>
/// <returns> フィールドの種類 </returns>
EFieldType AFieldBase::GetFieldType() {

	return FieldType;
}

/// <summary>
/// フィールドの番号をセット
/// </summary>
/// <param name="Order"> フィールドの番号 </param>
/// <returns></returns>
void AFieldBase::SetFieldOrder(int32 Order) {

	FieldOrder = Order;
}

/// <summary>
/// フィールドの番号を取得
/// </summary>
/// <param name=""></param>
/// <returns> フィールドの番号 </returns>
int32 AFieldBase::GetFieldOrder() {

	return FieldOrder;
}

/// <summary>
/// フィールドの固有番号をセット
/// </summary>
/// <param name="Order"> フィールドの固有番号 </param>
/// <returns></returns>
void AFieldBase::SetFieldUniOrder(int32 Order) {

	FieldUniOrder = Order;
}

/// <summary>
/// フィールドの固有番号を取得
/// </summary>
/// <param name=""></param>
/// <returns> フィールドの固有番号 </returns>
int32 AFieldBase::GetFieldUniOrder() {

	return FieldUniOrder;
}

/// <summary>
/// メッシュマテリアルの設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AFieldBase::SetupMeshMaterial() {

	FString MaterialPath;

	switch (FieldType) {
	case EFieldType::Plus: {
		//-----------------------------
		//お金を獲得するマス
		//-----------------------------
		MaterialPath = BlueMaterialPath;
		break;
	}
	case EFieldType::Minus: {
		//-----------------------------
		//お金を損失するマス
		//-----------------------------
		MaterialPath = RedMaterialPath;
		break;
	}
	case EFieldType::Move: {
		//-----------------------------
		//強制移動するマス
		//-----------------------------
		MaterialPath = YellowMaterialPath;
		break;
	}
	case EFieldType::Normal: {
		//-----------------------------
		//通常マス
		//-----------------------------
		MaterialPath = NormalMaterialPath;
		break;
	}
	case EFieldType::Start: {
		//-----------------------------
		//スタートマス
		//-----------------------------
		MaterialPath = LightBlueMaterialPath;
		break;
	}
	case EFieldType::Goal: {
		//-----------------------------
		//ゴールマス
		//-----------------------------
		MaterialPath = PinkMaterialPath;
		break;
	}
	default: {
		return;
	}
	}

	// マテリアルをロードしてセット
	UMaterial* Material = LoadObject<UMaterial>(nullptr, *MaterialPath);
	if (!IsValid(Material)) {
		return;
	}
	// マテリアルをセット
	StaticMeshComp->SetMaterial(MaterialIndex0, Material);
	StaticMeshComp->RegisterComponent();
}

/// <summary>
/// ウィジェットマテリアルの設定
/// </summary>
/// <param name="MaterialPath"> マテリアルパス </param>
/// <returns></returns>
void AFieldBase::SetupWidgetMaterial(FString MaterialPath) {

	// マテリアルをロードしてセット
	UMaterial* Material = LoadObject<UMaterial>(nullptr, *MaterialPath);
	if (!IsValid(Material)) {
		return;
	}
	// マテリアルをセット
	WidgetComp->SetMaterial(MaterialIndex0, Material);
	WidgetComp->RegisterComponent();

	// ウィジェットマテリアルのサイズや位置、角度の変更
	AdjustWidgetTransform();
}

/// <summary>
/// ウィジェットクラスの設定
/// </summary>
/// <param name="MoneyValue"> 金額 </param>
/// <returns></returns>
void AFieldBase::SetupMoneyWidgetClass(int32 MoneyValue) {

	// マニーウィジェットを生成する
	UE_LOG(LogTemp, Display, TEXT("%s"), *MoneyWidgetPath);
	MoneyTextWidgetClass = TSoftClassPtr<UMoneyTextWidget>(FSoftObjectPath(*MoneyWidgetPath)).LoadSynchronous();
	if (!MoneyTextWidgetClass) {
		UE_LOG(LogTemp, Display, TEXT("MoneyTextWidgetClass NULL"))
		return;
	}
	MoneyTextWidget = CreateWidget<UMoneyTextWidget>(GetWorld(), MoneyTextWidgetClass);
	if (!MoneyTextWidget) {
		UE_LOG(LogTemp, Display, TEXT("MoneyTextWidget nullptr"))
		return;
	}
	UE_LOG(LogTemp, Display, TEXT("AFieldBase CreateMoneyTextWidget"));

	// ウィジェットコンポーネントにマニーウィジェットをセット
	WidgetComp->SetWidget(MoneyTextWidget);

	UE_LOG(LogTemp, Display, TEXT("AFieldBase WidgetComp->SetWidget()"));

	//-------------------------------------------------------
	// 符号と金額をマニーウィジェットのテキストにセットする
	//-------------------------------------------------------
	// 符号のセット
	FString SignText;
	EFieldType Type = GetFieldType();
	if (Type == EFieldType::Plus) {
		SignText = "+";
	}
	else {
		SignText = "-";
	}

	// 金額を3桁ごとにカンマで区切る
	FString OldMoneyText = FString::FromInt(MoneyValue);
	FString NewMoneyText;
	while (OldMoneyText.Len() > Three) {  // 「,」をつける必要があるまでループ
		NewMoneyText = "," + OldMoneyText.Mid(OldMoneyText.Len() - Three, Three) + NewMoneyText;  // 3文字ごとに「,」で区切る
		OldMoneyText = OldMoneyText.Mid(Zero, OldMoneyText.Len() - Three);  // 文字列を更新
	}
	NewMoneyText = OldMoneyText.Append(NewMoneyText);

	// テキストブロックに符号をセット
	UTextBlock* TextBlock_Sign = Cast<UTextBlock>(MoneyTextWidget->GetWidgetFromName("TextBlock_Sign"));
	if (!IsValid(TextBlock_Sign)) {
		UE_LOG(LogTemp, Display, TEXT("AFieldBase TextBlock_Sign nullptr"));
		return;
	}
	TextBlock_Sign->SetText(FText::FromString(SignText));

	UE_LOG(LogTemp, Display, TEXT("AFieldBase SignText"));

	// テキストブロックに金額をセット
	UTextBlock* TextBlock_Money = Cast<UTextBlock>(MoneyTextWidget->GetWidgetFromName("TextBlock_Money"));
	if (!IsValid(TextBlock_Money)) {
		UE_LOG(LogTemp, Display, TEXT("AFieldBase TextBlock_Money nullptr"));
		return;
	}
	TextBlock_Money->SetText(FText::FromString(NewMoneyText));

	UE_LOG(LogTemp, Display, TEXT("AFieldBase NewMoneyText"));

	// ウィジェットマテリアルのサイズや位置、角度の変更
	AdjustWidgetTransform();
}

/// <summary>
/// インフォウィジェットクラスの設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AFieldBase::SetupInfoWidgetClass() {

	// マニーウィジェットを生成する
	UE_LOG(LogTemp, Display, TEXT("%s"), *InfoWidgetPath);
	InfoWidgetClass = TSoftClassPtr<UInfoWidget>(FSoftObjectPath(*InfoWidgetPath)).LoadSynchronous();
	if (!InfoWidgetClass) {
		UE_LOG(LogTemp, Display, TEXT("InfoWidgetClass NULL"))
		return;
	}
	InfoWidget = CreateWidget<UInfoWidget>(GetWorld(), InfoWidgetClass);
	if (!InfoWidget) {
		UE_LOG(LogTemp, Display, TEXT("InfoWidget nullptr"))
		return;
	}
	UE_LOG(LogTemp, Display, TEXT("AFieldBase CreateInfoWidget"));

	// ウィジェットコンポーネントにマニーウィジェットをセット
	WidgetComp->SetWidget(InfoWidget);

	UE_LOG(LogTemp, Display, TEXT("AFieldBase WidgetComp->SetWidget()"));

	FString InfoText;
	EFieldType Type = GetFieldType();
	if (Type == EFieldType::Start) {
		InfoText = "Start";
	}
	else {
		InfoText = "Goal";
	}
	UTextBlock* TextBlock_Info = Cast<UTextBlock>(InfoWidget->GetWidgetFromName("TextBlock_Info"));
	if (!IsValid(TextBlock_Info)) {
		UE_LOG(LogTemp, Display, TEXT("AFieldBase TextBlock_Info nullptr"));
		return;
	}
	TextBlock_Info->SetText(FText::FromString(InfoText));

	UE_LOG(LogTemp, Display, TEXT("AFieldBase InfoText"));

	// ウィジェットマテリアルのサイズや位置、角度の変更
	AdjustWidgetTransform();
}

/// <summary>
/// フィールドの種類をセット
/// </summary>
/// <param name="FieldTypeValue"> フィールドの種類 </param>
/// <returns></returns>
void AFieldBase::SetFieldType_Implementation(EFieldType FieldTypeValue) {

	FieldType = FieldTypeValue;
}

/// <summary>
/// フィールドの種類を取得
/// </summary>
/// <param name=""></param>
/// <returns> フィールドの種類 </returns>
EFieldType AFieldBase::GetFieldType_Implementation() {

	return FieldType;
}

/// <summary>
/// ウィジェット配置 調整処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AFieldBase::AdjustWidgetTransform_Implementation() {

}