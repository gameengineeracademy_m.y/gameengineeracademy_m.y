﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget.h"
#include "Battle/PlayerBase.h"
#include "ResultWidget.generated.h"


/// <summary>
/// リザルトフェーズ
/// </summary>
UENUM()
enum class EResultPhaseType {
	Wait,
	Prepare,
	ShowBack,
	ShowResultBoard,
	WaitAniation,
	RankAnimation,
	ShowGuide,
	WaitTransition,
	MaxPhaseIndex
};

/// <summary>
/// アニメーションフェーズ
/// </summary>
UENUM(BlueprintType)
enum class EAnimationPhase : uint8 {
	AnimFirst,
	AnimSecond,
	AnimThird,
	AnimFourth,
	Finish,
	MaxPhaseIndex
};

/// <summary>
/// ランキング
/// </summary>
enum class ERanking {
	First,
	Second,
	Third,
	Fourth,
	MaxPhaseIndex
};

/// <summary>
/// リザルトウィジェット
/// </summary>
UCLASS()
class UResultWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// ウィジェットの非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ResultWidget")
	void HideWidget();

	/// <summary>
	/// ランクパネルの非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ResultWidget")
	void HideRankPanel();

	/// <summary>
	/// ガイドパネルの非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ResultWidget")
	void HideGuidePanel();

	/// <summary>
	/// 背景ウィジェットの表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ResultWidget")
	void ShowBack();

	/// <summary>
	/// リザルトボードウィジェットの表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ResultWidget")
	void ShowResultBoard();

	/// <summary>
	/// ランキングパネルの座標セット
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <param name="Position"> 座標 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ResultWidget")
	void SetRankPanelPosition(EPlayerOrder Order, FVector2D Position);

	/// <summary>
	/// ランク情報パネルの表示
	/// </summary>
	/// <param name="PhaseType"></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ResultWidget")
	void ShowRankPanel(EAnimationPhase PhaseType);

	/// <summary>
	/// 案内ウィジェットの表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ResultWidget")
	void ShowGuideText();


public:

	/// <summary>
	/// 順位情報のセット
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <param name="Rank"> ランキング </param>
	/// <param name="Money"> 所持金 </param>
	/// <returns></returns>
	void SetupRankingData(EPlayerOrder Order, ERanking Rank, int32 Money);

	/// <summary>
	/// 現在のフェーズを変更
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentPhase(EResultPhaseType PhaseType);

	/// <summary>
	/// アニメーション配列に追加
	/// </summary>
	/// <param name="AnimPhase"> フェーズの種類 </param>
	/// <returns></returns>
	void AddAnimationArray(EAnimationPhase AnimPhase);


protected:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

	/// <summary>
	/// Tick処理
	/// </summary>
	/// <param name="MyGeometry"> ジオメトリー </param>
	/// <param name="InDaltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void NativeTick(const FGeometry& MyGeometry, float InDaltaTime) override;


private:

	/// <summary>
	/// 順位パネル 表示位置
	/// </summary>
	TMap<ERanking, FVector2D> RankPanelPosition;

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	EResultPhaseType CurrentPhase;

	/// <summary>
	/// アニメーション配列
	/// </summary>
	TArray<EAnimationPhase> ArrayAnimPhase;

	/// <summary>
	/// インデックス
	/// </summary>
	int32 AnimIndex;

	/// <summary>
	/// 累積時間
	/// </summary>
	float AccumulateTime;

	/// <summary>
	/// ウィジェットの非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void HideWidget_Implementation();

	/// <summary>
	/// ランクパネルの非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void HideRankPanel_Implementation();

	/// <summary>
	/// ガイドパネルの非表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void HideGuidePanel_Implementation();

	/// <summary>
	/// 背景ウィジェットの表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ShowBack_Implementation();

	/// <summary>
	/// リザルトボードウィジェットの表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ShowResultBoard_Implementation();

	/// <summary>
	/// ランキングパネルの座標セット
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <param name="Position"> 座標 </param>
	/// <returns></returns>
	virtual void SetRankPanelPosition_Implementation(EPlayerOrder Order, FVector2D Position);

	/// <summary>
	/// ランク情報パネルの表示
	/// </summary>
	/// <param name="PhaseType"></param>
	/// <returns></returns>
	virtual void ShowRankPanel_Implementation(EAnimationPhase PhaseType);

	/// <summary>
	/// 案内ウィジェットの表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void ShowGuideText_Implementation();

private:

	/// <summary>
	/// ランキング表示位置 X座標
	/// </summary>
	const float RankingPosX = 650.0f;

	/// <summary>
	/// ランキング1位 表示位置 Y座標
	/// </summary>
	const float RankingFirstPosY = 325.0f;

	/// <summary>
	/// ランキング2位 表示位置 Y座標
	/// </summary>
	const float RankingSecondPosY = 432.0f;

	/// <summary>
	/// ランキング3位 表示位置 Y座標
	/// </summary>
	const float RankingThirdPosY = 540.0f;

	/// <summary>
	/// ランキング4位 表示位置 Y座標
	/// </summary>
	const float RankingFourthPosY = 645.0f;

	/// <summary>
	/// ゼロ
	/// </summary>
	const int32 Zero = 0;

	/// <summary>
	/// 参
	/// </summary>
	const int32 Three = 3;

	/// <summary>
	/// リセット時間
	/// </summary>
	const float ResetTime = 0.0f;

	/// <summary>
	/// ボード表示前待機時間
	/// </summary>
	const float BoardWaitTime = 0.3f;

	/// <summary>
	/// ボード表示後待機時間
	/// </summary>
	const float ShowBoardWaitTime = 0.5f;

	/// <summary>
	/// アニメーション待機時間
	/// </summary>
	const float AnimWaitTime = 0.3f;

	/// <summary>
	/// プレイヤー
	/// </summary>
	const FString PlayerText = "P";

	/// <summary>
	/// アニメーションインデックス初期値
	/// </summary>
	const int32 InitAnimIndex = 0;
};
