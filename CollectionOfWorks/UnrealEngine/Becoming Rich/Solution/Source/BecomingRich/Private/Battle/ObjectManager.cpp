﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/ObjectManager.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AObjectManager::AObjectManager() {

 	PrimaryActorTick.bCanEverTick = false;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AObjectManager::BeginPlay() {

	Super::BeginPlay();

	// フィールドマップ情報の読み込み
	FString ProjectPath = FPaths::ProjectDir();
	FString FilePath = ProjectPath + ObjectListCsvFilePath;

	ObjectList = ReadCsvObjectListFile(FilePath);
	if (ObjectList.Num() == Zero) {
		UE_LOG(LogTemp, Display, TEXT("AObjectManager ReadCsvObjectListFile Error"));
		return;
	}

	// フィールドデータの読み込み
	FilePath.Empty();
	FilePath = ProjectPath + ObjectDataCsvFilePath;

	ObjectData = ReadCsvObjectDataFile(FilePath);
	if (ObjectData.Num() == Zero) {
		UE_LOG(LogTemp, Display, TEXT("AObjectManager ReadCsvObjectDataFile Error"));
		return;
	}

	// オブジェクトの生成
	CreateObject();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AObjectManager::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}

/// <summary>
/// オブジェクトリストCSV読み込み処理
/// </summary>
/// <param name="FilePath"> ファイルパス </param>
/// <returns> オブジェクトリスト </returns>
TMap<FString, TMap<EObjectList, float>> AObjectManager::ReadCsvObjectListFile(FString FilePath) {

	UE_LOG(LogTemp, Display, TEXT("AObjectManager ReadCsvObjectListFile"));

	TMap<FString, TMap<EObjectList, float>> FileData;
	FileData.Empty();

	// CSVファイルからデータを取得
	FString CsvDataFile;
	FFileHelper::LoadFileToString(CsvDataFile, *FilePath);
	if (CsvDataFile.IsEmpty()) {
		return FileData;
	}

	// 各行で分割した値の保持
	TArray<FString> Row;
	// カンマ区切りごとの値
	TArray<FString> Element;
	TMap<EObjectList, float> ObjectTransData;

	// 行ごとに配列に取得
	CsvDataFile.ParseIntoArrayLines(Row);

	for (int i = 1; i < Row.Num(); ++i) {
		// カンマ区切りで取得
		Row[i].ParseIntoArray(Element, TEXT(","), true);

		// 取得した値を文字列から数値に変換
		for (int j = 1; j < Element.Num(); ++j) {
			ObjectTransData.Add(static_cast<EObjectList>(j - 1), FCString::Atof(*Element[j]));
		}
		// 配列に格納
		FileData.Add(Element[MinIndex], ObjectTransData);
		// 一時配列の初期化
		Element.Empty();
	}

	return FileData;
}

/// <summary>
/// オブジェクトデータCSV読み込み処理
/// </summary>
/// <param name="FilePath"> ファイルパス </param>
/// <returns> オブジェクトデータ </returns>
TMap<FString, FString> AObjectManager::ReadCsvObjectDataFile(FString FilePath) {

	UE_LOG(LogTemp, Display, TEXT("AObjectManager ReadCsvObjectDataFile"));

	TMap<FString, FString> FileData;
	FileData.Empty();

	// CSVファイルからデータを取得
	FString CsvDataFile;
	FFileHelper::LoadFileToString(CsvDataFile, *FilePath);
	if (CsvDataFile.IsEmpty()) {
		return FileData;
	}

	// 各行で分割した値の保持
	TArray<FString> Row;
	// カンマ区切りごとの値
	TArray<FString> Element;
	// 行ごとに配列に取得
	CsvDataFile.ParseIntoArrayLines(Row);

	for (int i = 0; i < Row.Num(); ++i) {
		// カンマ区切りで取得
		Row[i].ParseIntoArray(Element, TEXT(","), true);
		// 配列に格納
		FileData.Add(Element[MinIndex], Element[1]);
		// 一時配列の初期化
		Element.Empty();
	}

	return FileData;
}

/// <summary>
/// オブジェクト生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AObjectManager::CreateObject() {

	UE_LOG(LogTemp, Display, TEXT("AObjectManager CreateObject"));

	for (auto DataList : ObjectList) {

		// フィールド番号の先頭1桁を取得
		FString ObjectKey = DataList.Key.Left(4);
		if (!ObjectData.Contains(ObjectKey)) {
			UE_LOG(LogTemp, Display, TEXT("AObjectManager ObjectKey Error"));
			return;
		}
		// フロアの生成
		AObjectBase* Object = SpawnObject(ObjectData[ObjectKey]);
		if (!IsValid(Object)) {
			continue;
		}
		// 配置、スケール、角度の設定
		FVector Position;
		Position.X = DataList.Value[EObjectList::PosX];
		Position.Y = DataList.Value[EObjectList::PosY];
		Position.Z = DataList.Value[EObjectList::PosZ];
		FRotator Rotation;
		Rotation.Pitch = DataList.Value[EObjectList::RotX];
		Rotation.Yaw = DataList.Value[EObjectList::RotY];
		Rotation.Roll = DataList.Value[EObjectList::RotZ];
		FQuat Quat = FQuat(Rotation);
		FVector Scale;
		Scale.X = DataList.Value[EObjectList::ScaleX];
		Scale.Y = DataList.Value[EObjectList::ScaleY];
		Scale.Z = DataList.Value[EObjectList::ScaleZ];

		// 配置、スケール、角度のセット
		FTransform ActorTrans;
		ActorTrans.SetLocation(Position);
		ActorTrans.SetRotation(Quat);
		ActorTrans.SetScale3D(Scale);
		Object->SetActorTransform(ActorTrans);

		// オブジェクト識別番号をセット
		Object->SetObjectStrNum(DataList.Key);
		UE_LOG(LogTemp, Display, TEXT("AObjectManager ObjectNum : %s"), *DataList.Key);

		// 配列にセット
		ObjectDataDetail.Add(DataList.Key, Object);
	
	}

	UE_LOG(LogTemp, Display, TEXT("AObjectManager CreateObject Success!"));
}

/// <summary>
/// オブジェクトのスポーン
/// </summary>
/// <param name="オブジェクトクラスパス"> ObjectClassPath </param>
/// <returns></returns>
AObjectBase* AObjectManager::SpawnObject(FString ObjectClassPath) {

	// アクタクラス取得
	TSubclassOf<class AObjectBase> ObjectActor = TSoftClassPtr<AObjectBase>(FSoftObjectPath(*ObjectClassPath)).LoadSynchronous();
	if (!IsValid(ObjectActor)) {
		return nullptr;
	}
	AObjectBase* Object = GetWorld()->SpawnActor<AObjectBase>(ObjectActor);
	if (!IsValid(Object)) {
		return nullptr;
	}

	return Object;
}