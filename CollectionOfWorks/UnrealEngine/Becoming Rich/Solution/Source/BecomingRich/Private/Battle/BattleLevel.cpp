﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/BattleLevel.h"
#include "../Public/UiAssignmentGameInstance.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ABattleLevel::ABattleLevel()
	: CurrentPhase(BattlePhaseType::Initialize)
	, AccumulateTime(0.0f) {

	// Tickイベントの有効化
	PrimaryActorTick.bCanEverTick = true;
}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::BeginPlay() {

	Super::BeginPlay();

	UE_LOG(LogTemp, Display, TEXT("ABattleLevel BeginPlay"));

	// 累積時間を初期化
	AccumulateTime = 0.0f;
	// 現在のフェーズを「初期化処理」にセット
	CurrentPhase = BattlePhaseType::Initialize;
	CurrentEventPhase = EEventPhaseType::InitWait;

	// ゲームモードのインスタンスを取得しておく
	BattleGameMode = Cast<ABattleGameMode>(UGameplayStatics::GetGameMode(this));
	BattleGameMode->SetEventInterface(this);

	// バトルコントローラを取得、開始処理を実行
	ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(BattleController)) {
		return;
	}
	// イベントインターフェースのセット
	BattleController->SetEventInterface(this);
	// UIマネージャ処理の実行
	BattleController->OpenBattle();

	UE_LOG(LogTemp, Display, TEXT("ABattleLevel BattleController->OpenBattle()"));
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="InDeltaTime"> 処理時間 </param>
/// <returns></returns>
void ABattleLevel::Tick(float InDeltaTime) {

	switch (CurrentPhase) {
	case BattlePhaseType::Initialize: {
		//----------------------------
		//初期化処理フェーズ
		//----------------------------
		// フィールドマネージャ 初期化
		InitializeFieldManager();
		// プレイヤーマネージャ 初期化
		InitializePlayerManager();
		// フロアマネージャー 初期化
		InitializeFloorManager();
		// オブジェクトマネージャー 初期化
		InitializeObjectManager();

		// プレイカメラの生成
		CreatePlayCamera();
		// マップカメラの生成
		CreateMapCamera();
		// 矢印の生成
		CreateArrow();

		// 初期化処理
		Initialize();

		// 現在のフェーズを「ゲーム開始」に変更
		ChangeCurrentPhase(BattlePhaseType::GameStart);
		break;
	}
	case BattlePhaseType::GameStart: {
		//----------------------------
		//ゲーム開始フェーズ
		//----------------------------
		// ゲームモードのフェーズを「プレイヤー切り替え」に変更
		BattleGameMode->ChangeCurrentPhase(ETurnPhase::SwitchNextPlayer);
		// 現在のフェーズを「プレイ中」に変更
		ChangeCurrentPhase(BattlePhaseType::Play);
		break;
	}
	case BattlePhaseType::Play: {
		//----------------------------
		//プレイ中フェーズ
		//----------------------------
		break;
	}
	case BattlePhaseType::ChangeLevelPreparing: {
		//----------------------------
		//レベル変更準備フェーズ
		//----------------------------
		// バトルコントローラを取得、終了準備処理を実行
		ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
		if (!IsValid(BattleController)) {
			return;
		}
		// UIマネージャ処理の実行
		BattleController->ClosePreparing();
		// 累積時間をリセット
		AccumulateTime = ResetTime;
		
		UE_LOG(LogTemp, Display, TEXT("ABattleLevel ChangeLevelPreparing"));
		// 現在のフェーズを「レベル変更」に変更
		ChangeCurrentPhase(BattlePhaseType::ChangeLevel);
		break;
	}
	case BattlePhaseType::ChangeLevel: {
		//----------------------------
		//レベル変更フェーズ
		//----------------------------
		AccumulateTime += InDeltaTime;
		if (AccumulateTime < FinishWaitTime) {
			break;
		}
		AccumulateTime = ResetTime;

		// バトルコントローラを取得、終了処理を実行
		ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
		if (!IsValid(BattleController)) {
			return;
		}
		// UIマネージャ処理の実行
		BattleController->CloseBattle();
		// レベルを変更する
		UGameplayStatics::OpenLevel(GetWorld(), TEXT("TitleLevel"), true);
		// ブートレベルに遷移する
		UE_LOG(LogTemp, Display, TEXT("ABattleLevel ChangeLevel"));
		// 現在のフェーズを「終了処理」に変更
		ChangeCurrentPhase(BattlePhaseType::Finish);
		break;
	}
	case BattlePhaseType::Finish: {
		//----------------------------
		//終了処理フェーズ
		//----------------------------
		break;
	}
	}
}

/// <summary>
/// 現在のフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ABattleLevel::ChangeCurrentPhase(BattlePhaseType PhaseType) {

	CurrentPhase = PhaseType;
}

/// <summary>
/// イベントフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ABattleLevel::ChangeCurrentEventPhase(EEventPhaseType PhaseType) {

	CurrentEventPhase = PhaseType;
}

/// <summary>
/// フィールドマネージャ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::InitializeFieldManager() {

	UE_LOG(LogTemp, Display, TEXT("ABattleLevel InitializeFieldManager"));

	// アクタクラス取得
	FieldManagerClass = TSoftClassPtr<AFieldManager>(FSoftObjectPath(*FieldManagerPath)).LoadSynchronous();
	if (!IsValid(FieldManagerClass)) {
		return;
	}
	UE_LOG(LogTemp, Display, TEXT("ABattleLevel CreateFieldManagerClass"));

	FieldManager = GetWorld()->SpawnActor<AFieldManager>(FieldManagerClass);
	if (!IsValid(FieldManager)) {
		return;
	}
	UE_LOG(LogTemp, Display, TEXT("ABattleLevel CreateFieldManager"));
}

/// <summary>
/// プレイヤーマネージャ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::InitializePlayerManager() {

	UE_LOG(LogTemp, Display, TEXT("ABattleLevel InitializePlayerManager"));

	// アクタクラス取得
	PlayerManagerClass = TSoftClassPtr<APlayerManager>(FSoftObjectPath(*PlayerManagerPath)).LoadSynchronous();
	if (!IsValid(PlayerManagerClass)) {
		return;
	}
	UE_LOG(LogTemp, Display, TEXT("ABattleLevel CreatePlayerManagerClass"));

	PlayerManager = GetWorld()->SpawnActor<APlayerManager>(PlayerManagerClass);
	if (!IsValid(PlayerManager)) {
		return;
	}
	// イベントインターフェースのセット
	PlayerManager->SetEventInterface(this);
	// 初期化処理
	PlayerManager->Initialize();
	UE_LOG(LogTemp, Display, TEXT("ABattleLevel CreatePlayerManager"));
}

/// <summary>
/// フロアマネージャ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::InitializeFloorManager() {

	UE_LOG(LogTemp, Display, TEXT("ABattleLevel InitializeFloorManager"));

	// アクタクラス取得
	FloorManagerClass = TSoftClassPtr<AFloorManager>(FSoftObjectPath(*FloorManagerPath)).LoadSynchronous();
	if (!IsValid(FloorManagerClass)) {
		return;
	}
	UE_LOG(LogTemp, Display, TEXT("ABattleLevel CreateFloorManagerClass"));

	FloorManager = GetWorld()->SpawnActor<AFloorManager>(FloorManagerClass);
	if (!IsValid(FloorManager)) {
		return;
	}
	UE_LOG(LogTemp, Display, TEXT("ABattleLevel CreateFloorManager"));
}

/// <summary>
/// オブジェクトマネージャ 初期化処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::InitializeObjectManager() {

	UE_LOG(LogTemp, Display, TEXT("ABattleLevel InitializeObjectManager"));

	// アクタクラス取得
	ObjectManagerClass = TSoftClassPtr<AObjectManager>(FSoftObjectPath(*ObjectManagerPath)).LoadSynchronous();
	if (!IsValid(ObjectManagerClass)) {
		return;
	}
	UE_LOG(LogTemp, Display, TEXT("ABattleLevel CreateObjectManagerClass"));

	ObjectManager = GetWorld()->SpawnActor<AObjectManager>(ObjectManagerClass);
	if (!IsValid(ObjectManager)) {
		return;
	}
	UE_LOG(LogTemp, Display, TEXT("ABattleLevel CreateObjectManager"));
}

/// <summary>
/// プレイ用カメラ 生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::CreatePlayCamera() {

	UE_LOG(LogTemp, Display, TEXT("ABattleLevel CreatePlayCamera"));

	// カメラクラス取得
	PlayCameraClass = TSoftClassPtr<APlayCamera>(FSoftObjectPath(*PlayCameraPath)).LoadSynchronous();
	if (!IsValid(PlayCameraClass)) {
		return;
	}
	UE_LOG(LogTemp, Display, TEXT("ABattleLevel CreatePlayCameraClass"));

	PlayCamera = GetWorld()->SpawnActor<APlayCamera>(PlayCameraClass);
	if (!IsValid(PlayCamera)) {
		return;
	}
	UE_LOG(LogTemp, Display, TEXT("ABattleLevel CreatePlayCameraPtr"));
}

/// <summary>
/// マップ用カメラ 生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::CreateMapCamera() {

	UE_LOG(LogTemp, Display, TEXT("ABattleLevel CreateMapCamera"));

	// カメラクラス取得
	MapCameraClass = TSoftClassPtr<AMapCamera>(FSoftObjectPath(*MapCameraPath)).LoadSynchronous();
	if (!IsValid(MapCameraClass)) {
		return;
	}
	UE_LOG(LogTemp, Display, TEXT("ABattleLevel CreateMapCameraClass"));

	MapCamera = GetWorld()->SpawnActor<AMapCamera>(MapCameraClass);
	if (!IsValid(MapCamera)) {
		return;
	}
	UE_LOG(LogTemp, Display, TEXT("ABattleLevel CreateMapCameraPtr"));
}

/// <summary>
/// 分岐選択用矢印
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::CreateArrow() {

	UE_LOG(LogTemp, Display, TEXT("ABattleLevel CreateArrow"));

	// カメラクラス取得
	ArrowClass = TSoftClassPtr<AArrow>(FSoftObjectPath(*ArrowPath)).LoadSynchronous();
	if (!IsValid(ArrowClass)) {
		return;
	}
	UE_LOG(LogTemp, Display, TEXT("ABattleLevel CreateArrowClass"));

	Arrow = GetWorld()->SpawnActor<AArrow>(ArrowClass);
	if (!IsValid(Arrow)) {
		return;
	}
	UE_LOG(LogTemp, Display, TEXT("ABattleLevel CreateArrowPtr"));

	// 矢印を隠す
	Arrow->Hide();
	// イベントインターフェースのセット
	Arrow->SetEventInterface(this);
}

/// <summary>
/// 初期設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::Initialize() {

	// バトルウィジェットを取得
	UBattleWidget* BattleWidget = GetBattleWidget();
	if (!IsValid(BattleWidget)) {
		return;
	}

	//-----------------------------------
	// プレイヤー情報の初期化処理
	//-----------------------------------
	// 全てのプレイヤーがいると想定する
	TArray<EPlayerOrder> ArrayPlayerOrder;
	ArrayPlayerOrder.Add(EPlayerOrder::First);
	ArrayPlayerOrder.Add(EPlayerOrder::Second);
	ArrayPlayerOrder.Add(EPlayerOrder::Third);
	ArrayPlayerOrder.Add(EPlayerOrder::Fourth);

	for (EPlayerOrder Order : ArrayPlayerOrder) {

		// ターン表示パネルを非表示にする
		BattleWidget->HideTurnPanelWidget(Order);

		APlayerBase* Player = PlayerManager->GetPlayer(Order);
		if (IsValid(Player)) {
			// プレイヤーが存在するなら所持金を取得し、ウィジェットに渡す
			int32 Money = Player->GetPocketMoney();
			BattleWidget->MakeMoney(Order, Money);
			BattleWidget->ShowMoneyPanelWidget(Order);
			// ランクをセット
			BattleWidget->SetRank(Order);
			BattleWidget->SetInitPlayerState(Order, EStateType::Play);
		}
		else {
			// プレイヤーが存在しないなら架空の所持金をウィジェットに渡す
			int32 Money = NotExistPlayerMoney;
			BattleWidget->MakeMoney(Order, Money);
			// 存在しないプレイヤーのウィジェットを隠す
			BattleWidget->HideMoneyPanelWidget(Order);
		}

		// ここでは一旦ターンパネルを非表示にしておく
		BattleWidget->HideTurnPanelWidget(Order);
	}

	// 矢印を非表示にする
	BattleWidget->HideTurnArrowWidget();

	//-----------------------------------
	// ダイスの初期化処理
	//-----------------------------------
	// ダイスウィジェットを取得
	UDiceWidget* DiceWidget = GetDiceWidget();
	if (!IsValid(DiceWidget)) {
		return;
	}
	// ダイス操作ガイドは非表示にしておく
	DiceWidget->HideDiceGuide();

	//-----------------------------------
	// サウンドマネージャの初期設定
	//-----------------------------------
	// サウンドマネージャを取得
	ASoundManager* SoundManager = GetSoundManager();
	if (!IsValid(SoundManager)) {
		return;
	}
	// BGMの設定と再生
	SoundManager->SetBgmSound(EPlaySoundType::BattleBgm);
	SoundManager->PlayBgmSound();
}

/// <summary>
/// プレイカメラの位置情報セット
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::SetupPlayCameraPosition() {

	// バトルコントローラを取得
	ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(BattleController)) {
		return;
	}
	// 現在プレイ中のプレイヤーを取得
	APlayerBase* Player = GetPlayablePlayer();
	if (!IsValid(Player)) {
		return;
	}
	// プレイヤーの座標をカメラの座標にセットする
	FVector PlayerPos = Player->GetActorLocation();
	PlayerPos.Z = PlayCameraPosZ;
	PlayCamera->SetActorLocation(PlayerPos);
	BattleController->SetViewTargetWithBlend(PlayCamera);
}

/// <summary>
/// バトルウィジェットを取得
/// </summary>
/// <param name=""></param>
/// <returns> バトルウィジェット </returns>
UBattleWidget* ABattleLevel::GetBattleWidget() {

	// バトルコントローラを取得
	ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(BattleController)) {
		return nullptr;
	}
	// バトルウィジェットを取得
	UBattleWidget* Widget = BattleController->GetBattleWidget();
	if (!IsValid(Widget)) {
		return nullptr;
	}

	return Widget;
}

/// <summary>
/// リザルトウィジェットを取得
/// </summary>
/// <param name=""></param>
/// <returns> リザルトウィジェット </returns>
UResultWidget* ABattleLevel::GetResultWidget() {

	// バトルコントローラを取得
	ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(BattleController)) {
		return nullptr;
	}
	// リザルトウィジェットを取得
	UResultWidget* Widget = BattleController->GetResultWidget();
	if (!IsValid(Widget)) {
		return nullptr;
	}

	return Widget;
}

/// <summary>
/// ダイスウィジェットを取得
/// </summary>
/// <param name=""></param>
/// <returns> ダイスウィジェット </returns>
UDiceWidget* ABattleLevel::GetDiceWidget() {

	// バトルコントローラを取得
	ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(BattleController)) {
		return nullptr;
	}
	// ダイスウィジェットを取得
	UDiceWidget* Widget = BattleController->GetDiceWidget();
	if (!IsValid(Widget)) {
		return nullptr;
	}

	return Widget;
}

/// <summary>
/// ダイアログウィジェットを取得
/// </summary>
/// <param name=""></param>
/// <returns> ダイアログウィジェット </returns>
UDialogWidget* ABattleLevel::GetDialogWidget() {

	// バトルコントローラを取得
	ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(BattleController)) {
		return nullptr;
	}
	// ダイアログウィジェットを取得
	UDialogWidget* Widget = BattleController->GetDialogWidget();
	if (!IsValid(Widget)) {
		return nullptr;
	}

	return Widget;
}

/// <summary>
/// サウンドマネージャを取得
/// </summary>
/// <param name=""></param>
/// <returns> サウンドマネージャ </returns>
ASoundManager* ABattleLevel::GetSoundManager() {

	// バトルコントローラを取得
	ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(BattleController)) {
		return nullptr;
	}
	ASoundManager* SoundManager = BattleController->GetSoundManager();

	return SoundManager;
}

/// <summary>
/// 現在プレイ中のプレイヤーを取得
/// </summary>
/// <param name=""></param>
/// <returns> プレイヤー </returns>
APlayerBase* ABattleLevel::GetPlayablePlayer() {

	int32 Turn = PlayerManager->GetCurrentTurn();
	EPlayerOrder PlayerOrder = PlayerManager->GetOperationOrder(Turn);
	APlayerBase* Player = PlayerManager->GetPlayer(PlayerOrder);
	if (!IsValid(Player)) {
		return nullptr;
	}

	return Player;
}

/// <summary>
/// フィールドの座標を取得
/// </summary>
/// <param name="FieldNum"> フィールド番号 </param>
/// <returns> 表示座標 </returns>
FVector ABattleLevel::OnGetFieldPosition(int32 FieldNum) {

	FVector Position = FieldManager->GetFieldPosition(FieldNum);
	return Position;
}

/// <summary>
/// 前方選択
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnSelectFront() {

	// ターンフェーズが分岐選択中で
	// プレイヤーが移動待機中以外は処理終了
	bool IsSelectRoute = BattleGameMode->IsSelectRoute();
	bool IsWaitMove = PlayerManager->IsWaitMove();
	bool IsThink = PlayerManager->IsThinkMoveDirection();
	if (!IsSelectRoute && (!IsWaitMove || !IsThink)) {
		return;
	}

	// 選択方向が進行可能かチェック
	int32 FieldNum = FieldManager->GetNextMoveNumber(EDirectionType::Front);
	if (FieldNum == FieldNotExists) {
		return;
	}

	FVector Pos = FieldManager->GetFieldPosition(FieldNum);
	Pos.Z += AllowAdjustPosZ;
	// 矢印を指定方向へ表示する
	Arrow->SetActorLocation(Pos);

	// 前方を選択中にする
	FieldManager->SetSelectDirection(EDirectionType::Front);

	// 現在プレイ中のプレイヤーを取得
	APlayerBase* Player = GetPlayablePlayer();
	if (!IsValid(Player)) {
		return;
	}
	FRotator PlayerRot = FRotator(0.0f, 180.0f, 0.0f);
	// プレイヤーの向きを変える
	Player->SetActorRotation(PlayerRot);

	// サウンドマネージャを取得
	ASoundManager* SoundManager = GetSoundManager();
	if (!IsValid(SoundManager)) {
		return;
	}
	SoundManager->PlaySESound(EPlaySoundType::PressButton);
}

/// <summary>
/// 左方向選択
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnSelectLeft() {

	// ターンフェーズが分岐選択中で
	// プレイヤーが移動待機中以外は処理終了
	bool IsSelectRoute = BattleGameMode->IsSelectRoute();
	bool IsWaitMove = PlayerManager->IsWaitMove();
	bool IsThink = PlayerManager->IsThinkMoveDirection();
	if (!IsSelectRoute && (!IsWaitMove || !IsThink)) {
		return;
	}

	// 選択方向が進行可能かチェック
	int32 FieldNum = FieldManager->GetNextMoveNumber(EDirectionType::Left);
	if (FieldNum == FieldNotExists) {
		return;
	}

	FVector Pos = FieldManager->GetFieldPosition(FieldNum);
	Pos.Z += AllowAdjustPosZ;
	// 矢印を指定方向へ表示する
	Arrow->SetActorLocation(Pos);

	// 左方向を選択中にする
	FieldManager->SetSelectDirection(EDirectionType::Left);

	// 現在プレイ中のプレイヤーを取得
	APlayerBase* Player = GetPlayablePlayer();
	if (!IsValid(Player)) {
		return;
	}
	FRotator PlayerRot = FRotator(0.0f, 90.0f, 0.0f);
	// プレイヤーの向きを変える
	Player->SetActorRotation(PlayerRot);

	// サウンドマネージャを取得
	ASoundManager* SoundManager = GetSoundManager();
	if (!IsValid(SoundManager)) {
		return;
	}
	SoundManager->PlaySESound(EPlaySoundType::PressButton);
}

/// <summary>
/// 右方向選択
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnSelectRight() {

	// ターンフェーズが分岐選択中で
	// プレイヤーが移動待機中以外は処理終了
	bool IsSelectRoute = BattleGameMode->IsSelectRoute();
	bool IsWaitMove = PlayerManager->IsWaitMove();
	bool IsThink = PlayerManager->IsThinkMoveDirection();
	if (!IsSelectRoute && (!IsWaitMove || !IsThink)) {
		return;
	}

	// 選択方向が進行可能かチェック
	int32 FieldNum = FieldManager->GetNextMoveNumber(EDirectionType::Right);
	if (FieldNum == FieldNotExists) {
		return;
	}

	FVector Pos = FieldManager->GetFieldPosition(FieldNum);
	Pos.Z += AllowAdjustPosZ;
	// 矢印を指定方向へ表示する
	Arrow->SetActorLocation(Pos);

	// 右方向を選択中にする
	FieldManager->SetSelectDirection(EDirectionType::Right);

	// 現在プレイ中のプレイヤーを取得
	APlayerBase* Player = GetPlayablePlayer();
	if (!IsValid(Player)) {
		return;
	}
	FRotator PlayerRot = FRotator(0.0f, 270.0f, 0.0f);
	// プレイヤーの向きを変える
	Player->SetActorRotation(PlayerRot);

	// サウンドマネージャを取得
	ASoundManager* SoundManager = GetSoundManager();
	if (!IsValid(SoundManager)) {
		return;
	}
	SoundManager->PlaySESound(EPlaySoundType::PressButton);
}

/// <summary>
/// ダイス切り替え時間変更
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnChangeDiceTime() {

	UE_LOG(LogTemp, Display, TEXT("ABattleLevel OnChangeDiceTime"));

	// バトルコントローラを取得、終了処理を実行
	ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(BattleController)) {
		return;
	}
	// プレイヤーのフェーズがサイコロ待機中か
	bool IsWaitRoll = PlayerManager->IsWaitRollDice();
	if (!IsWaitRoll) {
		return;
	}
	// ダイスウィジェットを取得
	UDiceWidget* Widget = GetDiceWidget();
	if (!IsValid(Widget)) {
		return;
	}
	Widget->ChangeWaitTime(0.5f);
}

/// <summary>
/// マップ用カメラの切り替え
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnSwitchMapCamera() {

	UE_LOG(LogTemp, Display, TEXT("ABattleLevel SwitchMapCamera"));

	// バトルコントローラを取得、終了処理を実行
	ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(BattleController)) {
		return;
	}
	// プレイヤーのフェーズがサイコロ待機中か
	// 移動待機中のどちらでも無い場合は処理終了
	bool IsWaitRoll = PlayerManager->IsWaitRollDice();
	bool IsWaitMove = PlayerManager->IsWaitMove();
	if (!IsWaitRoll && !IsWaitMove) {
		return;
	}

	// 現在プレイ中のプレイヤーを取得
	APlayerBase* Player = GetPlayablePlayer();
	if (!IsValid(Player)) {
		return;
	}
	// ダイスウィジェットを取得
	UDiceWidget* DiceWidget = GetDiceWidget();
	if (!IsValid(DiceWidget)) {
		return;
	}
	// バトルウィジェットを取得
	UBattleWidget* BattleWidget = GetBattleWidget();
	if (!IsValid(BattleWidget)) {
		return;
	}
	// ダイアログウィジェットを取得
	UDialogWidget* DialogWidget = GetDialogWidget();
	if (!IsValid(DialogWidget)) {
		return;
	}

	// プレイヤー識別配列作成
	TArray<EPlayerOrder> ArrayPlayerOrder;
	ArrayPlayerOrder.Add(EPlayerOrder::First);
	ArrayPlayerOrder.Add(EPlayerOrder::Second);
	ArrayPlayerOrder.Add(EPlayerOrder::Third);
	ArrayPlayerOrder.Add(EPlayerOrder::Fourth);

	// ゲーム全体の現在のフェーズを取得
	ETurnPhase TurnPhase = BattleGameMode->GetCurrentPhase();
	switch (TurnPhase) {
	case ETurnPhase::RollDice:
	case ETurnPhase::SelectRoute: {
		//-----------------------------
		// サイコロ振り待機中
		// 分岐選択中
		//-----------------------------
		// プレイヤー座標を取得
		FVector PlayerPos = Player->GetActorLocation();
		FRotator PlayerRot = Player->GetActorRotation();
		MapCamera->SetActorLocation(PlayerPos);
		MapCamera->SetActorRotation(PlayerRot);
		// 画角を初期化
		MapCamera->InitializeFieldOfView();

		// PlayerControllerが所有しているアクターを手放す
		BattleController->UnPossess();
		// スポーンしたカメラを所有して操作可能にする
		BattleController->Possess(MapCamera);
		// 視点をマップカメラに移す
		BattleController->SetViewTargetWithBlend(MapCamera);

		// ダイスウィジェットを非表示にする
		DiceWidget->HideWidget();
		DiceWidget->HideDiceGuide();
		// 所持金、ターンパネルを非表示にする
		for (EPlayerOrder Order : ArrayPlayerOrder) {
			BattleWidget->HideMoneyPanelWidget(Order);
			BattleWidget->HideTurnPanelWidget(Order);
		}
		BattleWidget->HideTurnArrowWidget();

		// 移動待機中のみ矢印を隠す処理実行
		if (IsWaitMove) {
			Arrow->Hide();
		}

		// 現在のフェーズを「マップ確認」に変更する
		BattleGameMode->ChangeCurrentPhase(ETurnPhase::ConfirmMap);

		// プレイヤーの操作タイプと現在のフェーズを取得
		EOperationType OperationType = Player->GetOperationType();
		if (OperationType != EOperationType::Player) {
			return;
		}
		// ダイアログを表示
		DialogWidget->OutputSceneText(EOutputScene::OperateCamera, Zero, Zero);
		break;
	}
	case ETurnPhase::ConfirmMap: {
		//-----------------------------
		// マップ確認中
		//-----------------------------
		// PlayerControllerが所有しているアクターを手放す
		BattleController->UnPossess();
		// スポーンしたカメラを所有して操作可能にする
		BattleController->Possess(Player);
		// 視点をマップカメラに移す
		BattleController->SetViewTargetWithBlend(PlayCamera);

		// ダイスウィジェットを表示にする
		DiceWidget->ShowWidget();

		// プレイ人数を取得し、人数分だけ所持金パネルを表示する
		int32 Number = PlayerManager->GetPlayerNum();

		int32 PlayerCount = 0;
		for (EPlayerOrder Order : ArrayPlayerOrder) {
			// 指定プレイヤーを取得する
			APlayerBase* AnyPlayer = PlayerManager->GetPlayer(Order);
			if (!IsValid(AnyPlayer)) {
				continue;
			}
			// 所持金パネルを表示
			BattleWidget->ShowMoneyPanelWidget(Order);
			EStateType State = AnyPlayer->GetStateType();
			// プレイヤーがプレイ中ならターンパネルを表示
			//if (State != EStateType::Play) {
			//	continue;
			//}
			//BattleWidget->ShowTurnPanelWidget(Order);
			// カウンターを加算
			++PlayerCount;
		}
		// ターンパネル横の矢印を必要数表示
		//BattleWidget->ShowTurnArrowWidget(PlayerCount);

		if (IsWaitRoll) {
			// 現在のフェーズを「サイコロ待機中」に変更する
			BattleGameMode->ChangeCurrentPhase(ETurnPhase::RollDice);

			// ダイスガイドを表示にする
			DiceWidget->ShowDiceGuide();

			// プレイヤーの操作タイプと現在のフェーズを取得
			EOperationType OperationType = Player->GetOperationType();
			if (OperationType != EOperationType::Player) {
				return;
			}
			// ダイアログを表示
			DialogWidget->OutputSceneText(EOutputScene::WaitRollDice, Zero, Zero);
		}
		else if (IsWaitMove) {
			// PlayerControllerが所有しているアクターを手放す
			BattleController->UnPossess();
			// スポーンしたカメラを所有して操作可能にする
			BattleController->Possess(Arrow);
			// 視点をマップカメラに移す
			BattleController->SetViewTargetWithBlend(PlayCamera);
			// 矢印を表示する
			Arrow->Show();
			// 現在のフェーズを「分岐選択中」に変更する
			BattleGameMode->ChangeCurrentPhase(ETurnPhase::SelectRoute);
			
			// プレイヤーの操作タイプと現在のフェーズを取得
			EOperationType OperationType = Player->GetOperationType();
			if (OperationType != EOperationType::Player) {
				return;
			}
			// ダイアログを表示
			DialogWidget->OutputSceneText(EOutputScene::SelectRoute, Zero, Zero);
		}

		break;
	}
	}

	// サウンドマネージャを取得
	ASoundManager* SoundManager = GetSoundManager();
	if (!IsValid(SoundManager)) {
		return;
	}
	SoundManager->PlaySESound(EPlaySoundType::PressButton);
}

/// <summary>
/// サイコロを振る
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnRollDice() {

	UE_LOG(LogTemp, Display, TEXT("ABattleLevel OnRollDice"));

	// ダイスウィジェットを取得
	UDiceWidget* Widget = GetDiceWidget();
	if (!IsValid(Widget)) {
		return;
	}
	// 現在プレイ中のプレイヤーを取得
	APlayerBase* Player = GetPlayablePlayer();
	if (!IsValid(Player)) {
		return;
	}
	bool IsRollDice = BattleGameMode->IsRollDice();
	bool IsWaitRoll = Widget->IsWaitRollDice();

	// 全体フェーズが「ダイスを振る」で
	// ダイスのフェーズが「ダイス待機中」の場合のみ実行
	if (!IsRollDice || !IsWaitRoll) {
		UE_LOG(LogTemp, Display, TEXT("ABattleLevel NotTargetPhase"));
		return;
	}

	// ダイスのフェーズを「ダイスを振っている」に変更
	// 待機中アニメーションを停止、ガイドテキスト変更
	Widget->ChangeCurrentPhase(EDicePhaseType::Roll);
	Widget->StopWaitAnimation();

	// プレイヤーのフェーズを「サイコロ振り中」に変更
	PlayerManager->ChangeCurrentPhase(EOperatePhase::RollDice);

	// プレイヤーの操作タイプと現在のフェーズを取得
	EOperationType OperationType = Player->GetOperationType();
	if (OperationType != EOperationType::Player) {
		return;
	}
	Widget->StopDiceGuideAnimation();
	Widget->ChangeGuideText();

	// ダイアログウィジェットを取得
	UDialogWidget* DialogWidget = GetDialogWidget();
	if (!IsValid(DialogWidget)) {
		return;
	}
	// ダイアログを表示
	DialogWidget->OutputSceneText(EOutputScene::WaitDecideRollDice, Zero, Zero);

	// サウンドマネージャを取得
	ASoundManager* SoundManager = GetSoundManager();
	if (!IsValid(SoundManager)) {
		return;
	}
	SoundManager->PlaySESound(EPlaySoundType::StartRollDice);
}

/// <summary>
/// 決定操作
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnDecide() {

	UE_LOG(LogTemp, Display, TEXT("ABattleLevel OnDecide"));

	// 全体のフェーズを取得
	ETurnPhase TurnPhase = BattleGameMode->GetCurrentPhase();
	switch (TurnPhase) {
	case ETurnPhase::RollDice: {
		//-----------------------------
		//ダイスを振るフェーズ
		//-----------------------------
		// 現在プレイ中のプレイヤーを取得
		APlayerBase* Player = GetPlayablePlayer();
		if (!IsValid(Player)) {
			return;
		}
		// ダイスウィジェットを取得
		UDiceWidget* Widget = GetDiceWidget();
		if (!IsValid(Widget)) {
			return;
		}
		// ダイスのフェーズが「振っている」かどうか
		bool IsRoll = Widget->IsRollDice();
		if (!IsRoll) {
			return;
		}

		// 確定したダイスの出目を取得
		int32 Roll = Widget->GetCurrentDiceRoll();
		PlayerManager->SetForwardNum(Roll);
		UE_LOG(LogTemp, Display, TEXT("Dice:%d"), Roll);

		// ダイスのフェーズを「ダイス確定」に変更
		Widget->ChangeCurrentPhase(EDicePhaseType::Confirm);
		// 押しましたアニメーションを実行
		Widget->PlayStartAnimation();
		// プレイヤーのフェーズを「待機中」に変更
		PlayerManager->ChangeCurrentPhase(EOperatePhase::Wait);

		// プレイヤーの操作タイプと現在のフェーズを取得
		EOperationType OperationType = Player->GetOperationType();
		if (OperationType != EOperationType::Player) {
			return;
		}
		// 操作ガイドをフェードアウトさせる
		Widget->PlayFadeOutDiceGuideAnimation();

		// ダイアログウィジェットを取得
		UDialogWidget* DialogWidget = GetDialogWidget();
		if (!IsValid(DialogWidget)) {
			return;
		}
		// ダイアログを非表示
		DialogWidget->Hide();

		// サウンドマネージャを取得
		ASoundManager* SoundManager = GetSoundManager();
		if (!IsValid(SoundManager)) {
			return;
		}
		SoundManager->PlaySESound(EPlaySoundType::StopRollDice);

		break;
	}
	case ETurnPhase::SelectRoute: {
		//-----------------------------
		//分岐選択フェーズ
		//-----------------------------
		// プレイヤー側フェーズを「方向決定」フェーズに変更
		PlayerManager->ChangeCurrentPhase(EOperatePhase::DecideMoveDirection);
		// 全体のフェーズを「分岐決定」フェーズに変更
		BattleGameMode->ChangeCurrentPhase(ETurnPhase::DecideRoute);
		
		// 現在プレイ中のプレイヤーを取得
		APlayerBase* Player = GetPlayablePlayer();
		if (!IsValid(Player)) {
			return;
		}
		// バトルコントローラを取得、終了処理を実行
		ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
		if (!IsValid(BattleController)) {
			return;
		}
		// PlayerControllerが所有しているアクターを手放す
		BattleController->UnPossess();
		// スポーンしたカメラを所有して操作可能にする
		BattleController->Possess(Player);
		// 視点をマップカメラに移す
		BattleController->SetViewTargetWithBlend(PlayCamera);

		Arrow->Hide();
		
		// プレイヤーの操作タイプと現在のフェーズを取得
		EOperationType OperationType = Player->GetOperationType();
		if (OperationType != EOperationType::Player) {
			return;
		}
		// ダイアログウィジェットを取得
		UDialogWidget* DialogWidget = GetDialogWidget();
		if (!IsValid(DialogWidget)) {
			return;
		}
		// ダイアログを非表示
		DialogWidget->Hide();
		break;
	}
	case ETurnPhase::WaitFinishResult: {
		//-----------------------------
		//レベル遷移待機(終了待機)
		//-----------------------------
		// 現在のフェーズを「レベル遷移準備」に変更
		ChangeCurrentPhase(BattlePhaseType::ChangeLevelPreparing);

		// リザルトウィジェットの取得
		UResultWidget* ResultWidget = GetResultWidget();
		if (!IsValid(ResultWidget)) {
			return;
		}
		ResultWidget->HideWidget();
		ResultWidget->HideRankPanel();
		ResultWidget->HideGuidePanel();
		break;
	}
	default: {
		//-----------------------------
		//それ以外
		//-----------------------------
		break;
	}
	}
}

/// <summary>
/// マスイベント処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns> true:処理終了, false:処理中 </returns>
bool ABattleLevel::OnOccurEvent(float DeltaTime) {

	switch (CurrentEventPhase) {
	case EEventPhaseType::InitWait: {
		//-----------------------------
		//初期待機フェーズ
		//-----------------------------
		AccumulateTime += DeltaTime;
		if (AccumulateTime < EventInitWaitTime) {
			break;
		}
		AccumulateTime = ResetTime;

		// 現在のイベントフェーズを「イベント確認」に変更
		ChangeCurrentEventPhase(EEventPhaseType::SearchEvent);
		break;
	}
	case EEventPhaseType::SearchEvent: {
		//-----------------------------
		//イベント探索フェーズ
		//-----------------------------
		// 現在プレイ中のプレイヤーを取得
		APlayerBase* Player = GetPlayablePlayer();
		if (!IsValid(Player)) {
			return false;
		}
		// フィールド番号からイベント内容取得
		int32 FieldNum = Player->GetFieldNum();
		TArray<int32> Event = FieldManager->GetFieldEvent(FieldNum);
		EFieldType Type = static_cast<EFieldType>(Event[ColEventType]);
		switch (Type) {
		case EFieldType::Goal: {
			//---------------------
			//ゴール
			//---------------------
			int32 Money = 0;
			// バトルウィジェットを取得
			UBattleWidget* BattleWidget = GetBattleWidget();
			if (!IsValid(BattleWidget)) {
				return false;
			}

			// ゴールの着順を取得
			int32 ArrivalOrder = PlayerManager->GetArrivalGoalOrder();
			if (ArrivalOrder == FirstOrder) {
				// 1着
				Money = FirstOrderMoney;
			}
			else if (ArrivalOrder == SecondOrder) {
				// 2着
				Money = SecondOrderMoney;
			}
			else if (ArrivalOrder == ThirdOrder) {
				// 3着
				Money = ThirdOrderMoney;
			}
			else if (ArrivalOrder == FourthOrder) {
				// 4着
				Money = FourthOrderMoney;
			}

			// 対象プレイヤーの所持金に獲得金額を加算
			EPlayerOrder Order = Player->GetPlayerOrder();
			Player->AddPocketMoney(Money);

			UE_LOG(LogTemp, Warning, TEXT("Order:%d"), Order);
			UE_LOG(LogTemp, Warning, TEXT("GetMoneyValue:%d"), Money);

			// ダイアログウィジェットを取得
			UDialogWidget* DialogWidget = GetDialogWidget();
			if (!IsValid(DialogWidget)) {
				return false;
			}
			// ダイアログを表示
			DialogWidget->Show();
			DialogWidget->OutputSceneText(EOutputScene::ArriveGoal, FMath::Abs(Money), ArrivalOrder);

			break;
		}
		case EFieldType::None:
		case EFieldType::Normal: {
			//---------------------
			//何もしない
			//---------------------
			// ダイアログウィジェットを取得
			UDialogWidget* DialogWidget = GetDialogWidget();
			if (!IsValid(DialogWidget)) {
				return false;
			}
			// ダイアログを表示
			DialogWidget->Show();
			DialogWidget->OutputSceneText(EOutputScene::EventNone, Zero, Zero);
			// 現在のイベントフェーズを「終了待機」に変更
			ChangeCurrentEventPhase(EEventPhaseType::EventWait2);
			break;
		}
		case EFieldType::Plus: {
			//---------------------
			//所持金増加マス
			//---------------------
			// 対象プレイヤーの所持金に獲得金額を加算
			int32 Money = Event[ColEvent];
			Player->AddPocketMoney(Money);

			// ダイアログウィジェットを取得
			UDialogWidget* DialogWidget = GetDialogWidget();
			if (!IsValid(DialogWidget)) {
				return false;
			}
			// ダイアログを表示
			DialogWidget->Show();
			DialogWidget->OutputSceneText(EOutputScene::EventGetMoney, FMath::Abs(Money), Zero);

			break;
		}
		case EFieldType::Minus: {
			//---------------------
			//所持金減少マス
			//---------------------
			// 対象プレイヤーの所持金に獲得金額を加算
			int32 Money = Minus * Event[ColEvent];
			Player->AddPocketMoney(Money);

			// ダイアログウィジェットを取得
			UDialogWidget* DialogWidget = GetDialogWidget();
			if (!IsValid(DialogWidget)) {
				return false;
			}
			// ダイアログを表示
			DialogWidget->Show();
			DialogWidget->OutputSceneText(EOutputScene::EventLostMoney, FMath::Abs(Money), Zero);

			break;
		}
		case EFieldType::Move: {
			//---------------------
			//移動マス
			//---------------------
			int32 GoalFieldNum = Event[ColEvent];

			// ダイアログウィジェットを取得
			UDialogWidget* DialogWidget = GetDialogWidget();
			if (!IsValid(DialogWidget)) {
				return false;
			}
			// ダイアログを表示
			DialogWidget->Show();
			DialogWidget->OutputSceneText(EOutputScene::EventMove, Zero, Zero);
			break;
		}
		}

		// 現在のイベントフェーズを「イベント待機」に変更
		ChangeCurrentEventPhase(EEventPhaseType::EventWait0);
		break;
	}
	case EEventPhaseType::EventWait0: {
		//-----------------------------
		//イベント待機フェーズ
		//-----------------------------
		AccumulateTime += DeltaTime;
		if (AccumulateTime < 1.0f) {
			break;
		}
		AccumulateTime = ResetTime;

		// 現在のイベントフェーズを「イベント実行」に変更
		ChangeCurrentEventPhase(EEventPhaseType::ExecuteEvent);
		break;
	}
	case EEventPhaseType::ExecuteEvent: {
		//-----------------------------
		//イベント実行フェーズ
		//-----------------------------
		// 現在プレイ中のプレイヤーを取得
		APlayerBase* Player = GetPlayablePlayer();
		if (!IsValid(Player)) {
			return false;
		}
		// フィールド番号からイベント内容取得
		int32 FieldNum = Player->GetFieldNum();
		TArray<int32> Event = FieldManager->GetFieldEvent(FieldNum);
		EFieldType Type = static_cast<EFieldType>(Event[ColEventType]);
		switch (Type) {
		case EFieldType::Goal: {
			//---------------------
			//ゴール
			//---------------------
			// バトルウィジェットを取得
			UBattleWidget* BattleWidget = GetBattleWidget();
			if (!IsValid(BattleWidget)) {
				return false;
			}
			// 対象プレイヤーの所持金に獲得金額を加算
			EPlayerOrder Order = Player->GetPlayerOrder();
			int32 CurrentMoneyPlayer = Player->GetPocketMoney();
			int32 CurrentMoneyWidget = BattleWidget->GetPocketMoney(Order);
			int32 DiffMoney = CurrentMoneyPlayer - CurrentMoneyWidget;

			UE_LOG(LogTemp, Warning, TEXT("Player:%d"), Order);
			UE_LOG(LogTemp, Warning, TEXT("CurrentMoneyPlayer:%d"), CurrentMoneyPlayer);
			UE_LOG(LogTemp, Warning, TEXT("CurrentMoneyWidget:%d"), CurrentMoneyWidget);
			UE_LOG(LogTemp, Warning, TEXT("DiffMoney:%d"), DiffMoney);

			// ウィジェットに所持金をセット
			BattleWidget->MakeMoney(Order, DiffMoney);
			BattleWidget->ChangeCurrentMoneyPhase(EMoneyPanelPhase::Sort);
			// ウィジェットのプレイヤーの状態をゴールにする
			BattleWidget->SetPlayerStateGoal(Order);

			// 現在のイベントフェーズを「終了待機」に変更
			ChangeCurrentEventPhase(EEventPhaseType::EventWait1);

			// サウンドマネージャを取得
			ASoundManager* SoundManager = GetSoundManager();
			if (!IsValid(SoundManager)) {
				return false;
			}
			SoundManager->PlaySESound(EPlaySoundType::Goal01);
			break;
		}
		case EFieldType::None:
		case EFieldType::Normal: {
			//---------------------
			//何もしない
			//---------------------
			// 現在のイベントフェーズを「終了待機」に変更
			ChangeCurrentEventPhase(EEventPhaseType::EventWait2);
			return false;
		}
		case EFieldType::Plus: {
			//---------------------
			//所持金増加マス
			//---------------------
			// 対象プレイヤーの所持金に獲得金額を加算
			int32 Money = Event[ColEvent];
			// バトルウィジェットを取得
			UBattleWidget* BattleWidget = GetBattleWidget();
			if (!IsValid(BattleWidget)) {
				return false;
			}
			// 対象プレイヤーの所持金に獲得金額を加算
			EPlayerOrder Order = Player->GetPlayerOrder();
			// ウィジェットに所持金をセット
			BattleWidget->MakeMoney(Order, Money);
			BattleWidget->ChangeCurrentMoneyPhase(EMoneyPanelPhase::Sort);

			// アニメーション実行
			Player->StartGetMoney();

			// 現在のイベントフェーズを「イベント待機」に変更
			ChangeCurrentEventPhase(EEventPhaseType::EventWait1);

			// サウンドマネージャを取得
			ASoundManager* SoundManager = GetSoundManager();
			if (!IsValid(SoundManager)) {
				break;
			}
			SoundManager->PlaySESound(EPlaySoundType::GetMoney);
			break;
		}
		case EFieldType::Minus: {
			//---------------------
			//所持金減少マス
			//---------------------
			// 対象プレイヤーの所持金に獲得金額を加算
			int32 Money = Minus * Event[ColEvent];
			// バトルウィジェットを取得
			UBattleWidget* BattleWidget = GetBattleWidget();
			if (!IsValid(BattleWidget)) {
				return false;
			}
			// 対象プレイヤーの所持金に獲得金額を加算
			EPlayerOrder Order = Player->GetPlayerOrder();
			// ウィジェットに所持金をセット
			BattleWidget->MakeMoney(Order, Money);
			BattleWidget->ChangeCurrentMoneyPhase(EMoneyPanelPhase::Sort);

			// アニメーション実行
			Player->StartLostMoney();

			// 現在のイベントフェーズを「イベント待機」に変更
			ChangeCurrentEventPhase(EEventPhaseType::EventWait1);

			// サウンドマネージャを取得
			ASoundManager* SoundManager = GetSoundManager();
			if (!IsValid(SoundManager)) {
				break;
			}
			SoundManager->PlaySESound(EPlaySoundType::LostMoney);
			break;
		}
		case EFieldType::Move: {
			//---------------------
			//移動マス
			//---------------------
			int32 GoalFieldNum = Event[ColEvent];
			FVector GoalPos = FieldManager->GetFieldPosition(GoalFieldNum);
			GoalPos.Z = PlayerPosZ;
			// プレイヤーの移動目標座標にセット
			Player->SetFieldNum(GoalFieldNum);
			PlayerManager->SetGoalPosition(GoalPos);
			PlayerManager->ChangeCurrentMovePhase(EEventMovePhase::MovePrepare);

			// 現在のイベントフェーズを「イベント待機」に変更
			ChangeCurrentEventPhase(EEventPhaseType::EventWait3);

			// サウンドマネージャを取得
			ASoundManager* SoundManager = GetSoundManager();
			if (!IsValid(SoundManager)) {
				break;
			}
			SoundManager->PlaySESound(EPlaySoundType::WarpPlayer);
			break;
		}
		}
		break;
	}
	case EEventPhaseType::EventWait1: {
		//-----------------------------
		//イベント待機フェーズ
		//-----------------------------
		AccumulateTime += DeltaTime;
		if (AccumulateTime < EventWaitTime) {
			break;
		}
		AccumulateTime = ResetTime;

		// 現在のイベントフェーズを「終了」に変更
		ChangeCurrentEventPhase(EEventPhaseType::Finish);
		break;
	}
	case EEventPhaseType::EventWait2: {
		//-----------------------------
		//イベント待機フェーズ
		//-----------------------------
		AccumulateTime += DeltaTime;
		if (AccumulateTime < EventShortWaitTime) {
			break;
		}
		AccumulateTime = ResetTime;

		// 現在のイベントフェーズを「終了」に変更
		ChangeCurrentEventPhase(EEventPhaseType::Finish);
		break;
	}
	case EEventPhaseType::EventWait3: {
		//-----------------------------
		//イベント待機フェーズ
		//-----------------------------
		break;
	}
	case EEventPhaseType::Finish: {
		//-----------------------------
		//終了フェーズ
		//-----------------------------
		// 現在のイベントフェーズを「初期待機」に変更
		ChangeCurrentEventPhase(EEventPhaseType::SearchEvent);

		// 現在プレイ中のプレイヤーを取得
		APlayerBase* Player = GetPlayablePlayer();
		if (!IsValid(Player)) {
			return false;
		}
		// プレイヤーアニメーションを元に戻す
		Player->ResetAnimation();

		// ダイアログウィジェットを取得
		UDialogWidget* DialogWidget = GetDialogWidget();
		if (!IsValid(DialogWidget)) {
			return true;
		}
		// ダイアログを非表示にする
		DialogWidget->Hide();

		return true;
	}
	}

	return false;
}

/// <summary>
/// プレイヤーの切り替え
/// </summary>
/// <param name=""></param>
/// <returns> true:リザルトへ, false:プレイ続行 </returns>
bool ABattleLevel::OnSwitchPlayer() {

	// 現在のプレイヤーを取得し、マスの端に配置する
	APlayerBase* Player = GetPlayablePlayer();
	if (!IsValid(Player)) {
		return false;
	}
	EPlayerOrder Order = Player->GetPlayerOrder();
	int32 Number = Player->GetFieldNum();
	// 現在の座標と待機座標の調整値を取得
	FVector AdjustPos = PlayerManager->GetWaitAdjustPosition(Order);
	FVector CurPos = FieldManager->GetFieldPosition(Number);
	Player->SetActorLocation(CurPos + AdjustPos);

	FVector Scale = FVector(WaitSizeX, WaitSizeY, WaitSizeZ);
	FTransform ActorTrans = Player->GetActorTransform();
	ActorTrans.SetLocation(CurPos + AdjustPos);
	ActorTrans.SetScale3D(Scale);
	Player->SetActorRelativeTransform(ActorTrans);
	
	//--------------------------
	// プレイヤーを切り替える
	//--------------------------
	bool IsFinish = PlayerManager->SwitchOperationPlayer();
	if (IsFinish) {
		return true;
	}

	// プレイヤーを再取得する
	Player = GetPlayablePlayer();
	if (!IsValid(Player)) {
		return false;
	}

	// プレイヤーが非表示なら表示する
	bool IsCheck = Player->IsShow();
	if (!IsCheck) {
		Player->Show();
		Player->SetShowFlag(true);
	}

	return false;
}

/// <summary>
/// 各オブジェクトの切り替え
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnSwitchObject() {

	// バトルコントローラを取得
	ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(BattleController)) {
		return;
	}
	//-----------------------
	// カメラを切り替える
	//-----------------------
	APlayerBase* Player = GetPlayablePlayer();
	if (!IsValid(Player)) {
		return;
	}
	// プレイヤーをマスの中央に配置
	int32 Number = Player->GetFieldNum();
	FVector CurPos = FieldManager->GetFieldPosition(Number);
	CurPos.Z = PlayerPosZ;
	Player->SetActorLocation(CurPos);

	// PlayerControllerが所有しているアクターを手放す
	BattleController->UnPossess();
	// スポーンしたプレイヤーを所有して操作可能にする
	BattleController->Possess(Player);

	// 視点をマップカメラに移す
	SetupPlayCameraPosition();

	FVector Scale = FVector(PlaySizeX, PlaySizeY, PlaySizeZ);
	FTransform ActorTrans = Player->GetActorTransform();
	ActorTrans.SetLocation(CurPos);
	ActorTrans.SetScale3D(Scale);
	Player->SetActorTransform(ActorTrans);

	// バトルウィジェットの取得
	EPlayerOrder Order = Player->GetPlayerOrder();
	UBattleWidget* BattleWidget = GetBattleWidget();
	if (!IsValid(BattleWidget)) {
		return;
	}
	//--------------------------------
	// ターンパネルのプレイヤー修正
	// アニメーション実行
	//--------------------------------
	// 現在のターンフレームの処理を停止する
	BattleWidget->HideTurnFrame();
	BattleWidget->StopTurnFrame();

	BattleWidget->SetCurrentTurn(Order);
	switch (Order) {
	case EPlayerOrder::First: {
		BattleWidget->PlayTurnStart01Animation();
		break;
	}
	case EPlayerOrder::Second: {
		BattleWidget->PlayTurnStart02Animation();
		break;
	}
	case EPlayerOrder::Third: {
		BattleWidget->PlayTurnStart03Animation();
		break;
	}
	case EPlayerOrder::Fourth: {
		BattleWidget->PlayTurnStart04Animation();
		break;
	}
	}

	// 現在のフェーズを「パネルを隠す」に変更
	//BattleWidget->ChangeCurrentTurnPhase(ETurnPanelPhase::HidePanel);
	// ターンフレームの位置を変更し、アニメーションを開始する
	BattleWidget->SwitchTurnFrame();

	// ダイスウィジェットを取得
	UDiceWidget* DiceWidget = GetDiceWidget();
	if (!IsValid(DiceWidget)) {
		return;
	}
	// ダイスインデックスを初期化
	int32 Index = DiceWidget->InitializeDiceIndex();
	// ウィジェットをリセット
	DiceWidget->ChangeDiceTexture(Index);
	DiceWidget->ChangeRemainAdvancedNum(Index);
	// 待機中アニメーションの実行
	DiceWidget->PlayWaitAnimation();
}


/// <summary>
/// ターン開始処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnStartTurn() {

	UE_LOG(LogTemp, Display, TEXT("ABattleLevel OnStartTurn"));

	// ダイスウィジェットを取得
	UDiceWidget* DiceWidget = GetDiceWidget();
	if (!IsValid(DiceWidget)) {
		return;
	}
	// ダイスを「ダイス待機中」に変更
	// ガイドテキスト変更
	DiceWidget->ChangeCurrentPhase(EDicePhaseType::WaitRoll);

	UE_LOG(LogTemp, Display, TEXT("ABattleLevel Widget->WaitRoll"));

	// 操作フェーズを「サイコロ待機中」に変更
	PlayerManager->ChangeCurrentPhase(EOperatePhase::WaitRollDice);

	// 現在のプレイヤーを取得し、マスの端に配置する
	APlayerBase* Player = GetPlayablePlayer();
	if (!IsValid(Player)) {
		return;
	}
	// プレイヤーの操作タイプと現在のフェーズを取得
	EOperationType OperationType = Player->GetOperationType();
	if (OperationType != EOperationType::Player) {
		return;
	}
	// ガイドテキスト変更、ガイドアニメーション実行
	DiceWidget->ChangeGuideText();
	DiceWidget->PlayDiceGuideAnimation();

	// ダイアログウィジェットを取得
	UDialogWidget* DialogWidget = GetDialogWidget();
	if (!IsValid(DialogWidget)) {
		return;
	}
	// ダイアログを表示
	DialogWidget->Show();
	DialogWidget->OutputSceneText(EOutputScene::WaitRollDice, Zero, Zero);
}

/// <summary>
/// サイコロ処理が終了しているかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:終了, false:処理中 </returns>
bool ABattleLevel::OnIsFinishDice() {

	// ダイスウィジェットを取得
	UDiceWidget* Widget = GetDiceWidget();
	if (!IsValid(Widget)) {
		return false;
	}
	// ダイスのフェーズが「終了」かどうか
	bool IsFinish = Widget->IsFinish();
	if (!IsFinish) {
		return false;
	}

	return true;
}

/// <summary>
/// 操作フェーズを「移動」に変更
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnChangePlayerPhaseMove() {

	// 現在プレイ中のプレイヤーを取得
	APlayerBase* Player = GetPlayablePlayer();
	if (!IsValid(Player)) {
		return;
	}
	PlayerManager->ChangeCurrentPhase(EOperatePhase::PrepareMove);
}

/// <summary>
/// 次に進むマスを探索
/// </summary>
/// <param name=""></param>
/// <returns> フィールド番号の配列 </returns>
TArray<int32> ABattleLevel::OnSearchForward() {

	TArray<int32> ArrayFieldNum;
	TMap<EDirectionType, int32> ArrayFieldData;
	EDirectionType Direction = EDirectionType::Front;

	// 現在プレイ中のプレイヤーを取得
	APlayerBase* Player = GetPlayablePlayer();
	if (!IsValid(Player)) {
		return ArrayFieldNum;
	}
	// プレイヤーが存在する座標を取得
	int32 FieldNum = Player->GetFieldNum();

	// 現在の位置から移動可能な座標を取得
	ArrayFieldData = FieldManager->SearchCanForward(FieldNum);
	// 進めるマスの数だけ取得
	for (auto Data : ArrayFieldData) {
		if (ArrayFieldData[Data.Key] == FieldNotExists) {
			continue;
		}
		// 配列にマスを格納する
		ArrayFieldNum.Add(ArrayFieldData[Data.Key]);
		Direction = Data.Key;
	}

	// 進める方向からプレイヤーの向きを変更する
	if (ArrayFieldNum.Num() == One) {
		
		FRotator PlayerRot;
		switch (Direction) {
		case EDirectionType::Front: {

			PlayerRot = FRotator(0.0f, 180.0f, 0.0f);
			break;
		}
		case EDirectionType::Right: {

			PlayerRot = FRotator(0.0f, 270.0f, 0.0f);
			break;
		}
		case EDirectionType::Left: {

			PlayerRot = FRotator(0.0f, 90.0f, 0.0f);
			break;
		}
		}
		// プレイヤーの向きを変える
		Player->SetActorRotation(PlayerRot);
	}

	return ArrayFieldNum;
}

/// <summary>
/// 分岐選択準備
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnPrepareSelectRoute() {

	TArray<EDirectionType> ArrayDirection;
	ArrayDirection.Add(EDirectionType::Front);
	ArrayDirection.Add(EDirectionType::Left);
	ArrayDirection.Add(EDirectionType::Right);

	FVector NextPos;

	// 進むことができる方向を取得
	for (auto Direction : ArrayDirection) {

		int32 NextNum = FieldManager->GetNextMoveNumber(Direction);
		if (NextNum == FieldNotExists) {
			continue;
		}
		// 選択方向をセットする
		FieldManager->SetSelectDirection(Direction);
		// 座標を取得
		NextPos = FieldManager->GetFieldPosition(NextNum);
		break;
	}

	// 矢印の表示座標をセット
	NextPos.Z += AllowAdjustPosZ;
	Arrow->SetActorLocation(NextPos);

	// 矢印を表示する
	Arrow->Show();

	// 現在のフェーズを「分岐選択」フェーズに遷移
	BattleGameMode->ChangeCurrentPhase(ETurnPhase::SelectRoute);

	// 現在プレイ中のプレイヤーを取得
	APlayerBase* Player = GetPlayablePlayer();
	if (!IsValid(Player)) {
		return;
	}

	// バトルコントローラを取得、終了処理を実行
	ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
	if (!IsValid(BattleController)) {
		return;
	}
	// PlayerControllerが所有しているアクターを手放す
	BattleController->UnPossess();
	// スポーンしたカメラを所有して操作可能にする
	BattleController->Possess(Arrow);
	// 視点をマップカメラに移す
	BattleController->SetViewTargetWithBlend(PlayCamera);

	// プレイヤーの操作タイプと現在のフェーズを取得
	EOperationType OperationType = Player->GetOperationType();
	if (OperationType != EOperationType::Player) {
		return;
	}
	// ダイアログウィジェットを取得
	UDialogWidget* DialogWidget = GetDialogWidget();
	if (!IsValid(DialogWidget)) {
		return;
	}
	// ダイアログを表示
	DialogWidget->Show();
	DialogWidget->OutputSceneText(EOutputScene::SelectRoute, Zero, Zero);
}

/// <summary>
/// 分岐選択思考
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnThinkSelectRoute() {

	int32 FieldPos = FieldNotExists;
	TMap<EDirectionType, int32> NextPos = FieldManager->GetNextPos();

	// 現在のプレイヤーを取得し、マスの端に配置する
	APlayerBase* Player = GetPlayablePlayer();
	if (!IsValid(Player)) {
		return;
	}
	// プレイヤーの操作タイプと現在のフェーズを取得
	EOperationType OperationType = Player->GetOperationType();
	switch (OperationType) {
	case EOperationType::CpuTypeA: {
		//--------------------------
		//最短距離を選ぶ
		//--------------------------
		FieldPos = FieldManager->SearchShortAndLongRoute(ESearchType::ShortRoute, NextPos);
		break;
	}
	case EOperationType::CpuTypeB: {
		//--------------------------
		//最長距離を選ぶ
		//--------------------------
		FieldPos = FieldManager->SearchShortAndLongRoute(ESearchType::LongRoute, NextPos);
		break;
	}
	case EOperationType::CpuTypeC: {
		//--------------------------
		//プレイヤーを追う
		//--------------------------
		TArray<EPlayerOrder> ArrayOrder;
		ArrayOrder.Add(EPlayerOrder::First);
		ArrayOrder.Add(EPlayerOrder::Second);
		ArrayOrder.Add(EPlayerOrder::Third);
		ArrayOrder.Add(EPlayerOrder::Fourth);
		TArray<APlayerBase*> ArrayPlayer;

		for (EPlayerOrder Order : ArrayOrder) {
			// プレイヤーを順番に取得
			APlayerBase* TempPlayer = PlayerManager->GetPlayer(Order);
			if (!IsValid(TempPlayer)) {
				break;
			}
			// ユーザー操作かどうか確認
			// ユーザー操作なら配列に格納する
			EOperationType Type = TempPlayer->GetOperationType();
			if (Type == EOperationType::Player) {
				ArrayPlayer.Add(TempPlayer);
			}
		}

		// ユーザー操作の数に応じて処理実行
		if (ArrayPlayer.Num() == Zero) {
			// ユーザー操作がいない場合は最長距離を探索
			FieldPos = FieldManager->SearchShortAndLongRoute(ESearchType::LongRoute, NextPos);
		}
		else {
			int32 PlayerPos;
			if (ArrayPlayer.Num() > One) {
				// ユーザー操作が複数いる場合はランダムに選択
				// 乱数にて取得取得した数値を対象人数で割った
				// 余りをインデックスとして使用する
				int32 Number = FMath::RandRange(One, Hundred);
				int32 PlayerNum = ArrayPlayer.Num();
				int32 Index = static_cast<int32>(FMath::Fmod(Number, PlayerNum));
				// プレイヤーのいるマスを取得
				PlayerPos = ArrayPlayer[Index]->GetFieldNum();
			}
			else {
				PlayerPos = ArrayPlayer[MinIndex]->GetFieldNum();
			}
			// 対象プレイヤーがいる道を探索
			FieldPos = FieldManager->ChasePlayer(PlayerPos, NextPos);
		}
		break;
	}
	case EOperationType::CpuTypeD: {
		//--------------------------
		//赤マスを踏む確率が一番低い道を選ぶ
		//--------------------------
		FieldPos = FieldManager->SearchLowRiskRoute(NextPos);
		break;
	}
	case EOperationType::CpuTypeE: {
		//--------------------------
		//止まるマスが最大の利益になる道を選ぶ
		//--------------------------
		// ダイスウィジェットを取得、現在のダイスの出目を取得
		UDiceWidget* DiceWidget = GetDiceWidget();
		if (!IsValid(DiceWidget)) {
			return;
		}
		int32 Roll = DiceWidget->GetCurrentDiceRoll();
		FieldPos = FieldManager->SearchImmediateProfitRoute(Roll, NextPos);
		break;
	}
	case EOperationType::CpuTypeF: {
		//--------------------------
		//最大の利益になる道を選ぶ
		//--------------------------
		// ダイスウィジェットを取得、現在のダイスの出目を取得
		UDiceWidget* DiceWidget = GetDiceWidget();
		if (!IsValid(DiceWidget)) {
			return;
		}
		int32 Roll = DiceWidget->GetCurrentDiceRoll();
		FieldPos = FieldManager->SearchBestProfitRoute(Roll, NextPos);
		break;
	}
	}

	// 進むべき方向をセット
	for (auto Direction : NextPos) {

		if (FieldPos != Direction.Value) {
			continue;
		}
		switch (Direction.Key) {
		case EDirectionType::Front: {
			OnSelectFront();
			break;
		}
		case EDirectionType::Right: {
			OnSelectRight();
			break;
		}
		case EDirectionType::Left: {
			OnSelectLeft();
			break;
		}
		}
	}
}

/// <summary>
/// 次に進むマスを取得
/// </summary>
/// <param name=""></param>
/// <returns></returns>
int32 ABattleLevel::OnGetNextMovePos() {

	// 選択中の方向を取得
	EDirectionType Direction = FieldManager->GetSelectDirection();
	int32 FieldNum = FieldManager->GetNextMoveNumber(Direction);

	return FieldNum;
}

/// <summary>
/// サイコロウィジェットの更新
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnUpdateDiceWidget() {

	// ダイスウィジェットを取得
	UDiceWidget* Widget = GetDiceWidget();
	if (!IsValid(Widget)) {
		return;
	}
	// ダイスウィジェットの更新
	Widget->ReflectDiceWidget();
}

/// <summary>
/// プレイ用カメラをプレイヤーに追従させる
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnFollowPlayCamera() {

	SetupPlayCameraPosition();
}

/// <summary>
/// イベント処理終了
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnFinishEvent() {

	CurrentEventPhase = EEventPhaseType::Finish;
}

/// <summary>
/// 移動処理が終了したかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:終了, false:移動中 </returns>
bool ABattleLevel::OnIsFinishMove() {

	bool IsFinishMove = PlayerManager->IsFinishMove();
	if (IsFinishMove) {
		return true;
	}

	return false;
}

/// <summary>
/// イベント処理が終了しているかどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:終了, false:処理中 </returns>
bool ABattleLevel::OnIsFinishEvent() {

	if (CurrentEventPhase == EEventPhaseType::SearchEvent) {
		return true;
	}

	return false;
}

/// <summary>
/// 操作可能か取得
/// </summary>
/// <param name=""></param>
/// <returns> true:操作可能, false:操作不可 </returns>
bool ABattleLevel::OnCheckEnable() {

	ETurnPhase TurnPhase = BattleGameMode->GetCurrentPhase();
	switch (TurnPhase) {
	case ETurnPhase::WaitStart:
	case ETurnPhase::FinishBattle:
	case ETurnPhase::ShowResult: {
		//-----------------------------
		//初期化処理、バトル終了時
		//-----------------------------
		// 操作不可
		return false;
	}
	case ETurnPhase::WaitFinishResult: {
		//-----------------------------
		//リザルト処理時
		//-----------------------------
		// 操作可能
		break;
	}
	default: {
		//-----------------------------
		//ゲームプレイ中
		//-----------------------------
		// 現在プレイ中のプレイヤーを取得
		APlayerBase* Player = GetPlayablePlayer();
		if (!IsValid(Player)) {
			return false;
		}
		EOperationType Type = Player->GetOperationType();
		// 操作キャラがCPUの場合は処理終了
		if (Type != EOperationType::Player) {
			return false;
		}
		break;
	}
	}

	return true;
}

/// <summary>
/// ゴールに到達したかチェック
/// </summary>
/// <param name=""></param>
/// <returns> true:ゴール, false:ゴール以外 </returns>
bool ABattleLevel::OnCheckArriveGoal() {

	// 現在プレイ中のプレイヤーを取得
	APlayerBase* Player = GetPlayablePlayer();
	if (!IsValid(Player)) {
		return false;
	}
	// プレイヤーが存在する座標を取得
	int32 FieldNum = Player->GetFieldNum();
	bool IsGoal = FieldManager->IsArriveGoal(FieldNum);
	if (IsGoal) {
		return true;
	}

	return false;
}

/// <summary>
/// ゲーム情報をリザルトへ渡す
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnPrepareResult() {

	// バトルウィジェットの取得
	UBattleWidget* BattleWidget = GetBattleWidget();
	if (!IsValid(BattleWidget)) {
		return;
	}
	// リザルトウィジェットの取得
	UResultWidget* ResultWidget = GetResultWidget();
	if (!IsValid(ResultWidget)) {
		return;
	}

	TArray<ERank> ArrayRank;
	ArrayRank.Add(ERank::First);
	ArrayRank.Add(ERank::Second);
	ArrayRank.Add(ERank::Third);
	ArrayRank.Add(ERank::Fourth);

	// 順位情報をリザルトへ渡す
	for (ERank Rank : ArrayRank) {
		// 指定のランクのプレイヤー識別を取得
		EPlayerOrder Order = BattleWidget->GetPlayerOrder(Rank);
		if (Order == EPlayerOrder::None) {
			continue;
		}
		// 指定プレイヤーを取得、所持金を取得
		APlayerBase* Player = PlayerManager->GetPlayer(Order);
		if (!IsValid(Player)) {
			return;
		}
		int32 Money = Player->GetPocketMoney();
		// リザルトウィジェットに渡す
		ERanking Ranking = static_cast<ERanking>(Rank);
		ResultWidget->SetupRankingData(Order, Ranking, Money);

		// アニメーション開始位置をセット
		switch (Order) {
		case EPlayerOrder::First: {
			ResultWidget->AddAnimationArray(EAnimationPhase::AnimFirst);
			break;
		}
		case EPlayerOrder::Second: {
			ResultWidget->AddAnimationArray(EAnimationPhase::AnimSecond);
			break;
		}
		case EPlayerOrder::Third: {
			ResultWidget->AddAnimationArray(EAnimationPhase::AnimThird);
			break;
		}
		case EPlayerOrder::Fourth: {
			ResultWidget->AddAnimationArray(EAnimationPhase::AnimFourth);
			break;
		}
		}
	}
}

/// <summary>
/// フィニッシュ表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnShowFinish() {

	// バトルウィジェットの取得
	UBattleWidget* BattleWidget = GetBattleWidget();
	if (!IsValid(BattleWidget)) {
		return;
	}
	//ターンパネルを非表示にする
	BattleWidget->PlayOrderFadeOutAnimation();
	// フィニッシュ表示
	BattleWidget->ShowFinishWidget();

	// サウンドマネージャを取得
	ASoundManager* SoundManager = GetSoundManager();
	if (!IsValid(SoundManager)) {
		return;
	}
	SoundManager->FadeOut();
}

/// <summary>
/// リザルト表示
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnShowResult() {

	// リザルトウィジェットの取得
	UResultWidget* ResultWidget = GetResultWidget();
	if (!IsValid(ResultWidget)) {
		return;
	}
	// リザルトのフェーズを「準備」に変更
	ResultWidget->ChangeCurrentPhase(EResultPhaseType::Prepare);

	// サウンドマネージャを取得
	ASoundManager* SoundManager = GetSoundManager();
	if (!IsValid(SoundManager)) {
		return;
	}
	SoundManager->SetBgmSound(EPlaySoundType::ResultBgm);
	SoundManager->PlayBgmSound();
}

/// <summary>
/// ゲーム終了処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleLevel::OnEndGame() {

	UE_LOG(LogTemp, Display, TEXT("ABattleLevel OnEndGame"));

	// 現在のフェーズを「レベル変更準備」に変更
	ChangeCurrentPhase(BattlePhaseType::ChangeLevelPreparing);
}