﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "Battle/BattleGameMode.h"
#include "Battle/BattleUiManager.h"
#include "Battle/BattleController.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
ABattleGameMode::ABattleGameMode()
	: CurrentPhase(ETurnPhase::WaitStart)
	, AccumulateTime(0.0f) {

	PrimaryActorTick.bCanEverTick = true;

	HUDClass = ABattleUiManager::StaticClass();
	PlayerControllerClass = ABattleController::StaticClass();
	DefaultPawnClass = nullptr;

	UE_LOG(LogTemp, Display, TEXT("ABattleGameMode Constructor"));
}

/// <summary>
/// 実行開始イベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void ABattleGameMode::BeginPlay() {

	// 現在のフェーズを「ゲーム開始待機」にセット
	CurrentPhase = ETurnPhase::WaitStart;
	// 累積時間をリセット
	AccumulateTime = ResetTime;
}

/// <summary>
/// 毎フレームの処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void ABattleGameMode::Tick(float DeltaTime) {

	switch (CurrentPhase) {
	case ETurnPhase::WaitStart: {
		//---------------------------
		// ゲーム開始待機フェーズ
		//---------------------------
		break;
	}
	case ETurnPhase::StartTurn: {
		//---------------------------
		// ターン開始フェーズ
		//---------------------------
		AccumulateTime += DeltaTime;
		if (AccumulateTime < TurnStartWaitTime) {
			return;
		}
		AccumulateTime = ResetTime;

		// ターン開始処理実行
		EventInterface->OnStartTurn();
		// 現在のフェーズを「サイコロを振る」フェーズに変更
		ChangeCurrentPhase(ETurnPhase::RollDice);
		break;
	}
	case ETurnPhase::RollDice: {
		//---------------------------
		// サイコロを振るフェーズ
		//---------------------------
		// ダイス処理が終了しているかどうか確認
		bool IsFinish = EventInterface->OnIsFinishDice();
		if (!IsFinish) {
			return;
		}
		// プレイヤーのフェーズを変更
		EventInterface->OnChangePlayerPhaseMove();
		// 現在のフェーズを「プレイヤー移動」フェーズに変更
		ChangeCurrentPhase(ETurnPhase::MovePlayer);
		break;
	}
	case ETurnPhase::MovePlayer: {
		//---------------------------
		// プレイヤー移動フェーズ
		//---------------------------
		// 移動処理が終了しているかどうか確認
		bool IsFinish = EventInterface->OnIsFinishMove();
		if (!IsFinish) {
			return;
		}
		// 現在のフェーズを「目標マス到達イベント」フェーズに変更
		ChangeCurrentPhase(ETurnPhase::OccurEvent);
		break;
	}
	case ETurnPhase::SelectRoute: {
		//---------------------------
		// 分岐選択フェーズ
		//---------------------------
		break;
	}
	case ETurnPhase::DecideRoute: {
		//---------------------------
		// 分岐決定フェーズ
		//---------------------------
		// 現在のフェーズを「プレイヤー移動」フェーズに変更
		ChangeCurrentPhase(ETurnPhase::MovePlayer);
		break;
	}
	case ETurnPhase::OccurEvent: {
		//---------------------------
		// イベント発生フェーズ
		//---------------------------
		// イベント発生フェーズ
		bool IsFinish = EventInterface->OnOccurEvent(DeltaTime);
		if (!IsFinish) {
			break;
		}
		// 現在のフェーズを「イベント発生後」フェーズに変更
		ChangeCurrentPhase(ETurnPhase::OccuredEvent);
		break;
	}
	case ETurnPhase::OccuredEvent: {
		//---------------------------
		// イベント発生後フェーズ
		//---------------------------
		bool IsFinish = EventInterface->OnIsFinishEvent();
		if (!IsFinish) {
			break;
		}
		// 現在のフェーズを「ターン終了」フェーズに変更
		ChangeCurrentPhase(ETurnPhase::FinishTurn);
		break;
	}
	case ETurnPhase::FinishTurn: {
		//---------------------------
		// ターン終了フェーズ
		//---------------------------
		// プレイヤーの切り替え
		bool IsFinish = EventInterface->OnSwitchPlayer();
		if (IsFinish) {
			// 現在のフェーズを「バトル終了」フェーズに変更
			ChangeCurrentPhase(ETurnPhase::FinishBattle);
			break;
		}
		// 現在のフェーズを「プレイヤー切り替え」フェーズに変更
		ChangeCurrentPhase(ETurnPhase::SwitchNextPlayer);
		break;
	}
	case ETurnPhase::SwitchNextPlayer: {
		//---------------------------
		// プレイヤー切り替えフェーズ
		//---------------------------
		// 各オブジェクトの切り替え
		EventInterface->OnSwitchObject();
		// 現在のフェーズを「ターン開始」フェーズに変更
		ChangeCurrentPhase(ETurnPhase::StartTurn);
		break;
	}
	case ETurnPhase::FinishBattle: {
		//---------------------------
		// バトル終了フェーズ
		//---------------------------
		AccumulateTime += DeltaTime;
		if (AccumulateTime < FinishBattleWaitTime) {
			return;
		}
		AccumulateTime = ResetTime;

		// ゲーム終了を通知
		EventInterface->OnShowFinish();

		// 現在のフェーズを「リザルト準備」フェーズに変更
		ChangeCurrentPhase(ETurnPhase::PrepareResult);
		break;
	}
	case ETurnPhase::PrepareResult: {
		//---------------------------
		// リザルト準備フェーズ
		//---------------------------
		// リザルトウィジェットにゲーム情報を渡す処理
		EventInterface->OnPrepareResult();

		// 現在のフェーズを「リザルト表示」フェーズに変更
		ChangeCurrentPhase(ETurnPhase::ShowResult);
		break;
	}
	case ETurnPhase::ShowResult: {
		//---------------------------
		// リザルト表示フェーズ
		//---------------------------
		AccumulateTime += DeltaTime;
		if (AccumulateTime < ResultWaitTime) {
			return;
		}
		AccumulateTime = ResetTime;

		// リザルトを表示する
		EventInterface->OnShowResult();

		// 現在のフェーズを「リザルト終了待機」フェーズに変更
		ChangeCurrentPhase(ETurnPhase::WaitFinishResult);
		break;
	}
	default: {
		break;
	}
	}
}

/// <summary>
/// 現在のフェーズを変更する
/// </summary>
/// <param name="PhaseType"> フェーズの種類 </param>
/// <returns></returns>
void ABattleGameMode::ChangeCurrentPhase(ETurnPhase PhaseType) {

	CurrentPhase = PhaseType;
}

/// <summary>
/// 現在のフェーズを取得
/// </summary>
/// <param name=""></param>
/// <returns> フェーズの種類 </returns>
ETurnPhase ABattleGameMode::GetCurrentPhase() {

	return CurrentPhase;
}

/// <summary>
/// 現在のフェーズが「サイコロを振る」かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:サイコロを振る, false:それ以外 </returns>
bool ABattleGameMode::IsRollDice() {

	return CurrentPhase == ETurnPhase::RollDice;
}

/// <summary>
/// 現在のフェーズが「分岐選択中」かどうか
/// </summary>
/// <param name=""></param>
/// <returns> true:分岐選択中, false:それ以外 </returns>
bool ABattleGameMode::IsSelectRoute() {

	return CurrentPhase == ETurnPhase::SelectRoute;
}

/// <summary>
/// イベントインターフェース
/// </summary>
/// <param name=""> イベントインターフェース </param>
/// <returns></returns>
void ABattleGameMode::SetEventInterface(const TScriptInterface<IBattleEvent>& Interface) {

	EventInterface = Interface;
}
