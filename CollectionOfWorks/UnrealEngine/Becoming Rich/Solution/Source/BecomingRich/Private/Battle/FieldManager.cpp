﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/FieldManager.h"
#include <fstream>
#include <sstream>


// Sets default values
AFieldManager::AFieldManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void AFieldManager::BeginPlay()
{
	Super::BeginPlay();
	
	// 次に進むマスの初期設定
	NextMovePos.Add(EDirectionType::Front, FieldNotExists);
	NextMovePos.Add(EDirectionType::Left, FieldNotExists);
	NextMovePos.Add(EDirectionType::Right, FieldNotExists);

	// フィールドマップ情報の読み込み
	FString ProjectPath = FPaths::ProjectDir();
	FString FilePath = ProjectPath + FieldMapCsvFilePath;

	FieldMap = ReadCsvFieldMapFile(FilePath);
	if (FieldMap.Num() == Zero) {
		UE_LOG(LogTemp, Display, TEXT("AFieldManager ReadCsvFieldMapFile Error"));
		return;
	}

	// フィールド順番の読み込み
	FilePath.Empty();
	FilePath = ProjectPath + FieldOrderCsvFilePath;

	FieldOrder = ReadCsvFieldMapFile(FilePath);
	if (FieldOrder.Num() == Zero) {
		UE_LOG(LogTemp, Display, TEXT("AFieldManager ReadCsvFieldOrderFile Error"));
		return;
	}

	// フィールドデータの読み込み
	FilePath.Empty();
	FilePath = ProjectPath + FieldDataCsvFilePath;

	FieldData = ReadCsvFieldDataFile(FilePath);
	if (FieldData.Num() == Zero) {
		UE_LOG(LogTemp, Display, TEXT("AFieldManager ReadCsvFieldDataFile Error"));
		return;
	}

	// フィールドの生成
	CreateField();
	// ウィジェットマテリアルの設定
	SetupWidgetMaterial();
}

// Called every frame
void AFieldManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

/// <summary>
/// フィールドCSV読み込み処理
/// </summary>
/// <param name="FilePath"> ファイルパス </param>
/// <returns> フィールド配列 </returns>
TMap<int32, TArray<int32>> AFieldManager::ReadCsvFieldMapFile(FString FilePath) {

	UE_LOG(LogTemp, Display, TEXT("AFieldManager ReadCsvFieldMapFile"));

	TMap<int32, TArray<int32>> FileData;
	FileData.Empty();

	// CSVファイルからデータを取得
	FString CsvDataFile;
	FFileHelper::LoadFileToString(CsvDataFile, *FilePath);
	if (CsvDataFile.IsEmpty()) {
		return FileData;
	}

	// 各行で分割した値の保持
	TArray<FString> Row;
	// カンマ区切りごとの値
	TArray<FString> Element;
	TArray<int32> IntElement;

	CsvDataFile.ParseIntoArrayLines(Row);

	for (int i = 0; i < Row.Num(); ++i) {
		// カンマ区切りで取得
		Row[i].ParseIntoArray(Element, TEXT(","), true);
		// 取得した値を文字列から数値に変換
		for (int j = 0; j < Element.Num(); ++j) {
			IntElement.Add(FCString::Atoi(*Element[j]));
		}
		// 配列に格納
		FileData.Add(i, IntElement);
		// 一時配列の初期化
		Element.Empty();
		IntElement.Empty();
	}

	return FileData;
}

/// <summary>
/// フィールドデータCSV読み込み処理
/// </summary>
/// <param name="FilePath"> ファイルパス </param>
/// <returns> フィールド配列 </returns>
TMap<int32, TArray<int32>> AFieldManager::ReadCsvFieldDataFile(FString FilePath) {

	UE_LOG(LogTemp, Display, TEXT("AFieldManager ReadCsvFieldDataFile"));

	TMap<int32, TArray<int32>> FileData;
	FileData.Empty();

	// CSVファイルからデータを取得
	FString CsvDataFile;
	FFileHelper::LoadFileToString(CsvDataFile, *FilePath);
	if (CsvDataFile.IsEmpty()) {
		return FileData;
	}

	// 各行で分割した値の保持
	TArray<FString> Row;
	// カンマ区切りごとの値
	TArray<FString> Element;
	int32 IntElementKey;
	TArray<int32> IntElementValue;

	CsvDataFile.ParseIntoArrayLines(Row);

	for (int i = 1; i < Row.Num(); ++i) {
		// カンマ区切りで取得
		Row[i].ParseIntoArray(Element, TEXT(","), true);
		// 取得した値を文字列から数値に変換
		for (int j = 0; j < Element.Num(); ++j) {

			if (j == MinIndex) {
				// 先頭列はキー値として取得
				IntElementKey = FCString::Atoi(*Element[j]);
			}
			else {
				// 先頭以外
				IntElementValue.Add(FCString::Atoi(*Element[j]));
			}
		}
		// 配列に格納
		FileData.Add(IntElementKey, IntElementValue);
		// 一時配列の初期化
		Element.Empty();
		IntElementValue.Empty();
	}

	return FileData;
}

/// <summary>
/// フィールド生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AFieldManager::CreateField() {

	UE_LOG(LogTemp, Display, TEXT("AFieldManager CreateField"));

	// アクタクラス取得
	TSubclassOf<class AFieldBase> FieldActor = TSoftClassPtr<AFieldBase>(FSoftObjectPath(*FieldClassPath)).LoadSynchronous();
	if (!IsValid(FieldActor)) {
		return;
	}

	FVector FieldPosition;
	int32 RowNo = 0;

	for (auto RowData : FieldMap) {

		for (int i = 0; i < static_cast<int>(RowData.Value.Num()); ++i) {
			
			// フィールドを生成しない箇所の場合は処理しない
			if (RowData.Value[i] == FieldNotExists) {
				continue;
			}
			
			// フィールド番号の先頭1桁を取得
			int FieldIndex = CalcFirstNumber(RowData.Value[i]);
			if (FieldIndex < static_cast<int>(EFieldType::Normal) || 
				FieldIndex > static_cast<int>(EFieldType::Goal)) {
				UE_LOG(LogTemp, Display, TEXT("AFieldManager FieldIndex Error"));
				return;
			}
			EFieldType FieldType = static_cast<EFieldType>(FieldIndex);

			// 配置位置の確定
			FieldPosition.X = FieldPosX + i * FieldSizeWidth + FieldSizeWidth / Half;
			FieldPosition.Y = FieldPosY + RowData.Key * FieldSizeHeight + FieldSizeHeight / Half;
			FieldPosition.Z = FieldPosZ;

			// フィールドの生成
			AFieldBase* Field = GetWorld()->SpawnActor<AFieldBase>(FieldActor);
			// 配置座標をセット、初期化処理実行
			Field->SetActorLocation(FieldPosition);
			Field->Initialize(FieldType);
			Field->SetFieldOrder(FieldOrder[RowNo][i]);
			Field->SetFieldUniOrder(RowData.Value[i]);

			if (FieldType == EFieldType::Start || FieldType == EFieldType::Goal) {
				FVector Scale = FVector(ScaleX, ScaleY, ScaleZ);
				FTransform ActorTrans;
				ActorTrans.SetLocation(FieldPosition);
				ActorTrans.SetScale3D(Scale);
				Field->SetActorTransform(ActorTrans);
			}

			// 配列にセット
			FieldDataDetail.Add(RowData.Value[i], Field);
		}

		// 行番号を加算
		++RowNo;
	}

	UE_LOG(LogTemp, Display, TEXT("AFieldManager CreateField Success!"));
}

/// <summary>
/// ウィジェットマテリアルの設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AFieldManager::SetupWidgetMaterial() {

	for (auto Field : FieldDataDetail) {

		FString MaterialPath01;
		FString MaterialPath02;

		if (!IsValid(Field.Value)) {
			continue;
		}

		EFieldType FieldType = Field.Value->GetFieldType();
		switch (FieldType) {
		case EFieldType::Start: 
		case EFieldType::Goal: {
			//------------------------
			//スタート・ゴールマス
			//------------------------
			// 表示用ウィジェットの設定
			Field.Value->SetupInfoWidgetClass();
			break;
		}
		case EFieldType::Plus:
		case EFieldType::Minus: {
			//------------------------
			//金額獲得・損失マス
			//------------------------
			int32 Order = Field.Value->GetFieldUniOrder();
			int32 ColEvent = static_cast<int32>(EEventColumn::Detail);
			if (!FieldData.Contains(Order)) {
				continue;
			}
			int32 MoneyValue = FieldData[Order][ColEvent];
			// 金額表示用ウィジェットの設定
			Field.Value->SetupMoneyWidgetClass(MoneyValue);
			break;
		}
		case EFieldType::Move: {
			//------------------------
			//強制移動マス
			//------------------------
			// フィールド番号を取得し、
			// 移動先のフィールド番号を調べる
			int32 Order = Field.Value->GetFieldUniOrder();
			int32 ColEvent = static_cast<int32>(EEventColumn::Detail);
			if (!FieldData.Contains(Order)) {
				continue;
			}
			TArray<int32> MovedOrderData = FieldData[Order];
			int32 MovedOrder = MovedOrderData[ColEvent];
			AFieldBase* MovedField = FieldDataDetail[MovedOrder];

			if (Order == ForcedMoveOrder01) {
				MaterialPath01 = MagicCircle01Path;
				MaterialPath02 = MagicCircle02Path;
			}
			else if (Order == ForcedMoveOrder02 || Order == ForcedMoveOrder03) {
				MaterialPath01 = MagicCircle03Path;
				MaterialPath02 = MagicCircle04Path;
			}

			// 移動先のウィジェットマテリアルを設定
			MovedField->SetupWidgetMaterial(MaterialPath02);
			// マテリアルをロードして設定
			Field.Value->SetupWidgetMaterial(MaterialPath01);
			break;
		}
		default: {
			//------------------------
			//その他
			//------------------------
			continue;
		}
		}
	}
}

/// <summary>
/// 数値の先頭1桁を取得
/// </summary>
/// <param name="Number"> 対象の数値 </param>
/// <returns> 数値の先頭1桁 </returns>
int AFieldManager::CalcFirstNumber(int32 Number) {

	int32 Num = std::abs(Number);
	int32 Degit = Zero;

	while (Num >= Ten) {
		Num /= Ten;
		++Degit;
	}

	return Num;
}

/// <summary>
/// フィールドの表示座標取得
/// </summary>
/// <param name="FieldNum"> フィールド番号 </param>
/// <returns> 表示座標 </returns>
FVector AFieldManager::GetFieldPosition(int32 FieldNum) {

	FVector Position;

	// リスト内に存在するか確認
	// 存在しない場合はスタート地点の座標を返す
	if (!FieldDataDetail.Contains(FieldNum)) {
		return FieldDataDetail[StartPos]->GetActorLocation();
	}

	Position = FieldDataDetail[FieldNum]->GetActorLocation();

	return Position;
}

/// <summary>
/// ゴールかどうかの確認
/// </summary>
/// <param name="FieldNum"> フィールド番号 </param>
/// <returns> true:ゴール, false:ゴール以外 </returns>
bool AFieldManager::IsArriveGoal(int32 FieldNum) {

	// 存在しない番号なら処理終了
	if (!FieldDataDetail.Contains(FieldNum)) {
		return false;
	}
	AFieldBase* Field = FieldDataDetail[FieldNum];
	if (!IsValid(Field)) {
		return false;
	}
	EFieldType FieldType = Field->GetFieldType();

	if (FieldType == EFieldType::Goal) {
		return true;
	}

	return false;
}

/// <summary>
/// 進むことができるマスの取得
/// </summary>
/// <param name="FieldNum"> フィールド番号 </param>
/// <returns> フィールド番号の配列 </returns>
TMap<EDirectionType, int32> AFieldManager::SearchCanForward(int32 FieldNum) {

	TArray<int32> ArrayNextPos;

	// 初期化しておく
	NextMovePos[EDirectionType::Front] = FieldNotExists;
	NextMovePos[EDirectionType::Left] = FieldNotExists;
	NextMovePos[EDirectionType::Right] = FieldNotExists;

	// 経路探索処理
	NextMovePos = SearchRoute(FieldNum);

	return NextMovePos;
}

/// <summary>
/// 経路探索
/// </summary>
/// <param name="FieldNum"> フィールド番号 </param>
/// <returns> 移動可能マス </returns>
TMap<EDirectionType, int32> AFieldManager::SearchRoute(int32 FieldNum) {

	TMap<EDirectionType, int32> ArrayNextPos;
	int32 RowNo = 0;
	int32 ColNo = 0;
	bool IsFinish = false;

	// 配列の初期化
	ArrayNextPos.Add(EDirectionType::Front, FieldNotExists);
	ArrayNextPos.Add(EDirectionType::Left, FieldNotExists);
	ArrayNextPos.Add(EDirectionType::Right, FieldNotExists);

	// フィールド番号から配列位置を取得
	for (auto RowData : FieldMap) {
		for (int i = 0; i < static_cast<int>(RowData.Value.Num()); ++i) {
			if (FieldNum == RowData.Value[i]) {
				ColNo = i;
				IsFinish = true;
				break;
			}
		}
		if (IsFinish) {
			break;
		}
		// 行番号を加算
		++RowNo;
	}

	// 現在のマスの順番
	int32 CurrentOrder = FieldOrder[RowNo][ColNo];
	// 前左右のマスを取得
	int32 FrontNum = RowNo - One;
	int32 LeftNum = ColNo - One;
	int32 RightNum = ColNo + One;
	// 前のマス
	if (FrontNum >= MinIndex) {
		// 現在のマスの順番より大きいなら配列に格納
		if (CurrentOrder < FieldOrder[FrontNum][ColNo]) {
			ArrayNextPos[EDirectionType::Front] = FieldMap[FrontNum][ColNo];
		}
	}
	// 左のマス
	if (LeftNum >= MinIndex) {
		// 現在のマスの順番より大きいなら配列に格納
		if (CurrentOrder < FieldOrder[RowNo][LeftNum]) {
			ArrayNextPos[EDirectionType::Left] = FieldMap[RowNo][LeftNum];
		}
	}
	// 右のマス
	if (RightNum < FieldMap[MinIndex].Num()) {
		// 現在のマスの順番より大きいなら配列に格納
		if (CurrentOrder < FieldOrder[RowNo][RightNum]) {
			ArrayNextPos[EDirectionType::Right] = FieldMap[RowNo][RightNum];
		}
	}

	return ArrayNextPos;
}

/// <summary>
/// 最短・最長経路探索処理
/// </summary>
/// <param name="Type"> 探索種類 </param>
/// <param name="NextMove"> 次に進めるマス </param>
/// <returns></returns>
int32 AFieldManager::SearchShortAndLongRoute(ESearchType Type, TMap<EDirectionType, int32> NextMove) {

	// 方向配列  初期設定
	TArray<EDirectionType> ArrayDirection;
	ArrayDirection.Add(EDirectionType::Front);
	ArrayDirection.Add(EDirectionType::Left);
	ArrayDirection.Add(EDirectionType::Right);
	// 次に進めるマス配列を取得
	TMap<EDirectionType, int32> NextMovePosition;
	NextMovePosition = NextMove;
	// 方向毎のゴールまでの残りのマス数  初期設定
	TMap<EDirectionType, int32> RemainToGoal;
	RemainToGoal.Add(EDirectionType::Front, InitRemainToGoalNum);
	RemainToGoal.Add(EDirectionType::Left, InitRemainToGoalNum);
	RemainToGoal.Add(EDirectionType::Right, InitRemainToGoalNum);

	// ゴールまでのマス数を方向毎に求める
	for (auto Direction : ArrayDirection) {

		int32 Count = 0;
		TMap<EDirectionType, int32> Pos;
		Pos.Add(EDirectionType::Front, FieldNotExists);
		Pos.Add(EDirectionType::Left, FieldNotExists);
		Pos.Add(EDirectionType::Right, FieldNotExists);

		// 規定値以外の方向が設定されている場合
		if (!NextMovePosition.Contains(Direction)) {
			continue;
		}
		// 指定方向に進めない場合
		int32 FieldNum = NextMovePosition[Direction];
		if (FieldNum == FieldNotExists) {
			continue;
		}

		// ゴールのマスまで経路探索を行う
		bool IsFinish = false;
		int32 NextPosCount = 0;
		while (!IsFinish) {
			// カウントをリセット
			NextPosCount = 0;
			// 経路探索
			Pos = SearchRoute(FieldNum);

			for (auto SubDirection : ArrayDirection) {
				// 指定方向に進めない場合
				int Number = Pos[SubDirection];
				if (Number == FieldNotExists) {
					continue;
				}
				++NextPosCount;
				FieldNum = Pos[SubDirection];
			}
			
			if (NextPosCount == Zero) {
				// これ以上進めない場合は処理終了
				break;
			}
			else if (NextPosCount > One) {
				// 移動可能マスが複数ある場合
				int32 TempFieldNum = SearchShortAndLongRoute(Type, Pos);
				FieldNum = TempFieldNum;
			}
			// ループ回数を加算
			++RemainToGoal[Direction];

			// 対象のマスがゴールかどうかチェック
			// ゴールだった場合、フラグを立て、ループを抜ける
			AFieldBase* Field = FieldDataDetail[FieldNum];
			if (!IsValid(Field)) {
				break;
			}
			EFieldType FieldType = Field->GetFieldType();
			if (FieldType == EFieldType::Goal) {
				IsFinish = true;
			}
		}
		// ゴールに辿り着けない場合はカウントリセット
		if (!IsFinish) {
			RemainToGoal[Direction] = InitRemainToGoalNum;
			continue;
		}
	}

	UE_LOG(LogTemp, Display, TEXT("RemainToGoal Front:%d"), static_cast<int32>(RemainToGoal[EDirectionType::Front]));
	UE_LOG(LogTemp, Display, TEXT("RemainToGoal Left:%d"), static_cast<int32>(RemainToGoal[EDirectionType::Left]));
	UE_LOG(LogTemp, Display, TEXT("RemainToGoal Right:%d"), static_cast<int32>(RemainToGoal[EDirectionType::Right]));

	int32 TempNumber = 0;
	EDirectionType RouteDirection = EDirectionType::Front;
	for (auto Direction : ArrayDirection) {

		// ゴールまでの最大値を取得
		if (RemainToGoal[Direction] == InitRemainToGoalNum) {
			continue;
		}

		switch (Type) {
		case ESearchType::LongRoute: {
			if (TempNumber < RemainToGoal[Direction]) {
				TempNumber = RemainToGoal[Direction];
				RouteDirection = Direction;
			}
			break;
		}
		case ESearchType::ShortRoute: {
			// ゴールまでの最小値を取得
			if (TempNumber > RemainToGoal[Direction]) {
				TempNumber = RemainToGoal[Direction];
				RouteDirection = Direction;
			}
			break;
		}
		}
	}

	// 最短・最長のマスを取得
	int32 Number;
	Number = NextMovePosition[RouteDirection];

	UE_LOG(LogTemp, Display, TEXT("RouteDirection:%d"), static_cast<int32>(RouteDirection));
	UE_LOG(LogTemp, Display, TEXT("Front:%d"), static_cast<int32>(NextMovePosition[EDirectionType::Front]));
	UE_LOG(LogTemp, Display, TEXT("Left:%d"), static_cast<int32>(NextMovePosition[EDirectionType::Left]));
	UE_LOG(LogTemp, Display, TEXT("Right:%d"), static_cast<int32>(NextMovePosition[EDirectionType::Right]));

	return Number;
}

/// <summary>
/// 脇道にプレイヤーがいる時に追跡する
/// </summary>
/// <param name="AimPos"> 目標のマス </param>
/// <param name="NextMove"> 次に進めるマス </param>
/// <returns> フィールドのマス </returns>
int32 AFieldManager::ChasePlayer(int32 AimPos, TMap<EDirectionType, int32> NextMove) {

	// 方向配列  初期設定
	TArray<EDirectionType> ArrayDirection;
	ArrayDirection.Add(EDirectionType::Front);
	ArrayDirection.Add(EDirectionType::Left);
	ArrayDirection.Add(EDirectionType::Right);

	// 次に進めるマス配列を取得
	TMap<EDirectionType, int32> NextMovePosition;
	NextMovePosition = NextMove;

	int32 FieldNum = FieldNotExists;
	bool IsFinish = false;
	EDirectionType MoveDirection = EDirectionType::Front;

	// ゴールまでのマス数を方向毎に求める
	for (auto Direction : ArrayDirection) {

		TMap<EDirectionType, int32> Pos;
		Pos.Add(EDirectionType::Front, FieldNotExists);
		Pos.Add(EDirectionType::Left, FieldNotExists);
		Pos.Add(EDirectionType::Right, FieldNotExists);

		// 規定値以外の方向が設定されている場合
		if (!NextMovePosition.Contains(Direction)) {
			continue;
		}
		// 指定方向に進めない場合
		FieldNum = NextMovePosition[Direction];
		if (FieldNum == FieldNotExists) {
			continue;
		}

		// ゴールのマスまで経路探索を行う
		int32 NextPosCount = 0;
		while (!IsFinish) {

			// 目標となるプレイヤーに到達したら
			// フラグを立てて、ループを抜ける
			if (FieldNum == AimPos) {
				IsFinish = true;
				MoveDirection = Direction;
				break;
			}

			// カウントをリセット
			NextPosCount = 0;
			// 経路探索
			Pos = SearchRoute(FieldNum);

			for (auto SubDirection : ArrayDirection) {
				// 指定方向に進めない場合
				int32 Number = Pos[SubDirection];
				if (Number == FieldNotExists) {
					continue;
				}
				++NextPosCount;
				FieldNum = Pos[SubDirection];
			}

			if (NextPosCount == Zero) {
				// これ以上進めない場合は処理終了
				break;
			}
			else if (NextPosCount > One) {
				// 移動可能マスが複数ある場合
				FieldNum = ChasePlayer(AimPos, Pos);
				UE_LOG(LogTemp, Display, TEXT("ChasePlayer MultiDecide FieldNum : %d"), static_cast<int32>(MoveDirection));
			}

			// 対象のマスがゴールかどうかチェック
			// ゴールだった場合、ループを抜ける
			AFieldBase* Field = FieldDataDetail[FieldNum];
			if (!IsValid(Field)) {
				break;
			}
			EFieldType FieldType = Field->GetFieldType();
			if (FieldType == EFieldType::Goal) {
				break;
			}
		}
	}

	// 対象プレイヤーが自身より先にいない又は
	// ゴール済みの場合は進行可能な方向を選択する
	if (!IsFinish) {
		for (EDirectionType Direction : ArrayDirection) {
			if (NextMovePosition[MoveDirection] == FieldNotExists) {
				continue;
			}
			MoveDirection = Direction;
			break;
		}
	}

	UE_LOG(LogTemp, Display, TEXT("ChasePlayer Direction : %d"), static_cast<int32>(MoveDirection));
	UE_LOG(LogTemp, Display, TEXT("ChasePlayer NextMovePosition Front : %d"), static_cast<int32>(NextMovePosition[EDirectionType::Front]));
	UE_LOG(LogTemp, Display, TEXT("ChasePlayer NextMovePosition Left  : %d"), static_cast<int32>(NextMovePosition[EDirectionType::Left]));
	UE_LOG(LogTemp, Display, TEXT("ChasePlayer NextMovePosition Right : %d"), static_cast<int32>(NextMovePosition[EDirectionType::Right]));

	// 進むマスをセット
	int32 Number;
	Number = NextMovePosition[MoveDirection];

	return Number;
}

/// <summary>
/// 赤パネルを踏む確率が少ない道を選ぶ
/// </summary>
/// <param name="NextMove"> 次に進めるマス </param>
/// <returns> フィールドのマス </returns>
int32 AFieldManager::SearchLowRiskRoute(TMap<EDirectionType, int32> NextMove) {

	// 方向配列  初期設定
	TArray<EDirectionType> ArrayDirection;
	ArrayDirection.Add(EDirectionType::Front);
	ArrayDirection.Add(EDirectionType::Left);
	ArrayDirection.Add(EDirectionType::Right);
	// 次に進めるマス配列を取得
	TMap<EDirectionType, int32> NextMovePosition;
	NextMovePosition = NextMove;

	// 赤マスと総マス数
	TArray<int32> InitArray;
	InitArray.Add(Zero);   // 総マス数
	InitArray.Add(Zero);   // 赤マス数
	// 方向毎の赤マスと総マス数
	TMap<EDirectionType, TArray<int32>> FieldNumToGoal;
	FieldNumToGoal.Add(EDirectionType::Front, InitArray);
	FieldNumToGoal.Add(EDirectionType::Left, InitArray);
	FieldNumToGoal.Add(EDirectionType::Right, InitArray);

	// ゴールまでのマス数を方向毎に求める
	for (auto Direction : ArrayDirection) {

		TMap<EDirectionType, int32> Pos;
		Pos.Add(EDirectionType::Front, FieldNotExists);
		Pos.Add(EDirectionType::Left, FieldNotExists);
		Pos.Add(EDirectionType::Right, FieldNotExists);

		// 規定値以外の方向が設定されている場合
		if (!NextMovePosition.Contains(Direction)) {
			continue;
		}
		// 指定方向に進めない場合
		int32 FieldNum = NextMovePosition[Direction];
		if (FieldNum == FieldNotExists) {
			continue;
		}

		// ゴールのマスまで経路探索を行う
		bool IsFinish = false;
		int32 NextPosCount = 0;
		while (!IsFinish) {

			// ループ回数を加算
			++FieldNumToGoal[Direction][AllFieldIndex];

			// 対象のマスがゴールかどうかチェック
			// 赤マスの場合は配列内の数値を加算
			// ゴールだった場合、フラグを立て、ループを抜ける
			AFieldBase* Field = FieldDataDetail[FieldNum];
			if (!IsValid(Field)) {
				break;
			}
			EFieldType FieldType = Field->GetFieldType();
			switch (FieldType) {
			case EFieldType::Minus: {
				// 金額減少マス
				++FieldNumToGoal[Direction][RedFieldIndex];
				break;
			}
			case EFieldType::Goal: {
				// ゴールマス
				IsFinish = true;
				break;
			}
			}

			// カウントをリセット
			NextPosCount = 0;
			// 経路探索
			Pos = SearchRoute(FieldNum);

			for (auto SubDirection : ArrayDirection) {
				// 指定方向に進めない場合
				int Number = Pos[SubDirection];
				if (Number == FieldNotExists) {
					continue;
				}
				++NextPosCount;
				FieldNum = Pos[SubDirection];
			}

			if (NextPosCount == Zero) {
				// これ以上進めない場合は処理終了
				break;
			}
			else if (NextPosCount > One) {
				// 移動可能マスが複数ある場合
				int32 TempFieldNum = SearchLowRiskRoute(Pos);
				FieldNum = TempFieldNum;
			}
		}
		// ゴールに辿り着けない場合はカウントリセット
		if (!IsFinish) {
			FieldNumToGoal[Direction][AllFieldIndex] = InitRemainToGoalNum;
			FieldNumToGoal[Direction][RedFieldIndex] = InitRemainToGoalNum;
			continue;
		}
	}

	float TempNumber = HundredPercent;
	EDirectionType RouteDirection = EDirectionType::Front;
	for (auto Direction : ArrayDirection) {

		// ゴールに到達できない場合は処理しない
		if (FieldNumToGoal[Direction][AllFieldIndex] == InitRemainToGoalNum || 
			FieldNumToGoal[Direction][AllFieldIndex] == Zero) {
			continue;
		}

		// 赤マス数と総マス数から赤マスを踏む確率を取得
		float Probability = static_cast<float>(FieldNumToGoal[Direction][RedFieldIndex]) / static_cast<float>(FieldNumToGoal[Direction][AllFieldIndex]);
		// 前回算出した確率より低い場合は変数にセット
		if (TempNumber > Probability) {
			TempNumber = Probability;
			RouteDirection = Direction;
		}
	}

	UE_LOG(LogTemp, Display, TEXT("LowRisk Direction : %d"), static_cast<int32>(RouteDirection));
	UE_LOG(LogTemp, Display, TEXT("LowRisk FieldNumToGoal All Front : %d"), FieldNumToGoal[EDirectionType::Front][AllFieldIndex]);
	UE_LOG(LogTemp, Display, TEXT("LowRisk FieldNumToGoal Red Front : %d"), FieldNumToGoal[EDirectionType::Front][RedFieldIndex]);
	UE_LOG(LogTemp, Display, TEXT("LowRisk FieldNumToGoal All Left  : %d"), FieldNumToGoal[EDirectionType::Left][AllFieldIndex]);
	UE_LOG(LogTemp, Display, TEXT("LowRisk FieldNumToGoal Red Left  : %d"), FieldNumToGoal[EDirectionType::Left][RedFieldIndex]);
	UE_LOG(LogTemp, Display, TEXT("LowRisk FieldNumToGoal All Right : %d"), FieldNumToGoal[EDirectionType::Right][AllFieldIndex]);
	UE_LOG(LogTemp, Display, TEXT("LowRisk FieldNumToGoal Red Right : %d"), FieldNumToGoal[EDirectionType::Right][RedFieldIndex]);

	// 進むマスをセット
	int32 Number;
	Number = NextMovePosition[RouteDirection];

	return Number;
}

/// <summary>
/// 止まるマスが最大の利益になる道を選ぶ
/// </summary>
/// <param name="RemainNum"> 残りの進めるマス数 </param>
/// <param name="NextMove"> 次に進めるマス </param>
/// <returns> フィールドのマス </returns>
int32 AFieldManager::SearchImmediateProfitRoute(int32 RemainNum, TMap<EDirectionType, int32> NextMove) {

	// 方向配列  初期設定
	TArray<EDirectionType> ArrayDirection;
	ArrayDirection.Add(EDirectionType::Front);
	ArrayDirection.Add(EDirectionType::Left);
	ArrayDirection.Add(EDirectionType::Right);
	// 次に進めるマス配列を取得
	TMap<EDirectionType, int32> NextMovePosition;
	NextMovePosition = NextMove;

	// 方向毎の獲得金額
	TMap<EDirectionType, int32> GettingMoneyValue;
	GettingMoneyValue.Add(EDirectionType::Front, InitGetMoney);
	GettingMoneyValue.Add(EDirectionType::Left, InitGetMoney);
	GettingMoneyValue.Add(EDirectionType::Right, InitGetMoney);

	// ゴールまでのマス数を方向毎に求める
	for (auto Direction : ArrayDirection) {

		// 残りの進めるマス数
		int32 RemainTempNum = RemainNum;
		// 方向ごとの移動先マス
		TMap<EDirectionType, int32> Pos;
		Pos.Add(EDirectionType::Front, FieldNotExists);
		Pos.Add(EDirectionType::Left, FieldNotExists);
		Pos.Add(EDirectionType::Right, FieldNotExists);

		// 規定値以外の方向が設定されている場合
		if (!NextMovePosition.Contains(Direction)) {
			continue;
		}
		// 指定方向に進めない場合
		int32 FieldNum = NextMovePosition[Direction];
		if (FieldNum == FieldNotExists) {
			continue;
		}

		// ゴールまたは目標のマスまで経路探索を行う
		bool IsFinish = false;
		int32 NextPosCount = 0;
		while (!IsFinish) {

			// 残りの進めるマス数を減算
			--RemainTempNum;
			// 対象のマスがゴールかどうかチェック
			// ゴールだった場合、フラグを立て、ループを抜ける
			AFieldBase* Field = FieldDataDetail[FieldNum];
			if (!IsValid(Field)) {
				break;
			}
			EFieldType FieldType = Field->GetFieldType();
			if (FieldType == EFieldType::Goal) {
				IsFinish = true;
				break;
			}

			// 停止予定マスなら獲得金額を取得
			if (RemainTempNum == Zero) {

				TArray<int32> ArrayEvent = GetFieldEvent(FieldNum);
				switch (FieldType) {
				case EFieldType::Plus: {
					// 金額増加マス
					int32 ColEvent = static_cast<int32>(EEventColumn::Detail);
					int32 Money = ArrayEvent[ColEvent];
					GettingMoneyValue[Direction] = Money;
					break;
				}
				case EFieldType::Minus: {
					// 金額減少マス
					int32 ColEvent = static_cast<int32>(EEventColumn::Detail);
					int32 Money = ArrayEvent[ColEvent];
					GettingMoneyValue[Direction] = MinusSign * Money;
					break;
				}
				default: {
					// 金額増加・減少マス
					GettingMoneyValue[Direction] = Zero;
				}
				}
				IsFinish = true;
				break;
			}

			// カウントをリセット
			NextPosCount = 0;
			// 経路探索
			Pos = SearchRoute(FieldNum);

			for (auto SubDirection : ArrayDirection) {
				// 指定方向に進めない場合
				int Number = Pos[SubDirection];
				if (Number == FieldNotExists) {
					continue;
				}
				++NextPosCount;
				FieldNum = Pos[SubDirection];
			}

			if (NextPosCount == Zero) {
				// これ以上進めない場合は処理終了
				break;
			}
			else if (NextPosCount > One) {
				// 移動可能マスが複数ある場合
				int32 TempFieldNum = SearchImmediateProfitRoute(RemainTempNum, Pos);
				FieldNum = TempFieldNum;
			}
		}
		// ゴールまたは目標座標に辿り着けない場合はカウントリセット
		if (!IsFinish) {
			continue;
		}
	}

	int32 TempMoney = InitGetMoney;
	EDirectionType RouteDirection = EDirectionType::Front;
	for (auto Direction : ArrayDirection) {

		// 金額が初期値の場合は処理しない
		if (GettingMoneyValue[Direction] == InitGetMoney) {
			continue;
		}
		// 獲得金額が前回保持した金額より多い場合、変数に格納
		if (TempMoney < GettingMoneyValue[Direction]) {
			TempMoney = GettingMoneyValue[Direction];
			RouteDirection = Direction;
		}
	}

	UE_LOG(LogTemp, Display, TEXT("ImmediateProfit Direction : %d"), static_cast<int32>(RouteDirection));
	UE_LOG(LogTemp, Display, TEXT("ImmediateProfit GettingMoneyValue Front : %d"), GettingMoneyValue[EDirectionType::Front]);
	UE_LOG(LogTemp, Display, TEXT("ImmediateProfit GettingMoneyValue Left  : %d"), GettingMoneyValue[EDirectionType::Left]);
	UE_LOG(LogTemp, Display, TEXT("ImmediateProfit GettingMoneyValue Right : %d"), GettingMoneyValue[EDirectionType::Right]);

	// 進むマスをセット
	int32 Number;
	Number = NextMovePosition[RouteDirection];

	return Number;
}

/// <summary>
/// 止まるマスが最大の利益になる道を選ぶ
/// 最大利益マスが複数ある場合は赤パネルを踏む確率が少ない道を選ぶ
/// </summary>
/// <param name="RemainNum"> 残りの進めるマス数 </param>
/// <param name="NextMove"> 次に進めるマス </param>
/// <returns> フィールドのマス </returns>
int32 AFieldManager::SearchBestProfitRoute(int32 RemainNum, TMap<EDirectionType, int32> NextMove) {

	// 方向配列  初期設定
	TArray<EDirectionType> ArrayDirection;
	ArrayDirection.Add(EDirectionType::Front);
	ArrayDirection.Add(EDirectionType::Left);
	ArrayDirection.Add(EDirectionType::Right);
	// 次に進めるマス配列を取得
	TMap<EDirectionType, int32> NextMovePosition;
	NextMovePosition = NextMove;

	// 赤マスと総マス数
	TArray<int32> InitArray;
	InitArray.Add(Zero);   // 総マス数
	InitArray.Add(Zero);   // 赤マス数
	// 方向毎の赤マスと総マス数
	TMap<EDirectionType, TArray<int32>> FieldNumToGoal;
	FieldNumToGoal.Add(EDirectionType::Front, InitArray);
	FieldNumToGoal.Add(EDirectionType::Left, InitArray);
	FieldNumToGoal.Add(EDirectionType::Right, InitArray);

	// 方向毎の獲得金額
	TMap<EDirectionType, int32> GettingMoneyValue;
	GettingMoneyValue.Add(EDirectionType::Front, InitGetMoney);
	GettingMoneyValue.Add(EDirectionType::Left, InitGetMoney);
	GettingMoneyValue.Add(EDirectionType::Right, InitGetMoney);

	// ゴールまでのマス数を方向毎に求める
	for (auto Direction : ArrayDirection) {

		// 残りの進めるマス数
		int32 RemainTempNum = RemainNum;
		TMap<EDirectionType, int32> Pos;
		Pos.Add(EDirectionType::Front, FieldNotExists);
		Pos.Add(EDirectionType::Left, FieldNotExists);
		Pos.Add(EDirectionType::Right, FieldNotExists);

		// 規定値以外の方向が設定されている場合
		if (!NextMovePosition.Contains(Direction)) {
			continue;
		}
		// 指定方向に進めない場合
		int32 FieldNum = NextMovePosition[Direction];
		if (FieldNum == FieldNotExists) {
			continue;
		}

		// ゴールのマスまで経路探索を行う
		bool IsArriveGoal = false;
		bool IsArriveAim = false;
		int32 NextPosCount = 0;
		while (!IsArriveGoal) {

			// ループ回数を加算
			++FieldNumToGoal[Direction][AllFieldIndex];
			// 残りの進めるマス数を減算
			if (RemainTempNum > Zero) {
				--RemainTempNum;
			}

			//---------------------------------
			// 対象のマスがゴールかどうかチェック
			// 赤マスの場合は配列内の数値を加算
			// ゴールだった場合、フラグを立て、ループを抜ける
			//---------------------------------
			AFieldBase* Field = FieldDataDetail[FieldNum];
			if (!IsValid(Field)) {
				break;
			}
			EFieldType FieldType = Field->GetFieldType();
			switch (FieldType) {
			case EFieldType::Minus: {
				// 金額減少マス
				++FieldNumToGoal[Direction][RedFieldIndex];
				break;
			}
			case EFieldType::Goal: {
				// ゴールマス
				IsArriveGoal = true;
				break;
			}
			}

			//---------------------------------
			// 停止予定マスなら獲得金額を取得
			//---------------------------------
			if (!IsArriveAim && RemainTempNum == Zero) {

				IsArriveAim = true;

				TArray<int32> ArrayEvent = GetFieldEvent(FieldNum);
				switch (FieldType) {
				case EFieldType::Plus: {
					// 金額増加マス
					int32 ColEvent = static_cast<int32>(EEventColumn::Detail);
					int32 Money = ArrayEvent[ColEvent];
					GettingMoneyValue[Direction] = Money;
					break;
				}
				case EFieldType::Minus: {
					// 金額減少マス
					int32 ColEvent = static_cast<int32>(EEventColumn::Detail);
					int32 Money = ArrayEvent[ColEvent];
					GettingMoneyValue[Direction] = MinusSign * Money;
					break;
				}
				default: {
					// 金額増加・減少マス
					GettingMoneyValue[Direction] = Zero;
				}
				}
			}

			// カウントをリセット
			NextPosCount = 0;
			// 経路探索
			Pos = SearchRoute(FieldNum);

			for (auto SubDirection : ArrayDirection) {
				// 指定方向に進めない場合
				int Number = Pos[SubDirection];
				if (Number == FieldNotExists) {
					continue;
				}
				++NextPosCount;
				FieldNum = Pos[SubDirection];
			}

			if (NextPosCount == Zero) {
				// これ以上進めない場合は処理終了
				break;
			}
			else if (NextPosCount > One) {
				// 移動可能マスが複数ある場合
				int32 TempFieldNum = SearchBestProfitRoute(RemainTempNum, Pos);
				FieldNum = TempFieldNum;
			}
		}
		// ゴールに辿り着けない場合はカウントリセット
		if (!IsArriveGoal) {
			FieldNumToGoal[Direction][AllFieldIndex] = InitRemainToGoalNum;
			FieldNumToGoal[Direction][RedFieldIndex] = InitRemainToGoalNum;
			continue;
		}
	}

	int32 TempMoney = InitGetMoney;
	EDirectionType RouteDirection = EDirectionType::Front;
	for (auto Direction : ArrayDirection) {

		// 金額が初期値の場合は処理しない
		if (GettingMoneyValue[Direction] == InitGetMoney) {
			continue;
		}
		// 獲得金額が前回保持した金額より多い場合、変数に格納
		if (TempMoney < GettingMoneyValue[Direction]) {
			TempMoney = GettingMoneyValue[Direction];
			RouteDirection = Direction;
		}
		else if (TempMoney == GettingMoneyValue[Direction]) {
			// 獲得金額が前回保持した金額と同額の場合、
			// 赤マスを踏む確率が少ない道を選択する
			float LastTimeProbability = static_cast<float>(FieldNumToGoal[RouteDirection][RedFieldIndex]) / static_cast<float>(FieldNumToGoal[RouteDirection][AllFieldIndex]);
			float CurrentProbability = static_cast<float>(FieldNumToGoal[Direction][RedFieldIndex]) / static_cast<float>(FieldNumToGoal[Direction][AllFieldIndex]);
			if (CurrentProbability > LastTimeProbability) {
				continue;
			}
			TempMoney = GettingMoneyValue[Direction];
			RouteDirection = Direction;
		}
	}

	UE_LOG(LogTemp, Display, TEXT("BestProfit Direction : %d"), static_cast<int32>(RouteDirection));
	UE_LOG(LogTemp, Display, TEXT("BestProfit GettingMoneyValue Front : %d"), GettingMoneyValue[EDirectionType::Front]);
	UE_LOG(LogTemp, Display, TEXT("BestProfit GettingMoneyValue Left  : %d"), GettingMoneyValue[EDirectionType::Left]);
	UE_LOG(LogTemp, Display, TEXT("BestProfit GettingMoneyValue Right : %d"), GettingMoneyValue[EDirectionType::Right]);

	UE_LOG(LogTemp, Display, TEXT("BestProfit Direction : %d"), static_cast<int32>(RouteDirection));
	UE_LOG(LogTemp, Display, TEXT("BestProfit FieldNumToGoal All Front : %d"), FieldNumToGoal[EDirectionType::Front][AllFieldIndex]);
	UE_LOG(LogTemp, Display, TEXT("BestProfit FieldNumToGoal Red Front : %d"), FieldNumToGoal[EDirectionType::Front][RedFieldIndex]);
	UE_LOG(LogTemp, Display, TEXT("BestProfit FieldNumToGoal All Left  : %d"), FieldNumToGoal[EDirectionType::Left][AllFieldIndex]);
	UE_LOG(LogTemp, Display, TEXT("BestProfit FieldNumToGoal Red Left  : %d"), FieldNumToGoal[EDirectionType::Left][RedFieldIndex]);
	UE_LOG(LogTemp, Display, TEXT("BestProfit FieldNumToGoal All Right : %d"), FieldNumToGoal[EDirectionType::Right][AllFieldIndex]);
	UE_LOG(LogTemp, Display, TEXT("BestProfit FieldNumToGoal Red Right : %d"), FieldNumToGoal[EDirectionType::Right][RedFieldIndex]);

	// 進むマスをセット
	int32 Number;
	Number = NextMovePosition[RouteDirection];

	return Number;
}

/// <summary>
/// フィールドイベントを取得
/// </summary>
/// <param name="Number"> フィールド番号 </param>
/// <returns> イベント内容 </returns>
TArray<int32> AFieldManager::GetFieldEvent(int Number) {

	// 対象のフィールド番号が存在しない場合はイベント無しとする
	if (!FieldData.Contains(Number)) {
		return FieldData[StartPos];
	}

	return FieldData[Number];
}

/// <summary>
/// 次に進めるマスの番号を取得
/// </summary>
/// <param name="Direction"> 進む方向 </param>
/// <returns> フィールド番号 </returns>
int32 AFieldManager::GetNextMoveNumber(EDirectionType Direction) {

	if (!NextMovePos.Contains(Direction)) {
		return FieldNotExists;
	}

	return NextMovePos[Direction];
}

/// <summary>
/// 選択中の方向をセット
/// </summary>
/// <param name="Direction"> 方向 </param>
/// <returns></returns>
void AFieldManager::SetSelectDirection(EDirectionType Direction) {

	SelectDirection = Direction;
}

/// <summary>
/// 選択中の方向を取得
/// </summary>
/// <param name=""></param>
/// <returns> 選択中の方向 </returns>
EDirectionType AFieldManager::GetSelectDirection() {

	return SelectDirection;
}

/// <summary>
/// 次に移動できるマスを取得
/// </summary>
/// <param name=""></param>
/// <returns> 移動できるマス配列 </returns>
TMap<EDirectionType, int32> AFieldManager::GetNextPos() {

	return NextMovePos;
}