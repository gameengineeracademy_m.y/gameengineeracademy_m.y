﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/UMG/Public/Components/WidgetComponent.h"
#include "Battle/MoneyTextWidget.h"
#include "Battle/InfoWidget.h"
#include "FieldBase.generated.h"

/// <summary>
/// フィールドの種類
/// </summary>
UENUM(BlueprintType)
enum class EFieldType : uint8 {
	None,          // マスなし
	Normal,        // 何も起こらないマス
	Plus,          // お金獲得マス
	Minus,         // お金損失マス
	Move,          // 強制移動マス
	Start,         // スタートマス
	Goal,          // ゴールマス
	MaxTypeIndex   // 種類数
};

/// <summary>
/// フィールドマネージャ処理
/// </summary>
UCLASS()
class AFieldBase : public AActor
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AFieldBase();

	/// <summary>
	/// フィールドの種類をセット
	/// </summary>
	/// <param name="FieldTypeValue"> フィールドの種類 </param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "FieldBase")
	void SetFieldType(EFieldType FieldTypeValue);

	/// <summary>
	/// フィールドの種類を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> フィールドの種類 </returns>
	UFUNCTION(BlueprintCallable, Category = "FieldBase")
	EFieldType GetFieldType();

	/// <summary>
	/// ウィジェット配置 調整処理
	/// </summary>
	/// <param name=""></param>
	/// <returns> フィールドの種類 </returns>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FieldBase")
	void AdjustWidgetTransform();

	/// <summary>
	/// フィールドの番号をセット
	/// </summary>
	/// <param name="Order"> フィールドの番号 </param>
	/// <returns></returns>
	void SetFieldOrder(int32 Order);

	/// <summary>
	/// フィールドの番号を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> フィールドの番号 </returns>
	int32 GetFieldOrder();

	/// <summary>
	/// フィールドの固有番号をセット
	/// </summary>
	/// <param name="Order"> フィールドの固有番号 </param>
	/// <returns></returns>
	void SetFieldUniOrder(int32 Order);

	/// <summary>
	/// フィールドの固有番号を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> フィールドの固有番号 </returns>
	int32 GetFieldUniOrder();

	/// <summary>
	/// 初期化処理フェーズ
	/// </summary>
	/// <param name="FieldTypeValue"> フィールドの種類 </param>
	/// <returns></returns>
	void Initialize(EFieldType FieldTypeValue);

	/// <summary>
	/// ウィジェットマテリアルの設定
	/// </summary>
	/// <param name="MaterialPath"> マテリアルパス </param>
	/// <returns></returns>
	void SetupWidgetMaterial(FString MaterialPath);

	/// <summary>
	/// マニーウィジェットクラスの設定
	/// </summary>
	/// <param name="MoneyValue"> 金額 </param>
	/// <returns></returns>
	void SetupMoneyWidgetClass(int32 MoneyValue);

	/// <summary>
	/// インフォウィジェットクラスの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupInfoWidgetClass();

public:

	/// <summary>
	/// スタティックメッシュ
	/// </summary>
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "FieldBase")
	UStaticMeshComponent* StaticMeshComp;

	/// <summary>
	/// ウィジェットコンポーネント
	/// </summary>
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="FieldBase")
	UWidgetComponent* WidgetComp;

	/// <summary>
	/// マニーウィジェットクラス
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	TSubclassOf<UMoneyTextWidget> MoneyTextWidgetClass;

	/// <summary>
	/// マニーウィジェット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	UMoneyTextWidget* MoneyTextWidget;

	/// <summary>
	/// インフォウィジェットクラス
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	TSubclassOf<UInfoWidget> InfoWidgetClass;

	/// <summary>
	/// インフォウィジェット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	UInfoWidget* InfoWidget;


public:

	/// <summary>
	/// 毎フレームの処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

protected:

	/// <summary>
	/// 実行開始イベント
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// フィールドの種類
	/// </summary>
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="FieldBase")
	EFieldType FieldType;


private:

	/// <summary>
	/// フィールドの進行番号 
	/// </summary>
	int32 FieldOrder;

	/// <summary>
	/// フィールドの固有番号 
	/// </summary>
	int32 FieldUniOrder;

	/// <summary>
	/// メッシュマテリアルの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupMeshMaterial();

	/// <summary>
	/// フィールドの種類をセット
	/// </summary>
	/// <param name="FieldTypeValue"> フィールドの種類 </param>
	/// <returns></returns>
	virtual void SetFieldType_Implementation(EFieldType FieldTypeValue);

	/// <summary>
	/// フィールドの種類を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> フィールドの種類 </returns>
	virtual EFieldType GetFieldType_Implementation();

	/// <summary>
	/// ウィジェット配置 調整処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void AdjustWidgetTransform_Implementation();


private:

	/// <summary>
	/// ブルーマテリアルパス
	/// </summary>
	//const FString BlueMaterialPath = "/Game/Materials/BlueCubeMaterial";
	const FString BlueMaterialPath = "/Game/Materials/FieldMaterial01";

	/// <summary>
	/// レッドマテリアルパス
	/// </summary>
	//const FString RedMaterialPath = "/Game/Materials/RedCubeMaterial";
	const FString RedMaterialPath = "/Game/Materials/FieldMaterial03";

	/// <summary>
	/// イエローマテリアルパス
	/// </summary>
	//const FString YellowMaterialPath = "/Game/Materials/YellowCubeMaterial";
	const FString YellowMaterialPath = "/Game/Materials/FieldMaterial02";

	/// <summary>
	/// ノーマルマテリアルパス
	/// </summary>
	//const FString NormalMaterialPath = "/Game/Materials/NormalCubeMaterial";
	const FString NormalMaterialPath = "/Game/Materials/FieldMaterial04";

	/// <summary>
	/// ライトブルーマテリアルパス
	/// </summary>
	const FString LightBlueMaterialPath = "/Game/Materials/FieldMaterial05";

	/// <summary>
	/// ピンクマテリアルパス
	/// </summary>
	//const FString PinkMaterialPath = "/Game/Materials/PinkCubeMaterial";
	const FString PinkMaterialPath = "/Game/Materials/FieldMaterial06";

	/// <summary>
	/// マテリアルインデックス
	/// </summary>
	const int32 MaterialIndex0 = 0;

	/// <summary>
	/// 魔法陣回転スピード
	/// </summary>
	const float RotSpeed = 300.0f;

	/// <summary>
	/// 魔法陣位置情報 X座標
	/// </summary>
	const float MagicCirclePosX = 0.0f;

	/// <summary>
	/// 魔法陣位置情報 Y座標
	/// </summary>
	const float MagicCirclePosY = 0.0f;

	/// <summary>
	/// 魔法陣位置情報 Z座標
	/// </summary>
	const float MagicCirclePosZ = 150.0f;

	/// <summary>
	/// 魔法陣回転情報 Pitch
	/// </summary>
	const float MagicCirclePitch = 0.0f;

	/// <summary>
	/// 魔法陣回転情報 Yaw
	/// </summary>
	const float MagicCircleYaw = 90.0f;

	/// <summary>
	/// 魔法陣回転情報 Roll
	/// </summary>
	const float MagicCircleRoll = 0.0f;

	/// <summary>
	/// 魔法陣拡大縮小情報 X方向
	/// </summary>
	const float MagicCircleScaleX = 1.0f;

	/// <summary>
	/// 魔法陣拡大縮小情報 Y方向
	/// </summary>
	const float MagicCircleScaleY = 0.2f;

	/// <summary>
	/// 魔法陣拡大縮小情報 Z方向
	/// </summary>
	const float MagicCircleScaleZ = 3.0f;

	/// <summary>
	/// 金額ウィジェットパス
	/// </summary>
	const FString MoneyWidgetPath = "/Game/Widgets/WBP_FieldMoneyText.WBP_FieldMoneyText_C";

	/// <summary>
	/// ウィジェットパス
	/// </summary>
	const FString InfoWidgetPath = "/Game/Widgets/WBP_FieldInfoText.WBP_FieldInfoText_C";

	/// <summary>
	/// ゼロ
	/// </summary>
	const int32 Zero = 0;

	/// <summary>
	/// 参
	/// </summary>
	const int32 Three = 3;
};