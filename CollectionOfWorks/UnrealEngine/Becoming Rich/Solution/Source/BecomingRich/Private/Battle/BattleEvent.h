﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "BattleEvent.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UBattleEvent : public UInterface
{
	GENERATED_BODY()
};

/// <summary>
/// バトルレベルイベントインターフェース
/// </summary>
class IBattleEvent
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	/// <summary>
	/// フィールドの座標を取得
	/// </summary>
	/// <param name="FieldNum"> フィールド番号 </param>
	/// <returns> 表示座標 </returns>
	virtual FVector OnGetFieldPosition(int32 FieldNum) = 0;

	/// <summary>
	/// 前方選択
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSelectFront() = 0;

	/// <summary>
	/// 左方向選択
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSelectLeft() = 0;

	/// <summary>
	/// 右方向選択
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSelectRight() = 0;

	/// <summary>
	/// ダイス切り替え時間変更
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnChangeDiceTime() = 0;

	/// <summary>
	/// マップ用カメラの切り替え
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSwitchMapCamera() = 0;

	/// <summary>
	/// サイコロを振る
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnRollDice() = 0;

	/// <summary>
	/// 決定操作
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnDecide() = 0;

	/// <summary>
	/// マスイベント処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns> true:処理終了, false:処理中 </returns>
	virtual bool OnOccurEvent(float DeltaTime) = 0;

	/// <summary>
	/// プレイヤーの切り替え
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:リザルトへ, false:プレイ続行 </returns>
	virtual bool OnSwitchPlayer() = 0;

	/// <summary>
	/// 各オブジェクトの切り替え
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSwitchObject() = 0;

	/// <summary>
	/// ターン開始処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnStartTurn() = 0;

	/// <summary>
	/// サイコロ処理が終了しているかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:終了, false:処理中 </returns>
	virtual bool OnIsFinishDice() = 0;

	/// <summary>
	/// プレイヤーのフェーズを「移動」に変更
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnChangePlayerPhaseMove() = 0;

	/// <summary>
	/// 次に進むマスを探索
	/// </summary>
	/// <param name=""></param>
	/// <returns> フィールド番号の配列 </returns>
	virtual TArray<int32> OnSearchForward() = 0;

	/// <summary>
	/// 分岐選択準備
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnPrepareSelectRoute() = 0;

	/// <summary>
	/// 分岐選択思考
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnThinkSelectRoute() = 0;

	/// <summary>
	/// 次に進むマスを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual int32 OnGetNextMovePos() = 0;

	/// <summary>
	/// サイコロウィジェットの更新
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnUpdateDiceWidget() = 0;

	/// <summary>
	/// プレイ用カメラをプレイヤーに追従させる
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnFollowPlayCamera() = 0;

	/// <summary>
	/// 移動処理が終了したかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:終了, false:移動中 </returns>
	virtual bool OnIsFinishMove() = 0;

	/// <summary>
	/// イベント処理終了
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnFinishEvent() = 0;

	/// <summary>
	/// イベント処理が終了しているかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:終了, false:処理中 </returns>
	virtual bool OnIsFinishEvent() = 0;

	/// <summary>
	/// 操作可能か取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:操作可能, false:操作不可 </returns>
	virtual bool OnCheckEnable() = 0;

	/// <summary>
	/// ゴールに到達したかチェック
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:ゴール, false:ゴール以外 </returns>
	virtual bool OnCheckArriveGoal() = 0;

	/// <summary>
	/// ゲーム情報をリザルトへ渡す
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnPrepareResult() = 0;

	/// <summary>
	/// フィニッシュ表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnShowFinish() = 0;

	/// <summary>
	/// リザルト表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnShowResult() = 0;

	/// <summary>
	/// ゲーム終了処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnEndGame() = 0;
};