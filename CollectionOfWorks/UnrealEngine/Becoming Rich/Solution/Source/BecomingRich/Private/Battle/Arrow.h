// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Battle/BattleEvent.h"
#include "Arrow.generated.h"

/// <summary>
/// 矢印
/// </summary>
UCLASS()
class AArrow : public APawn
{
	GENERATED_BODY()

public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AArrow();

	/// <summary>
	/// プレイヤーを表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Show();

	/// <summary>
	/// プレイヤーを非表示する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Hide();

	/// <summary>
	/// Wキー押下
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressKeyW();

	/// <summary>
	/// Aキー押下
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressKeyA();

	/// <summary>
	/// Dキー押下
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void PressKeyD();

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	/// <param name=""> イベントインターフェース </param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "EventInterface")
	void SetEventInterface(const TScriptInterface<IBattleEvent>& Interface);

	/// <summary>
	/// メッシュコンポーネント
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Arrow")
	UStaticMeshComponent* MeshComp;


protected:

	/// <summary>
	/// 実行開始イベント
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:

	/// <summary>
	/// 毎フレームの処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

	/// <summary>
	/// PlayerInputComponent設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IBattleEvent> EventInterface;

	/// <summary>
	/// 回転スピード
	/// </summary>
	float RotateSpeed;

	/// <summary>
	/// 回転動作
	/// </summary>
	float YawAngle;

private:
	//-------------------------------
	// 以下、定数宣言
	//-------------------------------

	/// <summary>
	/// 回転スピード
	/// </summary>
	const float RotSpeed = 100.0f;

	/// <summary>
	/// 角度
	/// </summary>
	const float InitAngle = 0.0f;
};
