﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Battle/PlayerBase.h"
#include "Battle/BattleEvent.h"
#include "PlayerManager.generated.h"


/// <summary>
/// 操作フェーズの種類
/// </summary>
UENUM()
enum class EOperatePhase {
	WaitRollDice,
	RollDice,
	Wait,
	PrepareMove,
	Move,
	Moved,
	WaitMove,
	ThinkMoveDirection,
	DecideCpu,
	DecideMoveDirection,
	SearchDestination,
	FinishMove,
	MaxPhaseIndex
};

/// <summary>
/// イベント移動フェーズ
/// </summary>
UENUM()
enum class EEventMovePhase {
	None,
	MovePrepare,
	MoveWait,
	Move,
	FinishMove,
	MaxPhaseIndex
};

/// <summary>
/// プレイヤーマネージャ処理
/// </summary>
UCLASS()
class APlayerManager : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	APlayerManager();

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	/// <param name=""> イベントインターフェース </param>
	/// <returns></returns>
	UFUNCTION(BlueprintCallable, Category = "EventInterface")
	void SetEventInterface(const TScriptInterface<IBattleEvent>& Interface);

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Initialize();

	/// <summary>
	/// 待機座標の取得
	/// </summary>
	/// <param name="Order"> プレイヤー識別 </param>
	/// <returns> 調整座標値 </returns>
	FVector GetWaitAdjustPosition(EPlayerOrder Order);

	/// <summary>
	/// 指定プレイヤーを取得
	/// </summary>
	/// <param name="Order"> 識別番号 </param>
	/// <returns> 指定プレイヤー </returns>
	APlayerBase* GetPlayer(EPlayerOrder Order);

	/// <summary>
	/// ターンをセット
	/// </summary>
	/// <param name="Turn"> 順番 </param>
	/// <returns></returns>
	void SetCurrentTurn(int32 Turn);

	/// <summary>
	/// ターンを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 順番 </returns>
	int32 GetCurrentTurn();

	/// <summary>
	/// プレイヤー識別を取得
	/// </summary>
	/// <param name="Turn"> 順番 </param>
	/// <returns> 順番 </returns>
	EPlayerOrder GetOperationOrder(int32 Turn);

	/// <summary>
	/// 全てのプレイヤーの取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> プレイヤー配列 </returns>
	TArray<EPlayerOrder> GetAllPlayer();

	/// <summary>
	/// プレイ人数を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> プレイ人数 </returns>
	int32 GetPlayerNum();

	/// <summary>
	/// 進むマス数をセット
	/// </summary>
	/// <param name="Roll"> ダイスの出目 </param>
	/// <returns></returns>
	void SetForwardNum(int32 Roll);

	/// <summary>
	/// 操作プレイヤーの切り替え
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:全プレイヤーゴール, false:プレイ中 </returns>
	bool SwitchOperationPlayer();

	/// <summary>
	/// 現在のフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentPhase(EOperatePhase PhaseType);

	/// <summary>
	/// 現在の移動フェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentMovePhase(EEventMovePhase PhaseType);

	/// <summary>
	/// 現在のフェーズを取得する
	/// </summary>
	/// <param name=""></param>
	/// <returns> フェーズの種類 </returns>
	EOperatePhase GetCurrentPhase();

	/// <summary>
	/// 移動方向を決定する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ThinkMoveDirection();

	/// <summary>
	/// 移動処理を実行する
	/// </summary>
	/// <param name="Player"> プレイヤー </param>
	/// <param name="Number"> フィールド番号 </param>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void ExecuteMove(APlayerBase* Player, int32 Number, float DeltaTime);

	/// <summary>
	/// 目標座標をセット
	/// </summary>
	/// <param name="Position"> 目標座標 </param>
	/// <returns></returns>
	void SetGoalPosition(FVector Position);

	/// <summary>
	/// プレイヤーがサイコロ振り待機中かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:待機中, false:それ以外 </returns>
	bool IsWaitRollDice();

	/// <summary>
	/// プレイヤーが移動待機中かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:待機中, false:それ以外 </returns>
	bool IsWaitMove();

	/// <summary>
	/// プレイヤーが思考中かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:思考中, false:思考中以外 </returns>
	bool IsThinkMoveDirection();

	/// <summary>
	/// プレイヤーが移動中かどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:移動中, false:移動以外 </returns>
	bool IsMove();

	/// <summary>
	/// 移動操作が終了したかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:終了, false:移動中 </returns>
	bool IsFinishMove();

	/// <summary>
	/// ゴールの着順を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ゴールの着順 </returns>
	int32 GetArrivalGoalOrder();


protected:
	// Called when the game starts or when spawned
	/// <summary>
	/// 実行開始イベント
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:	
	// Called every frame
	/// <summary>
	/// 毎フレームの処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	/// <summary>
	/// SetupPlayerInputComponent
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	/// <summary>
	/// イベントインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IBattleEvent> EventInterface;

	/// <summary>
	/// プレイヤーリスト
	/// </summary>
	UPROPERTY()
	TMap<EPlayerOrder, APlayerBase*> PlayerList;

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	EOperatePhase CurrentPhase;

	/// <summary>
	/// 現在の移動フェーズ
	/// </summary>
	EEventMovePhase CurrentMovePhase;

	/// <summary>
	/// 操作順序リスト
	/// </summary>
	TMap<int32, EPlayerOrder> OperationOrderList;

	/// <summary>
	/// 現在のターン
	/// </summary>
	int32 CurrentTurn;

	/// <summary>
	/// プレイ人数
	/// </summary>
	TArray<EPlayerOrder> AllPlayer;

	/// <summary>
	/// 進むマス数
	/// </summary>
	int32 ForwardNum;

	/// <summary>
	/// 目標座標
	/// </summary>
	FVector GoalPosition;

	/// <summary>
	/// 移動量
	/// </summary>
	FVector MoveValue;

	/// <summary>
	/// 累積時間
	/// </summary>
	float NormalMoveAccumuTime;

	/// <summary>
	/// 累積時間
	/// </summary>
	float SpecialMoveAccumuTime;

	/// <summary>
	/// 待機時間
	/// </summary>
	float WaitTime;

	/// <summary>
	/// プレイヤー生成
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:処理成功, false:処理失敗 </returns>
	bool CreatePlayer();

	/// <summary>
	/// プレイヤー通常移動処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <param name="Player"> プレイヤー </param>
	/// <returns></returns>
	void MoveNormalPlayer(float DeltaTime, APlayerBase* Player);

	/// <summary>
	/// プレイヤー特殊移動処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <param name="Player"> プレイヤー </param>
	/// <returns></returns>
	void MoveSpecialPlayer(float DeltaTime, APlayerBase* Player);

	/// <summary>
	/// サイコロを振る ※CPU操作
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OperationCpuRollDice();

	/// <summary>
	/// サイコロを止める ※CPU操作
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OperationCpuStopDice();

	/// <summary>
	/// 目標座標を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 目標座標 </returns>
	FVector GetGoalPosition();

	/// <summary>
	/// 移動量をセット
	/// </summary>
	/// <param name="Rate"> 移動時間の割合 </param>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	void SetMoveValue(float Rate, float DeltaTime);

	/// <summary>
	/// 移動量を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 移動量 </returns>
	FVector GetMoveValue();

	/// <summary>
	/// 移動後の座標を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 移動後の座標 </returns>
	FVector CalcMovedPosition();

	/// <summary>
	/// プレイヤーに力積を与える
	/// </summary>
	/// <param name="ValueX"> X軸方向のベクトル </param>
	/// <param name="ValueY"> Y軸方向のベクトル </param>
	/// <param name="ValueZ"> Z軸方向のベクトル </param>
	/// <returns></returns>
	void AddImpulsePlayer(float ValueX, float ValueY, float ValueZ);

	/// <summary>
	/// 目標座標に到達したかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:到達した, flase:到達していない </returns>
	bool IsArriveGoalPosition();


private:

	/// <summary>
	/// プレイヤー情報CSVファイルパス
	/// </summary>
	const FString PlayerCsvFilePath = "Content/File/PlayerData.csv";

	/// <summary>
	/// プレイヤークラスパス
	/// </summary>
	const FString PlayerClassPath = "/Game/Blueprints/BP_Player.BP_Player_C";

	/// <summary>
	/// ゼロ
	/// </summary>
	const int32 Zero = 0;

	/// <summary>
	/// ゼロ
	/// </summary>
	const float Zerof = 0.0f;

	/// <summary>
	/// 壱
	/// </summary>
	const int32 One = 1;

	/// <summary>
	/// 最小インデックス
	/// </summary>
	const int32 MinIndex = 0;

	/// <summary>
	/// プレイヤーの種類列
	/// </summary>
	const int32 ColPlayerType = 1;

	/// <summary>
	/// 配置マス列
	/// </summary>
	const int32 ColFieldNum = 2;

	/// <summary>
	/// 所持金列
	/// </summary>
	const int32 ColPocketMoney = 3;

	/// <summary>
	/// プレイヤー表示座標 調整値
	/// </summary>
	const float PlayerPosZ = 280.0f;

	/// <summary>
	/// プレイヤー表示角度 調整値
	/// </summary>
	const float PlayerAngleX = 0.0f;

	/// <summary>
	/// プレイヤー表示角度 調整値
	/// </summary>
	const float PlayerAngleY = 180.0f;

	/// <summary>
	/// プレイヤー表示角度 調整値
	/// </summary>
	const float PlayerAngleZ = 0.0f;

	/// <summary>
	/// リセット時間
	/// </summary>
	const float ResetTime = 0.0f;

	/// <summary>
	/// サイコロ振る待機時間
	/// </summary>
	const float RollDiceWaitTime = 0.5f;

	/// <summary>
	/// サイコロ停止までの待機時間 下限値
	/// </summary>
	const float StopDiceLowWaitTime = 2.0f;

	/// <summary>
	/// サイコロ停止までの待機時間 上限値
	/// </summary>
	const float StopDiceHighWaitTime = 4.0f;

	/// <summary>
	/// 移動までの待機時間
	/// </summary>
	const float MoveWaitTime = 1.2f;

	/// <summary>
	/// 移動先決定後の待機時間
	/// </summary>
	const float DecideMoveWaitTime = 1.0f;

	/// <summary>
	/// テスト用待機時間
	/// </summary>
	const float TestWaitTime = 0.5f;

	/// <summary>
	/// 移動量初期値
	/// </summary>
	const float InitMoveValue = 0.0f;

	/// <summary>
	/// プレイヤー移動時間
	/// </summary>
	const float NoramlMoveRate = 2.1f;

	/// <summary>
	/// プレイヤー移動時間
	/// </summary>
	const float FinalMoveRate = 1.0f;

	/// <summary>
	/// 加える力積の値
	/// </summary>
	const float ImpValueX = 0.0f;

	/// <summary>
	/// 加える力積の値
	/// </summary>
	const float ImpValueY = 0.0f;

	/// <summary>
	/// 加える力積の値
	/// </summary>
	const float NormalImpValueZ = 25000.0f;

	/// <summary>
	/// 加える力積の値
	/// </summary>
	const float FinalImpValueZ = 50000.0f;

	/// <summary>
	/// 待機場所 調整値 正の値
	/// </summary>
	const float PlusAdjust = 60.0f;

	/// <summary>
	/// 待機場所 調整値 正の値
	/// </summary>
	const float MinusAdjust = -60.0f;

	/// <summary>
	/// 待機場所 調整値 Z座標
	/// </summary>
	const float AdjustPosZ = 80.0f;

	/// <summary>
	/// 重力値 無重力
	/// </summary>
	const float ZeroGravityValue = 0.0f;

	/// <summary>
	/// 重力値 重力あり
	/// </summary>
	const float GravityValue = 1.0f;

	/// <summary>
	/// ワープ時の初期高さ
	/// </summary>
	const float WarpInitHeight = 350.0f;
};
