﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <unordered_map>
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Battle/FieldBase.h"
#include "FieldManager.generated.h"

/// <summary>
/// 方向
/// </summary>
UENUM()
enum class EDirectionType {
	Front,         // 前
	Right,         // 右
	Left,          // 左
	MaxTypeIndex   // 項目数
};

/// <summary>
/// イベント列
/// </summary>
UENUM()
enum class EEventColumn {
	Type,         // イベント種類
	Detail,       // イベント内容
	ColumnIndex   // 列数
};

/// <summary>
/// 探索方法種類
/// </summary>
UENUM()
enum class ESearchType {
	ShortRoute,   // 最短距離
	LongRoute,    // 最長距離
	ChasePlayer,  // プレイヤー追跡
	ColumnIndex   // 列数
};

/// <summary>
/// フィールドマネージャ処理
/// </summary>
UCLASS()
class AFieldManager : public AActor
{
	GENERATED_BODY()
	
public:	

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AFieldManager();

	/// <summary>
	/// フィールドの表示座標取得
	/// </summary>
	/// <param name="FieldNum"> フィールド番号 </param>
	/// <returns> 表示座標 </returns>
	FVector GetFieldPosition(int32 FieldNum);

	/// <summary>
	/// ゴールかどうかの確認
	/// </summary>
	/// <param name="FieldNum"> フィールド番号 </param>
	/// <returns> true:ゴール, false:ゴール以外 </returns>
	bool IsArriveGoal(int32 FieldNum);

	/// <summary>
	/// 進むことができるマスの取得
	/// </summary>
	/// <param name="FieldNum"> フィールド番号 </param>
	/// <returns> 表示座標 </returns>
	TMap<EDirectionType, int32> SearchCanForward(int32 FieldNum);

	/// <summary>
	/// 最短・最長経路探索処理
	/// </summary>
	/// <param name="Type"> 探索種類 </param>
	/// <param name="NextMove"> 次に進めるマス </param>
	/// <returns> フィールドのマス </returns>
	int32 SearchShortAndLongRoute(ESearchType Type, TMap<EDirectionType, int32> NextMove);

	/// <summary>
	/// 脇道にプレイヤーがいる時に追跡する
	/// </summary>
	/// <param name="AimPos"> 目標のマス </param>
	/// <param name="NextMove"> 次に進めるマス </param>
	/// <returns> フィールドのマス </returns>
	int32 ChasePlayer(int32 AimPos, TMap<EDirectionType, int32> NextMove);

	/// <summary>
	/// 赤パネルを踏む確率が少ない道を選ぶ
	/// </summary>
	/// <param name="NextMove"> 次に進めるマス </param>
	/// <returns> フィールドのマス </returns>
	int32 SearchLowRiskRoute(TMap<EDirectionType, int32> NextMove);

	/// <summary>
	/// 止まるマスが最大の利益になる道を選ぶ
	/// </summary>
	/// <param name="RemainNum"> 残りの進めるマス数 </param>
	/// <param name="NextMove"> 次に進めるマス </param>
	/// <returns> フィールドのマス </returns>
	int32 SearchImmediateProfitRoute(int32 RemainNum, TMap<EDirectionType, int32> NextMove);

	/// <summary>
	/// 止まるマスが最大の利益になる道を選ぶ
	/// 最大利益マスが複数ある場合は赤パネルを踏む確率が少ない道を選ぶ
	/// </summary>
	/// <param name="RemainNum"> 残りの進めるマス数 </param>
	/// <param name="NextMove"> 次に進めるマス </param>
	/// <returns> フィールドのマス </returns>
	int32 SearchBestProfitRoute(int32 RemainNum, TMap<EDirectionType, int32> NextMove);

	/// <summary>
	/// フィールドイベントを取得
	/// </summary>
	/// <param name="Number"> フィールド番号 </param>
	/// <returns> イベント内容 </returns>
	TArray<int32> GetFieldEvent(int Number);

	/// <summary>
	/// 次に進めるマスの番号を取得
	/// </summary>
	/// <param name="Direction"> 進む方向 </param>
	/// <returns> フィールド番号 </returns>
	int32 GetNextMoveNumber(EDirectionType Direction);

	/// <summary>
	/// 選択中の方向をセット
	/// </summary>
	/// <param name="Direction"> 方向 </param>
	/// <returns></returns>
	void SetSelectDirection(EDirectionType Direction);

	/// <summary>
	/// 選択中の方向を取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 選択中の方向 </returns>
	EDirectionType GetSelectDirection();

	/// <summary>
	/// 次に移動できるマスを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> 移動できるマス配列 </returns>
	TMap<EDirectionType, int32> GetNextPos();


protected:
	// Called when the game starts or when spawned
	/// <summary>
	/// 実行開始イベント
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

public:
	// Called every frame
	/// <summary>
	/// 毎フレームの処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

private:

	/// <summary>
	/// フィールド情報
	/// </summary>
	TMap<int32, TArray<int32>> FieldMap;

	/// <summary>
	/// フィールド順番
	/// </summary>
	TMap<int32, TArray<int32>> FieldOrder;

	/// <summary>
	/// フィールドマス情報
	/// </summary>
	UPROPERTY()
	TMap<int32, AFieldBase*> FieldDataDetail;

	/// <summary>
	/// フィールドデータ
	/// </summary>
	TMap<int32, TArray<int32>> FieldData;

	/// <summary>
	/// 進めるマスの候補
	/// </summary>
	UPROPERTY()
	TMap<EDirectionType, int32> NextMovePos;

	/// <summary>
	/// 選択中の方向
	/// </summary>
	EDirectionType SelectDirection;

	/// <summary>
	/// フィールドデータCSV読み込み処理
	/// </summary>
	/// <param name="FilePath"> ファイルパス </param>
	/// <returns> フィールド配列 </returns>
	TMap<int32, TArray<int32>> ReadCsvFieldDataFile(FString FilePath);

	/// <summary>
	/// フィールドCSV読み込み処理
	/// </summary>
	/// <param name="FilePath"> ファイルパス </param>
	/// <returns> フィールド配列 </returns>
	TMap<int32, TArray<int32>> ReadCsvFieldMapFile(FString FilePath);

	/// <summary>
	/// フィールド生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreateField();

	/// <summary>
	/// ウィジェットマテリアルの設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupWidgetMaterial();

	/// <summary>
	/// 数値の先頭1桁を取得
	/// </summary>
	/// <param name="Number"> 対象の数値 </param>
	/// <returns> 数値の先頭1桁 </returns>
	int CalcFirstNumber(int32 Number);

	/// <summary>
	/// 経路探索
	/// </summary>
	/// <param name=""> フィールド番号 </param>
	/// <returns> 移動可能マス </returns>
	TMap<EDirectionType, int32> SearchRoute(int32 FieldNum);


private:

	/// <summary>
	/// フィールドマップ情報CSVファイルパス
	/// </summary>
	const FString FieldMapCsvFilePath = "Content/File/FieldMap.csv";

	/// <summary>
	/// フィールド順番CSVファイルパス
	/// </summary>
	const FString FieldOrderCsvFilePath = "Content/File/FieldMapOrder.csv";

	/// <summary>
	/// フィールドクラスパス
	/// </summary>
	const FString FieldClassPath = "/Game/Blueprints/BP_Field.BP_Field_C";

	/// <summary>
	/// フィールドデータ情報CSVファイルパス
	/// </summary>
	const FString FieldDataCsvFilePath = "Content/File/FieldData.csv";

	/// <summary>
	/// フィールドが存在しないマス
	/// </summary>
	const int32 FieldNotExists = 0;

	/// <summary>
	/// フィールドサイズ 幅
	/// </summary>
	const float FieldSizeWidth = 250.0f;

	/// <summary>
	/// フィールドサイズ 高さ
	/// </summary>
	const float FieldSizeHeight = 250.0f;

	/// <summary>
	/// フィールド配置 X座標 初期値
	/// </summary>
	const float FieldPosX = 0.0f;

	/// <summary>
	/// フィールド配置 Y座標 初期値
	/// </summary>
	const float FieldPosY = 0.0f;

	/// <summary>
	/// フィールド配置高さ
	/// </summary>
	const float FieldPosZ = 200.0f;

	/// <summary>
	/// フィールド 回転初期値
	/// </summary>
	const float FieldInitRotation = 0.0f;

	/// <summary>
	/// スタート・ゴールマスのスケール X方向
	/// </summary>
	const float ScaleX = 2.5f;

	/// <summary>
	/// スタート・ゴールマスのスケール Y方向
	/// </summary>
	const float ScaleY = 2.5f;

	/// <summary>
	/// スタート・ゴールマスのスケール Z方向
	/// </summary>
	const float ScaleZ = 0.1f;

	/// <summary>
	/// 半分
	/// </summary>
	const float Half = 2.0f;

	/// <summary>
	/// ゼロ
	/// </summary>
	const int32 Zero = 0;

	/// <summary>
	/// 壱
	/// </summary>
	const int32 One = 1;

	/// <summary>
	/// 10
	/// </summary>
	const int32 Ten = 10;

	/// <summary>
	/// 最小インデックス
	/// </summary>
	const int32 MinIndex = 0;

	/// <summary>
	/// スタート地点
	/// </summary>
	const int32 StartPos = 500;

	/// <summary>
	/// 残りマス数 初期値
	/// </summary>
	const int32 InitRemainToGoalNum = -1;

	/// <summary>
	/// 強制移動マス その1
	/// </summary>
	const int32 ForcedMoveOrder01 = 401;

	/// <summary>
	/// 強制移動マス その2
	/// </summary>
	const int32 ForcedMoveOrder02 = 402;

	/// <summary>
	/// 強制移動マス その3
	/// </summary>
	const int32 ForcedMoveOrder03 = 403;

	/// <summary>
	/// 魔法陣01マテリアルパス
	/// </summary>
	const FString MagicCircle01Path = "/Game/Materials/MagicCircle01";

	/// <summary>
	/// 魔法陣02マテリアルパス
	/// </summary>
	const FString MagicCircle02Path = "/Game/Materials/MagicCircle02";

	/// <summary>
	/// 魔法陣03マテリアルパス
	/// </summary>
	const FString MagicCircle03Path = "/Game/Materials/MagicCircle03";

	/// <summary>
	/// 魔法陣04マテリアルパス
	/// </summary>
	const FString MagicCircle04Path = "/Game/Materials/MagicCircle04";

	/// <summary>
	/// 総マス数インデックス   「赤パネルを踏む確率が少ない道を選ぶ」処理で使用
	/// </summary>
	const int32 AllFieldIndex = 0;

	/// <summary>
	/// 赤マス数インデックス   「赤パネルを踏む確率が少ない道を選ぶ」処理で使用
	/// </summary>
	const int32 RedFieldIndex = 1;

	/// <summary>
	/// 100%   「赤パネルを踏む確率が少ない道を選ぶ」処理で使用
	/// </summary>
	const float HundredPercent = 100.0f;

	/// </summary>
	/// 初期獲得金額   「止まるマスが最大の利益になる道を選ぶ」処理で使用
	/// </summary>
	const int32 InitGetMoney = -9999999;

	/// <summary>
	/// マイナス
	/// </summary>
	const int32 MinusSign = -1;
};