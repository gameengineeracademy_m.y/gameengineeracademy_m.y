﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InfoWidget.generated.h"

/// <summary>
/// マスに表示するインフォウィジェット
/// </summary>
UCLASS()
class UInfoWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void NativeConstruct() override;

	/// <summary>
	/// Tick処理
	/// </summary>
	/// <param name="MyGeometry"> ジオメトリー </param>
	/// <param name="InDaltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void NativeTick(const FGeometry& MyGeometry, float InDaltaTime) override;

};
