// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/MapCamera.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AMapCamera::AMapCamera()
{
	// Tickイベントの処理は行わない
	PrimaryActorTick.bCanEverTick = false;

	BaseTurnRate = 45.0f;
	BaseLookUpRate = 45.0f;

	// アクターとカメラをつなぐ
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->TargetArmLength = 600.0f;
	SpringArm->bUsePawnControlRotation = true;
	SpringArm->bDoCollisionTest = false;   // カメラコリジョンテストは行わない
	SpringArm->bInheritPitch = false;      // Pitch制御なし
	SpringArm->bInheritYaw = false;        // Yaw制御なし
	SpringArm->bInheritRoll = false;       // Roll制御なし

	// カメラ設定
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false;
}

/// <summary>
/// 実行開始時処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AMapCamera::BeginPlay()
{
	Super::BeginPlay();

	// 画角をセット
	InitializeFieldOfView();
}

/// <summary>
/// 毎フレームの処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AMapCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

/// <summary>
/// SetupPlayerInputComponent
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AMapCamera::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMapCamera::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMapCamera::MoveRight);
	PlayerInputComponent->BindAxis("ZoomIn", this, &AMapCamera::ZoomIn);
	PlayerInputComponent->BindAxis("ZoomOut", this, &AMapCamera::ZoomOut);
}

/// <summary>
/// 画角の初期化
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AMapCamera::InitializeFieldOfView() {

	FieldOfViewAngle = InitAngleValue;
	FollowCamera->SetFieldOfView(FieldOfViewAngle);
}

/// <summary>
/// 画角の設定
/// </summary>
/// <param name="Angle"> 画角 </param>
/// <returns></returns>
void AMapCamera::SetFieldOfView(float Angle) {

	FieldOfViewAngle = Angle;
	if (FieldOfViewAngle <= MinAngleValue) {
		FieldOfViewAngle = MinAngleValue;
	}
	else if (FieldOfViewAngle >= MaxAngleValue) {
		FieldOfViewAngle = MaxAngleValue;
	}
	FollowCamera->SetFieldOfView(FieldOfViewAngle);
}

/// <summary>
/// 前進・後退
/// </summary>
/// <param name="Value"> 移動量 </param>
/// <returns></returns>
void AMapCamera::MoveForward(float Value) {

	UE_LOG(LogTemp, Display, TEXT("AMapCamera MoveForward Value:%f"), Value);

	if (Value == InitMoveValue) {
		return;
	}
	// プレイヤー方向に依存させずに、常にY方向に進むように変更
	const FVector Direction = FVector(InitMoveValue, MoveForwardValue, InitMoveValue);
	AddMovementInput(Direction, Value);
}

/// <summary>
/// 左右移動
/// </summary>
/// <param name="Value"> 移動量 </param>
/// <returns></returns>
void AMapCamera::MoveRight(float Value) {

	UE_LOG(LogTemp, Display, TEXT("AMapCamera MoveRight Value:%f"), Value);

	if (Value == InitMoveValue) {
		return;
	}
	// プレイヤー方向に依存させず、常にX方向に進むよう変更
	const FVector Direction = FVector(MoveRightValue, InitMoveValue, InitMoveValue);
	AddMovementInput(Direction, Value);
}

/// <summary>
/// ズームイン
/// </summary>
/// <param name="Value"> 移動量 </param>
/// <returns></returns>
void AMapCamera::ZoomIn(float Value) {

	UE_LOG(LogTemp, Display, TEXT("AMapCamera ZoomIn Value:%f"), Value);

	if (Value == InitMoveValue) {
		return;
	}
	// 画角を調整、カメラの画角を設定
	FieldOfViewAngle -= AdjustAngleValue;
	SetFieldOfView(FieldOfViewAngle);
}

/// <summary>
/// ズームアウト
/// </summary>
/// <param name="Value"> 移動量 </param>
/// <returns></returns>
void AMapCamera::ZoomOut(float Value) {

	UE_LOG(LogTemp, Display, TEXT("AMapCamera ZoomOut Value:%f"), Value);

	if (Value == InitMoveValue) {
		return;
	}
	// 画角を調整、カメラの画角を設定
	FieldOfViewAngle += AdjustAngleValue;
	SetFieldOfView(FieldOfViewAngle);
}

/// <summary>
/// 移動量
/// </summary>
/// <param name="Rate"> 割合 </param>
/// <returns></returns>
void AMapCamera::TurnAtRate(float Rate) {

	UE_LOG(LogTemp, Display, TEXT("AMapCamera TurnAtRate Rate:%f"), Rate);

	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

/// <summary>
/// 回転率
/// </summary>
/// <param name="Rate"> 割合 </param>
/// <returns></returns>
void AMapCamera::LookUpAtRate(float Rate) {

	UE_LOG(LogTemp, Display, TEXT("AMapCamera LookUpAtRate Rate:%f"), Rate);

	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}
