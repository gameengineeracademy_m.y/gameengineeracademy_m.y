﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "Battle/FieldManager.h"
#include "Battle/PlayerManager.h"
#include "Battle/BattleEvent.h"
#include "Battle/BattleGameMode.h"
#include "Battle/BattleController.h"
#include "Battle/PlayCamera.h"
#include "Battle/MapCamera.h"
#include "Battle/Arrow.h"
#include "Battle/FloorManager.h"
#include "Battle/ObjectManager.h"
#include "BattleLevel.generated.h"

/// <summary>
/// フェーズの種類
/// </summary>
UENUM()
enum class BattlePhaseType {
	Initialize,
	GameStart,
	Play,
	ChangeLevelPreparing,
	ChangeLevel,
	Finish,
	MaxPhaseIndex
};

/// <summary>
/// イベントフェーズの種類
/// </summary>
UENUM()
enum class EEventPhaseType {
	InitWait,      // 移動後待機フェーズ
	SearchEvent,   // イベント内容確認フェーズ
	EventWait0,    // 待機フェーズ
	ExecuteEvent,  // イベント実行フェーズ
	EventWait1,    // イベント処理フェーズ
	EventWait2,    // イベント処理フェーズ
	EventWait3,    // イベント処理フェーズ
	Finish,        // 終了フェーズ
	MaxPhaseIndex
};

/// <summary>
/// バトルレベル
/// </summary>
UCLASS()
class ABattleLevel : public ALevelScriptActor, public IBattleEvent
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ABattleLevel();

	/// <summary>
	/// 毎フレームの更新処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

	/// <summary>
	/// フィールドの座標を取得
	/// </summary>
	/// <param name="FieldNum"> フィールド番号 </param>
	/// <returns> 表示座標 </returns>
	virtual FVector OnGetFieldPosition(int32 FieldNum) override;

	/// <summary>
	/// 前方選択
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSelectFront() override;

	/// <summary>
	/// 左方向選択
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSelectLeft() override;

	/// <summary>
	/// 右方向選択
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSelectRight() override;

	/// <summary>
	/// ダイス切り替え時間変更
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnChangeDiceTime() override;

	/// <summary>
	/// マップ用カメラの切り替え
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSwitchMapCamera() override;

	/// <summary>
	/// サイコロを振る
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnRollDice() override;

	/// <summary>
	/// 決定操作
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnDecide() override;

	/// <summary>
	/// マスイベント処理
	/// </summary>
	/// <param name="DeltaTime"> 処理時間 </param>
	/// <returns> true:処理終了, false:処理中 </returns>
	virtual bool OnOccurEvent(float DeltaTime) override;

	/// <summary>
	/// プレイヤーの切り替え
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:リザルトへ, false:プレイ続行 </returns>
	virtual bool OnSwitchPlayer() override;

	/// <summary>
	/// 各オブジェクトの切り替え
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnSwitchObject() override;

	/// <summary>
	/// ターン開始処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnStartTurn() override;

	/// <summary>
	/// サイコロ処理が終了しているかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:終了, false:処理中 </returns>
	virtual bool OnIsFinishDice() override;

	/// <summary>
	/// プレイヤーのフェーズを「移動」に変更
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnChangePlayerPhaseMove() override;

	/// <summary>
	/// 次に進むマスを探索
	/// </summary>
	/// <param name=""></param>
	/// <returns> フィールド番号の配列 </returns>
	virtual TArray<int32> OnSearchForward() override;

	/// <summary>
	/// 分岐選択準備
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnPrepareSelectRoute() override;

	/// <summary>
	/// 分岐選択思考
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnThinkSelectRoute() override;

	/// <summary>
	/// 次に進むマスを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual int32 OnGetNextMovePos() override;

	/// <summary>
	/// サイコロウィジェットの更新
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnUpdateDiceWidget() override;

	/// <summary>
	/// プレイ用カメラをプレイヤーに追従させる
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnFollowPlayCamera() override;

	/// <summary>
	/// イベント処理終了
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnFinishEvent() override;

	/// <summary>
	/// 移動処理が終了したかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:終了, false:移動中 </returns>
	virtual bool OnIsFinishMove() override;

	/// <summary>
	/// イベント処理が終了しているかどうか
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:終了, false:処理中 </returns>
	virtual bool OnIsFinishEvent() override;

	/// <summary>
	/// 操作可能か取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:操作可能, false:操作不可 </returns>
	virtual bool OnCheckEnable() override;

	/// <summary>
	/// ゴールに到達したかチェック
	/// </summary>
	/// <param name=""></param>
	/// <returns> true:ゴール, false:ゴール以外 </returns>
	virtual bool OnCheckArriveGoal() override;

	/// <summary>
	/// ゲーム情報をリザルトへ渡す
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnPrepareResult() override;

	/// <summary>
	/// フィニッシュ表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnShowFinish() override;

	/// <summary>
	/// リザルト表示
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnShowResult() override;

	/// <summary>
	/// ゲーム終了処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void OnEndGame() override;

protected:

	/// <summary>
	/// 開始時実行処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

private:

	/// <summary>
	/// 現在のフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentPhase(BattlePhaseType PhaseType);

	/// <summary>
	/// イベントフェーズを変更する
	/// </summary>
	/// <param name="PhaseType"> フェーズの種類 </param>
	/// <returns></returns>
	void ChangeCurrentEventPhase(EEventPhaseType PhaseType);

	/// <summary>
	/// フィールドマネージャ 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeFieldManager();

	/// <summary>
	/// プレイヤーマネージャ 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializePlayerManager();

	/// <summary>
	/// フロアマネージャ 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeFloorManager();

	/// <summary>
	/// オブジェクトマネージャ 初期化処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeObjectManager();

	/// <summary>
	/// プレイ用カメラ 生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreatePlayCamera();

	/// <summary>
	/// マップ用カメラ 生成
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreateMapCamera();

	/// <summary>
	/// 分岐選択用矢印
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CreateArrow();

	/// <summary>
	/// 初期設定
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void Initialize();

	/// <summary>
	/// プレイカメラの位置情報セット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void SetupPlayCameraPosition();

	/// <summary>
	/// バトルウィジェットを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> バトルウィジェット </returns>
	UBattleWidget* GetBattleWidget();

	/// <summary>
	/// リザルトウィジェットを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> リザルトウィジェット </returns>
	UResultWidget* GetResultWidget();

	/// <summary>
	/// ダイスウィジェットを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ダイスウィジェット </returns>
	UDiceWidget* GetDiceWidget();

	/// <summary>
	/// ダイアログウィジェットを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ダイアログウィジェット </returns>
	UDialogWidget* GetDialogWidget();

	/// <summary>
	/// サウンドマネージャを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> サウンドマネージャ </returns>
	ASoundManager* GetSoundManager();

	/// <summary>
	/// 現在プレイ中のプレイヤーを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> プレイヤー </returns>
	APlayerBase* GetPlayablePlayer();

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	UPROPERTY()
	BattlePhaseType CurrentPhase;

	/// <summary>
	/// 現在のイベントフェーズ
	/// </summary>
	UPROPERTY()
	EEventPhaseType CurrentEventPhase;

	/// <summary>
	/// 累積時間
	/// </summary>
	UPROPERTY()
	float AccumulateTime;

	/// <summary>
	/// フィールドマネージャクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<AFieldManager> FieldManagerClass;

	/// <summary>
	/// フィールドマネージャ処理
	/// </summary>
	UPROPERTY()
	AFieldManager* FieldManager;

	/// <summary>
	/// プレイヤーマネージャクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<APlayerManager> PlayerManagerClass;

	/// <summary>
	/// プレイヤーマネージャ処理
	/// </summary>
	UPROPERTY()
	APlayerManager* PlayerManager;

	/// <summary>
	/// フロアマネージャクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<AFloorManager> FloorManagerClass;

	/// <summary>
	/// フロアマネージャ処理
	/// </summary>
	UPROPERTY()
	AFloorManager* FloorManager;

	/// <summary>
	/// オブジェクトマネージャクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<AObjectManager> ObjectManagerClass;

	/// <summary>
	/// オブジェクトマネージャ処理
	/// </summary>
	UPROPERTY()
	AObjectManager* ObjectManager;

	/// <summary>
	/// ゲームモード処理
	/// </summary>
	UPROPERTY()
	ABattleGameMode* BattleGameMode;

	/// <summary>
	/// プレイ用カメラクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<APlayCamera> PlayCameraClass;

	/// <summary>
	/// プレイ用カメラ処理
	/// </summary>
	UPROPERTY()
	APlayCamera* PlayCamera;

	/// <summary>
	/// マップ用カメラクラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<AMapCamera> MapCameraClass;

	/// <summary>
	/// マップ用カメラ処理
	/// </summary>
	UPROPERTY()
	AMapCamera* MapCamera;

	/// <summary>
	/// 矢印クラス
	/// </summary>
	UPROPERTY()
	TSubclassOf<AArrow> ArrowClass;

	/// <summary>
	/// 矢印処理
	/// </summary>
	UPROPERTY()
	AArrow* Arrow;

private:
	//-------------------------------
	// 以下、定数宣言
	//-------------------------------

	/// <summary>
	/// 終了待機時間
	/// </summary>
	const float WaitTime = 1.5f;

	/// <summary>
	/// フェードアウト終了待機時間
	/// </summary>
	const float FinishWaitTime = 0.5f;

	/// <summary>
	/// イベント初期待機時間
	/// </summary>
	const float EventInitWaitTime = 0.15f;

	/// <summary>
	/// イベント待機時間
	/// </summary>
	const float EventShortWaitTime = 1.0f;

	/// <summary>
	/// イベント待機時間
	/// </summary>
	const float EventWaitTime = 3.0f;

	/// <summary>
	/// リセット時間
	/// </summary>
	const float ResetTime = 0.0f;

	/// <summary>
	/// フィールドマネージャクラスパス
	/// </summary>
	const FString FieldManagerPath = "/Game/Blueprints/BP_FieldManager.BP_FieldManager_C";

	/// <summary>
	/// プレイヤーマネージャクラスパス
	/// </summary>
	const FString PlayerManagerPath = "/Game/Blueprints/BP_PlayerManager.BP_PlayerManager_C";

	/// <summary>
	/// フロアマネージャクラスパス
	/// </summary>
	const FString FloorManagerPath = "/Game/Blueprints/BP_FloorManager.BP_FloorManager_C";

	/// <summary>
	/// オブジェクトマネージャクラスパス
	/// </summary>
	const FString ObjectManagerPath = "/Game/Blueprints/BP_ObjectManager.BP_ObjectManager_C";

	/// <summary>
	/// プレイ用カメラクラスパス
	/// </summary>
	const FString PlayCameraPath = "/Game/Blueprints/BP_PlayCamera.BP_PlayCamera_C";

	/// <summary>
	/// マップ用カメラクラスパス
	/// </summary>
	const FString MapCameraPath = "/Game/Blueprints/BP_MapCamera.BP_MapCamera_C";

	/// <summary>
	/// 矢印クラスパス
	/// </summary>
	const FString ArrowPath = "/Game/Blueprints/BP_Arrow.BP_Arrow_C";

	/// <summary>
	/// プレイヤー表示座標 調整値
	/// </summary>
	const float PlayerPosZ = 280.0f;

	/// <summary>
	/// 矢印表示座標 調整値
	/// </summary>
	const float AllowAdjustPosZ = 10.0f;

	/// <summary>
	/// イベント種類列
	/// </summary>
	const int32 ColEventType = 0;

	/// <summary>
	/// イベント内容列
	/// </summary>
	const int32 ColEvent = 1;

	/// <summary>
	/// マイナス値
	/// </summary>
	const int32 Minus = -1;

	/// <summary>
	/// フィールドが存在しないマス
	/// </summary>
	const int32 FieldNotExists = 0;

	/// <summary>
	/// ゼロ
	/// </summary>
	const int32 Zero = 0;

	/// <summary>
	/// 壱
	/// </summary>
	const int32 One = 1;

	/// <summary>
	/// 百
	/// </summary>
	const int32 Hundred = 100;

	/// <summary>
	/// 最小インデックス
	/// </summary>
	const int32 MinIndex = 0;

	/// <summary>
	/// 存在しないプレイヤーの所持金
	/// </summary>
	const int32 NotExistPlayerMoney = -99999999;

	/// <summary>
	/// １着
	/// </summary>
	const int32 FirstOrder = 1;

	/// <summary>
	/// ２着
	/// </summary>
	const int32 SecondOrder = 2;

	/// <summary>
	/// ３着
	/// </summary>
	const int32 ThirdOrder = 3;

	/// <summary>
	/// ４着
	/// </summary>
	const int32 FourthOrder = 4;

	/// <summary>
	/// １着
	/// </summary>
	const int32 FirstOrderMoney = 3000000;

	/// <summary>
	/// ２着
	/// </summary>
	const int32 SecondOrderMoney = 1500000;

	/// <summary>
	/// ３着
	/// </summary>
	const int32 ThirdOrderMoney = 750000;

	/// <summary>
	/// ４着
	/// </summary>
	const int32 FourthOrderMoney = 300000;

	/// <summary>
	/// 待機中サイズ X方向
	/// </summary>
	const float WaitSizeX = 0.4f;

	/// <summary>
	/// 待機中サイズ Y方向
	/// </summary>
	const float WaitSizeY = 0.4f;

	/// <summary>
	/// 待機中サイズ Z方向
	/// </summary>
	const float WaitSizeZ = 0.4f;

	/// <summary>
	/// プレイ中サイズ X方向
	/// </summary>
	const float PlaySizeX = 1.0f;

	/// <summary>
	/// プレイ中サイズ Y方向
	/// </summary>
	const float PlaySizeY = 1.0f;

	/// <summary>
	/// プレイ中サイズ Z方向
	/// </summary>
	const float PlaySizeZ = 1.0f;

	/// <summary>
	/// カメラ位置 Z座標
	/// </summary>
	const float PlayCameraPosZ = 300.0f;
};