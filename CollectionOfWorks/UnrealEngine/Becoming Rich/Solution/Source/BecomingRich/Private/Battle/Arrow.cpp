// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/Arrow.h"


/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AArrow::AArrow() {

	PrimaryActorTick.bCanEverTick = true;

	// StaticMeshComponentを作成する
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	RootComponent = MeshComp;
}

/// <summary>
/// 実行開始イベント
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AArrow::BeginPlay() {

	Super::BeginPlay();

	RotateSpeed = RotSpeed;
	YawAngle = InitAngle;
}

/// <summary>
/// 毎フレームの処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AArrow::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

	YawAngle += RotateSpeed * DeltaTime;
	FRotator Rot = FRotator(InitAngle, YawAngle, InitAngle);
	MeshComp->SetRelativeRotation(Rot);
}

/// <summary>
/// PlayerInputComponent設定
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AArrow::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {

	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Front", EInputEvent::IE_Pressed, this, &AArrow::PressKeyW);
	PlayerInputComponent->BindAction("Left", EInputEvent::IE_Pressed, this, &AArrow::PressKeyA);
	PlayerInputComponent->BindAction("Right", EInputEvent::IE_Pressed, this, &AArrow::PressKeyD);
}

/// <summary>
/// プレイヤーを表示する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AArrow::Show() {

	MeshComp->SetVisibility(true);
}

/// <summary>
/// プレイヤーを非表示する
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AArrow::Hide() {

	MeshComp->SetVisibility(false);
}

/// <summary>
/// Wキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AArrow::PressKeyW() {

	// 操作可能かチェック
	bool IsEnable = EventInterface->OnCheckEnable();
	if (!IsEnable) {
		return;
	}
	EventInterface->OnSelectFront();
}

/// <summary>
/// Aキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AArrow::PressKeyA() {

	// 操作可能かチェック
	bool IsEnable = EventInterface->OnCheckEnable();
	if (!IsEnable) {
		return;
	}
	EventInterface->OnSelectLeft();
}

/// <summary>
/// Dキー押下
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AArrow::PressKeyD() {

	// 操作可能かチェック
	bool IsEnable = EventInterface->OnCheckEnable();
	if (!IsEnable) {
		return;
	}
	EventInterface->OnSelectRight();
}

/// <summary>
/// イベントインターフェース
/// </summary>
/// <param name=""> イベントインターフェース </param>
/// <returns></returns>
void AArrow::SetEventInterface(const TScriptInterface<IBattleEvent>& Interface) {

	EventInterface = Interface;
}
