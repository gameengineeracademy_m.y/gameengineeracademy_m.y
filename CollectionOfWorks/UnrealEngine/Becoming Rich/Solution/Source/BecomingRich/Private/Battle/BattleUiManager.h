// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "../UiAssignmentWidget.h"
#include "Battle/BattleWidget.h"
#include "Battle/ResultWidget.h"
#include "Battle/DiceWidget.h"
#include "Battle/DialogWidget.h"
#include "BattleUiManager.generated.h"


/// <summary>
/// バトルUIマネージャ処理
/// </summary>
UCLASS()
class ABattleUiManager : public AHUD
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	ABattleUiManager();

	/// <summary>
	/// PreInitializeComponents
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void PreInitializeComponents() override;

	/// <summary>
	/// OpenBattle
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void OpenBattle();

	/// <summary>
	/// ClosePreparing
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void ClosePreparing();

	/// <summary>
	/// CloseBattle
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void CloseBattle();

	/// <summary>
	/// バトルウィジェットを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> バトルウィジェット </returns>
	UBattleWidget* GetBattleWidget();

	/// <summary>
	/// リザルトウィジェットを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> リザルトウィジェット </returns>
	UResultWidget* GetResultWidget();

	/// <summary>
	/// ダイスウィジェットを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ダイスウィジェット </returns>
	UDiceWidget* GetDiceWidget();

	/// <summary>
	/// ダイアログウィジェットを取得
	/// </summary>
	/// <param name=""></param>
	/// <returns> ダイアログウィジェット </returns>
	UDialogWidget* GetDialogWidget();

	/// <summary>
	/// バトルウィジェットクラス
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	TSubclassOf<UBattleWidget> BattleWidgetClass;

	/// <summary>
	/// バトルウィジェット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	UBattleWidget* BattleWidget;

	/// <summary>
	/// リザルトウィジェットクラス
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	TSubclassOf<UResultWidget> ResultWidgetClass;

	/// <summary>
	/// リザルトウィジェット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	UResultWidget* ResultWidget;

	/// <summary>
	/// ダイスウィジェットクラス
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	TSubclassOf<UDiceWidget> DiceWidgetClass;

	/// <summary>
	/// ダイスウィジェット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	UDiceWidget* DiceWidget;

	/// <summary>
	/// ダイアログウィジェットクラス
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	TSubclassOf<UDialogWidget> DialogWidgetClass;

	/// <summary>
	/// ダイアログウィジェット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	UDialogWidget* DialogWidget;

	/// <summary>
	/// フェードウィジェットクラス
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	TSubclassOf<UUiAssignmentWidget> FadeWidgetClass;

	/// <summary>
	/// フェードウィジェット
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	UPROPERTY()
	UUiAssignmentWidget* FadeWidget;
};