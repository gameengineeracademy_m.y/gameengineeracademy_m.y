﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Battle/FloorManager.h"

/// <summary>
/// コンストラクタ
/// </summary>
/// <param name=""></param>
/// <returns></returns>
AFloorManager::AFloorManager() {

	PrimaryActorTick.bCanEverTick = false;

}

/// <summary>
/// 開始時実行処理
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AFloorManager::BeginPlay() {

	Super::BeginPlay();
	
	// フィールドマップ情報の読み込み
	FString ProjectPath = FPaths::ProjectDir();
	FString FilePath = ProjectPath + FloorMapCsvFilePath;

	FloorMap = ReadCsvFloorMapFile(FilePath);
	if (FloorMap.Num() == Zero) {
		UE_LOG(LogTemp, Display, TEXT("AFloorManager ReadCsvFloorMapFile Error"));
		return;
	}

	// フィールドデータの読み込み
	FilePath.Empty();
	FilePath = ProjectPath + FloorDataCsvFilePath;

	FloorData = ReadCsvFloorDataFile(FilePath);
	if (FloorData.Num() == Zero) {
		UE_LOG(LogTemp, Display, TEXT("AFloorManager ReadCsvFloorDataFile Error"));
		return;
	}

	// フロアキーデータのセット
	FloorKeyData.Add(KeyDataGround);
	FloorKeyData.Add(KeyDataWetland);
	FloorKeyData.Add(KeyDataPalace);

	// フロアの生成
	CreateFloor();
}

/// <summary>
/// 毎フレームの更新処理
/// </summary>
/// <param name="DeltaTime"> 処理時間 </param>
/// <returns></returns>
void AFloorManager::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);
}

/// <summary>
/// フロアデータCSV読み込み処理
/// </summary>
/// <param name="FilePath"> ファイルパス </param>
/// <returns> フィールド配列 </returns>
TMap<FString, FString> AFloorManager::ReadCsvFloorDataFile(FString FilePath) {

	UE_LOG(LogTemp, Display, TEXT("AFloorManager ReadCsvFloorDataFile"));

	TMap<FString, FString> FileData;
	FileData.Empty();

	// CSVファイルからデータを取得
	FString CsvDataFile;
	FFileHelper::LoadFileToString(CsvDataFile, *FilePath);
	if (CsvDataFile.IsEmpty()) {
		return FileData;
	}

	// 各行で分割した値の保持
	TArray<FString> Row;
	// カンマ区切りごとの値
	TArray<FString> Element;
	// 行ごとに配列に取得
	CsvDataFile.ParseIntoArrayLines(Row);

	for (int i = 1; i < Row.Num(); ++i) {
		// カンマ区切りで取得
		Row[i].ParseIntoArray(Element, TEXT(","), true);
		// 配列に格納
		FileData.Add(Element[FloorNumIndex], Element[PathIndex]);
		// 一時配列の初期化
		Element.Empty();
	}

	return FileData;
}

/// <summary>
/// フロアCSV読み込み処理
/// </summary>
/// <param name="FilePath"> ファイルパス </param>
/// <returns> フィールド配列 </returns>
TMap<int32, TArray<FString>> AFloorManager::ReadCsvFloorMapFile(FString FilePath) {

	UE_LOG(LogTemp, Display, TEXT("AFloorManager ReadCsvFloorMapFile"));

	TMap<int32, TArray<FString>> FileData;
	FileData.Empty();

	// CSVファイルからデータを取得
	FString CsvDataFile;
	FFileHelper::LoadFileToString(CsvDataFile, *FilePath);
	if (CsvDataFile.IsEmpty()) {
		return FileData;
	}

	// 各行で分割した値の保持
	TArray<FString> Row;
	// カンマ区切りごとの値
	TArray<FString> Element;
	// 行ごとに配列に取得
	CsvDataFile.ParseIntoArrayLines(Row);

	for (int i = 0; i < Row.Num(); ++i) {
		// カンマ区切りで取得
		Row[i].ParseIntoArray(Element, TEXT(","), true);
		// 配列に格納
		FileData.Add(i, Element);
		// 一時配列の初期化
		Element.Empty();
	}

	return FileData;
}

/// <summary>
/// フロア生成
/// </summary>
/// <param name=""></param>
/// <returns></returns>
void AFloorManager::CreateFloor() {

	UE_LOG(LogTemp, Display, TEXT("AFloorManager CreateFloor"));

	// アクタクラス取得
	TSubclassOf<class AFloorBase> FloorActor = TSoftClassPtr<AFloorBase>(FSoftObjectPath(*FloorClassPath)).LoadSynchronous();
	if (!IsValid(FloorActor)) {
		return;
	}

	FVector FloorPosition;

	for (auto RowData : FloorMap) {

		for (int32 i = 0; i < static_cast<int>(RowData.Value.Num()); ++i) {

			// フィールド番号の先頭1桁を取得
			FString FloorKey = RowData.Value[i].Left(1);
			if (!FloorKeyData.Contains(FloorKey)) {
				UE_LOG(LogTemp, Display, TEXT("AFloorManager FloorIndex Error"));
				return;
			}
			// 配置位置の確定
			FloorPosition.X = FloorPosX + i * FloorSizeWidth + FloorSizeWidth / Half;
			FloorPosition.Y = FloorPosY + RowData.Key * FloorSizeHeight + FloorSizeHeight / Half;
			FloorPosition.Z = FloorPosZ;

			// フロアの生成
			AFloorBase* Floor = GetWorld()->SpawnActor<AFloorBase>(FloorActor);
			// 配置座標をセット、初期化処理実行
			FVector Scale = FVector(FloorScaleX, FloorScaleY, FloorScaleZ);
			FTransform ActorTrans;
			ActorTrans.SetLocation(FloorPosition);
			ActorTrans.SetScale3D(Scale);
			Floor->SetActorTransform(ActorTrans);

			Floor->SetFloorStrNum(RowData.Value[i]);
			// マテリアルセット
			FString MaterialPath;
			if (FloorKey == KeyDataGround) {
				MaterialPath = FloorData[KeyDataGround];
			}
			else if (FloorKey == KeyDataWetland) {
				MaterialPath = FloorData[KeyDataWetland];
			}
			else if (FloorKey == KeyDataPalace) {
				MaterialPath = FloorData[KeyDataPalace];
			}

			if (!MaterialPath.IsEmpty()) {
				Floor->SetMaterial(MaterialPath);
			}

			// 配列にセット
			FloorDataDetail.Add(RowData.Value[i], Floor);
		}
	}

	UE_LOG(LogTemp, Display, TEXT("AFloorManager CreateFloor Success!"));
}