// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MapCamera.generated.h"

/// <summary>
/// マップ用カメラ
/// </summary>
UCLASS()
class AMapCamera : public ACharacter
{
	GENERATED_BODY()

	/// <summary>
	/// カメラアーム
	/// </summary>
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* SpringArm;

	/// <summary>
	/// カメラ
	/// </summary>
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	AMapCamera();

	/// <summary>
	/// 画角の初期化
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	void InitializeFieldOfView();

	/// <summary>
	/// 画角の設定
	/// </summary>
	/// <param name="Angle"> 画角 </param>
	/// <returns></returns>
	void SetFieldOfView(float Angle);

	/// <summary>
	/// 
	/// </summary>
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/// <summary>
	/// 
	/// </summary>
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

protected:

	/// <summary>
	/// 実行開始時処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void BeginPlay() override;

	/// <summary>
	/// 移動量
	/// </summary>
	/// <param name="Rate"> 割合 </param>
	/// <returns></returns>
	void TurnAtRate(float Rate);

	/// <summary>
	/// 回転率
	/// </summary>
	/// <param name="Rate"> 割合 </param>
	/// <returns></returns>
	void LookUpAtRate(float Rate);

public:

	/// <summary>
	/// 毎フレームの処理
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void Tick(float DeltaTime) override;

	/// <summary>
	/// SetupPlayerInputComponent
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	/// <summary>
	/// 前進・後退
	/// </summary>
	/// <param name="Value"> 移動量 </param>
	/// <returns></returns>
	void MoveForward(float Value);

	/// <summary>
	/// 左右移動
	/// </summary>
	/// <param name="Value"> 移動量 </param>
	/// <returns></returns>
	void MoveRight(float Value);

	/// <summary>
	/// ズームイン
	/// </summary>
	/// <param name="Value"> 移動量 </param>
	/// <returns></returns>
	void ZoomIn(float Value);

	/// <summary>
	/// ズームアウト
	/// </summary>
	/// <param name="Value"> 移動量 </param>
	/// <returns></returns>
	void ZoomOut(float Value);

	/// <summary>
	/// 画角
	/// </summary>
	float FieldOfViewAngle;


private:

	/// <summary>
	/// 移動量初期値
	/// </summary>
	const float InitMoveValue = 0.0f;

	/// <summary>
	/// カメラ前進後退移動量
	/// </summary>
	const float MoveForwardValue = -1.0f;

	/// <summary>
	/// カメラ前進後退移動量
	/// </summary>
	const float MoveBackValue = 1.0f;

	/// <summary>
	/// カメラ左右移動量
	/// </summary>
	const float MoveRightValue = 1.0f;

	/// <summary>
	/// 回転量初期値
	/// </summary>
	const float InitRotValue = 0.0f;

	/// <summary>
	/// 画角初期値
	/// </summary>
	const float InitAngleValue = 90.0f;

	/// <summary>
	/// 画角 調整値
	/// </summary>
	const float AdjustAngleValue = 15.0f;

	/// <summary>
	/// 画角 最小値
	/// </summary>
	const float MinAngleValue = 60.0f;

	/// <summary>
	/// 画角 最大値
	/// </summary>
	const float MaxAngleValue = 150.0f;

	/// <summary>
	/// マイナス
	/// </summary>
	const float MinusSign = -1.0f;
};
