// Copyright Epic Games, Inc. All Rights Reserved.

#include "BecomingRich.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, BecomingRich, "BecomingRich" );
