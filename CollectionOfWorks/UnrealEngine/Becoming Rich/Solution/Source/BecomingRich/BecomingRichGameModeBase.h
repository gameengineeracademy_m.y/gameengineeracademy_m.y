// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BecomingRichGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BECOMINGRICH_API ABecomingRichGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
